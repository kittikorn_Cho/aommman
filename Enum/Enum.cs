﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Enum
{
    enum ERoomType
    {
        Suite = 1,
        Vip = 2,
        SuiteSub = 3,
        Normal = 4,
        Suite3 = 5,
        Suite4 = 6,
        Suite5 = 7,
        RoundSell = 8,
        C3 = 9,
        C5 = 10,
        Suite6 = 11,
        Suite7 = 12,
        Suite8 = 13,
        Suite9 = 14,
        Suite10 = 15,
        Vip2 = 16,
        Vip3 =17,
    }
    enum ERoomStatus
    {
        Ready = 1,
        Working = 2,
        Defective = 0
    }
    enum EAngelStatus
    {
        Ready = 1,
        Working = 2,
        BackHome = 3,
        Cancel = 4
    }
    enum EMemberPayment
    {
        Paid = 1,
        Unpaid = 2
    }
    enum EPaymentType
    {
        Cash = 1,
        Credit = 2,
        Member = 3,
        CashAndCredit = 4,
        CashAndMember = 5,
        CreditAndMember = 6,
        CashAndCreditAndMember = 7,
        QrCode = 8,
        CashAndQrCode = 9,
        CreditAndQrCode = 10,
        MemberAndQrCode = 11,
        CashAndCreditAndQrCode = 12,
        CashAndMemberAndQrCode = 13,
        CreditAndMemberAndQrCode = 14,
        CashAndCreditAndMemberAndQrCode = 15,
        NoPay = 99,

    }
    enum ECreditType 
    {
        Visa = 1,
        Master = 2,
        Amex = 3,
        PlatinumTitanium = 4,
        Coupon = 5,
        Other = 6,
        QRCODE = 7,
    }

    enum EReport
    {
        isMassage = 1,
        isMember = 2,
    }
    enum EDailydebt
    {
        Dailydebt1 = 1,
        Dailydebt2 = 2,
    }
    enum EDeducttype
    {
        Comm1 = 1,
        Comm2 = 3,
    }
}
