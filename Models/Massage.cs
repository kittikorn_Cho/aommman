﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Models
{
    public class Massage : BaseEntity
    {
        [Key]
        public int Id { get; set; }
        public int CalendarDayId { get; set; }
        public bool IsPaid { get; set; }
        public bool IsUnPaid { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal Total { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal AngelTotal { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal FoodTotal { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal RoomTotal { get; set; }
        public bool IsCashAngel { get; set; }
        public bool IsCreditAngel { get; set; }
        public bool IsMemberAngel { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal CashAngelTotal { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal CreditAngelTotal { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal MemberAngelTotal { get; set; }
        public bool IsCashFood { get; set; }
        public bool IsCreditFood { get; set; }
        public bool IsMemberFood { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal CashFoodTotal { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal CreditFoodTotal { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal MemberFoodTotal { get; set; }
        public bool IsCashRoom { get; set; }
        public bool IsCreditRoom { get; set; }
        public bool IsMemberRoom { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal CashRoomTotal { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal CreditRoomTotal { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal MemberRoomTotal { get; set; }
        public bool IsEntertainAngel { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal EntertainTotalAngel { get; set; }
        public string EntertainRemarkAngel { get; set; }
        public bool IsEntertainFood { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal EntertainTotalFood { get; set; }
        public string EntertainRemarkFood { get; set; }
        public DateTime? PayDate { get; set; }
        public DateTime? PayUnPaidDate { get; set; }
        public int? CancelBy { get; set; }
        public DateTime? CancelDate { get; set; }
        public string DocumentNumber { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal EtcTotal { get; set; }
        public bool IsCashEtc { get; set; }
        public bool IsCreditEtc { get; set; }
        public bool IsMemberEtc { get; set; }
        public bool IsEntertainEtc { get; set; }
        public string EntertainEtcRemark { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal CashEtcTotal { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal CreditEtcTotal { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal MemberEtcTotal { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal EntertainEtcTotal { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal TipTotal { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal TipCommTotal { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal DamagesTotal { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal OtherServiceChargesTotal { get; set; }
        public string EtcRemark { get; set; }
        public int? CashierId { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal QrCodeTotal { get; set; }
        public bool IsQrCodeAngel { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal QrCodeAngelTotal { get; set; }
        public bool IsQrCodeFood { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal QrCodeFoodTotal { get; set; }
        public bool IsQrCodeRoom { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal QrCodeRoomTotal { get; set; }
        public bool IsQrCodeEtc { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal QrCodeEtcTotal { get; set; }
        public string UnPaidRemark { get; set; }
        public bool IsPaymentTip { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal TipQrCodeTotal { get; set; }
        public int CountriesId { get; set; }
        public int AgenciesId { get; set; }
        public int AgencyPerformanceId { get; set; }
        public bool IsCheckInLater { get; set; }    

        [ForeignKey("CarlendarDayId")]
        public virtual CalendarDay CalendarDay { get; set; }
        [ForeignKey("CountriesId")]
        public virtual Countries Countries { get; set; }
        [ForeignKey("AgenciesId")]
        public virtual Agency Agency { get; set; }
        public CalendarDay CalendarDays { get; set; }
    }
}
