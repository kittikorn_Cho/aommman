﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Models
{
    public class User : BaseEntity
    {
        [Key]
        public int Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public byte[] PasswordSalt { get; set; }
        public string UserFullname { get; set; }
        public string Tel { get; set; }
        public int Status { get; set; }

        public List<UserRole> UserRoles { get; set; }

        [NotMapped]
        public List<Role> Roles { get; set; }
        [NotMapped]
        public List<Menu> Menus { get; set; }
    }
}
