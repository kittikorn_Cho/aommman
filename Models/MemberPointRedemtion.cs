﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace OONApi.Models
{
    public class MemberPointRedemtion : BaseEntity
    {
        public int Id { get; set; }
        public int MemberId { get; set; }
        public int CalendarDayId { get; set; }

        [Column(TypeName = "decimal(18, 2)")]
        public decimal Point { get; set; }

        [Column(TypeName = "decimal(18, 2)")]
        public decimal PointRedem { get; set; }
        public string Remark { get; set; }

        [ForeignKey("MemberId")]
        public virtual Member Member { get; set; }

        [ForeignKey("CalendarDayId")]
        public virtual CalendarDay CalendarDay { get; set; }


    }
}
