﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;


namespace OONApi.Models
{
    public class AngelType : BaseEntity
    {
        [Key]
        public int Id { get; set; }
        public string Code { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal Fee { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal Wage { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal CreditComm { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal CheerComm { get; set; }
        public int RoundTime { get; set; }
        public int Order { get; set; }
        public bool IsSpecialComm { get; set; }
        public bool IsSpecialClean { get; set; }



    }
}
