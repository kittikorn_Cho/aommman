﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace OONApi.Models
{
    public class DailyPriceHistory: BaseEntity
    {
        [Key]
        public int Id { get; set; }
        public int CalendarDayId { get; set; }
        public int AngelTypeId { get; set; }
        public int CommId1 { get; set; }
        public int CommId2 { get; set; }
        public int DailydebtId1 { get; set; }
        public int DailydebtId2 { get; set; }

        [Column(TypeName = "decimal(18, 2)")]
        public decimal Round { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal Price { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal Wage { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal Comm1 { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal Comm2 { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal Dailydebt1 { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal Dailydebt2 { get; set; }
        [Column(TypeName = "decimal(18, 2)")]

        [ForeignKey("CarlendarDayId")]
        public virtual CalendarDay CalendarDay { get; set; }
        [ForeignKey("AngelTypeId")]
        public virtual AngelType AgencyType { get; set; }
    }
}
