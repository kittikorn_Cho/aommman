﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Models
{
    public class Item : BaseEntity
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Slug { get; set; }

        public List<MemberItem> MemberItems { get; set; }
        public List<MemberTypeItem> MemberTypeItems { get; set; }
    }
}
