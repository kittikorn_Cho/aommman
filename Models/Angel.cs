﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Models
{
    public class Angel : BaseEntity
    {
        [Key]
        public int Id { get; set; }
        public int AngelTypeId { get; set; }
        public string Code { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Nickname { get; set; }
        public string Tel { get; set; }
        public int? Status { get; set; }
        public DateTime? WorkFromDate { get; set; }
        public DateTime? WorkToDate { get; set; }
        public string Rfid { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal Point { get; set; }

        [ForeignKey("AngelTypeId")]
        public virtual AngelType AngelType { get; set; }
        public ICollection<AngelDebt> AngelDebts{ get; set; }
    }
}
