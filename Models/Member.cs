﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Models
{
    public class Member : BaseEntity
    {
        [Key]
        public int Id { get; set; }
        public string Code { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Tel { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal CreditAmount { get; set; }
        public int ReceptionId { get; set; }
        public DateTime ExpiredDate { get; set; }
        public int Status { get; set; }
        public DateTime? FromDate { get; set; }
        public int CountriesId { get; set; }
        public int AgenciesId { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal Point { get; set; }

        [ForeignKey("ReceptionId")]
        public virtual Reception Reception { get; set; }
        [ForeignKey("CountriesId")]
        public virtual Countries Countries { get; set; }
        [ForeignKey("AgenciesId")]
        public virtual Agency Agency { get; set; }

        public List<MemberItem> MemberItems { get; set; }
        public List<MemberTransaction> MemberTransactions { get; set; }
    }
}
