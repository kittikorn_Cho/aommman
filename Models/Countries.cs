﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Models
{
    public class Countries
    {
        [Key]
        public int Id { get; set; }
        public string NameEn { get; set; }
        public string NameTh { get; set; }
    }
}
