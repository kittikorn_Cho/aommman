﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Models
{
    public class DocumentNumber : BaseEntity
    {
        [Key]
        public int Id { get; set; }
        public int CalendarDayId { get; set; }
        public int RunningNumber { get; set; }
        public int PadLength { get; set; }

        [ForeignKey("CalendarDayId")]
        public virtual CalendarDay CalendarDay { get; set; }

    }
}
