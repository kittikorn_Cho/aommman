﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Models
{
    public class AngelPerformanceDeduct : BaseEntity
    {
        [Key]
        public int Id { get; set; }
        public int MassageAngelId { get; set; }
        public int DeductTypeId { get; set; }
        public int AngelPerformanceId { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal Fee { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal DiscountFee { get; set; }

        [ForeignKey("MassageAngelId")]
        public MassageAngel MassageAngel { get; set; }
        [ForeignKey("DeductTypeId")]
        public DeductType DeductType { get; set; }
        [ForeignKey("AngelPerformanceId")]
        public AngelPerformance AngelPerformance { get; set; }
    }
}
