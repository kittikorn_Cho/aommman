﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OONApi.Models
{
    public class MassageMemberAndRoom : BaseEntity
    {
        [Key]
        public int Id { get; set; }
        public int MemberId { get; set; }
        public int MassageRoomId { get; set; }
        public int MassageMemberPaymentId { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal Round { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal RoomWage { get; set; }

        [ForeignKey("MemberId")]
        public Member Member { get; set; }
        [ForeignKey("MassageRoomId")]
        public MassageRoom MassageRoom { get; set; }
        [ForeignKey("MassageMemberPaymentId")]
        public MassageMemberPayment MassageMemberPayment { get; set; }
    }
}
