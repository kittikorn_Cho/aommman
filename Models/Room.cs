﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Models
{
    public class Room:BaseEntity
    {
        [Key]
        public int Id { get; set; }
        public int RoomTypeId { get; set; }
        public int? ParentRoomId { get; set; }
        public string RoomNo { get; set; }
        public int Status { get; set; }
        public int BuildingTypeId { get; set; }

        [ForeignKey("RoomTypeId")]
        public virtual RoomType RoomType { get; set; }
        [ForeignKey("BuildingTypeId")]
        public virtual BuildingType BuildingType { get; set; }

    }
}
