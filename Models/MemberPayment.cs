﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OONApi.Models
{
    public class MemberPayment : BaseEntity
    {
        [Key]
        public int Id { get; set; }
        public int CalendarId { get; set; }
        public string DocumentNumber { get; set; }
        public int MemberId { get; set; }
        public string Name_1 { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal Amount_1 { get; set; }
        public string Name_2 { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal Amount_2 { get; set; }
        public string Name_3 { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal Amount_3 { get; set; }
        public string Name_4 { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal Amount_4 { get; set; }
        public string Name_5 { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal Amount_5 { get; set; }

        [Column(TypeName = "decimal(18, 2)")]
        public decimal CerditAmount { get; set; }
        public bool IsPay { get; set; }
        public bool IsUnPaid { get; set; }
        public DateTime? PayDate { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal Total { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal UnpaidTotal { get; set; }
        public bool IsCash { get; set; }
        public bool IsCredit { get; set; }
        public bool IsQrCode { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal CashTotal { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal CreditTotal { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal QrCodeTotal { get; set; }


        [ForeignKey("MemberId")]
        public Member Member { get; set; }

        [ForeignKey("CalendarId")]
        public CalendarDay CalendarDay { get; set; }
    }
}
