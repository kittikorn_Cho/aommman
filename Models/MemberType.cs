﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Models
{
    public class MemberType : BaseEntity
    {
        [Key]
        public int Id { get; set; }
        public string Code { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal Fee { get; set; }

        public List<MemberTypeItem> MemberTypeItems { get; set; }
    }
}
