﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OONApi.Models
{
    public class AngelGallery : BaseEntity
    {
        [Key]
        public int Id { get; set; }
        public int AngelId { get; set; }
        public string Path { get; set; }

        [ForeignKey("AngelId")]
        public virtual Angel Angel { get; set; }
    }
}
