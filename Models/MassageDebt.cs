﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OONApi.Models
{
    public class MassageDebt : BaseEntity
    {
        [Key]
        public int Id { get; set; }
        public int MassageId { get; set; }

        [Column(TypeName = "decimal(18, 2)")]
        public decimal DebtTotal { get; set; }

        [Column(TypeName = "decimal(18, 2)")]
        public decimal CashTotal { get; set; }

        [Column(TypeName = "decimal(18, 2)")]
        public decimal CreditTotal { get; set; }

        [Column(TypeName = "decimal(18, 2)")]
        public decimal QrTotal { get; set; }

        [Column(TypeName = "decimal(18, 2)")]
        public decimal MemberTotal { get; set; }

        [Column(TypeName = "decimal(18, 2)")]
        public decimal EntertainTotal { get; set; }


        [ForeignKey("MassageId")]
        public virtual Massage Massage { get; set; }

    }
}
