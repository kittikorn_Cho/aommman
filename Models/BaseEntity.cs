﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Models
{
    public abstract class BaseEntity
    {
        public int? CreatedBy { get; set; } = null!;
        public DateTime? CreatedDate { get; set; } = null!;
        public int? UpdatedBy { get; set; } = null!;
        public DateTime? UpdatedDate { get; set; } = null!;
        public int? DeletedBy { get; set; } = null!;
        public DateTime? DeletedDate { get; set; } = null!;
    }
}
