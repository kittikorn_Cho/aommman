﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Models
{
    public class AngelDoctor : BaseEntity
    {
        [Key]
        public int Id { get; set; }
        public int AngelId { get; set; }
        public string DoctorName { get; set; }
        public string Detail { get; set; }

        [ForeignKey("AngelId")]
        public virtual Angel Angel { get; set; }
    }
}
