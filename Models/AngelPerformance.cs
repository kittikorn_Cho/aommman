﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Models
{
    public class AngelPerformance : BaseEntity
    {
        [Key]
        public int Id { get; set; }
        public int CarlendarDayId { get; set; }
        public int AngelId { get; set; }
        public bool IsPay { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal TotalFee { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal TotalWage { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal TotalReceive { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal TotalDeduct { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal TotalDebt { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal TotalDailyDebt { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal TotalRound { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal AnotherDeductAmount { get; set; }
        public string AnotherDeductDescription { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal AnotherAddAmount { get; set; }
        public string AnotherAddDescription { get; set; }
        public bool IsPerformanceNew { get; set; }

        [ForeignKey("CarlendarDayId")]
        public virtual CalendarDay CalendarDay { get; set; }

        [ForeignKey("AngelId")]
        public virtual Angel Angel { get; set; }

    }
}
