﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Models
{
    public class MassageMemberPayment : BaseEntity
    {
        [Key]
        public int Id { get; set; }
        public int MassageId { get; set; }
        public int MemberId { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal MemberTotal { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal UseSuite { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal UseVip { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal MemberFoodTotal { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal MemberMassageTotal { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal MemberRoomTotal { get; set; }


        [ForeignKey("MassageId")]
        public Massage Massage { get; set; }
        [ForeignKey("MemberId")]
        public Member Member { get; set; }
    }
}
