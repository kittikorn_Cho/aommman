﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Models
{
    public class MemberTransaction : BaseEntity
    {
        [Key]
        public int Id { get; set; }
        public int CalendarDayId { get; set; }
        public int MemberId { get; set; }
        public int ReceptionId { get; set; }
        public bool IsPaid { get; set; }
        public bool IsUnPaid { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal Total { get; set; }
        public bool IsCash { get; set; }
        public bool IsCredit { get; set; }
        public bool IsEntertain { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal CashTotal { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal CreditTotal { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal EntertainTotal { get; set; }
        public DateTime? PayDate { get; set; }
        public string DocumentNumber { get; set; }
        public bool IsQrCode { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal QrCodeTotal { get; set; }
        public bool IsPercent { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal PercentTopup { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal PercentTotalTopup { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal Point { get; set; }


        [ForeignKey("CalendarDayId")]
        public CalendarDay CalendarDay { get; set; }
        [ForeignKey("MemberId")]
        public Member Member { get; set; }
        [ForeignKey("ReceptionId")]
        public Reception Reception { get; set; }

        public List<MemberItemTransaction> MemberItemTransactions { get; set; }
    }
}
