﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Models
{
    public class AngelPerformanceDebt : BaseEntity
    {
        [Key]
        public int Id { get; set; }
        public int DebtTypeId { get; set; }
        public int AngelPerformanceId { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal Fee { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal DebtNow { get; set; }

        [ForeignKey("DebtTypeId")]
        public DebtType DebtType { get; set; }
        [ForeignKey("AngelPerformanceId")]
        public AngelPerformance AngelPerformance { get; set; }
    }
}
