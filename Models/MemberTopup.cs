﻿using System.ComponentModel.DataAnnotations.Schema;

namespace OONApi.Models
{
    public class MemberTopup : BaseEntity
    {
        public int Id { get; set; }
        public int MemberId { get; set; }
        public int CalendarId { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal CerditAmount { get; set; }
        public string Name_1 { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal Amount_1 { get; set; }
        public string Name_2 { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal Amount_2 { get; set; }
        public string Name_3 { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal Amount_3 { get; set; }
        public string Name_4 { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal Amount_4 { get; set; }
        public string Name_5 { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal Amount_5 { get; set; }

        [ForeignKey("MemberId")]
        public Member Member { get; set; }
        [ForeignKey("CalendarId")]
        public CalendarDay calendarDay { get; set; }

        public Item Item { get; set; }
    }
}
