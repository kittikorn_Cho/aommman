﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace OONApi.Models
{
    public class AppDbContext : DbContext
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {
            _httpContextAccessor = this.GetService<IHttpContextAccessor>();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Angel>().Property<DateTime?>("DeletedDate");
            modelBuilder.Entity<Angel>().HasQueryFilter(m => EF.Property<DateTime?>(m, "DeletedDate") == null);
            modelBuilder.Entity<AngelDebt>().Property<DateTime?>("DeletedDate");
            modelBuilder.Entity<AngelDebt>().HasQueryFilter(m => EF.Property<DateTime?>(m, "DeletedDate") == null);
            modelBuilder.Entity<AngelDoctor>().Property<DateTime?>("DeletedDate");
            modelBuilder.Entity<AngelDoctor>().HasQueryFilter(m => EF.Property<DateTime?>(m, "DeletedDate") == null);
            modelBuilder.Entity<AngelPerformance>().Property<DateTime?>("DeletedDate");
            modelBuilder.Entity<AngelPerformance>().HasQueryFilter(m => EF.Property<DateTime?>(m, "DeletedDate") == null);
            modelBuilder.Entity<AngelPerformanceDebt>().Property<DateTime?>("DeletedDate");
            modelBuilder.Entity<AngelPerformanceDebt>().HasQueryFilter(m => EF.Property<DateTime?>(m, "DeletedDate") == null);
            modelBuilder.Entity<AngelPerformanceDeduct>().Property<DateTime?>("DeletedDate");
            modelBuilder.Entity<AngelPerformanceDeduct>().HasQueryFilter(m => EF.Property<DateTime?>(m, "DeletedDate") == null);
            modelBuilder.Entity<AngelType>().Property<DateTime?>("DeletedDate");
            modelBuilder.Entity<AngelType>().HasQueryFilter(m => EF.Property<DateTime?>(m, "DeletedDate") == null);
            modelBuilder.Entity<AngelWorking>().Property<DateTime?>("DeletedDate");
            modelBuilder.Entity<AngelWorking>().HasQueryFilter(m => EF.Property<DateTime?>(m, "DeletedDate") == null);
            modelBuilder.Entity<BuildingType>().Property<DateTime?>("DeletedDate");
            modelBuilder.Entity<BuildingType>().HasQueryFilter(m => EF.Property<DateTime?>(m, "DeletedDate") == null);
            modelBuilder.Entity<DebtType>().Property<DateTime?>("DeletedDate");
            modelBuilder.Entity<DebtType>().HasQueryFilter(m => EF.Property<DateTime?>(m, "DeletedDate") == null);
            modelBuilder.Entity<DeductType>().Property<DateTime?>("DeletedDate");
            modelBuilder.Entity<DeductType>().HasQueryFilter(m => EF.Property<DateTime?>(m, "DeletedDate") == null);
            modelBuilder.Entity<Item>().Property<DateTime?>("DeletedDate");
            modelBuilder.Entity<Item>().HasQueryFilter(m => EF.Property<DateTime?>(m, "DeletedDate") == null);
            modelBuilder.Entity<Massage>().Property<DateTime?>("DeletedDate");
            modelBuilder.Entity<Massage>().HasQueryFilter(m => EF.Property<DateTime?>(m, "DeletedDate") == null);
            modelBuilder.Entity<MassageAngel>().Property<DateTime?>("DeletedDate");
            modelBuilder.Entity<MassageAngel>().HasQueryFilter(m => EF.Property<DateTime?>(m, "DeletedDate") == null);
            modelBuilder.Entity<MassageCreditPayment>().Property<DateTime?>("DeletedDate");
            modelBuilder.Entity<MassageCreditPayment>().HasQueryFilter(m => EF.Property<DateTime?>(m, "DeletedDate") == null);
            modelBuilder.Entity<MassageMemberPayment>().Property<DateTime?>("DeletedDate");
            modelBuilder.Entity<MassageMemberPayment>().HasQueryFilter(m => EF.Property<DateTime?>(m, "DeletedDate") == null);
            modelBuilder.Entity<MassageRoom>().Property<DateTime?>("DeletedDate");
            modelBuilder.Entity<MassageRoom>().HasQueryFilter(m => EF.Property<DateTime?>(m, "DeletedDate") == null);
            modelBuilder.Entity<MemberItem>().Property<DateTime?>("DeletedDate");
            modelBuilder.Entity<MemberItem>().HasQueryFilter(m => EF.Property<DateTime?>(m, "DeletedDate") == null);
            modelBuilder.Entity<Member>().Property<DateTime?>("DeletedDate");
            modelBuilder.Entity<Member>().HasQueryFilter(m => EF.Property<DateTime?>(m, "DeletedDate") == null);
            modelBuilder.Entity<MemberTransaction>().Property<DateTime?>("DeletedDate");
            modelBuilder.Entity<MemberTransaction>().HasQueryFilter(m => EF.Property<DateTime?>(m, "DeletedDate") == null);
            modelBuilder.Entity<MemberType>().Property<DateTime?>("DeletedDate");
            modelBuilder.Entity<MemberType>().HasQueryFilter(m => EF.Property<DateTime?>(m, "DeletedDate") == null);
            modelBuilder.Entity<MemberTypeItem>().Property<DateTime?>("DeletedDate");
            modelBuilder.Entity<MemberTypeItem>().HasQueryFilter(m => EF.Property<DateTime?>(m, "DeletedDate") == null);
            modelBuilder.Entity<Menu>().Property<DateTime?>("DeletedDate");
            modelBuilder.Entity<Menu>().HasQueryFilter(m => EF.Property<DateTime?>(m, "DeletedDate") == null);
            modelBuilder.Entity<Reception>().Property<DateTime?>("DeletedDate");
            modelBuilder.Entity<Reception>().HasQueryFilter(m => EF.Property<DateTime?>(m, "DeletedDate") == null);
            modelBuilder.Entity<Role>().Property<DateTime?>("DeletedDate");
            modelBuilder.Entity<Role>().HasQueryFilter(m => EF.Property<DateTime?>(m, "DeletedDate") == null);
            modelBuilder.Entity<Room>().Property<DateTime?>("DeletedDate");
            modelBuilder.Entity<Room>().HasQueryFilter(m => EF.Property<DateTime?>(m, "DeletedDate") == null);
            modelBuilder.Entity<RoomType>().Property<DateTime?>("DeletedDate");
            modelBuilder.Entity<RoomType>().HasQueryFilter(m => EF.Property<DateTime?>(m, "DeletedDate") == null);
            modelBuilder.Entity<User>().Property<DateTime?>("DeletedDate");
            modelBuilder.Entity<User>().HasQueryFilter(m => EF.Property<DateTime?>(m, "DeletedDate") == null);
            modelBuilder.Entity<DailyDebt>().Property<DateTime?>("DeletedDate");
            modelBuilder.Entity<DailyDebt>().HasQueryFilter(m => EF.Property<DateTime?>(m, "DeletedDate") == null);
            modelBuilder.Entity<MemberPayment>().Property<DateTime?>("DeletedDate");
            modelBuilder.Entity<MemberPayment>().HasQueryFilter(m => EF.Property<DateTime?>(m, "DeletedDate") == null);
            modelBuilder.Entity<MemberItemTransaction>().Property<DateTime?>("DeletedDate");
            modelBuilder.Entity<MemberItemTransaction>().HasQueryFilter(m => EF.Property<DateTime?>(m, "DeletedDate") == null);
            modelBuilder.Entity<MemberTopup>().Property<DateTime?>("DeletedDate");
            modelBuilder.Entity<MemberTopup>().HasQueryFilter(m => EF.Property<DateTime?>(m, "DeletedDate") == null);
            modelBuilder.Entity<AngelGallery>().Property<DateTime?>("DeletedDate");
            modelBuilder.Entity<AngelGallery>().HasQueryFilter(m => EF.Property<DateTime?>(m, "DeletedDate") == null);
            modelBuilder.Entity<MassageQrCodePayment>().Property<DateTime?>("DeletedDate");
            modelBuilder.Entity<MassageQrCodePayment>().HasQueryFilter(m => EF.Property<DateTime?>(m, "DeletedDate") == null);
            modelBuilder.Entity<Agency>().Property<DateTime?>("DeletedDate");
            modelBuilder.Entity<Agency>().HasQueryFilter(m => EF.Property<DateTime?>(m, "DeletedDate") == null);
            modelBuilder.Entity<MassageMemberAndRoom>().Property<DateTime?>("DeletedDate");
            modelBuilder.Entity<MassageMemberAndRoom>().HasQueryFilter(m => EF.Property<DateTime?>(m, "DeletedDate") == null);
            modelBuilder.Entity<Booking>().Property<DateTime?>("DeletedDate");
            modelBuilder.Entity<Booking>().HasQueryFilter(m => EF.Property<DateTime?>(m, "DeletedDate") == null);
            modelBuilder.Entity<BookingAngel>().Property<DateTime?>("DeletedDate");
            modelBuilder.Entity<BookingAngel>().HasQueryFilter(m => EF.Property<DateTime?>(m, "DeletedDate") == null);
            modelBuilder.Entity<AngelPointTransaction>().Property<DateTime?>("DeletedDate");
            modelBuilder.Entity<AngelPointTransaction>().HasQueryFilter(m => EF.Property<DateTime?>(m, "DeletedDate") == null);
            modelBuilder.Entity<ReceptionPointTransaction>().Property<DateTime?>("DeletedDate");
            modelBuilder.Entity<ReceptionPointTransaction>().HasQueryFilter(m => EF.Property<DateTime?>(m, "DeletedDate") == null);
            modelBuilder.Entity<MemberPointRedemtion>().Property<DateTime?>("DeletedDate");
            modelBuilder.Entity<MemberPointRedemtion>().HasQueryFilter(m => EF.Property<DateTime?>(m, "DeletedDate") == null);
            modelBuilder.Entity<AngelPointRedemtion>().Property<DateTime?>("DeletedDate");
            modelBuilder.Entity<AngelPointRedemtion>().HasQueryFilter(m => EF.Property<DateTime?>(m, "DeletedDate") == null);
            modelBuilder.Entity<ReceptionPointRedemtion>().Property<DateTime?>("DeletedDate");
            modelBuilder.Entity<ReceptionPointRedemtion>().HasQueryFilter(m => EF.Property<DateTime?>(m, "DeletedDate") == null);
            modelBuilder.Entity<MassageDebt>().Property<DateTime?>("DeletedDate");
            modelBuilder.Entity<MassageDebt>().HasQueryFilter(m => EF.Property<DateTime?>(m, "DeletedDate") == null);

            modelBuilder.Entity<MemberTypeItem>()
                .HasOne(m => m.MemberType)
                .WithMany(m => m.MemberTypeItems)
                .HasForeignKey(m => m.MemberTypeId)
                .OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<MemberTypeItem>()
                .HasOne(i => i.Item)
                .WithMany(i => i.MemberTypeItems)
                .HasForeignKey(i => i.ItemId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<MemberItem>()
                 .HasOne(m => m.Member)
                 .WithMany(m => m.MemberItems)
                 .HasForeignKey(m => m.MemberId)
                 .OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<MemberItem>()
                .HasOne(i => i.Item)
                .WithMany(i => i.MemberItems)
                .HasForeignKey(i => i.ItemId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<UserRole>().HasKey(r => new { r.UserId, r.RoleId });
            modelBuilder.Entity<UserRole>()
                .HasOne(c => c.User)
                .WithMany(cp => cp.UserRoles)
                .HasForeignKey(cf => cf.UserId)
                .OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<UserRole>()
                .HasOne(p => p.Role)
                .WithMany(pm => pm.UserRoles)
                .HasForeignKey(f => f.RoleId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<RoleMenu>().HasKey(r => new { r.RoleId, r.MenuId });
            modelBuilder.Entity<RoleMenu>()
                .HasOne(c => c.Role)
                .WithMany(cp => cp.RoleMenus)
                .HasForeignKey(cf => cf.RoleId)
                .OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<RoleMenu>()
                .HasOne(p => p.Menu)
                .WithMany(pm => pm.RoleMenus)
                .HasForeignKey(f => f.MenuId)
                .OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<Angel>()
                .HasIndex(i => i.Rfid)
                .IsUnique();

            modelBuilder.Seed();
        }

        public override Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = new CancellationToken())
        {
            AddTimeStamps();
            UpdateSoftDeleteStatuses();
            return base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
        }

        private void UpdateSoftDeleteStatuses()
        {

            var entries = ChangeTracker.Entries().Where(e =>
                e.Entity is BaseEntity && (e.State == EntityState.Added || e.State == EntityState.Deleted));

            var userId = _httpContextAccessor.HttpContext.User.Identity.Name;

            int? currentUserId = !string.IsNullOrEmpty(userId) ? (Int32.TryParse(userId, out var tempVal) ? tempVal : (int?)null) : null;

            foreach (var entityEntry in entries)
            {
                switch (entityEntry.State)
                {
                    case EntityState.Added:
                        ((BaseEntity)entityEntry.Entity).DeletedDate = null;
                        break;
                    case EntityState.Deleted:
                        entityEntry.State = EntityState.Modified;
                        ((BaseEntity)entityEntry.Entity).DeletedBy = currentUserId;
                        ((BaseEntity)entityEntry.Entity).DeletedDate = DateTime.Now;
                        break;
                }
            }
        }

        private void AddTimeStamps()
        {
            if (_httpContextAccessor.HttpContext != null)
            {
                var entries = ChangeTracker.Entries().Where(e =>
                e.Entity is BaseEntity && (e.State == EntityState.Added || e.State == EntityState.Modified));

                // var userId = _httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value;
                var userId = _httpContextAccessor.HttpContext.User.Identity.Name;

                int? currentUserId = !string.IsNullOrEmpty(userId) ? (Int32.TryParse(userId, out var tempVal) ? tempVal : (int?)null) : null;

                foreach (var entityEntry in entries)
                {
                    switch (entityEntry.State)
                    {
                        case EntityState.Added:
                            ((BaseEntity)entityEntry.Entity).CreatedDate = DateTime.Now;
                            ((BaseEntity)entityEntry.Entity).CreatedBy = currentUserId;
                            break;
                        case EntityState.Modified:
                            entityEntry.Property("CreatedDate").IsModified = false;
                            entityEntry.Property("CreatedBy").IsModified = false;
                            ((BaseEntity)entityEntry.Entity).UpdatedDate = DateTime.Now;
                            ((BaseEntity)entityEntry.Entity).UpdatedBy = currentUserId;
                            break;
                    }
                }
            }
        }


        public DbSet<Test> Tests { get; set; }
        public DbSet<Angel> Angels { get; set; }
        public DbSet<AngelDebt> AngelDebts { get; set; }
        public DbSet<AngelDoctor> AngelDoctors { get; set; }
        public DbSet<AngelPerformance> AngelPerformances { get; set; }
        public DbSet<AngelPerformanceDebt> AngelPerformanceDebts { get; set; }
        public DbSet<AngelPerformanceDeduct> AngelPerformanceDeducts { get; set; }
        public DbSet<AngelType> AngelTypes { get; set; }
        public DbSet<AngelWorking> AngelWorkings { get; set; }
        public DbSet<BuildingType> BuildingTypes { get; set; }
        public DbSet<CalendarDay> CalendarDays { get; set; }
        public DbSet<DebtType> DebtTypes { get; set; }
        public DbSet<DeductType> DeductTypes { get; set; }
        public DbSet<Item> Items { get; set; }
        public DbSet<Massage> Massages { get; set; }
        public DbSet<MassageAngel> MassageAngels { get; set; }
        public DbSet<MassageCreditPayment> MassageCreditPayments { get; set; }
        public DbSet<MassageMemberPayment> MassageMemberPayments { get; set; }
        public DbSet<MassageRoom> MassageRooms { get; set; }
        public DbSet<Member> Members { get; set; }
        public DbSet<MemberItem> MemberItems { get; set; }
        public DbSet<MemberTransaction> MemberTransactions { get; set; }
        public DbSet<MemberType> MemberTypes { get; set; }
        public DbSet<MemberTypeItem> MemberTypeItems { get; set; }
        public DbSet<Menu> Menus { get; set; }
        public DbSet<Reception> Receptions { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Room> Rooms { get; set; }
        public DbSet<RoomType> RoomTypes { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<RoleMenu> RoleMenu { get; set; }
        public DbSet<DailyDebt> DailyDebts { get; set; }
        public DbSet<DocumentNumber> DocumentNumbers { get; set; }
        public DbSet<MemberPayment> memberPayments { get; set; }
        public DbSet<MemberItemTransaction> MemberItemTransactions { get; set; }
        public DbSet<MemberTopup> MemberTopups { get; set; }
        public DbSet<AngelGallery> AngelGallerys { get; set; }
        public DbSet<MassageQrCodePayment> MassageQrCodePayments { get; set; }
        public DbSet<Setting> Settings { get; set; }
        public DbSet<Countries> Countries { get; set; }
        public DbSet<Agency> Agency { get; set; }
        public DbSet<AgencyPerformance> AgencyPerformances { get; set; }
        public DbSet<AgencyType> AgencyTypes { get; set; }
        public DbSet<MassageMemberAndRoom> massageMemberAndRooms { get; set; }
        public DbSet<Booking> Bookings { get; set; }
        public DbSet<BookingAngel> BookingAngels { get; set; }
        public DbSet<AngelPointTransaction> AngelPointTransactions { get; set; }
        public DbSet<ReceptionPointTransaction> ReceptionPointTransactions { get; set; }
        public DbSet<MemberPointRedemtion> MemberPointRedemtions { get; set; }
        public DbSet<AngelPointRedemtion> AngelPointRedemtions { get; set; }
        public DbSet<ReceptionPointRedemtion> ReceptionPointRedemtions { get; set; }
        public DbSet<MassageDebt> MassageDebts { get; set; }
        public DbSet<DailyPriceHistory> DailyPriceHistorys { get; set;}
    }
}
