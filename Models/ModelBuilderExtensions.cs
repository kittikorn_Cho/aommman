﻿using Microsoft.EntityFrameworkCore;
using OONApi.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Models
{
    public static class ModelBuilderExtensions
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Role>().HasData(
                new Role
                {
                    Id = 1,
                    Name = "ซุปเปอร์แอดมิน",
                    Code = "_SUPERADMIN"
                },
                new Role
                {
                    Id = 2,
                    Name = "แอดมิน",
                    Code = "ADMIN"
                },
                 new Role
                 {
                     Id = 3,
                     Name = "แคชเชียร์",
                     Code = "CASHIER"
                 },
                 new Role
                 {
                     Id = 4,
                     Name = "เชียร์แขก",
                     Code = "RECEPT"
                 },
                 new Role
                 {
                     Id = 5,
                     Name = "ฝ่ายบุคคล",
                     Code = "HR"
                 },
                 new Role
                 {
                     Id = 6,
                     Name = "บัญชี",
                     Code = "ACC"
                 }
            );
            HashSalt hashSalt = UserService.EncryptPassword("P@ssw0rd");
            modelBuilder.Entity<User>().HasData(
                new User
                {
                    Id = 1,
                    Username = "superadmin",
                    Password = hashSalt.Hash,
                    PasswordSalt = hashSalt.Salt,
                    UserFullname = "SuperAdministrator",
                    Status = 1,
                }
            );

            modelBuilder.Entity<UserRole>().HasData(
                new UserRole
                {
                    UserId = 1,
                    RoleId = 1
                }
            );

            modelBuilder.Entity<RoomType>().HasData(
                new RoomType
                {
                    Id = 1,
                    Code = "suite",
                    Name = "สูท",
                    RoomRate = 1000,
                },
                new RoomType
                {
                    Id = 2,
                    Code = "vip",
                    Name = "วีไอพี",
                    RoomRate = 600,
                },
                new RoomType
                {
                    Id = 3,
                    Code = "suiteSub",
                    Name = "สูทย่อย",
                },
                new RoomType
                {
                    Id = 4,
                    Code = "normal",
                    Name = "ธรรมดา",
                },
                new RoomType
                {
                    Id = 5,
                    Code = "suite3",
                    Name = "สูท3",
                },
                new RoomType
                {
                    Id = 6,
                    Code = "suite4",
                    Name = "สูท4",
                },
                new RoomType
                {
                    Id = 7,
                    Code = "suite5",
                    Name = "สูท5",
                },
                new RoomType
                {
                    Id = 8,
                    Code = "roundSell",
                    Name = "ซื้อรอบ",
                },
                new RoomType
                {
                    Id = 9,
                    Code = "c3",
                    Name = "สูทC3",
                },
                new RoomType
                {
                    Id = 10,
                    Code = "c5",
                    Name = "สูทC5",
                },
                new RoomType
                {
                    Id = 11,
                    Code = "suite6",
                    Name = "สูท6",
                },
                new RoomType
                {
                    Id = 12,
                    Code = "suite7",
                    Name = "สูท7",
                },
                new RoomType
                {
                    Id = 13,
                    Code = "suite8",
                    Name = "สูท8",
                },
                new RoomType
                {
                    Id = 14,
                    Code = "suite9",
                    Name = "สูท9",
                },
                new RoomType
                {
                    Id = 15,
                    Code = "suite10",
                    Name = "สูท10",
                },
                new RoomType
                {
                    Id = 16,
                    Code = "vip2",
                    Name = "วีไอพี2",
                },
                new RoomType
                {
                    Id = 17,
                    Code = "vip3",
                    Name = "วีไอพี3",
                }
            );

            modelBuilder.Entity<BuildingType>().HasData(
                new BuildingType
                {
                    Id = 1,
                    Name = "ตึกหลัก",
                }
            );

            List<Menu> menus = new List<Menu> {  new Menu
                {
                    Id = 1,
                    Code = "Dashboard",
                    Name = "Dashboard",
                    Slug = "",
                },
                new Menu
                {
                    Id = 2,
                    Code = "Angel",
                    Name = "พนักงานบริการ --> จัดการข้อมูลพนักงานบริการ",
                    Slug = "",
                },
                new Menu
                {
                    Id = 3,
                    Code = "AngelType",
                    Name = "พนักงานบริการ --> ประเภทพนักงานบริการ",
                    Slug = "",
                },
                new Menu
                {
                    Id = 4,
                    Code = "AngelDebt",
                    Name = "พนักงานบริการ -->  ประเภทหนี้คงที่พนักงานบริการ",
                    Slug = "",
                },
                new Menu
                {
                    Id = 5,
                    Code = "AngelDeduct",
                    Name = "พนักงานบริการ -->  ประเภทนี้หักตามรอบพนักงานบริการ",
                    Slug = "",
                },
                new Menu
                {
                    Id = 6,
                    Code = "AngelDoctor",
                    Name = "พนักงานบริการ --> ตรวจสุขภาพ",
                    Slug = "",
                },
                new Menu
                {
                    Id = 7,
                    Code = "AngelWorkingList",
                    Name = "พนักงานบริการ --> แสดงเวลาปฎิบัติงาน",
                    Slug = "",
                },
                new Menu
                {
                    Id = 8,
                    Code = "AngelRoundMonth",
                    Name = "พนักงานบริการ --> ตรวจสอบจำนวนรอบตามช่วง",
                    Slug = "",
                },
                new Menu
                {
                    Id = 9,
                    Code = "AngelSalaryCountDay",
                    Name = "พนักงานบริการ --> ตรวจสอบรอบ-เวลาทำงาน",
                    Slug = "",
                },
                new Menu
                {
                    Id = 10,
                    Code = "AngelHistoryDebt",
                    Name = "พนักงานบริการ --> ประวัติหักหนี้",
                    Slug = "",
                },
                new Menu
                {
                    Id = 11,
                    Code = "AngelManageWorking",
                    Name = "พนักงานบริการ --> ลงเวลาปฎิบัติงาน",
                    Slug = "",
                },

                new Menu
                {
                    Id = 12,
                    Code = "Member",
                    Name = "เมมเบอร์ --> จัดการข้อมูลเมมเบอร์",
                    Slug = "",
                },
                new Menu
                {
                    Id = 13,
                    Code = "MemberType",
                    Name = "เมมเบอร์ --> ประเภทเมมเบอร์",
                    Slug = "",
                },
                new Menu
                {
                    Id = 14,
                    Code = "MemberItem",
                    Name = "เมมเบอร์ --> ของแถมเมมเบอร์",
                    Slug = "",
                },
                new Menu
                {
                    Id = 15,
                    Code = "MemberPayment",
                    Name = "เมมเบอร์ --> จัดการข้อมูลชำระเงินเมมเบอร์",
                    Slug = "",
                },
                new Menu
                {
                    Id = 16,
                    Code = "Reception",
                    Name = "พนักงานต้อนรับ",
                    Slug = "",
                },
                new Menu
                {
                    Id = 17,
                    Code = "ReceptionSell",
                    Name ="สรุปการทำงาน",
                    Slug = "",
                },
                new Menu
                {
                    Id = 18,
                    Code = "Room",
                    Name = "ห้อง",
                    Slug = "",
                },
                new Menu
                {
                    Id = 19,
                    Code = "Role",
                    Name = "สิทธิ์",
                    Slug = "",
                },
                new Menu
                {
                    Id = 20,
                    Code = "User",
                    Name = "ผู้ใช้ระบบ",
                    Slug = "",
                },
                new Menu
                {
                    Id = 21,
                    Code = "CheckIn",
                    Name = "เช็คอิน",
                    Slug = "",
                },
                new Menu
                {
                    Id = 22,
                    Code = "SettingDailyDebt",
                    Name = "ตั้งค่า --> ค่าใช้จ่ายประจำวัน",
                    Slug = "",
                },
                new Menu
                {
                    Id = 23,
                    Code = "Working",
                    Name = "สถานะการทำงาน",
                    Slug = "",
                },
                new Menu
                {
                    Id = 24,
                    Code = "WagePayment",
                    Name = "รอการชำระเงิน",
                    Slug = "",
                },
                new Menu
                {
                    Id = 25,
                    Code = "AngelPerformance",
                    Name = "จ่ายค่าตอบแทน",
                    Slug = "",
                },
                new Menu
                {
                    Id = 26,
                    Code = "UnPaid",
                    Name = "ค้างชำระ",
                    Slug = "",
                },

                 new Menu
                {
                    Id = 27,
                    Code = "EditBillReception",
                    Name = "พนักงานต้อนรับ --> แก้หัวเชียร์",
                    Slug = "",
                },
                  new Menu
                {
                    Id = 28,
                    Code = "Report",
                    Name = "รายงาน",
                    Slug = "",
                },
                new Menu
                {
                    Id = 29,
                    Code = "DailyRemittanceSum",
                    Name = "รายงาน --> ใบสรุปส่งเงินประจำวัน",
                    Slug = "",
                },
                new Menu
                {
                    Id = 30,
                    Code = "DailyRemittanceSumFinance",
                    Name = "รายงาน --> สรุปยอดขายประจำวัน (บัญชี)",
                    Slug = "",
                },
                new Menu
                {
                    Id = 31,
                    Code = "DiscountMaasageRoom",
                    Name = "รายงาน --> รับรองส่วนลดห้อง",
                    Slug = "",
                },
                new Menu
                {
                    Id = 32,
                    Code = "DiscountMaasageAngel",
                    Name = "รายงาน --> รับรองส่วนลดนวด",
                    Slug = "",
                },
                new Menu
                {
                    Id = 33,
                    Code = "UnpaidMassage",
                    Name = "รายงาน --> ค้างชำระบิล",
                    Slug = "",
                },
                 new Menu
                {
                    Id = 34,
                    Code = "MemberSaleSum",
                    Name = "รายงาน --> สรุปยอดขายเมมเบอร์",
                    Slug = "",
                },
                new Menu
                {
                    Id = 35,
                    Code = "MemberRoleSum",
                    Name = "รายงาน --> สรุปใช้สิทธ์เมมเบอร์",
                    Slug = "",
                },
                 new Menu
                {
                    Id = 36,
                    Code = "MemberExpire",
                    Name = "รายงาน --> วันหมดอายุสมาชิก",
                    Slug = "",
                },
                new Menu
                {
                    Id = 37,
                    Code = "CouponPaymentAmount",
                    Name = "รายงาน --> ใบสรุปส่งเงิน(จ่ายคูปอง)",
                    Slug = "",
                },
                 new Menu
                {
                    Id = 38,
                    Code = "InfoPayment",
                    Name = "รายการชำระเงิน",
                    Slug = "",
                },
                new Menu
                {
                    Id = 39,
                    Code = "ReceptionSell",
                    Name = "รายงาน --> ยอดเมมเบอร์ระหว่างวัน",
                    Slug = "",
                },
                new Menu
                {
                    Id = 40,
                    Code = "ReceptionRoundTotal",
                    Name = "รายงาน --> สรุปยอดเชียร์",
                    Slug = "",
                },
                new Menu
                {
                    Id = 41,
                    Code = "TipAngel",
                    Name = "จ่ายทิปพนักงาน",
                    Slug = "",
                },
                new Menu
                {
                    Id = 42,
                    Code = "SearchRoom",
                    Name = "ค้นหาห้อง",
                    Slug = "",
                },
                new Menu
                {
                    Id = 43,
                    Code = "TableCashier",
                    Name = "รายงาน --> สรุปแคชเชียร์รายบุคคล",
                    Slug = "",
                },
                new Menu
                {
                    Id = 44,
                    Code = "MemberFoodPayment",
                    Name = "เมมเบอร์ --> จัดการข้อมูลชำระเงินค่าอาหารเมมเบอร์",
                    Slug = "",
                },
                new Menu
                {
                    Id = 45,
                    Code = "MembeFoodReport",
                    Name = "รายงาน --> ตัดค่าอาหารเมมเบอร์",
                    Slug = "",
                },
                new Menu
                {
                    Id = 46,
                    Code = "SellSuite",
                    Name = "รายงาน --> สรุปการขายห้องสูท",
                    Slug = "",
                },
                new Menu
                {
                    Id = 47,
                    Code = "MemberTopupReport",
                    Name = "รายงาน --> สรุปยอดออนท็อปเมมเบอร์",
                    Slug = "",
                },
                new Menu
                {
                    Id = 48,
                    Code = "MemberCreditAmount",
                    Name = "รายงาน --> ยอดคงเหลือเมมเบอร์",
                    Slug = "",
                },
                new Menu
                {
                    Id = 49,
                    Code = "MassageSetting",
                    Name = "ตั้งค่า --> ตั้งค่าระบบ",
                    Slug = "",
                },
                new Menu
                {
                    Id = 50,
                    Code = "AllMemberExpire",
                    Name = "รายงาน --> วันหมดอายุสมาชิกทั้งหมด",
                    Slug = "",
                },
                new Menu
                {
                    Id = 51,
                    Code = "CountWorkDaily",
                    Name = "รายงาน --> ตรวจสอบ-เวลาทำงาน",
                    Slug = "",
                },
                new Menu
                {
                    Id = 52,
                    Code = "AngelDebtReport",
                    Name = "รายงาน --> สรุปหนี้พนักงาน",
                    Slug = "",
                },
                new Menu
                {
                    Id = 53,
                    Code = "InComeDaily",
                    Name = "รายงาน --> สรุปรายได้ประจำวันที่",
                    Slug = "",
                },
                new Menu
                {
                    Id = 54,
                    Code = "CountriesMassageOrMemberList",
                    Name = "รายงาน --> สรุปประเทศที่มาใช้งาน",
                    Slug = "",
                },
                new Menu
                {
                    Id = 55,
                    Code = "ReportAgenciesList",
                    Name = "รายงาน --> จ่ายค่าตอบแทนเอเจนซี่",
                    Slug = "",
                },
                new Menu
                {
                    Id = 56,
                    Code = "Agency",
                    Name = "เอเจนซี่ -->  จัดการข้อมูลเอเจนซี่",
                    Slug = "",
                },
                new Menu
                {
                    Id = 57,
                    Code = "AgencyPerformance",
                    Name = "เอเจนซี่ --> จ่ายค่าตอบแทนเอเจนซี่",
                    Slug = "",
                },
                new Menu
                {
                    Id = 58,
                    Code = "MergeCheckIn",
                    Name = "เช็คอินห้องทะลุ",
                    Slug = "",
                },
                new Menu
                {
                    Id = 59,
                    Code = "Booking",
                    Name = "จองห้อง",
                    Slug = "",
                },
                new Menu
                {
                    Id = 60,
                    Code = "Point",
                    Name = "สะสมคะแนน",
                    Slug = "",
                },
                new Menu
                {
                    Id = 61,
                    Code = "PointMember",
                    Name = "พ้อยท์เมมเบอร์ --> จัดการพ้อยท์เมมเบอร์",
                    Slug = "",
                },
                new Menu
                {
                    Id = 62,
                    Code = "PointAngel",
                    Name = "พ้อยท์พนักงานบริการ --> จัดการพ้อยท์พนักงานบริการ",
                    Slug = "",
                },
                new Menu
                {
                    Id = 63,
                    Code = "PointReception",
                    Name = "พ้อยท์พนักงานต้อนรับ --> จัดการพ้อยท์พนักงานต้อนรับ",
                    Slug = "",
                },
                new Menu
                {
                    Id = 64,
                    Code = "SettingPoint",
                    Name = "ตั้งค่า --> ตั้งค่าคะแนน",
                    Slug = "",
                },
                 new Menu
                {
                    Id = 65,
                    Code = "CheckInLater",
                    Name = "เช็คอินย้อนหลัง",
                    Slug = "",
                },
            };
            modelBuilder.Entity<Menu>().HasData(
               menus
            );

            List<RoleMenu> roleMenus = new List<RoleMenu>();
            foreach (var item in menus)
            {
                roleMenus.Add(new RoleMenu { RoleId = 1, MenuId = item.Id });
            }

            modelBuilder.Entity<RoleMenu>().HasData(
                roleMenus
            );

            modelBuilder.Entity<Countries>().HasData(
               new Countries
               {
                   Id = 1,
                   NameEn = "Not Specified",
                   NameTh = "ไม่ระบุ"
               },
               new Countries
              {
                  Id = 2,
                  NameEn = "Aruba",
                  NameTh = "อารูบา"
               },
               new Countries
               {
                   Id = 3,
                   NameEn = "Afghanistan",
                   NameTh = "อัฟกานิสถาน"
               },
               new Countries
               {
                   Id = 4,
                   NameEn = "Angola",
                   NameTh = "แองโกลา"
               },
               new Countries
               {
                   Id = 5,
                   NameEn = "Anguilla",
                   NameTh = "แองกวิลลา"
               },
               new Countries
               {
                   Id = 6,
                   NameEn = "Åland",
                   NameTh = "โอลันด์"
               },
               new Countries
               {
                   Id = 7,
                   NameEn = "Albania",
                   NameTh = "แอลเบเนีย"
               },
               new Countries
               {
                   Id = 8,
                   NameEn = "Andorra",
                   NameTh = "อันดอร์รา"
               },
               new Countries
               {
                   Id = 9,
                   NameEn = "United Arab Emirates",
                   NameTh = "สหรัฐอาหรับเอมิเรตส์"
               },
               new Countries
               {
                   Id = 10,
                   NameEn = "Argentina",
                   NameTh = "อาร์เจนตินา"
               },
               new Countries
               {
                   Id = 11,
                   NameEn = "Armenia",
                   NameTh = "อาร์เมเนีย"
               },
               new Countries
               {
                   Id = 12,
                   NameEn = "American Samoa",
                   NameTh = "อเมริกันซามัว"
               },
               new Countries
               {
                   Id = 13,
                   NameEn = "Antarctica",
                   NameTh = "ทวิปแอนตาร์กติกา"
               },
               new Countries
               {
                   Id = 14,
                   NameEn = "French Southern Territories",
                   NameTh = "ดินแดนทางตอนใต้ของฝรั่งเศส"
               },
               new Countries
               {
                   Id = 15,
                   NameEn = "Antigua and Barbuda",
                   NameTh = "แอนติกาและบาร์บูดา"
               },
               new Countries
               {
                   Id = 16,
                   NameEn = "Australia",
                   NameTh = "ออสเตรเลีย"
               },
               new Countries
               {
                   Id = 17,
                   NameEn = "Austria",
                   NameTh = "ออสเตรีย"
               },
               new Countries
               {
                   Id = 18,
                   NameEn = "Azerbaijan",
                   NameTh = "อาเซอร์ไบจาน"
               },
               new Countries
               {
                   Id = 19,
                   NameEn = "Burundi",
                   NameTh = "บุรุนดี"
               },
               new Countries
               {
                   Id = 20,
                   NameEn = "Belgium",
                   NameTh = "เบลเยียม"
               },
               new Countries
               {
                   Id = 21,
                   NameEn = "Benin",
                   NameTh = "เบนิน"
               },
               new Countries
               {
                   Id = 22,
                   NameEn = "Bonaire",
                   NameTh = "โบแนร์"
               },
               new Countries
               {
                   Id = 23,
                   NameEn = "Burkina Faso",
                   NameTh = "บูร์กินาฟาโซ"
               },
               new Countries
               {
                   Id = 24,
                   NameEn = "Bangladesh",
                   NameTh = "บังคลาเทศ"
               },
               new Countries
               {
                   Id = 25,
                   NameEn = "Bulgaria",
                   NameTh = "บัลแกเรีย"
               },
               new Countries
               {
                   Id = 26,
                   NameEn = "Bahrain",
                   NameTh = "บาห์เรน"
               },
               new Countries
               {
                   Id = 27,
                   NameEn = "Bahamas",
                   NameTh = "บาฮามาส"
               },
               new Countries
               {
                   Id = 28,
                   NameEn = "Bosnia and Herzegovina",
                   NameTh = "บอสเนียและเฮอร์เซโก"
               },
               new Countries
               {
                   Id = 29,
                   NameEn = "Saint Barthélemy",
                   NameTh = "เซนต์บาร์เตเลมี"
               },
               new Countries
               {
                   Id = 30,
                   NameEn = "Belarus",
                   NameTh = "เบลารุส"
               },
               new Countries
               {
                   Id = 31,
                   NameEn = "Belize",
                   NameTh = "เบลีซ"
               },
               new Countries
               {
                   Id = 32,
                   NameEn = "Bermuda",
                   NameTh = "เบอร์มิวดา"
               },
               new Countries
               {
                   Id = 33,
                   NameEn = "Bolivia",
                   NameTh = "โบลิเวีย"
               },
               new Countries
               {
                   Id = 34,
                   NameEn = "Brazil",
                   NameTh = "บราซิล"
               },
               new Countries
               {
                   Id = 35,
                   NameEn = "Barbados",
                   NameTh = "บาร์เบโดส"
               },
               new Countries
               {
                   Id = 36,
                   NameEn = "Brunei",
                   NameTh = "บรูไน"
               },
               new Countries
               {
                   Id = 37,
                   NameEn = "Bhutan",
                   NameTh = "ภูฏาน"
               },
               new Countries
               {
                   Id = 38,
                   NameEn = "Bouvet Island",
                   NameTh = "เกาะบูเว็ต"
               },
               new Countries
               {
                   Id = 39,
                   NameEn = "Botswana",
                   NameTh = "บอตสวานา"
               },
               new Countries
               {
                   Id = 40,
                   NameEn = "Central African Republic",
                   NameTh = "สาธารณรัฐแอฟริกากลาง"
               },
               new Countries
               {
                   Id = 41,
                   NameEn = "Canada",
                   NameTh = "แคนาดา"
               },
               new Countries
               {
                   Id = 42,
                   NameEn = "Cocos [Keeling] Islands",
                   NameTh = "เกาะโคโคส [คีลิง],"
               },
               new Countries
               {
                   Id = 43,
                   NameEn = "Switzerland",
                   NameTh = "สวิสเซอร์แลนด์"
               },
               new Countries
               {
                   Id = 44,
                   NameEn = "Chile",
                   NameTh = "ชิลี"
               },
               new Countries
               {
                   Id = 45,
                   NameEn = "China",
                   NameTh = "จีน"
               },
               new Countries
               {
                   Id = 46,
                   NameEn = "Ivory Coast",
                   NameTh = "ไอวอรี่โคสต์"
               },
               new Countries
               {
                   Id = 47,
                   NameEn = "Cameroon",
                   NameTh = "แคเมอรูน"
               },
               new Countries
               {
                   Id = 48,
                   NameEn = "Democratic Republic of the Congo",
                   NameTh = "สาธารณรัฐประชาธิปไตยคองโก"
               },
               new Countries
               {
                   Id = 49,
                   NameEn = "Republic of the Congo",
                   NameTh = "สาธารณรัฐคองโก"
               },
               new Countries
               {
                   Id = 50,
                   NameEn = "Cook Islands",
                   NameTh = "หมู่เกาะคุก"
               },
               new Countries
               {
                   Id = 51,
                   NameEn = "Colombia",
                   NameTh = "โคลอมเบีย"
               },
               new Countries
               {
                   Id = 52,
                   NameEn = "Comoros",
                   NameTh = "คอโมโรส"
               },
               new Countries
               {
                   Id = 53,
                   NameEn = "Cape Verde",
                   NameTh = "เคปเวิร์ด"
               },
               new Countries
               {
                   Id = 54,
                   NameEn = "Costa Rica",
                   NameTh = "คอสตาริกา"
               },
               new Countries
               {
                   Id = 55,
                   NameEn = "Cuba",
                   NameTh = "คิวบา"
               },
               new Countries
               {
                   Id = 56,
                   NameEn = "Curacao",
                   NameTh = "คูราเซา"
               },
               new Countries
               {
                   Id = 57,
                   NameEn = "Christmas Island",
                   NameTh = "เกาะคริสต์มาส"
               },
               new Countries
               {
                   Id = 58,
                   NameEn = "Cayman Islands",
                   NameTh = "หมู่เกาะเคย์เเมน"
               },
               new Countries
               {
                   Id = 59,
                   NameEn = "Cyprus",
                   NameTh = "ไซปรัส"
               },
               new Countries
               {
                   Id = 60,
                   NameEn = "Czech Republic",
                   NameTh = "สาธารณรัฐเช็ก"
               },
               new Countries
               {
                   Id = 61,
                   NameEn = "Germany",
                   NameTh = "เยอรมันนี"
               },
               new Countries
               {
                   Id = 62,
                   NameEn = "Djibouti",
                   NameTh = "จิบูตี"
               },
               new Countries
               {
                   Id = 63,
                   NameEn = "Dominica",
                   NameTh = "โดมินิกา"
               },
               new Countries
               {
                   Id = 64,
                   NameEn = "Denmark",
                   NameTh = "เดนมาร์ก"
               },
               new Countries
               {
                   Id = 65,
                   NameEn = "Dominican Republic",
                   NameTh = "สาธารณรัฐโดมินิกัน"
               },
               new Countries
               {
                   Id = 66,
                   NameEn = "Algeria",
                   NameTh = "แอลจีเรีย"
               },
               new Countries
               {
                   Id = 67,
                   NameEn = "Ecuador",
                   NameTh = "เอกวาดอร์"
               },
               new Countries
               {
                   Id = 68,
                   NameEn = "Egypt",
                   NameTh = "อียิปต์"
               },
               new Countries
               {
                   Id = 69,
                   NameEn = "Eritrea",
                   NameTh = "เอริเทรี"
               },
               new Countries
               {
                   Id = 70,
                   NameEn = "Western Sahara",
                   NameTh = "ซาฮาร่าตะวันตก"
               },
               new Countries
               {
                   Id = 71,
                   NameEn = "Spain",
                   NameTh = "สเปน"
               },
               new Countries
               {
                   Id = 72,
                   NameEn = "Estonia",
                   NameTh = "เอสโตเนีย"
               },
               new Countries
               {
                   Id = 73,
                   NameEn = "Ethiopia",
                   NameTh = "สาธารณรัฐเอธิโอเปีย"
               },
               new Countries
               {
                   Id = 74,
                   NameEn = "Finland",
                   NameTh = "ฟินแลนด์"
               },
               new Countries
               {
                   Id = 75,
                   NameEn = "Fiji",
                   NameTh = "ฟิจิ"
               },
               new Countries
               {
                   Id = 76,
                   NameEn = "Falkland Islands",
                   NameTh = "หมู่เกาะฟอล์คแลนด์"
               },
               new Countries
               {
                   Id = 77,
                   NameEn = "France",
                   NameTh = "ฝรั่งเศส"
               },
               new Countries
               {
                   Id = 78,
                   NameEn = "Faroe Islands",
                   NameTh = "หมู่เกาะแฟโร"
               },
               new Countries
               {
                   Id = 79,
                   NameEn = "Micronesia",
                   NameTh = "ไมโครนีเซีย"
               },
               new Countries
               {
                   Id = 80,
                   NameEn = "Gabon",
                   NameTh = "กาบอง"
               },
               new Countries
               {
                   Id = 81,
                   NameEn = "United Kingdom",
                   NameTh = "อังกฤษ(สหราชอาณาจักร) "
               },
               new Countries
               {
                   Id = 82,
                   NameEn = "Georgia",
                   NameTh = "จอร์เจีย"
               },
               new Countries
               {
                   Id = 83,
                   NameEn = "Guernsey",
                   NameTh = "เกิร์นซีย์"
               },
               new Countries
               {
                   Id = 84,
                   NameEn = "Ghana",
                   NameTh = "กานา"
               },
               new Countries
               {
                   Id = 85,
                   NameEn = "Gibraltar",
                   NameTh = "ยิบรอลตา"
               },
               new Countries
               {
                   Id = 86,
                   NameEn = "Guinea",
                   NameTh = "กินี"
               },
               new Countries
               {
                   Id = 87,
                   NameEn = "Guadeloupe",
                   NameTh = "กัวเดลุฟ"
               },
               new Countries
               {
                   Id = 88,
                   NameEn = "Gambia",
                   NameTh = "แกมเบีย"
               },
               new Countries
               {
                   Id = 89,
                   NameEn = "Guinea-Bissau",
                   NameTh = "กินีบิสเซา"
               },
               new Countries
               {
                   Id = 90,
                   NameEn = "Equatorial Guinea",
                   NameTh = "อิเควทอเรียลกินี"
               },
               new Countries
               {
                   Id = 91,
                   NameEn = "Greece",
                   NameTh = "กรีซ"
               },
               new Countries
               {
                   Id = 92,
                   NameEn = "Grenada",
                   NameTh = "เกรเนดา"
               },
               new Countries
               {
                   Id = 93,
                   NameEn = "Greenland",
                   NameTh = "กรีนแลนด์"
               },
               new Countries
               {
                   Id = 94,
                   NameEn = "Guatemala",
                   NameTh = "กัวเตมาลา"
               },
               new Countries
               {
                   Id = 95,
                   NameEn = "French Guiana",
                   NameTh = "เฟรนช์เกียนา"
               },
               new Countries
               {
                   Id = 96,
                   NameEn = "Guam",
                   NameTh = "เกาะกวม"
               },
               new Countries
               {
                   Id = 97,
                   NameEn = "Guyana",
                   NameTh = "กายอานา"
               },
               new Countries
               {
                   Id = 98,
                   NameEn = "Hong Kong",
                   NameTh = "ฮ่องกง"
               },
               new Countries
               {
                   Id = 99,
                   NameEn = "Heard Island and McDonald Islands",
                   NameTh = "เกาะเฮิร์ดและหมู่เกาะแมคโดนัลด์"
               },
               new Countries
               {
                   Id = 100,
                   NameEn = "Honduras",
                   NameTh = "ฮอนดูรัส"
               },
               new Countries
               {
                   Id = 101,
                   NameEn = "Croatia",
                   NameTh = "โครเอเชีย"
               },
               new Countries
               {
                   Id = 102,
                   NameEn = "Haiti",
                   NameTh = "เฮติ"
               },
               new Countries
               {
                   Id = 103,
                   NameEn = "Hungary",
                   NameTh = "ฮังการี"
               },
               new Countries
               {
                   Id = 104,
                   NameEn = "Indonesia",
                   NameTh = "อินโดนีเซีย"
               },
               new Countries
               {
                   Id = 105,
                   NameEn = "Isle of Man",
                   NameTh = "เกาะแมน"
               },
               new Countries
               {
                   Id = 106,
                   NameEn = "India",
                   NameTh = "อินเดีย"
               },
               new Countries
               {
                   Id = 107,
                   NameEn = "British Indian Ocean Territory",
                   NameTh = "หมู่เกาะบริติชเวอร์จิน"
               },
               new Countries
               {
                   Id = 108,
                   NameEn = "Ireland",
                   NameTh = "ไอร์แลนด์"
               },
               new Countries
               {
                   Id = 109,
                   NameEn = "Iran",
                   NameTh = "อิหร่าน"
               },
               new Countries
               {
                   Id = 110,
                   NameEn = "Iraq",
                   NameTh = "อิรัก"
               },
               new Countries
               {
                   Id = 111,
                   NameEn = "Israel",
                   NameTh = "ไอซ์แลนด์"
               },
               new Countries
               {
                   Id = 112,
                   NameEn = "Israel",
                   NameTh = "อิสราเอล"
               },
               new Countries
               {
                   Id = 113,
                   NameEn = "Italy",
                   NameTh = "อิตาลี"
               },
               new Countries
               {
                   Id = 114,
                   NameEn = "Jamaica",
                   NameTh = "เกาะจาเมกา"
               },
               new Countries
               {
                   Id = 115,
                   NameEn = "Jersey",
                   NameTh = "นิวเจอร์ซีย์"
               },
               new Countries
               {
                   Id = 116,
                   NameEn = "Jordan",
                   NameTh = "จอร์แดน"
               },
               new Countries
               {
                   Id = 117,
                   NameEn = "Japan",
                   NameTh = "ญี่ปุ่น"
               },
               new Countries
               {
                   Id = 118,
                   NameEn = "Kazakhstan",
                   NameTh = "คาซัคสถาน"
               },
               new Countries
               {
                   Id = 119,
                   NameEn = "Kenya",
                   NameTh = "เคนย่า"
               },
               new Countries
               {
                   Id = 120,
                   NameEn = "Kyrgyzstan",
                   NameTh = "คีร์กีสถาน"
               },
               new Countries
               {
                   Id = 121,
                   NameEn = "Cambodia",
                   NameTh = "กัมพูชา"
               },
               new Countries
               {
                   Id = 122,
                   NameEn = "Kiribati",
                   NameTh = "คิริบาส"
               },
               new Countries
               {
                   Id = 123,
                   NameEn = "Saint Kitts and Nevis",
                   NameTh = "เซนต์คิตส์และเนวิส"
               },
               new Countries
               {
                   Id = 124,
                   NameEn = "South Korea",
                   NameTh = "เกาหลีใต้"
               },
               new Countries
               {
                   Id = 125,
                   NameEn = "Kuwait",
                   NameTh = "คูเวต"
               },
               new Countries
               {
                   Id = 126,
                   NameEn = "Laos",
                   NameTh = "ลาว"
               },
               new Countries
               {
                   Id = 127,
                   NameEn = "Lebanon",
                   NameTh = "เลบานอน"
               },
               new Countries
               {
                   Id = 128,
                   NameEn = "Liberia",
                   NameTh = "ไลบีเรีย"
               },
               new Countries
               {
                   Id = 129,
                   NameEn = "Libya",
                   NameTh = "ลิบยา"
               },
               new Countries
               {
                   Id = 130,
                   NameEn = "Saint Lucia",
                   NameTh = "เซนต์ลูเซีย"
               },
               new Countries
               {
                   Id = 131,
                   NameEn = "Liechtenstein",
                   NameTh = "ลิกเตนสไตน์"
               },
               new Countries
               {
                   Id = 132,
                   NameEn = "Sri Lanka",
                   NameTh = "ศรีลังกา"
               },
               new Countries
               {
                   Id = 133,
                   NameEn = "Lesotho",
                   NameTh = "เลโซโท"
               },
               new Countries
               {
                   Id = 134,
                   NameEn = "Lithuania",
                   NameTh = "ลิธัวเนีย"
               },
               new Countries
               {
                   Id = 135,
                   NameEn = "Luxembourg",
                   NameTh = "ลักเซมเบิร์ก"
               },
               new Countries
               {
                   Id = 136,
                   NameEn = "Latvia",
                   NameTh = "ลัตเวีย"
               },
               new Countries
               {
                   Id = 137,
                   NameEn = "Macao",
                   NameTh = "มาเก๊า"
               },
               new Countries
               {
                   Id = 138,
                   NameEn = "Saint Martin",
                   NameTh = "เซนต์มาร์ติน"
               },
               new Countries
               {
                   Id = 139,
                   NameEn = "Morocco",
                   NameTh = "โมร็อกโก"
               },
               new Countries
               {
                   Id = 140,
                   NameEn = "Monaco",
                   NameTh = "โมนาโก"
               },
               new Countries
               {
                   Id = 141,
                   NameEn = "Moldova",
                   NameTh = "มอลโดวา"
               },
               new Countries
               {
                   Id = 142,
                   NameEn = "Madagascar",
                   NameTh = "มาดากัสการ์"
               },
               new Countries
               {
                   Id = 143,
                   NameEn = "Maldives",
                   NameTh = "มัลดีฟส์"
               },
               new Countries
               {
                   Id = 144,
                   NameEn = "Mexico",
                   NameTh = "เม็กซิโก"
               },
               new Countries
               {
                   Id = 145,
                   NameEn = "Marshall Islands",
                   NameTh = "หมู่เกาะมาร์แชลล์"
               },
               new Countries
               {
                   Id = 146,
                   NameEn = "Macedonia",
                   NameTh = "มาซิโดเนีย"
               },
               new Countries
               {
                   Id = 147,
                   NameEn = "Mali",
                   NameTh = "มาลี"
               },
               new Countries
               {
                   Id = 148,
                   NameEn = "Malta",
                   NameTh = "เกาะมอลตา"
               },
               new Countries
               {
                   Id = 149,
                   NameEn = "Myanmar",
                   NameTh = "พม่า"
               },
               new Countries
               {
                   Id = 150,
                   NameEn = "Montenegro",
                   NameTh = "มอนเตเนโก"
               },
               new Countries
               {
                   Id = 151,
                   NameEn = "Mongolia",
                   NameTh = "มองโกเลีย"
               },
               new Countries
               {
                   Id = 152,
                   NameEn = "Northern Mariana Islands",
                   NameTh = "หมู่เกาะนอร์เทิร์นมาเรียนา"
               },
               new Countries
               {
                   Id = 153,
                   NameEn = "Mozambique",
                   NameTh = "โมซัมบิก"
               },
               new Countries
               {
                   Id = 154,
                   NameEn = "Mauritania",
                   NameTh = "มอริเตเนีย"
               },
               new Countries
               {
                   Id = 155,
                   NameEn = "Montserrat",
                   NameTh = "มอนต์เซอร์รัต"
               },
               new Countries
               {
                   Id = 156,
                   NameEn = "Martinique",
                   NameTh = "มาร์ตินีก"
               },
               new Countries
               {
                   Id = 157,
                   NameEn = "Mauritius",
                   NameTh = "มอริเชียส"
               },
               new Countries
               {
                   Id = 158,
                   NameEn = "Malawi",
                   NameTh = "มาลาวี"
               },
               new Countries
               {
                   Id = 159,
                   NameEn = "Malaysia",
                   NameTh = "มาเลเซีย"
               },
               new Countries
               {
                   Id = 160,
                   NameEn = "Mayotte",
                   NameTh = "มายอต"
               },
               new Countries
               {
                   Id = 161,
                   NameEn = "Namibia",
                   NameTh = "นามิเบีย"
               },
               new Countries
               {
                   Id = 162,
                   NameEn = "New Caledonia",
                   NameTh = "นิวแคลิโดเนีย"
               },
               new Countries
               {
                   Id = 163,
                   NameEn = "Niger",
                   NameTh = "ไนเธอร์"
               },
               new Countries
               {
                   Id = 164,
                   NameEn = "Norfolk Island",
                   NameTh = "เกาะนอร์ฟอล์ก"
               },
               new Countries
               {
                   Id = 165,
                   NameEn = "Nigeria",
                   NameTh = "ไนจีเรีย"
               },
               new Countries
               {
                   Id = 166,
                   NameEn = "Nicaragua",
                   NameTh = "นิการากัว"
               },
               new Countries
               {
                   Id = 167,
                   NameEn = "Niue",
                   NameTh = "นีอูเอ"
               },
               new Countries
               {
                   Id = 168,
                   NameEn = "Netherlands",
                   NameTh = "เนเธอร์แลนด์"
               },
               new Countries
               {
                   Id = 169,
                   NameEn = "Norway",
                   NameTh = "นอร์เวย์"
               },
               new Countries
               {
                   Id = 170,
                   NameEn = "Nepal",
                   NameTh = "เนปาล"
               },
               new Countries
               {
                   Id = 171,
                   NameEn = "Nauru",
                   NameTh = "นาอูรู"
               },
               new Countries
               {
                   Id = 172,
                   NameEn = "New Zealand",
                   NameTh = "นิวซีแลนด์"
               },
               new Countries
               {
                   Id = 173,
                   NameEn = "Oman",
                   NameTh = "โอมาน"
               },
               new Countries
               {
                   Id = 174,
                   NameEn = "Pakistan",
                   NameTh = "ปากีสถาน"
               },
               new Countries
               {
                   Id = 175,
                   NameEn = "Panama",
                   NameTh = "ปานามา"
               },
               new Countries
               {
                   Id = 176,
                   NameEn = "Pitcairn Islands",
                   NameTh = "หมู่เกาะพิตแคร์น"
               },
               new Countries
               {
                   Id = 177,
                   NameEn = "Peru",
                   NameTh = "เปรู"
               },
               new Countries
               {
                   Id = 178,
                   NameEn = "Philippines",
                   NameTh = "ฟิลิปปินส์"
               },
               new Countries
               {
                   Id = 179,
                   NameEn = "Palau",
                   NameTh = "ปาเลา"
               },
               new Countries
               {
                   Id = 180,
                   NameEn = "Papua New Guinea",
                   NameTh = "ปาปัวนิวกินี"
               },
               new Countries
               {
                   Id = 181,
                   NameEn = "Poland",
                   NameTh = "โปแลนด์"
               },
               new Countries
               {
                   Id = 182,
                   NameEn = "Puerto Rico",
                   NameTh = "เปอร์โตริโก"
               },
               new Countries
               {
                   Id = 183,
                   NameEn = "North Korea",
                   NameTh = "เกาหลีเหนือ"
               },
               new Countries
               {
                   Id = 184,
                   NameEn = "Portugal",
                   NameTh = "โปรตุเกส"
               },
               new Countries
               {
                   Id = 185,
                   NameEn = "Paraguay",
                   NameTh = "ปารากวัย"
               },
               new Countries
               {
                   Id = 186,
                   NameEn = "Palestine",
                   NameTh = "ปาเลสไตน์"
               },
               new Countries
               {
                   Id = 187,
                   NameEn = "French Polynesia",
                   NameTh = "เฟรนช์โปลินีเซีย"
               },
               new Countries
               {
                   Id = 188,
                   NameEn = "Qatar",
                   NameTh = "กาตาร์"
               },
               new Countries
               {
                   Id = 189,
                   NameEn = "Réunion",
                   NameTh = "เรอูนียง"
               },
               new Countries
               {
                   Id = 190,
                   NameEn = "Romania",
                   NameTh = "โรมาเนีย"
               },
               new Countries
               {
                   Id = 191,
                   NameEn = "Russia",
                   NameTh = "รัสเซีย"
               },
               new Countries
               {
                   Id = 192,
                   NameEn = "Rwanda",
                   NameTh = "รวันดา"
               },
               new Countries
               {
                   Id = 193,
                   NameEn = "Saudi Arabia",
                   NameTh = "ซาอุดิอาราเบีย"
               },
               new Countries
               {
                   Id = 194,
                   NameEn = "Sudan",
                   NameTh = "ซูดาน"
               },
               new Countries
               {
                   Id = 195,
                   NameEn = "Senegal",
                   NameTh = "เซเนกัล"
               },
               new Countries
               {
                   Id = 196,
                   NameEn = "Singapore",
                   NameTh = "สิงคโปร์"
               },
               new Countries
               {
                   Id = 197,
                   NameEn = "South Georgia and the South Sandwich Islands",
                   NameTh = "หมู่เกาะเซาท์จอร์เจียและหมู่เกาะเซาท์แซนด์วิช"
               },
               new Countries
               {
                   Id = 198,
                   NameEn = "Saint Helena",
                   NameTh = "เซนต์เฮเลนา"
               },
               new Countries
               {
                   Id = 199,
                   NameEn = "Svalbard and Jan Mayen",
                   NameTh = "สฟาลบาร์และยานไมเอน"
               },
               new Countries
               {
                   Id = 200,
                   NameEn = "Solomon Islands",
                   NameTh = "หมู่เกาะโซโลมอน"
               },
               new Countries
               {
                   Id = 201,
                   NameEn = "Sierra Leone",
                   NameTh = "เซียร์ราลีโอน"
               },
               new Countries
               {
                   Id = 202,
                   NameEn = "El Salvador",
                   NameTh = "เอลซัลวาดอร์"
               },
               new Countries
               {
                   Id = 203,
                   NameEn = "San Marino",
                   NameTh = "ซานมาริโน"
               },
               new Countries
               {
                   Id = 204,
                   NameEn = "Somalia",
                   NameTh = "โซมาเลีย"
               },
               new Countries
               {
                   Id = 205,
                   NameEn = "Saint Pierre and Miquelon",
                   NameTh = "เซนต์ปิแอร์และมีเกอลง"
               },
               new Countries
               {
                   Id = 206,
                   NameEn = "Serbia",
                   NameTh = "เซอร์เบีย"
               },
               new Countries
               {
                   Id = 207,
                   NameEn = "South Sudan",
                   NameTh = "ซูดานใต้"
               },
               new Countries
               {
                   Id = 208,
                   NameEn = "São Tomé and Príncipe",
                   NameTh = "เซาตูเมและปรินซิปี"
               },
               new Countries
               {
                   Id = 209,
                   NameEn = "Suriname",
                   NameTh = "ซูรินาเม"
               },
               new Countries
               {
                   Id = 210,
                   NameEn = "Slovakia",
                   NameTh = "สโลวะเกีย"
               },
               new Countries
               {
                   Id = 211,
                   NameEn = "Slovenia",
                   NameTh = "สโลวีเนีย"
               },
               new Countries
               {
                   Id = 212,
                   NameEn = "Sweden",
                   NameTh = "สวีเดน"
               },
               new Countries
               {
                   Id = 213,
                   NameEn = "Swaziland",
                   NameTh = "สวาซิแลนด์"
               },
               new Countries
               {
                   Id = 214,
                   NameEn = "Sint Maarten",
                   NameTh = "เกาะเซนต์มาร์ติน"
               },
               new Countries
               {
                   Id = 215,
                   NameEn = "Seychelles",
                   NameTh = "เซเชลส์"
               },
               new Countries
               {
                   Id = 216,
                   NameEn = "Syria",
                   NameTh = "ซีเรีย"
               },
               new Countries
               {
                   Id = 217,
                   NameEn = "Turks and Caicos Islands",
                   NameTh = "หมู่เกาะเติกส์และหมู่เกาะเคคอส"
               },
               new Countries
               {
                   Id = 218,
                   NameEn = "Chad",
                   NameTh = "ชาด"
               },
               new Countries
               {
                   Id = 219,
                   NameEn = "Togo",
                   NameTh = "โตโก"
               },
               new Countries
               {
                   Id = 220,
                   NameEn = "Thailand",
                   NameTh = "ไทย"
               },
               new Countries
               {
                   Id = 221,
                   NameEn = "Tajikistan",
                   NameTh = "ทาจิกิสถาน"
               },
               new Countries
               {
                   Id = 222,
                   NameEn = "Tokelau",
                   NameTh = "โตเกเลา"
               },
               new Countries
               {
                   Id = 223,
                   NameEn = "Turkmenistan",
                   NameTh = "เติร์กเมนิสถาน"
               },
               new Countries
               {
                   Id = 224,
                   NameEn = "East Timor",
                   NameTh = "ติมอร์ตะวันออก"
               },
               new Countries
               {
                   Id = 225,
                   NameEn = "Tonga",
                   NameTh = "ตองกา"
               },
               new Countries
               {
                   Id = 226,
                   NameEn = "Trinidad and Tobago",
                   NameTh = "ตรินิแดดและโตเบโก"
               },
               new Countries
               {
                   Id = 227,
                   NameEn = "Tunisia",
                   NameTh = "ตูนิเซีย"
               },
               new Countries
               {
                   Id = 228,
                   NameEn = "Turkey",
                   NameTh = "ตุรกี"
               },
               new Countries
               {
                   Id = 229,
                   NameEn = "Tuvalu",
                   NameTh = "ตูวาลู"
               },
               new Countries
               {
                   Id = 230,
                   NameEn = "Taiwan",
                   NameTh = "ไต้หวัน"
               },
               new Countries
               {
                   Id = 231,
                   NameEn = "Tanzania",
                   NameTh = "แทนซาเนีย"
               },
               new Countries
               {
                   Id = 232,
                   NameEn = "Uganda",
                   NameTh = "ยูกันดา"
               },
               new Countries
               {
                   Id = 233,
                   NameEn = "Ukraine",
                   NameTh = "ยูเครน"
               },
               new Countries
               {
                   Id = 234,
                   NameEn = "U.S. Minor Outlying Islands",
                   NameTh = "เกาะนอกรีตของสหรัฐฯ"
               },
               new Countries
               {
                   Id = 235,
                   NameEn = "Uruguay",
                   NameTh = "อุรุกวัย"
               },
               new Countries
               {
                   Id = 236,
                   NameEn = "United States",
                   NameTh = "สหรัฐอเมริกา"
               },
               new Countries
               {
                   Id = 237,
                   NameEn = "Uzbekistan",
                   NameTh = "อุซเบกิสถาน"
               },
               new Countries
               {
                   Id = 238,
                   NameEn = "Vatican City",
                   NameTh = "เมืองวาติกัน"
               },
               new Countries
               {
                   Id = 239,
                   NameEn = "Saint Vincent and the Grenadines",
                   NameTh = "เซนต์วินเซนต์และเกรนาดีนส์"
               },
               new Countries
               {
                   Id = 240,
                   NameEn = "Venezuela",
                   NameTh = "เวเนซุเอลา"
               },
               new Countries
               {
                   Id = 241,
                   NameEn = "British Virgin Islands",
                   NameTh = "หมู่เกาะบริติชเวอร์จิน"
               },
               new Countries
               {
                   Id = 242,
                   NameEn = "U.S. Virgin Islands",
                   NameTh = "หมู่เกาะเวอร์จินของสหรัฐอเมริกา"
               },
               new Countries
               {
                   Id = 243,
                   NameEn = "Vietnam",
                   NameTh = "เวียดนาม"
               },
               new Countries
               {
                   Id = 244,
                   NameEn = "Vanuatu",
                   NameTh = "วานูอาตู"
               },
               new Countries
               {
                   Id = 245,
                   NameEn = "Wallis and Futuna",
                   NameTh = "วาลลิสและฟุตูนา"
               },
               new Countries
               {
                   Id = 246,
                   NameEn = "Samoa",
                   NameTh = "ซามัว"
               },
               new Countries
               {
                   Id = 247,
                   NameEn = "Kosovo",
                   NameTh = "โคโซโว"
               },
               new Countries
               {
                   Id = 248,
                   NameEn = "Yemen",
                   NameTh = "เยเมน"
               },
               new Countries
               {
                   Id = 249,
                   NameEn = "South Africa",
                   NameTh = "แอฟริกาใต้"
               },
               new Countries
               {
                   Id = 250,
                   NameEn = "Zambia",
                   NameTh = "แซมเบีย"
               },
               new Countries
                {
                    Id = 251,
                    NameEn = "Zimbabwe",
                    NameTh = "ซิมบับเว"
                }
           );

            modelBuilder.Entity<AgencyType>().HasData(
               new AgencyType
               {
                   Id = 1,
                   Name = "อื่นๆ",
                   Fee = 0,
                   Order = 1
               },
               new AgencyType
               {
                   Id = 2,
                   Name = "แท็กซี่",
                   Fee= 0,
                   Order = 2    
               },
                new AgencyType
                {
                    Id = 3,
                    Name = "คนขับรถ",
                    Fee = 0,
                    Order = 3
                },
                 new AgencyType
                 {
                     Id = 4,
                     Name = "โรงแรม",
                     Fee = 0,
                     Order = 4
                 },
                  new AgencyType
                  {
                      Id = 5,
                      Name = "คณะทัวร์",
                      Fee = 0,
                      Order = 5
                  },
                new AgencyType
                {
                    Id = 6,
                    Name = "ไกด์",
                    Fee = 0,
                    Order = 6
                },
                new AgencyType
                {
                    Id = 7,
                    Name = "บริษัททัวส์,สมาคมจีน",
                    Fee = 0,
                    Order = 7
                },
                 new AgencyType
                 {
                     Id = 8,
                     Name = "สมาคมจีน",
                     Fee = 0,
                     Order = 8
                 }
           );
            modelBuilder.Entity<Agency>().HasData(
              new Agency
              {
                  Id = 1,
                  Code = "Not Specified",
                  Firstname = "ไม่ระบุ",
                  AgencyTypeId = 1,
              }
          );
        }
    }
}
