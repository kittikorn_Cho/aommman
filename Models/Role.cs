﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Models
{
    public class Role : BaseEntity
    {
        [Key]
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }


        public List<RoleMenu> RoleMenus { get; set; }   
        public List<UserRole> UserRoles { get; set; }

        
        [NotMapped]
        public List<int> MenuIds { get; set; }
        [NotMapped]
        public List<Menu> Menus { get; set; }

    }
}
