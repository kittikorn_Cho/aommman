﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Models
{
    public class AngelDebt : BaseEntity
    {
        [Key]
        public int Id { get; set; }
        public int AngelId { get; set; }
        public int DebtTypeId { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal Fee { get; set; }

        [ForeignKey("AngelId")]
        public Angel Angel { get; set; }
        [ForeignKey("DebtTypeId")]
        public DebtType DebtType { get; set; }
    }
}
