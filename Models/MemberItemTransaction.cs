﻿using System.ComponentModel.DataAnnotations.Schema;

namespace OONApi.Models
{
    public class MemberItemTransaction : BaseEntity
    {
        public int Id { get; set; }
        public int MemberId { get; set; }
        public int ItemId { get; set; }
        public int MemberTransactionId { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal Amount { get; set; }

        [ForeignKey("MemberId")]
        public Member Member { get; set; }
        [ForeignKey("MemberTransactionId")]
        public MemberTransaction MemberTransaction { get; set; }
        [ForeignKey("ItemId")]
        public Item Item { get; set; }
    }
}
