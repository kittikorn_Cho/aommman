﻿using System.ComponentModel.DataAnnotations.Schema;

namespace OONApi.Models
{
    public class ReceptionPointRedemtion : BaseEntity
    {
        public int Id { get; set; }
        public int ReceptionId { get; set; }
        public int CalendarDayId { get; set; }

        [Column(TypeName = "decimal(18, 2)")]
        public decimal Point { get; set; }

        [Column(TypeName = "decimal(18, 2)")]
        public decimal PointRedem { get; set; }
        public string Remark { get; set; }

        [ForeignKey("ReceptionId")]
        public virtual Reception Reception { get; set; }

        [ForeignKey("CalendarDayId")]
        public virtual CalendarDay CalendarDay { get; set; }
    }
}
