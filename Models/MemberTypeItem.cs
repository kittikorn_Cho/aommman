﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Models
{
    public class MemberTypeItem : BaseEntity
    {
        [Key]
        public int Id { get; set; }
        public int MemberTypeId { get; set; }
        public int ItemId { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal Amount { get; set; }

        [ForeignKey("MemberTypeId")]
        public MemberType MemberType { get; set; }
        [ForeignKey("ItemId")]
        public Item Item { get; set; }
    }
}
