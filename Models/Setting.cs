﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Models
{
    public class Setting
    {
        [Key]
        public int Id { get; set; }
        public string MassageName { get; set; }
        public string MassagePath { get; set; }
        public bool IsReportCashier { get; set; }
        public bool IsAngelPerformanceCarlendar { get; set; }
        public bool IsDailyDebt { get; set; }
        public bool IsAngelDiscountComm { get; set; }
        public bool IsQrCodeReport { get; set; }
        public bool IsCountDaily { get; set; }

        [Column(TypeName = "decimal(18, 2)")]
        public decimal PointMember { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal PointAngel { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal PointReception { get; set; }
        public bool IsPoint { get; set; }
        public string MassageCode { get; set; }
    }
}
