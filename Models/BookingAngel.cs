﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace OONApi.Models
{
    public class BookingAngel : BaseEntity
    {
        [Key]
        public int Id { get; set; }
        public int BookingId { get; set; }
        public int AngelId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public TimeSpan? StartTime { get; set; }
        public TimeSpan? EndTime { get; set; }
        public bool Flag { get; set; }

        [ForeignKey("BookingId")]
        public virtual Booking Booking { get; set; }

        [ForeignKey("AngelId")]
        public virtual Angel Angel { get; set; }
    }
}
