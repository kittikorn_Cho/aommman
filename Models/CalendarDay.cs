﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Models
{
    public class CalendarDay
    {
        [Key]
        public int Id { get; set; }
        public DateTime ToDay { get; set; }
        public bool IsDate { get; set; }
    }
}
