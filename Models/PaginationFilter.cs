﻿namespace OONApi.Models
{
    public class PaginationFilter
    {
        private int _PageNumber = 1;
        public int PageNumber
        {
            get
            {
                if (this._PageNumber == 0)
                {
                    return 1;
                }
                return this._PageNumber;
            }
            set
            {
                this._PageNumber = value;
            }
        }
        private int _PageSize = 10;
        public int PageSize
        {
            get
            {
                if (this._PageSize == 0)
                {
                    return 10;
                }
                return this._PageSize;
            }
            set
            {
                this._PageSize = value;
            }
        }
        public int TotalRecords { get; set; }
        public string _orderBy = "";
        public string OrderBy { get { return string.IsNullOrEmpty(_orderBy)?"Id":_orderBy; } set { _orderBy = value; } }
        public string _orderDerection = "";
        public string OrderDirection { get { return string.IsNullOrEmpty(_orderDerection) ? "ascending" : _orderDerection; } set { _orderDerection = value; } }
        public string Keyword { get; set; } = "";
    }
}
