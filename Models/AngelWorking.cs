﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Models
{
    public class AngelWorking : BaseEntity
    {
        [Key]
        public int Id { get; set; }
        public int CalendarDayId { get; set; }
        public int AngelId { get; set; }
        public DateTime? GetInTime { get; set; }
        public DateTime? GetOutTime { get; set; }

        [ForeignKey("CalendarDayId")]
        public virtual CalendarDay CalendarDay { get; set; }
        [ForeignKey("AngelId")]
        public virtual Angel Angel { get; set; }
    }
}
