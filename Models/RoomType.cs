﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Models
{
    public class RoomType : BaseEntity
    {
        [Key]
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal RoomRate { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal RoomRateNext { get; set; }
        public int RoundTime { get; set; }
    }
}
