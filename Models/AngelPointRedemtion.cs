﻿using System.ComponentModel.DataAnnotations.Schema;

namespace OONApi.Models
{
    public class AngelPointRedemtion : BaseEntity
    {
        public int Id { get; set; }
        public int AngelId { get; set; }
        public int CalendarDayId { get; set; }

        [Column(TypeName = "decimal(18, 2)")]
        public decimal Point { get; set; }

        [Column(TypeName = "decimal(18, 2)")]
        public decimal PointRedem { get; set; }
        public string Remark { get; set; }

        [ForeignKey("AngelId")]
        public virtual Angel Angel { get; set; }

        [ForeignKey("CalendarDayId")]
        public virtual CalendarDay CalendarDay { get; set; }
    }
}
