﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;


namespace OONApi.Models
{
    public class Booking : BaseEntity
    {
        [Key]

        public int Id { get; set; }
        public int RoomId { get; set; }
        public string DocumentNumber { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public TimeSpan? StartTime { get; set; }
        public TimeSpan? EndTime { get; set; }
        public bool Flag { get; set; }
        public string Tel { get; set; }
        public string Remark { get; set; }

        [ForeignKey("RoomId")]
        public virtual Room Room { get; set; }
    }
}
