﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Models
{
    public class AgencyPerformance:BaseEntity
    {
        [Key]
        public int Id { get; set; }
        public int CarlendarDayId { get; set; }
        public int AgenciesId { get; set; }
        public bool IsPay { get; set; }
        public bool IsUnPaid { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal TotalMassage { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal TotalRound { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal TotalMember { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal TotalReceive { get; set; }
        public string Remark { get; set; }


        [ForeignKey("CarlendarDayId")]
        public virtual CalendarDay CalendarDay { get; set; }
        [ForeignKey("AgenciesId")]
        public virtual Agency Agency { get; set; }
    }
}
