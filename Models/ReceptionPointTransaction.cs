﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OONApi.Models
{
    public class ReceptionPointTransaction : BaseEntity
    {
        [Key]
        public int Id { get; set; }
        public int MassageAngelId { get; set; }
        public int ReceptionId { get; set; }

        [Column(TypeName = "decimal(18, 2)")]
        public decimal Point { get; set; }

        [ForeignKey("MassageAngelId")]
        public virtual MassageAngel MassageAngel { get; set; }
    }
}
