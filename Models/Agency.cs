﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OONApi.Models
{
    public class Agency : BaseEntity
    {
        [Key]
        public int Id { get; set; }
        public string Code { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Nickname { get; set; }
        public string Tel { get; set; }
        public string Address { get; set; }
        public DateTime? WorkFromDate { get; set; }
        public DateTime? WorkToDate { get; set; }
        public int? Status { get; set; }
        public int AgencyTypeId { get; set; }

        [ForeignKey("AgencyTypeId")]
        public virtual AgencyType AgencyType { get; set; }
    }
}
