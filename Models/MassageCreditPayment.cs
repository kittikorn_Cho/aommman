﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Models
{
    public class MassageCreditPayment : BaseEntity
    {
        [Key]
        public int Id { get; set; }
        public int MassageId { get; set; }
        public string CardNo { get; set; }
        public int? CardType { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal CreditTotal { get; set; }

        [ForeignKey("MassageId")]
        public Massage Massage { get; set; }
    }
}
