﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Models
{
    public class MassageAngel : BaseEntity
    {
        [Key]
        public int Id { get; set; }
        public int MassageId { get; set; }
        public int RoomId { get; set; }
        public int ReceptionId { get; set; }
        public int AngelId { get; set; }
        public DateTime? CheckInTime { get; set; }
        public DateTime? CheckOutTime { get; set; }
        public DateTime? FirstCheckInTime { get; set; }
        public int TimeMinute { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal Round { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal RoundFact { get; set; }
        public DateTime? LastCallTime { get; set; }
        public int CallCount { get; set; }
        public bool IsNotCall { get; set; }
        public int? PaymentTypeId { get; set; }
        public bool IsChangeRoom { get; set; }
        public bool IsDiscountBaht { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal DiscountBaht { get; set; }
        public bool IsDiscountPercent { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal DiscountPercent { get; set; }
        public bool IsDiscountAngelRound { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal DiscountAngelRound { get; set; }
        public string DiscountRemark { get; set; }
        public bool IsDiscount { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal AddRound { get; set; }
        public bool DataOld { get; set; }
        public int? AngelPerformanceId { get; set; }


        [ForeignKey("MassageId")]
        public Massage Massage { get; set; }
        [ForeignKey("RoomId")]
        public Room Room { get; set; }
        [ForeignKey("ReceptionId")]
        public Reception Reception { get; set; }
        [ForeignKey("AngelId")]
        public Angel Angel { get; set; }
    }
}
