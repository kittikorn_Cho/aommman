﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class AddColumnCancelTabelMassageAndAnotherAddAndeDeducttableAngelPerformance : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "CancelBy",
                table: "Massages",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CancelDate",
                table: "Massages",
                type: "datetime(6)",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "AnotherAddAmount",
                table: "AngelPerformances",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<string>(
                name: "AnotherAddDescription",
                table: "AngelPerformances",
                type: "longtext",
                nullable: true)
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AddColumn<decimal>(
                name: "AnotherDeductAmount",
                table: "AngelPerformances",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<string>(
                name: "AnotherDeductDescription",
                table: "AngelPerformances",
                type: "longtext",
                nullable: true)
                .Annotation("MySql:CharSet", "utf8mb4");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CancelBy",
                table: "Massages");

            migrationBuilder.DropColumn(
                name: "CancelDate",
                table: "Massages");

            migrationBuilder.DropColumn(
                name: "AnotherAddAmount",
                table: "AngelPerformances");

            migrationBuilder.DropColumn(
                name: "AnotherAddDescription",
                table: "AngelPerformances");

            migrationBuilder.DropColumn(
                name: "AnotherDeductAmount",
                table: "AngelPerformances");

            migrationBuilder.DropColumn(
                name: "AnotherDeductDescription",
                table: "AngelPerformances");
        }
    }
}
