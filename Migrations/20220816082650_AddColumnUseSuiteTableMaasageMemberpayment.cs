﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class AddColumnUseSuiteTableMaasageMemberpayment : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "UseSuite",
                table: "MassageMemberPayments",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "/ZDWTeETJN/qIQF6xMTGwSo3ZkAFkZtWOEeJa1OAXec=", new byte[] { 167, 127, 55, 87, 241, 216, 242, 152, 187, 56, 98, 179, 80, 122, 71, 65 } });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "UseSuite",
                table: "MassageMemberPayments");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "iFAGvlBAPkw1ZLRCTPfmmkui9RTDaTwy/0+wVyxYLeY=", new byte[] { 122, 224, 241, 210, 193, 233, 225, 212, 148, 73, 93, 164, 89, 217, 1, 80 } });
        }
    }
}
