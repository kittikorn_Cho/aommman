﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class deleteSeed : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "DeductTypes",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "Jgj13evPoqM7txQrIk+57YsH/nDjjixTc6JVY/yFjVY=", new byte[] { 96, 176, 145, 59, 129, 65, 68, 8, 163, 41, 153, 140, 86, 228, 124, 141 } });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "DeductTypes",
                columns: new[] { "Id", "DeletedDate", "Fee", "Flag", "Name", "Slug" },
                values: new object[] { 1, null, 100m, null, "ค่าคอมมิทชั่น", "Comm" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "1p0uON2yzdr3ce1ynPzu8sahUrByaR4bYE3Q9nV83n0=", new byte[] { 226, 139, 171, 38, 128, 147, 10, 149, 224, 194, 25, 106, 148, 5, 219, 151 } });
        }
    }
}
