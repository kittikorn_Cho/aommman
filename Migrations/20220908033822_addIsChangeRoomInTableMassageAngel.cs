﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class addIsChangeRoomInTableMassageAngel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsChangeRoom",
                table: "MassageAngels",
                type: "tinyint(1)",
                nullable: false,
                defaultValue: false);

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "Kwl3aonv8CZ56xLrMKtNUCWi6SAWsgryg4Tp29YHbfo=", new byte[] { 70, 228, 162, 34, 80, 64, 135, 220, 137, 168, 115, 91, 22, 204, 132, 160 } });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsChangeRoom",
                table: "MassageAngels");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "MTRchUhP7oi0NoPku4LnaQCvLKWFRPqAd7KIvbpQzB8=", new byte[] { 14, 28, 249, 49, 147, 95, 252, 17, 200, 219, 209, 140, 110, 105, 156, 36 } });
        }
    }
}
