﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class OldMassageIdTableMassage : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "OldMassageId",
                table: "Massages",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "v6avvlJ1Ezl28m01XxWqxKAXn9FIEXgRRrMbW2F+2do=", new byte[] { 205, 79, 147, 145, 106, 210, 20, 37, 33, 193, 178, 218, 106, 184, 118, 137 } });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "OldMassageId",
                table: "Massages",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "dbnMK9J3iKZWWwOvinz3h+hgILlbGx8YDHzLKX8szPU=", new byte[] { 75, 120, 36, 247, 123, 107, 114, 147, 84, 34, 5, 178, 143, 87, 205, 151 } });
        }
    }
}
