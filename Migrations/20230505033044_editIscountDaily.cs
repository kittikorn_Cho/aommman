﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class editIscountDaily : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "IsCuontDaily",
                table: "Settings",
                newName: "IsCountDaily");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "oSBbg4oOlF/AbZFVjeBmjrJJyupAyjkMFDr8WWMgMAU=", new byte[] { 194, 52, 107, 75, 184, 4, 125, 49, 176, 216, 90, 201, 171, 202, 156, 206 } });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "IsCountDaily",
                table: "Settings",
                newName: "IsCuontDaily");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "R1qiaGzX8sCUikHW/4TaCzc92pUzd5DB13AkrPc8Vus=", new byte[] { 126, 141, 238, 61, 151, 76, 237, 199, 105, 218, 195, 51, 131, 41, 204, 195 } });
        }
    }
}
