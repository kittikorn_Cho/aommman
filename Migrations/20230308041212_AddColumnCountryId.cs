﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class AddColumnCountryId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.AddColumn<int>(
            //    name: "CountriesId",
            //    table: "Members",
            //    type: "int",
            //    nullable: false,
            //    defaultValue: 0);

            //migrationBuilder.AddColumn<int>(
            //    name: "CountriesId",
            //    table: "Massages",
            //    type: "int",
            //    nullable: false,
            //    defaultValue: 0);

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "mEKoJ8UWF9spiECIi06rQ+VZyTjdBqCSubOLQblOcgs=", new byte[] { 88, 129, 2, 72, 78, 7, 134, 119, 74, 216, 35, 99, 183, 97, 200, 71 } });

            //migrationBuilder.CreateIndex(
            //    name: "IX_Members_CountriesId",
            //    table: "Members",
            //    column: "CountriesId");

            //migrationBuilder.CreateIndex(
            //    name: "IX_Massages_CountriesId",
            //    table: "Massages",
            //    column: "CountriesId");

            //migrationBuilder.AddForeignKey(
            //    name: "FK_Massages_Countries_CountriesId",
            //    table: "Massages",
            //    column: "CountriesId",
            //    principalTable: "Countries",
            //    principalColumn: "Id",
            //    onDelete: ReferentialAction.Cascade);

            //migrationBuilder.AddForeignKey(
            //    name: "FK_Members_Countries_CountriesId",
            //    table: "Members",
            //    column: "CountriesId",
            //    principalTable: "Countries",
            //    principalColumn: "Id",
            //    onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.DropForeignKey(
            //    name: "FK_Massages_Countries_CountriesId",
            //    table: "Massages");

            //migrationBuilder.DropForeignKey(
            //    name: "FK_Members_Countries_CountriesId",
            //    table: "Members");

            //migrationBuilder.DropIndex(
            //    name: "IX_Members_CountriesId",
            //    table: "Members");

            //migrationBuilder.DropIndex(
            //    name: "IX_Massages_CountriesId",
            //    table: "Massages");

            //migrationBuilder.DropColumn(
            //    name: "CountriesId",
            //    table: "Members");

            //migrationBuilder.DropColumn(
            //    name: "CountriesId",
            //    table: "Massages");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "kVTQfsSIjqre7+CRnvszanguEdGdkQnxl9P3ce9qIgU=", new byte[] { 64, 5, 184, 131, 48, 39, 230, 65, 91, 126, 139, 203, 130, 158, 147, 100 } });
        }
    }
}
