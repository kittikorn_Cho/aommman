﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class addAgencies : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.DropForeignKey(
            //    name: "FK_Massages_Agency_AgenciesId",
            //    table: "Massages");

            //migrationBuilder.DropForeignKey(
            //    name: "FK_Members_Agency_AgenciesId",
            //    table: "Members");

            migrationBuilder.AddColumn<int>(
                name: "AgenciesId",
                table: "Members",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "AgenciesId",
                table: "Massages",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "t/VkyL8Ln2WYO9aWYuMDWJBMuqoRhT1cQjHMg9a/7tk=", new byte[] { 218, 15, 48, 79, 110, 1, 44, 127, 206, 80, 227, 104, 163, 87, 194, 132 } });

            //migrationBuilder.AddForeignKey(
            //    name: "FK_Massages_Agency_AgenciesId",
            //    table: "Massages",
            //    column: "AgencyId",
            //    principalTable: "Agency",
            //    principalColumn: "Id",
            //    onDelete: ReferentialAction.Restrict);

            //migrationBuilder.AddForeignKey(
            //    name: "FK_Members_Agency_AgenciesId",
            //    table: "Members",
            //    column: "AgencyId",
            //    principalTable: "Agency",
            //    principalColumn: "Id",
            //    onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.DropForeignKey(
            //    name: "FK_Massages_Agency_AgenciesId",
            //    table: "Massages");

            //migrationBuilder.DropForeignKey(
            //    name: "FK_Members_Agency_AgenciesId",
            //    table: "Members");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "dDwZACqxOliGjUYVRtDoVpJycsDrAUBC/c6+6DfI4PA=", new byte[] { 37, 70, 237, 7, 111, 105, 187, 50, 167, 76, 95, 47, 99, 82, 115, 162 } });

            //migrationBuilder.AddForeignKey(
            //    name: "FK_Massages_Agency_AgenciesId",
            //    table: "Massages",
            //    column: "AgencyId",
            //    principalTable: "Agency",
            //    principalColumn: "Id",
            //    onDelete: ReferentialAction.Cascade);

            //migrationBuilder.AddForeignKey(
            //    name: "FK_Members_Agency_AgenciesId",
            //    table: "Members",
            //    column: "AgencyId",
            //    principalTable: "Agency",
            //    principalColumn: "Id",
            //    onDelete: ReferentialAction.Cascade);
        }
    }
}
