﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class addNassageMemberAndRoomTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "massageMemberAndRooms",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    MemberId = table.Column<int>(type: "int", nullable: false),
                    MassageRoomId = table.Column<int>(type: "int", nullable: false),
                    MassageMemberPaymentId = table.Column<int>(type: "int", nullable: false),
                    CreatedBy = table.Column<int>(type: "int", nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    UpdatedBy = table.Column<int>(type: "int", nullable: true),
                    UpdatedDate = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    DeletedBy = table.Column<int>(type: "int", nullable: true),
                    DeletedDate = table.Column<DateTime>(type: "datetime(6)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_massageMemberAndRooms", x => x.Id);
                    table.ForeignKey(
                        name: "FK_massageMemberAndRooms_MassageMemberPayments_MassageMemberPay~",
                        column: x => x.MassageMemberPaymentId,
                        principalTable: "MassageMemberPayments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_massageMemberAndRooms_MassageRooms_MassageRoomId",
                        column: x => x.MassageRoomId,
                        principalTable: "MassageRooms",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_massageMemberAndRooms_Members_MemberId",
                        column: x => x.MemberId,
                        principalTable: "Members",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "rQEsW36OzEQfJBCy1BywMin0RLUfjnvsbvR7fELQLcE=", new byte[] { 88, 227, 59, 38, 81, 198, 202, 139, 80, 131, 112, 74, 11, 83, 116, 81 } });

            migrationBuilder.CreateIndex(
                name: "IX_massageMemberAndRooms_MassageMemberPaymentId",
                table: "massageMemberAndRooms",
                column: "MassageMemberPaymentId");

            migrationBuilder.CreateIndex(
                name: "IX_massageMemberAndRooms_MassageRoomId",
                table: "massageMemberAndRooms",
                column: "MassageRoomId");

            migrationBuilder.CreateIndex(
                name: "IX_massageMemberAndRooms_MemberId",
                table: "massageMemberAndRooms",
                column: "MemberId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "massageMemberAndRooms");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "I0kXVEgv5KJ0f/OMC8svrCguSTjS6OGE6JAMdS9pZao=", new byte[] { 131, 242, 228, 174, 129, 75, 36, 14, 123, 1, 142, 254, 250, 52, 53, 94 } });
        }
    }
}
