﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class cashierAndDataOldTableMassage : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "CashierId",
                table: "Massages",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "DataOld",
                table: "MassageRooms",
                type: "tinyint(1)",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "DataOld",
                table: "MassageAngels",
                type: "tinyint(1)",
                nullable: false,
                defaultValue: false);

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "bbqX/s9O9VKn/swl6IbSeXD8VFdk9oC2+Z7i1zHbjE0=", new byte[] { 173, 22, 37, 38, 95, 56, 60, 153, 37, 120, 21, 36, 102, 248, 148, 134 } });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CashierId",
                table: "Massages");

            migrationBuilder.DropColumn(
                name: "DataOld",
                table: "MassageRooms");

            migrationBuilder.DropColumn(
                name: "DataOld",
                table: "MassageAngels");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "KiQYXQo6HUuHRpEaEx2143NiDsGdoyYLzQX+gMGtFsQ=", new byte[] { 117, 60, 19, 3, 49, 30, 190, 120, 236, 143, 132, 146, 242, 136, 212, 223 } });
        }
    }
}
