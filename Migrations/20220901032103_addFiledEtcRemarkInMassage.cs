﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class addFiledEtcRemarkInMassage : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "EtcRemark",
                table: "Massages",
                type: "longtext",
                nullable: true)
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "x/OwDWswKufXTspXfHw0CLbx341AXQxmkEBnhDXKEJc=", new byte[] { 253, 169, 140, 171, 180, 32, 80, 82, 90, 217, 73, 200, 216, 30, 213, 208 } });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EtcRemark",
                table: "Massages");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "7KyUsBYgRxY83LfUVSmHEnSCl+7WxSLPmO4KVRQaCH4=", new byte[] { 71, 8, 52, 175, 135, 188, 10, 168, 145, 107, 180, 130, 38, 182, 50, 98 } });
        }
    }
}
