﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class AddAgencyTransaction : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.DropForeignKey(
            //    name: "FK_Massages_Agency_AgencyId",
            //    table: "Massages");

            //migrationBuilder.DropForeignKey(
            //    name: "FK_Members_Agency_AgencyId",
            //    table: "Members");

            //migrationBuilder.DropIndex(
            //    name: "IX_Members_AgencyId",
            //    table: "Members");

            //migrationBuilder.DropIndex(
            //    name: "IX_Massages_AgencyId",
            //    table: "Massages");

            //migrationBuilder.DropColumn(
            //    name: "AgencyId",
            //    table: "Members");

            //migrationBuilder.DropColumn(
            //    name: "AgencyId",
            //    table: "Massages");

            //migrationBuilder.AddColumn<DateTime>(
            //    name: "DeletedDate",
            //    table: "Countries",
            //    type: "datetime(6)",
            //    nullable: true);

            migrationBuilder.CreateTable(
                name: "AgencyTransaction",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    MassageId = table.Column<int>(type: "int", nullable: false),
                    AgenciesId = table.Column<int>(type: "int", nullable: false),
                    Amount = table.Column<decimal>(type: "decimal(65,30)", nullable: false),
                    CreatedBy = table.Column<int>(type: "int", nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    UpdatedBy = table.Column<int>(type: "int", nullable: true),
                    UpdatedDate = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    DeletedBy = table.Column<int>(type: "int", nullable: true),
                    DeletedDate = table.Column<DateTime>(type: "datetime(6)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AgencyTransaction", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AgencyTransaction_Agency_AgenciesId",
                        column: x => x.AgenciesId,
                        principalTable: "Agency",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AgencyTransaction_Massages_MassageId",
                        column: x => x.MassageId,
                        principalTable: "Massages",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "rHQfX4zvlxkyFWKx0og+h0MoP+tL+Qkdm6vQhoOe4Dg=", new byte[] { 183, 59, 5, 59, 77, 165, 152, 47, 232, 111, 183, 11, 197, 130, 247, 195 } });

            //migrationBuilder.CreateIndex(
            //    name: "IX_Members_AgenciesId",
            //    table: "Members",
            //    column: "AgenciesId");

            //migrationBuilder.CreateIndex(
            //    name: "IX_Massages_AgenciesId",
            //    table: "Massages",
            //    column: "AgenciesId");

            //migrationBuilder.CreateIndex(
            //    name: "IX_AgencyTransaction_AgenciesId",
            //    table: "AgencyTransaction",
            //    column: "AgenciesId");

            //migrationBuilder.CreateIndex(
            //    name: "IX_AgencyTransaction_MassageId",
            //    table: "AgencyTransaction",
            //    column: "MassageId");

            //migrationBuilder.AddForeignKey(
            //    name: "FK_Massages_Agency_AgenciesId",
            //    table: "Massages",
            //    column: "AgenciesId",
            //    principalTable: "Agency",
            //    principalColumn: "Id",
            //    onDelete: ReferentialAction.Cascade);

            //migrationBuilder.AddForeignKey(
            //    name: "FK_Members_Agency_AgenciesId",
            //    table: "Members",
            //    column: "AgenciesId",
            //    principalTable: "Agency",
            //    principalColumn: "Id",
            //    onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.DropForeignKey(
            //    name: "FK_Massages_Agency_AgenciesId",
            //    table: "Massages");

            //migrationBuilder.DropForeignKey(
            //    name: "FK_Members_Agency_AgenciesId",
            //    table: "Members");

            //migrationBuilder.DropTable(
            //    name: "AgencyTransaction");

            //migrationBuilder.DropIndex(
            //    name: "IX_Members_AgenciesId",
            //    table: "Members");

            //migrationBuilder.DropIndex(
            //    name: "IX_Massages_AgenciesId",
            //    table: "Massages");

            //migrationBuilder.DropColumn(
            //    name: "DeletedDate",
            //    table: "Countries");

            //migrationBuilder.AddColumn<int>(
            //    name: "AgenciesId",
            //    table: "Members",
            //    type: "int",
            //    nullable: true);

            //migrationBuilder.AddColumn<int>(
            //    name: "AgenciesId",
            //    table: "Massages",
            //    type: "int",
            //    nullable: true);

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "t/VkyL8Ln2WYO9aWYuMDWJBMuqoRhT1cQjHMg9a/7tk=", new byte[] { 218, 15, 48, 79, 110, 1, 44, 127, 206, 80, 227, 104, 163, 87, 194, 132 } });

            //migrationBuilder.CreateIndex(
            //    name: "IX_Members_AgenciesId",
            //    table: "Members",
            //    column: "AgencyId");

            //migrationBuilder.CreateIndex(
            //    name: "IX_Massages_AgenciesId",
            //    table: "Massages",
            //    column: "AgencyId");

            //migrationBuilder.AddForeignKey(
            //    name: "FK_Massages_Agency_AgenciesId",
            //    table: "Massages",
            //    column: "AgencyId",
            //    principalTable: "Agency",
            //    principalColumn: "Id",
            //    onDelete: ReferentialAction.Restrict);

            //migrationBuilder.AddForeignKey(
            //    name: "FK_Members_Agency_AgenciesId",
            //    table: "Members",
            //    column: "AgencyId",
            //    principalTable: "Agency",
            //    principalColumn: "Id",
            //    onDelete: ReferentialAction.Restrict);
        }
    }
}
