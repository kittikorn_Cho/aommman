﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class seedMenuWagePayment : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Menus",
                columns: new[] { "Id", "Code", "CreatedBy", "CreatedDate", "DeletedBy", "DeletedDate", "Name", "Slug", "UpdatedBy", "UpdatedDate" },
                values: new object[] { 20, "WagePayment", null, null, null, null, "รอการชำระเงิน", "", null, null });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "I+FAqmaY5kxOhAoXi90IFoaPzBffxw/vdpbz8GlnB00=", new byte[] { 87, 9, 100, 231, 23, 218, 36, 97, 249, 107, 218, 3, 20, 5, 103, 23 } });

            migrationBuilder.InsertData(
                table: "RoleMenu",
                columns: new[] { "MenuId", "RoleId" },
                values: new object[] { 20, 1 });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "RoleMenu",
                keyColumns: new[] { "MenuId", "RoleId" },
                keyValues: new object[] { 20, 1 });

            migrationBuilder.DeleteData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "aX+O9FPnDUC+GwoPuvGpv4NN/xp1fkcJI9Z5m4sb5kA=", new byte[] { 27, 97, 228, 197, 41, 187, 201, 38, 50, 48, 218, 173, 2, 0, 215, 116 } });
        }
    }
}
