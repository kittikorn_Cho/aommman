﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class addmenu : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Menus",
                columns: new[] { "Id", "Code", "CreatedBy", "CreatedDate", "DeletedBy", "DeletedDate", "Name", "Slug", "UpdatedBy", "UpdatedDate" },
                values: new object[] { 15, "CheckIn", null, null, null, null, "เช็คอิน", "", null, null });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "vqN7UMkMHWuQgIHFuUviJ0gbxpkWsAeJ/rccD7w1FC4=", new byte[] { 238, 130, 136, 80, 152, 88, 184, 54, 162, 93, 1, 214, 193, 137, 129, 238 } });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "LkR246tYN7pP5k5CRlYcKvy4jb6cO6T1hdRBPkMAbco=", new byte[] { 92, 61, 95, 233, 7, 247, 177, 28, 201, 119, 157, 33, 118, 227, 170, 13 } });
        }
    }
}
