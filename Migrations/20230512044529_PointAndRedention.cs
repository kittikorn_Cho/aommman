﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class PointAndRedention : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "PointAngel",
                table: "Settings",
                type: "decimal(65,30)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "PointMember",
                table: "Settings",
                type: "decimal(65,30)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "PointReception",
                table: "Settings",
                type: "decimal(65,30)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "Point",
                table: "Receptions",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "Point",
                table: "MemberTransactions",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "Point",
                table: "Members",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "Point",
                table: "Angels",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.CreateTable(
                name: "AngelPointRedemtions",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    AngelId = table.Column<int>(type: "int", nullable: false),
                    CalendarDayId = table.Column<int>(type: "int", nullable: false),
                    Point = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    PointRedem = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    Remark = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    CreatedBy = table.Column<int>(type: "int", nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    UpdatedBy = table.Column<int>(type: "int", nullable: true),
                    UpdatedDate = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    DeletedBy = table.Column<int>(type: "int", nullable: true),
                    DeletedDate = table.Column<DateTime>(type: "datetime(6)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AngelPointRedemtions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AngelPointRedemtions_Angels_AngelId",
                        column: x => x.AngelId,
                        principalTable: "Angels",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AngelPointRedemtions_CalendarDays_CalendarDayId",
                        column: x => x.CalendarDayId,
                        principalTable: "CalendarDays",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "AngelPointTransactions",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    MassageAngelId = table.Column<int>(type: "int", nullable: false),
                    AngelId = table.Column<int>(type: "int", nullable: false),
                    Point = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    CreatedBy = table.Column<int>(type: "int", nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    UpdatedBy = table.Column<int>(type: "int", nullable: true),
                    UpdatedDate = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    DeletedBy = table.Column<int>(type: "int", nullable: true),
                    DeletedDate = table.Column<DateTime>(type: "datetime(6)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AngelPointTransactions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AngelPointTransactions_MassageAngels_MassageAngelId",
                        column: x => x.MassageAngelId,
                        principalTable: "MassageAngels",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "MemberPointRedemtions",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    MemberId = table.Column<int>(type: "int", nullable: false),
                    CalendarDayId = table.Column<int>(type: "int", nullable: false),
                    Point = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    PointRedem = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    Remark = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    CreatedBy = table.Column<int>(type: "int", nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    UpdatedBy = table.Column<int>(type: "int", nullable: true),
                    UpdatedDate = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    DeletedBy = table.Column<int>(type: "int", nullable: true),
                    DeletedDate = table.Column<DateTime>(type: "datetime(6)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MemberPointRedemtions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MemberPointRedemtions_CalendarDays_CalendarDayId",
                        column: x => x.CalendarDayId,
                        principalTable: "CalendarDays",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MemberPointRedemtions_Members_MemberId",
                        column: x => x.MemberId,
                        principalTable: "Members",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "ReceptionPointRedemtions",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    ReceptionId = table.Column<int>(type: "int", nullable: false),
                    CalendarDayId = table.Column<int>(type: "int", nullable: false),
                    Point = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    PointRedem = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    Remark = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    CreatedBy = table.Column<int>(type: "int", nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    UpdatedBy = table.Column<int>(type: "int", nullable: true),
                    UpdatedDate = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    DeletedBy = table.Column<int>(type: "int", nullable: true),
                    DeletedDate = table.Column<DateTime>(type: "datetime(6)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ReceptionPointRedemtions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ReceptionPointRedemtions_CalendarDays_CalendarDayId",
                        column: x => x.CalendarDayId,
                        principalTable: "CalendarDays",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ReceptionPointRedemtions_Receptions_ReceptionId",
                        column: x => x.ReceptionId,
                        principalTable: "Receptions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "ReceptionPointTransactions",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    MassageAngelId = table.Column<int>(type: "int", nullable: false),
                    ReceptionId = table.Column<int>(type: "int", nullable: false),
                    Point = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    CreatedBy = table.Column<int>(type: "int", nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    UpdatedBy = table.Column<int>(type: "int", nullable: true),
                    UpdatedDate = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    DeletedBy = table.Column<int>(type: "int", nullable: true),
                    DeletedDate = table.Column<DateTime>(type: "datetime(6)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ReceptionPointTransactions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ReceptionPointTransactions_MassageAngels_MassageAngelId",
                        column: x => x.MassageAngelId,
                        principalTable: "MassageAngels",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "0sCHScO7lhXrYcQ9r076uR6+Cy4sKgqMqzq7cogolwg=", new byte[] { 149, 195, 114, 14, 171, 156, 201, 168, 63, 167, 58, 176, 44, 34, 10, 67 } });

            migrationBuilder.CreateIndex(
                name: "IX_AngelPointRedemtions_AngelId",
                table: "AngelPointRedemtions",
                column: "AngelId");

            migrationBuilder.CreateIndex(
                name: "IX_AngelPointRedemtions_CalendarDayId",
                table: "AngelPointRedemtions",
                column: "CalendarDayId");

            migrationBuilder.CreateIndex(
                name: "IX_AngelPointTransactions_MassageAngelId",
                table: "AngelPointTransactions",
                column: "MassageAngelId");

            migrationBuilder.CreateIndex(
                name: "IX_MemberPointRedemtions_CalendarDayId",
                table: "MemberPointRedemtions",
                column: "CalendarDayId");

            migrationBuilder.CreateIndex(
                name: "IX_MemberPointRedemtions_MemberId",
                table: "MemberPointRedemtions",
                column: "MemberId");

            migrationBuilder.CreateIndex(
                name: "IX_ReceptionPointRedemtions_CalendarDayId",
                table: "ReceptionPointRedemtions",
                column: "CalendarDayId");

            migrationBuilder.CreateIndex(
                name: "IX_ReceptionPointRedemtions_ReceptionId",
                table: "ReceptionPointRedemtions",
                column: "ReceptionId");

            migrationBuilder.CreateIndex(
                name: "IX_ReceptionPointTransactions_MassageAngelId",
                table: "ReceptionPointTransactions",
                column: "MassageAngelId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AngelPointRedemtions");

            migrationBuilder.DropTable(
                name: "AngelPointTransactions");

            migrationBuilder.DropTable(
                name: "MemberPointRedemtions");

            migrationBuilder.DropTable(
                name: "ReceptionPointRedemtions");

            migrationBuilder.DropTable(
                name: "ReceptionPointTransactions");

            migrationBuilder.DropColumn(
                name: "PointAngel",
                table: "Settings");

            migrationBuilder.DropColumn(
                name: "PointMember",
                table: "Settings");

            migrationBuilder.DropColumn(
                name: "PointReception",
                table: "Settings");

            migrationBuilder.DropColumn(
                name: "Point",
                table: "Receptions");

            migrationBuilder.DropColumn(
                name: "Point",
                table: "MemberTransactions");

            migrationBuilder.DropColumn(
                name: "Point",
                table: "Members");

            migrationBuilder.DropColumn(
                name: "Point",
                table: "Angels");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "COg3rH7m/hXc4ek4xGsLocSVDzIQcwNxSD3AgLyZCBw=", new byte[] { 105, 65, 216, 197, 54, 169, 75, 111, 217, 4, 209, 200, 52, 10, 115, 229 } });
        }
    }
}
