﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class addFieldisCheckInLater : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "isCheckInLater",
                table: "Massages",
                type: "tinyint(1)",
                nullable: false,
                defaultValue: false);

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "Q7BNWFyahrh69FpEd0LdrWtOFzO9L6VVecCLUrVHuoM=", new byte[] { 16, 211, 44, 108, 253, 188, 99, 209, 122, 59, 74, 56, 40, 244, 165, 195 } });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "isCheckInLater",
                table: "Massages");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "zRKI2zfmDyRRbKUsxvgGvQ2YbQhRh9HTx9Xh0CbOBzs=", new byte[] { 130, 251, 243, 1, 126, 54, 227, 226, 81, 32, 8, 55, 204, 46, 26, 86 } });
        }
    }
}
