﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class addfeildRoomWageinMassageMemberAndRoom : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "RoomWage",
                table: "massageMemberAndRooms",
                type: "decimal(65,30)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "9y88A9DTbrOj1ZlkMcfQxpYHi51DTMWLJBdH2MQ/N2o=", new byte[] { 105, 119, 110, 0, 255, 157, 21, 242, 16, 47, 120, 128, 66, 36, 96, 226 } });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "RoomWage",
                table: "massageMemberAndRooms");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "lL0Et7FPwSKU+6YCc7Le40V/Ei238Wx5NjNvNkod9D8=", new byte[] { 212, 197, 118, 153, 10, 158, 140, 141, 215, 27, 151, 236, 63, 167, 61, 220 } });
        }
    }
}
