﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class addTableMassageEntertain : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "IsEntertain",
                table: "Massages",
                newName: "IsEntertainFood");

            migrationBuilder.RenameColumn(
                name: "EntertainTotal",
                table: "Massages",
                newName: "EntertainTotalFood");

            migrationBuilder.RenameColumn(
                name: "EntertainRemark",
                table: "Massages",
                newName: "EntertainRemarkFood");

            migrationBuilder.AddColumn<string>(
                name: "EntertainRemarkAngel",
                table: "Massages",
                type: "longtext",
                nullable: true)
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AddColumn<decimal>(
                name: "EntertainTotalAngel",
                table: "Massages",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<bool>(
                name: "IsEntertainAngel",
                table: "Massages",
                type: "tinyint(1)",
                nullable: false,
                defaultValue: false);

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "VivhsGf6ac259Pok0wQMxcCCz5Vnk/eCAJnDOH05xj4=", new byte[] { 225, 18, 207, 198, 40, 193, 238, 195, 66, 89, 175, 86, 162, 162, 35, 191 } });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EntertainRemarkAngel",
                table: "Massages");

            migrationBuilder.DropColumn(
                name: "EntertainTotalAngel",
                table: "Massages");

            migrationBuilder.DropColumn(
                name: "IsEntertainAngel",
                table: "Massages");

            migrationBuilder.RenameColumn(
                name: "IsEntertainFood",
                table: "Massages",
                newName: "IsEntertain");

            migrationBuilder.RenameColumn(
                name: "EntertainTotalFood",
                table: "Massages",
                newName: "EntertainTotal");

            migrationBuilder.RenameColumn(
                name: "EntertainRemarkFood",
                table: "Massages",
                newName: "EntertainRemark");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "mEzWevWINWtLZrVODhwfg/LSHQjBPf3qX26TYwTDDzg=", new byte[] { 46, 165, 157, 46, 105, 149, 159, 213, 26, 62, 56, 216, 155, 159, 157, 237 } });
        }
    }
}
