﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class editMasterDataMenus : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 59,
                columns: new[] { "Code", "Name" },
                values: new object[] { "Booking", "จองห้อง" });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 60,
                columns: new[] { "Code", "Name" },
                values: new object[] { "Point", "สะสมคะแนน" });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 61,
                columns: new[] { "Code", "Name" },
                values: new object[] { "PointMember", "พ้อยท์เมมเบอร์ --> จัดการพ้อยท์เมมเบอร์" });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 62,
                columns: new[] { "Code", "Name" },
                values: new object[] { "PointAngel", "พ้อยท์พนักงานบริการ --> จัดการพ้อยท์พนักงานบริการ" });

            migrationBuilder.InsertData(
                table: "Menus",
                columns: new[] { "Id", "Code", "CreatedBy", "CreatedDate", "DeletedBy", "DeletedDate", "Name", "Slug", "UpdatedBy", "UpdatedDate" },
                values: new object[] { 63, "PointReception", null, null, null, null, "พ้อยท์พนักงานต้อนรับ --> จัดการพ้อยท์พนักงานต้อนรับ", "", null, null });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "uldE4mjyYmeRT3DSwwQjdW+rAxr9ugO9X9VF9HN+mEs=", new byte[] { 153, 175, 157, 174, 144, 76, 182, 161, 250, 230, 24, 179, 149, 246, 152, 198 } });

            migrationBuilder.InsertData(
                table: "RoleMenu",
                columns: new[] { "MenuId", "RoleId" },
                values: new object[] { 63, 1 });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "RoleMenu",
                keyColumns: new[] { "MenuId", "RoleId" },
                keyValues: new object[] { 63, 1 });

            migrationBuilder.DeleteData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 63);

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 59,
                columns: new[] { "Code", "Name" },
                values: new object[] { "Point", "สะสมคะแนน" });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 60,
                columns: new[] { "Code", "Name" },
                values: new object[] { "PointMember", "พ้อยท์เมมเบอร์ --> จัดการพ้อยท์เมมเบอร์" });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 61,
                columns: new[] { "Code", "Name" },
                values: new object[] { "PointAngel", "พ้อยท์พนักงานบริการ --> จัดการพ้อยท์พนักงานบริการ" });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 62,
                columns: new[] { "Code", "Name" },
                values: new object[] { "PointReception", "พ้อยท์พนักงานต้อนรับ --> จัดการพ้อยท์พนักงานต้อนรับ" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "qCg2K03ugJXYgek4SuPO0JOCdL0aOH8KGdOO/TPIDD0=", new byte[] { 230, 81, 158, 52, 176, 245, 146, 240, 128, 54, 145, 63, 244, 251, 124, 4 } });
        }
    }
}
