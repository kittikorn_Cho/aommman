﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class addisDailyDebtinSetting : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsDailyDebt",
                table: "Settings",
                type: "tinyint(1)",
                nullable: false,
                defaultValue: false);

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "YqU6V9jVYy1G+aBGYoTPevU7eviEncLDUnl3PzuxUKk=", new byte[] { 184, 245, 76, 43, 10, 90, 162, 105, 104, 34, 141, 129, 189, 18, 92, 164 } });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsDailyDebt",
                table: "Settings");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "kbtd5UdzdjnraQNa8aLwr4EsJsokrEf0AT0OXOn4Dzs=", new byte[] { 67, 55, 42, 246, 15, 117, 119, 144, 20, 171, 203, 95, 98, 135, 120, 236 } });
        }
    }
}
