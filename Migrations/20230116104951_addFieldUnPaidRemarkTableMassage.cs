﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class addFieldUnPaidRemarkTableMassage : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "UnPaidRemark",
                table: "Massages",
                type: "longtext",
                nullable: true)
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "flT2uLb2PawBTzAv+gXYvIx0jocrpgumMYhuyJUgQrU=", new byte[] { 58, 119, 43, 29, 115, 95, 96, 254, 83, 65, 173, 70, 155, 16, 113, 175 } });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "UnPaidRemark",
                table: "Massages");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "07OzPx4323VtxZDUKBVW6QgiCOKSG9e56xfARhKFZpQ=", new byte[] { 59, 197, 250, 253, 227, 98, 71, 2, 149, 238, 19, 95, 232, 225, 113, 115 } });
        }
    }
}
