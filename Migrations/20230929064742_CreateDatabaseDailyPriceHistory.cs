﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class CreateDatabaseDailyPriceHistory : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "DailyPriceHistorys",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    CalendarDayId = table.Column<int>(type: "int", nullable: false),
                    AngelTypeId = table.Column<int>(type: "int", nullable: false),
                    CommId1 = table.Column<int>(type: "int", nullable: false),
                    CommId2 = table.Column<int>(type: "int", nullable: false),
                    DailydebtId1 = table.Column<int>(type: "int", nullable: false),
                    DailydebtId2 = table.Column<int>(type: "int", nullable: false),
                    Round = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    Price = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    Wage = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    Comm1 = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    Comm2 = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    Dailydebt1 = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    Dailydebt2 = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    CarlendarDayId = table.Column<int>(type: "int", nullable: true),
                    CreatedBy = table.Column<int>(type: "int", nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    UpdatedBy = table.Column<int>(type: "int", nullable: true),
                    UpdatedDate = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    DeletedBy = table.Column<int>(type: "int", nullable: true),
                    DeletedDate = table.Column<DateTime>(type: "datetime(6)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DailyPriceHistorys", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DailyPriceHistorys_AngelTypes_AngelTypeId",
                        column: x => x.AngelTypeId,
                        principalTable: "AngelTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DailyPriceHistorys_CalendarDays_CarlendarDayId",
                        column: x => x.CarlendarDayId,
                        principalTable: "CalendarDays",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "4gS2kkyXyPhaMDX8JHFqbMnuO1RK2uoP7fgOiH0WzdQ=", new byte[] { 227, 55, 124, 174, 48, 48, 162, 126, 200, 207, 156, 132, 120, 224, 23, 252 } });

            migrationBuilder.CreateIndex(
                name: "IX_DailyPriceHistorys_AngelTypeId",
                table: "DailyPriceHistorys",
                column: "AngelTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_DailyPriceHistorys_CarlendarDayId",
                table: "DailyPriceHistorys",
                column: "CarlendarDayId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DailyPriceHistorys");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "NOt1y4lriytkSXbo2m93DD/bojZ8uWJ00IOvZCZbYh0=", new byte[] { 197, 149, 243, 210, 104, 240, 92, 24, 47, 3, 111, 224, 58, 254, 124, 197 } });
        }
    }
}
