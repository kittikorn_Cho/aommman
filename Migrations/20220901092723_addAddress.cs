﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class addAddress : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Address",
                table: "Receptions",
                type: "longtext",
                nullable: true)
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "NYR2wam/MnKWquL+Fme5IrFxwLWJaqDLmvYWNwawKWU=", new byte[] { 42, 192, 126, 255, 139, 121, 157, 237, 230, 36, 59, 48, 164, 88, 243, 174 } });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Address",
                table: "Receptions");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "x/OwDWswKufXTspXfHw0CLbx341AXQxmkEBnhDXKEJc=", new byte[] { 253, 169, 140, 171, 180, 32, 80, 82, 90, 217, 73, 200, 216, 30, 213, 208 } });
        }
    }
}
