﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class RemoveTableAgencyTransaction : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AgencyTransaction");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "Rmtl0mDeBNptJWgMjJT6It1ld5kSOFSJ5AQUWUN2VuQ=", new byte[] { 30, 115, 21, 133, 201, 153, 195, 17, 43, 227, 80, 21, 212, 237, 156, 108 } });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AgencyTransaction",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    AgenciesId = table.Column<int>(type: "int", nullable: false),
                    Amount = table.Column<decimal>(type: "decimal(65,30)", nullable: false),
                    CreatedBy = table.Column<int>(type: "int", nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    DeletedBy = table.Column<int>(type: "int", nullable: true),
                    DeletedDate = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    MassageId = table.Column<int>(type: "int", nullable: false),
                    UpdatedBy = table.Column<int>(type: "int", nullable: true),
                    UpdatedDate = table.Column<DateTime>(type: "datetime(6)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AgencyTransaction", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AgencyTransaction_Agency_AgenciesId",
                        column: x => x.AgenciesId,
                        principalTable: "Agency",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AgencyTransaction_Massages_MassageId",
                        column: x => x.MassageId,
                        principalTable: "Massages",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "rHQfX4zvlxkyFWKx0og+h0MoP+tL+Qkdm6vQhoOe4Dg=", new byte[] { 183, 59, 5, 59, 77, 165, 152, 47, 232, 111, 183, 11, 197, 130, 247, 195 } });

            migrationBuilder.CreateIndex(
                name: "IX_AgencyTransaction_AgenciesId",
                table: "AgencyTransaction",
                column: "AgenciesId");

            migrationBuilder.CreateIndex(
                name: "IX_AgencyTransaction_MassageId",
                table: "AgencyTransaction",
                column: "MassageId");
        }
    }
}
