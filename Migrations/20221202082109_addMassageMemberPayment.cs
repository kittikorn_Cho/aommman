﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class addMassageMemberPayment : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "MemberFoodTotal",
                table: "MassageMemberPayments",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "MemberMassageTotal",
                table: "MassageMemberPayments",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "MemberRoomTotal",
                table: "MassageMemberPayments",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "L0qleGLpaHgbP5Fvllr4ud7u6XWrn1SIZ70RaIrPLis=", new byte[] { 186, 143, 56, 234, 106, 44, 199, 100, 100, 137, 238, 158, 1, 175, 69, 221 } });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "MemberFoodTotal",
                table: "MassageMemberPayments");

            migrationBuilder.DropColumn(
                name: "MemberMassageTotal",
                table: "MassageMemberPayments");

            migrationBuilder.DropColumn(
                name: "MemberRoomTotal",
                table: "MassageMemberPayments");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "+w8RwL6zcYXuDpVmOvhEZ28eBKSgw8uDg+UoOe1dvVs=", new byte[] { 50, 174, 126, 212, 243, 199, 106, 194, 18, 209, 112, 27, 238, 218, 9, 137 } });
        }
    }
}
