﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class AddColumnTotalDailyDebt : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "TotalDailyDebt",
                table: "AngelPerformances",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "M5kFyJ2qWKI+lwm1YFq15HDk9vpd76NqCxEQt9GiSL0=", new byte[] { 174, 29, 204, 224, 105, 134, 175, 28, 181, 210, 82, 192, 191, 56, 148, 73 } });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TotalDailyDebt",
                table: "AngelPerformances");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "FTQIcypwgcF425mbBsP5sDdXOJWgFU+FcTauHCQCrXA=", new byte[] { 104, 130, 27, 97, 80, 187, 13, 136, 94, 232, 76, 21, 67, 123, 29, 242 } });
        }
    }
}
