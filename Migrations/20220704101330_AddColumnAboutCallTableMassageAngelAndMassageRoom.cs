﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class AddColumnAboutCallTableMassageAngelAndMassageRoom : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "RoundFect",
                table: "MassageAngels",
                newName: "RoundFact");

            migrationBuilder.AddColumn<bool>(
                name: "IsNotCall",
                table: "MassageRooms",
                type: "tinyint(1)",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "CallCount",
                table: "MassageAngels",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<bool>(
                name: "IsNotCall",
                table: "MassageAngels",
                type: "tinyint(1)",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastCallTime",
                table: "MassageAngels",
                type: "datetime(6)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsNotCall",
                table: "MassageRooms");

            migrationBuilder.DropColumn(
                name: "CallCount",
                table: "MassageAngels");

            migrationBuilder.DropColumn(
                name: "IsNotCall",
                table: "MassageAngels");

            migrationBuilder.DropColumn(
                name: "LastCallTime",
                table: "MassageAngels");

            migrationBuilder.RenameColumn(
                name: "RoundFact",
                table: "MassageAngels",
                newName: "RoundFect");
        }
    }
}
