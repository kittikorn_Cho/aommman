﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class AddColumnAtMemberPayments : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
           
            migrationBuilder.AddColumn<decimal>(
                name: "CashTotal",
                table: "memberPayments",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "CreditTotal",
                table: "memberPayments",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<bool>(
                name: "IsCash",
                table: "memberPayments",
                type: "tinyint(1)",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsCredit",
                table: "memberPayments",
                type: "tinyint(1)",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsPay",
                table: "memberPayments",
                type: "tinyint(1)",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsQrCode",
                table: "memberPayments",
                type: "tinyint(1)",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsUnPaid",
                table: "memberPayments",
                type: "tinyint(1)",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "PayDate",
                table: "memberPayments",
                type: "datetime(6)",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "QrCodeTotal",
                table: "memberPayments",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "Total",
                table: "memberPayments",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "UnpaidTotal",
                table: "memberPayments",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "pTGpSEQVHbZ1UySHxjn3vNsipC1HhYWbifmbes2Qa1U=", new byte[] { 108, 6, 147, 240, 35, 95, 148, 221, 68, 114, 214, 213, 16, 92, 15, 104 } });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CashTotal",
                table: "memberPayments");

            migrationBuilder.DropColumn(
                name: "CreditTotal",
                table: "memberPayments");

            migrationBuilder.DropColumn(
                name: "IsCash",
                table: "memberPayments");

            migrationBuilder.DropColumn(
                name: "IsCredit",
                table: "memberPayments");

            migrationBuilder.DropColumn(
                name: "IsPay",
                table: "memberPayments");

            migrationBuilder.DropColumn(
                name: "IsQrCode",
                table: "memberPayments");

            migrationBuilder.DropColumn(
                name: "IsUnPaid",
                table: "memberPayments");

            migrationBuilder.DropColumn(
                name: "PayDate",
                table: "memberPayments");

            migrationBuilder.DropColumn(
                name: "QrCodeTotal",
                table: "memberPayments");

            migrationBuilder.DropColumn(
                name: "Total",
                table: "memberPayments");

            migrationBuilder.DropColumn(
                name: "UnpaidTotal",
                table: "memberPayments");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "36cjU4AzJUq7JTQxiuhLZxNlXmx/nOU1SZQkwab+vY4=", new byte[] { 69, 42, 233, 150, 168, 138, 6, 63, 155, 170, 32, 147, 70, 42, 44, 68 } });
        }
    }
}
