﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class addSeedderSlugTableMenu : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Code", "Slug" },
                values: new object[] { "Dashboard", "View Dashboard" });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "Code", "Slug" },
                values: new object[] { "Angel", "View Angels" });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "Code", "Slug" },
                values: new object[] { "Angel", "View AngelTypes" });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "Code", "Slug" },
                values: new object[] { "Angel", "View AngelDebts" });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "Code", "Slug" },
                values: new object[] { "Angel", "View AngelDeducts" });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "Code", "Slug" },
                values: new object[] { "Angel", "View AngelDoctor" });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "Code", "Slug" },
                values: new object[] { "Member", "View Members" });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "Code", "Slug" },
                values: new object[] { "Member", "View MemberTypes" });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "Code", "Slug" },
                values: new object[] { "Member", "View MemberItems" });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "Code", "Slug" },
                values: new object[] { "Member", "View MemberPayments" });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "Code", "Slug" },
                values: new object[] { "Receptions", "View Receptions" });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "Code", "Slug" },
                values: new object[] { "Room", "View Rooms" });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "Code", "Slug" },
                values: new object[] { "Role", "View Roles" });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "Code", "Slug" },
                values: new object[] { "User", "View Users" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "tOioWeBzyfD5QVsqv6BHcjRf3XSXYuKb+S/LIc9dvbQ=", new byte[] { 32, 70, 172, 191, 123, 56, 50, 254, 206, 69, 31, 191, 61, 6, 62, 211 } });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Code", "Slug" },
                values: new object[] { "", "" });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "Code", "Slug" },
                values: new object[] { "", "" });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "Code", "Slug" },
                values: new object[] { "", "" });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "Code", "Slug" },
                values: new object[] { "", "" });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "Code", "Slug" },
                values: new object[] { "", "" });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "Code", "Slug" },
                values: new object[] { "", "" });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "Code", "Slug" },
                values: new object[] { "", "" });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "Code", "Slug" },
                values: new object[] { "", "" });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "Code", "Slug" },
                values: new object[] { "", "" });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "Code", "Slug" },
                values: new object[] { "", "" });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "Code", "Slug" },
                values: new object[] { "", "" });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "Code", "Slug" },
                values: new object[] { "", "" });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "Code", "Slug" },
                values: new object[] { "", "" });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "Code", "Slug" },
                values: new object[] { "", "" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "VIyY2Wm0uCIKCvqphrAME6KnOuac6NgcTZlWZXjQgqo=", new byte[] { 5, 47, 58, 32, 1, 114, 30, 19, 155, 92, 225, 217, 115, 156, 203, 225 } });
        }
    }
}
