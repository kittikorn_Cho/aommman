﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class AddColumnDiscountTableMassageAngeAndMassageRoom : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "DiscountBaht",
                table: "MassageRooms",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "DiscountPercent",
                table: "MassageRooms",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<string>(
                name: "DiscountRemark",
                table: "MassageRooms",
                type: "longtext",
                nullable: true)
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AddColumn<decimal>(
                name: "DiscountRound",
                table: "MassageRooms",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<bool>(
                name: "IsDiscountBaht",
                table: "MassageRooms",
                type: "tinyint(1)",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsDiscountPercent",
                table: "MassageRooms",
                type: "tinyint(1)",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsDiscountRound",
                table: "MassageRooms",
                type: "tinyint(1)",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<decimal>(
                name: "DiscountBaht",
                table: "MassageAngels",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "DiscountPercent",
                table: "MassageAngels",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<string>(
                name: "DiscountRemark",
                table: "MassageAngels",
                type: "longtext",
                nullable: true)
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AddColumn<decimal>(
                name: "DiscountRound",
                table: "MassageAngels",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<bool>(
                name: "IsDiscountBaht",
                table: "MassageAngels",
                type: "tinyint(1)",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsDiscountPercent",
                table: "MassageAngels",
                type: "tinyint(1)",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsDiscountRound",
                table: "MassageAngels",
                type: "tinyint(1)",
                nullable: false,
                defaultValue: false);

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "2E7c3GdiyAKDJeIeF8PZlCQcOq2aXh+5oTU3Pc5RcL4=", new byte[] { 127, 233, 97, 173, 2, 10, 124, 164, 100, 158, 99, 191, 54, 44, 92, 214 } });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DiscountBaht",
                table: "MassageRooms");

            migrationBuilder.DropColumn(
                name: "DiscountPercent",
                table: "MassageRooms");

            migrationBuilder.DropColumn(
                name: "DiscountRemark",
                table: "MassageRooms");

            migrationBuilder.DropColumn(
                name: "DiscountRound",
                table: "MassageRooms");

            migrationBuilder.DropColumn(
                name: "IsDiscountBaht",
                table: "MassageRooms");

            migrationBuilder.DropColumn(
                name: "IsDiscountPercent",
                table: "MassageRooms");

            migrationBuilder.DropColumn(
                name: "IsDiscountRound",
                table: "MassageRooms");

            migrationBuilder.DropColumn(
                name: "DiscountBaht",
                table: "MassageAngels");

            migrationBuilder.DropColumn(
                name: "DiscountPercent",
                table: "MassageAngels");

            migrationBuilder.DropColumn(
                name: "DiscountRemark",
                table: "MassageAngels");

            migrationBuilder.DropColumn(
                name: "DiscountRound",
                table: "MassageAngels");

            migrationBuilder.DropColumn(
                name: "IsDiscountBaht",
                table: "MassageAngels");

            migrationBuilder.DropColumn(
                name: "IsDiscountPercent",
                table: "MassageAngels");

            migrationBuilder.DropColumn(
                name: "IsDiscountRound",
                table: "MassageAngels");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "PAnVZ9EQPxUMTcjaprgK8bOpTAO95PQsmaTOJq4c8ts=", new byte[] { 149, 138, 246, 58, 59, 77, 78, 15, 172, 93, 237, 179, 205, 52, 70, 248 } });
        }
    }
}
