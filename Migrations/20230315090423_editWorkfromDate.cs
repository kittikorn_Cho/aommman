﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class editWorkfromDate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "WorkFronData",
                table: "Agency",
                newName: "WorkFromDate");;

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "fOl6atVB9M/s2dLjRNhByX1t6fgH0SES3T47cF5lbwQ=", new byte[] { 141, 232, 23, 157, 8, 215, 67, 71, 235, 155, 48, 63, 109, 16, 41, 75 } });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "WorkFromDate",
                table: "Agency",
                newName: "WorkFronData");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "6xtqxU+vJQ7LhfjKFCP0nxQueGT4+AKNU7B6jmxKHdk=", new byte[] { 104, 95, 32, 147, 179, 14, 251, 9, 116, 98, 24, 225, 108, 1, 55, 226 } });
        }
    }
}
