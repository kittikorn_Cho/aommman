﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class addFiledTipCommTotal : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "TipCommTotal",
                table: "Massages",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "7KyUsBYgRxY83LfUVSmHEnSCl+7WxSLPmO4KVRQaCH4=", new byte[] { 71, 8, 52, 175, 135, 188, 10, 168, 145, 107, 180, 130, 38, 182, 50, 98 } });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TipCommTotal",
                table: "Massages");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "mpwD9kY83rUjQ50f338BF3ana5EeXGJNx4L0hsgZkhs=", new byte[] { 240, 241, 24, 244, 161, 123, 63, 239, 123, 93, 203, 149, 180, 241, 74, 201 } });
        }
    }
}
