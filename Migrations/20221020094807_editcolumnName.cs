﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class editcolumnName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "IsDiscountRound",
                table: "MassageAngels",
                newName: "IsDiscountAngelRound");

            migrationBuilder.RenameColumn(
                name: "DiscountRound",
                table: "MassageAngels",
                newName: "DiscountAngelRound");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "F+6eN3kcpyAkBN7FnGca+hTgcJNucGdxggVLHgxLppg=", new byte[] { 106, 95, 57, 251, 235, 226, 153, 22, 41, 223, 141, 53, 166, 30, 185, 93 } });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "IsDiscountAngelRound",
                table: "MassageAngels",
                newName: "IsDiscountRound");

            migrationBuilder.RenameColumn(
                name: "DiscountAngelRound",
                table: "MassageAngels",
                newName: "DiscountRound");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "13AHmuRDDh1rqqtHK/Mi+rIknFPVa3ZHGAa9vw6zY80=", new byte[] { 171, 144, 125, 154, 215, 16, 234, 1, 13, 8, 171, 42, 177, 145, 148, 241 } });
        }
    }
}
