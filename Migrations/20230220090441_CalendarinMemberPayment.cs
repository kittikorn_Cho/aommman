﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class CalendarinMemberPayment : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "/stVlpd73qkAKidec39R4eVnteTuT8MBAScfsAKdgTI=", new byte[] { 102, 243, 172, 176, 237, 211, 158, 19, 205, 193, 3, 70, 240, 134, 33, 102 } });

            migrationBuilder.CreateIndex(
                name: "IX_memberPayments_CalendarId",
                table: "memberPayments",
                column: "CalendarId");

            migrationBuilder.AddForeignKey(
                name: "FK_memberPayments_CalendarDays_CalendarId",
                table: "memberPayments",
                column: "CalendarId",
                principalTable: "CalendarDays",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_memberPayments_CalendarDays_CalendarId",
                table: "memberPayments");

            migrationBuilder.DropIndex(
                name: "IX_memberPayments_CalendarId",
                table: "memberPayments");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "jdj4vdusB/LEiZyosb/TUxeCDm9vzL8ObYilr1hanvo=", new byte[] { 128, 91, 131, 145, 75, 41, 183, 146, 61, 136, 60, 39, 221, 69, 138, 206 } });
        }
    }
}
