﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class RelationAngelDebtAngel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "AngelId1",
                table: "AngelDebts",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_AngelDebts_AngelId1",
                table: "AngelDebts",
                column: "AngelId1");

            migrationBuilder.AddForeignKey(
                name: "FK_AngelDebts_Angels_AngelId1",
                table: "AngelDebts",
                column: "AngelId1",
                principalTable: "Angels",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AngelDebts_Angels_AngelId1",
                table: "AngelDebts");

            migrationBuilder.DropIndex(
                name: "IX_AngelDebts_AngelId1",
                table: "AngelDebts");

            migrationBuilder.DropColumn(
                name: "AngelId1",
                table: "AngelDebts");
        }
    }
}
