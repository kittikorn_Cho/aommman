﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class addfeildRoundinMassageMemberAndRoom : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "Round",
                table: "massageMemberAndRooms",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "lL0Et7FPwSKU+6YCc7Le40V/Ei238Wx5NjNvNkod9D8=", new byte[] { 212, 197, 118, 153, 10, 158, 140, 141, 215, 27, 151, 236, 63, 167, 61, 220 } });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Round",
                table: "massageMemberAndRooms");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "rQEsW36OzEQfJBCy1BywMin0RLUfjnvsbvR7fELQLcE=", new byte[] { 88, 227, 59, 38, 81, 198, 202, 139, 80, 131, 112, 74, 11, 83, 116, 81 } });
        }
    }
}
