﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class AddColumnRoundFectTableMassageAngelAndAddColumnTotalRoundTableAngelPerformance : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "RoundFect",
                table: "MassageAngels",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "TotalRound",
                table: "AngelPerformances",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "RoundFect",
                table: "MassageAngels");

            migrationBuilder.DropColumn(
                name: "TotalRound",
                table: "AngelPerformances");
        }
    }
}
