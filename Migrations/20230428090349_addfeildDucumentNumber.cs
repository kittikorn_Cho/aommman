﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class addfeildDucumentNumber : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "DocumentNumber",
                table: "Bookings",
                type: "longtext",
                nullable: true)
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "j10XaFlxYRnmpR8gTA240qsHhtU0xNal3690EWkf15Y=", new byte[] { 197, 69, 131, 60, 80, 184, 64, 105, 2, 228, 230, 217, 78, 32, 86, 21 } });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DocumentNumber",
                table: "Bookings");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "taKvo0Z5u84sXm2j59sxzsmRikf7PbR0+Y5W5CJK7gg=", new byte[] { 216, 24, 162, 34, 205, 82, 101, 187, 229, 83, 56, 36, 189, 172, 18, 250 } });
        }
    }
}
