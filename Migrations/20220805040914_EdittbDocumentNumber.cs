﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class EdittbDocumentNumber : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DocumentNumbers_CalendarDays_CarlendarDayId",
                table: "DocumentNumbers");

            migrationBuilder.DropIndex(
                name: "IX_DocumentNumbers_CarlendarDayId",
                table: "DocumentNumbers");

            migrationBuilder.DropColumn(
                name: "CarlendarDayId",
                table: "DocumentNumbers");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "nRKGuTXPDMmsuZy7DyZXhQovgrgNq9yLBZORygSu5qg=", new byte[] { 139, 147, 59, 54, 126, 168, 242, 97, 176, 87, 84, 60, 93, 230, 117, 80 } });

            migrationBuilder.CreateIndex(
                name: "IX_DocumentNumbers_CalendarDayId",
                table: "DocumentNumbers",
                column: "CalendarDayId");

            migrationBuilder.AddForeignKey(
                name: "FK_DocumentNumbers_CalendarDays_CalendarDayId",
                table: "DocumentNumbers",
                column: "CalendarDayId",
                principalTable: "CalendarDays",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DocumentNumbers_CalendarDays_CalendarDayId",
                table: "DocumentNumbers");

            migrationBuilder.DropIndex(
                name: "IX_DocumentNumbers_CalendarDayId",
                table: "DocumentNumbers");

            migrationBuilder.AddColumn<int>(
                name: "CarlendarDayId",
                table: "DocumentNumbers",
                type: "int",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "sAkg3uTCmmPVo6F6jh9cJvCKsKd8WjAfVn7+FXzlTEU=", new byte[] { 250, 86, 198, 169, 190, 31, 54, 41, 165, 203, 192, 185, 78, 246, 220, 243 } });

            migrationBuilder.CreateIndex(
                name: "IX_DocumentNumbers_CarlendarDayId",
                table: "DocumentNumbers",
                column: "CarlendarDayId");

            migrationBuilder.AddForeignKey(
                name: "FK_DocumentNumbers_CalendarDays_CarlendarDayId",
                table: "DocumentNumbers",
                column: "CarlendarDayId",
                principalTable: "CalendarDays",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
