﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class addAngelUnique : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Rfid",
                table: "Angels",
                type: "varchar(255)",
                nullable: true)
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "5edXSep3G3fnzI4fOi1bYh7E7pGRhgd7yRcipo6Mg2M=", new byte[] { 195, 71, 210, 161, 172, 83, 109, 122, 127, 8, 107, 212, 115, 246, 36, 240 } });

            migrationBuilder.CreateIndex(
                name: "IX_Angels_Rfid",
                table: "Angels",
                column: "Rfid",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Angels_Rfid",
                table: "Angels");

            migrationBuilder.DropColumn(
                name: "Rfid",
                table: "Angels");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "kMFgBnpy6iMtjmq0fJ6WlMs1DrgwMUiYkWBNhZbv3lQ=", new byte[] { 140, 162, 252, 73, 142, 90, 111, 134, 236, 158, 43, 86, 175, 227, 82, 234 } });
        }
    }
}
