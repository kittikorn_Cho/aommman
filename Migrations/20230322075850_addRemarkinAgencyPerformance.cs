﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class addRemarkinAgencyPerformance : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Remark",
                table: "AgencyPerformances",
                type: "longtext",
                nullable: true)
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "I0kXVEgv5KJ0f/OMC8svrCguSTjS6OGE6JAMdS9pZao=", new byte[] { 131, 242, 228, 174, 129, 75, 36, 14, 123, 1, 142, 254, 250, 52, 53, 94 } });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Remark",
                table: "AgencyPerformances");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "TLSQTddZ+fhiFxqjq57a7H1xcSzliAx4llqoYqyZ4c8=", new byte[] { 226, 64, 212, 138, 112, 83, 31, 224, 5, 150, 11, 157, 187, 229, 123, 214 } });
        }
    }
}
