﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class addRoundTableMassageAngel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "AddRound",
                table: "MassageAngels",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "gEGMD8ceMIf8pzKypCPZOQP0T0f2JIlYPgyFoayEhEI=", new byte[] { 165, 48, 16, 160, 94, 123, 102, 255, 165, 163, 4, 83, 109, 204, 138, 128 } });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AddRound",
                table: "MassageAngels");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "2aOo9Yg5GlVb2yBuj+u3REsR1KjOrsxESBUXlyqEIo4=", new byte[] { 92, 26, 75, 139, 246, 117, 200, 79, 27, 84, 57, 19, 154, 117, 158, 123 } });
        }
    }
}
