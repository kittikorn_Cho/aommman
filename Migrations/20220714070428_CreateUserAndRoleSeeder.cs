﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class CreateUserAndRoleSeeder : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Roles",
                columns: new[] { "Id", "Code", "CreatedBy", "CreatedDate", "DeletedBy", "DeletedDate", "Name", "UpdatedBy", "UpdatedDate" },
                values: new object[,]
                {
                    { 1, "ADMIN", null, null, null, null, "แอดมิน", null, null },
                    { 2, "CASHIER", null, null, null, null, "แคชเชียร์", null, null },
                    { 3, "RECEPT", null, null, null, null, "เชียร์แขก", null, null },
                    { 4, "HR", null, null, null, null, "ฝ่ายบุคคล", null, null },
                    { 5, "ACC", null, null, null, null, "บัญชี", null, null }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "CreatedBy", "CreatedDate", "DeletedBy", "DeletedDate", "Password", "PasswordSalt", "Status", "Tel", "UpdatedBy", "UpdatedDate", "UserFullname", "Username" },
                values: new object[] { 1, null, null, null, null, "QrHQRDA2XgdxIW74pW5TqfLjjQkUOu+RLTohEnZxaO4=", new byte[] { 37, 216, 169, 24, 144, 63, 218, 240, 83, 7, 64, 229, 178, 12, 166, 189 }, 1, null, null, null, "Administrator", "admin" });

            migrationBuilder.InsertData(
                table: "UserRoles",
                columns: new[] { "Id", "RoleId", "UserId" },
                values: new object[] { 1, 1, 1 });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Roles",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Roles",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Roles",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Roles",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "UserRoles",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Roles",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1);
        }
    }
}
