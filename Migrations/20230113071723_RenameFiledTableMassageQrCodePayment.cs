﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class RenameFiledTableMassageQrCodePayment : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "CreditTotal",
                table: "MassageQrCodePayments",
                newName: "QrCodeTotal");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "07OzPx4323VtxZDUKBVW6QgiCOKSG9e56xfARhKFZpQ=", new byte[] { 59, 197, 250, 253, 227, 98, 71, 2, 149, 238, 19, 95, 232, 225, 113, 115 } });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "QrCodeTotal",
                table: "MassageQrCodePayments",
                newName: "CreditTotal");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "NL2oMRLPZZ8Lye5LlT17mEIIA1a9OtzYep4R+mPfw58=", new byte[] { 164, 81, 2, 159, 50, 34, 241, 233, 240, 70, 232, 92, 247, 237, 43, 135 } });
        }
    }
}
