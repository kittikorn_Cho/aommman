﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class addSluginMassageAngel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Slug",
                table: "MassageAngels",
                type: "longtext",
                nullable: true)
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "HrOY3/VLFyR8ztZaYC0MohY6FUdgooqwwk3vgrTbyoU=", new byte[] { 52, 225, 92, 58, 228, 82, 44, 230, 249, 182, 124, 246, 184, 110, 144, 46 } });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Slug",
                table: "MassageAngels");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "/ZDWTeETJN/qIQF6xMTGwSo3ZkAFkZtWOEeJa1OAXec=", new byte[] { 167, 127, 55, 87, 241, 216, 242, 152, 187, 56, 98, 179, 80, 122, 71, 65 } });
        }
    }
}
