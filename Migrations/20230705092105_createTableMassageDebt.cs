﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class createTableMassageDebt : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "MassageDebts",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    MassageId = table.Column<int>(type: "int", nullable: false),
                    DebtTotal = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    CashTotal = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    CreditTotal = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    QrTotal = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    MemberTotal = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    CreatedBy = table.Column<int>(type: "int", nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    UpdatedBy = table.Column<int>(type: "int", nullable: true),
                    UpdatedDate = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    DeletedBy = table.Column<int>(type: "int", nullable: true),
                    DeletedDate = table.Column<DateTime>(type: "datetime(6)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MassageDebts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MassageDebts_Massages_MassageId",
                        column: x => x.MassageId,
                        principalTable: "Massages",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "gHXiPUD66I+MzusL4C8WypzeIU6uZJwE5csJfYdZWpQ=", new byte[] { 227, 13, 2, 39, 37, 225, 62, 198, 164, 184, 5, 203, 145, 103, 162, 233 } });

            migrationBuilder.CreateIndex(
                name: "IX_MassageDebts_MassageId",
                table: "MassageDebts",
                column: "MassageId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MassageDebts");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "M6Dy62LDOwPmXLYO+UwWnBz22VTUsPsfBL7iuo6+/Fw=", new byte[] { 76, 87, 201, 208, 24, 48, 193, 155, 180, 32, 232, 162, 152, 220, 138, 60 } });
        }
    }
}
