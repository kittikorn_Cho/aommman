﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class addIsReportCashierinSetting : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsReportCashier",
                table: "Settings",
                type: "tinyint(1)",
                nullable: false,
                defaultValue: false);

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "cQBqhkKZqhkGKIg8fY4+YBiRGDP9W7FaJ0xj+/NvMR0=", new byte[] { 38, 2, 238, 131, 82, 142, 215, 114, 6, 155, 159, 122, 75, 195, 1, 139 } });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsReportCashier",
                table: "Settings");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "D6ROAgJNB0BnLU4wbM0gM80dfZjngIOvz+tzsOpagRI=", new byte[] { 37, 217, 70, 158, 63, 221, 126, 64, 109, 58, 246, 169, 108, 190, 19, 35 } });
        }
    }
}
