﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class updateSeederMenu : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 2,
                column: "Name",
                value: "พนักงานบริการ --> จัดการข้อมูลพนักงานบริการ");

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 3,
                column: "Name",
                value: "พนักงานบริการ --> ประเภทพนักงานบริการ");

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 4,
                column: "Name",
                value: "พนักงานบริการ -->  ประเภทหนี้คงที่พนักงานบริการ");

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 5,
                column: "Name",
                value: "พนักงานบริการ -->  ประเภทนี้หักตามรอบพนักงานบริการ");

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 6,
                column: "Name",
                value: "พนักงานบริการ --> ตรวจสุขภาพ");

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 7,
                column: "Name",
                value: "เมมเบอร์ --> จัดการข้อมูลเมมเบอร์");

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 8,
                column: "Name",
                value: "เมมเบอร์ --> ประเภทเมมเบอร์");

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 9,
                column: "Name",
                value: "เมมเบอร์ --> ของแถมเมมเบอร์");

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 10,
                column: "Name",
                value: "เมมเบอร์ --> จัดการข้อมูลชำระเงินเมมเบอร์");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "lBjFvKA5PYr0Tk4ChTgy+EMj+mnd4tovWLmodjDkGZw=", new byte[] { 36, 30, 131, 130, 140, 154, 94, 134, 182, 149, 212, 77, 139, 47, 209, 8 } });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 2,
                column: "Name",
                value: "จัดการข้อมูลพนักงานบริการ");

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 3,
                column: "Name",
                value: "ประเภทพนักงานบริการ");

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 4,
                column: "Name",
                value: "ประเภทหนี้คงที่พนักงานบริการ");

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 5,
                column: "Name",
                value: "ประเภทนี้หักตามรอบพนักงานบริการ");

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 6,
                column: "Name",
                value: "ตรวจสุขภาพพนักงานบริการ");

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 7,
                column: "Name",
                value: "จัดการข้อมูลเมมเบอร์");

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 8,
                column: "Name",
                value: "ประเภทเมมเบอร์");

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 9,
                column: "Name",
                value: "ของแถมเมมเบอร์");

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 10,
                column: "Name",
                value: "จัดการข้อมูลชำระเงินเมมเบอร์");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "o3wNFBTR+6SjC/Ih+pqF+3CG7sAPc9z6YG69bYe5M+U=", new byte[] { 94, 136, 116, 6, 235, 223, 131, 18, 168, 198, 148, 152, 65, 138, 237, 72 } });
        }
    }
}
