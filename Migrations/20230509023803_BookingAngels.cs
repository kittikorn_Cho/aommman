﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class BookingAngels : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "BookingAngels",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    BookingId = table.Column<int>(type: "int", nullable: false),
                    AngelId = table.Column<int>(type: "int", nullable: false),
                    StartDate = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    StartTime = table.Column<TimeSpan>(type: "time(6)", nullable: true),
                    EndTime = table.Column<TimeSpan>(type: "time(6)", nullable: true),
                    Flag = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    DeletedDate = table.Column<DateTime>(type: "datetime(6)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BookingAngels", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BookingAngels_Angels_AngelId",
                        column: x => x.AngelId,
                        principalTable: "Angels",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BookingAngels_Bookings_BookingId",
                        column: x => x.BookingId,
                        principalTable: "Bookings",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "JPDGj2sAST2nz0uFo8KmIhaoG+jt1pd0OY0rRxfan6g=", new byte[] { 208, 49, 157, 198, 42, 244, 20, 253, 154, 79, 23, 38, 210, 15, 133, 85 } });

            migrationBuilder.CreateIndex(
                name: "IX_BookingAngels_AngelId",
                table: "BookingAngels",
                column: "AngelId");

            migrationBuilder.CreateIndex(
                name: "IX_BookingAngels_BookingId",
                table: "BookingAngels",
                column: "BookingId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BookingAngels");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "oSBbg4oOlF/AbZFVjeBmjrJJyupAyjkMFDr8WWMgMAU=", new byte[] { 194, 52, 107, 75, 184, 4, 125, 49, 176, 216, 90, 201, 171, 202, 156, 206 } });
        }
    }
}
