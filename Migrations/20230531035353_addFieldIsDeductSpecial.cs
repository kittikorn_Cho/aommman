﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class addFieldIsDeductSpecial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsDeductSpecial",
                table: "AngelTypes",
                type: "tinyint(1)",
                nullable: false,
                defaultValue: false);

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "afazpP6xltO9S4iV/qDbNg4BG+/uMya3y/yC8w4rTeg=", new byte[] { 103, 211, 177, 217, 69, 246, 27, 116, 93, 105, 207, 63, 206, 43, 55, 220 } });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsDeductSpecial",
                table: "AngelTypes");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "SNYh3QvurwNH8OloolEf6jujlyLboqPGuj1pKqjJvKk=", new byte[] { 213, 10, 43, 215, 104, 22, 28, 175, 117, 97, 225, 151, 40, 43, 95, 16 } });
        }
    }
}
