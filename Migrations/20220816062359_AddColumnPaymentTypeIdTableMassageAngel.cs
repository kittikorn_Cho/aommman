﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class AddColumnPaymentTypeIdTableMassageAngel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "PaymentTypeId",
                table: "MassageAngels",
                type: "int",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "ltXKnDWlaj/IRMeLB7uUNhAMoxT9Y4R/dJ/euEzC6Wk=", new byte[] { 230, 211, 252, 167, 192, 222, 67, 136, 200, 236, 222, 178, 201, 130, 64, 207 } });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PaymentTypeId",
                table: "MassageAngels");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "UYfIUOWXjIJ6GlPS99brwcHXQpvHACw2ot3F8abgtpM=", new byte[] { 218, 163, 21, 98, 171, 126, 68, 222, 113, 125, 184, 3, 173, 179, 65, 242 } });
        }
    }
}
