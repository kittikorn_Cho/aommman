﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class addseedBuildingType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "BuildingTypes",
                columns: new[] { "Id", "CreatedBy", "CreatedDate", "DeletedBy", "DeletedDate", "Name", "UpdatedBy", "UpdatedDate" },
                values: new object[] { 1, null, null, null, null, "ตึกหลัก", null, null });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "q4KaqcVMh6CqJR/axo0Qqy7XqoFRGAhb3UBG9+SMoqI=", new byte[] { 5, 100, 56, 211, 233, 19, 108, 88, 171, 223, 253, 3, 57, 23, 190, 53 } });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "BuildingTypes",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "SKw6AdBrhdLgcvDcssLaNHerxkeop/DspEj33IwjyEI=", new byte[] { 244, 255, 38, 250, 140, 72, 204, 231, 186, 116, 175, 24, 23, 142, 206, 33 } });
        }
    }
}
