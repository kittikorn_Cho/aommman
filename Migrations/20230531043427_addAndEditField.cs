﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class addAndEditField : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "IsDeductSpecial",
                table: "AngelTypes",
                newName: "IsSpecialComm");

            migrationBuilder.AddColumn<bool>(
                name: "IsSpecialClean",
                table: "AngelTypes",
                type: "tinyint(1)",
                nullable: false,
                defaultValue: false);

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "ZQX5CRhAZ9rNm+1/qirgx5wlPprOlzX5XAKC3SVhJiI=", new byte[] { 193, 26, 207, 221, 18, 1, 187, 110, 219, 204, 31, 1, 37, 107, 224, 198 } });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsSpecialClean",
                table: "AngelTypes");

            migrationBuilder.RenameColumn(
                name: "IsSpecialComm",
                table: "AngelTypes",
                newName: "IsDeductSpecial");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "afazpP6xltO9S4iV/qDbNg4BG+/uMya3y/yC8w4rTeg=", new byte[] { 103, 211, 177, 217, 69, 246, 27, 116, 93, 105, 207, 63, 206, 43, 55, 220 } });
        }
    }
}
