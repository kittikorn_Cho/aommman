﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class AddNewTableMassageQrCodePaymentsAndAddFieldTableMemberTransaction : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsQrCode",
                table: "MemberTransactions",
                type: "tinyint(1)",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<decimal>(
                name: "QrCodeTotal",
                table: "MemberTransactions",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<bool>(
                name: "IsQrCodeAngel",
                table: "Massages",
                type: "tinyint(1)",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsQrCodeEtc",
                table: "Massages",
                type: "tinyint(1)",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsQrCodeFood",
                table: "Massages",
                type: "tinyint(1)",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsQrCodeRoom",
                table: "Massages",
                type: "tinyint(1)",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<decimal>(
                name: "QrCodeAngelTotal",
                table: "Massages",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "QrCodeEtcTotal",
                table: "Massages",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "QrCodeFoodTotal",
                table: "Massages",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "QrCodeRoomTotal",
                table: "Massages",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "QrCodeTotal",
                table: "Massages",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.CreateTable(
                name: "MassageQrCodePayments",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    MassageId = table.Column<int>(type: "int", nullable: false),
                    CreditTotal = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    CreatedBy = table.Column<int>(type: "int", nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    UpdatedBy = table.Column<int>(type: "int", nullable: true),
                    UpdatedDate = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    DeletedBy = table.Column<int>(type: "int", nullable: true),
                    DeletedDate = table.Column<DateTime>(type: "datetime(6)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MassageQrCodePayments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MassageQrCodePayments_Massages_MassageId",
                        column: x => x.MassageId,
                        principalTable: "Massages",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "NL2oMRLPZZ8Lye5LlT17mEIIA1a9OtzYep4R+mPfw58=", new byte[] { 164, 81, 2, 159, 50, 34, 241, 233, 240, 70, 232, 92, 247, 237, 43, 135 } });

            migrationBuilder.CreateIndex(
                name: "IX_MassageQrCodePayments_MassageId",
                table: "MassageQrCodePayments",
                column: "MassageId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MassageQrCodePayments");

            migrationBuilder.DropColumn(
                name: "IsQrCode",
                table: "MemberTransactions");

            migrationBuilder.DropColumn(
                name: "QrCodeTotal",
                table: "MemberTransactions");

            migrationBuilder.DropColumn(
                name: "IsQrCodeAngel",
                table: "Massages");

            migrationBuilder.DropColumn(
                name: "IsQrCodeEtc",
                table: "Massages");

            migrationBuilder.DropColumn(
                name: "IsQrCodeFood",
                table: "Massages");

            migrationBuilder.DropColumn(
                name: "IsQrCodeRoom",
                table: "Massages");

            migrationBuilder.DropColumn(
                name: "QrCodeAngelTotal",
                table: "Massages");

            migrationBuilder.DropColumn(
                name: "QrCodeEtcTotal",
                table: "Massages");

            migrationBuilder.DropColumn(
                name: "QrCodeFoodTotal",
                table: "Massages");

            migrationBuilder.DropColumn(
                name: "QrCodeRoomTotal",
                table: "Massages");

            migrationBuilder.DropColumn(
                name: "QrCodeTotal",
                table: "Massages");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "aAQ48O+KruQw4gDcG+k21TrU3dzhIwqcAq0gZlDyWxI=", new byte[] { 141, 5, 164, 216, 81, 51, 251, 121, 110, 81, 13, 137, 103, 6, 147, 91 } });
        }
    }
}
