﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class AddColumnFeeTableDeductType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AngelDebts_Angels_AngelId1",
                table: "AngelDebts");

            migrationBuilder.DropIndex(
                name: "IX_AngelDebts_AngelId1",
                table: "AngelDebts");

            migrationBuilder.DropColumn(
                name: "AngelId1",
                table: "AngelDebts");

            migrationBuilder.AddColumn<decimal>(
                name: "Fee",
                table: "DeductTypes",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Fee",
                table: "DeductTypes");

            migrationBuilder.AddColumn<int>(
                name: "AngelId1",
                table: "AngelDebts",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_AngelDebts_AngelId1",
                table: "AngelDebts",
                column: "AngelId1");

            migrationBuilder.AddForeignKey(
                name: "FK_AngelDebts_Angels_AngelId1",
                table: "AngelDebts",
                column: "AngelId1",
                principalTable: "Angels",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
