﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class changeTablemassageCreditandmassageMember : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Total",
                table: "MassageMemberPayments",
                newName: "MemberTotal");

            migrationBuilder.RenameColumn(
                name: "Total",
                table: "MassageCreditPayments",
                newName: "CreditTotal");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "FTQIcypwgcF425mbBsP5sDdXOJWgFU+FcTauHCQCrXA=", new byte[] { 104, 130, 27, 97, 80, 187, 13, 136, 94, 232, 76, 21, 67, 123, 29, 242 } });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "MemberTotal",
                table: "MassageMemberPayments",
                newName: "Total");

            migrationBuilder.RenameColumn(
                name: "CreditTotal",
                table: "MassageCreditPayments",
                newName: "Total");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "VivhsGf6ac259Pok0wQMxcCCz5Vnk/eCAJnDOH05xj4=", new byte[] { 225, 18, 207, 198, 40, 193, 238, 195, 66, 89, 175, 86, 162, 162, 35, 191 } });
        }
    }
}
