﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class EditAgency : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "zK9TKCzwQWarZX9fq6/r7BecBh50x1UzX99hSvkBmkA=", new byte[] { 209, 198, 38, 175, 119, 124, 48, 185, 7, 91, 178, 56, 208, 34, 93, 251 } });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "qaTOt/vJNzvH6A3IuYf5oopYx/rLoZLvcJ/4iJOdpmo=", new byte[] { 140, 36, 50, 63, 6, 166, 238, 181, 126, 89, 62, 45, 41, 28, 111, 199 } });
        }
    }
}
