﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class editMasterDataAgency : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Agency",
                keyColumn: "Id",
                keyValue: 1,
                column: "Code",
                value: "Not Specified");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "5CfyXgHkpwH4AaIDqu4p1IPeTdriKUu04pd8tRlnKt0=", new byte[] { 226, 101, 192, 82, 150, 19, 92, 11, 132, 81, 166, 28, 18, 174, 39, 75 } });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Agency",
                keyColumn: "Id",
                keyValue: 1,
                column: "Code",
                value: null);

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "uldE4mjyYmeRT3DSwwQjdW+rAxr9ugO9X9VF9HN+mEs=", new byte[] { 153, 175, 157, 174, 144, 76, 182, 161, 250, 230, 24, 179, 149, 246, 152, 198 } });
        }
    }
}
