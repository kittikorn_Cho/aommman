﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class AddRelationShipAngelPerformance : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "AgenciesId",
                table: "AgencyPerformances",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "36cjU4AzJUq7JTQxiuhLZxNlXmx/nOU1SZQkwab+vY4=", new byte[] { 69, 42, 233, 150, 168, 138, 6, 63, 155, 170, 32, 147, 70, 42, 44, 68 } });

            migrationBuilder.CreateIndex(
                name: "IX_AgencyPerformances_AgenciesId",
                table: "AgencyPerformances",
                column: "AgenciesId");

            migrationBuilder.CreateIndex(
                name: "IX_AgencyPerformances_CarlendarDayId",
                table: "AgencyPerformances",
                column: "CarlendarDayId");

            migrationBuilder.AddForeignKey(
                name: "FK_AgencyPerformances_Agency_AgenciesId",
                table: "AgencyPerformances",
                column: "AgenciesId",
                principalTable: "Agency",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_AgencyPerformances_CalendarDays_CarlendarDayId",
                table: "AgencyPerformances",
                column: "CarlendarDayId",
                principalTable: "CalendarDays",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AgencyPerformances_Agency_AgenciesId",
                table: "AgencyPerformances");

            migrationBuilder.DropForeignKey(
                name: "FK_AgencyPerformances_CalendarDays_CarlendarDayId",
                table: "AgencyPerformances");

            migrationBuilder.DropIndex(
                name: "IX_AgencyPerformances_AgenciesId",
                table: "AgencyPerformances");

            migrationBuilder.DropIndex(
                name: "IX_AgencyPerformances_CarlendarDayId",
                table: "AgencyPerformances");

            migrationBuilder.DropColumn(
                name: "AgenciesId",
                table: "AgencyPerformances");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "DuN8FPo65TZuHFeVpsDHxJIJxbHqbbccTKv/gqE2NuU=", new byte[] { 230, 151, 185, 28, 34, 188, 243, 160, 254, 4, 130, 1, 88, 213, 245, 249 } });
        }
    }
}
