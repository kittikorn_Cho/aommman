﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class seedMenuAngelSalaryCountDay : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "Code", "Name" },
                values: new object[] { "AngelSalaryCountDay", "พนักงานบริการ --> ตรวจสอบรอบ-เวลาทำงาน" });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "Code", "Name" },
                values: new object[] { "AngelHistoryDebt", "พนักงานบริการ --> ประวัติหักหนี้" });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "Code", "Name" },
                values: new object[] { "AngelManageWorking", "พนักงานบริการ --> ลงเวลาปฎิบัติงาน" });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "Code", "Name" },
                values: new object[] { "Member", "เมมเบอร์ --> จัดการข้อมูลเมมเบอร์" });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "Code", "Name" },
                values: new object[] { "MemberType", "เมมเบอร์ --> ประเภทเมมเบอร์" });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "Code", "Name" },
                values: new object[] { "MemberItem", "เมมเบอร์ --> ของแถมเมมเบอร์" });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "Code", "Name" },
                values: new object[] { "MemberPayment", "เมมเบอร์ --> จัดการข้อมูลชำระเงินเมมเบอร์" });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "Code", "Name" },
                values: new object[] { "Reception", "พนักงานต้อนรับ" });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "Code", "Name" },
                values: new object[] { "ReceptionSell", "สรุปการทำงาน" });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "Code", "Name" },
                values: new object[] { "Room", "ห้อง" });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "Code", "Name" },
                values: new object[] { "Role", "สิทธิ์" });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "Code", "Name" },
                values: new object[] { "User", "ผู้ใช้ระบบ" });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "Code", "Name" },
                values: new object[] { "CheckIn", "เช็คอิน" });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "Code", "Name" },
                values: new object[] { "SettingDailyDebt", "ตั้งค่า --> ค่าใช้จ่ายประจำวัน" });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "Code", "Name" },
                values: new object[] { "Working", "สถานะการทำงาน" });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "Code", "Name" },
                values: new object[] { "WagePayment", "รอการชำระเงิน" });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "Code", "Name" },
                values: new object[] { "AngelPerformance", "จ่ายค่าตอบแทน" });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "Code", "Name" },
                values: new object[] { "UnPaid", "ค้างชำระ" });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "Code", "Name" },
                values: new object[] { "Report", "รายงาน" });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "Code", "Name" },
                values: new object[] { "DailyRemittanceSum", "รายงาน --> ใบสรุปส่งเงินประจำวัน" });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "Code", "Name" },
                values: new object[] { "MemberSaleSum", "รายงาน --> สรุปยอดขายเมมเบอร์" });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "Code", "Name" },
                values: new object[] { "MemberRoleSum", "รายงาน --> สรุปใช้สิทธ์เมมเบอร์" });

            migrationBuilder.InsertData(
                table: "Menus",
                columns: new[] { "Id", "Code", "CreatedBy", "CreatedDate", "DeletedBy", "DeletedDate", "Name", "Slug", "UpdatedBy", "UpdatedDate" },
                values: new object[] { 31, "CouponPaymentAmount", null, null, null, null, "รายงาน --> ใบสรุปส่งเงิน(จ่ายคูปอง)", "", null, null });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "I+2p18nDgZTkruaQ41f3cN1+fDnzhwg+zmdVLv83P7U=", new byte[] { 182, 54, 97, 214, 120, 99, 153, 96, 208, 101, 215, 89, 18, 174, 119, 149 } });

            migrationBuilder.InsertData(
                table: "RoleMenu",
                columns: new[] { "MenuId", "RoleId" },
                values: new object[] { 31, 1 });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "RoleMenu",
                keyColumns: new[] { "MenuId", "RoleId" },
                keyValues: new object[] { 31, 1 });

            migrationBuilder.DeleteData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 31);

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "Code", "Name" },
                values: new object[] { "AngelHistoryDebt", "พนักงานบริการ --> ประวัติหักหนี้" });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "Code", "Name" },
                values: new object[] { "AngelManageWorking", "พนักงานบริการ --> ลงเวลาปฎิบัติงาน" });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "Code", "Name" },
                values: new object[] { "Member", "เมมเบอร์ --> จัดการข้อมูลเมมเบอร์" });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "Code", "Name" },
                values: new object[] { "MemberType", "เมมเบอร์ --> ประเภทเมมเบอร์" });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "Code", "Name" },
                values: new object[] { "MemberItem", "เมมเบอร์ --> ของแถมเมมเบอร์" });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "Code", "Name" },
                values: new object[] { "MemberPayment", "เมมเบอร์ --> จัดการข้อมูลชำระเงินเมมเบอร์" });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "Code", "Name" },
                values: new object[] { "Reception", "พนักงานต้อนรับ" });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "Code", "Name" },
                values: new object[] { "ReceptionSell", "สรุปการทำงาน" });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "Code", "Name" },
                values: new object[] { "Room", "ห้อง" });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "Code", "Name" },
                values: new object[] { "Role", "สิทธิ์" });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "Code", "Name" },
                values: new object[] { "User", "ผู้ใช้ระบบ" });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "Code", "Name" },
                values: new object[] { "CheckIn", "เช็คอิน" });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "Code", "Name" },
                values: new object[] { "SettingDailyDebt", "ตั้งค่า --> ค่าใช้จ่ายประจำวัน" });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "Code", "Name" },
                values: new object[] { "Working", "สถานะการทำงาน" });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "Code", "Name" },
                values: new object[] { "WagePayment", "รอการชำระเงิน" });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "Code", "Name" },
                values: new object[] { "AngelPerformance", "จ่ายค่าตอบแทน" });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "Code", "Name" },
                values: new object[] { "UnPaid", "ค้างชำระ" });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "Code", "Name" },
                values: new object[] { "Report", "รายงาน" });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "Code", "Name" },
                values: new object[] { "DailyRemittanceSum", "รายงาน --> ใบสรุปส่งเงินประจำวัน" });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "Code", "Name" },
                values: new object[] { "MemberSaleSum", "รายงาน --> สรุปยอดขายเมมเบอร์" });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "Code", "Name" },
                values: new object[] { "MemberRoleSum", "รายงาน --> สรุปใช้สิทธ์เมมเบอร์" });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "Code", "Name" },
                values: new object[] { "CouponPaymentAmount", "รายงาน --> ใบสรุปส่งเงิน(จ่ายคูปอง)" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "C8+QtfgVWHWDbjFvpv0i404A60EC8+ztCsknVyOSJEE=", new byte[] { 134, 216, 97, 180, 64, 206, 141, 52, 32, 89, 143, 33, 79, 13, 65, 24 } });
        }
    }
}
