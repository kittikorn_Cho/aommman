﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class AddColumnDebtNowTableAngelPerformanceDebt : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "DebtNow",
                table: "AngelPerformanceDebts",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "UYfIUOWXjIJ6GlPS99brwcHXQpvHACw2ot3F8abgtpM=", new byte[] { 218, 163, 21, 98, 171, 126, 68, 222, 113, 125, 184, 3, 173, 179, 65, 242 } });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DebtNow",
                table: "AngelPerformanceDebts");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "M5kFyJ2qWKI+lwm1YFq15HDk9vpd76NqCxEQt9GiSL0=", new byte[] { 174, 29, 204, 224, 105, 134, 175, 28, 181, 210, 82, 192, 191, 56, 148, 73 } });
        }
    }
}
