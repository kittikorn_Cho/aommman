﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class AddNewPasswordSeeder : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Menus",
                columns: new[] { "Id", "Code", "CreatedBy", "CreatedDate", "DeletedBy", "DeletedDate", "Name", "Slug", "UpdatedBy", "UpdatedDate" },
                values: new object[,]
                {
                    { 38, "InfoPayment", null, null, null, null, "รายการชำระเงิน", "", null, null },
                    { 62, "PointReception", null, null, null, null, "พ้อยท์พนักงานต้อนรับ --> จัดการพ้อยท์พนักงานต้อนรับ", "", null, null },
                    { 61, "PointAngel", null, null, null, null, "พ้อยท์พนักงานบริการ --> จัดการพ้อยท์พนักงานบริการ", "", null, null },
                    { 60, "PointMember", null, null, null, null, "พ้อยท์เมมเบอร์ --> จัดการพ้อยท์เมมเบอร์", "", null, null },
                    { 59, "Point", null, null, null, null, "สะสมคะแนน", "", null, null },
                    { 58, "MergeCheckIn", null, null, null, null, "เช็คอินห้องทะลุ", "", null, null },
                    { 56, "Agency", null, null, null, null, "เอเจนซี่ -->  จัดการข้อมูลเอเจนซี่", "", null, null },
                    { 55, "ReportAgenciesList", null, null, null, null, "รายงาน --> จ่ายค่าตอบแทนเอเจนซี่", "", null, null },
                    { 54, "CountriesMassageOrMemberList", null, null, null, null, "รายงาน --> สรุปประเทศที่มาใช้งาน", "", null, null },
                    { 53, "InComeDaily", null, null, null, null, "รายงาน --> สรุปรายได้ประจำวันที่", "", null, null },
                    { 52, "AngelDebtReport", null, null, null, null, "รายงาน --> สรุปหนี้พนักงาน", "", null, null },
                    { 51, "CountWorkDaily", null, null, null, null, "รายงาน --> ตรวจสอบ-เวลาทำงาน", "", null, null },
                    { 57, "AgencyPerformance", null, null, null, null, "เอเจนซี่ --> จ่ายค่าตอบแทนเอเจนซี่", "", null, null },
                    { 49, "MassageSetting", null, null, null, null, "ตั้งค่า --> ตั้งค่าระบบ", "", null, null },
                    { 50, "AllMemberExpire", null, null, null, null, "รายงาน --> วันหมดอายุสมาชิกทั้งหมด", "", null, null },
                    { 39, "ReceptionSell", null, null, null, null, "รายงาน --> ยอดเมมเบอร์ระหว่างวัน", "", null, null },
                    { 40, "ReceptionRoundTotal", null, null, null, null, "รายงาน --> สรุปยอดเชียร์", "", null, null },
                    { 41, "TipAngel", null, null, null, null, "จ่ายทิปพนักงาน", "", null, null },
                    { 43, "TableCashier", null, null, null, null, "รายงาน --> สรุปแคชเชียร์รายบุคคล", "", null, null },
                    { 42, "SearchRoom", null, null, null, null, "ค้นหาห้อง", "", null, null },
                    { 44, "MemberFoodPayment", null, null, null, null, "เมมเบอร์ --> จัดการข้อมูลชำระเงินค่าอาหารเมมเบอร์", "", null, null },
                    { 45, "MembeFoodReport", null, null, null, null, "รายงาน --> ตัดค่าอาหารเมมเบอร์", "", null, null },
                    { 46, "SellSuite", null, null, null, null, "รายงาน --> สรุปการขายห้องสูท", "", null, null },
                    { 47, "MemberTopupReport", null, null, null, null, "รายงาน --> สรุปยอดออนท็อปเมมเบอร์", "", null, null },
                    { 48, "MemberCreditAmount", null, null, null, null, "รายงาน --> ยอดคงเหลือเมมเบอร์", "", null, null }
                });

            migrationBuilder.InsertData(
                table: "RoomTypes",
                columns: new[] { "Id", "Code", "CreatedBy", "CreatedDate", "DeletedBy", "DeletedDate", "Name", "RoomRate", "RoomRateNext", "RoundTime", "UpdatedBy", "UpdatedDate" },
                values: new object[,]
                {
                    { 12, "suite7", null, null, null, null, "สูท7", 0m, 0m, 0, null, null },
                    { 15, "suite10", null, null, null, null, "สูท10", 0m, 0m, 0, null, null },
                    { 14, "suite9", null, null, null, null, "สูท9", 0m, 0m, 0, null, null },
                    { 13, "suite8", null, null, null, null, "สูท8", 0m, 0m, 0, null, null },
                    { 16, "vip2", null, null, null, null, "วีไอพี2", 0m, 0m, 0, null, null },
                    { 11, "suite6", null, null, null, null, "สูท6", 0m, 0m, 0, null, null },
                    { 5, "suite3", null, null, null, null, "สูท3", 0m, 0m, 0, null, null },
                    { 9, "c3", null, null, null, null, "สูทC3", 0m, 0m, 0, null, null },
                    { 8, "roundSell", null, null, null, null, "ซื้อรอบ", 0m, 0m, 0, null, null },
                    { 7, "suite5", null, null, null, null, "สูท5", 0m, 0m, 0, null, null },
                    { 6, "suite4", null, null, null, null, "สูท4", 0m, 0m, 0, null, null },
                    { 17, "vip3", null, null, null, null, "วีไอพี3", 0m, 0m, 0, null, null },
                    { 10, "c5", null, null, null, null, "สูทC5", 0m, 0m, 0, null, null }
                });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "fBSeK9SV1rUYCpNO/dDgi5jhTd4A4OPbsDygyqWhKg4=", new byte[] { 41, 66, 48, 157, 209, 132, 31, 14, 250, 20, 39, 233, 243, 62, 164, 86 } });

            migrationBuilder.InsertData(
                table: "RoleMenu",
                columns: new[] { "MenuId", "RoleId" },
                values: new object[,]
                {
                    { 38, 1 },
                    { 60, 1 },
                    { 59, 1 },
                    { 58, 1 },
                    { 57, 1 },
                    { 56, 1 },
                    { 55, 1 },
                    { 54, 1 },
                    { 53, 1 },
                    { 52, 1 },
                    { 51, 1 },
                    { 61, 1 },
                    { 50, 1 },
                    { 48, 1 },
                    { 47, 1 },
                    { 46, 1 },
                    { 45, 1 },
                    { 44, 1 },
                    { 43, 1 },
                    { 42, 1 },
                    { 41, 1 },
                    { 40, 1 },
                    { 39, 1 },
                    { 49, 1 },
                    { 62, 1 }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "RoleMenu",
                keyColumns: new[] { "MenuId", "RoleId" },
                keyValues: new object[] { 38, 1 });

            migrationBuilder.DeleteData(
                table: "RoleMenu",
                keyColumns: new[] { "MenuId", "RoleId" },
                keyValues: new object[] { 39, 1 });

            migrationBuilder.DeleteData(
                table: "RoleMenu",
                keyColumns: new[] { "MenuId", "RoleId" },
                keyValues: new object[] { 40, 1 });

            migrationBuilder.DeleteData(
                table: "RoleMenu",
                keyColumns: new[] { "MenuId", "RoleId" },
                keyValues: new object[] { 41, 1 });

            migrationBuilder.DeleteData(
                table: "RoleMenu",
                keyColumns: new[] { "MenuId", "RoleId" },
                keyValues: new object[] { 42, 1 });

            migrationBuilder.DeleteData(
                table: "RoleMenu",
                keyColumns: new[] { "MenuId", "RoleId" },
                keyValues: new object[] { 43, 1 });

            migrationBuilder.DeleteData(
                table: "RoleMenu",
                keyColumns: new[] { "MenuId", "RoleId" },
                keyValues: new object[] { 44, 1 });

            migrationBuilder.DeleteData(
                table: "RoleMenu",
                keyColumns: new[] { "MenuId", "RoleId" },
                keyValues: new object[] { 45, 1 });

            migrationBuilder.DeleteData(
                table: "RoleMenu",
                keyColumns: new[] { "MenuId", "RoleId" },
                keyValues: new object[] { 46, 1 });

            migrationBuilder.DeleteData(
                table: "RoleMenu",
                keyColumns: new[] { "MenuId", "RoleId" },
                keyValues: new object[] { 47, 1 });

            migrationBuilder.DeleteData(
                table: "RoleMenu",
                keyColumns: new[] { "MenuId", "RoleId" },
                keyValues: new object[] { 48, 1 });

            migrationBuilder.DeleteData(
                table: "RoleMenu",
                keyColumns: new[] { "MenuId", "RoleId" },
                keyValues: new object[] { 49, 1 });

            migrationBuilder.DeleteData(
                table: "RoleMenu",
                keyColumns: new[] { "MenuId", "RoleId" },
                keyValues: new object[] { 50, 1 });

            migrationBuilder.DeleteData(
                table: "RoleMenu",
                keyColumns: new[] { "MenuId", "RoleId" },
                keyValues: new object[] { 51, 1 });

            migrationBuilder.DeleteData(
                table: "RoleMenu",
                keyColumns: new[] { "MenuId", "RoleId" },
                keyValues: new object[] { 52, 1 });

            migrationBuilder.DeleteData(
                table: "RoleMenu",
                keyColumns: new[] { "MenuId", "RoleId" },
                keyValues: new object[] { 53, 1 });

            migrationBuilder.DeleteData(
                table: "RoleMenu",
                keyColumns: new[] { "MenuId", "RoleId" },
                keyValues: new object[] { 54, 1 });

            migrationBuilder.DeleteData(
                table: "RoleMenu",
                keyColumns: new[] { "MenuId", "RoleId" },
                keyValues: new object[] { 55, 1 });

            migrationBuilder.DeleteData(
                table: "RoleMenu",
                keyColumns: new[] { "MenuId", "RoleId" },
                keyValues: new object[] { 56, 1 });

            migrationBuilder.DeleteData(
                table: "RoleMenu",
                keyColumns: new[] { "MenuId", "RoleId" },
                keyValues: new object[] { 57, 1 });

            migrationBuilder.DeleteData(
                table: "RoleMenu",
                keyColumns: new[] { "MenuId", "RoleId" },
                keyValues: new object[] { 58, 1 });

            migrationBuilder.DeleteData(
                table: "RoleMenu",
                keyColumns: new[] { "MenuId", "RoleId" },
                keyValues: new object[] { 59, 1 });

            migrationBuilder.DeleteData(
                table: "RoleMenu",
                keyColumns: new[] { "MenuId", "RoleId" },
                keyValues: new object[] { 60, 1 });

            migrationBuilder.DeleteData(
                table: "RoleMenu",
                keyColumns: new[] { "MenuId", "RoleId" },
                keyValues: new object[] { 61, 1 });

            migrationBuilder.DeleteData(
                table: "RoleMenu",
                keyColumns: new[] { "MenuId", "RoleId" },
                keyValues: new object[] { 62, 1 });

            migrationBuilder.DeleteData(
                table: "RoomTypes",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "RoomTypes",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "RoomTypes",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "RoomTypes",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "RoomTypes",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "RoomTypes",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "RoomTypes",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "RoomTypes",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "RoomTypes",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "RoomTypes",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "RoomTypes",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "RoomTypes",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "RoomTypes",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 38);

            migrationBuilder.DeleteData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 39);

            migrationBuilder.DeleteData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 40);

            migrationBuilder.DeleteData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 41);

            migrationBuilder.DeleteData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 42);

            migrationBuilder.DeleteData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 43);

            migrationBuilder.DeleteData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 44);

            migrationBuilder.DeleteData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 45);

            migrationBuilder.DeleteData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 46);

            migrationBuilder.DeleteData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 47);

            migrationBuilder.DeleteData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 48);

            migrationBuilder.DeleteData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 49);

            migrationBuilder.DeleteData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 50);

            migrationBuilder.DeleteData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 51);

            migrationBuilder.DeleteData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 52);

            migrationBuilder.DeleteData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 53);

            migrationBuilder.DeleteData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 54);

            migrationBuilder.DeleteData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 55);

            migrationBuilder.DeleteData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 56);

            migrationBuilder.DeleteData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 57);

            migrationBuilder.DeleteData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 58);

            migrationBuilder.DeleteData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 59);

            migrationBuilder.DeleteData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 60);

            migrationBuilder.DeleteData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 61);

            migrationBuilder.DeleteData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 62);

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "Qb3V2/pvEzUNegeGWCWFbTv7S2b8xCmhwf7IbRcIGtA=", new byte[] { 68, 212, 17, 195, 75, 83, 233, 17, 87, 49, 233, 52, 91, 92, 70, 33 } });
        }
    }
}
