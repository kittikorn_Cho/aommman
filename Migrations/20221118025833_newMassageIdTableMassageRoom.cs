﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class newMassageIdTableMassageRoom : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "OldMassageId",
                table: "Massages");

            migrationBuilder.AddColumn<int>(
                name: "NewMassageId",
                table: "MassageRooms",
                type: "int",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "KiQYXQo6HUuHRpEaEx2143NiDsGdoyYLzQX+gMGtFsQ=", new byte[] { 117, 60, 19, 3, 49, 30, 190, 120, 236, 143, 132, 146, 242, 136, 212, 223 } });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "NewMassageId",
                table: "MassageRooms");

            migrationBuilder.AddColumn<int>(
                name: "OldMassageId",
                table: "Massages",
                type: "int",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "v6avvlJ1Ezl28m01XxWqxKAXn9FIEXgRRrMbW2F+2do=", new byte[] { 205, 79, 147, 145, 106, 210, 20, 37, 33, 193, 178, 218, 106, 184, 118, 137 } });
        }
    }
}
