﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class addColumnIsDisCountMassageAngelandmassageRoom : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DiscountRound",
                table: "MassageRooms");

            migrationBuilder.RenameColumn(
                name: "IsDiscountRound",
                table: "MassageRooms",
                newName: "IsDiscount");

            migrationBuilder.AddColumn<bool>(
                name: "IsDiscount",
                table: "MassageAngels",
                type: "tinyint(1)",
                nullable: false,
                defaultValue: false);

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "13AHmuRDDh1rqqtHK/Mi+rIknFPVa3ZHGAa9vw6zY80=", new byte[] { 171, 144, 125, 154, 215, 16, 234, 1, 13, 8, 171, 42, 177, 145, 148, 241 } });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsDiscount",
                table: "MassageAngels");

            migrationBuilder.RenameColumn(
                name: "IsDiscount",
                table: "MassageRooms",
                newName: "IsDiscountRound");

            migrationBuilder.AddColumn<decimal>(
                name: "DiscountRound",
                table: "MassageRooms",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "2E7c3GdiyAKDJeIeF8PZlCQcOq2aXh+5oTU3Pc5RcL4=", new byte[] { 127, 233, 97, 173, 2, 10, 124, 164, 100, 158, 99, 191, 54, 44, 92, 214 } });
        }
    }
}
