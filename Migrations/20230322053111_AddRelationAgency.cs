﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class AddRelationAgency : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.AddColumn<int>(
            //    name: "AgencyTypeId",
            //    table: "Agency",
            //    type: "int",
            //    nullable: false,
            //    defaultValue: 0);

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "jQXewJ0AEZl1bLDZYUgFR8Pnf/7BwHO24jbO93VPwus=", new byte[] { 127, 24, 134, 176, 216, 194, 254, 30, 188, 118, 124, 183, 63, 60, 158, 125 } });

            //migrationBuilder.CreateIndex(
            //    name: "IX_Agency_AgencyTypeId",
            //    table: "Agency",
            //    column: "AgencyTypeId");

            //migrationBuilder.AddForeignKey(
            //    name: "FK_Agency_AgencyTypes_AgencyTypeId",
            //    table: "Agency",
            //    column: "AgencyTypeId",
            //    principalTable: "AgencyTypes",
            //    principalColumn: "Id",
            //    onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.DropForeignKey(
            //    name: "FK_Agency_AgencyTypes_AgencyTypeId",
            //    table: "Agency");

            //migrationBuilder.DropIndex(
            //    name: "IX_Agency_AgencyTypeId",
            //    table: "Agency");

            //migrationBuilder.DropColumn(
            //    name: "AgencyTypeId",
            //    table: "Agency");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "n6Z+k7MLgn0ae7S0WqOKXjyFwmkS/oJk7rsZ/AK8F+c=", new byte[] { 16, 60, 31, 209, 196, 112, 1, 237, 57, 152, 134, 218, 97, 34, 38, 225 } });
        }
    }
}
