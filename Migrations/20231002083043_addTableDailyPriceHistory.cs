﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class addTableDailyPriceHistory : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "MbMu8dBJ6QD07SvmA562V4B9EvoRbCIZlgaIxqwtKB0=", new byte[] { 113, 102, 46, 28, 211, 14, 147, 42, 191, 45, 17, 85, 47, 106, 134, 88 } });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "4gS2kkyXyPhaMDX8JHFqbMnuO1RK2uoP7fgOiH0WzdQ=", new byte[] { 227, 55, 124, 174, 48, 48, 162, 126, 200, 207, 156, 132, 120, 224, 23, 252 } });
        }
    }
}
