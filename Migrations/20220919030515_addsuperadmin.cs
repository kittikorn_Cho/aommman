﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class addsuperadmin : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Roles",
                keyColumn: "Id",
                keyValue: 1,
                column: "Code",
                value: "_SUPERADMIN");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "2z0CVnSyOX9RYhUOqpG21dKA/7JgO55Y3F5eSocUWac=", new byte[] { 175, 180, 97, 96, 242, 117, 67, 9, 147, 30, 134, 144, 119, 202, 42, 103 } });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Roles",
                keyColumn: "Id",
                keyValue: 1,
                column: "Code",
                value: "SUPERADMIN");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "NCM3WaahSFoKro1ZqJ1YlMgKl1UONNdYfM/dYvAGy6o=", new byte[] { 42, 109, 161, 191, 244, 175, 185, 223, 66, 102, 116, 44, 195, 18, 222, 131 } });
        }
    }
}
