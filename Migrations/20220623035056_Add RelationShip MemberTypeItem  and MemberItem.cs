﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class AddRelationShipMemberTypeItemandMemberItem : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MemberItems_Items_ItemId",
                table: "MemberItems");

            migrationBuilder.DropForeignKey(
                name: "FK_MemberItems_Members_MemberId",
                table: "MemberItems");

            migrationBuilder.DropForeignKey(
                name: "FK_MemberTypeItems_Items_ItemId",
                table: "MemberTypeItems");

            migrationBuilder.DropForeignKey(
                name: "FK_MemberTypeItems_MemberTypes_MemberTypeId",
                table: "MemberTypeItems");

            migrationBuilder.AddForeignKey(
                name: "FK_MemberItems_Items_ItemId",
                table: "MemberItems",
                column: "ItemId",
                principalTable: "Items",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_MemberItems_Members_MemberId",
                table: "MemberItems",
                column: "MemberId",
                principalTable: "Members",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_MemberTypeItems_Items_ItemId",
                table: "MemberTypeItems",
                column: "ItemId",
                principalTable: "Items",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_MemberTypeItems_MemberTypes_MemberTypeId",
                table: "MemberTypeItems",
                column: "MemberTypeId",
                principalTable: "MemberTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MemberItems_Items_ItemId",
                table: "MemberItems");

            migrationBuilder.DropForeignKey(
                name: "FK_MemberItems_Members_MemberId",
                table: "MemberItems");

            migrationBuilder.DropForeignKey(
                name: "FK_MemberTypeItems_Items_ItemId",
                table: "MemberTypeItems");

            migrationBuilder.DropForeignKey(
                name: "FK_MemberTypeItems_MemberTypes_MemberTypeId",
                table: "MemberTypeItems");

            migrationBuilder.AddForeignKey(
                name: "FK_MemberItems_Items_ItemId",
                table: "MemberItems",
                column: "ItemId",
                principalTable: "Items",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_MemberItems_Members_MemberId",
                table: "MemberItems",
                column: "MemberId",
                principalTable: "Members",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_MemberTypeItems_Items_ItemId",
                table: "MemberTypeItems",
                column: "ItemId",
                principalTable: "Items",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_MemberTypeItems_MemberTypes_MemberTypeId",
                table: "MemberTypeItems",
                column: "MemberTypeId",
                principalTable: "MemberTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
