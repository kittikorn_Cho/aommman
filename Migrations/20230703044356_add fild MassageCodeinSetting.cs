﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class addfildMassageCodeinSetting : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "MassageCode",
                table: "Settings",
                type: "longtext",
                nullable: true)
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "M6Dy62LDOwPmXLYO+UwWnBz22VTUsPsfBL7iuo6+/Fw=", new byte[] { 76, 87, 201, 208, 24, 48, 193, 155, 180, 32, 232, 162, 152, 220, 138, 60 } });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "MassageCode",
                table: "Settings");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "ZQX5CRhAZ9rNm+1/qirgx5wlPprOlzX5XAKC3SVhJiI=", new byte[] { 193, 26, 207, 221, 18, 1, 187, 110, 219, 204, 31, 1, 37, 107, 224, 198 } });
        }
    }
}
