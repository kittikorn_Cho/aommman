﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class editTableMassageAngel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "C8+QtfgVWHWDbjFvpv0i404A60EC8+ztCsknVyOSJEE=", new byte[] { 134, 216, 97, 180, 64, 206, 141, 52, 32, 89, 143, 33, 79, 13, 65, 24 } });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "Kwl3aonv8CZ56xLrMKtNUCWi6SAWsgryg4Tp29YHbfo=", new byte[] { 70, 228, 162, 34, 80, 64, 135, 220, 137, 168, 115, 91, 22, 204, 132, 160 } });
        }
    }
}
