﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class addAgencyPerformanceIdinMassage : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "AgencyPerformanceId",
                table: "Massages",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "TLSQTddZ+fhiFxqjq57a7H1xcSzliAx4llqoYqyZ4c8=", new byte[] { 226, 64, 212, 138, 112, 83, 31, 224, 5, 150, 11, 157, 187, 229, 123, 214 } });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AgencyPerformanceId",
                table: "Massages");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "jQXewJ0AEZl1bLDZYUgFR8Pnf/7BwHO24jbO93VPwus=", new byte[] { 127, 24, 134, 176, 216, 194, 254, 30, 188, 118, 124, 183, 63, 60, 158, 125 } });
        }
    }
}
