﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class DeleteTableReceptionType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Receptions_ReceptionTypes_ReceptionTypeId",
                table: "Receptions");

            migrationBuilder.DropTable(
                name: "ReceptionTypes");

            migrationBuilder.DropIndex(
                name: "IX_Receptions_ReceptionTypeId",
                table: "Receptions");

            migrationBuilder.DropColumn(
                name: "ReceptionTypeId",
                table: "Receptions");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "WcxqED2Fm+RlIsilW1twXm0kjkdQyXJUyyc0GfLt768=", new byte[] { 32, 201, 117, 115, 182, 114, 110, 90, 206, 217, 76, 102, 105, 215, 114, 211 } });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ReceptionTypeId",
                table: "Receptions",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "ReceptionTypes",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Code = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    CreatedBy = table.Column<int>(type: "int", nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    DeletedBy = table.Column<int>(type: "int", nullable: true),
                    DeletedDate = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    Name = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    UpdatedBy = table.Column<int>(type: "int", nullable: true),
                    UpdatedDate = table.Column<DateTime>(type: "datetime(6)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ReceptionTypes", x => x.Id);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "QrHQRDA2XgdxIW74pW5TqfLjjQkUOu+RLTohEnZxaO4=", new byte[] { 37, 216, 169, 24, 144, 63, 218, 240, 83, 7, 64, 229, 178, 12, 166, 189 } });

            migrationBuilder.CreateIndex(
                name: "IX_Receptions_ReceptionTypeId",
                table: "Receptions",
                column: "ReceptionTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_Receptions_ReceptionTypes_ReceptionTypeId",
                table: "Receptions",
                column: "ReceptionTypeId",
                principalTable: "ReceptionTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
