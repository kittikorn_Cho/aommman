﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class AddForeignKeyCalendarIdTableMassage : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CalendarId",
                table: "Massages");

            migrationBuilder.RenameColumn(
                name: "ReceptionId",
                table: "Massages",
                newName: "CalendarDayId");

            migrationBuilder.CreateIndex(
                name: "IX_Massages_CalendarDayId",
                table: "Massages",
                column: "CalendarDayId");

            migrationBuilder.AddForeignKey(
                name: "FK_Massages_CalendarDays_CalendarDayId",
                table: "Massages",
                column: "CalendarDayId",
                principalTable: "CalendarDays",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Massages_CalendarDays_CalendarDayId",
                table: "Massages");

            migrationBuilder.DropIndex(
                name: "IX_Massages_CalendarDayId",
                table: "Massages");

            migrationBuilder.RenameColumn(
                name: "CalendarDayId",
                table: "Massages",
                newName: "ReceptionId");

            migrationBuilder.AddColumn<int>(
                name: "CalendarId",
                table: "Massages",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}
