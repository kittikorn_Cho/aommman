﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class AddColumnIsAngelDiscountCommTableSetting : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsAngelDiscountComm",
                table: "Settings",
                type: "tinyint(1)",
                nullable: false,
                defaultValue: false);

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "3k3hQYAJeeniraPFgjpl1QAdhjzPDXNf61Y8pdeJHPk=", new byte[] { 100, 124, 154, 131, 87, 143, 30, 118, 47, 84, 75, 226, 35, 222, 20, 18 } });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsAngelDiscountComm",
                table: "Settings");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "/FH8dg4g+0z6jXwI5r3q2Ihozx3y2IWggf2p8XTJG/U=", new byte[] { 106, 233, 204, 38, 117, 80, 156, 62, 7, 4, 110, 130, 188, 43, 193, 179 } });
        }
    }
}
