﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class editmemberitemTransaction : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "CreatedBy",
                table: "memberItemTransactions",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedDate",
                table: "memberItemTransactions",
                type: "datetime(6)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "DeletedBy",
                table: "memberItemTransactions",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UpdatedBy",
                table: "memberItemTransactions",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "UpdatedDate",
                table: "memberItemTransactions",
                type: "datetime(6)",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "99Cjs4S1T7WDs3eTFRZmeU2IccnvdOr+t+zmZzRloPQ=", new byte[] { 220, 90, 57, 163, 235, 151, 195, 61, 111, 23, 201, 158, 109, 234, 84, 181 } });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "memberItemTransactions");

            migrationBuilder.DropColumn(
                name: "CreatedDate",
                table: "memberItemTransactions");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "memberItemTransactions");

            migrationBuilder.DropColumn(
                name: "UpdatedBy",
                table: "memberItemTransactions");

            migrationBuilder.DropColumn(
                name: "UpdatedDate",
                table: "memberItemTransactions");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "VkeGOIpmcQE1+nt5CldMmlA9edBoPxn1V5KlOe41kYg=", new byte[] { 118, 45, 110, 52, 135, 191, 160, 142, 123, 225, 63, 48, 208, 72, 171, 159 } });
        }
    }
}
