﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class ChangeNameTableCalendar : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AngelPerformances_Calendars_CarlendarId",
                table: "AngelPerformances");

            migrationBuilder.DropForeignKey(
                name: "FK_AngelWorkings_Calendars_CalendarId",
                table: "AngelWorkings");

            migrationBuilder.DropForeignKey(
                name: "FK_MassageAngels_Calendars_CarlendarId",
                table: "MassageAngels");

            migrationBuilder.DropForeignKey(
                name: "FK_MassageRooms_Calendars_CarlendarId",
                table: "MassageRooms");

            migrationBuilder.DropForeignKey(
                name: "FK_MemberTransactions_Calendars_CarlendarId",
                table: "MemberTransactions");

            migrationBuilder.DropTable(
                name: "Calendars");

            migrationBuilder.DropIndex(
                name: "IX_MassageRooms_CarlendarId",
                table: "MassageRooms");

            migrationBuilder.DropIndex(
                name: "IX_MassageAngels_CarlendarId",
                table: "MassageAngels");

            migrationBuilder.DropColumn(
                name: "CarlendarId",
                table: "MassageRooms");

            migrationBuilder.DropColumn(
                name: "CalendarId",
                table: "MassageAngels");

            migrationBuilder.DropColumn(
                name: "CarlendarId",
                table: "MassageAngels");

            migrationBuilder.RenameColumn(
                name: "CarlendarId",
                table: "MemberTransactions",
                newName: "CalendarDayId");

            migrationBuilder.RenameIndex(
                name: "IX_MemberTransactions_CarlendarId",
                table: "MemberTransactions",
                newName: "IX_MemberTransactions_CalendarDayId");

            migrationBuilder.RenameColumn(
                name: "CalendarId",
                table: "AngelWorkings",
                newName: "CalendarDayId");

            migrationBuilder.RenameIndex(
                name: "IX_AngelWorkings_CalendarId",
                table: "AngelWorkings",
                newName: "IX_AngelWorkings_CalendarDayId");

            migrationBuilder.RenameColumn(
                name: "CarlendarId",
                table: "AngelPerformances",
                newName: "CarlendarDayId");

            migrationBuilder.RenameIndex(
                name: "IX_AngelPerformances_CarlendarId",
                table: "AngelPerformances",
                newName: "IX_AngelPerformances_CarlendarDayId");

            migrationBuilder.CreateTable(
                name: "CalendarDays",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    ToDay = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                    IsDate = table.Column<bool>(type: "tinyint(1)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CalendarDays", x => x.Id);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AddForeignKey(
                name: "FK_AngelPerformances_CalendarDays_CarlendarDayId",
                table: "AngelPerformances",
                column: "CarlendarDayId",
                principalTable: "CalendarDays",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_AngelWorkings_CalendarDays_CalendarDayId",
                table: "AngelWorkings",
                column: "CalendarDayId",
                principalTable: "CalendarDays",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_MemberTransactions_CalendarDays_CalendarDayId",
                table: "MemberTransactions",
                column: "CalendarDayId",
                principalTable: "CalendarDays",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AngelPerformances_CalendarDays_CarlendarDayId",
                table: "AngelPerformances");

            migrationBuilder.DropForeignKey(
                name: "FK_AngelWorkings_CalendarDays_CalendarDayId",
                table: "AngelWorkings");

            migrationBuilder.DropForeignKey(
                name: "FK_MemberTransactions_CalendarDays_CalendarDayId",
                table: "MemberTransactions");

            migrationBuilder.DropTable(
                name: "CalendarDays");

            migrationBuilder.RenameColumn(
                name: "CalendarDayId",
                table: "MemberTransactions",
                newName: "CarlendarId");

            migrationBuilder.RenameIndex(
                name: "IX_MemberTransactions_CalendarDayId",
                table: "MemberTransactions",
                newName: "IX_MemberTransactions_CarlendarId");

            migrationBuilder.RenameColumn(
                name: "CalendarDayId",
                table: "AngelWorkings",
                newName: "CalendarId");

            migrationBuilder.RenameIndex(
                name: "IX_AngelWorkings_CalendarDayId",
                table: "AngelWorkings",
                newName: "IX_AngelWorkings_CalendarId");

            migrationBuilder.RenameColumn(
                name: "CarlendarDayId",
                table: "AngelPerformances",
                newName: "CarlendarId");

            migrationBuilder.RenameIndex(
                name: "IX_AngelPerformances_CarlendarDayId",
                table: "AngelPerformances",
                newName: "IX_AngelPerformances_CarlendarId");

            migrationBuilder.AddColumn<int>(
                name: "CarlendarId",
                table: "MassageRooms",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CalendarId",
                table: "MassageAngels",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "CarlendarId",
                table: "MassageAngels",
                type: "int",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Calendars",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    IsDate = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    ToDay = table.Column<DateTime>(type: "datetime(6)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Calendars", x => x.Id);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateIndex(
                name: "IX_MassageRooms_CarlendarId",
                table: "MassageRooms",
                column: "CarlendarId");

            migrationBuilder.CreateIndex(
                name: "IX_MassageAngels_CarlendarId",
                table: "MassageAngels",
                column: "CarlendarId");

            migrationBuilder.AddForeignKey(
                name: "FK_AngelPerformances_Calendars_CarlendarId",
                table: "AngelPerformances",
                column: "CarlendarId",
                principalTable: "Calendars",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_AngelWorkings_Calendars_CalendarId",
                table: "AngelWorkings",
                column: "CalendarId",
                principalTable: "Calendars",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_MassageAngels_Calendars_CarlendarId",
                table: "MassageAngels",
                column: "CarlendarId",
                principalTable: "Calendars",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_MassageRooms_Calendars_CarlendarId",
                table: "MassageRooms",
                column: "CarlendarId",
                principalTable: "Calendars",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_MemberTransactions_Calendars_CarlendarId",
                table: "MemberTransactions",
                column: "CarlendarId",
                principalTable: "Calendars",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
