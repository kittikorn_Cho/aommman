﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class AddNewMenuReports : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "Code", "Name" },
                values: new object[] { "EditBillReception", "พนักงานต้อนรับ --> แก้หัวเชียร์" });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "Code", "Name" },
                values: new object[] { "Report", "รายงาน" });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "Code", "Name" },
                values: new object[] { "DailyRemittanceSum", "รายงาน --> ใบสรุปส่งเงินประจำวัน" });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "Code", "Name" },
                values: new object[] { "DailyRemittanceSumFinance", "รายงาน --> สรุปยอดขายประจำวัน (บัญชี)" });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "Code", "Name" },
                values: new object[] { "DiscountMaasageRoom", "รายงาน --> รับรองส่วนลดห้อง" });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "Code", "Name" },
                values: new object[] { "DiscountMaasageAngel", "รายงาน --> รับรองส่วนลดนวด" });

            migrationBuilder.InsertData(
                table: "Menus",
                columns: new[] { "Id", "Code", "CreatedBy", "CreatedDate", "DeletedBy", "DeletedDate", "Name", "Slug", "UpdatedBy", "UpdatedDate" },
                values: new object[,]
                {
                    { 33, "UnpaidMassage", null, null, null, null, "รายงาน --> ค้างชำระบิล", "", null, null },
                    { 34, "MemberSaleSum", null, null, null, null, "รายงาน --> สรุปยอดขายเมมเบอร์", "", null, null },
                    { 35, "MemberRoleSum", null, null, null, null, "รายงาน --> สรุปใช้สิทธ์เมมเบอร์", "", null, null },
                    { 36, "MemberExpire", null, null, null, null, "รายงาน --> วันหมดอายุสมาชิก", "", null, null },
                    { 37, "CouponPaymentAmount", null, null, null, null, "รายงาน --> ใบสรุปส่งเงิน(จ่ายคูปอง)", "", null, null }
                });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "2aOo9Yg5GlVb2yBuj+u3REsR1KjOrsxESBUXlyqEIo4=", new byte[] { 92, 26, 75, 139, 246, 117, 200, 79, 27, 84, 57, 19, 154, 117, 158, 123 } });

            migrationBuilder.InsertData(
                table: "RoleMenu",
                columns: new[] { "MenuId", "RoleId" },
                values: new object[,]
                {
                    { 33, 1 },
                    { 34, 1 },
                    { 35, 1 },
                    { 36, 1 },
                    { 37, 1 }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "RoleMenu",
                keyColumns: new[] { "MenuId", "RoleId" },
                keyValues: new object[] { 33, 1 });

            migrationBuilder.DeleteData(
                table: "RoleMenu",
                keyColumns: new[] { "MenuId", "RoleId" },
                keyValues: new object[] { 34, 1 });

            migrationBuilder.DeleteData(
                table: "RoleMenu",
                keyColumns: new[] { "MenuId", "RoleId" },
                keyValues: new object[] { 35, 1 });

            migrationBuilder.DeleteData(
                table: "RoleMenu",
                keyColumns: new[] { "MenuId", "RoleId" },
                keyValues: new object[] { 36, 1 });

            migrationBuilder.DeleteData(
                table: "RoleMenu",
                keyColumns: new[] { "MenuId", "RoleId" },
                keyValues: new object[] { 37, 1 });

            migrationBuilder.DeleteData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 33);

            migrationBuilder.DeleteData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 34);

            migrationBuilder.DeleteData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 35);

            migrationBuilder.DeleteData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 36);

            migrationBuilder.DeleteData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 37);

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "Code", "Name" },
                values: new object[] { "Report", "รายงาน" });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "Code", "Name" },
                values: new object[] { "DailyRemittanceSum", "รายงาน --> ใบสรุปส่งเงินประจำวัน" });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "Code", "Name" },
                values: new object[] { "MemberSaleSum", "รายงาน --> สรุปยอดขายเมมเบอร์" });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "Code", "Name" },
                values: new object[] { "MemberRoleSum", "รายงาน --> สรุปใช้สิทธ์เมมเบอร์" });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "Code", "Name" },
                values: new object[] { "CouponPaymentAmount", "รายงาน --> ใบสรุปส่งเงิน(จ่ายคูปอง)" });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "Code", "Name" },
                values: new object[] { "EditBillReception", "พนักงานต้อนรับ --> แก้หัวเชียร์" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "F+6eN3kcpyAkBN7FnGca+hTgcJNucGdxggVLHgxLppg=", new byte[] { 106, 95, 57, 251, 235, 226, 153, 22, 41, 223, 141, 53, 166, 30, 185, 93 } });
        }
    }
}
