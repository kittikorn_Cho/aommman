﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class editBookingAngels : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "CreatedBy",
                table: "BookingAngels",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedDate",
                table: "BookingAngels",
                type: "datetime(6)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "DeletedBy",
                table: "BookingAngels",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UpdatedBy",
                table: "BookingAngels",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "UpdatedDate",
                table: "BookingAngels",
                type: "datetime(6)",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "D6TCObEHng1IPzq0hjZm1S9h3vV59Vy1R7AKSjypZZ0=", new byte[] { 32, 20, 180, 60, 199, 236, 30, 22, 84, 101, 64, 197, 237, 83, 211, 133 } });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "BookingAngels");

            migrationBuilder.DropColumn(
                name: "CreatedDate",
                table: "BookingAngels");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "BookingAngels");

            migrationBuilder.DropColumn(
                name: "UpdatedBy",
                table: "BookingAngels");

            migrationBuilder.DropColumn(
                name: "UpdatedDate",
                table: "BookingAngels");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "JPDGj2sAST2nz0uFo8KmIhaoG+jt1pd0OY0rRxfan6g=", new byte[] { 208, 49, 157, 198, 42, 244, 20, 253, 154, 79, 23, 38, 210, 15, 133, 85 } });
        }
    }
}
