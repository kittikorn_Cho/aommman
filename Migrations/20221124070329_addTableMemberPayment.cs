﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class addTableMemberPayment : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<decimal>(
                name: "CerditAmount",
                table: "memberPayments",
                type: "decimal(18,2)",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(65,30)");

            migrationBuilder.AddColumn<int>(
                name: "CalendarId",
                table: "memberPayments",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "DocumentNumber",
                table: "memberPayments",
                type: "longtext",
                nullable: true)
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "KiTniVz2helBySZdVZlArsnetTvG1Ux5ZW5CRl+LNq8=", new byte[] { 137, 123, 6, 214, 23, 157, 42, 166, 136, 199, 213, 10, 40, 58, 10, 155 } });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CalendarId",
                table: "memberPayments");

            migrationBuilder.DropColumn(
                name: "DocumentNumber",
                table: "memberPayments");

            migrationBuilder.AlterColumn<decimal>(
                name: "CerditAmount",
                table: "memberPayments",
                type: "decimal(65,30)",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,2)");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "vWg4sn0qYUj4fgO+VQ7MraGI5Evad/l7url9zCyWHyw=", new byte[] { 115, 174, 165, 179, 26, 231, 124, 4, 205, 186, 123, 34, 245, 214, 219, 160 } });
        }
    }
}
