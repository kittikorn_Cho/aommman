﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class addIsAngalPerformanceCarlendarinSetting : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsAngelPerformanceCarlendar",
                table: "Settings",
                type: "tinyint(1)",
                nullable: false,
                defaultValue: false);

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "kbtd5UdzdjnraQNa8aLwr4EsJsokrEf0AT0OXOn4Dzs=", new byte[] { 67, 55, 42, 246, 15, 117, 119, 144, 20, 171, 203, 95, 98, 135, 120, 236 } });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsAngelPerformanceCarlendar",
                table: "Settings");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "cQBqhkKZqhkGKIg8fY4+YBiRGDP9W7FaJ0xj+/NvMR0=", new byte[] { 38, 2, 238, 131, 82, 142, 215, 114, 6, 155, 159, 122, 75, 195, 1, 139 } });
        }
    }
}
