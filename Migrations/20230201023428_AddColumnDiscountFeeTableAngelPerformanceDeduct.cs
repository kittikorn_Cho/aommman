﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class AddColumnDiscountFeeTableAngelPerformanceDeduct : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "DiscountFee",
                table: "AngelPerformanceDeducts",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "/FH8dg4g+0z6jXwI5r3q2Ihozx3y2IWggf2p8XTJG/U=", new byte[] { 106, 233, 204, 38, 117, 80, 156, 62, 7, 4, 110, 130, 188, 43, 193, 179 } });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DiscountFee",
                table: "AngelPerformanceDeducts");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "l2Ab/9XPAIM70A1u1pucfD9/+C273dygvXl0P4hCoPw=", new byte[] { 249, 21, 51, 99, 192, 140, 47, 220, 173, 190, 20, 255, 131, 14, 45, 120 } });
        }
    }
}
