﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class editRoundDecimal : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<decimal>(
                name: "Round",
                table: "MassageRooms",
                type: "decimal(18,2)",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "LumosKk4HUo9m033eVxZU4Xm+aIxTEfo0Bt31D0Pt24=", new byte[] { 123, 185, 234, 90, 199, 105, 101, 41, 60, 191, 81, 182, 87, 95, 239, 191 } });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "Round",
                table: "MassageRooms",
                type: "int",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,2)");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "KVqnHn5nwWJO4wJaGEd7OhcqWXbgBWgCC89vVwENq+A=", new byte[] { 226, 253, 217, 10, 33, 3, 99, 246, 24, 54, 209, 196, 120, 59, 191, 197 } });
        }
    }
}
