﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class DeletefieldAmountAndMemberTypeIdTableMemberTransaction : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MemberTransactions_MemberTypes_MemberTypeId",
                table: "MemberTransactions");

            migrationBuilder.DropIndex(
                name: "IX_MemberTransactions_MemberTypeId",
                table: "MemberTransactions");

            migrationBuilder.DropColumn(
                name: "Amount",
                table: "MemberTransactions");

            migrationBuilder.DropColumn(
                name: "MemberTypeId",
                table: "MemberTransactions");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "cO2QZuh8h7zxVprFNMHasE5R64Kou3Rw96/i0o5dB10=", new byte[] { 96, 229, 2, 255, 163, 142, 163, 100, 13, 171, 154, 17, 162, 46, 160, 54 } });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Amount",
                table: "MemberTransactions",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "MemberTypeId",
                table: "MemberTransactions",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "5edXSep3G3fnzI4fOi1bYh7E7pGRhgd7yRcipo6Mg2M=", new byte[] { 195, 71, 210, 161, 172, 83, 109, 122, 127, 8, 107, 212, 115, 246, 36, 240 } });

            migrationBuilder.CreateIndex(
                name: "IX_MemberTransactions_MemberTypeId",
                table: "MemberTransactions",
                column: "MemberTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_MemberTransactions_MemberTypes_MemberTypeId",
                table: "MemberTransactions",
                column: "MemberTypeId",
                principalTable: "MemberTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
