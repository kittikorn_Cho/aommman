﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class delete : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "GetOutTimeOlds",
                table: "AngelWorkings");

            migrationBuilder.DropColumn(
                name: "IsComebacks",
                table: "AngelWorkings");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "okMNED71nmtB5ErlUfEwM4DBxqAJ6ClIT85OwMr3kFA=", new byte[] { 117, 246, 33, 80, 208, 211, 184, 90, 7, 229, 234, 68, 138, 127, 93, 18 } });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "GetOutTimeOlds",
                table: "AngelWorkings",
                type: "datetime(6)",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsComebacks",
                table: "AngelWorkings",
                type: "tinyint(1)",
                nullable: false,
                defaultValue: false);

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "WJgMcFbQguLUrpqc4jAfsOr1IIBzP66jmO90JwgDPUo=", new byte[] { 43, 252, 153, 28, 192, 84, 84, 18, 226, 219, 26, 74, 210, 164, 84, 127 } });
        }
    }
}
