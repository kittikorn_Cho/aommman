﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class addIsPaymentTipinMassage : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsPaymentTip",
                table: "Massages",
                type: "tinyint(1)",
                nullable: false,
                defaultValue: false);

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "D6ROAgJNB0BnLU4wbM0gM80dfZjngIOvz+tzsOpagRI=", new byte[] { 37, 217, 70, 158, 63, 221, 126, 64, 109, 58, 246, 169, 108, 190, 19, 35 } });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsPaymentTip",
                table: "Massages");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "dj3dZPJWMuggzJTY957IlejlXsozT5RV+7hfL/NlUpU=", new byte[] { 5, 18, 76, 109, 114, 125, 238, 27, 202, 205, 172, 255, 36, 202, 231, 138 } });
        }
    }
}
