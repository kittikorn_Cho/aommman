﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class seedRoomRate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "RoomTypes",
                keyColumn: "Id",
                keyValue: 1,
                column: "RoomRate",
                value: 1000m);

            migrationBuilder.UpdateData(
                table: "RoomTypes",
                keyColumn: "Id",
                keyValue: 2,
                column: "RoomRate",
                value: 600m);

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "mEzWevWINWtLZrVODhwfg/LSHQjBPf3qX26TYwTDDzg=", new byte[] { 46, 165, 157, 46, 105, 149, 159, 213, 26, 62, 56, 216, 155, 159, 157, 237 } });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "RoomTypes",
                keyColumn: "Id",
                keyValue: 1,
                column: "RoomRate",
                value: 0m);

            migrationBuilder.UpdateData(
                table: "RoomTypes",
                keyColumn: "Id",
                keyValue: 2,
                column: "RoomRate",
                value: 0m);

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "nRKGuTXPDMmsuZy7DyZXhQovgrgNq9yLBZORygSu5qg=", new byte[] { 139, 147, 59, 54, 126, 168, 242, 97, 176, 87, 84, 60, 93, 230, 117, 80 } });
        }
    }
}
