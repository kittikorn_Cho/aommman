﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class AddColumnUseVipTableMemberpaymentAndAddColumnEtcTotalTableMassage : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "CashEtcTotal",
                table: "Massages",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "CreditEtcTotal",
                table: "Massages",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "DamagesTotal",
                table: "Massages",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<string>(
                name: "EntertainEtcRemark",
                table: "Massages",
                type: "longtext",
                nullable: true)
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AddColumn<decimal>(
                name: "EntertainEtcTotal",
                table: "Massages",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "EtcTotal",
                table: "Massages",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<bool>(
                name: "IsCashEtc",
                table: "Massages",
                type: "tinyint(1)",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsCreditEtc",
                table: "Massages",
                type: "tinyint(1)",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsEntertainEtc",
                table: "Massages",
                type: "tinyint(1)",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsMemberEtc",
                table: "Massages",
                type: "tinyint(1)",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<decimal>(
                name: "MemberEtcTotal",
                table: "Massages",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "OtherServiceChargesTotal",
                table: "Massages",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "TipTotal",
                table: "Massages",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "UseVip",
                table: "MassageMemberPayments",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "mpwD9kY83rUjQ50f338BF3ana5EeXGJNx4L0hsgZkhs=", new byte[] { 240, 241, 24, 244, 161, 123, 63, 239, 123, 93, 203, 149, 180, 241, 74, 201 } });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CashEtcTotal",
                table: "Massages");

            migrationBuilder.DropColumn(
                name: "CreditEtcTotal",
                table: "Massages");

            migrationBuilder.DropColumn(
                name: "DamagesTotal",
                table: "Massages");

            migrationBuilder.DropColumn(
                name: "EntertainEtcRemark",
                table: "Massages");

            migrationBuilder.DropColumn(
                name: "EntertainEtcTotal",
                table: "Massages");

            migrationBuilder.DropColumn(
                name: "EtcTotal",
                table: "Massages");

            migrationBuilder.DropColumn(
                name: "IsCashEtc",
                table: "Massages");

            migrationBuilder.DropColumn(
                name: "IsCreditEtc",
                table: "Massages");

            migrationBuilder.DropColumn(
                name: "IsEntertainEtc",
                table: "Massages");

            migrationBuilder.DropColumn(
                name: "IsMemberEtc",
                table: "Massages");

            migrationBuilder.DropColumn(
                name: "MemberEtcTotal",
                table: "Massages");

            migrationBuilder.DropColumn(
                name: "OtherServiceChargesTotal",
                table: "Massages");

            migrationBuilder.DropColumn(
                name: "TipTotal",
                table: "Massages");

            migrationBuilder.DropColumn(
                name: "UseVip",
                table: "MassageMemberPayments");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "UxdOqXf2GtqZDnUph9/xW7MtiaVpJ4w/sRkr9wZS8RY=", new byte[] { 231, 195, 236, 188, 220, 228, 202, 143, 154, 108, 181, 117, 196, 239, 106, 85 } });
        }
    }
}
