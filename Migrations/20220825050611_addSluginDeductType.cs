﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class addSluginDeductType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Slug",
                table: "MassageAngels");

            migrationBuilder.AddColumn<string>(
                name: "Slug",
                table: "DeductTypes",
                type: "longtext",
                nullable: true)
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "jdrxSH93iF6eRSJFV+4uGfsc+evMRrZgH97F9x2r/Dw=", new byte[] { 181, 195, 58, 54, 224, 249, 209, 99, 65, 236, 93, 240, 144, 110, 17, 33 } });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Slug",
                table: "DeductTypes");

            migrationBuilder.AddColumn<string>(
                name: "Slug",
                table: "MassageAngels",
                type: "longtext",
                nullable: true)
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "HrOY3/VLFyR8ztZaYC0MohY6FUdgooqwwk3vgrTbyoU=", new byte[] { 52, 225, 92, 58, 228, 82, 44, 230, 249, 182, 124, 246, 184, 110, 144, 46 } });
        }
    }
}
