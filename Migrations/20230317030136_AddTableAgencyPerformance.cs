﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class AddTableAgencyPerformance : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AgencyPerformances",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    CarlendarDayId = table.Column<int>(type: "int", nullable: false),
                    IsPay = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    IsUnPaid = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    TotalMassage = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    TotalRound = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    TotalMember = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    TotalReceive = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    CreatedBy = table.Column<int>(type: "int", nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    UpdatedBy = table.Column<int>(type: "int", nullable: true),
                    UpdatedDate = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    DeletedBy = table.Column<int>(type: "int", nullable: true),
                    DeletedDate = table.Column<DateTime>(type: "datetime(6)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AgencyPerformances", x => x.Id);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "DuN8FPo65TZuHFeVpsDHxJIJxbHqbbccTKv/gqE2NuU=", new byte[] { 230, 151, 185, 28, 34, 188, 243, 160, 254, 4, 130, 1, 88, 213, 245, 249 } });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AgencyPerformances");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "Rmtl0mDeBNptJWgMjJT6It1ld5kSOFSJ5AQUWUN2VuQ=", new byte[] { 30, 115, 21, 133, 201, 153, 195, 17, 43, 227, 80, 21, 212, 237, 156, 108 } });
        }
    }
}
