﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class addIsQrCodeReportinSitting : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsQrCodeReport",
                table: "Settings",
                type: "tinyint(1)",
                nullable: false,
                defaultValue: false);

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "jdj4vdusB/LEiZyosb/TUxeCDm9vzL8ObYilr1hanvo=", new byte[] { 128, 91, 131, 145, 75, 41, 183, 146, 61, 136, 60, 39, 221, 69, 138, 206 } });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsQrCodeReport",
                table: "Settings");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "l23T8UtFsS11j5aUNIhLI42wRGmHwEvqhwZDqie/k0g=", new byte[] { 155, 106, 183, 125, 117, 189, 122, 129, 250, 212, 240, 173, 248, 190, 195, 140 } });
        }
    }
}
