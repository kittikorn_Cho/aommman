﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class seedAgency : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Agency",
                columns: new[] { "Id", "Address", "AgencyTypeId", "Code", "CreatedBy", "CreatedDate", "DeletedBy", "DeletedDate", "Firstname", "Lastname", "Nickname", "Status", "Tel", "UpdatedBy", "UpdatedDate", "WorkFromDate", "WorkToDate" },
                values: new object[] { 1, null, 1, null, null, null, null, null, "ไม่ระบุ", null, null, null, null, null, null, null, null });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "qCg2K03ugJXYgek4SuPO0JOCdL0aOH8KGdOO/TPIDD0=", new byte[] { 230, 81, 158, 52, 176, 245, 146, 240, 128, 54, 145, 63, 244, 251, 124, 4 } });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Agency",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "fBSeK9SV1rUYCpNO/dDgi5jhTd4A4OPbsDygyqWhKg4=", new byte[] { 41, 66, 48, 157, 209, 132, 31, 14, 250, 20, 39, 233, 243, 62, 164, 86 } });
        }
    }
}
