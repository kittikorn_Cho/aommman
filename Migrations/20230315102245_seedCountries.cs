﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class seedCountries : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Countries",
                columns: new[] { "Id", "NameEn", "NameTh" },
                values: new object[,]
                {
                    { 1, "Not Specified", "ไม่ระบุ" },
                    { 161, "Namibia", "นามิเบีย" },
                    { 162, "New Caledonia", "นิวแคลิโดเนีย" },
                    { 163, "Niger", "ไนเธอร์" },
                    { 164, "Norfolk Island", "เกาะนอร์ฟอล์ก" },
                    { 165, "Nigeria", "ไนจีเรีย" },
                    { 166, "Nicaragua", "นิการากัว" },
                    { 167, "Niue", "นีอูเอ" },
                    { 168, "Netherlands", "เนเธอร์แลนด์" },
                    { 169, "Norway", "นอร์เวย์" },
                    { 170, "Nepal", "เนปาล" },
                    { 171, "Nauru", "นาอูรู" },
                    { 172, "New Zealand", "นิวซีแลนด์" },
                    { 173, "Oman", "โอมาน" },
                    { 174, "Pakistan", "ปากีสถาน" },
                    { 175, "Panama", "ปานามา" },
                    { 176, "Pitcairn Islands", "หมู่เกาะพิตแคร์น" },
                    { 177, "Peru", "เปรู" },
                    { 178, "Philippines", "ฟิลิปปินส์" },
                    { 179, "Palau", "ปาเลา" },
                    { 180, "Papua New Guinea", "ปาปัวนิวกินี" },
                    { 181, "Poland", "โปแลนด์" },
                    { 182, "Puerto Rico", "เปอร์โตริโก" },
                    { 183, "North Korea", "เกาหลีเหนือ" },
                    { 184, "Portugal", "โปรตุเกส" },
                    { 185, "Paraguay", "ปารากวัย" },
                    { 186, "Palestine", "ปาเลสไตน์" },
                    { 187, "French Polynesia", "เฟรนช์โปลินีเซีย" },
                    { 160, "Mayotte", "มายอต" },
                    { 159, "Malaysia", "มาเลเซีย" },
                    { 158, "Malawi", "มาลาวี" },
                    { 157, "Mauritius", "มอริเชียส" },
                    { 129, "Libya", "ลิบยา" },
                    { 130, "Saint Lucia", "เซนต์ลูเซีย" },
                    { 131, "Liechtenstein", "ลิกเตนสไตน์" },
                    { 132, "Sri Lanka", "ศรีลังกา" },
                    { 133, "Lesotho", "เลโซโท" },
                    { 134, "Lithuania", "ลิธัวเนีย" },
                    { 135, "Luxembourg", "ลักเซมเบิร์ก" },
                    { 136, "Latvia", "ลัตเวีย" },
                    { 137, "Macao", "มาเก๊า" },
                    { 138, "Saint Martin", "เซนต์มาร์ติน" },
                    { 139, "Morocco", "โมร็อกโก" },
                    { 140, "Monaco", "โมนาโก" },
                    { 141, "Moldova", "มอลโดวา" },
                    { 188, "Qatar", "กาตาร์" },
                    { 142, "Madagascar", "มาดากัสการ์" },
                    { 144, "Mexico", "เม็กซิโก" },
                    { 145, "Marshall Islands", "หมู่เกาะมาร์แชลล์" },
                    { 146, "Macedonia", "มาซิโดเนีย" },
                    { 147, "Mali", "มาลี" },
                    { 148, "Malta", "เกาะมอลตา" },
                    { 149, "Myanmar", "พม่า" },
                    { 150, "Montenegro", "มอนเตเนโก" },
                    { 151, "Mongolia", "มองโกเลีย" },
                    { 152, "Northern Mariana Islands", "หมู่เกาะนอร์เทิร์นมาเรียนา" },
                    { 153, "Mozambique", "โมซัมบิก" },
                    { 154, "Mauritania", "มอริเตเนีย" },
                    { 155, "Montserrat", "มอนต์เซอร์รัต" },
                    { 156, "Martinique", "มาร์ตินีก" },
                    { 143, "Maldives", "มัลดีฟส์" },
                    { 128, "Liberia", "ไลบีเรีย" },
                    { 189, "Réunion", "เรอูนียง" },
                    { 191, "Russia", "รัสเซีย" },
                    { 224, "East Timor", "ติมอร์ตะวันออก" },
                    { 225, "Tonga", "ตองกา" },
                    { 226, "Trinidad and Tobago", "ตรินิแดดและโตเบโก" },
                    { 227, "Tunisia", "ตูนิเซีย" },
                    { 228, "Turkey", "ตุรกี" },
                    { 229, "Tuvalu", "ตูวาลู" },
                    { 230, "Taiwan", "ไต้หวัน" },
                    { 231, "Tanzania", "แทนซาเนีย" },
                    { 232, "Uganda", "ยูกันดา" },
                    { 233, "Ukraine", "ยูเครน" },
                    { 234, "U.S. Minor Outlying Islands", "เกาะนอกรีตของสหรัฐฯ" },
                    { 235, "Uruguay", "อุรุกวัย" },
                    { 236, "United States", "สหรัฐอเมริกา" },
                    { 237, "Uzbekistan", "อุซเบกิสถาน" },
                    { 238, "Vatican City", "เมืองวาติกัน" },
                    { 239, "Saint Vincent and the Grenadines", "เซนต์วินเซนต์และเกรนาดีนส์" },
                    { 240, "Venezuela", "เวเนซุเอลา" },
                    { 241, "British Virgin Islands", "หมู่เกาะบริติชเวอร์จิน" },
                    { 242, "U.S. Virgin Islands", "หมู่เกาะเวอร์จินของสหรัฐอเมริกา" },
                    { 243, "Vietnam", "เวียดนาม" },
                    { 244, "Vanuatu", "วานูอาตู" },
                    { 245, "Wallis and Futuna", "วาลลิสและฟุตูนา" },
                    { 246, "Samoa", "ซามัว" },
                    { 247, "Kosovo", "โคโซโว" },
                    { 248, "Yemen", "เยเมน" },
                    { 249, "South Africa", "แอฟริกาใต้" },
                    { 250, "Zambia", "แซมเบีย" },
                    { 223, "Turkmenistan", "เติร์กเมนิสถาน" },
                    { 222, "Tokelau", "โตเกเลา" },
                    { 221, "Tajikistan", "ทาจิกิสถาน" },
                    { 220, "Thailand", "ไทย" },
                    { 192, "Rwanda", "รวันดา" },
                    { 193, "Saudi Arabia", "ซาอุดิอาราเบีย" },
                    { 194, "Sudan", "ซูดาน" },
                    { 195, "Senegal", "เซเนกัล" },
                    { 196, "Singapore", "สิงคโปร์" },
                    { 197, "South Georgia and the South Sandwich Islands", "หมู่เกาะเซาท์จอร์เจียและหมู่เกาะเซาท์แซนด์วิช" },
                    { 198, "Saint Helena", "เซนต์เฮเลนา" },
                    { 199, "Svalbard and Jan Mayen", "สฟาลบาร์และยานไมเอน" },
                    { 200, "Solomon Islands", "หมู่เกาะโซโลมอน" },
                    { 201, "Sierra Leone", "เซียร์ราลีโอน" },
                    { 202, "El Salvador", "เอลซัลวาดอร์" },
                    { 203, "San Marino", "ซานมาริโน" },
                    { 204, "Somalia", "โซมาเลีย" },
                    { 190, "Romania", "โรมาเนีย" },
                    { 205, "Saint Pierre and Miquelon", "เซนต์ปิแอร์และมีเกอลง" },
                    { 207, "South Sudan", "ซูดานใต้" },
                    { 208, "São Tomé and Príncipe", "เซาตูเมและปรินซิปี" },
                    { 209, "Suriname", "ซูรินาเม" },
                    { 210, "Slovakia", "สโลวะเกีย" },
                    { 211, "Slovenia", "สโลวีเนีย" },
                    { 212, "Sweden", "สวีเดน" },
                    { 213, "Swaziland", "สวาซิแลนด์" },
                    { 214, "Sint Maarten", "เกาะเซนต์มาร์ติน" },
                    { 215, "Seychelles", "เซเชลส์" },
                    { 216, "Syria", "ซีเรีย" },
                    { 217, "Turks and Caicos Islands", "หมู่เกาะเติกส์และหมู่เกาะเคคอส" },
                    { 218, "Chad", "ชาด" },
                    { 219, "Togo", "โตโก" },
                    { 206, "Serbia", "เซอร์เบีย" },
                    { 127, "Lebanon", "เลบานอน" },
                    { 126, "Laos", "ลาว" },
                    { 125, "Kuwait", "คูเวต" },
                    { 34, "Brazil", "บราซิล" },
                    { 35, "Barbados", "บาร์เบโดส" },
                    { 36, "Brunei", "บรูไน" },
                    { 37, "Bhutan", "ภูฏาน" },
                    { 38, "Bouvet Island", "เกาะบูเว็ต" },
                    { 39, "Botswana", "บอตสวานา" },
                    { 40, "Central African Republic", "สาธารณรัฐแอฟริกากลาง" },
                    { 41, "Canada", "แคนาดา" },
                    { 42, "Cocos [Keeling] Islands", "เกาะโคโคส [คีลิง]," },
                    { 43, "Switzerland", "สวิสเซอร์แลนด์" },
                    { 44, "Chile", "ชิลี" },
                    { 45, "China", "จีน" },
                    { 46, "Ivory Coast", "ไอวอรี่โคสต์" },
                    { 47, "Cameroon", "แคเมอรูน" },
                    { 48, "Democratic Republic of the Congo", "สาธารณรัฐประชาธิปไตยคองโก" },
                    { 49, "Republic of the Congo", "สาธารณรัฐคองโก" },
                    { 50, "Cook Islands", "หมู่เกาะคุก" },
                    { 51, "Colombia", "โคลอมเบีย" },
                    { 52, "Comoros", "คอโมโรส" },
                    { 53, "Cape Verde", "เคปเวิร์ด" },
                    { 54, "Costa Rica", "คอสตาริกา" },
                    { 55, "Cuba", "คิวบา" },
                    { 56, "Curacao", "คูราเซา" },
                    { 57, "Christmas Island", "เกาะคริสต์มาส" },
                    { 58, "Cayman Islands", "หมู่เกาะเคย์เเมน" },
                    { 59, "Cyprus", "ไซปรัส" },
                    { 60, "Czech Republic", "สาธารณรัฐเช็ก" },
                    { 33, "Bolivia", "โบลิเวีย" },
                    { 32, "Bermuda", "เบอร์มิวดา" },
                    { 31, "Belize", "เบลีซ" },
                    { 30, "Belarus", "เบลารุส" },
                    { 2, "Aruba", "อารูบา" },
                    { 3, "Afghanistan", "อัฟกานิสถาน" },
                    { 4, "Angola", "แองโกลา" },
                    { 5, "Anguilla", "แองกวิลลา" },
                    { 6, "Åland", "โอลันด์" },
                    { 7, "Albania", "แอลเบเนีย" },
                    { 8, "Andorra", "อันดอร์รา" },
                    { 9, "United Arab Emirates", "สหรัฐอาหรับเอมิเรตส์" },
                    { 10, "Argentina", "อาร์เจนตินา" },
                    { 11, "Armenia", "อาร์เมเนีย" },
                    { 12, "American Samoa", "อเมริกันซามัว" },
                    { 13, "Antarctica", "ทวิปแอนตาร์กติกา" },
                    { 14, "French Southern Territories", "ดินแดนทางตอนใต้ของฝรั่งเศส" },
                    { 61, "Germany", "เยอรมันนี" },
                    { 15, "Antigua and Barbuda", "แอนติกาและบาร์บูดา" },
                    { 17, "Austria", "ออสเตรีย" },
                    { 18, "Azerbaijan", "อาเซอร์ไบจาน" },
                    { 19, "Burundi", "บุรุนดี" },
                    { 20, "Belgium", "เบลเยียม" },
                    { 21, "Benin", "เบนิน" },
                    { 22, "Bonaire", "โบแนร์" },
                    { 23, "Burkina Faso", "บูร์กินาฟาโซ" },
                    { 24, "Bangladesh", "บังคลาเทศ" },
                    { 25, "Bulgaria", "บัลแกเรีย" },
                    { 26, "Bahrain", "บาห์เรน" },
                    { 27, "Bahamas", "บาฮามาส" },
                    { 28, "Bosnia and Herzegovina", "บอสเนียและเฮอร์เซโก" },
                    { 29, "Saint Barthélemy", "เซนต์บาร์เตเลมี" },
                    { 16, "Australia", "ออสเตรเลีย" },
                    { 62, "Djibouti", "จิบูตี" },
                    { 63, "Dominica", "โดมินิกา" },
                    { 64, "Denmark", "เดนมาร์ก" },
                    { 97, "Guyana", "กายอานา" },
                    { 98, "Hong Kong", "ฮ่องกง" },
                    { 99, "Heard Island and McDonald Islands", "เกาะเฮิร์ดและหมู่เกาะแมคโดนัลด์" },
                    { 100, "Honduras", "ฮอนดูรัส" },
                    { 101, "Croatia", "โครเอเชีย" },
                    { 102, "Haiti", "เฮติ" },
                    { 103, "Hungary", "ฮังการี" },
                    { 104, "Indonesia", "อินโดนีเซีย" },
                    { 105, "Isle of Man", "เกาะแมน" },
                    { 106, "India", "อินเดีย" },
                    { 107, "British Indian Ocean Territory", "หมู่เกาะบริติชเวอร์จิน" },
                    { 108, "Ireland", "ไอร์แลนด์" },
                    { 109, "Iran", "อิหร่าน" },
                    { 96, "Guam", "เกาะกวม" },
                    { 110, "Iraq", "อิรัก" },
                    { 112, "Israel", "อิสราเอล" },
                    { 113, "Italy", "อิตาลี" },
                    { 114, "Jamaica", "เกาะจาเมกา" },
                    { 115, "Jersey", "นิวเจอร์ซีย์" },
                    { 116, "Jordan", "จอร์แดน" },
                    { 117, "Japan", "ญี่ปุ่น" },
                    { 118, "Kazakhstan", "คาซัคสถาน" },
                    { 119, "Kenya", "เคนย่า" },
                    { 120, "Kyrgyzstan", "คีร์กีสถาน" },
                    { 121, "Cambodia", "กัมพูชา" },
                    { 122, "Kiribati", "คิริบาส" },
                    { 123, "Saint Kitts and Nevis", "เซนต์คิตส์และเนวิส" },
                    { 124, "South Korea", "เกาหลีใต้" },
                    { 111, "Israel", "ไอซ์แลนด์" },
                    { 251, "Zimbabwe", "ซิมบับเว" },
                    { 95, "French Guiana", "เฟรนช์เกียนา" },
                    { 93, "Greenland", "กรีนแลนด์" },
                    { 65, "Dominican Republic", "สาธารณรัฐโดมินิกัน" },
                    { 66, "Algeria", "แอลจีเรีย" },
                    { 67, "Ecuador", "เอกวาดอร์" },
                    { 68, "Egypt", "อียิปต์" },
                    { 69, "Eritrea", "เอริเทรี" },
                    { 70, "Western Sahara", "ซาฮาร่าตะวันตก" },
                    { 71, "Spain", "สเปน" },
                    { 72, "Estonia", "เอสโตเนีย" },
                    { 73, "Ethiopia", "สาธารณรัฐเอธิโอเปีย" },
                    { 74, "Finland", "ฟินแลนด์" },
                    { 75, "Fiji", "ฟิจิ" },
                    { 76, "Falkland Islands", "หมู่เกาะฟอล์คแลนด์" },
                    { 77, "France", "ฝรั่งเศส" },
                    { 94, "Guatemala", "กัวเตมาลา" },
                    { 78, "Faroe Islands", "หมู่เกาะแฟโร" },
                    { 80, "Gabon", "กาบอง" },
                    { 81, "United Kingdom", "อังกฤษ(สหราชอาณาจักร) " },
                    { 82, "Georgia", "จอร์เจีย" },
                    { 83, "Guernsey", "เกิร์นซีย์" },
                    { 84, "Ghana", "กานา" },
                    { 85, "Gibraltar", "ยิบรอลตา" },
                    { 86, "Guinea", "กินี" },
                    { 87, "Guadeloupe", "กัวเดลุฟ" },
                    { 88, "Gambia", "แกมเบีย" },
                    { 89, "Guinea-Bissau", "กินีบิสเซา" },
                    { 90, "Equatorial Guinea", "อิเควทอเรียลกินี" },
                    { 91, "Greece", "กรีซ" },
                    { 92, "Grenada", "เกรเนดา" },
                    { 79, "Micronesia", "ไมโครนีเซีย" }
                });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "QBYXYVyPAFdEVDKSztbS1+BuHfqKf77rLpzyJfUWz/E=", new byte[] { 200, 26, 51, 169, 97, 70, 44, 58, 30, 206, 186, 205, 129, 167, 182, 198 } });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 21);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 22);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 23);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 24);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 25);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 26);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 27);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 28);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 29);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 30);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 31);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 32);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 33);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 34);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 35);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 36);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 37);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 38);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 39);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 40);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 41);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 42);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 43);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 44);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 45);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 46);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 47);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 48);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 49);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 50);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 51);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 52);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 53);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 54);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 55);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 56);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 57);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 58);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 59);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 60);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 61);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 62);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 63);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 64);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 65);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 66);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 67);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 68);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 69);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 70);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 71);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 72);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 73);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 74);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 75);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 76);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 77);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 78);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 79);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 80);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 81);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 82);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 83);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 84);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 85);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 86);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 87);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 88);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 89);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 90);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 91);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 92);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 93);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 94);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 95);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 96);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 97);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 98);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 99);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 100);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 101);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 102);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 103);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 104);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 105);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 106);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 107);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 108);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 109);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 110);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 111);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 112);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 113);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 114);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 115);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 116);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 117);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 118);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 119);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 120);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 121);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 122);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 123);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 124);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 125);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 126);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 127);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 128);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 129);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 130);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 131);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 132);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 133);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 134);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 135);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 136);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 137);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 138);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 139);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 140);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 141);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 142);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 143);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 144);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 145);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 146);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 147);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 148);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 149);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 150);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 151);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 152);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 153);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 154);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 155);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 156);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 157);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 158);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 159);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 160);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 161);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 162);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 163);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 164);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 165);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 166);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 167);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 168);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 169);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 170);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 171);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 172);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 173);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 174);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 175);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 176);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 177);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 178);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 179);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 180);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 181);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 182);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 183);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 184);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 185);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 186);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 187);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 188);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 189);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 190);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 191);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 192);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 193);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 194);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 195);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 196);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 197);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 198);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 199);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 200);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 201);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 202);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 203);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 204);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 205);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 206);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 207);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 208);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 209);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 210);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 211);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 212);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 213);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 214);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 215);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 216);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 217);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 218);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 219);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 220);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 221);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 222);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 223);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 224);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 225);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 226);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 227);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 228);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 229);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 230);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 231);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 232);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 233);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 234);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 235);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 236);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 237);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 238);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 239);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 240);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 241);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 242);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 243);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 244);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 245);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 246);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 247);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 248);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 249);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 250);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: 251);

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "fOl6atVB9M/s2dLjRNhByX1t6fgH0SES3T47cF5lbwQ=", new byte[] { 141, 232, 23, 157, 8, 215, 67, 71, 235, 155, 48, 63, 109, 16, 41, 75 } });
        }
    }
}
