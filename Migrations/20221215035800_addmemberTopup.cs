﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class addmemberTopup : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_memberItemTransactions_Items_ItemId",
                table: "memberItemTransactions");

            migrationBuilder.DropForeignKey(
                name: "FK_memberItemTransactions_Members_MemberId",
                table: "memberItemTransactions");

            migrationBuilder.DropForeignKey(
                name: "FK_memberItemTransactions_MemberTransactions_MemberTransactionId",
                table: "memberItemTransactions");

            migrationBuilder.DropPrimaryKey(
                name: "PK_memberItemTransactions",
                table: "memberItemTransactions");

            migrationBuilder.RenameTable(
                name: "memberItemTransactions",
                newName: "MemberItemTransactions");

            migrationBuilder.RenameIndex(
                name: "IX_memberItemTransactions_MemberTransactionId",
                table: "MemberItemTransactions",
                newName: "IX_MemberItemTransactions_MemberTransactionId");

            migrationBuilder.RenameIndex(
                name: "IX_memberItemTransactions_MemberId",
                table: "MemberItemTransactions",
                newName: "IX_MemberItemTransactions_MemberId");

            migrationBuilder.RenameIndex(
                name: "IX_memberItemTransactions_ItemId",
                table: "MemberItemTransactions",
                newName: "IX_MemberItemTransactions_ItemId");

            migrationBuilder.AddColumn<int>(
                name: "ItemId",
                table: "memberPayments",
                type: "int",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_MemberItemTransactions",
                table: "MemberItemTransactions",
                column: "Id");

            migrationBuilder.CreateTable(
                name: "memberTopups",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    MemberId = table.Column<int>(type: "int", nullable: false),
                    CalendarId = table.Column<int>(type: "int", nullable: false),
                    CerditAmount = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    Name_1 = table.Column<int>(type: "int", nullable: false),
                    Amount_1 = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    Name_2 = table.Column<int>(type: "int", nullable: false),
                    Amount_2 = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    Name_3 = table.Column<int>(type: "int", nullable: false),
                    Amount_3 = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    Name_4 = table.Column<int>(type: "int", nullable: false),
                    Amount_4 = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    Name_5 = table.Column<int>(type: "int", nullable: false),
                    Amount_5 = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    ItemId = table.Column<int>(type: "int", nullable: true),
                    CreatedBy = table.Column<int>(type: "int", nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    UpdatedBy = table.Column<int>(type: "int", nullable: true),
                    UpdatedDate = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    DeletedBy = table.Column<int>(type: "int", nullable: true),
                    DeletedDate = table.Column<DateTime>(type: "datetime(6)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_memberTopups", x => x.Id);
                    table.ForeignKey(
                        name: "FK_memberTopups_Items_ItemId",
                        column: x => x.ItemId,
                        principalTable: "Items",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_memberTopups_Members_MemberId",
                        column: x => x.MemberId,
                        principalTable: "Members",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "3lF3KJZRkVHGPY+3ImG+4W7+KCWcZRWxoxJuxAXTxzE=", new byte[] { 126, 98, 7, 182, 183, 253, 40, 147, 142, 244, 24, 135, 179, 146, 21, 183 } });

            migrationBuilder.CreateIndex(
                name: "IX_memberPayments_ItemId",
                table: "memberPayments",
                column: "ItemId");

            migrationBuilder.CreateIndex(
                name: "IX_memberTopups_ItemId",
                table: "memberTopups",
                column: "ItemId");

            migrationBuilder.CreateIndex(
                name: "IX_memberTopups_MemberId",
                table: "memberTopups",
                column: "MemberId");

            migrationBuilder.AddForeignKey(
                name: "FK_MemberItemTransactions_Items_ItemId",
                table: "MemberItemTransactions",
                column: "ItemId",
                principalTable: "Items",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_MemberItemTransactions_Members_MemberId",
                table: "MemberItemTransactions",
                column: "MemberId",
                principalTable: "Members",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_MemberItemTransactions_MemberTransactions_MemberTransactionId",
                table: "MemberItemTransactions",
                column: "MemberTransactionId",
                principalTable: "MemberTransactions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_memberPayments_Items_ItemId",
                table: "memberPayments",
                column: "ItemId",
                principalTable: "Items",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MemberItemTransactions_Items_ItemId",
                table: "MemberItemTransactions");

            migrationBuilder.DropForeignKey(
                name: "FK_MemberItemTransactions_Members_MemberId",
                table: "MemberItemTransactions");

            migrationBuilder.DropForeignKey(
                name: "FK_MemberItemTransactions_MemberTransactions_MemberTransactionId",
                table: "MemberItemTransactions");

            migrationBuilder.DropForeignKey(
                name: "FK_memberPayments_Items_ItemId",
                table: "memberPayments");

            migrationBuilder.DropTable(
                name: "memberTopups");

            migrationBuilder.DropIndex(
                name: "IX_memberPayments_ItemId",
                table: "memberPayments");

            migrationBuilder.DropPrimaryKey(
                name: "PK_MemberItemTransactions",
                table: "MemberItemTransactions");

            migrationBuilder.DropColumn(
                name: "ItemId",
                table: "memberPayments");

            migrationBuilder.RenameTable(
                name: "MemberItemTransactions",
                newName: "memberItemTransactions");

            migrationBuilder.RenameIndex(
                name: "IX_MemberItemTransactions_MemberTransactionId",
                table: "memberItemTransactions",
                newName: "IX_memberItemTransactions_MemberTransactionId");

            migrationBuilder.RenameIndex(
                name: "IX_MemberItemTransactions_MemberId",
                table: "memberItemTransactions",
                newName: "IX_memberItemTransactions_MemberId");

            migrationBuilder.RenameIndex(
                name: "IX_MemberItemTransactions_ItemId",
                table: "memberItemTransactions",
                newName: "IX_memberItemTransactions_ItemId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_memberItemTransactions",
                table: "memberItemTransactions",
                column: "Id");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "99Cjs4S1T7WDs3eTFRZmeU2IccnvdOr+t+zmZzRloPQ=", new byte[] { 220, 90, 57, 163, 235, 151, 195, 61, 111, 23, 201, 158, 109, 234, 84, 181 } });

            migrationBuilder.AddForeignKey(
                name: "FK_memberItemTransactions_Items_ItemId",
                table: "memberItemTransactions",
                column: "ItemId",
                principalTable: "Items",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_memberItemTransactions_Members_MemberId",
                table: "memberItemTransactions",
                column: "MemberId",
                principalTable: "Members",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_memberItemTransactions_MemberTransactions_MemberTransactionId",
                table: "memberItemTransactions",
                column: "MemberTransactionId",
                principalTable: "MemberTransactions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
