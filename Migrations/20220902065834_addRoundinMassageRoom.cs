﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class addRoundinMassageRoom : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Round",
                table: "MassageRooms",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "KVqnHn5nwWJO4wJaGEd7OhcqWXbgBWgCC89vVwENq+A=", new byte[] { 226, 253, 217, 10, 33, 3, 99, 246, 24, 54, 209, 196, 120, 59, 191, 197 } });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Round",
                table: "MassageRooms");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "NYR2wam/MnKWquL+Fme5IrFxwLWJaqDLmvYWNwawKWU=", new byte[] { 42, 192, 126, 255, 139, 121, 157, 237, 230, 36, 59, 48, 164, 88, 243, 174 } });
        }
    }
}
