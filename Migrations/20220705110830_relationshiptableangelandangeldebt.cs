﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class relationshiptableangelandangeldebt : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<decimal>(
                name: "Fee",
                table: "AngelDebts",
                type: "decimal(18,2)",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(65,30)");

            migrationBuilder.CreateIndex(
                name: "IX_AngelPerformances_AngelId",
                table: "AngelPerformances",
                column: "AngelId");

            migrationBuilder.AddForeignKey(
                name: "FK_AngelPerformances_Angels_AngelId",
                table: "AngelPerformances",
                column: "AngelId",
                principalTable: "Angels",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AngelPerformances_Angels_AngelId",
                table: "AngelPerformances");

            migrationBuilder.DropIndex(
                name: "IX_AngelPerformances_AngelId",
                table: "AngelPerformances");

            migrationBuilder.AlterColumn<decimal>(
                name: "Fee",
                table: "AngelDebts",
                type: "decimal(65,30)",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,2)");
        }
    }
}
