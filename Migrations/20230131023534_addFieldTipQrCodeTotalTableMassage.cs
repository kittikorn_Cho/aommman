﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class addFieldTipQrCodeTotalTableMassage : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "TipQrCodeTotal",
                table: "Massages",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "l2Ab/9XPAIM70A1u1pucfD9/+C273dygvXl0P4hCoPw=", new byte[] { 249, 21, 51, 99, 192, 140, 47, 220, 173, 190, 20, 255, 131, 14, 45, 120 } });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TipQrCodeTotal",
                table: "Massages");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "YqU6V9jVYy1G+aBGYoTPevU7eviEncLDUnl3PzuxUKk=", new byte[] { 184, 245, 76, 43, 10, 90, 162, 105, 104, 34, 141, 129, 189, 18, 92, 164 } });
        }
    }
}
