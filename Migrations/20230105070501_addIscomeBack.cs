﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class addIscomeBack : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "GetOutTimeOlds",
                table: "AngelWorkings",
                type: "datetime(6)",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsComebacks",
                table: "AngelWorkings",
                type: "tinyint(1)",
                nullable: false,
                defaultValue: false);

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "WJgMcFbQguLUrpqc4jAfsOr1IIBzP66jmO90JwgDPUo=", new byte[] { 43, 252, 153, 28, 192, 84, 84, 18, 226, 219, 26, 74, 210, 164, 84, 127 } });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "GetOutTimeOlds",
                table: "AngelWorkings");

            migrationBuilder.DropColumn(
                name: "IsComebacks",
                table: "AngelWorkings");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "vH9j97SojyIffK2iqDEKdAVN9YTFLAk4CXjmUuByyaw=", new byte[] { 139, 171, 41, 131, 171, 98, 15, 141, 239, 216, 203, 25, 8, 57, 36, 130 } });
        }
    }
}
