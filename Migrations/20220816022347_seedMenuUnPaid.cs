﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class seedMenuUnPaid : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Menus",
                columns: new[] { "Id", "Code", "CreatedBy", "CreatedDate", "DeletedBy", "DeletedDate", "Name", "Slug", "UpdatedBy", "UpdatedDate" },
                values: new object[] { 22, "UnPaid", null, null, null, null, "ค้างชำระ", "", null, null });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "iFAGvlBAPkw1ZLRCTPfmmkui9RTDaTwy/0+wVyxYLeY=", new byte[] { 122, 224, 241, 210, 193, 233, 225, 212, 148, 73, 93, 164, 89, 217, 1, 80 } });

            migrationBuilder.InsertData(
                table: "RoleMenu",
                columns: new[] { "MenuId", "RoleId" },
                values: new object[] { 22, 1 });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "RoleMenu",
                keyColumns: new[] { "MenuId", "RoleId" },
                keyValues: new object[] { 22, 1 });

            migrationBuilder.DeleteData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 22);

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "UYfIUOWXjIJ6GlPS99brwcHXQpvHACw2ot3F8abgtpM=", new byte[] { 218, 163, 21, 98, 171, 126, 68, 222, 113, 125, 184, 3, 173, 179, 65, 242 } });
        }
    }
}
