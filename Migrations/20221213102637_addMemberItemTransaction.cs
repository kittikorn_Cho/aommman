﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class addMemberItemTransaction : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "memberItemTransactions",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    MemberId = table.Column<int>(type: "int", nullable: false),
                    ItemId = table.Column<int>(type: "int", nullable: false),
                    MemberTransactionId = table.Column<int>(type: "int", nullable: false),
                    Amount = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    DeletedDate = table.Column<DateTime>(type: "datetime(6)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_memberItemTransactions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_memberItemTransactions_Items_ItemId",
                        column: x => x.ItemId,
                        principalTable: "Items",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_memberItemTransactions_Members_MemberId",
                        column: x => x.MemberId,
                        principalTable: "Members",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_memberItemTransactions_MemberTransactions_MemberTransactionId",
                        column: x => x.MemberTransactionId,
                        principalTable: "MemberTransactions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "VkeGOIpmcQE1+nt5CldMmlA9edBoPxn1V5KlOe41kYg=", new byte[] { 118, 45, 110, 52, 135, 191, 160, 142, 123, 225, 63, 48, 208, 72, 171, 159 } });

            migrationBuilder.CreateIndex(
                name: "IX_memberItemTransactions_ItemId",
                table: "memberItemTransactions",
                column: "ItemId");

            migrationBuilder.CreateIndex(
                name: "IX_memberItemTransactions_MemberId",
                table: "memberItemTransactions",
                column: "MemberId");

            migrationBuilder.CreateIndex(
                name: "IX_memberItemTransactions_MemberTransactionId",
                table: "memberItemTransactions",
                column: "MemberTransactionId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "memberItemTransactions");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "L0qleGLpaHgbP5Fvllr4ud7u6XWrn1SIZ70RaIrPLis=", new byte[] { 186, 143, 56, 234, 106, 44, 199, 100, 100, 137, 238, 158, 1, 175, 69, 221 } });
        }
    }
}
