﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class AddRoomTypeSeeder : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "RoomTypes",
                columns: new[] { "Id", "Code", "CreatedBy", "CreatedDate", "DeletedBy", "DeletedDate", "Name", "RoomRate", "RoomRateNext", "RoundTime", "UpdatedBy", "UpdatedDate" },
                values: new object[,]
                {
                    { 1, "SU", null, null, null, null, "สูท", 0m, 0m, 0, null, null },
                    { 2, "VIP", null, null, null, null, "วีไอพี", 0m, 0m, 0, null, null },
                    { 3, "SS", null, null, null, null, "สูทย่อย", 0m, 0m, 0, null, null },
                    { 4, "NORMAL", null, null, null, null, "ธรรมดา", 0m, 0m, 0, null, null }
                });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "SKw6AdBrhdLgcvDcssLaNHerxkeop/DspEj33IwjyEI=", new byte[] { 244, 255, 38, 250, 140, 72, 204, 231, 186, 116, 175, 24, 23, 142, 206, 33 } });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "RoomTypes",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "RoomTypes",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "RoomTypes",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "RoomTypes",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "sOhd0dpnZ+u3ZB6AQLeXFmBULzyPvYKuABOVHoPc6iY=", new byte[] { 9, 116, 28, 243, 97, 157, 216, 38, 133, 201, 15, 193, 110, 251, 246, 62 } });
        }
    }
}
