﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class addIscountdaily : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsCuontDaily",
                table: "Settings",
                type: "tinyint(1)",
                nullable: false,
                defaultValue: false);

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "R1qiaGzX8sCUikHW/4TaCzc92pUzd5DB13AkrPc8Vus=", new byte[] { 126, 141, 238, 61, 151, 76, 237, 199, 105, 218, 195, 51, 131, 41, 204, 195 } });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsCuontDaily",
                table: "Settings");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "j10XaFlxYRnmpR8gTA240qsHhtU0xNal3690EWkf15Y=", new byte[] { 197, 69, 131, 60, 80, 184, 64, 105, 2, 228, 230, 217, 78, 32, 86, 21 } });
        }
    }
}
