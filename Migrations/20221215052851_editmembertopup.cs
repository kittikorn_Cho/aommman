﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class editmembertopup : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Name_5",
                table: "memberTopups",
                type: "longtext",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int")
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AlterColumn<string>(
                name: "Name_4",
                table: "memberTopups",
                type: "longtext",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int")
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AlterColumn<string>(
                name: "Name_3",
                table: "memberTopups",
                type: "longtext",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int")
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AlterColumn<string>(
                name: "Name_2",
                table: "memberTopups",
                type: "longtext",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int")
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AlterColumn<string>(
                name: "Name_1",
                table: "memberTopups",
                type: "longtext",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int")
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "Suq/oal7QvgtrZlCf54BbmK6Jf4Yhbn7V/n536Tn05g=", new byte[] { 67, 163, 101, 254, 164, 51, 4, 34, 108, 136, 15, 66, 255, 228, 233, 235 } });

            migrationBuilder.CreateIndex(
                name: "IX_memberTopups_CalendarId",
                table: "memberTopups",
                column: "CalendarId");

            migrationBuilder.AddForeignKey(
                name: "FK_memberTopups_CalendarDays_CalendarId",
                table: "memberTopups",
                column: "CalendarId",
                principalTable: "CalendarDays",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_memberTopups_CalendarDays_CalendarId",
                table: "memberTopups");

            migrationBuilder.DropIndex(
                name: "IX_memberTopups_CalendarId",
                table: "memberTopups");

            migrationBuilder.AlterColumn<int>(
                name: "Name_5",
                table: "memberTopups",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(string),
                oldType: "longtext",
                oldNullable: true)
                .OldAnnotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AlterColumn<int>(
                name: "Name_4",
                table: "memberTopups",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(string),
                oldType: "longtext",
                oldNullable: true)
                .OldAnnotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AlterColumn<int>(
                name: "Name_3",
                table: "memberTopups",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(string),
                oldType: "longtext",
                oldNullable: true)
                .OldAnnotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AlterColumn<int>(
                name: "Name_2",
                table: "memberTopups",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(string),
                oldType: "longtext",
                oldNullable: true)
                .OldAnnotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AlterColumn<int>(
                name: "Name_1",
                table: "memberTopups",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(string),
                oldType: "longtext",
                oldNullable: true)
                .OldAnnotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "3lF3KJZRkVHGPY+3ImG+4W7+KCWcZRWxoxJuxAXTxzE=", new byte[] { 126, 98, 7, 182, 183, 253, 40, 147, 142, 244, 24, 135, 179, 146, 21, 183 } });
        }
    }
}
