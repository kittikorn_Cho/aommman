﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class AddColumnMemberTypeIdAndAmountTableMemberTransaction : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Amount",
                table: "MemberTransactions",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "MemberTypeId",
                table: "MemberTransactions",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_MemberTransactions_MemberTypeId",
                table: "MemberTransactions",
                column: "MemberTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_MemberTransactions_MemberTypes_MemberTypeId",
                table: "MemberTransactions",
                column: "MemberTypeId",
                principalTable: "MemberTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MemberTransactions_MemberTypes_MemberTypeId",
                table: "MemberTransactions");

            migrationBuilder.DropIndex(
                name: "IX_MemberTransactions_MemberTypeId",
                table: "MemberTransactions");

            migrationBuilder.DropColumn(
                name: "Amount",
                table: "MemberTransactions");

            migrationBuilder.DropColumn(
                name: "MemberTypeId",
                table: "MemberTransactions");
        }
    }
}
