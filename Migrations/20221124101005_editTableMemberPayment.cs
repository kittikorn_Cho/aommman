﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class editTableMemberPayment : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Amount",
                table: "memberPayments",
                newName: "AmountWhisky3");

            migrationBuilder.AddColumn<decimal>(
                name: "AmountWhisky",
                table: "memberPayments",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "AmountWhisky2",
                table: "memberPayments",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "Epv7qOz05Q8Z8d1R8qsQK2TCeyxw9mgiMWq8XX6dG+M=", new byte[] { 135, 27, 151, 53, 223, 39, 222, 96, 188, 147, 156, 5, 153, 133, 15, 133 } });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AmountWhisky",
                table: "memberPayments");

            migrationBuilder.DropColumn(
                name: "AmountWhisky2",
                table: "memberPayments");

            migrationBuilder.RenameColumn(
                name: "AmountWhisky3",
                table: "memberPayments",
                newName: "Amount");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "KiTniVz2helBySZdVZlArsnetTvG1Ux5ZW5CRl+LNq8=", new byte[] { 137, 123, 6, 214, 23, 157, 42, 166, 136, 199, 213, 10, 40, 58, 10, 155 } });
        }
    }
}
