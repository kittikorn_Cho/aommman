﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class addfeildEndDate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "EndDate",
                table: "Bookings",
                type: "datetime(6)",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "EndDate",
                table: "BookingAngels",
                type: "datetime(6)",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "COg3rH7m/hXc4ek4xGsLocSVDzIQcwNxSD3AgLyZCBw=", new byte[] { 105, 65, 216, 197, 54, 169, 75, 111, 217, 4, 209, 200, 52, 10, 115, 229 } });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EndDate",
                table: "Bookings");

            migrationBuilder.DropColumn(
                name: "EndDate",
                table: "BookingAngels");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "D6TCObEHng1IPzq0hjZm1S9h3vV59Vy1R7AKSjypZZ0=", new byte[] { 32, 20, 180, 60, 199, 236, 30, 22, 84, 101, 64, 197, 237, 83, 211, 133 } });
        }
    }
}
