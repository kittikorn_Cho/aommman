﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class addSettingIsPoint : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsPoint",
                table: "Settings",
                type: "tinyint(1)",
                nullable: false,
                defaultValue: false);

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "Qb3V2/pvEzUNegeGWCWFbTv7S2b8xCmhwf7IbRcIGtA=", new byte[] { 68, 212, 17, 195, 75, 83, 233, 17, 87, 49, 233, 52, 91, 92, 70, 33 } });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsPoint",
                table: "Settings");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "x6xMoIQk/2vdL8Hs/KafM+Fl6qpk3jcBZn9O1oql+NY=", new byte[] { 27, 21, 92, 236, 135, 45, 244, 197, 60, 30, 24, 242, 199, 120, 228, 151 } });
        }
    }
}
