﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class addMenuCheckInLater : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Menus",
                columns: new[] { "Id", "Code", "CreatedBy", "CreatedDate", "DeletedBy", "DeletedDate", "Name", "Slug", "UpdatedBy", "UpdatedDate" },
                values: new object[] { 65, "CheckInLater", null, null, null, null, "เช็คอินย้อนหลัง", "", null, null });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "NOt1y4lriytkSXbo2m93DD/bojZ8uWJ00IOvZCZbYh0=", new byte[] { 197, 149, 243, 210, 104, 240, 92, 24, 47, 3, 111, 224, 58, 254, 124, 197 } });

            migrationBuilder.InsertData(
                table: "RoleMenu",
                columns: new[] { "MenuId", "RoleId" },
                values: new object[] { 65, 1 });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "RoleMenu",
                keyColumns: new[] { "MenuId", "RoleId" },
                keyValues: new object[] { 65, 1 });

            migrationBuilder.DeleteData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 65);

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "4SWHPqOD5MOAEItoURroopMwEMLufKmWmJU3OH3icvA=", new byte[] { 179, 111, 238, 197, 134, 3, 16, 19, 154, 120, 32, 153, 156, 210, 61, 71 } });
        }
    }
}
