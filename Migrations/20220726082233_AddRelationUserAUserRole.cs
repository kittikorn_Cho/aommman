﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class AddRelationUserAUserRole : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UserRole_Users_RoleId",
                table: "UserRole");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "VIyY2Wm0uCIKCvqphrAME6KnOuac6NgcTZlWZXjQgqo=", new byte[] { 5, 47, 58, 32, 1, 114, 30, 19, 155, 92, 225, 217, 115, 156, 203, 225 } });

            migrationBuilder.AddForeignKey(
                name: "FK_UserRole_Users_UserId",
                table: "UserRole",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UserRole_Users_UserId",
                table: "UserRole");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "otwl8qPUtZ/thMFeESsD+ZLuHm1ccqwuWfMi+BiI6vs=", new byte[] { 212, 12, 187, 195, 226, 70, 107, 12, 131, 236, 249, 36, 1, 107, 209, 88 } });

            migrationBuilder.AddForeignKey(
                name: "FK_UserRole_Users_RoleId",
                table: "UserRole",
                column: "RoleId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
