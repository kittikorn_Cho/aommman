﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class addTableMemberPayments : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_memberPayments_Items_ItemId",
                table: "memberPayments");

            migrationBuilder.DropIndex(
                name: "IX_memberPayments_ItemId",
                table: "memberPayments");

            migrationBuilder.DropColumn(
                name: "ItemId",
                table: "memberPayments");

            migrationBuilder.RenameColumn(
                name: "AmountWhisky3",
                table: "memberPayments",
                newName: "Amount_5");

            migrationBuilder.RenameColumn(
                name: "AmountWhisky2",
                table: "memberPayments",
                newName: "Amount_4");

            migrationBuilder.RenameColumn(
                name: "AmountWhisky",
                table: "memberPayments",
                newName: "Amount_3");

            migrationBuilder.AddColumn<decimal>(
                name: "Amount_1",
                table: "memberPayments",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "Amount_2",
                table: "memberPayments",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<string>(
                name: "Name_1",
                table: "memberPayments",
                type: "longtext",
                nullable: true)
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AddColumn<string>(
                name: "Name_2",
                table: "memberPayments",
                type: "longtext",
                nullable: true)
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AddColumn<string>(
                name: "Name_3",
                table: "memberPayments",
                type: "longtext",
                nullable: true)
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AddColumn<string>(
                name: "Name_4",
                table: "memberPayments",
                type: "longtext",
                nullable: true)
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AddColumn<string>(
                name: "Name_5",
                table: "memberPayments",
                type: "longtext",
                nullable: true)
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "MiIPVFLI+eeg44f6q5sQtdBaotogpWOoT2kbJA+aOys=", new byte[] { 31, 199, 93, 37, 177, 167, 44, 162, 168, 22, 117, 119, 97, 196, 81, 31 } });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Amount_1",
                table: "memberPayments");

            migrationBuilder.DropColumn(
                name: "Amount_2",
                table: "memberPayments");

            migrationBuilder.DropColumn(
                name: "Name_1",
                table: "memberPayments");

            migrationBuilder.DropColumn(
                name: "Name_2",
                table: "memberPayments");

            migrationBuilder.DropColumn(
                name: "Name_3",
                table: "memberPayments");

            migrationBuilder.DropColumn(
                name: "Name_4",
                table: "memberPayments");

            migrationBuilder.DropColumn(
                name: "Name_5",
                table: "memberPayments");

            migrationBuilder.RenameColumn(
                name: "Amount_5",
                table: "memberPayments",
                newName: "AmountWhisky3");

            migrationBuilder.RenameColumn(
                name: "Amount_4",
                table: "memberPayments",
                newName: "AmountWhisky2");

            migrationBuilder.RenameColumn(
                name: "Amount_3",
                table: "memberPayments",
                newName: "AmountWhisky");

            migrationBuilder.AddColumn<int>(
                name: "ItemId",
                table: "memberPayments",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "Epv7qOz05Q8Z8d1R8qsQK2TCeyxw9mgiMWq8XX6dG+M=", new byte[] { 135, 27, 151, 53, 223, 39, 222, 96, 188, 147, 156, 5, 153, 133, 15, 133 } });

            migrationBuilder.CreateIndex(
                name: "IX_memberPayments_ItemId",
                table: "memberPayments",
                column: "ItemId");

            migrationBuilder.AddForeignKey(
                name: "FK_memberPayments_Items_ItemId",
                table: "memberPayments",
                column: "ItemId",
                principalTable: "Items",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
