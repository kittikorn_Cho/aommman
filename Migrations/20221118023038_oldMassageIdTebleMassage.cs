﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class oldMassageIdTebleMassage : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "OldMassageId",
                table: "Massages",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "dbnMK9J3iKZWWwOvinz3h+hgILlbGx8YDHzLKX8szPU=", new byte[] { 75, 120, 36, 247, 123, 107, 114, 147, 84, 34, 5, 178, 143, 87, 205, 151 } });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "OldMassageId",
                table: "Massages");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "gEGMD8ceMIf8pzKypCPZOQP0T0f2JIlYPgyFoayEhEI=", new byte[] { 165, 48, 16, 160, 94, 123, 102, 255, 165, 163, 4, 83, 109, 204, 138, 128 } });
        }
    }
}
