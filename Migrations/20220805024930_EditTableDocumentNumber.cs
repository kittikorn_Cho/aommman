﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class EditTableDocumentNumber : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Type",
                table: "DocumentNumbers");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "sAkg3uTCmmPVo6F6jh9cJvCKsKd8WjAfVn7+FXzlTEU=", new byte[] { 250, 86, 198, 169, 190, 31, 54, 41, 165, 203, 192, 185, 78, 246, 220, 243 } });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Type",
                table: "DocumentNumbers",
                type: "longtext",
                nullable: true)
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "THT70k73jagmBPUywFBIYR4Izd667v7xtGq+i2i7Lo0=", new byte[] { 46, 180, 216, 44, 134, 187, 253, 218, 207, 237, 219, 13, 102, 174, 134, 181 } });
        }
    }
}
