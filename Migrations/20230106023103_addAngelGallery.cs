﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class addAngelGallery : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AngelGallerys",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    AngelId = table.Column<int>(type: "int", nullable: false),
                    Path = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    CreatedBy = table.Column<int>(type: "int", nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    UpdatedBy = table.Column<int>(type: "int", nullable: true),
                    UpdatedDate = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    DeletedBy = table.Column<int>(type: "int", nullable: true),
                    DeletedDate = table.Column<DateTime>(type: "datetime(6)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AngelGallerys", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AngelGallerys_Angels_AngelId",
                        column: x => x.AngelId,
                        principalTable: "Angels",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "aAQ48O+KruQw4gDcG+k21TrU3dzhIwqcAq0gZlDyWxI=", new byte[] { 141, 5, 164, 216, 81, 51, 251, 121, 110, 81, 13, 137, 103, 6, 147, 91 } });

            migrationBuilder.CreateIndex(
                name: "IX_AngelGallerys_AngelId",
                table: "AngelGallerys",
                column: "AngelId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AngelGallerys");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "okMNED71nmtB5ErlUfEwM4DBxqAJ6ClIT85OwMr3kFA=", new byte[] { 117, 246, 33, 80, 208, 211, 184, 90, 7, 229, 234, 68, 138, 127, 93, 18 } });
        }
    }
}
