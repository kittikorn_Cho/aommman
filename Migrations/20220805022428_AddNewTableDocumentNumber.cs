﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class AddNewTableDocumentNumber : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "DocumentNumber",
                table: "MemberTransactions",
                type: "longtext",
                nullable: true)
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AddColumn<string>(
                name: "DocumentNumber",
                table: "Massages",
                type: "longtext",
                nullable: true)
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "DocumentNumbers",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    CalendarDayId = table.Column<int>(type: "int", nullable: false),
                    Type = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    RunningNumber = table.Column<int>(type: "int", nullable: false),
                    PadLength = table.Column<int>(type: "int", nullable: false),
                    CarlendarDayId = table.Column<int>(type: "int", nullable: true),
                    CreatedBy = table.Column<int>(type: "int", nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    UpdatedBy = table.Column<int>(type: "int", nullable: true),
                    UpdatedDate = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    DeletedBy = table.Column<int>(type: "int", nullable: true),
                    DeletedDate = table.Column<DateTime>(type: "datetime(6)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DocumentNumbers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DocumentNumbers_CalendarDays_CarlendarDayId",
                        column: x => x.CarlendarDayId,
                        principalTable: "CalendarDays",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "THT70k73jagmBPUywFBIYR4Izd667v7xtGq+i2i7Lo0=", new byte[] { 46, 180, 216, 44, 134, 187, 253, 218, 207, 237, 219, 13, 102, 174, 134, 181 } });

            migrationBuilder.CreateIndex(
                name: "IX_DocumentNumbers_CarlendarDayId",
                table: "DocumentNumbers",
                column: "CarlendarDayId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DocumentNumbers");

            migrationBuilder.DropColumn(
                name: "DocumentNumber",
                table: "MemberTransactions");

            migrationBuilder.DropColumn(
                name: "DocumentNumber",
                table: "Massages");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "I+FAqmaY5kxOhAoXi90IFoaPzBffxw/vdpbz8GlnB00=", new byte[] { 87, 9, 100, 231, 23, 218, 36, 97, 249, 107, 218, 3, 20, 5, 103, 23 } });
        }
    }
}
