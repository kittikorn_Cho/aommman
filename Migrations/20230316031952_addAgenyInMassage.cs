﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class addAgenyInMassage : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "eiipqRTIHZFUbdpSUTjzIaoJxgnCKH88KYOIeD2OAF0=", new byte[] { 165, 61, 21, 151, 88, 250, 184, 17, 123, 149, 199, 5, 203, 19, 20, 146 } });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "QBYXYVyPAFdEVDKSztbS1+BuHfqKf77rLpzyJfUWz/E=", new byte[] { 200, 26, 51, 169, 97, 70, 44, 58, 30, 206, 186, 205, 129, 167, 182, 198 } });
        }
    }
}
