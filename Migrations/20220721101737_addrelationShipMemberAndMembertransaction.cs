﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class addrelationShipMemberAndMembertransaction : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "sOhd0dpnZ+u3ZB6AQLeXFmBULzyPvYKuABOVHoPc6iY=", new byte[] { 9, 116, 28, 243, 97, 157, 216, 38, 133, 201, 15, 193, 110, 251, 246, 62 } });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "WcxqED2Fm+RlIsilW1twXm0kjkdQyXJUyyc0GfLt768=", new byte[] { 32, 201, 117, 115, 182, 114, 110, 90, 206, 217, 76, 102, 105, 215, 114, 211 } });
        }
    }
}
