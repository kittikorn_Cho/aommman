﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class addStatusinReception : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Status",
                table: "Receptions",
                type: "int",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "l23T8UtFsS11j5aUNIhLI42wRGmHwEvqhwZDqie/k0g=", new byte[] { 155, 106, 183, 125, 117, 189, 122, 129, 250, 212, 240, 173, 248, 190, 195, 140 } });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Status",
                table: "Receptions");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "3k3hQYAJeeniraPFgjpl1QAdhjzPDXNf61Y8pdeJHPk=", new byte[] { 100, 124, 154, 131, 87, 143, 30, 118, 47, 84, 75, 226, 35, 222, 20, 18 } });
        }
    }
}
