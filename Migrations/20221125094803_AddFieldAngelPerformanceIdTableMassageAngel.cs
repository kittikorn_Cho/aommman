﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class AddFieldAngelPerformanceIdTableMassageAngel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "AngelPerformanceId",
                table: "MassageAngels",
                type: "int",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "gIbqm0YTCryLkgNAvy6RLal9sqxelhV54Els+6FqJQ0=", new byte[] { 64, 172, 201, 153, 158, 19, 54, 199, 54, 135, 20, 39, 164, 194, 41, 155 } });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AngelPerformanceId",
                table: "MassageAngels");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "MiIPVFLI+eeg44f6q5sQtdBaotogpWOoT2kbJA+aOys=", new byte[] { 31, 199, 93, 37, 177, 167, 44, 162, 168, 22, 117, 119, 97, 196, 81, 31 } });
        }
    }
}
