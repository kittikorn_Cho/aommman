﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class addFieldEntertainTotal : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "EntertainTotal",
                table: "MassageDebts",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "zRKI2zfmDyRRbKUsxvgGvQ2YbQhRh9HTx9Xh0CbOBzs=", new byte[] { 130, 251, 243, 1, 126, 54, 227, 226, 81, 32, 8, 55, 204, 46, 26, 86 } });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EntertainTotal",
                table: "MassageDebts");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "gHXiPUD66I+MzusL4C8WypzeIU6uZJwE5csJfYdZWpQ=", new byte[] { 227, 13, 2, 39, 37, 225, 62, 198, 164, 184, 5, 203, 145, 103, 162, 233 } });
        }
    }
}
