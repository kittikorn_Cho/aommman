﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class setNullCountriesId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "6xtqxU+vJQ7LhfjKFCP0nxQueGT4+AKNU7B6jmxKHdk=", new byte[] { 104, 95, 32, 147, 179, 14, 251, 9, 116, 98, 24, 225, 108, 1, 55, 226 } });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "wAE6QCMIpwAEPm7hIoLN1s8dFPQtSMUN1TStbjByUYs=", new byte[] { 145, 94, 166, 53, 73, 23, 33, 182, 246, 214, 61, 4, 219, 26, 118, 96 } });
        }
    }
}
