﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class seedAgencyType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "AgencyTypes",
                columns: new[] { "Id", "Fee", "Name", "Order" },
                values: new object[,]
                {
                    { 1, 0m, "อื่นๆ", 1 },
                    { 2, 0m, "แท็กซี่", 2 },
                    { 3, 0m, "คนขับรถ", 3 },
                    { 4, 0m, "โรงแรม", 4 },
                    { 5, 0m, "คณะทัวร์", 5 },
                    { 6, 0m, "ไกด์", 6 },
                    { 7, 0m, "บริษัททัวส์,สมาคมจีน", 7 },
                    { 8, 0m, "สมาคมจีน", 8 }
                });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "n6Z+k7MLgn0ae7S0WqOKXjyFwmkS/oJk7rsZ/AK8F+c=", new byte[] { 16, 60, 31, 209, 196, 112, 1, 237, 57, 152, 134, 218, 97, 34, 38, 225 } });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AgencyTypes",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "AgencyTypes",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "AgencyTypes",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "AgencyTypes",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "AgencyTypes",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "AgencyTypes",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "AgencyTypes",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "AgencyTypes",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "4zlIjyiBEvCfHhwXQEBRchUdotxApHTStSoKq22+FMo=", new byte[] { 50, 219, 251, 48, 118, 1, 128, 230, 65, 134, 220, 1, 4, 51, 211, 0 } });
        }
    }
}
