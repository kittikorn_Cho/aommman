﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class AddfiledPercentMemberTransaction : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsPercent",
                table: "MemberTransactions",
                type: "tinyint(1)",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<decimal>(
                name: "PercentTopup",
                table: "MemberTransactions",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "PercentTotalTopup",
                table: "MemberTransactions",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "H4rvdf6aafOfVLXZIThV3tNq1R4pAMqCqpRFGo0jVMQ=", new byte[] { 103, 238, 208, 15, 234, 59, 61, 216, 245, 90, 178, 45, 87, 98, 115, 204 } });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsPercent",
                table: "MemberTransactions");

            migrationBuilder.DropColumn(
                name: "PercentTopup",
                table: "MemberTransactions");

            migrationBuilder.DropColumn(
                name: "PercentTotalTopup",
                table: "MemberTransactions");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "flT2uLb2PawBTzAv+gXYvIx0jocrpgumMYhuyJUgQrU=", new byte[] { 58, 119, 43, 29, 115, 95, 96, 254, 83, 65, 173, 70, 155, 16, 113, 175 } });
        }
    }
}
