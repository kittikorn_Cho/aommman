﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class seedMenu : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Menus",
                columns: new[] { "Id", "Code", "CreatedBy", "CreatedDate", "DeletedBy", "DeletedDate", "Name", "Slug", "UpdatedBy", "UpdatedDate" },
                values: new object[,]
                {
                    { 1, "", null, null, null, null, "Dashboard", "", null, null },
                    { 2, "", null, null, null, null, "จัดการข้อมูลพนักงานบริการ", "", null, null },
                    { 3, "", null, null, null, null, "ประเภทพนักงานบริการ", "", null, null },
                    { 4, "", null, null, null, null, "ประเภทหนี้คงที่พนักงานบริการ", "", null, null },
                    { 5, "", null, null, null, null, "ประเภทนี้หักตามรอบพนักงานบริการ", "", null, null },
                    { 6, "", null, null, null, null, "ตรวจสุขภาพพนักงานบริการ", "", null, null },
                    { 7, "", null, null, null, null, "จัดการข้อมูลเมมเบอร์", "", null, null },
                    { 8, "", null, null, null, null, "ประเภทเมมเบอร์", "", null, null },
                    { 9, "", null, null, null, null, "ของแถมเมมเบอร์", "", null, null },
                    { 10, "", null, null, null, null, "จัดการข้อมูลชำระเงินเมมเบอร์", "", null, null },
                    { 11, "", null, null, null, null, "พนักงานต้อนรับ", "", null, null },
                    { 12, "", null, null, null, null, "ห้อง", "", null, null },
                    { 13, "", null, null, null, null, "สิทธิ์", "", null, null },
                    { 14, "", null, null, null, null, "ผู้ใช้ระบบ", "", null, null }
                });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "o3wNFBTR+6SjC/Ih+pqF+3CG7sAPc9z6YG69bYe5M+U=", new byte[] { 94, 136, 116, 6, 235, 223, 131, 18, 168, 198, 148, 152, 65, 138, 237, 72 } });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "q4KaqcVMh6CqJR/axo0Qqy7XqoFRGAhb3UBG9+SMoqI=", new byte[] { 5, 100, 56, 211, 233, 19, 108, 88, 171, 223, 253, 3, 57, 23, 190, 53 } });
        }
    }
}
