﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class editfeildRoomWage : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<decimal>(
                name: "RoomWage",
                table: "massageMemberAndRooms",
                type: "decimal(18,2)",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(65,30)");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "tovtzzipysOtBT8ylctjn3npPMIh791E3kflpSwAwbU=", new byte[] { 201, 227, 80, 186, 237, 158, 217, 190, 75, 85, 183, 191, 189, 230, 57, 1 } });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<decimal>(
                name: "RoomWage",
                table: "massageMemberAndRooms",
                type: "decimal(65,30)",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,2)");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "9y88A9DTbrOj1ZlkMcfQxpYHi51DTMWLJBdH2MQ/N2o=", new byte[] { 105, 119, 110, 0, 255, 157, 21, 242, 16, 47, 120, 128, 66, 36, 96, 226 } });
        }
    }
}
