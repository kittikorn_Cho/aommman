﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class addIsNew : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsPerformanceNew",
                table: "AngelPerformances",
                type: "tinyint(1)",
                nullable: false,
                defaultValue: false);

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "+w8RwL6zcYXuDpVmOvhEZ28eBKSgw8uDg+UoOe1dvVs=", new byte[] { 50, 174, 126, 212, 243, 199, 106, 194, 18, 209, 112, 27, 238, 218, 9, 137 } });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsPerformanceNew",
                table: "AngelPerformances");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "gIbqm0YTCryLkgNAvy6RLal9sqxelhV54Els+6FqJQ0=", new byte[] { 64, 172, 201, 153, 158, 19, 54, 199, 54, 135, 20, 39, 164, 194, 41, 155 } });
        }
    }
}
