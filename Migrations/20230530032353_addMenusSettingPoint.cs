﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class addMenusSettingPoint : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Menus",
                columns: new[] { "Id", "Code", "CreatedBy", "CreatedDate", "DeletedBy", "DeletedDate", "Name", "Slug", "UpdatedBy", "UpdatedDate" },
                values: new object[] { 64, "SettingPoint", null, null, null, null, "ตั้งค่า --> ตั้งค่าคะแนน", "", null, null });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "SNYh3QvurwNH8OloolEf6jujlyLboqPGuj1pKqjJvKk=", new byte[] { 213, 10, 43, 215, 104, 22, 28, 175, 117, 97, 225, 151, 40, 43, 95, 16 } });

            migrationBuilder.InsertData(
                table: "RoleMenu",
                columns: new[] { "MenuId", "RoleId" },
                values: new object[] { 64, 1 });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "RoleMenu",
                keyColumns: new[] { "MenuId", "RoleId" },
                keyValues: new object[] { 64, 1 });

            migrationBuilder.DeleteData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 64);

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "5CfyXgHkpwH4AaIDqu4p1IPeTdriKUu04pd8tRlnKt0=", new byte[] { 226, 101, 192, 82, 150, 19, 92, 11, 132, 81, 166, 28, 18, 174, 39, 75 } });
        }
    }
}
