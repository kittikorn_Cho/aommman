﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class addNewMenubillchageReception : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Menus",
                columns: new[] { "Id", "Code", "CreatedBy", "CreatedDate", "DeletedBy", "DeletedDate", "Name", "Slug", "UpdatedBy", "UpdatedDate" },
                values: new object[] { 32, "EditBillReception", null, null, null, null, "พนักงานต้อนรับ --> แก้หัวเชียร์", "", null, null });

            migrationBuilder.UpdateData(
                table: "RoomTypes",
                keyColumn: "Id",
                keyValue: 1,
                column: "Code",
                value: "suite");

            migrationBuilder.UpdateData(
                table: "RoomTypes",
                keyColumn: "Id",
                keyValue: 2,
                column: "Code",
                value: "vip");

            migrationBuilder.UpdateData(
                table: "RoomTypes",
                keyColumn: "Id",
                keyValue: 3,
                column: "Code",
                value: "suiteSub");

            migrationBuilder.UpdateData(
                table: "RoomTypes",
                keyColumn: "Id",
                keyValue: 4,
                column: "Code",
                value: "normal");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "PAnVZ9EQPxUMTcjaprgK8bOpTAO95PQsmaTOJq4c8ts=", new byte[] { 149, 138, 246, 58, 59, 77, 78, 15, 172, 93, 237, 179, 205, 52, 70, 248 } });

            migrationBuilder.InsertData(
                table: "RoleMenu",
                columns: new[] { "MenuId", "RoleId" },
                values: new object[] { 32, 1 });

            migrationBuilder.CreateIndex(
                name: "IX_Massages_CalendarDayId",
                table: "Massages",
                column: "CalendarDayId");

            migrationBuilder.AddForeignKey(
                name: "FK_Massages_CalendarDays_CalendarDayId",
                table: "Massages",
                column: "CalendarDayId",
                principalTable: "CalendarDays",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Massages_CalendarDays_CalendarDayId",
                table: "Massages");

            migrationBuilder.DropIndex(
                name: "IX_Massages_CalendarDayId",
                table: "Massages");

            migrationBuilder.DeleteData(
                table: "RoleMenu",
                keyColumns: new[] { "MenuId", "RoleId" },
                keyValues: new object[] { 32, 1 });

            migrationBuilder.DeleteData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 32);

            migrationBuilder.UpdateData(
                table: "RoomTypes",
                keyColumn: "Id",
                keyValue: 1,
                column: "Code",
                value: "SU");

            migrationBuilder.UpdateData(
                table: "RoomTypes",
                keyColumn: "Id",
                keyValue: 2,
                column: "Code",
                value: "VIP");

            migrationBuilder.UpdateData(
                table: "RoomTypes",
                keyColumn: "Id",
                keyValue: 3,
                column: "Code",
                value: "SS");

            migrationBuilder.UpdateData(
                table: "RoomTypes",
                keyColumn: "Id",
                keyValue: 4,
                column: "Code",
                value: "NORMAL");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "2z0CVnSyOX9RYhUOqpG21dKA/7JgO55Y3F5eSocUWac=", new byte[] { 175, 180, 97, 96, 242, 117, 67, 9, 147, 30, 134, 144, 119, 202, 42, 103 } });
        }
    }
}
