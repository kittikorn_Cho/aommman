﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class seedMenuReports : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Menus",
                columns: new[] { "Id", "Code", "CreatedBy", "CreatedDate", "DeletedBy", "DeletedDate", "Name", "Slug", "UpdatedBy", "UpdatedDate" },
                values: new object[,]
                {
                    { 24, "Report", null, null, null, null, "รายงาน", "", null, null },
                    { 25, "DailyRemittanceSum", null, null, null, null, "รายงาน --> ใบสรุปส่งเงินประจำวัน", "", null, null },
                    { 26, "MemberSaleSum", null, null, null, null, "รายงาน --> สรุปยอดขายเมมเบอร์", "", null, null },
                    { 27, "MemberRoleSum", null, null, null, null, "รายงาน --> สรุปใช้สิทธ์เมมเบอร์", "", null, null },
                    { 28, "CouponPaymentAmount", null, null, null, null, "รายงาน --> ใบสรุปส่งเงิน(จ่ายคูปอง)", "", null, null }
                });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "UxdOqXf2GtqZDnUph9/xW7MtiaVpJ4w/sRkr9wZS8RY=", new byte[] { 231, 195, 236, 188, 220, 228, 202, 143, 154, 108, 181, 117, 196, 239, 106, 85 } });

            migrationBuilder.InsertData(
                table: "RoleMenu",
                columns: new[] { "MenuId", "RoleId" },
                values: new object[,]
                {
                    { 24, 1 },
                    { 25, 1 },
                    { 26, 1 },
                    { 27, 1 },
                    { 28, 1 }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "RoleMenu",
                keyColumns: new[] { "MenuId", "RoleId" },
                keyValues: new object[] { 24, 1 });

            migrationBuilder.DeleteData(
                table: "RoleMenu",
                keyColumns: new[] { "MenuId", "RoleId" },
                keyValues: new object[] { 25, 1 });

            migrationBuilder.DeleteData(
                table: "RoleMenu",
                keyColumns: new[] { "MenuId", "RoleId" },
                keyValues: new object[] { 26, 1 });

            migrationBuilder.DeleteData(
                table: "RoleMenu",
                keyColumns: new[] { "MenuId", "RoleId" },
                keyValues: new object[] { 27, 1 });

            migrationBuilder.DeleteData(
                table: "RoleMenu",
                keyColumns: new[] { "MenuId", "RoleId" },
                keyValues: new object[] { 28, 1 });

            migrationBuilder.DeleteData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 24);

            migrationBuilder.DeleteData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 25);

            migrationBuilder.DeleteData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 26);

            migrationBuilder.DeleteData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 27);

            migrationBuilder.DeleteData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 28);

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "aureAo/zahM0X/48ng+8cvStUwc3DYw95hRH1OU/Qlw=", new byte[] { 35, 89, 16, 220, 185, 104, 87, 16, 213, 66, 131, 171, 98, 113, 18, 83 } });
        }
    }
}
