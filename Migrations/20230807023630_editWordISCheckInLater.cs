﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OONApi.Migrations
{
    public partial class editWordISCheckInLater : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "isCheckInLater",
                table: "Massages",
                newName: "IsCheckInLater");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "4SWHPqOD5MOAEItoURroopMwEMLufKmWmJU3OH3icvA=", new byte[] { 179, 111, 238, 197, 134, 3, 16, 19, 154, 120, 32, 153, 156, 210, 61, 71 } });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "IsCheckInLater",
                table: "Massages",
                newName: "isCheckInLater");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Password", "PasswordSalt" },
                values: new object[] { "Q7BNWFyahrh69FpEd0LdrWtOFzO9L6VVecCLUrVHuoM=", new byte[] { 16, 211, 44, 108, 253, 188, 99, 209, 122, 59, 74, 56, 40, 244, 165, 195 } });
        }
    }
}
