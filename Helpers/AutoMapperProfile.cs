﻿using AutoMapper;
using OONApi.Contracts.Responses;
using OONApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Helpers
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<AngelType, AngelTypeResponse>();
            CreateMap<Angel, AngelResponse>();
            CreateMap<BuildingType, BuildingTypeResponse>();
            CreateMap<RoomType, RoomTypeResponse>();
            CreateMap<RoomType, RoomTypeChildRoomResponse>();
            CreateMap<Room, RoomResponse>();
            CreateMap<Room, RoomParentResponse>();
            CreateMap<MemberType, MemberTypeResponse>();
            CreateMap<MemberTypeItem, MemberTypeItemResponse>();
            CreateMap<Item, ItemResponse>();
            CreateMap<Member , MemberResponse>();
            CreateMap<Reception, ReceptionResponse>();
            CreateMap<ReceptionType, ReceptionTypeResponse>();
            CreateMap<AngelDoctor, AngelDoctorResponse>();
            CreateMap<DebtType, DebtTypeResponse>();
            CreateMap<DeductType, DeductTypeResponse>();
            CreateMap<AngelDebt, AngelDebtResponse>();
            CreateMap<Role, RoleResponse>();
            CreateMap<User, UserResponse>();
            CreateMap<UserRole, UserRoleResponse>();
            CreateMap<AngelPerformance, AngelPerformanceResponse>();
            CreateMap<MassageAngel, MassageAngelResponse>();
            CreateMap<Massage, MassageResponse>();
            CreateMap<MassageRoom, MassageRoomResponse>();
            CreateMap<MassageRoom, NewMassageRoomResponse>();
            CreateMap<AngelPerformanceDeduct, AngelPerformanceDeductResponse>();
            CreateMap<MemberItem, MemberItemResponse>();
            CreateMap<MemberTransaction, MemberTransactionResponse>();
            CreateMap<CalendarDay, CalendarDayResponse>();
            CreateMap<Menu, MenuResponse>();
            CreateMap<DailyDebt, DailyDebtResponse>();
            CreateMap<AngelWorking, AngelWorkingResponse>();
            CreateMap<AngelType, AngelWorkingListResponse>();
            CreateMap<MassageCreditPayment, MassageCreditPaymentResponse>();
            CreateMap<MassageMemberPayment, MassageMemberPaymentResponse>();
            CreateMap<AngelPerformanceDebt, AngelPerformanceDebtResponse>();
            CreateMap<Reception, ReceptionSellResponse>();
            CreateMap<AngelTypeResponse, SummaryRoundResponse>();

            CreateMap<Member, MemberPaymentEtcResponse>();
            CreateMap<MemberPayment, MemberPaymentResponse>();
            CreateMap<MemberTopup, MemberTopupResponse>();
            CreateMap<MemberItemTransaction, MemberItemTransactionResponse>();
            //Report

            CreateMap<MemberTransaction, ReportMemberSellResponse>();
            CreateMap<AngelType, ReportPaymentAngelPerformanceResponse>();
            CreateMap<AngelType, ReportSummaryDayResponse>();
            CreateMap<MassageMemberPayment, ReportMemberPaymentResponse>();
            CreateMap<MassageRoom, ReportDiscountRoomResponse>();
            CreateMap<MassageAngel, ReportDiscountAngelResponse>();
            CreateMap<Massage, ReportUnpaidBillResponse>();
            CreateMap<Reception, ReportReceptionSellResponse>();
            CreateMap<Reception, ReportReceptionRoundTotalResponse>();
            CreateMap<MemberPayment, ReportMemberFoodResponse>();
            CreateMap<MassageRoom, ReportMassageSellResponse>();
            CreateMap<Room, ReportRoomSuiteResponse>();
            CreateMap<MemberTopup, ReportMemberTopupResponse>();
            CreateMap<Member, ReportMemberCreditAmountResponse>();
            CreateMap<MassageDebt, ReportMassageDebtResponse>();

            //End----


            CreateMap<AngelType, AngelTypeSummaryTotalRoundMonthResponse>();
            CreateMap<Angel, AngelSummaryTotalRoundMonthResponse>();
            CreateMap<CalendarDay, CalendarSummaryTotalRoundMonthResponse>();
            CreateMap<Angel, AngelHistoryDebtResponse>();
            CreateMap<Angel, AngelSalaryCountDayResponse>();
            CreateMap<CalendarDay, CalendarSalaryCountDayResponse>();
            CreateMap<MassageRoom, RoomSuiteCheckInResponse>();
            CreateMap<Room, RoomMassageAngelResponse>();
            CreateMap<MassageQrCodePayment, MassageQrCodePaymentResponse>();
            CreateMap<Setting, SettingResponse>();
            CreateMap<AngelType, ReportAngelDebtByAngelTypeResponse>();
            CreateMap<Angel, ReportAngelDebtByAngelResponse>();
            CreateMap<Countries, CountriesResponse>();
            CreateMap<Agency, AgencyResponse>();
            CreateMap<AgencyPerformance, AgencyPerformanceResponse>();
            CreateMap<AgencyType, AgencyTypeResponse>();
            CreateMap<MassageMemberAndRoom, MassageMemberAndRoomResponse>();
            CreateMap<Booking, BookingResponse>();
            CreateMap<BookingAngel, BookingAngelsResponse>();
            CreateMap<AngelPointTransaction, AngelPointTransactionResponse>();
            CreateMap<ReceptionPointTransaction, ReceptionPointTransactionResponse>();
            CreateMap<MemberPointRedemtion, MemberPointRedemtionsResponse>();
            CreateMap<AngelPointRedemtion, AngelPointRedemtionsResponse>();
            CreateMap<ReceptionPointRedemtion, ReceptionPointRedemtionsResponse>();
        }
    }
}
