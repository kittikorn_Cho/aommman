﻿using OONApi.Contracts.Requests.Queires;
using OONApi.Contracts.Responses;
using OONApi.Models;
using OONApi.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Helpers
{
    public class PaginationHelpers
    {
        public static PagedResponse<T> CreatePaginationResponse<T>(IUriService uriService, PaginationFilter pagination, List<T> response)
        {
            var nextPage = pagination.PageNumber >= 1
                ? uriService.GetAllSitesUri(new PaginationQuery(pagination.PageNumber + 1, pagination.PageSize, pagination.OrderBy, pagination.OrderDirection, pagination.Keyword)).ToString()
                : null;

            var previousPage = pagination.PageNumber - 1 >= 1
                ? uriService.GetAllSitesUri(new PaginationQuery(pagination.PageNumber - 1, pagination.PageSize, pagination.OrderBy, pagination.OrderDirection, pagination.Keyword)).ToString()
                : null;

            return new PagedResponse<T>
            {
                Data = response,
                PageNumber = pagination.PageNumber >= 1 ? pagination.PageNumber : (int?)null,
                PageSize = pagination.PageSize >= 1 ? pagination.PageSize : (int?)null,
                TotalRecords = pagination.TotalRecords,
                OrderBy = pagination.OrderBy,
                OrderDirection = pagination.OrderDirection,
                Keyword = pagination.Keyword ?? "",
                NextPage = response.Any() ? nextPage : null,
                PreviousPage = previousPage,
            };
        }
    }
}
