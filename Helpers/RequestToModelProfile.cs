﻿using AutoMapper;
using OONApi.Contracts.Requests.Queires;
using OONApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Helpers
{
    public class RequestToModelProfile : Profile
    {
       public RequestToModelProfile()
        {
            CreateMap<PaginationQuery, PaginationFilter>();
            CreateMap<AngelSearchQuery, PaginationFilter>();
            CreateMap<MemberSearchQuery, PaginationFilter>();
            CreateMap<AngelDoctorSearchQuery, PaginationFilter>();
            CreateMap<ReceptionSearchQuery, PaginationFilter>();
            CreateMap<AngelTypesSearchQuery, PaginationFilter>();
            CreateMap<DebtTypeSearchQuery, PaginationFilter>();
            CreateMap<DeductTypeSearchQuery, PaginationFilter>();
            CreateMap<RoleSearchQuery, PaginationFilter>();
            CreateMap<UserSearchQuery, PaginationFilter>();
            CreateMap<AngelPerformanceSearchQuery, PaginationFilter>();
            CreateMap<CheckInSearchQuery, PaginationFilter>();
            CreateMap<ItemSearchQuery, PaginationFilter>();
            CreateMap<RoomSearchQuery, PaginationFilter>();
            CreateMap<MemberPaymentSearchQuery, PaginationFilter>();
            CreateMap<DailyDebtSearchQuery, PaginationFilter>();
            CreateMap<AngelWorkingListSearchQuery, PaginationFilter>();
            CreateMap<UnPaidSearchQurey, PaginationFilter>();
            CreateMap<MemberHistorySearchQuery, PaginationFilter>();
            CreateMap<WagePaymentCancelSearchQuery, PaginationFilter>();
            CreateMap<AgencySearchQuery, PaginationFilter>();
            CreateMap<AgencyPerformanceSearchQuery, PaginationFilter>();
            CreateMap<MemberPointRedemtionsHistorySearchQuery, PaginationFilter>();
            CreateMap<AngelPointRedemtionsHistorySearchQuery, PaginationFilter>();
            CreateMap<ReceptionPointRedemtionsHistorySearchQuery, PaginationFilter>();
        }
    }
}
