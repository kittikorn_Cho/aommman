using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using OONApi.Helpers;
using OONApi.Models;
using OONApi.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OONApi.Hubs;
using Microsoft.AspNetCore.HttpOverrides;
using DinkToPdf.Contracts;
using DinkToPdf;

namespace OONApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton(typeof(IConverter), new SynchronizedConverter(new PdfTools()));
            services.AddAutoMapper(typeof(Startup));
            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "OONApi", Version = "v1" });
                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    In = ParameterLocation.Header,
                    Description = "Please insert JWT with Bearer into field",
                    Name = "Authorization",
                    Type = SecuritySchemeType.ApiKey
                });
                c.AddSecurityRequirement(new OpenApiSecurityRequirement {
                   {
                     new OpenApiSecurityScheme
                     {
                       Reference = new OpenApiReference
                       {
                         Type = ReferenceType.SecurityScheme,
                         Id = "Bearer"
                       }
                      },
                      new string[] { }
                    }
                  });
            });

            string mySqlConnectionStr = Configuration.GetConnectionString("DefaultConnection");
            services.AddDbContextPool<AppDbContext>(options => {
                options.ConfigureWarnings(builder =>
                {
                    builder.Ignore(CoreEventId.PossibleIncorrectRequiredNavigationWithQueryFilterInteractionWarning);
                });
                options.UseMySql(mySqlConnectionStr, ServerVersion.AutoDetect(mySqlConnectionStr));
             });


            services.AddScoped<IAngelTypeService, AngelTypeService>();
            services.AddScoped<IAngelService, AngelService>();
            services.AddScoped<IBuildingTypeService, BuildingTypeService>();
            services.AddScoped<IRoomTypeService, RoomTypeService>();
            services.AddScoped<IRoomService, RoomService>();
            services.AddScoped<IMemberTypeService, MemberTypeService>();
            services.AddScoped<IItemService, ItemService>();
            services.AddScoped<IMemberService, MemberService>();
            services.AddScoped<IReceptionService, ReceptionService>();
            services.AddScoped<IAngelDoctorService, AngelDoctorService>();
            services.AddScoped<IDebtTypeService, DebtTypeService>();
            services.AddScoped<IDeductTypeService, DeductTypeService>();
            services.AddScoped<IRoleService, RoleService>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<ISettingService, SettingService>();
            services.AddScoped<IMassageService, MassageService>();
            services.AddScoped<IAngelPerformanceService, AngelPerformanceService>();
            services.AddScoped<IMenuService, MenuService>();
            services.AddScoped<IDailyDebtServicee, DailyDebtService>();
            services.AddScoped<IGenerateReportService, GenerateReportService>();
            services.AddScoped<IReportService, ReportService>();
            services.AddScoped<ICountriesService, CountriesService>();
            services.AddScoped<IAgencyService, AgencyService>();
            services.AddScoped<IAgencyPerformanceService, AgencyPerformanceService>();
            services.AddScoped<IBookingService, BookingService>();
            services.AddScoped<IAngelPointTransactionService, AngelPointTransactionService>();
            services.AddScoped<IReceptionPointTransactionService, ReceptionPointTransactionService>();
            services.AddScoped<IMemberPointRedemtionsService, MemberPointRedemtionService>();
            services.AddScoped<IAngelPointRedemtionsService, AngelPointRedemtionService>();
            services.AddScoped<IReceptionPointRedemtionsService, ReceptionPointRedemtionService>();
            services.AddScoped<IMassageDebtService, MassageDebtService>();
            services.AddScoped<IDailyPriceHistoryService, DailyPriceHistoryService>();


            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
            .AddJwtBearer(options =>
            {
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,
                    ValidIssuer = Configuration["Jwt:Issuer"],
                    ValidAudience = Configuration["Jwt:Issuer"],
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["Jwt:Key"]))
                };
            });

            services.AddControllers().AddNewtonsoftJson(options => options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);

            var appSettingSection = Configuration.GetSection("AppSettings");
            services.Configure<AppSettings>(appSettingSection);

            var appSettings = appSettingSection.Get<AppSettings>();

            services.AddSingleton<IUriService>(provider =>
            {
                var accessor = provider.GetRequiredService<IHttpContextAccessor>();
                var request = accessor.HttpContext.Request;
                var absoluteUri = string.Concat(request.Scheme, "://", request.Host.ToUriComponent(), request.PathBase, "/");

                return new UriService(absoluteUri);
            });
            services.AddSignalR();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "OONApi v1"));
            }

            app.UseCors(builder =>
            {
                builder
                .WithOrigins(Configuration.GetSection("AllowedOrigin").Get<string[]>())
                .AllowAnyMethod()
                .AllowAnyHeader()
                .AllowCredentials();
            });
            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
            });
            app.UsePathBase(Configuration.GetSection("AppBase").Get<string>());
            app.UseAuthentication();

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapHub<CheckInHub>("/checkin-hub");
                endpoints.MapControllers();
            });
        }
    }
}
