﻿using Microsoft.EntityFrameworkCore;
using OONApi.Contracts.Requests.Queires;
using OONApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace OONApi.Services
{
    public class DebtTypeService : IDebtTypeService
    {
        private readonly AppDbContext _db;

        public DebtTypeService(AppDbContext db)
        {
            _db = db;
        }

        public async Task<List<DebtType>> GetAllAsync(DebtTypeSearchQuery debtType, PaginationFilter paginationFilter = null)
        {
            if (paginationFilter == null)
            {
                return await _db.DebtTypes.AsNoTracking().ToListAsync();
            }

            if (typeof(DebtType).GetProperties().All(s => s.Name != paginationFilter.OrderBy.First().ToString().ToUpper() + paginationFilter.OrderBy.Substring(1)))
            {
                paginationFilter.OrderBy = "Id";
            }
            string direction = null;
            if (string.Equals(paginationFilter.OrderDirection, "desc", StringComparison.OrdinalIgnoreCase))
            {
                direction = "descending";
            }
            else
            {
                direction = "ascending";
            }

            var skip = (paginationFilter.PageNumber -1) * paginationFilter.PageSize;

            IQueryable<DebtType> query = _db.DebtTypes;
            if (!String.IsNullOrEmpty(debtType.Name))
            {
                query = query.Where(d => d.Name == debtType.Name);
            }
            if (!String.IsNullOrEmpty(paginationFilter.Keyword))
            {
                query = query.Where(d => d.Name.Contains(paginationFilter.Keyword));
            }
            paginationFilter.TotalRecords = query.Count();
            string sortBy = char.ToUpper(paginationFilter.OrderBy.First()) + paginationFilter.OrderBy.Substring(1).ToLower();
            if(paginationFilter == null)
            {
                return await query.AsNoTracking().ToListAsync();
            }
            return await query.OrderBy(sortBy + " " + direction).Skip(skip).Take(paginationFilter.PageSize).AsNoTracking().ToListAsync();
        }

        public async Task<DebtType> GetByIdAsync(int id)
        {
            return await _db.DebtTypes.Where(a => a.Id == id).SingleOrDefaultAsync();
        }

        public async Task<bool> CreateAsync(DebtType debtType)
        {
            _db.DebtTypes.Add(debtType);
            var create = await _db.SaveChangesAsync();
            return create > 0;
        }

        public async Task<bool> UpdateAsync(DebtType debtType)
        {
            _db.DebtTypes.Update(debtType);
            var update = await _db.SaveChangesAsync();
            return update > 0;
        }

        public async Task<bool> DeleteAsync(int id)
        {
            var deleted = 0;
            var debtType = await GetByIdAsync(id);
            if (debtType != null)
            {
                _db.DebtTypes.Remove(debtType);
                deleted = await _db.SaveChangesAsync();
            }
            return deleted > 0;
        }
    }
}
