﻿using DocumentFormat.OpenXml.Office2010.Excel;
using OONApi.Contracts.Requests.Queires;
using OONApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Services
{
    public interface IAngelService
    {
        Task<List<Angel>> GetAllAsync(AngelSearchQuery angel, PaginationFilter paginationFilter = null);
        Task<Angel> GetByIdAsync(int id);
        Task<List<AngelDebt>> GetAngelDebtByAngelId(int id);
        Task<bool> CreateAsync(Angel angel);
        Task<bool> CreateAngelDebtAsync(List<AngelDebt> angelDebts);
        Task<bool> UpdateAsync(Angel angel);
        Task<bool> UpdateAngelDebtAsync(List<AngelDebt> angelDebts);
        Task<bool> DeleteAsync(int id);
        Task<bool> IsDuplicateRfid(Angel angel, bool isUpdate = false);
        Task<AngelWorking> GetAngelWorking(int angelId, int calendarDayId);
        Task<AngelWorking> GetAngelWorkingById(int Id);
        Task<List<AngelWorking>> GetAllWorkingAsync(int angelTypeId, int calendarDayId);
        Task<bool> CheckInTimeAngel(AngelWorking angelWorking);
        Task<bool> CheckOutTimeAngel(AngelWorking angelWorking);
        Task<Angel> GetByRfidAsync(string rfid);
        Task<List<Angel>> GetAngelByAngeltypeId(int angelTypeid);
        Task<List<Angel>> GetAngelByAngeltypeIdStatus(int angelTypeid);
        Task<List<AngelPerformanceDebt>> GetHistoryDebt(List<int> calendarIds, AngelSearchQuery angel, PaginationFilter paginationFilter = null);
        Task<AngelDebt> GetAngelDebtByAngelidAndDebtTypeId(int angelId, int debtTypeId);
        Task<AngelDebt> GetAngelDebtById(int id);
        Task<bool> UpdateAngelDebtByPerformance(AngelDebt angelDebt);
        Task<bool> CreateAngelGalleryAsync(AngelGallery angelGallery);
        Task<bool> DeleteAngelsWorking(int id);
    }
}
