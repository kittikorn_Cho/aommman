﻿using OONApi.Contracts.Requests.Queires;
using OONApi.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OONApi.Services
{
    public interface IDailyDebtServicee
    {
        Task<List<DailyDebt>> GetAllAsync(DailyDebtSearchQuery dailyDebt, PaginationFilter paginationFilter = null);
        Task<DailyDebt> GetByIdAsync(int id);
        Task<bool> CreateAsync(DailyDebt dailyDebt);
        Task<bool> UpdateAsync(DailyDebt dailyDebt);
        Task<bool> DeleteAsync(int id);
    }
}
