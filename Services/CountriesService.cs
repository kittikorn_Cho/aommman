﻿using DocumentFormat.OpenXml.Office2021.DocumentTasks;
using Microsoft.EntityFrameworkCore;
using OONApi.Models;
using System.Collections.Generic;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using System.Linq;

namespace OONApi.Services
{
    public class CountriesService : ICountriesService
    {
        private readonly AppDbContext _db;

        public CountriesService(AppDbContext db)
        {
            _db = db;
        }
        public async Task<List<Countries>> GetAllAsync()
        {
            return await _db.Countries.ToListAsync();
        }
        public async Task<Countries> GetByIdAsync(int id)
        {
            return await _db.Countries.Where(c => c.Id == id).SingleOrDefaultAsync();
        }
    }
}
