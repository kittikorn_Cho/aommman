﻿using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using Microsoft.EntityFrameworkCore;
using OONApi.Contracts.Requests.Queires;
using OONApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Security.Cryptography;
using System.Threading.Tasks;

namespace OONApi.Services
{
    public class UserService : IUserService
    {
        private readonly AppDbContext _db;

        public UserService(AppDbContext db)
        {
            _db = db;
        }
        public async Task<bool> IsDuplicateUser(string username)
        {
            return await _db.Users.AnyAsync(w => w.Username == username);
        }
        public async Task<List<User>> GetAllAsync(UserSearchQuery user, PaginationFilter paginationFilter = null, bool isRole = false)
        {
            if (paginationFilter == null)
            {
                if (isRole)
                {
                    return await _db.Users.Include(u => u.UserRoles).AsNoTracking().ToListAsync();
                }
                else
                {
                    return await _db.Users.Include(u => u.UserRoles).Where(a=>a.Id != 1).AsNoTracking().ToListAsync();
                }
               
            }

            if (typeof(User).GetProperties().All(s => s.Name != paginationFilter.OrderBy.First().ToString().ToUpper() + paginationFilter.OrderBy.Substring(1)))
            {
                paginationFilter.OrderBy = "Id";
            }
            string direction = null;
            if (string.Equals(paginationFilter.OrderDirection, "desc", StringComparison.OrdinalIgnoreCase))
            {
                direction = "descending";
            }
            else
            {
                direction = "ascending";
            }

            var skip = (paginationFilter.PageNumber - 1) * paginationFilter.PageSize;

            IQueryable<User> query = _db.Users.Include(u => u.UserRoles);
            if (!String.IsNullOrEmpty(user.Username))
            {
                query = query.Where(u => u.Username == user.Username);
            }
            if (!String.IsNullOrEmpty(user.UserFullname))
            {
                query = query.Where(u => u.UserFullname == user.UserFullname);
            }
            if (!String.IsNullOrEmpty(user.Tel))
            {
                query = query.Where(u => u.Tel == user.Tel);
            }
            if ((user.Status != 0))
            {
                query = query.Where(u => u.Status == user.Status);
            }
            if (!String.IsNullOrEmpty(paginationFilter.Keyword))
            {
                query = query.Where(u => u.Username.Contains(paginationFilter.Keyword) || u.UserFullname.Contains(paginationFilter.Keyword)); 
            }

            if (!isRole)
            {
                query = query.Where(u => u.Id != 1);
            }

            paginationFilter.TotalRecords = query.Count();
            string sorBy = char.ToUpper(paginationFilter.OrderBy.First()) + paginationFilter.OrderBy.Substring(1).ToUpper();
            var list = await query.OrderBy(sorBy + " " + direction).Skip(skip).Take(paginationFilter.PageSize).AsNoTracking().ToListAsync();
            foreach (var item in list)
            {
                List<Role> roles = new List<Role>();
                foreach(var userRole in item.UserRoles)
                {
                    Role role = await _db.Roles.Where(r => r.Id == userRole.RoleId).SingleOrDefaultAsync();
                    roles.Add(role);
                }
                item.Roles = roles;
            }
            return list;
        }

        public async Task<User> GetByIdAsync(int id)
        {
           User user = await _db.Users.Include(u => u.UserRoles).SingleOrDefaultAsync(u => u.Id == id);
           if(user != null)
            {
               
                List<int> roleIds = user.UserRoles.Select(r => r.RoleId).ToList();

                List<Role> roles = await _db.Roles.Where(r => roleIds.Contains(r.Id)).ToListAsync();

                List<Menu> menus = await _db.Menus.Join(_db.RoleMenu, me => me.Id, r => r.MenuId, (m, r) => new { Menus = m, RoleMenu = r }).Where(m => roleIds.Contains(m.RoleMenu.RoleId)).Select(s=>s.Menus).Distinct().ToListAsync();

                user.Menus = menus;
                user.Roles = roles; 
            }
           return user;
        }

        public async Task<bool> CreateAsync(User user, List<int> roleIds)
        {
            _db.Users.Add(user);
            var created = await _db.SaveChangesAsync();

            List<UserRole> list = new List<UserRole>();
            foreach(var id in roleIds)
            {
                UserRole userRole = new UserRole
                {
                    UserId = user.Id,
                    RoleId = id
                };
                list.Add(userRole);
            }
            _db.AddRange(list);
            created = await _db.SaveChangesAsync();

            return created > 0;
        }
        public static HashSalt EncryptPassword(string password)
        {
            byte[] salt = new byte[128 / 8]; // Generate a 128-bit salt using a secure PRNG
            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(salt);
            }
            string encryptedPassw = Convert.ToBase64String(KeyDerivation.Pbkdf2(
                password: password,
                salt: salt,
                prf: KeyDerivationPrf.HMACSHA1,
                iterationCount: 10000,
                numBytesRequested: 256 / 8
            ));
            return new HashSalt { Hash = encryptedPassw, Salt = salt };
        }
        public async Task<bool> UpdateAsync(User user, List<int> roleIds)
        {
            _db.Users.Update(user);
            var updated = await _db.SaveChangesAsync();

            if (user.UserRoles.Count > 0)
            {
                foreach (UserRole i in user.UserRoles.ToList())
                {
                    _db.Remove<UserRole>(i);
                    _db.SaveChanges();
                }
            }

            List<UserRole> list = new List<UserRole>();
            foreach (var id in roleIds)
            {
                UserRole userRole = new UserRole
                {
                    UserId = user.Id,
                    RoleId = id
                };
                list.Add(userRole);
            }
            _db.AddRange(list);
            updated = await _db.SaveChangesAsync();

            return updated > 0;
        }
        public async Task<bool> DeleteAsync(int id)
        {
            var deleted = 0;
            var user = await GetByIdAsync(id);

            foreach (UserRole i in user.UserRoles.ToList())
            {
                _db.Remove<UserRole>(i);
                _db.SaveChanges();
            }

            if (user != null)
            {
                _db.Users.Remove(user);
                deleted = await _db.SaveChangesAsync();
            }
            return deleted > 0;
        }
        public async Task<User> Authenticate(string username, string password)
        {
            var query = await _db.Users.Include(i => i.UserRoles).Where(w => w.Username == username).FirstOrDefaultAsync();
            if (query == null)
            {
                return null;
            }
            if (!VerifyPassword(password, query.PasswordSalt, query.Password))
            {
                return null;
            }
            return query;
        }
        public bool VerifyPassword(string enteredPassword, byte[] salt, string storedPassword)
        {
            string encryptedPassw = Convert.ToBase64String(KeyDerivation.Pbkdf2(
                password: enteredPassword,
                salt: salt,
                prf: KeyDerivationPrf.HMACSHA1,
                iterationCount: 10000,
                numBytesRequested: 256 / 8
            ));
            return encryptedPassw == storedPassword;
        }

        public void ChangePassword(User user)
        {
            _db.Users.Update(user);
            _db.SaveChanges();
        }

        public async Task<User> GetByIdToReportAsync(int id)
        {
            return await _db.Users.Where(u => u.Id == id).SingleOrDefaultAsync();
        }
    }
}
