﻿using OONApi.Models;
using System.Threading.Tasks;

namespace OONApi.Services
{
    public interface IDailyPriceHistoryService 
    {
        Task<bool> UpdateDailyPriceHistorys(DailyPriceHistory dailyPriceHistory);
        Task<bool> CreateDailyPriceHistory(DailyPriceHistory dailyPriceHistory);


    }
}
