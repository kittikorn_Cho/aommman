﻿using OONApi.Contracts.Requests.Queires;
using OONApi.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OONApi.Services
{
    public interface IReceptionPointRedemtionsService
    {
        public Task<bool> CreateReceptionPointRedemtions(ReceptionPointRedemtion receptionPointRedemtion);
        Task<ReceptionPointRedemtion> GetByIdAsync(int id);
        Task<bool> CancelReceptionPointRedemtions(int id);
        Task<ReceptionPointRedemtion> GetReceptionPointRedemtionByReceptionId(int id);
        Task<List<ReceptionPointRedemtion>> GetReceptionPointRedemtionsHistory(ReceptionPointRedemtionsHistorySearchQuery receptionPointRedemtionsHistory, PaginationFilter paginationFilter = null);

    }
}
