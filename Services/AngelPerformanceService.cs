﻿using DocumentFormat.OpenXml.Office2010.Excel;
using Microsoft.EntityFrameworkCore;
using OONApi.Contracts.Requests.Queires;
using OONApi.Contracts.Responses;
using OONApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace OONApi.Services
{
    public class AngelPerformanceService : IAngelPerformanceService
    {
        private readonly AppDbContext _db;

        public AngelPerformanceService(AppDbContext db)
        {
            _db = db;
        }

        public async Task<List<AngelPerformance>> GetAllAsync(AngelPerformanceSearchQuery angelPerformance, PaginationFilter paginationFilter = null)
        {
            if (paginationFilter == null)
            {
                return await _db.AngelPerformances.Include(c => c.CalendarDay).AsNoTracking().ToListAsync();
            }

            if (typeof(AngelPerformance).GetProperties().All(s => s.Name != paginationFilter.OrderBy.First().ToString().ToUpper() + paginationFilter.OrderBy.Substring(1)))
            {
                paginationFilter.OrderBy = "Id";
            }
            string direction = null;
            if (string.Equals(paginationFilter.OrderDirection, "desc", StringComparison.OrdinalIgnoreCase))
            {
                direction = "descending";
            }
            else
            {
                direction = "ascending";
            }

            var skip = (paginationFilter.PageNumber - 1) * paginationFilter.PageSize;

            IQueryable<AngelPerformance> query = _db.AngelPerformances.Include(c => c.CalendarDay).Include(a => a.Angel).ThenInclude(an => an.AngelType);
            if (angelPerformance.StartDate != null && angelPerformance.EndDate != null)
            {
                query = query.Where(c => c.CalendarDay.ToDay >= angelPerformance.StartDate && c.CalendarDay.ToDay <= angelPerformance.EndDate);
            }
            if (angelPerformance.AngelId != 0)
            {
                query = query.Where(c => c.AngelId == angelPerformance.AngelId);
            }
            if (angelPerformance.IsPay != null)
            {
                query = query.Where(c => c.IsPay == angelPerformance.IsPay);
            }
            if (!String.IsNullOrEmpty(angelPerformance.RfId))
            {
                query = query.Where(c => c.Angel.Rfid == angelPerformance.RfId);
            }

            paginationFilter.TotalRecords = query.Count();
            string sortBy = char.ToUpper(paginationFilter.OrderBy.First()) + paginationFilter.OrderBy.Substring(1).ToLower();
            if (paginationFilter == null)
            {
                return await query.AsNoTracking().ToListAsync();
            }
            return await query.OrderBy(sortBy + " " + direction).Skip(skip).Take(paginationFilter.PageSize).AsNoTracking().ToListAsync();
        }

        public async Task<AngelPerformance> GetByIdAsync(int id)
        {
            return await _db.AngelPerformances.Include(c => c.CalendarDay).Include(a => a.Angel).ThenInclude(an => an.AngelType).Where(a => a.Id == id).SingleOrDefaultAsync();
        }

        public async Task<AngelPerformance> GetByCalendarIdAndAngelIdAsync(int calendarId,int angelId)
        {
            return await _db.AngelPerformances.Include(c => c.CalendarDay).Include(a => a.Angel).ThenInclude(an => an.AngelType).Where(a => a.CarlendarDayId == calendarId && a.AngelId == angelId).SingleOrDefaultAsync();
        }

        public async Task<List<MassageAngel>> GetMassageAngelsDetailAsync(int CarlendarDayId, int AngelId)
        {
            return await _db.MassageAngels.Include(m => m.Massage).Include(m=>m.Room).Where(a => a.AngelId == AngelId).Where(c => c.Massage.CalendarDayId == CarlendarDayId).ToListAsync();
        }

        public async Task<List<AngelPerformanceDebt>> GetAngelPerformanceDebtsByAngelPerformanceId(int angelPerformanceId)
        {
            return await _db.AngelPerformanceDebts.Include(ap=>ap.AngelPerformance).ThenInclude(af=>af.Angel).Where(ap => ap.AngelPerformanceId == angelPerformanceId).ToListAsync();
        }

        public async Task<List<MassageAngel>> GetMassageAngelsDetailAsync(int CarlendarDayId, int AngelId, int angelPerfornaceId)
        {
            return await _db.MassageAngels.Include(m => m.Massage).Include(m => m.Room).Where(a => a.AngelId == AngelId).Where(c => c.Massage.CalendarDayId == CarlendarDayId).Where(a => a.AngelPerformanceId == angelPerfornaceId).ToListAsync();
        }

        public async Task<List<AngelPerformanceDeduct>> GetAngelPerformanceDeducts(int massageAngelId)
        {
            return await _db.AngelPerformanceDeducts.Where(a => a.MassageAngelId == massageAngelId).AsNoTracking().ToListAsync();
        }

        //public async Task<AngelPerformanceDeduct> GetAngelPerformanceDeductsTransaction(int count, int angelPerformanceId)
        //{
        //    return await _db.AngelPerformanceDeducts.Where(a => a.AngelPerformanceId == angelPerformanceId)
        //}

        public async Task<AngelPerformance> CreateOrUpdateByIdAsync(MassageAngel massageAngel, int calendarDayId)
        {
            var AngelPerformances = _db.AngelPerformances.Where(i => i.CarlendarDayId == calendarDayId).Where(i => i.AngelId == massageAngel.AngelId).FirstOrDefault();
            if (AngelPerformances == null)
            {
                AngelPerformance angelPerformance = new AngelPerformance();
                angelPerformance.CarlendarDayId = calendarDayId;
                angelPerformance.AngelId = massageAngel.AngelId;

                _db.AngelPerformances.Add(angelPerformance);
                var created = await _db.SaveChangesAsync();
                return angelPerformance;
            }
            else
            {
                var isPayAngelPeroformance = await _db.AngelPerformances.Where(i => i.CarlendarDayId == calendarDayId).Where(i => i.AngelId == massageAngel.AngelId && i.IsPay == true).ToListAsync();
                var historyAngelPerformance = await _db.AngelPerformances.Where(i => i.CarlendarDayId == calendarDayId).Where(i => i.AngelId == massageAngel.AngelId).ToListAsync();
                if (isPayAngelPeroformance.Count > 0 && historyAngelPerformance.Count == 1) //กลับมาทำงานใหม่
                {
                    AngelPerformance angelPerformance = new AngelPerformance();
                    angelPerformance.CarlendarDayId = calendarDayId;
                    angelPerformance.AngelId = massageAngel.AngelId;
                    angelPerformance.IsPerformanceNew = true;

                    _db.AngelPerformances.Add(angelPerformance);
                    var created = await _db.SaveChangesAsync();
                    return angelPerformance;
                }
                else if (isPayAngelPeroformance.Count == 2 && historyAngelPerformance.Count == 2) //กลับมาทำงานใหม่
                {
                        AngelPerformance angelPerformance = new AngelPerformance();
                        angelPerformance.CarlendarDayId = calendarDayId;
                        angelPerformance.AngelId = massageAngel.AngelId;
                        angelPerformance.IsPerformanceNew = true;

                        _db.AngelPerformances.Add(angelPerformance);
                        var created = await _db.SaveChangesAsync();
                        return angelPerformance;
                }
                else if (isPayAngelPeroformance.Count == 3 && historyAngelPerformance.Count ==3) //กลับมาทำงานใหม่
                {
                    AngelPerformance angelPerformance = new AngelPerformance();
                    angelPerformance.CarlendarDayId = calendarDayId;
                    angelPerformance.AngelId = massageAngel.AngelId;
                    angelPerformance.IsPerformanceNew = true;

                    _db.AngelPerformances.Add(angelPerformance);
                    var created = await _db.SaveChangesAsync();
                    return angelPerformance;
                }
                else
                {
                    var newAngelPerformance = await _db.AngelPerformances.Where(i => i.CarlendarDayId == calendarDayId).Where(i => i.AngelId == massageAngel.AngelId && i.IsPay == false).SingleOrDefaultAsync();
                    return newAngelPerformance;
                }
            }

            //return AngelPerformances;
            //else
            //{
            //    AngelPerformances.AngelId = massageAngel.AngelId;
            //    AngelPerformances.TotalRound += massageAngel.Round;
            //    TotalAngelPerformanceResponse total = await TotalAngelPerformance(AngelPerformances.AngelId, massageAngel.Round);
            //    AngelPerformances.TotalFee += total.TotalFee;
            //    AngelPerformances.TotalWage += total.TotalWage;
            //    _db.AngelPerformances.Update(AngelPerformances);
            //    var updated = await _db.SaveChangesAsync();
            //    return AngelPerformances;
            //}
        }

        public async Task<bool> UpdateAsync(AngelPerformance angelPerformance)
        {
            _db.AngelPerformances.Update(angelPerformance);
            var updated = await _db.SaveChangesAsync();
            return updated > 0;
        }

        public async Task<List<AngelPerformanceDebt>> GetPerformanceDebtByAngelPerformanceIdAsync(int id)
        {
            return await _db.AngelPerformanceDebts.Where(a => a.AngelPerformanceId == id).ToListAsync();
        }

        public async Task<bool> DeleteAngelPerformanceDebts(int id)
        {
            var deleted = 0;
            var angelPerformanceDebt = await _db.AngelPerformanceDebts.Where(a => a.Id == id).SingleOrDefaultAsync();
            _db.AngelPerformanceDebts.Remove(angelPerformanceDebt);
            deleted = await _db.SaveChangesAsync();

            return deleted > 0;
        }

        public async Task<List<AngelPerformanceDeduct>> GetPerformanceDeductByAngelPerformanceIdAsync(int id)
        {
            return await _db.AngelPerformanceDeducts.Where(a => a.AngelPerformanceId == id).OrderByDescending(a => a.Id).ToListAsync();
        }

        public async Task<List<DeductType>> GetAllDeductAsync()
        {
            return await _db.DeductTypes.AsNoTracking().ToListAsync();
        }

        public async Task<bool> DeleteAsync(int angelPerformanceId)
        {
            var deleted = 0;
            var angelPerformance = await GetPerformanceDeductByAngelPerformanceIdAsync(angelPerformanceId);
            if (angelPerformance != null)
            {
                var deduct = await GetAllDeductAsync();

                List<int> ids = new List<int>();

                int count = deduct.Count;

                int ii = 0;
                foreach(var i in angelPerformance)
                {
                    ii++;
                    if (count == ii)
                    {
                        ids.Add(i.Id);
                        break;
                    }
                    else
                    {
                        ids.Add(i.Id);
                    }
                    
                }

                foreach (var i in ids)
                {
                    foreach (var item in angelPerformance)
                    {
                        if (item.Id == i)
                        {
                            _db.AngelPerformanceDeducts.Remove(item);
                            deleted = await _db.SaveChangesAsync();
                        }
                    }
                }

                var _angelPerformance = await GetByIdAsync(angelPerformanceId);
                if (_angelPerformance != null)
                {
                    _db.AngelPerformances.Remove(_angelPerformance);
                    deleted = await _db.SaveChangesAsync();
                }
            }
            return deleted > 0;
        }

        public async Task<TotalAngelPerformanceResponse> TotalAngelPerformance(int angelId, decimal round)
        {
            var angel = await _db.Angels.Include(i => i.AngelType).Where(i => i.Id == angelId).SingleOrDefaultAsync();
            TotalAngelPerformanceResponse total = new TotalAngelPerformanceResponse();
            total.TotalFee = angel.AngelType.Fee * round;
            total.TotalWage = angel.AngelType.Wage * round;
            return total;
        }

        public async Task<bool> CreatePerformanceDebtAsync(List<AngelPerformanceDebt> angelPerformanceDebts)
        {
            _db.AngelPerformanceDebts.AddRange(angelPerformanceDebts);
            var created = await _db.SaveChangesAsync();
            return created > 0;
        }

        public async Task<bool> UpdateAngelDebtByAngelId(AngelPerformanceDebt angelPerformanceDebt)
        {
            var angelperformance = await _db.AngelPerformances.Where(a => a.Id == angelPerformanceDebt.AngelPerformanceId).SingleOrDefaultAsync();
            var updated = 0;
            if(angelperformance != null)
            {
                var angelDebt = await _db.AngelDebts.Where(ad => ad.AngelId == angelperformance.AngelId && ad.DebtTypeId == angelPerformanceDebt.DebtTypeId).SingleOrDefaultAsync();
                angelDebt.Fee -= angelPerformanceDebt.Fee;
                _db.AngelDebts.Update(angelDebt);
                updated = await _db.SaveChangesAsync();
            }
            return updated > 0;
        }
        public decimal SummaryTotalRoundMonthByAngel(int angelId,int calendarId)
        {
            decimal round = _db.AngelPerformances.Where(an => an.AngelId == angelId && an.CarlendarDayId == calendarId).Sum(i => i.TotalRound);

            return round;
        }

        public async Task<decimal> TotalAngelPerformanceDeduct(int angelPerformanceId)
        {
            List<AngelPerformanceDeduct> angelPerformanceDeducts = await _db.AngelPerformanceDeducts.Where(a => a.AngelPerformanceId == angelPerformanceId).ToListAsync();
            decimal total = 0;
            foreach (var item in angelPerformanceDeducts)
            {
                total += item.Fee;
            }
            return total;
        }

        public async Task<AngelPerformanceDeduct> GetAngelPerformanceDeductByAngelPerformanceIdAndDeductTypeId(int massageAngelId, int deductTypeId)
        {
            var angelPerformanceDeduct = await _db.AngelPerformanceDeducts.Where(a => a.MassageAngelId == massageAngelId && a.DeductTypeId == deductTypeId).SingleOrDefaultAsync();

            return angelPerformanceDeduct;
        }

        public async Task<AngelPerformanceDeduct> GetAngelPerformanceDeductById(int id)
        {
            return await _db.AngelPerformanceDeducts.Where(a => a.Id == id).SingleOrDefaultAsync();
        }

        public async Task<bool> UpdateAngelPerformanceDeduct(AngelPerformanceDeduct angelPerformanceDeduct)
        {
            var updated = 0;
            _db.AngelPerformanceDeducts.Update(angelPerformanceDeduct);
            updated = await _db.SaveChangesAsync();

            return updated > 0;
        }
        public async Task<bool> DeleteAngelPerformance(int id)
        {
            var deleted = 0;
            var angelPerformance = await GetByIdAsync(id);
            if (angelPerformance != null)
            {
                _db.AngelPerformances.Remove(angelPerformance);
                deleted = await _db.SaveChangesAsync();
            }

            return deleted > 0;
        }

        public async Task<int> GetCountMassageAngel(int angelperformanceId)
        {
            return await _db.MassageAngels.Where(ma => ma.AngelPerformanceId == angelperformanceId).CountAsync();
        }
    }
}
