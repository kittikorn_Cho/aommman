﻿using OONApi.Contracts.Requests.Queires;
using OONApi.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OONApi.Services
{
    public interface IAngelDoctorService
    {
        Task<List<AngelDoctor>> GetAllAsync(AngelDoctorSearchQuery angelDoctor, PaginationFilter paginationFilter = null);
        Task<AngelDoctor> GetByIdAsync(int id);
        Task<bool> CreateAsync(AngelDoctor angelDoctor);
        Task<bool> UpdateAsync(AngelDoctor angelDoctor);
        Task<bool> DeleteAsync(int id);
    }
}
