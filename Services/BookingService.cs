﻿using OONApi.Models;
using System.Collections.Generic;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System;
using OONApi.Contracts.Responses;

namespace OONApi.Services
{
    public class BookingService : IBookingService
    {
        private readonly AppDbContext _db;

        public BookingService(AppDbContext db)
        {
            _db = db;
        }
        public async Task<bool> Booking(Booking booking)
        {
            _db.Bookings.Add(booking);
            var created = await _db.SaveChangesAsync();
            return created > 0;
        }
        public async Task<bool> UpdateAsync(Booking booking)
        {
            _db.Bookings.Update(booking);
            var updated = await _db.SaveChangesAsync();
            return updated > 0;
        }
        public async Task<List<Booking>> GetAllAsync()
        {
            return await _db.Bookings.ToListAsync();
        }
        public async Task<Booking> GetByIdAsync(int id)
        {
            return await _db.Bookings.Where(bk => bk.Id == id).SingleOrDefaultAsync();
        }
        public async Task<List<Booking>> GetBookingByDateTimeAndRoomId(DateTime startDate, DateTime endDate, int? roomId = null)
        {
            IQueryable<Booking> query = _db.Bookings.Where(bk => bk.Flag != false);

            query = query.Where(bk => ((bk.StartDate <= endDate && bk.StartDate >= startDate) || (bk.EndDate >= startDate && bk.EndDate <= endDate) || (bk.StartDate >= startDate && bk.EndDate <= endDate)) && bk.RoomId == roomId);
            
            return await query.ToListAsync();
        }
        public async Task<List<Booking>> GetBookingByStartDateAndStartTimeEndTime(int id, DateTime startDate, TimeSpan startTime, TimeSpan endTime, int roomId)
        {
            return await _db.Bookings.Where(bk => bk.Id != id && bk.StartDate == startDate && (bk.StartTime >= startTime && bk.EndTime < endTime) && bk.RoomId == roomId).ToListAsync();
        }

        public async Task<List<BookingDayResponse>> GetBookingByDay(DateTime startDate, DateTime endDate) 
        {
            List<int> id = await _db.Bookings.Include(r => r.Room).Where(bk => (bk.StartDate <= endDate && bk.StartDate >= startDate) || (bk.EndDate >= startDate && bk.EndDate <= endDate) || (bk.StartDate >= startDate && bk.EndDate <= endDate)).Select(r => r.RoomId).Distinct().ToListAsync();

            List<BookingDayResponse> bookingDays = new List<BookingDayResponse>();

            foreach (var day in id)
            {
                BookingDayResponse bookingDay = new BookingDayResponse();
                string room = await _db.Rooms.Where(i => i.Id == day).Select(r=>r.RoomNo).SingleAsync();

                bookingDay.RoomNo = room;
                bookingDay.RoomId = day;

                bookingDays.Add(bookingDay);
            }

            return bookingDays;
        }

        public async Task<List<Booking>> GetListBooking(DateTime startDate, DateTime endDate, int roomId) 
        {
            return await _db.Bookings.Where(bk => ((bk.StartDate <= endDate && bk.StartDate >= startDate) || (bk.EndDate >= startDate && bk.EndDate <= endDate) || (bk.StartDate >= startDate && bk.EndDate <= endDate)) && bk.RoomId == roomId).ToListAsync();
        }
        public async Task<bool> DeleteAsync(int id) 
        {
            var deleted = 0;
            var booking = await GetByIdAsync(id);
            if(booking != null) 
            {
                _db.Bookings.Remove(booking);
                deleted = await _db.SaveChangesAsync();
            }
            return deleted > 0;
        }
        public async Task<bool> BookingAngels(BookingAngel bookingAngel) 
        {
            _db.BookingAngels.Add(bookingAngel);
            var created = await _db.SaveChangesAsync();
            return created > 0;
        }
        public bool GetBookingAngels(DateTime startDate, DateTime endDate, int angelId) 
        {
            bool check = false;
            int count  = _db.BookingAngels.Where(ba => ((ba.StartDate <= endDate && ba.StartDate >= startDate) || (ba.EndDate >= startDate && ba.EndDate <= endDate) || (ba.StartDate >= startDate && ba.EndDate <= endDate)) && ba.AngelId == angelId).Count();
            if (count > 0)
            {
                check = true;
            }
            return check;
        }
        public async Task<List<BookingAngel>> GetBookingAngelsByBookingId(int bookingId) 
        {
            return await _db.BookingAngels.Include(b => b.Booking).Where(ba => ba.BookingId == bookingId).ToListAsync();
        }
        public async Task<BookingAngel> GetBookingAngelsByIdAsync(int id)
        {
            return await _db.BookingAngels.Where(ba => ba.Id == id).SingleOrDefaultAsync();
        }
        public async Task<bool> DeleteBookingAngels(int id)
        {
            var deleted = 0;
            var bookingAngel = await GetBookingAngelsByIdAsync(id);
            if (bookingAngel != null)
            {
                _db.BookingAngels.Remove(bookingAngel);
                deleted = await _db.SaveChangesAsync();
            }
            return deleted > 0;
        }
    }
}
