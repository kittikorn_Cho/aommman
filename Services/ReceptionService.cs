﻿using Microsoft.EntityFrameworkCore;
using OONApi.Contracts.Requests.Queires;
using OONApi.Contracts.Responses;
using OONApi.Enum;
using OONApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using static OONApi.Contracts.ApiRoutes.ApiRoutes;

namespace OONApi.Services
{
    public class ReceptionService : IReceptionService
    {
        private readonly AppDbContext _db;

        public ReceptionService(AppDbContext db)
        {
            _db = db;
        }

        public async Task<List<Reception>> GetAllAsync(ReceptionSearchQuery reception, PaginationFilter paginationFilter = null)
        {
            if (paginationFilter == null)
            {
                return await _db.Receptions.Where(r => r.Status != (int)EAngelStatus.Cancel).OrderBy(r => r.Code).OrderBy(r => Convert.ToInt32(r.Code.Replace(r.Code, ""))).AsNoTracking().ToListAsync();
            }

            if (typeof(Reception).GetProperties().All(s => s.Name != paginationFilter.OrderBy.First().ToString().ToUpper() + paginationFilter.OrderBy.Substring(1)))
            {
                paginationFilter.OrderBy = "Id";
            }
            string direction = null;
            if (string.Equals(paginationFilter.OrderDirection, "desc", StringComparison.OrdinalIgnoreCase))
            {
                direction = "descending";
            }
            else
            {
                direction = "ascending";
            }

            var skip = (paginationFilter.PageNumber - 1) * paginationFilter.PageSize;

            IQueryable<Reception> query = _db.Receptions.Where(r => r.Status != (int)EAngelStatus.Cancel).OrderBy(r => Convert.ToInt32(r.Code.Replace(r.Code, "")));

            if(reception.Id > 0)
            {
                query = query.Where(r => r.Id == reception.Id);
            }
            if (!String.IsNullOrEmpty(reception.Code))
            {
                query = query.Where(r => r.Code.Contains(reception.Code));
            }
            if (!String.IsNullOrEmpty(reception.Firstname))
            {
                query = query.Where(r => r.Firstname.Contains(reception.Firstname));
            }
            if (!String.IsNullOrEmpty(reception.Lastname))
            {
                query = query.Where(r => r.Lastname.Contains(reception.Lastname));
            }
            if (!String.IsNullOrEmpty(reception.Nickname))
            {
                query = query.Where(r => r.Nickname.Contains(reception.Nickname));
            }
            if (reception.Tel != null)
            {
                query = query.Where(r => r.Tel.Contains(reception.Tel));
            }

            if (!String.IsNullOrEmpty(paginationFilter.Keyword))
            {
                query = query.Where(r => r.Firstname.Contains(paginationFilter.Keyword) || r.Lastname.Contains(paginationFilter.Keyword) || r.Nickname.Contains(paginationFilter.Keyword));
            }
            paginationFilter.TotalRecords = query.Count();
            string sortBy = char.ToUpper(paginationFilter.OrderBy.First()) + paginationFilter.OrderBy.Substring(1).ToLower();
            if (paginationFilter == null)
            {
                return await query.AsNoTracking().ToListAsync();
            }
            return await query.OrderBy(sortBy + " " + direction).Skip(skip).Take(paginationFilter.PageSize).AsNoTracking().ToListAsync();
        }

        public async Task<Reception> GetByIdAsync(int id)
        {
            return await _db.Receptions.Where(a => a.Id == id).SingleOrDefaultAsync();
        }

        public async Task<bool> CreateAsync(Reception reception)
        {
            _db.Receptions.Add(reception);
            var created = await _db.SaveChangesAsync();
            return created > 0;
        }

        public async Task<bool> UpdateAsync(Reception reception)
        {
            _db.Receptions.Update(reception);
            var updated = await _db.SaveChangesAsync();
            return updated > 0;
        }
        public async Task<bool> DeleteAsync(int id)
        {
            var deleted = 0;
            var reception = await GetByIdAsync(id);
            if (reception != null)
            {
                _db.Receptions.Remove(reception);
                deleted = await _db.SaveChangesAsync();
            }
            return deleted > 0;
        }

        public decimal GetSellAngelTypes(int receptionId,int angelTypeId,List<int> calendarIds)
        {
            decimal sumRound = _db.MassageAngels.Include(m => m.Massage).ThenInclude(ma=>ma.CalendarDay).Include(m => m.Angel).ThenInclude(ma => ma.AngelType).Where(m => m.ReceptionId == receptionId && m.Angel.AngelTypeId == angelTypeId && calendarIds.Contains(m.Massage.CalendarDayId)).Sum(i => i.Round);
            //decimal sumRound = _db.MassageAngels.Include(m => m.Massage).Include(m => m.Angel).ThenInclude(ma => ma.AngelType).Where(m => m.ReceptionId == receptionId && m.Angel.AngelTypeId == angelTypeId).Sum(i => i.Round);
            return sumRound;
        }

        public int GetSellRoomTypeSuite(int receptionId, List<int> calendarIds)
        {
            //int countSuite = _db.MassageRooms.Include(m => m.Massage).Include(m => m.Room).ThenInclude(mr => mr.RoomType).Where(m => m.ReceptionId == receptionId && m.Room.RoomTypeId == (int)ERoomType.Suite).Count();
            int countSuite = _db.MassageRooms.Include(m => m.Massage).Include(m => m.Room).ThenInclude(mr => mr.RoomType).Where(m => m.ReceptionId == receptionId && (m.Room.RoomTypeId == (int)ERoomType.Suite || m.Room.RoomTypeId == (int)ERoomType.Suite3 || m.Room.RoomTypeId == (int)ERoomType.Suite4 || m.Room.RoomTypeId == (int)ERoomType.Suite5 || m.Room.RoomTypeId == (int)ERoomType.Suite6 || m.Room.RoomTypeId == (int)ERoomType.Suite7 || m.Room.RoomTypeId == (int)ERoomType.Suite8 || m.Room.RoomTypeId == (int)ERoomType.Suite9 || m.Room.RoomTypeId == (int)ERoomType.Suite10 || m.Room.RoomTypeId == (int)ERoomType.C3 || m.Room.RoomTypeId == (int)ERoomType.C5) && calendarIds.Contains(m.Massage.CalendarDayId)).Count();
            return countSuite;
        }

        public decimal GetSellMember(int receptionId, List<int> calendarIds)
        {
            decimal sumMember = 0;

            //sumMember = _db.MemberTransactions.Include(m => m.Member).Include(m => m.Reception).Where(m => m.ReceptionId == receptionId && (m.IsPaid == true && m.IsUnPaid == false)).Sum(i => i.Total);
            //sumMember += _db.MemberTransactions.Include(m => m.Member).Include(m => m.Reception).Where(m => m.ReceptionId == receptionId && (m.IsPaid == true && m.IsUnPaid == true)).Sum(i => i.Total + i.CashTotal + i.CreditTotal + i.EntertainTotal);

            sumMember = _db.MemberTransactions.Include(m => m.Member).Include(m => m.Reception).Where(m => m.ReceptionId == receptionId && (m.IsPaid == true && m.IsUnPaid == false) && calendarIds.Contains(m.CalendarDayId)).Sum(i => i.Total);
            sumMember += _db.MemberTransactions.Include(m => m.Member).Include(m => m.Reception).Where(m => m.ReceptionId == receptionId && (m.IsPaid == true && m.IsUnPaid == true) && calendarIds.Contains(m.CalendarDayId)).Sum(i => i.Total);

            return sumMember;
        }
    }
}