﻿using OONApi.Contracts.Responses;
using OONApi.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OONApi.Services
{
    public interface IBookingService
    {
        Task<bool> Booking(Booking booking);
        Task<bool> UpdateAsync(Booking booking);
        Task<List<Booking>> GetAllAsync();
        Task<Booking> GetByIdAsync(int id);
        Task<List<Booking>> GetBookingByDateTimeAndRoomId(DateTime startDate, DateTime endDate, int? roomId);
        Task<List<Booking>> GetBookingByStartDateAndStartTimeEndTime(int id, DateTime startDate, TimeSpan startTime, TimeSpan endTime, int roomId);
        Task<List<BookingDayResponse>> GetBookingByDay(DateTime startDate, DateTime endDate);
        Task<List<Booking>> GetListBooking(DateTime startDate, DateTime endDate, int roomId);
        Task<bool> DeleteAsync(int id);
        Task<bool> BookingAngels(BookingAngel bookingAngel);
        bool GetBookingAngels(DateTime startDate, DateTime endDate, int angelId);
        Task<List<BookingAngel>> GetBookingAngelsByBookingId(int bookingId);
        Task<BookingAngel> GetBookingAngelsByIdAsync(int id);
        Task<bool> DeleteBookingAngels(int id);
    }
}
