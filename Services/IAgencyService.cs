﻿using DocumentFormat.OpenXml.Office2010.ExcelAc;
using OONApi.Models;
using System.Threading.Tasks;
using System.Collections.Generic;
using OONApi.Contracts.Requests.Queires;
using OONApi.Contracts.Responses;
using static OONApi.Contracts.ApiRoutes.ApiRoutes;

namespace OONApi.Services
{
    public interface IAgencyService
    {
        Task<List<Agency>> GetAllAsync(AgencySearchQuery agency, PaginationFilter paginationFilter = null);
        Task<Agency> GetByIdAsync(int id);
        Task<bool> CreateAsync(Agency agency);
        Task<bool> UpdateAsync(Agency agency);
        Task<List<AgencyType>> GetAllAgencyTypesAsync();
        Task<List<AgencyPerformance>> GetAgenciesPerformanceByCalendarDay(int calendarDayIds, int agenciesId);
        Task<List<AgencyPerformance>> GetAllAgenciesPerformanceAsync(List<int> calendarDayIds);
        
    }
}
