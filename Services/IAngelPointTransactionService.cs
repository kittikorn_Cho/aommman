﻿using OONApi.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OONApi.Services
{
    public interface IAngelPointTransactionService
    {
        Task<bool> CreateAngelPointTransaction(AngelPointTransaction angelPointTransaction);
        Task<AngelPointTransaction> GetByIdAsync(int id);
        Task<List<AngelPointTransaction>> GetAngelPointTransactionByMassageAngelId(List<int> massageAngelId);
        Task<bool> DeleteAngelPointTransation(int id);
        Task<AngelPointTransaction> GetAngelPointTransactionByMaasageAngelIdAndAngelId(int maasageAngelId, int angelId);
    }
}
