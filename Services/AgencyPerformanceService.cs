﻿using DocumentFormat.OpenXml.Office2010.Excel;
using Microsoft.EntityFrameworkCore;
using OONApi.Contracts.Requests.Queires;
using OONApi.Contracts.Responses;
using OONApi.Enum;
using OONApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using static OONApi.Contracts.ApiRoutes.ApiRoutes;

namespace OONApi.Services
{
    public class AgencyPerformanceService : IAgencyPerformanceService
    {
         private readonly AppDbContext _db;

        public AgencyPerformanceService(AppDbContext db)
        {
            _db= db;
        }

        public async Task<bool> CreatePaymentAgencyAsync(AgencyPerformance agencyPerformance)
        {
            _db.AgencyPerformances.Add(agencyPerformance);
            var created = await _db.SaveChangesAsync();
            return created > 0;
        }

        public async Task<List<AgencyPerformance>> GetAllAgencyPerformances(AgencyPerformanceSearchQuery agencyPerformance, PaginationFilter paginationFilter = null)
        {
            if (paginationFilter == null)
            {
                return await _db.AgencyPerformances.Include(c => c.CalendarDay).Include(a => a.Agency).ThenInclude(a => a.AgencyType).AsNoTracking().ToListAsync();
            }

            if (typeof(AgencyPerformance).GetProperties().All(s => s.Name != paginationFilter.OrderBy.First().ToString().ToUpper() + paginationFilter.OrderBy.Substring(1)))
            {
                paginationFilter.OrderBy = "Id";
            }
            string direction = null;
            if (string.Equals(paginationFilter.OrderDirection, "desc", StringComparison.OrdinalIgnoreCase))
            {
                direction = "descending";
            }
            else
            {
                direction = "ascending";
            }

            var skip = (paginationFilter.PageNumber - 1) * paginationFilter.PageSize;

            IQueryable<AgencyPerformance> query = _db.AgencyPerformances.Include(c => c.CalendarDay).Include(a => a.Agency).ThenInclude(a => a.AgencyType);
            if (agencyPerformance.StartDate != null && agencyPerformance.EndDate != null)
            {
                query = query.Where(c => c.CalendarDay.ToDay >= agencyPerformance.StartDate && c.CalendarDay.ToDay <= agencyPerformance.EndDate);
            }
            if (agencyPerformance.AgenciesId != null)
            {
                query = query.Where(r => r.AgenciesId == agencyPerformance.AgenciesId);
            }
            if (agencyPerformance.IsPay != null)
            {
                query = query.Where(c => c.IsPay == agencyPerformance.IsPay);
            }
            if (agencyPerformance.IsUnPaid != null)
            {
                query = query.Where(c => c.IsUnPaid == agencyPerformance.IsUnPaid);
            }

            paginationFilter.TotalRecords = query.Count();
            string sortBy = char.ToUpper(paginationFilter.OrderBy.First()) + paginationFilter.OrderBy.Substring(1).ToLower();
            if (paginationFilter == null)
            {
                return await query.AsNoTracking().ToListAsync();
            }
            return await query.OrderBy(sortBy + " " + direction).Skip(skip).Take(paginationFilter.PageSize).AsNoTracking().ToListAsync();
        }
        public async Task<AgencyPerformance> GetByIdAsync(int id)
        {
            return await _db.AgencyPerformances.Include(cld => cld.CalendarDay).Include(agc => agc.Agency).ThenInclude(agc => agc.AgencyType).Where(agcpfm => agcpfm.Id == id).SingleOrDefaultAsync();
        }
        public async Task<bool> UpdateAsync(AgencyPerformance agencyPerformance)
        {
            _db.AgencyPerformances.Update(agencyPerformance);
            var updated = await _db.SaveChangesAsync();
            return updated > 0;
        }
        public async Task<bool> DeleteAgencyPerformances(int id) 
        {
            var deleted = 0;
            var agencyPerformances = await GetByIdAsync(id);
            if (agencyPerformances != null)
            {
                _db.AgencyPerformances.Remove(agencyPerformances);
                deleted =  await _db.SaveChangesAsync();
            }
            return deleted > 0;
        }
        public async Task<AgencyPerformance> GetAgencyPerformancesByCalendarIdAndAgenciesId(int calendarDaysId, int id)
        {
           return await _db.AgencyPerformances.Include(cld => cld.CalendarDay).Include(agc => agc.Agency).ThenInclude(agc => agc.AgencyType).Where(agcpfm => agcpfm.CarlendarDayId == calendarDaysId && agcpfm.AgenciesId == id && agcpfm.IsPay == false).SingleOrDefaultAsync();
        }
        public async Task<List<AgencyPerformance>> GetAllByCalendarDays(List<int> calendarDayIds)
        {
            return await _db.AgencyPerformances.Include(cld => cld.CalendarDay).Include(agc => agc.Agency).ThenInclude(agc => agc.AgencyType).Where(agcpfm => calendarDayIds.Contains(agcpfm.CarlendarDayId)).AsNoTracking().ToListAsync();
        }
    }
}
