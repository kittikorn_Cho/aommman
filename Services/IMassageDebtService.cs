﻿using OONApi.Models;
using System.Threading.Tasks;

namespace OONApi.Services
{
    public interface IMassageDebtService
    {

        Task<bool> CreateAsync(MassageDebt massageDebt);
        Task<bool> UpdateAsync(MassageDebt memberDebt);
        Task<bool> DeleteAsync(int id);
        Task<MassageDebt> GetByIdAsync(int id);
        Task<MassageDebt> GetMassageDebtByMassageId(int MassageId);
    }
}
