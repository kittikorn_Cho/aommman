﻿using OONApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Linq.Dynamic.Core;
using OONApi.Contracts.Requests.Queires;
using OONApi.Enum;
using static OONApi.Contracts.ApiRoutes.ApiRoutes;
using OONApi.Contracts.Responses;
using Microsoft.IdentityModel.Tokens;
using DocumentFormat.OpenXml.Office2010.ExcelAc;

namespace OONApi.Services
{
    public class MassageService : IMassageService
    {
        private readonly AppDbContext _db;
        public MassageService(AppDbContext db)
        {
            _db = db;
        }

        public async Task<bool> CreateAsync(Massage massage)
        {
            _db.Massages.Add(massage);
            var created = await _db.SaveChangesAsync();
            return created > 0;
        }
        public async Task<bool> UpdateAsync(Massage massage)
        {
            _db.Massages.Update(massage);
            var updated = await _db.SaveChangesAsync();
            return updated > 0;
        }
        public async Task<Massage> GetById(int id)
        {
            return await _db.Massages.Include(agc => agc.Agency).Include(c => c.Countries).Include(m => m.CalendarDay).Where(m => m.Id == id).SingleOrDefaultAsync();
            //return await _db.Massages.Include(m => m.CalendarDay).Where(m => m.Id == id).SingleOrDefaultAsync();
        }
      
        public async Task<List<MassageAngel>> GetMassageAngelBymassageId(int massageId)
        {
            return await _db.MassageAngels.Where(m => m.MassageId == massageId).ToListAsync();
        }
        public async Task<List<MassageAngel>> GetMassageAngelByAngelIdAsync(int angelId,int CalendarDayId)
        {
            return await _db.MassageAngels.Include(m => m.Massage).Where(m => m.AngelId == angelId && m.Massage.CalendarDayId == CalendarDayId).ToListAsync();
        }
        public async Task<MassageRoom> GetMassgeRoomBymassageId(int massageId)
        {
            return await _db.MassageRooms.Where(m => m.MassageId == massageId).SingleOrDefaultAsync();
        }
        public async Task<List<MassageAngel>> GetMassageAngelByMassageIdAndRoomId(int massageId , int RoomId)
        {
            return await _db.MassageAngels.Include(ma => ma.Massage).Include(ma => ma.Reception).Include(ma => ma.Angel).ThenInclude(an => an.AngelType).Include(ma => ma.Room).ThenInclude(r => r.RoomType).Where(m => m.MassageId == massageId && m.RoomId == RoomId).ToListAsync();
        }

        public async Task<List<MassageAngel>> GetMassageAngelByMassageIdAndRoomIdAndCheckoutIsNull(int massageId, int RoomId)
        {
            return await _db.MassageAngels.Include(ma => ma.Massage).Include(ma => ma.Reception).Include(ma => ma.Angel).ThenInclude(an => an.AngelType).Include(ma => ma.Room).ThenInclude(r => r.RoomType).Where(m => m.MassageId == massageId && m.RoomId == RoomId && m.CheckOutTime == null).ToListAsync();
        }
        public async Task<bool> CheckInMassageAngelList(List<MassageAngel> massageAngels)
        {
            _db.MassageAngels.AddRange(massageAngels);
            var created = await _db.SaveChangesAsync();
            return created > 0;
        }
        public async Task<MassageAngel> GetMassageAngelById(int id)
        {
            return await _db.MassageAngels.Include(ma => ma.Massage).Include(ma => ma.Reception).Include(ma => ma.Angel).ThenInclude(an => an.AngelType).Include(ma => ma.Room).ThenInclude(r => r.RoomType).Where(ma => ma.Id == id).SingleOrDefaultAsync();
        }
        
        public async Task<bool> UpdateMassageAngel(MassageAngel massageAngel)
        {
            _db.MassageAngels.Update(massageAngel);
            var updated = await _db.SaveChangesAsync();
            return updated > 0;
        }
        public async Task<bool> CheckInMassageRoom(MassageRoom massageRooms)
        {
            _db.MassageRooms.Add(massageRooms);
            var created = await _db.SaveChangesAsync();
            return created > 0;
        }
        public async Task<bool> UpdateMassageRoom(MassageRoom massageRoom)
        {
            _db.MassageRooms.Update(massageRoom);
            var updated = await _db.SaveChangesAsync();
            return updated > 0;
        }
        public async Task<MassageRoom> GetMassageRoomById(int id)
        {
            return await _db.MassageRooms.Include(mr => mr.Room).ThenInclude(r => r.RoomType).Include(mr => mr.Massage).Include(m => m.Reception).Where(mr => mr.Id == id).SingleOrDefaultAsync();
        }
  

        public async Task<MassageRoom> GetMassageRoomByMassageId(int id)
        {
            return await _db.MassageRooms.Include(mr => mr.Room).ThenInclude(r => r.RoomType).Include(mr => mr.Massage).Include(m => m.Reception).Where(mr => mr.MassageId == id).AsNoTracking().FirstOrDefaultAsync();
        }
        public async Task<MassageRoom> GetMassageRoomByNewMassageId(int id)
        {
            return await _db.MassageRooms.Include(mr => mr.Room).ThenInclude(r => r.RoomType).Include(mr => mr.Massage).Include(m => m.Reception).Where(mr => mr.NewMassageId == id).AsNoTracking().FirstOrDefaultAsync();
        }

        public async Task<List<MassageAngel>> GetmassageAngelByRoomIdAndMassageId(int roomId,int massageId)
        {
            return await _db.MassageAngels.Include(ma => ma.Massage).Include(ma => ma.Reception).Include(ma => ma.Angel).ThenInclude(an => an.AngelType).Include(ma => ma.Room).ThenInclude(r => r.RoomType).Where(ma => ma.MassageId == massageId && ma.RoomId == roomId).ToListAsync();
        }

        public async Task<List<MassageAngel>> GetMassageAngelByMassageId(int id)
        {
           return await _db.MassageAngels.Include(ma => ma.Massage).Include(ma => ma.Reception).Include(ma => ma.Angel).ThenInclude(an => an.AngelType).Include(ma => ma.Room).ThenInclude(r => r.RoomType).Where(ma => ma.Massage.Id == id).AsNoTracking().ToListAsync();
        }
        public async Task<List<MassageAngel>> GetMassageAngelByMassageId(int id, int roomId)
        {
            return await _db.MassageAngels.Include(ma => ma.Massage).Include(ma => ma.Reception).Include(ma => ma.Angel).ThenInclude(an => an.AngelType).Include(ma => ma.Room).ThenInclude(r => r.RoomType).Where(ma => ma.Massage.Id == id && ma.RoomId == roomId).AsNoTracking().ToListAsync();
        }
        public async Task<List<MassageAngel>> GetMassageAngelByRoomId(int id, int roomId)
        {
            return await _db.MassageAngels.Include(ma => ma.Massage).Include(ma => ma.Reception).Include(ma => ma.Angel).ThenInclude(an => an.AngelType).Include(ma => ma.Room).ThenInclude(r => r.RoomType).Where(ma => ma.Massage.Id == id && ma.Room.ParentRoomId == roomId).AsNoTracking().ToListAsync();
        }
        public async Task<List<MassageRoom>> GetAllCheckInRoomSuite(CheckInSearchQuery search, PaginationFilter paginationFilter = null, int? calendarDayId = null)
        {
            if (paginationFilter == null)
            {
                return await _db.MassageRooms.Include(ma => ma.Massage).Include(ma => ma.Room).ThenInclude(r => r.RoomType).Where(ma=>ma.Massage.CalendarDayId == calendarDayId && ma.NewMassageId == null).AsNoTracking().ToListAsync();
            }

            if (typeof(Angel).GetProperties().All(s => s.Name != paginationFilter.OrderBy.First().ToString().ToUpper() + paginationFilter.OrderBy.Substring(1)))
            {
                paginationFilter.OrderBy = "Id";
            }
            string direction = null;
            if (string.Equals(paginationFilter.OrderDirection, "desc", StringComparison.OrdinalIgnoreCase))
            {
                direction = "descending";
            }
            else
            {
                direction = "ascending";
            }

            var skip = (paginationFilter.PageNumber - 1) * paginationFilter.PageSize;

            IQueryable<MassageRoom> query = _db.MassageRooms.Include(mr => mr.Room).ThenInclude(r => r.RoomType).Include(mr => mr.Massage).Include(m => m.Reception).Where(m => m.Massage.CalendarDayId == calendarDayId && m.NewMassageId == null);

            if (search.IsStatusWaitingPaid)
            {
                query = query.Where(mr => mr.CheckOutTime != null);
            }
            else
            {
                query = query.Where(mr => mr.CheckOutTime == null);
            }

            if (!String.IsNullOrEmpty(search.RoomName))
            {
                var room = await _db.Rooms.Where(r => r.RoomNo == search.RoomName).SingleOrDefaultAsync();
                if(room != null)
                {
                    if (room.RoomTypeId == (int)ERoomType.Suite || room.RoomTypeId == (int)ERoomType.Suite3 || room.RoomTypeId == (int)ERoomType.Suite4 || room.RoomTypeId == (int)ERoomType.Suite5 || room.RoomTypeId == (int)ERoomType.Suite6 || room.RoomTypeId == (int)ERoomType.Suite7 || room.RoomTypeId == (int)ERoomType.Suite8 || room.RoomTypeId == (int)ERoomType.Suite9 || room.RoomTypeId == (int)ERoomType.Suite10 || room.RoomTypeId == (int)ERoomType.C3 || room.RoomTypeId == (int)ERoomType.C5)
                    {
                        query = query.Where(ma => ma.Room.Id == room.Id);
                    }
                    else if (room.RoomTypeId == (int)ERoomType.SuiteSub)
                    {
                        query = query.Where(ma => ma.Room.Id == room.ParentRoomId);
                    }
                }
               
               
            }
            if(search.ReceptionId != null)
            {
                query = query.Where(mr => mr.ReceptionId == search.ReceptionId);
            }

            if (!String.IsNullOrEmpty(paginationFilter.Keyword))
            {
                query = query.Where(a => a.Room.RoomNo.Contains(paginationFilter.Keyword));
            }

            if(search.RoomTypeId != null)
            {
                query = query.Where(mr => mr.Room.RoomTypeId == search.RoomTypeId);
            }

            paginationFilter.TotalRecords = query.Count();
            string sortBy = char.ToUpper(paginationFilter.OrderBy.First()) + paginationFilter.OrderBy.Substring(1).ToLower();

            if (paginationFilter == null)
            {
                return await query.AsNoTracking().ToListAsync();
            }
            return await query.OrderBy(sortBy + " " + direction).Skip(skip).Take(paginationFilter.PageSize).AsNoTracking().ToListAsync();
        }

        public async Task<List<MassageRoom>> GetAllCheckInChangeRoomSuite(CheckInSearchQuery search, PaginationFilter paginationFilter = null, int? calendarDayId = null)
        {
            if (paginationFilter == null)
            {
                return await _db.MassageRooms.Include(ma => ma.Massage).Include(ma => ma.Room).ThenInclude(r => r.RoomType).Where(ma => ma.Massage.CalendarDayId == calendarDayId && (ma.NewMassageId != null && ma.DeletedDate == null)).AsNoTracking().ToListAsync();
            }

            if (typeof(Angel).GetProperties().All(s => s.Name != paginationFilter.OrderBy.First().ToString().ToUpper() + paginationFilter.OrderBy.Substring(1)))
            {
                paginationFilter.OrderBy = "Id";
            }
            string direction = null;
            if (string.Equals(paginationFilter.OrderDirection, "desc", StringComparison.OrdinalIgnoreCase))
            {
                direction = "descending";
            }
            else
            {
                direction = "ascending";
            }

            var skip = (paginationFilter.PageNumber - 1) * paginationFilter.PageSize;

            IQueryable<MassageRoom> query = _db.MassageRooms.Include(mr => mr.Room).ThenInclude(r => r.RoomType).Include(mr => mr.Massage).Include(m => m.Reception).Where(m => m.Massage.CalendarDayId == calendarDayId && (m.NewMassageId != null && m.DeletedDate == null));

            if (search.IsStatusWaitingPaid)
            {
                query = query.Where(mr => mr.CheckOutTime != null);
            }
            else
            {
                query = query.Where(mr => mr.CheckOutTime == null);
            }

            if (!String.IsNullOrEmpty(search.RoomName))
            {
                query = query.Where(mr => mr.Room.RoomNo == search.RoomName);
            }
            if (search.ReceptionId != null)
            {
                query = query.Where(mr => mr.ReceptionId == search.ReceptionId);
            }

            if (!String.IsNullOrEmpty(paginationFilter.Keyword))
            {
                query = query.Where(a => a.Room.RoomNo.Contains(paginationFilter.Keyword));
            }

            paginationFilter.TotalRecords = query.Count();
            string sortBy = char.ToUpper(paginationFilter.OrderBy.First()) + paginationFilter.OrderBy.Substring(1).ToLower();

            if (paginationFilter == null)
            {
                return await query.AsNoTracking().ToListAsync();
            }
            return await query.OrderBy(sortBy + " " + direction).Skip(skip).Take(paginationFilter.PageSize).AsNoTracking().ToListAsync();
        }

        public async Task<List<MassageAngel>> GetAllCheckInRoom(CheckInSearchQuery search, PaginationFilter paginationFilter = null, int? calendarDayId = null)
        {
            if (paginationFilter == null)
            {
                return await _db.MassageAngels.Include(ma=>ma.Reception).Include(ma=>ma.Massage).Include(ma=>ma.Angel).ThenInclude(an=>an.AngelType).Include(ma=>ma.Room).ThenInclude(r=>r.RoomType).Where(ma=>ma.Massage.CalendarDayId == calendarDayId).AsNoTracking().ToListAsync();
            }

            if (typeof(Angel).GetProperties().All(s => s.Name != paginationFilter.OrderBy.First().ToString().ToUpper() + paginationFilter.OrderBy.Substring(1)))
            {
                paginationFilter.OrderBy = "Id";
            }
            string direction = null;
            if (string.Equals(paginationFilter.OrderDirection, "desc", StringComparison.OrdinalIgnoreCase))
            {
                direction = "descending";
            }
            else
            {
                direction = "ascending";
            }

            var skip = (paginationFilter.PageNumber - 1) * paginationFilter.PageSize;

            IQueryable<MassageAngel> query = _db.MassageAngels.Include(ma => ma.Reception).Include(ma => ma.Massage).Include(ma => ma.Angel).ThenInclude(an => an.AngelType).Include(ma => ma.Room).ThenInclude(r => r.RoomType).Where(ma => ma.Massage.CalendarDayId == calendarDayId);

            if (search.IsStatusWaitingPaid)
            {
                query = query.Where(ma => ma.CheckOutTime != null);
            }
            else
            {
                query = query.Where(ma => ma.CheckOutTime == null);
            }
            
            if(search.AngelTypeId != null)
            {
                query = query.Where(ma => ma.Angel.AngelTypeId == search.AngelTypeId);
            }
            if (!String.IsNullOrEmpty(search.Firstname))
            {
                query = query.Where(ma => ma.Angel.Firstname.Contains(search.Firstname));
            }
            if (!String.IsNullOrEmpty(search.Lastname))
            {
                query = query.Where(ma => ma.Angel.Lastname.Contains(search.Lastname));
            }
            if (!String.IsNullOrEmpty(search.Nickname))
            {
                query = query.Where(ma => ma.Angel.Nickname.Contains(search.Nickname));
            }
            if (!String.IsNullOrEmpty(search.AngelCode))
            {
                query = query.Where(ma => ma.Angel.Code.Contains(search.AngelCode));
            }
            if (!String.IsNullOrEmpty(search.RoomName))
            {
                var room = await _db.Rooms.Where(r => r.RoomNo == search.RoomName).SingleOrDefaultAsync();
                if (room != null)
                {
                    if (room.RoomTypeId == (int)ERoomType.Suite || room.RoomTypeId == (int)ERoomType.Suite3 || room.RoomTypeId == (int)ERoomType.Suite4 || room.RoomTypeId == (int)ERoomType.Suite5 || room.RoomTypeId == (int)ERoomType.Suite6 || room.RoomTypeId == (int)ERoomType.Suite7 || room.RoomTypeId == (int)ERoomType.Suite8 || room.RoomTypeId == (int)ERoomType.Suite9 || room.RoomTypeId == (int)ERoomType.Suite10 || room.RoomTypeId == (int)ERoomType.C3 || room.RoomTypeId == (int)ERoomType.C5)
                    {
                        query = query.Where(ma => ma.Room.ParentRoomId == room.Id);
                    }
                    else if (room.RoomTypeId == (int)ERoomType.SuiteSub)
                    {
                        query = query.Where(ma => ma.Room.Id == room.Id);
                    }
                    else
                    {
                        query = query.Where(ma => ma.Room.RoomNo.Contains(search.RoomName));
                    }
                }
                               
            }
            if(search.ReceptionId != null)
            {
                query = query.Where(ma => ma.ReceptionId == search.ReceptionId);
            }
            if(!String.IsNullOrEmpty(search.RfId))
            {
                query = query.Where(ma => ma.Angel.Rfid.Contains(search.RfId));
            }
            if(search.RoomTypeId != null)
            {
                if (search.RoomTypeId == (int)ERoomType.Suite || search.RoomTypeId == (int)ERoomType.Suite3 || search.RoomTypeId == (int)ERoomType.Suite4 || search.RoomTypeId == (int)ERoomType.Suite5 || search.RoomTypeId == (int)ERoomType.Suite6 || search.RoomTypeId == (int)ERoomType.Suite7 || search.RoomTypeId == (int)ERoomType.Suite8 || search.RoomTypeId == (int)ERoomType.Suite9 || search.RoomTypeId == (int)ERoomType.Suite10 || search.RoomTypeId == (int)ERoomType.C3 || search.RoomTypeId == (int)ERoomType.C5)
                {
                    List<int> id = await _db.Rooms.Where(r => r.RoomTypeId == (int)search.RoomTypeId).Select(a => a.Id).ToListAsync();

                    query = query.Where(ma => id.Contains((int)ma.Room.ParentRoomId));
                }
                else
                {
                    query = query.Where(mr => mr.Room.RoomTypeId == search.RoomTypeId);
                }
            }

            if (!String.IsNullOrEmpty(paginationFilter.Keyword))
            {
                query = query.Where(a => a.Room.RoomNo.Contains(paginationFilter.Keyword) || a.Angel.Nickname.Contains(paginationFilter.Keyword) || a.Angel.Code.Contains(paginationFilter.Keyword));
            }

            paginationFilter.TotalRecords = query.Count();
            string sortBy = char.ToUpper(paginationFilter.OrderBy.First()) + paginationFilter.OrderBy.Substring(1).ToLower();

            if (paginationFilter == null)
            {
                return await query.AsNoTracking().ToListAsync();
            }
            return await query.OrderBy(sortBy + " " + direction).Skip(skip).Take(paginationFilter.PageSize).AsNoTracking().ToListAsync();
        }

        public async Task<bool> CreateAngelPerformanceDeduct(List<AngelPerformanceDeduct> angelPerformanceDeducts)
        {
            _db.AngelPerformanceDeducts.AddRange(angelPerformanceDeducts);
            var created = await _db.SaveChangesAsync();
            return created > 0;
        }

        public async Task<bool> CreateMassagePaymentCredit(MassageCreditPayment credit)
        {
            _db.MassageCreditPayments.Add(credit);
            var created = await _db.SaveChangesAsync();
            return created > 0;
        }

        public async Task<bool> CreateMassagePaymentMember(MassageMemberPayment member)
        {
            _db.MassageMemberPayments.Add(member);
            var created = await _db.SaveChangesAsync();
            return created > 0;
        }

        public async Task<MassageCreditPayment> GetMassageCreditPaymentByMassageId(int massageId)
        {
            return await _db.MassageCreditPayments.Where(cp => cp.MassageId == massageId).SingleOrDefaultAsync();
        }
        public async Task<MassageCreditPayment> GetMassageCreditPaymentById(int id)
        {
            return await _db.MassageCreditPayments.Where(cp => cp.Id == id).SingleOrDefaultAsync();
        }
        public async Task<MassageQrCodePayment> GetMassageQrCodePaymentById(int id) 
        {
            return await _db.MassageQrCodePayments.Where(qrp => qrp.Id == id).SingleOrDefaultAsync();
        }
        public async Task<bool> UpdateMassagePaymentCredit(MassageCreditPayment creditPayment)
        {
            _db.MassageCreditPayments.Update(creditPayment);
            var created = await _db.SaveChangesAsync();
            return created > 0;
        }
        public async Task<bool> UpdateMassagePaymentQrCode(MassageQrCodePayment qrCodePayment) 
        {
            _db.MassageQrCodePayments.Update(qrCodePayment);
            var updated = await _db.SaveChangesAsync();
            return updated > 0;

        }
        public async Task<List<MassageMemberPayment>> GetMassageMemberPaymentsByMassageId(int massageId)
        {
            return await _db.MassageMemberPayments.Include(mp => mp.Member).Where(mp => mp.MassageId == massageId).ToListAsync();
        }
        public async Task<List<MassageMemberPayment>> GetMassageMemberPaymentsByMassageId(int massageId, int memberId)
        {
            return await _db.MassageMemberPayments.Where(mp => mp.MassageId == massageId && mp.MemberId == memberId).ToListAsync();
        }

        public async Task<bool> CancelCreditPayment(MassageCreditPayment credit)
        {
            _db.MassageCreditPayments.Remove(credit);
            var deleted = await _db.SaveChangesAsync();
            return deleted > 0;
        }
        public async Task<bool> CancelMemberPayment(MassageMemberPayment member)
        {
            _db.MassageMemberPayments.Remove(member);
            var deleted = await _db.SaveChangesAsync();
            return deleted > 0;
        }
        public async Task<List<Massage>> GetUnPaids(List<int> calendarIds, UnPaidSearchQurey unPaid, PaginationFilter paginationFilter = null)
        {
            if (paginationFilter == null)
            {
                return await _db.Massages.Include(c => c.CalendarDay).Where(m => m.IsPaid == true && m.IsUnPaid == true && calendarIds.Contains(m.CalendarDayId)).ToListAsync();
            }

            if (typeof(Massage).GetProperties().All(s => s.Name != paginationFilter.OrderBy.First().ToString().ToUpper() + paginationFilter.OrderBy.Substring(1)))
            {
                paginationFilter.OrderBy = "Id";
            }
            string direction = null;
            if (string.Equals(paginationFilter.OrderDirection, "desc", StringComparison.OrdinalIgnoreCase))
            {
                direction = "descending";
            }
            else
            {
                direction = "ascending";
            }

            var skip = (paginationFilter.PageNumber - 1) * paginationFilter.PageSize;
            IQueryable<Massage> query = _db.Massages.Include(c => c.CalendarDay).Where(m => m.IsPaid == true && m.IsUnPaid == true && calendarIds.Contains(m.CalendarDayId));

            if (unPaid.DocumentNumber != null)
            {
                query = query.Where(r => r.DocumentNumber.Contains(unPaid.DocumentNumber));
            }
            if (unPaid.ReceptionId != null)
            {
                query = query.Where(massage => _db.MassageAngels.Any(massageAngel => massageAngel.MassageId == massage.Id && massageAngel.ReceptionId ==unPaid.ReceptionId));
            }

            paginationFilter.TotalRecords = query.Count();
            string sortBy = char.ToUpper(paginationFilter.OrderBy.First()) + paginationFilter.OrderBy.Substring(1).ToLower();
            if (paginationFilter == null)
            {
                return await query.AsNoTracking().ToListAsync();
            }
            return await query.OrderBy(sortBy + " " + direction).Skip(skip).Take(paginationFilter.PageSize).AsNoTracking().ToListAsync();
        }

        public async Task<List<MassageAngel>> GetWagePayment(int calendarDayId)
        {
            return await _db.MassageAngels.Include(m => m.Massage).Include(a=> a.Angel).Include(r=> r.Room).Where(m => m.Massage.CalendarDayId == calendarDayId && m.CheckOutTime != null && m.Massage.IsPaid == false).ToListAsync();
        }

        public async Task<List<MassageRoom>> GetWagePaymentSuit(int calendarDayId)
        {
            return await _db.MassageRooms.Include(m => m.Massage).Include(r => r.Room).Where(m => m.Massage.CalendarDayId == calendarDayId && m.CheckOutTime != null && m.Massage.IsPaid == false).ToListAsync();
        }

        public async Task<bool> DeleteMassageAngel(int id)
        {
            var deleted = 0;
            var massageAngel = await GetMassageAngelById(id);
            if(massageAngel != null)
            {
                _db.MassageAngels.Remove(massageAngel);
                deleted = await _db.SaveChangesAsync();
            }
            
            return deleted > 0;
        }
        public async Task<bool> DeleteMassageRoom(int id)
        {
            var deleted = 0;
            var massageRoom = await GetMassageRoomById(id);
            if(massageRoom != null)
            {
                _db.MassageRooms.Remove(massageRoom);
                deleted = await _db.SaveChangesAsync();
            }
            
            return deleted > 0;
        }
        public async Task<List<MassageRoom>> GetMassageRoomsByMassageId(int massageId)
        {
            return await _db.MassageRooms.Include(mr => mr.Room).ThenInclude(r => r.RoomType).Include(mr => mr.Massage).Include(m => m.Reception).Where(mr => mr.MassageId == massageId).AsNoTracking().ToListAsync();
        }
        public async Task<MassageRoom> GetMaasageRoomByMaasageIdIsNotCheckOut(int massageId)
        {
            return await _db.MassageRooms.Include(mr => mr.Room).ThenInclude(r => r.RoomType).Include(mr => mr.Massage).Include(m => m.Reception).Where(mr => mr.MassageId == massageId && mr.CheckOutTime == null).AsNoTracking().SingleOrDefaultAsync();
        }

        public async Task<List<Massage>> GetSellMassageAsync(int calendarDayId, ReceptionSearchQuery reception, PaginationFilter paginationFilter = null)
        {
            if (paginationFilter == null)
            {
                return await _db.Massages.Where(m => m.CalendarDayId == calendarDayId).AsNoTracking().ToListAsync();
            }

            if (typeof(Reception).GetProperties().All(s => s.Name != paginationFilter.OrderBy.First().ToString().ToUpper() + paginationFilter.OrderBy.Substring(1)))
            {
                paginationFilter.OrderBy = "Id";
            }
            string direction = null;
            if (string.Equals(paginationFilter.OrderDirection, "desc", StringComparison.OrdinalIgnoreCase))
            {
                direction = "descending";
            }
            else
            {
                direction = "ascending";
            }

            var skip = (paginationFilter.PageNumber - 1) * paginationFilter.PageSize;

            IQueryable<Massage> query = _db.Massages.Where(m => m.CalendarDayId == calendarDayId);

            if (!String.IsNullOrEmpty(reception.DocumentNumber))
            {
                query = query.Where(r => r.DocumentNumber.Contains(reception.DocumentNumber));
            }

            if (!String.IsNullOrEmpty(paginationFilter.Keyword))
            {
                query = query.Where(r => r.DocumentNumber.Contains(paginationFilter.Keyword));
            }
            paginationFilter.TotalRecords = query.Count();
            string sortBy = char.ToUpper(paginationFilter.OrderBy.First()) + paginationFilter.OrderBy.Substring(1).ToLower();
            if (paginationFilter == null)
            {
                return await query.AsNoTracking().ToListAsync();
            }
            return await query.OrderBy(sortBy + " " + direction).Skip(skip).Take(paginationFilter.PageSize).AsNoTracking().ToListAsync();
        }

        public async Task<List<MassageAngel>> GetWagePaymentCancel(List<int> calendarDayIds ,int calendarDayId, WagePaymentCancelSearchQuery wagePayment, PaginationFilter paginationFilter = null)
        {
            if (paginationFilter == null)
            {
                return await _db.MassageAngels.Include(m => m.Massage).Where(m => m.Massage.CalendarDayId == calendarDayId && m.CheckOutTime != null && m.Massage.IsPaid == true).ToListAsync();
            }
            if (typeof(MassageAngel).GetProperties().All(s => s.Name != paginationFilter.OrderBy.First().ToString().ToUpper() + paginationFilter.OrderBy.Substring(1)))
            {
                paginationFilter.OrderBy = "Id";
            }
            string direction = null;
            if (string.Equals(paginationFilter.OrderDirection, "desc", StringComparison.OrdinalIgnoreCase))
            {
                direction = "descending";
            }
            else
            {
                direction = "ascending";
            }

            var skip = (paginationFilter.PageNumber - 1) * paginationFilter.PageSize;
            IQueryable<MassageAngel> query = _db.MassageAngels.Include(m => m.Massage).Where(m => m.CheckOutTime != null && m.Massage.IsPaid == true);
            if (!String.IsNullOrEmpty(wagePayment.CodeAngel))
            {
                query = query.Where(w => w.Angel.Code.Contains(wagePayment.CodeAngel));
            }
            if (!String.IsNullOrEmpty(wagePayment.RoomNo))
            {
                var room = await _db.Rooms.Where(r => r.RoomNo == wagePayment.RoomNo).SingleOrDefaultAsync();
                if (room != null)
                {
                    if (room.RoomTypeId == (int)ERoomType.Suite || room.RoomTypeId == (int)ERoomType.Suite3 || room.RoomTypeId == (int)ERoomType.Suite4 || room.RoomTypeId == (int)ERoomType.Suite5 || room.RoomTypeId == (int)ERoomType.Suite6 || room.RoomTypeId == (int)ERoomType.Suite7 || room.RoomTypeId == (int)ERoomType.Suite8 || room.RoomTypeId == (int)ERoomType.Suite9 || room.RoomTypeId == (int)ERoomType.Suite10 || room.RoomTypeId == (int)ERoomType.C3 || room.RoomTypeId == (int)ERoomType.C5)
                    {
                        query = query.Where(ma => ma.Room.ParentRoomId == room.Id);
                    }
                    else if (room.RoomTypeId == (int)ERoomType.SuiteSub)
                    {
                        query = query.Where(ma => ma.Room.Id == room.Id);
                    }
                    else
                    {
                        query = query.Where(ma => ma.Room.RoomNo.Contains(wagePayment.RoomNo));
                    }
                }

            }
            if (wagePayment.ReceptionId != null)
            {
                query = query.Where(mr => mr.ReceptionId == wagePayment.ReceptionId);
            }
            if (calendarDayIds.Count > 0 && calendarDayIds != null)
            {
                query = query.Where(w => calendarDayIds.Contains(w.Massage.CalendarDayId));
            }
            else
            {
                query = query.Where(w => w.Massage.CalendarDayId == calendarDayId);
            }
            if (!String.IsNullOrEmpty(wagePayment.DocumentNumber))
            {
                query = query.Where(r => r.Massage.DocumentNumber.Contains(wagePayment.DocumentNumber));
            }

            if (!String.IsNullOrEmpty(paginationFilter.Keyword))
            {
                query = query.Where(w => w.Angel.Code.Contains(paginationFilter.Keyword) || w.Room.RoomNo.Contains(paginationFilter.Keyword) || w.Reception.Code.Contains(paginationFilter.Keyword));
            }
            paginationFilter.TotalRecords = query.Count();
            string sortBy = char.ToUpper(paginationFilter.OrderBy.First()) + paginationFilter.OrderBy.Substring(1).ToLower();
            if (paginationFilter == null)
            {
                return await query.AsNoTracking().ToListAsync();
            }
            return await query.OrderBy(sortBy + " " + direction).Skip(skip).Take(paginationFilter.PageSize).AsNoTracking().ToListAsync();
        }
        public async Task<List<MassageRoom>> GetWagePaymentSuitCancel(List<int> calendarDayIds, int calendarDayId, WagePaymentCancelSearchQuery wagePayment, PaginationFilter paginationFilter = null)
        {
            if (paginationFilter == null)
            {
                return await _db.MassageRooms.Include(m => m.Massage).Where(m => m.Massage.CalendarDayId == calendarDayId && m.CheckOutTime != null && m.Massage.IsPaid == true).ToListAsync();
            }
            if (typeof(MassageRoom).GetProperties().All(s => s.Name != paginationFilter.OrderBy.First().ToString().ToUpper() + paginationFilter.OrderBy.Substring(1)))
            {
                paginationFilter.OrderBy = "Id";
            }
            string direction = null;
            if (string.Equals(paginationFilter.OrderDirection, "desc", StringComparison.OrdinalIgnoreCase))
            {
                direction = "descending";
            }
            else
            {
                direction = "ascending";
            }

            var skip = (paginationFilter.PageNumber - 1) * paginationFilter.PageSize;
            IQueryable<MassageRoom> query = _db.MassageRooms.Include(m => m.Massage).Where(m => m.CheckOutTime != null && m.Massage.IsPaid == true);
            if (!String.IsNullOrEmpty(wagePayment.RoomNo))
            {
                var room = await _db.Rooms.Where(r => r.RoomNo == wagePayment.RoomNo).SingleOrDefaultAsync();
                if (room != null)
                {
                    if (room.RoomTypeId == (int)ERoomType.Suite || room.RoomTypeId == (int)ERoomType.Suite3 || room.RoomTypeId == (int)ERoomType.Suite4 || room.RoomTypeId == (int)ERoomType.Suite5 || room.RoomTypeId == (int)ERoomType.Suite6 || room.RoomTypeId == (int)ERoomType.Suite7 || room.RoomTypeId == (int)ERoomType.Suite8 || room.RoomTypeId == (int)ERoomType.Suite9 || room.RoomTypeId == (int)ERoomType.Suite10 || room.RoomTypeId == (int)ERoomType.C3 || room.RoomTypeId == (int)ERoomType.C5)
                    {
                        query = query.Where(ma => ma.Room.Id == room.Id);
                    }
                    else if (room.RoomTypeId == (int)ERoomType.SuiteSub)
                    {
                        query = query.Where(ma => ma.Room.Id == room.ParentRoomId);
                    }
                    else
                    {
                        query = query.Where(ma => ma.Room.RoomNo.Contains(wagePayment.RoomNo));
                    }
                }
            }
            if (wagePayment.ReceptionId != null)
            {
                query = query.Where(mr => mr.ReceptionId == wagePayment.ReceptionId);
            }
            if (calendarDayIds.Count > 0 && calendarDayIds != null)
            {
                query = query.Where(w => calendarDayIds.Contains(w.Massage.CalendarDayId));
            }
            else
            {
                query = query.Where(w => w.Massage.CalendarDayId == calendarDayId);
            }
            if (!String.IsNullOrEmpty(wagePayment.DocumentNumber))
            {
                query = query.Where(r => r.Massage.DocumentNumber.Contains(wagePayment.DocumentNumber));
            }
            if (!String.IsNullOrEmpty(paginationFilter.Keyword))
            {
                query = query.Where(w => w.Room.RoomNo.Contains(paginationFilter.Keyword) || w.Reception.Code.Contains(paginationFilter.Keyword));
            }
            paginationFilter.TotalRecords = query.Count();
            string sortBy = char.ToUpper(paginationFilter.OrderBy.First()) + paginationFilter.OrderBy.Substring(1).ToLower();
            if (paginationFilter == null)
            {
                return await query.AsNoTracking().ToListAsync();
            }
            return await query.OrderBy(sortBy + " " + direction).Skip(skip).Take(paginationFilter.PageSize).AsNoTracking().ToListAsync();
        }

        public async Task<bool> CheckDuplicateRoomMaster(int roomId)
        {
            bool result = false;
            decimal i = await _db.MassageRooms.Where(mr => mr.RoomId == roomId && mr.DeletedDate == null && mr.CheckOutTime == null && mr.DataOld == false).CountAsync();
            if(i > 0)
            {
                result = true;
            }
            return result;
        }

        public async Task<bool> CheckDuplicateMassageAngelRoomMaster(int roomId)
        {
            bool result = false;
            decimal i = await _db.MassageAngels.Where(mr => mr.RoomId == roomId && mr.DeletedDate == null && mr.CheckOutTime == null && mr.DataOld == false).CountAsync();
            if (i > 0)
            {
                result = true;
            }
            return result;
        }

        public async Task<MassageRoom> DuplicateRoomMaster(int roomId)
        {
            var massageRoom = await _db.MassageRooms.Where(mr => mr.RoomId == roomId && mr.DeletedDate == null && mr.CheckOutTime == null && mr.DataOld == false).SingleOrDefaultAsync();

            return massageRoom;
        }

        public async Task<MassageAngel> DuplicateAngel(int roomId)
        {
            var massageAngel = await _db.MassageAngels.Where(mr => mr.RoomId == roomId && mr.DeletedDate == null && mr.CheckOutTime == null && mr.DataOld == false).SingleOrDefaultAsync();

            return massageAngel;
        }

        public async Task<List<Massage>> GetMassagelTipAngel(int calendarDayId)
        {
            var massageTipAngel = await _db.Massages.Where(mr => mr.CalendarDayId == calendarDayId && mr.TipTotal != 0 || mr.TipQrCodeTotal != 0).ToListAsync();

            return massageTipAngel;
        }

        public async Task<List<Massage>> GetMassagelTipAngel(int calendarDayId, int massageId)
        {
            //var massage = await _db.Massages.Where(m=>)
            var massageTipAngel = await _db.Massages.Where(mr => mr.CalendarDayId == calendarDayId && mr.Id == massageId && mr.TipTotal != 0 || mr.TipQrCodeTotal != 0).ToListAsync();

            return massageTipAngel;
        }

        public async Task<MassageQrCodePayment> GetMassageQrCodePaymentByMassageId(int massageId)
        {
            return await _db.MassageQrCodePayments.Where(cp => cp.MassageId == massageId).SingleOrDefaultAsync();
        }

        public async Task<bool> CreateMassageQrCode(MassageQrCodePayment qrcode)
        {
            _db.MassageQrCodePayments.Add(qrcode);
            var created = await _db.SaveChangesAsync();
            return created > 0;
        }

        public async Task<bool> CancelQrCodePayment(MassageQrCodePayment qrCode)
        {
            _db.MassageQrCodePayments.Remove(qrCode);
            var deleted = await _db.SaveChangesAsync();
            return deleted > 0;
        }

        public async Task<List<Massage>> GetMassageByCashierId(int id, List<int> calendarDayIds)
        {

            return await _db.Massages.Include(c => c.CalendarDay).Where(ch => ch.CashierId == id && calendarDayIds.Contains(ch.CalendarDayId)).ToListAsync();
        }

        public async Task<bool> GetMultipleAngleByRoomNormal(int massageId)
        {
            bool isMultiple = false;
            var count = await _db.MassageAngels.Where(ma => ma.MassageId == massageId && ma.CheckOutTime == null).CountAsync();
            if(count > 0)
            {
                isMultiple = true;
            }
            return isMultiple;
        }
        public async Task<bool> GetListMultipleAngleByRoomNormal(int massageId,int roomId)
        {
            bool isMultiple = false;
            var count = await _db.MassageAngels.Where(ma => ma.MassageId == massageId && ma.RoomId == roomId && ma.CheckOutTime == null).CountAsync();
            if (count > 0)
            {
                isMultiple = true;
            }
            return isMultiple;
        }

        public async Task<MassageRoom> GetMassageRoomByMassageIdAndRoomId(int massageId, int RoomId)
        {
            return await _db.MassageRooms.Where(mr => mr.MassageId == massageId && mr.RoomId == RoomId).SingleOrDefaultAsync();
        }
       
        public async Task<bool> CheckInMassageRoomList(MassageRoom massageRooms)
        {
            _db.MassageRooms.Add(massageRooms);
            var created = await _db.SaveChangesAsync();
            return created > 0;
        }
        public async Task<bool> CheckInMassageAngel(MassageAngel massageAngels)
        {
            _db.MassageAngels.Add(massageAngels);
            var created = await _db.SaveChangesAsync();
            return created > 0;
        }
        public async Task<List<MassageRoom>> GetListMassageRoomByMassageId(int id)
        {
            return await _db.MassageRooms.Include(mr => mr.Room).ThenInclude(r => r.RoomType).Include(mr => mr.Massage).Include(m => m.Reception).Where(mr => mr.MassageId == id).AsNoTracking().ToListAsync();
        }
        public async Task<List<Massage>> GetAllAsync()
        {
            return await _db.Massages.ToListAsync();
        }
        public async Task<List<Massage>> GetMassageByAgenciesId(int id, int calendarDays, int agencyPerformanceId) 
        {
            return await _db.Massages.Where(agc => agc.AgenciesId == id && agc.CalendarDayId == calendarDays && agc.AgencyPerformanceId == agencyPerformanceId && agc.CancelDate == null).AsNoTracking().ToListAsync();
        }

        public async Task<List<MassageRoom>> GetListMassageRoomByMassageIdAndRoomId(int massageId, int RoomId) 
        {
            return await _db.MassageRooms.Where(mr => mr.MassageId == massageId && mr.RoomId == RoomId).ToListAsync();
        }

        public async Task<bool> CreateMassageMemberAndRoom(MassageMemberAndRoom memberAndRoom)
        {
            _db.massageMemberAndRooms.Add(memberAndRoom);
            var created = await _db.SaveChangesAsync();
            return created > 0;
        }

        public async Task<MassageMemberAndRoom> GetMassageMemberAndRoomByMassageRoomId(int massageRoomId)
        {
            return await _db.massageMemberAndRooms.Include(m => m.Member).Include(mr => mr.MassageRoom).Include(mp => mp.MassageMemberPayment).Where(ms => ms.MassageRoomId == massageRoomId).SingleOrDefaultAsync();
        }
        public async Task<List<int>> GetMassageAngelByAngelPerformanceId(int angelPerformanceId) 
        {
            List<int> id = await _db.MassageAngels.Where(ma => ma.AngelPerformanceId == angelPerformanceId).Select(ma => ma.Id).ToListAsync();
            return id;
        }

        public async Task<List<MassageAngel>> GetMassageAngelByAngelPerformanceIdIsPoint(int angelPerformanceId)
        {
            var massageAngels = await _db.MassageAngels.Where(ma => ma.AngelPerformanceId == angelPerformanceId).ToListAsync();
            return massageAngels;
        }
        public async Task<List<MassageAngel>> GetListMassageAngelByAngelPerformanceId(int angelPerformanceId)
        {
            return await _db.MassageAngels.Include(r => r.Reception).Where(ma => ma.AngelPerformanceId == angelPerformanceId).ToListAsync();
        }

        public async Task<MassageMemberPayment> GetMassageMemberPaymentById(int id)
        {
            return await _db.MassageMemberPayments.Where(mp => mp.Id == id).SingleOrDefaultAsync();
        }
        public async Task<bool> CancelCheckInLater(int id)
        {
            var deleted = 0;
            var massage = await GetById(id);
            if (massage != null)
            {
                _db.Massages.Remove(massage);
                deleted = await _db.SaveChangesAsync();
            }

            return deleted > 0;
        }
        public async Task<List<MassageAngel>> _GetMassageAngelByAngelIdAsync(List<int> calendarDayIds)
        {
            return await _db.MassageAngels.Include(m => m.Massage).Include(a => a.Angel).ThenInclude(at => at.AngelType).Where(m => calendarDayIds.Contains( m.Massage.CalendarDayId)).ToListAsync();
        }
        public async Task<List<MassageAngel>> GetMassageAngelByCalendarDayIdAsync(int calendarDayId)
        {
            return await _db.MassageAngels.Include(m => m.Massage).Include(a => a.Angel).ThenInclude(at => at.AngelType).Where(m => m.Massage.CalendarDayId == calendarDayId)
                .ToListAsync();
        }

    }
}
