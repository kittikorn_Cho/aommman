﻿using OONApi.Contracts.Requests.Queires;
using OONApi.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OONApi.Services
{
    public interface IDeductTypeService
    {
        Task<List<DeductType>> GetAllAsync(DeductTypeSearchQuery deductType, PaginationFilter paginationFilter = null);
        Task<DeductType> GetByIdAsync(int id);
        Task<bool> CreateAsync(DeductType deductType);
        Task<bool> UpdateAsync(DeductType deductType);
        Task<bool> DeleteAsync(int id);
        bool CheckAngelPerformanceDeduct(int massageAngelId);
    }
}
