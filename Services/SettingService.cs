﻿using OONApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Linq.Dynamic.Core;
using System.Globalization;
using Microsoft.Extensions.Logging;
using DocumentFormat.OpenXml.Office2016.Excel;

namespace OONApi.Services
{
    public class SettingService : ISettingService
    {
        private readonly AppDbContext _db;
        public SettingService(AppDbContext db)
        {
            _db = db;
        }
        public async Task<bool> CreateCalendarDay(CalendarDay calendarDay)
        {
            _db.CalendarDays.Add(calendarDay);
            var created = await _db.SaveChangesAsync();
            return created > 0;
        }
        public async Task<bool> UpdateCalendarDay(CalendarDay calendarDay)
        {
            _db.CalendarDays.Update(calendarDay);
            var updated = await _db.SaveChangesAsync();
            return updated > 0;
        }
        public async Task<CalendarDay> GetCarlendarDay()
        {
            return await _db.CalendarDays.Where(c => c.IsDate == true).SingleOrDefaultAsync();
        }
        public async Task<List<CalendarDay>> GetCarlendarDayByMonth()
        {
            return await _db.CalendarDays.Where(c => DateTime.Now.Month == c.ToDay.Month && DateTime.Now.Year == c.ToDay.Year).ToListAsync();
        }

        public async Task<bool> CreateDocumentNumber(DocumentNumber documentNumbers)
        {
            _db.DocumentNumbers.Add(documentNumbers);
            var created = await _db.SaveChangesAsync();
            return created > 0;
        }
        public string GetDocumentNumberAndUpdate(int calendarDayId, bool updateFlag = false)
        {
            var documentNumber = _db.DocumentNumbers.Include(c => c.CalendarDay).Where(d => d.CalendarDayId == calendarDayId).AsNoTracking().SingleOrDefault();
            if (documentNumber == null)
            {
                return "";
            }
            string docNo = "";
            int runnigNumber = documentNumber.RunningNumber + 1;



            docNo =  documentNumber.CalendarDay.ToDay.ToString("ddMMyyyy",new CultureInfo("th-TH"))+ "" + runnigNumber.ToString().PadLeft(documentNumber.PadLength, '0');

            if (updateFlag)
            {
                var update = _db.DocumentNumbers.Where(d => d.Id == documentNumber.Id).SingleOrDefault();
                if(update != null)
                {
                    update.RunningNumber = runnigNumber;
                    _db.SaveChanges();
                }
            }
            return docNo;
        }

        public async Task<List<int>> GetCalendarId(DateTime startdate,DateTime enddate)
        {
            List<int> calendarDayIds = new List<int>();

            calendarDayIds = await _db.CalendarDays.Where(c => c.ToDay >= startdate && c.ToDay <= enddate).Select(s => s.Id).ToListAsync();

            return calendarDayIds;
        }
        public int GetCalendarId(DateTime startdate)
        {
            int calendarDayIds = _db.CalendarDays.Where(c => c.ToDay == startdate).Select(s => s.Id).Single();
            return calendarDayIds;
        }

        public async Task<List<CalendarDay>> GetCalendarDayStartDateAndEndDate(DateTime startDate,DateTime endDate)
        {
            var calendarDay = await _db.CalendarDays.Where(c => c.ToDay >= startDate && c.ToDay <= endDate).ToListAsync();
            return calendarDay;
        }

        public async Task<AngelWorking> GetAngelWorking(int angelId,int calendarId)
        {
            return await _db.AngelWorkings.Where(a => a.AngelId == angelId && a.CalendarDayId == calendarId).SingleOrDefaultAsync();
        }

        public async Task<decimal> GetCountAngelWorkingByCalendarDayId(int calendarId)
        {
            var count = 0;
            count = await _db.AngelWorkings.Where(a => a.CalendarDayId == calendarId).CountAsync();
            return count;
        }

        public async Task<List<MassageAngel>> GetMassageAngelByCalendarDayId(int calendarId)
        {
            return await _db.MassageAngels.Include(a => a.Massage).Where(a => a.Massage.CalendarDayId==calendarId).ToListAsync();
        }

        public async Task<decimal> GetMassageRoomByCalendarDayId(int calendarId, int roomSuiteId, int roomSuite3Id, int roomSuite4Id, int roomSuite5Id, int roomSuite6Id, int roomSuite7Id, int roomSuite8Id, int roomSuite9Id, int roomSuite10Id, int roomC3Id, int roomC5Id)
        {
            var count = 0;
            count = await _db.MassageRooms.Include(a => a.Room).Include(a => a.Massage).Where(a => a.Massage.CalendarDayId == calendarId && (a.Room.RoomTypeId == roomSuiteId || a.Room.RoomTypeId == roomSuite3Id || a.Room.RoomTypeId == roomSuite4Id || a.Room.RoomTypeId == roomSuite5Id || a.Room.RoomTypeId == roomSuite6Id || a.Room.RoomTypeId == roomSuite7Id || a.Room.RoomTypeId == roomSuite8Id || a.Room.RoomTypeId == roomSuite9Id || a.Room.RoomTypeId == roomSuite10Id || a.Room.RoomTypeId == roomC3Id || a.Room.RoomTypeId == roomC5Id)).CountAsync();
            return count;
        }
        public async Task<decimal> GetMassageRoomByCalendarDayIdAndCheckIsNull(int calendarId, int roomSuiteId, int roomSuite3Id, int roomSuite4Id, int roomSuite5Id, int roomSuite6Id, int roomSuite7Id, int roomSuite8Id, int roomSuite9Id, int roomSuite10Id, int roomC3Id, int roomC5Id)
        {
            var count = 0;
            count = await _db.MassageRooms.Include(a => a.Room).Include(a => a.Massage).Where(a => a.Massage.CalendarDayId == calendarId && a.CheckOutTime == null &&(a.Room.RoomTypeId == roomSuiteId || a.Room.RoomTypeId == roomSuite3Id || a.Room.RoomTypeId == roomSuite4Id || a.Room.RoomTypeId == roomSuite5Id || a.Room.RoomTypeId == roomSuite6Id || a.Room.RoomTypeId == roomSuite7Id || a.Room.RoomTypeId == roomSuite8Id || a.Room.RoomTypeId == roomSuite9Id || a.Room.RoomTypeId == roomSuite10Id || a.Room.RoomTypeId == roomC3Id || a.Room.RoomTypeId == roomC5Id)).CountAsync();
            return count;
        }

        public async Task<decimal> TotalCountAngelType(int id, int calendarId)
        {
            var count = 0;
            count = await _db.AngelWorkings.Include(a => a.Angel).ThenInclude(a => a.AngelType).Where(a => a.Angel.AngelType.Id == id && a.CalendarDayId == calendarId).CountAsync();
            return count;
        }

        public async Task<List<AngelPerformance>> GetAngelPerformancesByCalendarDayId(int id ,int calendarId)
        {
            return await _db.AngelPerformances.Include(a => a.Angel).ThenInclude(a => a.AngelType).Where(a => a.Angel.AngelType.Id == id && a.CarlendarDayId == calendarId).ToListAsync();
        }
        public async Task<List<MassageAngel>> GetMassageAngelByCalendarDayId(int id, int calendarId)
        {
            return await _db.MassageAngels.Include(m => m.Massage).Include(a => a.Angel).ThenInclude(a => a.AngelType).Where(a => a.Angel.AngelType.Id == id && a.Massage.CalendarDayId == calendarId).ToListAsync();
        }
        public async Task<CalendarDay> GetCalendarDayById(int calendarId)
        {
            return await _db.CalendarDays.Where(c => c.Id == calendarId).SingleOrDefaultAsync();
        }

        public async Task<bool> CreateSetting(Setting setting)
        {
            _db.Settings.Add(setting);
            var created = await _db.SaveChangesAsync();
            return created > 0;
        }

        public async Task<bool> UpdateSetting(Setting setting)
        {
            _db.Settings.Update(setting);
            var updated = await _db.SaveChangesAsync();
            return updated > 0;
        }

        public async Task<Setting> GetSettingById(int id)
        {
            return await _db.Settings.Where(s => s.Id == id).SingleOrDefaultAsync();
        }
    }
}
