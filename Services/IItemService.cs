﻿using OONApi.Contracts.Requests.Queires;
using OONApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Services
{
    public interface IItemService
    {
        Task<List<Item>> GetAllAsync(ItemSearchQuery item, PaginationFilter paginationFilter = null);
        Task<Item> GetByIdAsync(int id);
        Task<bool> CreateAsync(Item item);
        Task<bool> UpdateAsync(Item item);
        Task<bool> DeleteAsync(int id);
        Task<Item> GetSuitSlug();
    }
}
