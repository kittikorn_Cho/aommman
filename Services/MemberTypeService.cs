﻿using Microsoft.EntityFrameworkCore;
using OONApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Linq.Dynamic.Core;

namespace OONApi.Services
{
    public class MemberTypeService : IMemberTypeService
    {
        private readonly AppDbContext _db;

        public MemberTypeService(AppDbContext db)
        {
            _db = db;
        }

        public async Task<List<MemberType>> GetAllAsync(PaginationFilter paginationFilter = null)
        {
            if (paginationFilter == null)
            {
                return await _db.MemberTypes.AsNoTracking().ToListAsync();
            }

            if (typeof(Angel).GetProperties().All(l => l.Name != paginationFilter.OrderBy.First().ToString().ToUpper() + paginationFilter.OrderBy.Substring(1)))
            {
                paginationFilter.OrderBy = "Id";
            }

            string direction = null;
            direction = string.Equals(paginationFilter.OrderDirection, "desc", StringComparison.OrdinalIgnoreCase) ? "descending" : "ascending";

            var skip = (paginationFilter.PageNumber - 1) * paginationFilter.PageSize;

            paginationFilter.TotalRecords = _db.AngelTypes.Count(l =>
                                                EF.Functions.Like(l.Code, $"%{paginationFilter.Keyword}%")
                                            );

            var sortBy = char.ToUpper(paginationFilter.OrderBy.First()) + paginationFilter.OrderBy.Substring(1).ToLower();

            return await _db.MemberTypes.Where(a => EF.Functions.Like(a.Code, $"%{paginationFilter.Keyword}%")
                                              || EF.Functions.Like(a.Fee, $"%{paginationFilter.Keyword}%"))
                                        .OrderBy(sortBy + " " + direction)
                                        .Skip(skip).Take(paginationFilter.PageSize)
                                        .AsNoTracking()
                                        .ToListAsync();
        }
        public async Task<MemberType> GetByIdAsync(int id)
        {
            return await _db.MemberTypes.Include(m=>m.MemberTypeItems).ThenInclude(i=>i.Item).Where(a => a.Id == id).SingleOrDefaultAsync();
        }
        public async Task<bool> CreateAsync(MemberType memberType)
        {
            _db.MemberTypes.Add(memberType);
            var created = await _db.SaveChangesAsync();
            return created > 0;
        }
        public async Task<bool> CreateMemberTypeItemAsync(List<MemberTypeItem> memberTypeItems)
        {
            _db.MemberTypeItems.AddRange(memberTypeItems);
            var created = await _db.SaveChangesAsync();
            return created > 0;
        }

        public async Task<bool> UpdateMemberTypeItemAsync(List<MemberTypeItem> memberTypeItems)
        {
            _db.MemberTypeItems.UpdateRange(memberTypeItems);
            var updated = await _db.SaveChangesAsync();
            return updated > 0;
        }
        public async Task<bool> UpdateAsync(MemberType memberType)
        {
            _db.MemberTypes.Update(memberType);
            var updated = await _db.SaveChangesAsync();
            return updated > 0;
        }
        public async Task<bool> DeleteAsync(int id)
        {
            var deleted = 0;

            var memberType = await GetByIdAsync(id);

            foreach(MemberTypeItem i in memberType.MemberTypeItems.ToList())
            {
                _db.Remove<MemberTypeItem>(i);
                _db.SaveChanges();
            }

            if(memberType != null)
            {
                _db.MemberTypes.Remove(memberType);
                deleted = await _db.SaveChangesAsync();
            }

            return deleted > 0;
        }

        public async Task<MemberTypeItem> GetMemberTypeItemById(int id)
        {
            return await _db.MemberTypeItems.Where(m => m.Id == id).SingleOrDefaultAsync();
        }

        public async Task<List<MemberTypeItem>> GetMemberTypeItems(int memberTypeId)
        {
            return await _db.MemberTypeItems.Include(i=>i.Item).Where(m => m.MemberTypeId == memberTypeId).ToListAsync();
        }

        public async Task<MemberTypeItem> GetMemberTypeItems(int memberTypeId, int ItemId)
        {
            return await _db.MemberTypeItems.Where(m => m.MemberTypeId == memberTypeId && m.ItemId == ItemId).SingleOrDefaultAsync();
        }

    }
}
