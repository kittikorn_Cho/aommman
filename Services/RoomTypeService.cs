﻿using Microsoft.EntityFrameworkCore;
using OONApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Linq.Dynamic.Core;
using OONApi.Enum;

namespace OONApi.Services
{
    public class RoomTypeService : IRoomTypeService
    {
        private readonly AppDbContext _db;

        public RoomTypeService(AppDbContext db)
        {
            _db = db;
        }

        public async Task<List<RoomType>> GetAllAsync(PaginationFilter paginationFilter = null)
        {
            if (paginationFilter == null)
            {
                return await _db.RoomTypes.Where(r => r.Id != (int)ERoomType.SuiteSub).AsNoTracking().ToListAsync();
            }

            if (typeof(RoomType).GetProperties().All(l => l.Name != paginationFilter.OrderBy.First().ToString().ToUpper() + paginationFilter.OrderBy.Substring(1)))
            {
                paginationFilter.OrderBy = "Id";
            }

            string direction = null;
            direction = string.Equals(paginationFilter.OrderDirection, "desc", StringComparison.OrdinalIgnoreCase) ? "descending" : "ascending";

            var skip = (paginationFilter.PageNumber - 1) * paginationFilter.PageSize;

            paginationFilter.TotalRecords = _db.RoomTypes.Count(l =>
                                                EF.Functions.Like(l.Code, $"%{paginationFilter.Keyword}%")
                                            );

            var sortBy = char.ToUpper(paginationFilter.OrderBy.First()) + paginationFilter.OrderBy.Substring(1).ToLower();

            return await _db.RoomTypes.Where(r => r.Id != (int)ERoomType.SuiteSub).ToListAsync();
        }

        public async Task<RoomType> GetByIdAsync(int id)
        {
            return await _db.RoomTypes.Where(a => a.Id == id).SingleOrDefaultAsync();
        }
        public async Task<bool> CreateAsync(RoomType roomType)
        {
            _db.RoomTypes.Add(roomType);
            var created = await _db.SaveChangesAsync();
            return created > 0;
        }
        public async Task<bool> UpdateAsync(RoomType roomType)
        {
            _db.RoomTypes.Update(roomType);
            var updated = await _db.SaveChangesAsync();
            return updated > 0;
        }
        public async Task<bool> DeleteAsync(int id)
        {
            var deleted = 0;
            var roomType = await GetByIdAsync(id);
            if (roomType != null)
            {
                _db.RoomTypes.Remove(roomType);
                deleted = await _db.SaveChangesAsync();
            }
            return deleted > 0;
        }
    }
}
