﻿using Microsoft.EntityFrameworkCore;
using OONApi.Contracts.Requests.Queires;
using OONApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace OONApi.Services
{
    public class AngelDoctorService : IAngelDoctorService
    {
        private readonly AppDbContext _db;

        public AngelDoctorService(AppDbContext db)
        {
            _db = db;
        }

        public async Task<List<AngelDoctor>> GetAllAsync(AngelDoctorSearchQuery angelDoctor, PaginationFilter paginationFilter = null)
        {
            if (paginationFilter == null)
            {
                return await _db.AngelDoctors.Include(a => a.Angel).AsNoTracking().ToListAsync();
            }

            if (typeof(AngelDoctor).GetProperties().All(s => s.Name != paginationFilter.OrderBy.First().ToString().ToUpper() + paginationFilter.OrderBy.Substring(1)))
            {
                paginationFilter.OrderBy = "Id";
            }
            string direction = null;
            if (string.Equals(paginationFilter.OrderDirection, "desc", StringComparison.OrdinalIgnoreCase))
            {
                direction = "descending";
            }
            else
            {
                direction = "ascending";
            }

            var skip = (paginationFilter.PageNumber - 1) * paginationFilter.PageSize;

            IQueryable<AngelDoctor> query = _db.AngelDoctors.Include(a => a.Angel);
            if (angelDoctor.AngelId > 0)
            {
                query = query.Where(a => a.AngelId == angelDoctor.AngelId);
            }
            if (!String.IsNullOrEmpty(angelDoctor.DoctorName))
            {
                query = query.Where(a => a.DoctorName == angelDoctor.DoctorName);
            }
            if (!String.IsNullOrEmpty(angelDoctor.Detail))
            {
                query = query.Where(a => a.Detail == angelDoctor.Detail);
            }
            if (!String.IsNullOrEmpty(paginationFilter.Keyword))
            {
                query = query.Where(a => a.DoctorName.Contains(paginationFilter.Keyword) || a.Detail.Contains(paginationFilter.Keyword));
            }
            paginationFilter.TotalRecords = query.Count();
            string sortBy = char.ToUpper(paginationFilter.OrderBy.First()) + paginationFilter.OrderBy.Substring(1).ToLower();
            if (paginationFilter == null)
            {
                return await query.AsNoTracking().ToListAsync();
            }
            return await query.OrderBy(sortBy + " " + direction).Skip(skip).Take(paginationFilter.PageSize).AsNoTracking().ToListAsync();
        }

        public async Task<AngelDoctor> GetByIdAsync(int id)
        {
            return await _db.AngelDoctors.Include(i => i.Angel).Where(a => a.Id == id).SingleOrDefaultAsync();
        }

        public async Task<bool> CreateAsync(AngelDoctor angelDoctor)
        {
            _db.AngelDoctors.Add(angelDoctor);
            var create = await _db.SaveChangesAsync();
            return create > 0;
        }
        
        public async Task<bool> UpdateAsync(AngelDoctor angelDoctor)
        {
            _db.AngelDoctors.Update(angelDoctor);
            var update = await _db.SaveChangesAsync();
            return update > 0;
        }

        public async Task<bool> DeleteAsync(int id)
        {
            var deleted = 0;
            var angelDoctor = await GetByIdAsync(id);
            if(angelDoctor != null)
            {
                _db.AngelDoctors.Remove(angelDoctor);
                deleted = await _db.SaveChangesAsync();
            }
            return deleted > 0;
        }
    }
}

