﻿using OONApi.Contracts.Requests.Queires;
using OONApi.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OONApi.Services
{
    public interface IUserService
    {
        Task<List<User>> GetAllAsync(UserSearchQuery user, PaginationFilter paginationFilter = null, bool isRole = false);
        Task<User> GetByIdAsync(int id);
        //Task<List<Role>> GetRolesByUserIdAsync(int id);
        //Task<List<UserRole>> GetUserRoleByUserId(int id);
        Task<bool> IsDuplicateUser(string username);
        Task<bool> CreateAsync(User user, List<int> roleIds);
        Task<bool> UpdateAsync(User user, List<int> roleIds);
        Task<bool> DeleteAsync(int id);
        Task<User> Authenticate(string username, string password);
        bool VerifyPassword(string enteredPassword, byte[] salt, string storedPassword);
        void ChangePassword(User user);
        Task<User> GetByIdToReportAsync(int id);
    }
}
