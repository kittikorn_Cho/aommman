﻿using OONApi.Contracts.Requests.Queires;
using OONApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Services
{
    public interface IReceptionService
    {
        Task<List<Reception>> GetAllAsync(ReceptionSearchQuery reception, PaginationFilter paginationFilter = null);
        Task<Reception> GetByIdAsync(int id);
        Task<bool> CreateAsync(Reception reception);
        Task<bool> UpdateAsync(Reception reception);
        Task<bool> DeleteAsync(int id);
        decimal GetSellAngelTypes(int receptionId, int angelTypeId, List<int> calendarIds);
        int GetSellRoomTypeSuite(int receptionId, List<int> calendarIds);
        decimal GetSellMember(int receptionId, List<int> calendarIds);
    }
}
