﻿using OONApi.Contracts.Requests.Queires;
using OONApi.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OONApi.Services
{
    public interface IDebtTypeService
    {
        Task<List<DebtType>> GetAllAsync(DebtTypeSearchQuery debtType, PaginationFilter paginationFilter = null);
        Task<DebtType> GetByIdAsync(int id);
        Task<bool> CreateAsync(DebtType debtType);
        Task<bool> UpdateAsync(DebtType debtType);
        Task<bool> DeleteAsync(int id);
    }
}
