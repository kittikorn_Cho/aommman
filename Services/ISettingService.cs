﻿using OONApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Services
{
    public interface ISettingService
    {
        Task<bool> CreateCalendarDay(CalendarDay calendarDay);
        Task<bool> UpdateCalendarDay(CalendarDay calendarDay);
        Task<CalendarDay> GetCarlendarDay();
        Task<List<CalendarDay>> GetCarlendarDayByMonth();
        Task<bool> CreateDocumentNumber(DocumentNumber documentNumbers);
        string GetDocumentNumberAndUpdate(int calendarDayId, bool updateFlag = false);
        Task<List<int>> GetCalendarId(DateTime startdate, DateTime enddate);
        Task<List<CalendarDay>> GetCalendarDayStartDateAndEndDate(DateTime startDate, DateTime endDate);
        Task<AngelWorking> GetAngelWorking(int angelId, int calendarId);
        Task<decimal> GetCountAngelWorkingByCalendarDayId(int calendarId);
        Task<List<MassageAngel>> GetMassageAngelByCalendarDayId(int calendarId);
        Task<decimal> TotalCountAngelType(int id, int calendarId);
        Task<List<AngelPerformance>> GetAngelPerformancesByCalendarDayId(int id, int calendarId);
        int GetCalendarId(DateTime startdate);
        Task<CalendarDay> GetCalendarDayById(int calendarId);
        Task<decimal> GetMassageRoomByCalendarDayId(int calendarId, int roomSuiteId, int roomSuite3Id, int roomSuite4Id, int roomSuite5Id, int roomSuite6Id, int roomSuite7Id, int roomSuite8Id, int roomSuite9Id, int roomSuite10Id, int roomC3Id, int roomC5Id);
        Task<decimal> GetMassageRoomByCalendarDayIdAndCheckIsNull(int calendarId, int roomSuiteId, int roomSuite3Id, int roomSuite4Id, int roomSuite5Id, int roomSuite6Id, int roomSuite7Id, int roomSuite8Id, int roomSuite9Id, int roomSuite10Id, int roomC3Id, int roomC5Id);
        Task<List<MassageAngel>> GetMassageAngelByCalendarDayId(int id, int calendarId);
        Task<bool> CreateSetting(Setting setting);
        Task<bool> UpdateSetting(Setting setting);
        Task<Setting> GetSettingById(int id);
    }
}
