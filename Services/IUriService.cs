﻿using OONApi.Contracts.Requests.Queires;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Services
{
    public interface IUriService
    {
        Uri GetAllSitesUri(PaginationQuery pagination = null);
    }
}
