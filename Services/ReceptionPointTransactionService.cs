﻿using Microsoft.EntityFrameworkCore;
using OONApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using static OONApi.Contracts.ApiRoutes.ApiRoutes;


namespace OONApi.Services
{
    public class ReceptionPointTransactionService : IReceptionPointTransactionService
    {
        private readonly AppDbContext _db;

        public ReceptionPointTransactionService(AppDbContext db)
        {
            _db = db;
        }

        public async Task<bool> CreateReceptionPointTransaction(ReceptionPointTransaction receptionPointTransaction) 
        {
            _db.ReceptionPointTransactions.Add(receptionPointTransaction);
            var created = await _db.SaveChangesAsync();
            return created > 0;
        }
        public async Task<List<ReceptionPointTransaction>> GetReceptionPointTransactionByMassageAngelId(List<int> massageAngelId)
        {
            return await _db.ReceptionPointTransactions.Where(rpt => massageAngelId.Contains(rpt.MassageAngelId)).ToListAsync();
        }
        public async Task<ReceptionPointTransaction> GetByIdAsync(int id)
        {
            return await _db.ReceptionPointTransactions.Where(rpt => rpt.Id == id).SingleOrDefaultAsync();
        }
        public async Task<bool> DeleteReceptionPointTransation(int id)
        {
            var deleted = 0;
            var receptionPointTransaction = await GetByIdAsync(id);
            if (receptionPointTransaction != null)
            {
                _db.ReceptionPointTransactions.Remove(receptionPointTransaction);
                deleted = await _db.SaveChangesAsync();
            }
            return deleted > 0;
        }
        public async Task<ReceptionPointTransaction> GetReceptionPointTransactionByMassageAngelIdAndReceptionId(int maasageAngelId, int receptionId)
        {
            return await _db.ReceptionPointTransactions.Where(rp => rp.MassageAngelId == maasageAngelId && rp.ReceptionId == receptionId).SingleOrDefaultAsync();
        }
    }
}
