﻿using DinkToPdf.Contracts;
using DocumentFormat.OpenXml.Bibliography;
using DocumentFormat.OpenXml.Drawing;
using DocumentFormat.OpenXml.Office2010.Excel;
using DocumentFormat.OpenXml.Office2016.Drawing.ChartDrawing;
using DocumentFormat.OpenXml.Wordprocessing;
using OONApi.Contracts.Responses;
using OONApi.Enum;
using OONApi.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using static OONApi.Contracts.ApiRoutes.ApiRoutes;

namespace OONApi.Services
{
    public class GenerateReportService : IGenerateReportService
    {
        private readonly AppDbContext _db;
        public GenerateReportService(AppDbContext db)
        {
            _db = db;
        }
        public string MemberSell(DateTime startDate, DateTime endDate, List<ReportMemberSellResponse> responses, SettingResponse setting)
        {
            string sDate = startDate.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));
            string eDate = endDate.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));
            string qrCodeorTrCode = "";
            if (setting.IsQrCodeReport)
            {
                qrCodeorTrCode = "TR Code";
            }
            else
            {
                qrCodeorTrCode = "QR Code";
            }

            var html =
                $@"
                  <!DOCTYPE html>
                        <html>
                            <head>
                            </head>
                            <body>
                                <div class='text-center'><h4>สรุปยอดขายเมมเบอร์ประจำวันที่ {sDate} จนถึงวันที่ {eDate}</h4></div>
                                <table>
                                  <thead>
                                    <tr>
                                      <th>ลำดับ</th>
                                      <th>วันที่</th>
                                      <th>เมมเบอร์</th>
                                      <th>ชื่อ</th>
                                      <th>เชียร์แขก</th>
                                      <th>ราคา</th>    
                                      <th>เงินสด</th>
                                      <th>เครดิต</th>
                                      <th>{qrCodeorTrCode}</th>
                                      <th>ค้างชำระ</th>
                                      <th>%</th>
                                      <th>ยอด%</th>  
                                      <th>ผู้ทำรายการ</th>
                                    </tr>
                                  <tbody>
                                    [content]
                                  </tbody>
                                  </thead>
                                </table>
                                <br>
                                <div class='text-center'><h4>สรุปยอดรวม</h4></div>
                                <table>
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>ยอดขายเมมเบอร์</th>
                                            <th>เงินสด</th>
                                            <th>เครดิต</th>
                                            <th>{qrCodeorTrCode}</th>
                                            <th>ค้างชำระ</th>
                                            <th>ยอด %</th>
                                        </tr>
                                    <tbody>
                                       [footer]
                                    </tbody>
                                    </thead>
                                </table>
                            </body>
                        </html>
                    ";

            string content = "";
            string footer = "";
            decimal sumToTal = 0;
            decimal sumCashTotal = 0;
            decimal sumCreditTotal = 0;
            decimal sumUnPaid = 0;
            decimal sumQrCodeTotal = 0;
            decimal sumPercentTotalTopup = 0;
            int i = 0;
            foreach (var item in responses)
            {
                i++;
                decimal unPaid = item.Total - (item.CashTotal + item.CreditTotal + item.EntertainTotal + item.QrCodeTotal);
                string toDay = item.CalendarDay.ToDay.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));
                var toTal = String.Format("{0:N}", item.Total);
                var cashTotal = String.Format("{0:N}", item.CashTotal);
                var creditTotal = String.Format("{0:N}", item.CreditTotal);
                var _unPaid = String.Format("{0:N}", unPaid);
                var qrCodeTotal = String.Format("{0:N}", item.QrCodeTotal);
                var percentTopupTotal = String.Format("{0:N}", item.PercentTotalTopup);
                string user = "";
                var percentTopup = item.PercentTopup + "%";
                if (item.User != null)
                {
                    user = item.User.UserFullname;
                }
                content += $@"
                <tr class='text-center'>
                    <td>{i}</td>
                    <td>{toDay}</td>
                    <td>{item.Member.Code}</td>
                    <td>{item.Member.Firstname + "<br>" + item.Member.Lastname}</td>
                    <td>{item.Reception.Nickname}</td>
                    <td>{toTal}</td>
                    <td>{cashTotal}</td>
                    <td>{creditTotal}</td>
                    <td>{qrCodeTotal}</td>
                    <td>{_unPaid}</td>
                    <td>{percentTopup}</td>
                    <td>{percentTopupTotal}</td>
                    <td>{user}</td>
                </tr>
                ";
                sumToTal += item.Total;
                sumCashTotal += item.CashTotal;
                sumCreditTotal += item.CreditTotal;
                sumUnPaid += unPaid;
                sumQrCodeTotal += item.QrCodeTotal;
                sumPercentTotalTopup += item.PercentTotalTopup;
            }
            var _sumToTal = String.Format("{0:N}", sumToTal);
            var _sumCashTotal = String.Format("{0:N}", sumCashTotal);
            var _sumCreditTotal = String.Format("{0:N}", sumCreditTotal);
            var _sumUnPaid = String.Format("{0:N}", sumUnPaid);
            var _sumQrCode = String.Format("{0:N}", sumQrCodeTotal);
            footer += $@"
                <tr class='text-center'>
                    <td>ยอดรวม</td>
                    <td>{_sumToTal}</td>
                    <td>{_sumCashTotal}</td>
                    <td>{_sumCreditTotal}</td>
                    <td>{_sumQrCode}</td>
                    <td>{_sumUnPaid}</td>
                    <td>{sumPercentTotalTopup}</td>
                </tr>
            ";

            string htmlGroup = html.Replace("[content]", content).Replace("[footer]", footer);

            return htmlGroup.ToString();
        }

        public string SummaryDay(DateTime startDate, DateTime endDate, List<ReportSummaryDayResponse> responses, SummaryMassageResponse responseSummary, SettingResponse setting)
        {
            string sDate = startDate.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));
            string eDate = endDate.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));
            string qrCodeorTrCode = "";
            if (setting.IsQrCodeReport)
            {
                qrCodeorTrCode = "TR Code";
            }
            else
            {
                qrCodeorTrCode = "QR Code";
            }
            var html =
                 $@"
                  <!DOCTYPE html>
                        <html>
                            <head>
                            </head>
                            <body>
                                <div class='text-center'><h4>ใบสรุปส่งเงินประจำวันที่ {sDate} จนถึงวันที่ {eDate}</h4></div>
                                <table>
                                  <thead>
                                    <tr>
                                      <th>ประเภทพนักงาน</th>
                                      <th>จำนวนรอบ</th>
                                      <th>ยอดค่าตั้งต้น</th>
                                      <th>ส่วนลดของนวด</th>
                                      <th>ยอดคงเหลือที่หักจากส่วนนวด</th>
                                    </tr>
                                  <tbody>
                                    [content]
                                  </tbody>
                                  <tfoot>
                                    [footer]
                                  </tfoot>
                                  </thead>
                                </table>
                                 [summary]
                            </body>
                        </html>
                    ";

            string content = "";
            string footer = "";
            string summary = "";
            decimal totalRound = 0;
            decimal totalSummary = 0;
            foreach (var item in responses)
            {
                var totalRounds = String.Format("{0:N}", item.TotalRound);
                var sum = String.Format("{0:N}", item.Summary);
                content += $@"
                <tr class='text-center'>
                    <td>{item.Code}</td>
                    <td>{totalRounds}</td>
                    <td>{sum}</td>
                    <td>{0.00}</td>
                    <td>{sum}</td>
                </tr>
                ";
                totalRound += item.TotalRound;
                totalSummary += item.Summary;
            }
            var _totalRound = String.Format("{0:N}", totalRound);
            var _totalSummary = String.Format("{0:N}", totalSummary);
            footer += $@"
                <tr class='text-center'>
                    <td>ยอดรวม</td>
                    <td>{_totalRound}</td>
                    <td>{_totalSummary}</td>
                    <td>{0.00}</td>
                    <td>{_totalSummary}</td>
                </tr>
                ";
            var cashAngelTotal = String.Format("{0:N}", responseSummary.CashAngelTotal);
            var creditAngelTotal = String.Format("{0:N}", responseSummary.CreditAngelTotal);
            var cashFoodTotal = String.Format("{0:N}", responseSummary.CashFoodTotal);
            var creditFoodTotal = String.Format("{0:N}", responseSummary.CreditFoodTotal);
            var totalCash = String.Format("{0:N}", responseSummary.TotalCash);
            var totalCredit = String.Format("{0:N}", responseSummary.TotalCredit);
            var memberAngelTotal = String.Format("{0:N}", responseSummary.MemberAngelTotal);
            var totalRoomSuite = String.Format("{0:N}", responseSummary.TotalRoomSuite);
            var memberFoodTotal = String.Format("{0:N}", responseSummary.MemberFoodTotal);
            var cutMemberFood = String.Format("{0:N}", responseSummary.CutMemberFood);
            var totalRoomVip = String.Format("{0:N}", responseSummary.TotalRoomVip);
            var totalMember = String.Format("{0:N}", responseSummary.TotalMember + responseSummary.CutMemberFood);
            var summaryUnpaid = String.Format("{0:N}", responseSummary.SummaryUnpaid);
            var OtherServiceChargesTotal = String.Format("{0:N}", responseSummary.OtherServiceChargesTotal);
            var DamagesTotal = String.Format("{0:N}", responseSummary.DamagesTotal);
            var totalRoom = String.Format("{0:N}", responseSummary.TotalRoomVip + responseSummary.TotalRoomSuite);

            var qrCodeAngelTotal = String.Format("{0:N}", responseSummary.QrCodeAngelTotal);
            var qrCodeFoodTotal = String.Format("{0:N}", responseSummary.QrCodeFoodTotal);
            var totalQrCode = String.Format("{0:N}", responseSummary.TotalQrCode);

            var cashRoomSuiteTotal = String.Format("{0:N}", responseSummary.CashRoomSuiteTotal);
            var creditRoomSuiteTotal = String.Format("{0:N}", responseSummary.CreditRoomSuiteTotal);
            var memberRoomSuiteTotal = String.Format("{0:N}", responseSummary.MemberRoomSuiteTotal);
            var qrCodeRoomSuiteTotal = String.Format("{0:N}", responseSummary.QrCodeRoomSuiteTotal);

            var cashRoomVipTotal = String.Format("{0:N}", responseSummary.CashRoomVipTotal);
            var creditRoomVipTotal = String.Format("{0:N}", responseSummary.CreditRoomVipTotal);
            var memberRoomVipTotal = String.Format("{0:N}", responseSummary.MemberRoomVipTotal);
            var qrCodeRoomVipTotal = String.Format("{0:N}", responseSummary.QrCodeRoomVipTotal);

            var tipTotal = String.Format("{0:N}", responseSummary.TipTotal);
            var tipQrCode = String.Format("{0:N}", responseSummary.TipQrCodeTotal);

            summary += $@"
                <br>
                 <table class='no-border'>
                    <tr>
                        <td class='table-header no-border text-left'><b>ยอดใช้เงินสด</b></td>
                        <td class='no-border'></td>
                        <td class='table-header no-border text-left'><b>ยอดใช้บัตรเครดิต</b></td>
                    <tr>
                    <tr>
                        <td class='table-header no-border text-left'>ยอดใช้เงินสดค่านวด : {cashAngelTotal} บาท</td>
                        <td class='no-border'></td>
                        <td class='table-header no-border text-left'>ยอดใช้บัตรเครดิตค่านวด : {creditAngelTotal} บาท</td>
                    <tr>
                    <tr>
                        <td class='table-header no-border text-left'>ยอดใช้เงินสดค่าอาหาร : {cashFoodTotal} บาท</td>
                        <td class='no-border'></td>
                        <td class='table-header no-border text-left'>ยอดใช้บัตรเครดิตค่าอาหาร : {creditFoodTotal} บาท</td>
                    <tr>
                    <tr>
                        <td class='table-header no-border text-left'>รวม : {totalCash} บาท</td>
                        <td class='no-border'></td>
                        <td class='table-header no-border text-left'>รวม : {totalCredit} บาท</td>
                    <tr>
                 </table>
                <br>
                 <table class='no-border'>
                    <tr>
                        <td class='table-header no-border text-left'><b>ยอดใช้เมมเบอร์</b></td>
                        <td class='no-border'></td>
                        <td class='table-header no-border text-left'><b>ยอดใช้ {qrCodeorTrCode}</b></td>
                    <tr>
                    <tr>
                        <td class='table-header no-border text-left'>ยอดใช้เมมเบอร์ค่านวด : {memberAngelTotal} บาท</td>
                        <td class='no-border'></td>
                        <td class='table-header no-border text-left'>ใช้ {qrCodeorTrCode} ค่านวด : {qrCodeAngelTotal} บาท</td>
                    <tr>
                    <tr>
                        <td class='table-header no-border text-left'>ยอดใช้เมมเบอร์ค่าอาหาร : {memberFoodTotal} บาท</td>
                        <td class='no-border'></td>
                        <td class='table-header no-border text-left'>ใช้ {qrCodeorTrCode} ค่าอาหาร : {qrCodeFoodTotal} บาท</td>
                    <tr>
                    <tr>
                        <td class='table-header no-border text-left'>ยอดใช้ตัดค่าอาหารเมมเบอร์ : {cutMemberFood} บาท</td>
                        <td class='no-border'></td>
                        <td class='table-header no-border text-left'>รวม : {totalQrCode} บาท</td>
                    <tr>
                    <tr>
                        <td class='table-header no-border text-left'>รวม : {totalMember} บาท</td>
                        <td class='no-border'></td>
                        <td class='table-header no-border text-left'></td>
                    <tr>
                 </table>
                <br>
                 <table class='no-border'>
                    <tr>
                        <td class='table-header no-border text-left'><b>ยอดชำระห้อง</b></td>
                        <td class='no-border'></td>
                        <td class='table-header no-border text-left'><b>ยอดชำระค่าอื่นๆ</b></td>
                    <tr>
                    <tr>
                       <td class='table-header no-border text-left'>ยอดใช้ห้องสูท(เงินสด) : {cashRoomSuiteTotal} บาท</td>
                       <td class='no-border'></td>
                       <td class='table-header no-border text-left'>ยอดหลังเที่ยงคืน : {OtherServiceChargesTotal} บาท</td>
                    <tr>
                    <tr>
                       <td class='table-header no-border text-left'>ยอดใช้ห้องสูท(เครดิต) : {creditRoomSuiteTotal} บาท</td>
                       <td class='no-border'></td>
                       <td class='table-header no-border text-left'>ทิปเมมเบอร์/ค่าเสียหาย : {DamagesTotal} บาท</td>
                    <tr>
                    <tr>
                       <td class='table-header no-border text-left'>ยอดใช้ห้องสูท(เมมเบอร์) : {memberRoomSuiteTotal} บาท</td>
                       <td class='no-border'></td>
                       <td class='table-header no-border text-left'>ทิปบัตรเครดิต : {tipTotal}</td>
                    <tr>
                    <tr>
                       <td class='table-header no-border text-left'>ยอดใช้ห้องสูท({qrCodeorTrCode}) : {qrCodeRoomSuiteTotal} บาท</td>
                       <td class='no-border'></td>
                       <td class='table-header no-border text-left'>ทิป {qrCodeorTrCode} : {tipQrCode}</td>
                    <tr>
                    <tr>
                       <td class='table-header no-border text-left'>รวมใช้ห้องสูท : {totalRoomSuite} บาท</td>
                       <td class='no-border'></td>
                       <td class='table-header no-border text-left'></td>
                    <tr>
                    <tr>
                       <td class='table-header no-border text-left'>ยอดใช้ห้องVip(เงินสด) : {cashRoomVipTotal} บาท</td>
                       <td class='no-border'></td>
                       <td class='table-header no-border text-left'></td>
                    <tr>
                    <tr>
                       <td class='table-header no-border text-left'>ยอดใช้ห้องVip(เครดิต) : {creditRoomVipTotal} บาท</td>
                       <td class='no-border'></td>
                       <td class='table-header no-border text-left'></td>
                    <tr>
                    <tr>
                       <td class='table-header no-border text-left'>ยอดใช้ห้องVip(เมมเบอร์) : {memberRoomVipTotal} บาท</td>
                       <td class='no-border'></td>
                       <td class='table-header no-border text-left'></td>
                    <tr>
                    <tr>
                       <td class='table-header no-border text-left'>ยอดใช้ห้องVip({qrCodeorTrCode}) : {qrCodeRoomVipTotal} บาท</td>
                       <td class='no-border'></td>
                       <td class='table-header no-border text-left'></td>
                    <tr>
                    <tr>
                       <td class='table-header no-border text-left'>รวมใช้ห้องvip : {totalRoomVip} บาท</td>
                       <td class='no-border'></td>
                       <td class='table-header no-border text-left'></td>
                    <tr>
                    <tr>
                        <td class='table-header no-border text-left'>รวมยอดใช้ห้องทั้งหมด : {totalRoom} บาท</td>
                        <td class='no-border'></td>
                        <td class='no-border'></td>
                    <tr>
                 </table>
                <br>
                 <table class='no-border'>
                    <tr>
                        <td class='table-header no-border text-left'><b>ยอดค้างชำระ : {summaryUnpaid} บาท</b></td>
                    <tr>
                 </table>
                ";
            string htmlGroup = html.Replace("[content]", content).Replace("[footer]", footer).Replace("[summary]", summary);

            return htmlGroup.ToString();
        }
        public string MemberPayment(DateTime startDate, DateTime endDate, List<ReportMemberPaymentResponse> responses)
        {
            string sDate = startDate.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));
            string eDate = endDate.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));
            var html =
                 $@"
                  <!DOCTYPE html>
                        <html>
                            <head>
                            </head>
                            <body>
                                <div class='text-center'><h4>สรุปยอดใช้สิทธิ์เมมเบอร์ประจำวันที่ {sDate} จนถึงวันที่ {eDate}</h4></div>
                                <table>
                                  <thead>
                                    <tr>
                                      <th>เมมเบอร์</th>
                                      <th>ยอดนวด</th>
                                      <th>ยอดอาหาร</th>
                                      <th>ยอดห้อง</th>
                                      <th>สิทธิ์สูท</th>
                                      <th>ห้อง(รอบ)</th>    
                                      <th>รวมสิทธิ์</th>
                                      <th>ยอดคงเหลือ</th>
                                      <th>อื่นๆ...</th>
                                    </tr>
                                  <tbody>
                                    [content]
                                  </tbody>
                                  </thead>
                                </table>
                                <br>
                                <div class='text-center'><h4>สรุปยอดรวม</h4></div>
                                <table>
                                    <thead>
                                         <tr>
                                          <th>#</th>
                                          <th>ยอดนวด</th>
                                          <th>ยอดอาหาร</th>
                                          <th>ยอดห้อง</th>
                                          <th>สิทธิ์สูท</th> 
                                          <th>รวมสิทธิ์</th>
                                          <th>ยอดคงเหลือ</th>
                                          <th>อื่นๆ...</th>
                                        </tr>
                                  <tbody>
                                    [footer]
                                  </tbody>
                                    </thead>
                                </table>
                            </body>
                        </html>
                    ";

            string content = "";
            string footer = "";
            decimal sumMemberAngelTotal = 0;
            decimal sumMemberFoodTotal = 0;
            decimal sumUseSuite = 0;
            decimal sumMemberTotal = 0;
            decimal sumCreditAmount = 0;
            decimal sumRoomMember = 0;
            decimal sunUseVip = 0;
            foreach (var item in responses)
            {
                string roomNo = "";
                //var memberAngelTotal = String.Format("{0:N}", item.Massage.MemberAngelTotal);
                //var memberFoodTotal = String.Format("{0:N}", item.Massage.MemberFoodTotal);
                //var totalRoomMember = String.Format("{0:N}", item.Massage.MemberRoomTotal);
                var memberAngelTotal = String.Format("{0:N}", item.MemberMassageTotal);
                var memberFoodTotal = String.Format("{0:N}", item.MemberFoodTotal);
                var totalRoomMember = String.Format("{0:N}", item.MemberRoomTotal);
                var useSuite = "";
                var _useSuite = String.Format("{0:N}", item.UseSuite);
                var _useVip = String.Format("{0:N}", item.UseVip);
                decimal _memberTotal = item.MemberMassageTotal + item.MemberFoodTotal;
                var memberTotal = String.Format("{0:N}", _memberTotal);
                var creditAmount = String.Format("{0:N}", item.Member.CreditAmount);
                if (item.MassageRooms != null)
                {
                    foreach (var massageRoom in item.MassageRooms)
                    {
                        if (massageRoom.Room.RoomTypeId == (int)ERoomType.Suite3 || massageRoom.Room.RoomTypeId == (int)ERoomType.Suite4 || massageRoom.Room.RoomTypeId == (int)ERoomType.Suite5 || massageRoom.Room.RoomTypeId == (int)ERoomType.Suite6 || massageRoom.Room.RoomTypeId == (int)ERoomType.Suite7 || massageRoom.Room.RoomTypeId == (int)ERoomType.Suite8 || massageRoom.Room.RoomTypeId == (int)ERoomType.Suite9 || massageRoom.Room.RoomTypeId == (int)ERoomType.Suite10 || massageRoom.Room.RoomTypeId == (int)ERoomType.C3 || massageRoom.Room.RoomTypeId == (int)ERoomType.C5)
                        {
                            roomNo = massageRoom.Room.RoomNo + " " + "(" + _useSuite + ")";
                        }
                        else if (massageRoom.Room.RoomTypeId == (int)ERoomType.Vip || massageRoom.Room.RoomTypeId == (int)ERoomType.Vip2 || massageRoom.Room.RoomTypeId == (int)ERoomType.Vip3)
                        {
                            roomNo = massageRoom.Room.RoomNo + " " + "(" + _useVip + ")";
                        }
                    }
                }
                else
                {
                    foreach (var massageMemberAndRoom in item.MassageMemberAndRooms)
                    {

                        if (massageMemberAndRoom.MassageRoom.Room.RoomTypeId != (int)ERoomType.Normal || massageMemberAndRoom.MassageRoom.Room.RoomTypeId != (int)ERoomType.SuiteSub || massageMemberAndRoom.MassageRoom.Room.RoomTypeId != (int)ERoomType.RoundSell)
                        {
                            var _roundSuite = String.Format("{0:N}", massageMemberAndRoom.Round);
                            roomNo += massageMemberAndRoom.MassageRoom.Room.RoomNo + " " + "(" + _roundSuite + ")" + "<br>";
                            //roomNo = massageMemberAndRoom.MassageRoom.Room.RoomNo + " " + "(" + _useSuite + ")";
                        }
                    }
                }

                if (item.UseSuite == 0 && item.UseVip != 0)
                {
                    useSuite = String.Format("{0:N}", item.UseVip);
                }
                else if (item.UseSuite != 0 && item.UseVip == 0)
                {
                    useSuite = String.Format("{0:N}", item.UseSuite);
                }
                else if (item.UseSuite != 0 && item.UseVip != 0)
                {
                    useSuite = String.Format("{0:N}", item.UseSuite + item.UseVip);
                }
                else
                {
                    useSuite = "0.00";
                }

                content += $@"
                <tr class='text-center'>
                    <td>{item.Member.Code}</td>
                    <td>{memberAngelTotal}</td>
                    <td>{memberFoodTotal}</td>
                    <td>{totalRoomMember}</td>
                    <td>{useSuite}</td>
                    <td>{roomNo}</td>
                    <td>{memberTotal}</td>
                    <td>{creditAmount}</td>
                    <td>  </td>
                </tr>
                ";
                sumMemberAngelTotal += item.MemberMassageTotal;
                sumMemberFoodTotal += item.MemberFoodTotal;
                sumUseSuite += item.UseSuite;
                sumMemberTotal += _memberTotal;
                sumCreditAmount += item.Member.CreditAmount;
                sumRoomMember += item.MemberRoomTotal;
                sunUseVip += item.UseVip;
            }
            var _sumMemberAngelTotal = String.Format("{0:N}", sumMemberAngelTotal);
            var _sumMemberFoodTotal = String.Format("{0:N}", sumMemberFoodTotal);
            var _sumUseSuite = String.Format("{0:N}", sumUseSuite + sunUseVip);
            var _sumMemberTotal = String.Format("{0:N}", sumMemberTotal);
            var _sumCreditAmount = String.Format("{0:N}", sumCreditAmount);
            var _sumRoomMemberTotal = String.Format("{0:N}", sumRoomMember);
            footer += $@"
                <tr class='text-center'>
                    <td >ยอดรวม</td>
                    <td>{_sumMemberAngelTotal}</td>
                    <td>{_sumMemberFoodTotal}</td>
                    <td>{_sumRoomMemberTotal}</td>
                    <td>{_sumUseSuite}</td>
                    <td>{_sumMemberTotal}</td>
                    <td>{_sumCreditAmount}</td>
                    <td><td>
                </tr>
            ";
            string htmlGroup = html.Replace("[content]", content).Replace("[footer]", footer);

            return htmlGroup.ToString();
        }

        public string PaymentAngelPerformance(DateTime startDate, DateTime endDate, List<ReportPaymentAngelPerformanceResponse> responses, SummaryTotalDebtResponse responseSummaryDebt, List<DebtTypeResponse> debtTypes, List<DeductTypeResponse> deductTypes)
        {
            string sDate = startDate.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));
            string eDate = endDate.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));
            var html =
                 $@"
                  <!DOCTYPE html>
                        <html>
                            <head>
                            </head>
                            <body>
                                <div class='text-center'><h4>เงินสดสำรองจากการเงิน (เงินทอน) จากวันที่ {sDate} จนถึงวันที่ {eDate}</h4></div>
                                <div class='text-left'><b>หัก</b></div>
                                [content]
                                [sum]
                                <br>
                                <table>
                                  <thead>
                                    <tr>
                                      <th style='width:15%'>ประเภท</th>
                                      <th style='width:8%'>1</th>
                                      <th style='width:8%'>2</th>
                                      <th style='width:8%'>5</th>
                                      <th style='width:8%'>10</th>
                                      <th style='width:8%'>20</th>
                                      <th style='width:8%'>50</th>
                                      <th style='width:8%'>100</th>
                                      <th style='width:8%'>500</th>
                                      <th style='width:8%'>1000</th>
                                    </tr>
                                  <tbody>
                                    [cash]
                                  </tbody>
                                  </thead>
                                </table>
                            </body>
                        </html>
                    ";
            string content = "";
            string sum = "";
            string cash = "";
            string debtType = "";
            string deductType = "";
            decimal summary = 0;
            decimal summaryPerformance_ = 0;
            foreach (var item in responses)
            {
                var _summary = String.Format("{0:N}", item.Summary);
                var wage = String.Format("{0:N}", item.Wage);
                var totalRound = String.Format("{0:N}", item.TotalRound);
                summary += item.Summary;
                content += $@"
                <table class='no-border'>
                    <tr>
                        <td class='table-header no-border text-left'>ค่ารอบพนักงาน{item.Code}({wage}x{totalRound})</td>
                        <td class='table-header no-border text-right'>{_summary} บาท</td>
                    </tr>
                </table>
                ";
            }
            var summaryTotal = String.Format("{0:N}", summary);
            decimal TotalSumCommDamageTip = (responseSummaryDebt.DamagesTotal * 10) / 100;
            decimal TotalSumCommTip = (responseSummaryDebt.TotalTip * 10) / 100;
            decimal TotalSumCommTipQrCode = (responseSummaryDebt.TipQrCodeTotal * 10) / 100;
            decimal totalETC = TotalSumCommTip + TotalSumCommTipQrCode;
            int totalTipUp = (int)Math.Ceiling(totalETC);
            var totalTipSumAll = String.Format("{0:N}", totalTipUp);
            var totalDailyDebt = String.Format("{0:N}", responseSummaryDebt.TotalDailyDebt);
            var totalDebt = String.Format("{0:N}", responseSummaryDebt.TotalDebt);
            //var totalSummaryDebt = String.Format("{0:N}", responseSummaryDebt.TotalSummaryDebt);
            var TotalDeduct = String.Format("{0:N}", responseSummaryDebt.TotalDeduct);
            var totalSummary = totalTipUp + responseSummaryDebt.TotalDailyDebt + responseSummaryDebt.TotalDebt + responseSummaryDebt.TotalDeduct;
            var _totalSummary = String.Format("{0:N}", totalSummary);
            var _totalAll = String.Format("{0:N}", (summary + responseSummaryDebt.TotalTip + responseSummaryDebt.TipQrCodeTotal) - totalSummary);
            var totalTip = String.Format("{0:N}", responseSummaryDebt.TotalTip + responseSummaryDebt.TipQrCodeTotal);
            var totalAngel = String.Format("{0:N}", summary + responseSummaryDebt.TotalTip + responseSummaryDebt.TipQrCodeTotal);

            sum += $@"
                <div class='text-right'>
                    <b>รวม : {summaryTotal} บาท</b><br>
                    <b>ทิป : {totalTip} บาท</b>
                    <p>เงินสดคงเหลือหลังหักค่าพนักงาน : {totalAngel} บาท</p>
                    <p>บวก ค่าคอมรับรอง 100% : ____________</p>
                    <p>รวมเงินสดส่วนนวด : ____________</p>
                </div>
                <div class='text-left'><b>บวก</b></div>
                <table class='no-border'>
                    <tr>
                        <td class='table-header no-border text-left'>รายได้ค่าคอมบัตรเครดิต + คูปอง + เมมเบอร์ + ทิป</td>
                        <td class='table-header no-border text-right'>{totalTipSumAll} บาท</td>
                    </tr>
                    <tr>
                        <td class='table-header no-border text-left'>รายได้ค่าประจำวัน</td>
                        <td class='table-header no-border text-right'>{totalDailyDebt} บาท</td>
                    </tr>
                    <tr>
                        <td class='table-header no-border text-left'><b>รวมรายได้ค่าคงที่</b></td>
                        <td class='table-header no-border text-right'><b>{totalDebt} บาท</b></td>
                    </tr>

                    [debtType]

                    <tr>
                        <td class='table-header no-border text-left'><b>รวมรายได้หักต่อรอบ</b></td>
                        <td class='table-header no-border text-right'><b>{TotalDeduct} บาท</b></td>
                    </tr>
                    
                    [deductType]

                </table>
                <div class='text-right'>
                    <b>รวม : {_totalSummary} บาท</b><br>
                </div>
                <div class='text-right'>
                    <b>ยอดจ่าย : {_totalAll} บาท</b><br>
                    <h4>ยอดเงินสดรับประจำวัน : ____________</h4>
                </div>
                <div class='text-left'>
                    <b>บวกรับเงินจากแคชเชียร์ล่วงหน้า ____________</b><br>
                    <b>รวมเงินสดส่งการเงินทั้งสิ้น ____________ แคชเชียร์ ____________ การเงิน</b>
                </div>
            ";

            foreach (var debtTypeDetail in debtTypes)
            {
                var sumperformance = String.Format("{0:N}", debtTypeDetail.SummaryPerformance);

                debtType += $@"

                    <tr>
                        <td class='table-header no-border text-left'>{debtTypeDetail.Name}</td>
                        <td class='table-header no-border text-right'>{sumperformance} บาท</td>
                    </tr>

                ";
            }

            foreach (var deductTypeDetail in deductTypes)
            {
                summaryPerformance_ += deductTypeDetail.SummaryPerformance;
            }
            decimal receiveDeduct = responseSummaryDebt.TotalDeduct - summaryPerformance_;

            foreach (var deductTypeDetail in deductTypes)
            {
                var sumperformance = "";

                if (deductTypeDetail.Slug == "Comm")
                {
                    sumperformance = String.Format("{0:N}", deductTypeDetail.SummaryPerformance + receiveDeduct);
                }
                else
                {
                    sumperformance = String.Format("{0:N}", deductTypeDetail.SummaryPerformance);
                }

                deductType += $@"

                    <tr>
                        <td class='table-header no-border text-left'>{deductTypeDetail.Name}</td>
                        <td class='table-header no-border text-right'>{sumperformance} บาท</td>
                    </tr>
                ";
            }
            cash += $@"
                <tr class='text-center'>
                    <td>จำนวน</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            ";

            string htmlGroup = html.Replace("[content]", content).Replace("[sum]", sum).Replace("[debtType]", debtType).Replace("[deductType]", deductType).Replace("[cash]", cash);

            return htmlGroup.ToString();
        }

        public string DiscountMassageAngel(DateTime startDate, DateTime endDate, List<ReportDiscountAngelResponse> responses)
        {
            string sDate = startDate.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));
            string eDate = endDate.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));

            var html =
              $@"
                    <!DOCTYPE html>
                        <html>
                            <head></head>
                            <body>
                                <div class='text-center'><h4>รายงานยอดรับรองส่วนนวด จากวันที่ {sDate} จนถึงวันที่ {eDate}</h4></div>
                                <table>
                                  <thead>
                                    <tr>
                                      <th>วันที่</th>
                                      <th>รับรองนวด</th>
                                      <th>จำนวนรอบ</th>
                                      <th>พนักงาน</th>
                                      <th>เชียร์แขก</th>
                                    </tr>
                                  <tbody>
                                    [content]
                                  </tbody>
                                  </thead>
                                </table>
                                <br>
                                <table>
                                  <tfoot>
                                    [footer]
                                  </tfoot>
                                </table>
                            </body>
                        </html>
                ";
            string content = "";
            string footer = "";
            decimal sumRound = 0;
            foreach (var item in responses)
            {
                var calendarDay = _db.CalendarDays.Where(c => c.Id == item.Massage.CalendarDayId).SingleOrDefault();
                string _calendarDay = calendarDay.ToDay.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));
                DateTime checkInTime = (DateTime)item.CheckInTime;
                DateTime checkOutTime = (DateTime)item.CheckOutTime;
                string _checkInTime = checkInTime.ToString("HH:mm", new CultureInfo("th-TH"));
                string _checkOutTime = checkOutTime.ToString("HH:mm", new CultureInfo("th-TH"));

                string discountRemark = "";
                if (item.IsDiscountBaht)
                {
                    discountRemark += "รับรอง " + item.DiscountBaht + " บาท ";
                }
                if (item.IsDiscountPercent)
                {
                    discountRemark += "รับรอง " + item.DiscountPercent + " % ";
                }
                if (item.IsDiscountRound)
                {
                    discountRemark += "ลดรอบละ " + item.DiscountRound + " บาท ";
                }
                if (!String.IsNullOrEmpty(item.DiscountRemark))
                {
                    discountRemark = discountRemark + " " + item.DiscountRemark;
                }

                discountRemark = discountRemark + "     " + " เวลา " + " " + _checkInTime + " - " + _checkOutTime + " น.";

                content += $@"
                <tr class='text-center'>
                    <td>{_calendarDay}</td>
                    <td>{discountRemark}</td>
                    <td>{item.Round}</td>
                    <td>{item.Angel.Code}</td>
                    <td>{item.Reception.Nickname}</td>
                </tr>
                ";

                sumRound += item.Round;
            }

            footer += $@"
                <tr class='text-center'>
                    <td colspan='2'>ยอดรวม</td>
                    <td>{sumRound}</td>
                </tr>
            ";

            string htmlGroup = html.Replace("[content]", content).Replace("[footer]", footer);

            return htmlGroup.ToString();
        }
        public string DiscountMassageRoom(DateTime startDate, DateTime endDate, List<ReportDiscountRoomResponse> responses)
        {
            string sDate = startDate.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));
            string eDate = endDate.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));
            var html =
                $@"
                    <!DOCTYPE html>
                        <html>
                            <head></head>
                            <body>
                                <div class='text-center'><h4>รายงานยอดรับรองห้องสูท จากวันที่ {sDate} จนถึงวันที่ {eDate}</h4></div>
                                <table>
                                  <thead>
                                    <tr>
                                      <th>วันที่</th>
                                      <th>รับรองห้องสูท</th>
                                      <th>จำนวนชั่วโมง</th>
                                    </tr>
                                  <tbody>
                                    [content]
                                  </tbody>
                                  </thead>
                                </table>
                                <br>
                                <table>
                                  <tfoot>
                                    [footer]
                                  </tfoot>
                                </table>
                            </body>
                        </html>
                ";
            string content = "";
            string footer = "";
            int sumTime = 0;
            string _time = "";
            foreach (var item in responses)
            {
                var calendarDay = _db.CalendarDays.Where(c => c.Id == item.Massage.CalendarDayId).SingleOrDefault();
                string _calendarDay = calendarDay.ToDay.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));
                DateTime checkInTime = (DateTime)item.CheckInTime;
                DateTime checkOutTime = (DateTime)item.CheckOutTime;
                string _checkInTime = checkInTime.ToString("HH:mm", new CultureInfo("th-TH"));
                string _checkOutTime = checkOutTime.ToString("HH:mm", new CultureInfo("th-TH"));

                string discountRemark = "";

                if (item.IsDiscountBaht)
                {
                    discountRemark += "รับรอง " + item.DiscountBaht + " บาท ";
                }
                if (item.IsDiscountPercent)
                {
                    discountRemark += "รับรอง " + item.DiscountPercent + " % ";
                }

                if (!String.IsNullOrEmpty(item.DiscountRemark))
                {
                    discountRemark = discountRemark + " " + item.DiscountRemark;
                }

                discountRemark = discountRemark + "  ห้อง  " + item.Room.RoomNo + "    " + " เวลา " + " " + _checkInTime + " - " + _checkOutTime + " น.";

                TimeSpan hourAndMinute = TimeSpan.FromMinutes(item.TimeMinute);

                string time = string.Format("{0:00}:{1:00}", (int)hourAndMinute.TotalHours, hourAndMinute.Minutes);


                content += $@"
                <tr class='text-center'>
                    <td>{_calendarDay}</td>
                    <td>{discountRemark}</td>
                    <td>{time}</td>
                </tr>
                ";
                sumTime += item.TimeMinute;
                TimeSpan _hourAndMinute = TimeSpan.FromMinutes(sumTime);
                _time = string.Format("{0:00}:{1:00}", (int)_hourAndMinute.TotalHours, _hourAndMinute.Minutes);
            }
            footer += $@"
                <tr class='text-center'>
                    <td>ยอดรวม</td>
                    <td>{_time}</td>
                </tr>
                ";
            string htmlGroup = html.Replace("[content]", content).Replace("[footer]", footer);

            return htmlGroup.ToString();
        }

        public string UnpaidBillOld(DateTime startDate, DateTime endDate, List<ReportUnpaidBillResponse> responses)
        {
            string sDate = startDate.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));
            string eDate = endDate.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));
            var html =
                $@"
                    <!DOCTYPE html>
                        <html>
                            <head></head>
                            <body>
                                <div class='text-center'><h4>รายงานการค้างชำระ จากวันที่ {sDate} จนถึงวันที่ {eDate}</h4></div>
                                <table>
                                  <thead>
                                    <tr>
                                      <th>วันที่</th>
                                      <th>หมายเลขบิล</th>
                                      <th>เชียร์แขก</th>
                                      <th>จำนวนเงินค้าง</th>
                                      <th>ค้างคงเหลือ</th>
                                      <th>หมายเหตุการค้าง</th>
                                      <th>พนักงานทำรายการ</th>
                                      <th>วันที่ชำระเงิน</th>
                                    </tr>
                                  <tbody>
                                    [content]
                                  </tbody>
                                  </thead>
                                </table>
                                <br>
                                <table>
                                  <tfoot>
                                    [footer]
                                  </tfoot>
                                </table>
                            </body>
                        </html>
                ";
            string content = "";
            string footer = "";
            decimal sum = 0;
            foreach (var item in responses)
            {
                var calendarDay = _db.CalendarDays.Where(c => c.Id == item.CalendarDayId).SingleOrDefault();
                string _calendarDay = calendarDay.ToDay.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));
                string payDate = "";
                decimal Total = item.Total;
                decimal PaymentSummary = (item.CashAngelTotal + item.CashFoodTotal + item.CashEtcTotal) + (item.CreditAngelTotal + item.CreditFoodTotal + item.CreditEtcTotal) + (item.MemberAngelTotal + item.MemberFoodTotal + item.MemberEtcTotal) + (item.QrCodeAngelTotal + item.QrCodeFoodTotal + item.QrCodeEtcTotal);
                decimal SummaryUnpaid = Total - PaymentSummary;

                if (SummaryUnpaid == 0)
                {
                    SummaryUnpaid = Total;
                }

                decimal Unpaid = 0;

                var _summaryTotal = String.Format("{0:N}", SummaryUnpaid);
                var _unpaid = String.Format("{0:N}", Unpaid);

                if (item.PayDate != null)
                {
                    Unpaid = SummaryUnpaid;
                    DateTime _payDate = (DateTime)item.PayDate;
                    payDate = _payDate.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));
                }

                string receptionName = "";
                if (item.reception != null)
                {
                    receptionName = item.reception.Nickname;
                }
                //if (!String.IsNullOrEmpty(item.reception.Nickname))
                //{
                //    receptionName = item.reception.Nickname;
                //}
                string userName = "";
                if (item.user != null)
                {
                    userName = item.user.UserFullname;
                }
                //if (!String.IsNullOrEmpty(item.user.UserFullname))
                //{
                //    userName = item.user.UserFullname;
                //}

                content += $@"
                <tr class='text-center'>
                    <td>{_calendarDay}</td>
                    <td>{item.DocumentNumber}</td>
                    <td>{receptionName}</td>
                    <td>{_summaryTotal}</td>
                    <td>{_unpaid}</td>
                    <td></td>
                    <td>{userName}</td>
                    <td>{payDate}</td>
                </tr>
                ";

                sum += SummaryUnpaid;
            }
            var _sum = String.Format("{0:N}", sum);
            footer += $@"
                <tr class='text-center'>
                    <td colspan='3'>ยอดรวม</td>
                    <td colspan='5'>{_sum} บาท</td>
                </tr>
            ";

            string htmlGroup = html.Replace("[content]", content).Replace("[footer]", footer);

            return htmlGroup.ToString();
        }
        public string UnpaidBill(DateTime startDate, DateTime endDate, List<ReportMassageDebtResponse> responses)
        {
            string sDate = startDate.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));
            string eDate = endDate.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));
            var html =
                $@"
                    <!DOCTYPE html>
                        <html>
                            <head></head>
                            <body>
                                <div class='text-center'><h4>รายงานการค้างชำระ จากวันที่ {sDate} จนถึงวันที่ {eDate}</h4></div>
                                <table>
                                  <thead>
                                    <tr>
                                      <th>วันที่ค้างชำระ</th>
                                      <th>หมายเลขบิล</th>
                                      <th>รหัสเมมเบอร์</th>
                                      <th>ยอดคงค้าง</th>
                                      <th>หมายเหตุการค้าง</th>
                                      <th>เงินสด</th>
                                      <th>บัตรเครดิต</th>
                                      <th>คิวอาร์โค้ด</th>
                                      <th>เมมเบอร์</th>
                                      <th>พนักงานทำรายการ</th>
                                      <th>วันที่ชำระ</th>
                                    </tr>
                                  <tbody>
                                    [content]
                                  </tbody>
                                  </thead>
                                </table>
                                <br>
                                <table>
                                  <tfoot>
                                    [footer]
                                  </tfoot>
                                </table>
                            </body>
                        </html>
                ";
            string content = "";
            string footer = "";
            decimal sumTotal = 0;
            decimal sumUnpaid = 0;
            decimal unpaid = 0;
            //decimal total = 0;
            decimal cash = 0;
            decimal credit = 0;
            decimal qrCode = 0;
            decimal member = 0;
            string _payUnpaidDate = "";
            string unpaidRemark = "";
            foreach (var item in responses)
            {
                var calendarDay = _db.CalendarDays.Where(c => c.Id == item.Massage.CalendarDayId).SingleOrDefault();
                string _calendarDay = calendarDay.ToDay.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));
                unpaid = item.DebtTotal;
                //total = item.Massage.Total;
                cash = item.CashTotal;
                credit = item.CreditTotal;
                qrCode = item.QrTotal;
                member = item.MemberTotal;
                var _cash = String.Format("{0:N}", cash);
                var _credit = String.Format("{0:N}", credit);
                var _qrCode = String.Format("{0:N}", qrCode);
                var _member = String.Format("{0:N}", member);

                if (item.Massage.PayUnpaidDate != null)
                {

                    DateTime payUnpaidDate = (DateTime)item.Massage.PayUnpaidDate;
                    _payUnpaidDate = payUnpaidDate.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));
                }
                else
                {
                    _payUnpaidDate = "ยังไม่ได้ชำระ";
                }

                if (item.UnpaidRemark != null)
                {
                    unpaidRemark = item.UnpaidRemark;
                }

                var _unpaid = String.Format("{0:N}", unpaid);
                //var _total = String.Format("{0:N}", total);

                content += $@"
                <tr class='text-center'>
                    <td>{_calendarDay}</td>
                    <td>{item.Massage.DocumentNumber}</td>
                    <td>{item.Member}</td>
                    <td>{_unpaid}</td>
                    <td>{unpaidRemark}</td>
                    <td>{_cash}</td>
                    <td>{_credit}</td>
                    <td>{_qrCode}</td>
                    <td>{_member}</td>
                    <td>{item.Cashier}</td>
                    <td>{_payUnpaidDate}</td>
                </tr>
                ";
                unpaidRemark = "";
                sumUnpaid += unpaid;


            }
            var _sumTotal = String.Format("{0:N}", sumTotal);
            var _sumUnpaid = String.Format("{0:N}", sumUnpaid);
            footer += $@"
               <div class='text-center'><h4>สรุปยอดรวม</h4></div>
               <tr class='text-center'>
               <td colspan='3'>ยอดรวมคงค้าง</td>
               <td colspan='5'>{_sumUnpaid} บาท</td>
               </tr>

            ";

            string htmlGroup = html.Replace("[content]", content).Replace("[footer]", footer);

            return htmlGroup.ToString();
        }

        public string MemberExpire(DateTime startDate, DateTime endDate, List<MemberResponse> responses)
        {
            string sDate = startDate.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));
            string eDate = endDate.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));
            var itemMember = _db.Items.Where(i => i.Slug == "suite").SingleOrDefault();
            var html =
                $@"
                    <!DOCTYPE html>
                        <html>
                            <head></head>
                            <body>
                                <div class='text-center'><h4>รายงานการหมดอายุเมมเบอร์ จากวันที่ {sDate} จนถึงวันที่ {eDate}</h4></div>
                                <table>
                                  <thead>
                                    <tr>
                                      <th>รหัสเมมเบอร์</th>
                                      <th>ขื่อ</th>
                                      <th>เชียร์แขก</th>
                                      <th>วันหมดอายุ</th>
                                      <th>ยอดคงเหลือ</th>
                                      <th>ห้องสูท</th>
                                    </tr>
                                  <tbody>
                                    [content]
                                  </tbody>
                                  </thead>
                                </table>
                                <br>
                                <table>
                                  <tfoot>
                                    [footer]
                                  </tfoot>
                                </table>
                            </body>
                        </html>
                ";
            string content = "";
            string footer = "";
            decimal sumCreditAmount = 0;
            decimal sumSuite = 0;
            foreach (var item in responses)
            {
                DateTime expireDate = (DateTime)item.ExpiredDate;
                string _expireDate = expireDate.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));

                var creditAmount = String.Format("{0:N}", item.CreditAmount);

                decimal suite = 0;

                var MemberItems = item.MemberItems.Where(i => i.ItemId == itemMember.Id && i.MemberId == item.Id).OrderByDescending(i => i.Id).FirstOrDefault();

                if (MemberItems != null)
                {
                    suite = MemberItems.Amount;
                }

                content += $@"
                <tr class='text-center'>
                    <td>{item.Code}</td>
                    <td>{item.Firstname}</td>
                    <td>{item.Reception.Nickname}</td>
                    <td>{_expireDate}</td>
                    <td>{creditAmount}</td>
                    <td>{suite}</td>
                </tr>
                ";

                sumCreditAmount += item.CreditAmount;
                sumSuite += suite;
            }
            var _sumCreditAmount = String.Format("{0:N}", sumCreditAmount);
            footer += $@"
                <tr class='text-center'>
                    <td colspan='5'>ยอดรวม</td>
                    <td>{_sumCreditAmount} บาท</td>
                    <td>{sumSuite}</td>
                </tr>
            ";

            string htmlGroup = html.Replace("[content]", content).Replace("[footer]", footer);

            return htmlGroup.ToString();
        }

        public string DailyFinance(DateTime startDate, DateTime endDate, List<ReportSummaryDayResponse> responses, ReportDailyFinanceResponse response, SettingResponse setting)
        {
            string sDate = startDate.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));
            string eDate = endDate.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));
            string qrCodeorTrCode = "";
            if (setting.IsQrCodeReport)
            {
                qrCodeorTrCode = "TR Code";
            }
            else
            {
                qrCodeorTrCode = "QR Code";
            }
            var html =
                $@"
                    <!DOCTYPE html>
                        <html>
                            <head></head>
                            <body>
                                <div class='text-center'><h4>รายงานสรุปยอดขายประจำวัน(บัญชี) จากวันที่ {sDate} จนถึงวันที่ {eDate}</h4></div>
                                <table>
                                  <thead>
                                    <tr>
                                      <th>ประเภท</th>
                                      <th>ราคา</th>
                                      <th>Suite</th>
                                      <th>คอมเชียร์</th>
                                      <th>รวมเงิน</th>
                                      <th>คูปอง</th>
                                      <th>รับรอง(รอบ)</th>
                                    </tr>
                                  <tbody>
                                    [content]
                                  </tbody>
                                  <tfoot>
                                    [footer]
                                  </tfoot>
                                  </thead>
                                </table>
                                [summary]
                            </body>
                        </html>
                ";
            string content = "";
            string footer = "";
            string summary = "";
            decimal totalFee = 0;
            decimal totalRound = 0;
            decimal totalRoundDiscount = 0;
            foreach (var item in responses)
            {
                var iTotalFee = String.Format("{0:N}", item.Finance.TotalFee);
                var iFee = String.Format("{0:N}", item.Fee);

                content += $@"
                <tr class='text-center'>
                    <td>{item.Code}</td>
                    <td>{iFee}</td>
                    <td>0</td>
                    <td>0.00</td>
                    <td>{iTotalFee}</td>
                    <td>{item.Finance.TotalRound}</td>
                    <td>{item.Finance.TotalRoundDiscount}</td>
                </tr>
                ";

                totalFee += item.Finance.TotalFee;
                totalRound += item.Finance.TotalRound;
                totalRoundDiscount += item.Finance.TotalRoundDiscount;
            }

            var _totalFee = String.Format("{0:N}", totalFee);

            footer += $@"
                <tr class='text-center'>
                    <td colspan='2'>ยอดรวม</td>
                    <td>0</td>
                    <td>0.00</td>
                    <td>{_totalFee}</td>
                    <td>{totalRound}</td>
                    <td>{totalRoundDiscount}</td>
                </tr>
                ";

            var TotalCashAngel = String.Format("{0:N}", response.TotalCashAngel);
            var TotalCreditAngel = String.Format("{0:N}", response.TotalCreditAngel);
            var TotalMemberAngel = String.Format("{0:N}", response.TotalMemberAngel);
            var TotalQrCodeAngel = String.Format("{0:N}", response.TotalQrCodeAngel);
            //var TotalMassageAngel = String.Format("{0:N}", response.TotalMassageAngel);
            //var TotalCashEtc = String.Format("{0:N}", response.TotalCashEtc);
            //var TotalCreditEtc = String.Format("{0:N}", response.TotalCreditEtc);
            //var TotalMemberEtc = String.Format("{0:N}", response.TotalMemberEtc);
            //var TotalEtc = String.Format("{0:N}", response.TotalEtc);
            //var Total = String.Format("{0:N}", response.Total);

            summary += $@"

                <table class='no-border'>
                    <tr>
                        <td class='table-header no-border text-left'>เงินสด (นวด)</td>
                        <td class='table-header no-border text-right'>{TotalCashAngel} บาท</td>
                    </tr>
                    <tr>
                        <td class='table-header no-border text-left'>บัตรเครดิต (นวด)</td>
                        <td class='table-header no-border text-right'>{TotalCreditAngel} บาท</td>
                    </tr>
                    <tr>
                        <td class='table-header no-border text-left'>{qrCodeorTrCode} (นวด)</td>
                        <td class='table-header no-border text-right'>{TotalQrCodeAngel} บาท</td>
                    </tr>
                    <tr>
                        <td class='table-header no-border text-left'>เมมเบอร์ (นวด)</td>
                        <td class='table-header no-border text-right'>{TotalMemberAngel} บาท</td>
                    </tr>
                    <tr>
                        <td class='table-header no-border text-left'>ยอดรวม (นวด)</td>
                        <td class='table-header no-border text-right'>{_totalFee} บาท</td>
                    </tr>
                    <tr>
                        <td class='table-header no-border text-left'>ค้างชำระ (+)</td>
                        <td class='table-header no-border text-right'>______________________________</td>
                    </tr>
                    <tr>
                        <td class='table-header no-border text-left'>รับรอง 100% (+)</td>
                        <td class='table-header no-border text-right'>______________________________</td>
                    </tr>
                    <tr>
                        <td class='table-header no-border text-left'>รับรองส่วนลด (+)</td>
                        <td class='table-header no-border text-right'>______________________________</td>
                    </tr>
                    <tr>
                        <td class='table-header no-border text-left'>ลูกหนี้แคชเชียร์ (+)</td>
                        <td class='table-header no-border text-right'>______________________________</td>
                    </tr>
                    <tr>
                        <td class='table-header no-border text-left'>ลูกหนี้อื่นๆ (+)</td>
                        <td class='table-header no-border text-right'>______________________________</td>
                    </tr>
                    <tr>
                        <td class='table-header no-border text-left'>เงินทอน (-)</td>
                        <td class='table-header no-border text-right'>______________________________</td>
                    </tr>
                </table>
            ";


            string htmlGroup = html.Replace("[content]", content).Replace("[footer]", footer).Replace("[summary]", summary);

            return htmlGroup.ToString();
        }

        public string ReceptionSell(DateTime startDate, DateTime endDate, List<ReportReceptionSellResponse> responses, List<ReportReceptionSellResponse> responseSummary)
        {
            string sDate = startDate.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));
            string eDate = endDate.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));
            var html =
                  $@"
                  <!DOCTYPE html>
                        <html>
                            <head>
                            </head>
                            <body>
                                <div class='text-center'><h4>รายงานยอดเมมเบอร์ระหว่างวันที่ {sDate} จนถึงวันที่ {eDate}</h4></div>
                                <table>
                                  <thead>
                                    <tr>
                                      <th>เบอร์</th>
                                      <th>ชื่อ</th>   
                                      <th>รวม</th>
                                      <th>อันดับ</th>
                                    </tr>
                                  <tbody>
                                    [content]
                                  </tbody>
                                  </thead>
                                </table>
                                <br>
                                <table>
                                  <tfoot>
                                    [footer]
                                  </tfoot>
                                </table>
                            </body>
                        </html>
                    ";
            string content = "";
            string footer = "";
            decimal _summarys = 0;
            foreach (var _item in responses)
            {
                var summarys = String.Format("{0:N}", _item.Summarys.Summary);
                content += $@"
                            <tr class='text-center'>
                                <td>{_item.Code}</td>
                                <td>{_item.Nickname}</td>
                                <td>{summarys}</td>
                                <td>{_item.Summarys.Rank}</td>
                             </tr>
                            ";

                _summarys += _item.Summarys.Summary;
            }
            var _summary = String.Format("{0:N}", _summarys);

            footer += $@"
                        <tr class='text-center'>
                          <td colspan='2'>ยอดรวม</td>
                          <td>{_summary}</td>
                        </tr>
                        ";

            string htmlGroup = html.Replace("[content]", content).Replace("[footer]", footer);

            return htmlGroup.ToString();
        }

        public string TableCashier(DateTime startDate, string username, ReportTableCashierResponse response, SettingResponse setting)
        {
            string sDate = startDate.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));
            var AngelCash = String.Format("{0:N}", response.AngelCash);
            var AngelCredit = String.Format("{0:N}", response.AngelCredit);
            var AngelMember = String.Format("{0:N}", response.AngelMember);
            var AngelQrCode = String.Format("{0:N}", response.AngelQrCode);
            var FoodCash = String.Format("{0:N}", response.FoodCash);
            var FoodCredit = String.Format("{0:N}", response.FoodCredit);
            var FoodMember = String.Format("{0:N}", response.FoodMember);
            var FoodQrCode = String.Format("{0:N}", response.FoodQrCode);
            var CutFoodMember = String.Format("{0:N}", response.CutFoodMember);
            var RoomCash = String.Format("{0:N}", response.RoomCash);
            var RoomCredit = String.Format("{0:N}", response.RoomCredit);
            var RoomMember = String.Format("{0:N}", response.RoomMember);
            var RoomQrCode = String.Format("{0:N}", response.RoomQrCode);
            var OtherServiceChargeCash = String.Format("{0:N}", response.OtherServiceChargeCash);
            var OtherServiceChargeCredit = String.Format("{0:N}", response.OtherServiceChargeCredit);
            var OtherServiceChargeMember = String.Format("{0:N}", response.OtherServiceChargeMember);
            var OtherServiceChargeQrCode = String.Format("{0:N}", response.OtherServiceChargeQrCode);
            var SumCash = String.Format("{0:N}", response.SumCash);
            var SumCredit = String.Format("{0:N}", response.SumCredit);
            var SumMember = String.Format("{0:N}", response.SumMember);
            var SumQrCode = String.Format("{0:N}", response.SumQrCode);
            var SumUnPaid = String.Format("{0:N}", response.SumUnpaid);

            string qrCodeorTrCode = "";
            if (setting.IsQrCodeReport)
            {
                qrCodeorTrCode = "TR Code";
            }
            else
            {
                qrCodeorTrCode = "QR Code";
            }
            var html =
                $@"
                    <!DOCTYPE html>
                        <html>
                            <head>
                            </head>
                            <body>
                                <div class='text-center'><h4>รายงานลิ้นชักแคชเชียร์ {sDate} โดย {username} </h4></div>
                                  <table class='no-border'>
                                    <tr>
                                        <td class='table-header no-border text-left'>เงินสด (นวด)</td>
                                        <td class='table-header no-border text-right'>{AngelCash} บาท</td>
                                    </tr>
                                    <tr>
                                        <td class='table-header no-border text-left'>บัตรเครดิต (นวด)</td>
                                        <td class='table-header no-border text-right'>{AngelCredit} บาท</td>
                                    </tr>
                                    <tr>
                                        <td class='table-header no-border text-left'>{qrCodeorTrCode} (นวด)</td>
                                        <td class='table-header no-border text-right'>{AngelQrCode} บาท</td>
                                    </tr>
                                    <tr>
                                        <td class='table-header no-border text-left'>เมมเบอร์ (นวด)</td>
                                        <td class='table-header no-border text-right'>{AngelMember} บาท</td>
                                    </tr>
                                    <tr>
                                        <td class='table-header no-border text-left'>เงินสด (อาหาร)</td>
                                        <td class='table-header no-border text-right'>{FoodCash} บาท</td>
                                    </tr>
                                    <tr>
                                        <td class='table-header no-border text-left'>บัตรเครดิต (อาหาร)</td>
                                        <td class='table-header no-border text-right'>{FoodCredit} บาท</td>
                                    </tr>
                                    <tr>
                                        <td class='table-header no-border text-left'>{qrCodeorTrCode} (อาหาร)</td>
                                        <td class='table-header no-border text-right'>{FoodQrCode} บาท</td>
                                    </tr>
                                    <tr>
                                        <td class='table-header no-border text-left'>เมมเบอร์ (อาหาร)</td>
                                        <td class='table-header no-border text-right'>{FoodMember} บาท</td>
                                    </tr>
                                    <tr>
                                        <td class='table-header no-border text-left'>เมมเบอร์ (ตัดอาหาร)</td>
                                        <td class='table-header no-border text-right'>{CutFoodMember} บาท</td>
                                    </tr>
                                    <tr>
                                        <td class='table-header no-border text-left'>เงินสด (ห้อง)</td>
                                        <td class='table-header no-border text-right'>{RoomCash} บาท</td>
                                    </tr>
                                    <tr>
                                        <td class='table-header no-border text-left'>บัตรเครดิต (ห้อง)</td>
                                        <td class='table-header no-border text-right'>{RoomCredit} บาท</td>
                                    </tr>
                                    <tr>
                                        <td class='table-header no-border text-left'>{qrCodeorTrCode} (ห้อง)</td>
                                        <td class='table-header no-border text-right'>{RoomQrCode} บาท</td>
                                    </tr>
                                    <tr>
                                        <td class='table-header no-border text-left'>เมมเบอร์ (ห้อง)</td>
                                        <td class='table-header no-border text-right'>{RoomMember} บาท</td>
                                    </tr>
                                    <tr>
                                        <td class='table-header no-border text-left'>เงินสด (อื่นๆ)</td>
                                        <td class='table-header no-border text-right'>{OtherServiceChargeCash} บาท</td>
                                    </tr>
                                    <tr>
                                        <td class='table-header no-border text-left'>บัตรเครดิต (อื่นๆ)</td>
                                        <td class='table-header no-border text-right'>{OtherServiceChargeCredit} บาท</td>
                                    </tr>
                                    <tr>
                                        <td class='table-header no-border text-left'>{qrCodeorTrCode} (อื่นๆ)</td>
                                        <td class='table-header no-border text-right'>{OtherServiceChargeQrCode} บาท</td>
                                    </tr>
                                    <tr>
                                        <td class='table-header no-border text-left'>เมมเบอร์ (อื่นๆ)</td>
                                        <td class='table-header no-border text-right'>{OtherServiceChargeMember} บาท</td>
                                    </tr>
                                     <tr>
                                        <td class='table-header no-border text-left'>ยอดรวม เงินสด</td>
                                        <td class='table-header no-border text-right'>{SumCash} บาท</td>
                                    </tr>
                                    <tr>
                                        <td class='table-header no-border text-left'>ยอดรวม บัตรเครดิต</td>
                                        <td class='table-header no-border text-right'>{SumCredit} บาท</td>
                                    </tr>
                                    <tr>
                                        <td class='table-header no-border text-left'>ยอดรวม {qrCodeorTrCode}</td>
                                        <td class='table-header no-border text-right'>{SumQrCode} บาท</td>
                                    </tr>
                                    <tr>
                                        <td class='table-header no-border text-left'>ยอดรวม เมมเบอร์</td>
                                        <td class='table-header no-border text-right'>{SumMember} บาท</td>
                                    </tr>
                                    <tr>
                                        <td class='table-header no-border text-left'>ยอดรวม ค้างจ่าย</td>
                                        <td class='table-header no-border text-right'>{SumUnPaid} บาท</td>
                                    </tr>
                                </table>
                            </body>
                        </html>
                ";

            return html.ToString();
        }

        public string ReceptionRound(DateTime startDate, DateTime endDate, List<ReportReceptionRoundTotalResponse> responses, List<ReportReceptionRoundTotalResponse> responseRoundTotal)
        {
            string sDate = startDate.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));
            string eDate = endDate.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));
            var html =
                  $@"
                  <!DOCTYPE html>
                        <html>
                            <head>
                            </head>
                            <body>
                                <div class='text-center'><h4>สรุปยอดเชียร์ {sDate} จนถึงวันที่ {eDate}</h4></div>
                                <table>
                                  <thead>
                                    <tr>
                                      <th>เบอร์</th>
                                      <th>ชื่อ</th>   
                                      <th>จำนวน</th>
                                      <th>อันดับ</th>
                                    </tr>
                                  <tbody>
                                    [content]
                                  </tbody>
                                  </thead>
                                </table>
                                <br>
                                <table>
                                  <tfoot>
                                    [footer]
                                  </tfoot>
                                </table>
                            </body>
                        </html>
                    ";
            string content = "";
            string footer = "";
            decimal _roundTotals = 0;
            int i = 1;
            decimal oldRound = 0;
            bool isCheck = true;
            foreach (var item in responses)
            {

                var roundTotal = String.Format("{0:N}", item.ReceptionRound.RoundTotal);

                if (isCheck)
                {
                    oldRound = item.ReceptionRound.RoundTotal;
                    isCheck = false;
                }
                if (oldRound != item.ReceptionRound.RoundTotal)
                {
                    i++;
                }
                content += $@"
                            <tr class='text-center'>
                                <td>{item.Code}</td>
                                <td>{item.Nickname}</td>
                                <td>{roundTotal}</td>
                                <td>{item.ReceptionRound.Rank}</td>
                            </tr>
                            ";

                _roundTotals += item.ReceptionRound.RoundTotal;

                oldRound = item.ReceptionRound.RoundTotal;
            }
            var _roundTotal = String.Format("{0:N}", _roundTotals);

            footer += $@"
                        <tr class='text-center'>
                          <td>รวมจำนวนรอบ</td>
                          <td>{_roundTotal}</td>
                        </tr>
                        ";

            string htmlGroup = html.Replace("[content]", content).Replace("[footer]", footer);

            return htmlGroup.ToString();
        }


        public string Maid(DateTime startDate, List<ReportPaymentAngelPerformanceResponse> response)
        {
            string sDate = startDate.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));

            var html =
                $@"
                    <!DOCTYPE html>
                        <html>
                            <head>
                            </head>
                            <body>
                                <div class='text-center'><h4>รอบการทำงานพนักงาน {sDate} </h4></div>
                                  <table class='no-border'>
                                    [content] 
                                    [footer]
                                </table>
                            </body>
                        </html>
                ";
            //
            string content = "";
            string footer = "";
            decimal total = 0;
            foreach (var item in response)
            {
                content += $@"
                              <tr>
                                <td class='table-header no-border text-left'>{item.Code}</td>
                                <td class='table-header no-border text-right'>{item.TotalRound} รอบ</td>
                              </tr>
                            ";

                total += item.TotalRound;


            }
            var _total = String.Format("{0:N}", total);

            footer += $@"
                              <tr>
                                <td class='table-header no-border text-left'></td>
                                <td class='table-header no-border text-right'>รวม {_total} รอบ</td>
                              </tr>
                            ";

            string htmlGroup = html.Replace("[content]", content).Replace("[footer]", footer);

            return htmlGroup.ToString();
        }

        public string GetMemberFood(DateTime startDate, DateTime endDate, List<ReportMemberFoodResponse> response, SettingResponse setting)
        {
            string sDate = startDate.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));
            string eDate = endDate.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));
            string qrCodeorTrCode = "";
            if (setting.IsQrCodeReport)
            {
                qrCodeorTrCode = "TR Code";
            }
            else
            {
                qrCodeorTrCode = "QR Code";
            }
            var html = $@"
                        <!DOCTYPE html>
                        <html>
                            <head>
                            </head>
                            <body>
                                <div class='text-center'><h4>สรุปยอดเหล้าและอาหารเมมเบอร์ {sDate} จนถึงวันที่ {eDate}</h4></div>
                                <table>
                                  <thead>
                                    <tr>
                                      <th>เบอร์</th>
                                      <th>ชื่อ</th>   
                                      <th>ค่าอาหาร</th>
                                       <th>เมมเบอร์</th>
                                       <th>เงินสด</th>
                                       <th>บัตรเครดิต</th>
                                       <th>{qrCodeorTrCode}</th>
                                       <th>ค้างจ่าย</th>
                                      <th>Black</th>
                                      <th>Gold</th>
                                      <th>สถานะ</th>
                                      <th>ผู้ทำรายการ</th>
                                    </tr>
                                  <tbody>
                                    [content]
                                  </tbody>
                                  </thead>
                                </table>
                                <br>
                                <table>
                                  <tfoot>
                                    [footer]
                                  </tfoot>
                                </table>
                            </body>
                        </html>
                        ";

            string content = "";
            string footer = "";
            decimal totalAmount = 0;
            decimal totalAmount_1 = 0;
            decimal totalAmount_2 = 0;
            decimal _total = 0;
            decimal _cash = 0;
            decimal _credit = 0;
            decimal _qrCdoe = 0;
            decimal _unPaidTotal = 0;
            foreach (var item in response)
            {
                var cerditAmount = String.Format("{0:N}", item.CerditAmount);
                var total = String.Format("{0:N}", item.Total);
                var cash = String.Format("{0:N}", item.CashTotal);
                var credit = String.Format("{0:N}", item.CreditTotal);
                var qrCode = String.Format("{0:N}", item.QrCodeTotal);
                var unPaidTotal = String.Format("{0:N}", item.UnpaidTotal);
                string status = "";
                if (item.IsPay && !item.IsUnPaid)
                {
                    status = "ชำระแล้ว";
                }
                else if (item.IsPay && item.IsUnPaid)
                {
                    status = "ค้างชำระ";
                }
                else
                {
                    status = "รอการชำระ";
                }
                content += $@"
                            <tr class='text-center'>
                                <td>{item.Member.Code}</td>
                                <td>{item.Member.Firstname}</td>
                                <td>{total}</td>
                                <td>{cerditAmount}</td>
                                <td>{cash}</td>
                                <td>{credit}</td>
                                <td>{qrCode}</td>
                                <td>{unPaidTotal}</td>
                                <td>{item.Amount_1}</td>
                                <td>{item.Amount_2}</td>
                                <td>{status}</td>
                                <td>{item.User.UserFullname}</td>
                            </tr>
                            ";

                _total += item.Total;
                _cash += item.CashTotal;
                _credit += item.CreditTotal;
                _qrCdoe += item.QrCodeTotal;
                _unPaidTotal += item.UnpaidTotal;
                totalAmount += item.CerditAmount;
                totalAmount_1 += item.Amount_1;
                totalAmount_2 += item.Amount_2;
            }

            var sumTotal = String.Format("{0:N}", _total);
            var sumCash = String.Format("{0:N}", _cash);
            var sumCredit = String.Format("{0:N}", _credit);
            var sumQrCode = String.Format("{0:N}", _qrCdoe);
            var sumUnpaidTotal = String.Format("{0:N}", _unPaidTotal);
            var sumCerditAmount = String.Format("{0:N}", totalAmount);
            var sumCerditAmount_1 = String.Format("{0:N}", totalAmount_1);
            var sumCerditAmount_2 = String.Format("{0:N}", totalAmount_2);

            footer += $@"
                        <tr class='text-center'>
                          <td colspan='2'>ยอดรวม</td>
                          <td>{sumTotal}</td>
                          <td>{sumCerditAmount}</td>
                          <td>{sumCash}</td>
                          <td>{sumCredit}</td>
                          <td>{sumQrCode}</td>
                          <td>{sumUnpaidTotal}</td>
                          <td>{sumCerditAmount_1}</td>
                          <td>{sumCerditAmount_2}</td>
                        </tr>

                        ";

            string htmlGroup = html.Replace("[content]", content).Replace("[footer]", footer);

            return htmlGroup.ToString();
        }

        public string SellSuite(DateTime startDate, DateTime endDate, List<ReportRoomSuiteResponse> responses, SettingResponse setting)
        {
            string sDate = startDate.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));
            string eDate = endDate.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));
            string qrCodeorTrCode = "";
            if (setting.IsQrCodeReport)
            {
                qrCodeorTrCode = "TR Code";
            }
            else
            {
                qrCodeorTrCode = "QR Code";
            }
            var html = $@"
                        <!DOCTYPE html>
                        <html>
                            <head>
                            </head>
                            <body>
                                <div class='text-center'><h4>สรุปยอดขายห้องสูท {sDate} จนถึงวันที่ {eDate}</h4></div>
                                [content]
                            </body>
                        </html>
                        ";

            string content = "";
            string contents = "";
            decimal cashRoomSum = 0;
            decimal creditRoomSum = 0;
            decimal memberRoomSum = 0;
            decimal summaryRoom = 0;
            decimal useSuite = 0;
            decimal qrCodeRoomSum = 0;

            foreach (var item in responses)
            {
                if (item.reportMassageSellResponses.Count > 0)
                {
                    content += $@"
                            <h3>ห้อง{item.RoomNo}</h3>
                            <table>
                              [contents]
                            </table>
                            ";

                    contents += $@"
                                <thead>
                                    <tr>
                                      <th>วันที่</th>
                                      <th>เลขที่บิล</th>   
                                      <th>เชียร์</th>
                                      <th>ใช้สิทธ์</th>
                                      <th>เงินสด</th>
                                      <th>บัตรเครดิต</th>
                                      <th>{qrCodeorTrCode}</th>
                                      <th>หักสมาชิก</th>
                                      <th>รวม</th>
                                    </tr>
                                  </thead>
                                ";

                    contents += $@"
                                <tbody>
                                ";

                    foreach (var massage in item.reportMassageSellResponses)
                    {
                        if (massage.Massage != null)
                        {
                            DateTime oldCheckInTime = (DateTime)massage.OldCheckInTime;
                            string _oldCheckInTime = oldCheckInTime.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));
                            decimal sumRoomTotal = massage.Massage.CashRoomTotal + massage.Massage.MemberRoomTotal + massage.Massage.CreditRoomTotal + massage.Massage.QrCodeRoomTotal;
                            var _sumRoomTotal = String.Format("{0:N}", sumRoomTotal);
                            var cashRoomTotal = String.Format("{0:N}", massage.Massage.CashRoomTotal);
                            var creditRoomTotal = String.Format("{0:N}", massage.Massage.CreditRoomTotal);
                            var memberRoomTotal = String.Format("{0:N}", massage.Massage.MemberRoomTotal);
                            var qrCodeRoomTotal = String.Format("{0:N}", massage.Massage.QrCodeRoomTotal);
                            decimal _useSuite = 0;
                            //if (massage.MassageMemberPayment != null)
                            //{
                            //    _useSuite = massage.MassageMemberPayment.UseSuite;
                            //}

                            contents += $@"
                                <tr class='text-center'>
                                    <td>{_oldCheckInTime}</td>
                                    <td>{massage.Massage.DocumentNumber}</td>
                                    <td>{massage.Reception.Nickname}</td>
                                    <td>0.00</td>
                                    <td>{cashRoomTotal}</td>
                                    <td>{creditRoomTotal}</td>
                                    <td>{qrCodeRoomTotal}</td>
                                    <td>{memberRoomTotal}</td>
                                    <td>{_sumRoomTotal}</td>
                                </tr>
                            
                            ";

                            cashRoomSum += massage.Massage.CashRoomTotal;
                            creditRoomSum += massage.Massage.CreditRoomTotal;
                            memberRoomSum += massage.Massage.MemberRoomTotal;
                            summaryRoom += sumRoomTotal;
                            useSuite += _useSuite;
                            qrCodeRoomSum += massage.Massage.QrCodeRoomTotal;
                        }
                    }
                    var cashRoomSum_ = String.Format("{0:N}", cashRoomSum);
                    var creditRoomSum_ = String.Format("{0:N}", creditRoomSum);
                    var memberRoomSum_ = String.Format("{0:N}", memberRoomSum);
                    var summaryRoom_ = String.Format("{0:N}", summaryRoom);
                    var qrCodeRoomSum_ = String.Format("{0:N}", qrCodeRoomSum);

                    contents += $@"
                                <tr class='text-center'>
                                  <td colspan='3'>ยอดรวม</td>
                                  <td>{useSuite}</td>
                                  <td>{cashRoomSum_}</td>
                                  <td>{creditRoomSum_}</td>
                                  <td>{qrCodeRoomSum_}</td>
                                  <td>{memberRoomSum_}</td>
                                  <td>{summaryRoom_}</td>
                                </tr>
                                ";

                    contents += $@"
                                </tbody>
                                ";

                    cashRoomSum = 0;
                    creditRoomSum = 0;
                    memberRoomSum = 0;
                    summaryRoom = 0;
                    useSuite = 0;
                    qrCodeRoomSum = 0;
                }


                content = content.Replace("[contents]", contents);
                contents = "";
            }

            string htmlGroup = html.Replace("[content]", content);

            return htmlGroup.ToString();
        }

        public string GetMemberTopup(DateTime startDate, DateTime endDate, List<ReportMemberTopupResponse> responses)
        {
            string sDate = startDate.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));
            string eDate = endDate.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));
            var html = $@"
                        <!DOCTYPE html>
                        <html>
                            <head>
                            </head>
                            <body>
                                <div class='text-center'><h4>สรุปยอดออนท็อปเมมเบอร์ {sDate} จนถึงวันที่ {eDate}</h4></div>
                                <table>
                                  <thead>
                                    <tr>
                                      <th>เบอร์</th>
                                      <th>ชื่อ</th>   
                                      <th>ยอดเพิ่ม</th>
                                      <th>หักBlack</th>
                                      <th>หักGold</th>
                                      <th>หักสูท</th>
                                    </tr>
                                  <tbody>
                                    [content]
                                  </tbody>
                                  </thead>
                                </table>
                                <br>
                                <table>
                                  <tfoot>
                                    [footer]
                                  </tfoot>
                                </table>
                            </body>
                        </html>
                        ";

            string content = "";
            string footer = "";
            decimal totalAmount = 0;
            decimal totalAmount_1 = 0;
            decimal totalAmount_2 = 0;
            decimal totalAmount_3 = 0;

            foreach (var item in responses)
            {
                var cerditAmount = String.Format("{0:N}", item.CerditAmount);

                content += $@"
                            <tr class='text-center'>
                                <td>{item.Member.Code}</td>
                                <td>{item.Member.Firstname}</td>
                                <td>{cerditAmount}</td>
                                <td>{item.Amount_1}</td>
                                <td>{item.Amount_2}</td>
                                <td>{item.Amount_3}</td>
                            </tr>
                            ";
                totalAmount += item.CerditAmount;
                totalAmount_1 += item.Amount_1;
                totalAmount_2 += item.Amount_2;
                totalAmount_3 += item.Amount_3;
            }

            var sumCerditAmount = String.Format("{0:N}", totalAmount);
            var sumCerditAmount_1 = String.Format("{0:N}", totalAmount_1);
            var sumCerditAmount_2 = String.Format("{0:N}", totalAmount_2);
            var sumCerditAmount_3 = String.Format("{0:N}", totalAmount_3);

            footer += $@"
                        <tr class='text-center'>
                          <td colspan='2'>ยอดรวม</td>
                          <td>{sumCerditAmount}</td>
                          <td>{sumCerditAmount_1}</td>
                          <td>{sumCerditAmount_2}</td>
                          <td>{sumCerditAmount_3}</td>
                        </tr>
                        ";

            string htmlGroup = html.Replace("[content]", content).Replace("[footer]", footer);

            return htmlGroup.ToString();
        }

        public string GetReportAngelDebt(List<ReportAngelDebtByAngelTypeResponse> angelTypes, List<DebtTypeResponse> debtTypes)
        {
            var html = $@"
                        <!DOCTYPE html>
                        <html>
                            <head>
                            </head>
                            <body>
                                <div class='text-center'><h4>สรุปหนี้พนักงาน</div>
                                [content]
                            </body>
                        </html>
                        ";

            string content = "";
            string contents = "";
            decimal fee = 0;
            decimal sumFee = 0;

            foreach (var angelType in angelTypes)
            {
                content += $@"
                            <h3>ประเภท{angelType.Code}</h3>
                            <table>
                                <thead>
                                    <tr>
                                      <th>ชื่อ</th>
                                      [contents]
                                    </tr>
                                  </thead>
                            </table>
                            ";

                foreach (var debtType in debtTypes)
                {
                    contents += $@"
                                 <th>{debtType.Name}</th>
                                ";
                }


                contents += $@"
                                <th>รวม</th>
                               <tbody>
                              ";

                foreach (var angel in angelType.Angels)
                {
                    contents += $@"
                            <tr class='text-center'>
                                <td>{angel.Code} : {angel.Nickname}</td>
                            ";

                    foreach (var angelDebt in angel.AngelDebts)
                    {
                        sumFee += angelDebt.Fee;
                        fee += angelDebt.Fee;
                        var angelDebtFee = String.Format("{0:N}", angelDebt.Fee);

                        contents += $@"
                                      <td>{angelDebtFee}</td>
                                    ";
                    }
                    var totalFee = String.Format("{0:N}", fee);
                    fee = 0;

                    contents += $@"
                                <td>{totalFee}</td>
                               </tr>
                              ";
                }
                var sumFeeTotal = String.Format("{0:N}", sumFee);

                contents += $@"
                                <tr class='text-center'>
                                  <td colspan='6'>ยอดรวม</td>
                                  <td>{sumFeeTotal}</td>
                                </tr>
                            ";

                contents += $@"
                               </tbody>
                              ";

                content = content.Replace("[contents]", contents);
                contents = "";
            }

            string htmlGroup = html.Replace("[content]", content);
            return htmlGroup.ToString();
        }

        public string GetMemberCreditAmount(List<ReportMemberCreditAmountResponse> responses)
        {
            var html = $@"
                        <!DOCTYPE html>
                        <html>
                            <head>
                            </head>
                            <body>
                                <div class='text-center'><h4>รายงานยอดคงเหลือเมมเบอร์ </h4></div>
                                <table>
                                  <thead>
                                    <tr>
                                      <th>เบอร์</th>
                                      <th>ชื่อ</th>   
                                      <th>ยอดคงเหลือ</th>
                                      <th>วันหมดอายุ</th>
                                      <th>เชียร์แขก</th>
                                    </tr>
                                  <tbody>
                                    [content]
                                  </tbody>
                                  </thead>
                                </table>
                                <br>
                                <table>
                                  <tfoot>
                                    [footer]
                                  </tfoot>
                                </table>
                            </body>
                        </html>
                        ";

            string content = "";
            string footer = "";
            decimal totalAmount = 0;
            foreach (var item in responses)
            {
                var creditAmount = String.Format("{0:N}", item.CreditAmount);

                content += $@"
                            <tr class='text-center'>
                                <td>{item.Code}</td>
                                <td>{item.Firstname}</td>
                                <td>{creditAmount}</td>
                                <td>{item.ExpiredDate}</td>
                                <td>{item.Reception.Firstname} {item.Reception.Lastname} ({item.Reception.Nickname})</td>
                            </tr>
                            ";

                totalAmount += item.CreditAmount;
            }
            var sumCerditAmount = String.Format("{0:N}", totalAmount);

            footer += $@"
                        <tr class='text-center'>
                          <td colspan='2'>ยอดรวม</td>
                          <td>{sumCerditAmount}</td>
                        </tr>
                        ";

            string htmlGroup = html.Replace("[content]", content).Replace("[footer]", footer);

            return htmlGroup.ToString();
        }

        public string GetAllMemberExpire(List<MemberResponse> responses)
        {
            var html =
                $@"
                    <!DOCTYPE html>
                        <html>
                            <head></head>
                            <body>
                                <div class='text-center'><h4>รายงานเมมเบอร์ที่หมดอายุทั้งหมด</h4></div>
                                <table>
                                  <thead>
                                    <tr>
                                      <th>ลำดับ</th>
                                      <th>เมมเบอร์</th>
                                      <th>ชื่อ</th>
                                      <th>เชียร์แขก</th>
                                      <th>เบอร์โทรศัพท์</th>
                                      <th>ยอดคงเหลือ</th>
                                      <th>วันหมดอายุ</th>
                                    </tr>
                                  <tbody>
                                    [content]
                                  </tbody>
                                  </thead>
                                </table>
                                <br>
                                <table>
                                  <tfoot>
                                    [footer]
                                  </tfoot>
                                </table>
                            </body>
                        </html>
                ";
            string content = "";
            string footer = "";
            int sumAllPeople = 0;
            Decimal sumBalace = 0;
            int i = 0;
            foreach (var item in responses)
            {
                i++;
                DateTime expireDate = (DateTime)item.ExpiredDate;
                string _expireDate = expireDate.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));
                //var creditAmount = String.Format("{0:N}", item.CreditAmount);

                if (item.Firstname != null)
                {
                    sumAllPeople++;
                }

                content += $@"
                <tr class='text-center'>
                    <td>{i}</td>
                    <td>{item.Code}</td>
                    <td>{item.Firstname + " " + item.Lastname}</td>
                    <td>{item.Reception.Nickname}</td>
                    <td>{item.Tel}</td>
                    <td>{item.CreditAmount}</td>
                    <td>{_expireDate}</td>
                </tr>
                
                ";

                sumBalace += item.CreditAmount;
            }
            var _sumBalance = String.Format("{0:N}", sumBalace);
            footer += $@"
                <tr class='text-center'>
                    <td colspan='5'>ยอดรวมคงเหลือ</td>
                    <td>{_sumBalance} บาท</td>
                </tr>
                 <tr class='text-center'>
                    <td colspan='5'>ยอดรวมจำนวนสมาชิกที่หมดอายุ</td>
                    <td>{sumAllPeople} คน</td>
                </tr>
            ";

            string htmlGroup = html.Replace("[content]", content).Replace("[footer]", footer);

            return htmlGroup.ToString();
        }
        public string GetSalaryCountDay(DateTime startDate, DateTime endDate, AngelTypeResponse response)
        {
            string sDate = startDate.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));
            string eDate = endDate.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));
            var html = $@"
                        <!DOCTYPE html>
                        <html>
                            <head>
                            </head>
                            <body>
                                <div class='text-center'><h4>รายงานตรวจสอบ-เวลาทำงานประจำวันที่ {sDate} จนถึงวันที่ {eDate}</h4></div>
                                [content]
                            </body>
                        </html>
                        ";
            string content = "";
            string contents = "";

            content += $@"
                        <h3>ประเภทพนักงานบริการ : {response.Code}</h3>
                        ";


            foreach (var item in response.Angel)
            {
                content += $@"
                        <table>
                              <thead>
                                <h5>{item.Code} {item.Nickname}</h5>
                                <tr class='text-center'>
                                    <td><b>วันที่</b></td>
                                    <td><b>รอบ</b></td>
                                    <td><b>เข้า</b></td>
                                    <td><b>ออก</b></td>
                                    <td><b>นับวัน</b></td>
                                </tr>
                               </thead>
                               [contents]
                               </tr>

                            <br>
                        </table>
                        ";

                contents += $@"
                               <tbody>
                              ";
                int sumWorkingDates = 0;
                decimal sumRounds = 0;
                foreach (var calendarDay in item.AngelSalaryCountDay.CalendarDays)
                {
                    string data = "";
                    string getInTime = "";
                    string getOutTime = "";
                    string _startDate = "";
                    int Days = 0;
                    if (calendarDay.GetInTime != null)
                    {
                        DateTime _getInTime = (DateTime)calendarDay.GetInTime;
                        getInTime = _getInTime.ToString("HH:mm:ss", new CultureInfo("th-TH"));
                        _startDate = _getInTime.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));

                    }
                    if (calendarDay.GetOutTime != null)
                    {
                        DateTime _getOutTime = (DateTime)calendarDay.GetOutTime;
                        getOutTime = _getOutTime.ToString("HH:mm:ss", new CultureInfo("th-TH"));
                    }

                    if (calendarDay.IsDay)
                    {
                        Days = 1;
                        data = "✓";
                    }
                    else
                    {
                        data = "✕";
                    }
                    if (calendarDay.GetInTime != null)
                    {
                        contents += $@"
                                <tr class='text-center'>
                                    <td>{_startDate}</td>
                                    <td>{calendarDay.CountRound}</td>
                                    <td>{getInTime}</td>
                                    <td>{getOutTime}</td>
                                    <td>{data}</td>
                                     ";

                        sumRounds += calendarDay.CountRound;
                        sumWorkingDates += Days;
                    }
                }
                contents += $@"
                                  <tr class='text-center'>
                                  <td colspan='1'>ยอดรวม</td>
                                  <td>{sumRounds}</td>
                                  <td colspan='2'> </td>
                                  <td>{sumWorkingDates}</td>
                                </tr>
                             ";
                contents += $@"
                                </tbody>
                              ";

                content = content.Replace("[contents]", contents);
                contents = "";

            }

            string htmlGroup = html.Replace("[content]", content);

            return htmlGroup.ToString();
        }
        public string GetIncomeDaily(DateTime startDate, DateTime endDate, List<MassageReportResponse> response, MassageReportResponse sumResponse, UserResponse user, SettingResponse setting)
        {
            string sDate = startDate.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));
            string eDate = endDate.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));
            string qrCodeorTrCode = "";
            if (setting.IsQrCodeReport)
            {
                qrCodeorTrCode = "TR Code";
            }
            else
            {
                qrCodeorTrCode = "QR Code";
            }
            var html = $@"
                        <!DOCTYPE html>
                         <html>
                            <head>
                            </head>
                            <body>
                                <div class='text-center'><h4>รายงานสรุปรายได้ประจำวันที่ {sDate} จนถึงวันที่ {eDate}</h4></div>
                                [content]
                                    <br>
                              <table>
                                <thead>
                                  <tr class='text-center'>
                                    <th style='width:5%;font-size:70%'>วันที่</th>
                                    <th style='width:8%;font-size:70%'>เลขที่บิล</th>
                                    <th style='width:4%;font-size:70%;'>เชียร์</th>
                                    <th style='width:6%;font-size:70%'>ห้อง</th>
                                    <th style='width:5%;font-size:70%;'>พนักงาน</th>
                                    <th style='width:5%;font-size:70%;'>เวลาเข้า</th>
                                    <th style='width:5%;font-size:70%;'>เวลาออก</th>
                                    <th style='width:5%;font-size:70%;'>เวลารวม</th>
                                    <th style='width:3%;font-size:70%'>รอบ</th>
                                    <th style='width:6%;font-size:70%;'>จำนวนเงิน</th>
									<th style='width:5%;font-size:70%'>เงินสด</th>
									<th style='width:5%;font-size:70%'>{qrCodeorTrCode}</th>
                                    <th style='width:8%;font-size:70%;'>Credit</th>
                                    <th style='width:8%;font-size:70%;'>Member</th>
                                    <th style='width:4%;font-size:70%;'>Suite</th>
                                    <th style='width:4%;font-size:70%;'>Vip</th>
                                    <th style='width:8%;font-size:70%;'>หมายเหตุ</th>
                                </tr>
                                 <tbody>
                                    [contents]
                                 </tbody>
                               </thead>
                             </table>
                                <table>
                            <tfoorer>
                                 <thead>
                                  <tr class='text-center'>
                                    <th style='font-size:70%;'>จำนวนเงิน</th>
									<th style='width:8%;font-size:70%'>เงินสด</th>
									<th style='width:8%;font-size:70%'>{qrCodeorTrCode}</th>
                                    <th style='width:8%;font-size:70%;'>Credit</th>
                                    <th style='font-size:70%;'>Member</th>
                                    <th style='font-size:70%;'>Suite</th>
                                    <th style='font-size:70%;'>Vip</th>
                                </tr>
                           <div class='text-center'><h4>สรุปยอดรวม</h4></div>
                            [footer]
                            </tfoorer>
                            <thead>
                            </table>
                            </body>
                        </html>
                        ";
            string content = "";
            string contents = "";
            string footer = "";


            content += $@"<h3>พนักงานแคชเชียร์ : {user.UserFullname}";

            string angelTotal = "";
            string SumCashTotal = "";
            string SumQrCode = "";
            string SumCredit = "";
            string SumMember = "";
            string SumUseSuite = "";
            string SumUseVip = "";
            foreach (var report in response)
            {
                DateTime billDate = (DateTime)report.BillDate;
                string _billDate = billDate.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));
                DateTime checkInTime = (DateTime)report.CheckInTime;
                string _checkInTime = checkInTime.ToString("HH:mm", new CultureInfo("th-TH"));
                DateTime checkOutTime = (DateTime)report.CheckOutTime;
                string _checkOutTime = checkOutTime.ToString("HH:mm", new CultureInfo("th-TH"));
                TimeSpan hourAndMinute = TimeSpan.FromMinutes(report.TimeMinute);
                string sumTime = string.Format("{0:00}:{1:00}", (int)hourAndMinute.TotalHours, hourAndMinute.Minutes);
                string angelCode = "";
                decimal round = 0;
                string total = "";
                string cash = "";
                string qrCode = "";
                string credit = "";
                string member = "";
                //decimal useSuite = 0;
                //decimal useVip = 0;
                string useSuite = "";
                string useVip = "";
                string memberCode = "";


                if (report.IsMassageRoom)
                {
                    //useSuite = report.UseSuite;
                    //useVip = report.UseVip;
                    useSuite = String.Format("{0:N}", report.UseSuite);
                    useVip = String.Format("{0:N}", report.UseVip);

                }
                else
                {
                    angelCode = report.AngelCode;
                    round = report.Round;
                    total = String.Format("{0:N}", report.Wage);
                    cash = String.Format("{0:N}", report.CashTotal);
                    qrCode = String.Format("{0:N}", report.QrCodeTotal);
                    credit = String.Format("{0:N}", report.CreditTotal);
                    member = report.MemberCode;


                }


                contents += $@"
                    <tr class='text-center'>
                        <td style='font-size:60%'>{_billDate}</td>
                        <td style='font-size:60%'>{report.BillNo}</td>
                        <td style='font-size:60%'>{report.ReceptionCode}</td>
                        <td style='font-size:60%'>{report.RoomNo}</td>
                        <td style='font-size:60%'>{angelCode}</td>
                        <td style='font-size:60%'>{_checkInTime}</td>
                        <td style='font-size:60%'>{_checkOutTime}</td>
                        <td style='font-size:60%'>{sumTime}</td>
                        <td style='font-size:60%'>{round}</td>
                        <td style='font-size:60%'>{total}</td>
                        <td style='font-size:60%'>{cash}</td>
                        <td style='font-size:60%'>{qrCode}</td>
                        <td style='font-size:60%'>{credit}</td>
                        <td style='font-size:60%'>{member}</td>
                        <td style='font-size:60%'>{useSuite}</td>
                        <td style='font-size:60%'>{useVip}</td>
                        <td style='font-size:60%'>{report.Remark}</td>
                    </tr>";
                angelTotal = String.Format("{0:N}", sumResponse.Total);
                SumCashTotal = String.Format("{0:N}", sumResponse.SumCashTotal);
                SumQrCode = String.Format("{0:N}", sumResponse.SumQrCode);
                SumCredit = String.Format("{0:N}", sumResponse.SumCredit);
                SumMember = String.Format("{0:N}", sumResponse.SumMember);
                SumUseSuite = String.Format("{0:N}", sumResponse.SumUseSuite);
                SumUseVip = String.Format("{0:N}", sumResponse.SumUseVip);

            }
            footer += $@"
                            <tr class='text-center'>
                                 <td style='font-size:60%'>{angelTotal}</td>
                                 <td style='font-size:60%'>{SumCashTotal}</td>
                                 <td style='font-size:60%'>{SumQrCode}</td>
                                 <td style='font-size:60%'>{SumCredit}</td>
                                 <td style='font-size:60%'>{SumMember}</td>
                                 <td style='font-size:60%'>{SumUseSuite}</td>
                                 <td style='font-size:60%'>{SumUseVip}</td>
                            </tr>
                        ";
            string htmlGroup = html.Replace("[content]", content).Replace("[contents]", contents).Replace("[footer]", footer);
            return htmlGroup.ToString();
        }

        public string GetReportCountries(DateTime startDate, DateTime endDate, List<MassageResponse> massageResponse, List<MemberResponse> memberResponse, List<CountriesResponse> countriesResponses)
        {
            string sDate = startDate.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));
            string eDate = endDate.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));
            var html = "";
            var content = "";
            //var contents = "";
            var footer = "";
            decimal sumRound = 0;
            decimal sumTotal = 0;
            decimal sumMember = 0;

            if (massageResponse.Count > 0)
            {
                html = $@"
                       <!DOCTYPE html>
                        <html>
                            <head>
                            </head>
                            <body>
                                <div class='text-center'><h4>รายงานสรุปรายได้ประจำวันที่ {sDate} จนถึงวันที่ {eDate}</h4></div>
                            <table>
                                <thead>
                                    <tr class='text-center'>
                                      <th>วันที่</th>
                                      <th>ประเทศ</th>
                                      <th>รอบ</th>
                                      <th>รายรับ</th>
                                    </tr>
                                     <tbody>
                                    [content]
                                  </tbody>
                                </thead>
                            </table>
                            <br>
                            <table>
                               <tfoot>
                                 [footer]
                               </tfoot>
                            </table>
                            </body>
                        </html>";

                foreach (var county in countriesResponses)
                {
                    decimal total = 0;
                    decimal round = 0;
                    decimal totalRound = 0;
                    string _total = "";
                    string _totalRound = "";
                    foreach (var massages in massageResponse)
                    {
                        if (massages.massageAngels != null)
                        {
                            foreach (var massageAngel in massages.massageAngels)
                            {
                                round = massageAngel.Round;
                                if (county.Id == massages.CountriesId)
                                {
                                    totalRound += round;
                                }
                            }
                        }

                        if (county.Id == massages.CountriesId)
                        {
                            total += massages.Total;
                        }
                    }
                    _total = String.Format("{0:N}", total);
                    _totalRound = String.Format("{0:N}", totalRound);

                    if (totalRound != 0)
                    {
                        content += $@"
                                    <tr class='text-center'>
                                        <td>{sDate} - {eDate}</td>
                                        <td>{county.NameTh}</td>
                                        <td>{_totalRound}</td>
                                        <td>{_total}</td>
                                    </tr>
                                    ";
                    }
                    sumRound += totalRound;
                    sumTotal += total;
                }
                string _sumRound = String.Format("{0:N}", sumRound);
                string _sumTotal = String.Format("{0:N}", sumTotal);

                footer += $@"
                        <tr class='text-center'>
                          <td colspan='3'>ยอดรวม</td>
                          <td>{_sumRound}</td>
                          <td>{_sumTotal}</td>
                        </tr>
                        ";
            }
            else if (memberResponse.Count > 0)
            {
                html = $@"
                       <!DOCTYPE html>
                        <html>
                            <head>
                            </head>
                            <body>
                                <div class='text-center'><h4>รายงานเมมเบอร์แต่ละประเทศ</h4></div>
                            <table>
                                <thead>
                                    <tr class='text-center'>
                                      <th>ประเทศ</th>
                                      <th>จำนวน(คน)</th>
                                    </tr>
                                    [content]
                                </thead>
                            </table>
                            <br>
                            <table>
                               <tfoot>
                                 [footer]
                               </tfoot>
                            </table>
                            </body>
                        </html>";
                foreach (var county in countriesResponses)
                {
                    int i = 0;
                    foreach (var member in memberResponse)
                    {
                        if (county.Id == member.CountriesId)
                        {
                            i++;
                        }
                    }
                    string _countMember = String.Format("{0:N}", i);
                    if (i != 0)
                    {
                        content += $@"
                            <tr class='text-center'>
                              <td>{county.NameTh}</td>
                              <td>{_countMember}</td>
                            </tr>
                            ";
                    }

                    sumMember += i;
                }
                string _sumMember = String.Format("{0:N}", sumMember);
                footer += $@"
                        <tr class='text-center'>
                          <td colspan='3'>ยอดรวม</td>
                          <td>{_sumMember}</td>
                        </tr>
                        ";
            }
            string htmlGroup = html.Replace("[content]", content).Replace("[footer]", footer);
            return htmlGroup.ToString();
        }

        public string GetReportAgencies(DateTime startDate, DateTime endDate, List<AgencyPerformanceResponse> response)
        {
            string sDate = startDate.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));
            string eDate = endDate.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));
            var html =
               $@"
                    <!DOCTYPE html>
                        <html>
                            <head></head>
                            <body>
                              <div class='text-center'><h4>รายงานจ่ายค่าตอบแทนเอเจนซี่ {sDate} จนถึงวันที่ {eDate}</h4></div>
                                <table>
                                  <thead>
                                    <tr>
                                      <th>ลำดับ</th>
                                      <th>วันที่</th>
                                      <th>ชื่อ</th>
                                      <th>ประเภท</th>
                                      <th>ยอดรับ</th>
                                    </tr>
                                  <tbody>
                                    [content]
                                    [footer]
                                  </tbody>
                                  </thead>
                                </table>
                            </body>
                        </html>
                ";
            string content = "";
            string footer = "";
            decimal sumTotalRevice = 0;

            if (response != null)
            {
                int i = 0;
                foreach (var agencyPerformances in response)
                {
                    i++;
                    DateTime toDays = (DateTime)agencyPerformances.calendarDay.ToDay;
                    string _toDays = toDays.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));

                    content += $@"
                 <tr class='text-center'>
                    <td>{i}</td>
                    <td>{_toDays}</td>
                    <td>{agencyPerformances.agency.Firstname + " " + agencyPerformances.agency.Lastname}</td>
                    <td>{agencyPerformances.agency.AgencyType.Name}</td>
                    <td>{agencyPerformances.TotalReceive}</td>
                </tr>
                       ";
                    sumTotalRevice += agencyPerformances.TotalReceive;

                }
            }
            var _sumTotalRevice = String.Format("{0:N}", sumTotalRevice);
            footer += $@"
            
                <tr class='text-center'>
                   <td colspan='4'>ยอดรวม</td>
                
                   <td>{sumTotalRevice} บาท</td>
                    
                 </tr>
                 
                 
                           ";
            string htmlGroup = html.Replace("[content]", content).Replace("[footer]", footer);

            return htmlGroup.ToString();
        }
    }
}
