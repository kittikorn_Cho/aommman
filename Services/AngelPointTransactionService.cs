﻿using Microsoft.EntityFrameworkCore;
using OONApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace OONApi.Services
{
    public class AngelPointTransactionService : IAngelPointTransactionService
    {
        private readonly AppDbContext _db;

        public AngelPointTransactionService(AppDbContext db)
        {
            _db = db;
        }

        public async Task<bool> CreateAngelPointTransaction(AngelPointTransaction angelPointTransaction) 
        {
            _db.AngelPointTransactions.Add(angelPointTransaction);
            var created = await _db.SaveChangesAsync();
            return created > 0;
        }
        public async Task<AngelPointTransaction> GetByIdAsync(int id)
        {
            return await _db.AngelPointTransactions.Where(apt => apt.Id == id).SingleOrDefaultAsync();
        }
        public async Task<List<AngelPointTransaction>> GetAngelPointTransactionByMassageAngelId(List<int> massageAngelId) 
        {
            return await _db.AngelPointTransactions.Where(apt => massageAngelId.Contains(apt.MassageAngelId)).ToListAsync();
        }

        public async Task<AngelPointTransaction> GetAngelPointTransactionByMaasageAngelIdAndAngelId(int maasageAngelId,int angelId)
        {
            return await _db.AngelPointTransactions.Where(ap => ap.MassageAngelId == maasageAngelId && ap.AngelId == angelId).SingleOrDefaultAsync();
        }

        public async Task<bool> DeleteAngelPointTransation(int id)
        {
            var deleted = 0;
            var AngelPointTransaction = await GetByIdAsync(id);
            if (AngelPointTransaction != null)
            {
                _db.AngelPointTransactions.Remove(AngelPointTransaction);
                deleted = await _db.SaveChangesAsync();
            }
            return deleted > 0;
        }
    }
}
