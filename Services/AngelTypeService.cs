﻿using Microsoft.EntityFrameworkCore;
using OONApi.Contracts.Requests.Queires;
using OONApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;


namespace OONApi.Services
{
    public class AngelTypeService : IAngelTypeService
    {
        private readonly AppDbContext _db;

        public AngelTypeService(AppDbContext db)
        {
            _db = db;
        }
        public async Task<List<AngelType>> GetAllAsync(AngelTypesSearchQuery angelTypes, PaginationFilter paginationFilter = null)
        {
            if(paginationFilter == null)
            {
                return await _db.AngelTypes.OrderBy(a=>a.Order).AsNoTracking().ToListAsync();
            }

            if (typeof(AngelType).GetProperties().All(l => l.Name != paginationFilter.OrderBy.First().ToString().ToUpper() + paginationFilter.OrderBy.Substring(1)))
            {
                paginationFilter.OrderBy = "Id";
            }

            string direction = null;

            if (string.Equals(paginationFilter.OrderDirection, "desc", StringComparison.OrdinalIgnoreCase))
            {
                direction = "descending";
            }
            else
            {
                direction = "ascending";
            }

            var skip = (paginationFilter.PageNumber - 1) * paginationFilter.PageSize;

            IQueryable<AngelType> query = _db.AngelTypes;
            if (!String.IsNullOrEmpty(angelTypes.Code))
            {
                query = query.Where(a => a.Code == angelTypes.Code);
            }
            if (angelTypes.Fee != 0)
            {
                query = query.Where(a => a.Fee == angelTypes.Fee);
            }
            if (angelTypes.Wage != 0)
            {
                query = query.Where(a => a.Wage == angelTypes.Wage);
            }
            if (angelTypes.CreditComm != 0)
            {
                query = query.Where(a => a.CreditComm == angelTypes.CreditComm);
            }
            if (angelTypes.CheerComm != 0)
            {
                query = query.Where(a => a.CheerComm == angelTypes.CheerComm);
            }
            if (angelTypes.RoundTime != 0)
            {
                query = query.Where(a => a.RoundTime == angelTypes.RoundTime);
            }
            if (angelTypes.Order != 0)
            {
                query = query.Where(a => a.Order == angelTypes.Order);
            }
            if (!String.IsNullOrEmpty(paginationFilter.Keyword))
            {
                query = query.Where(a => a.Code.Contains(paginationFilter.Keyword));
            }

            paginationFilter.TotalRecords = query.Count();
            string sortBy = char.ToUpper(paginationFilter.OrderBy.First()) + paginationFilter.OrderBy.Substring(1).ToLower();
            if (paginationFilter == null)
            {
                return await query.AsNoTracking().ToListAsync();
            }
            return await query.OrderBy(sortBy + " " + direction).Skip(skip).Take(paginationFilter.PageSize).AsNoTracking().ToListAsync();

        }

        public async Task<AngelType> GetByIdAsync(int id)
        {
            return await _db.AngelTypes.Where(a => a.Id == id).SingleOrDefaultAsync();
        }
        public async Task<bool> CreateAsync(AngelType angelType)
        {
            _db.AngelTypes.Add(angelType);
            var created = await _db.SaveChangesAsync();
            return created > 0;
        }
        public async Task<bool> UpdateAsync(AngelType angelType)
        {
            _db.AngelTypes.Update(angelType);
            var updated = await _db.SaveChangesAsync();
            return updated > 0;
        }
        public async Task<bool> DeleteAsync(int id)
        {
            var deleted = 0;
            var angelType = await GetByIdAsync(id);
            if(angelType != null)
            {
                _db.AngelTypes.Remove(angelType);
                deleted = await _db.SaveChangesAsync();
            }
            return deleted > 0;
        }

        public async Task<List<AngelType>>GetAngelTypeByReport(int? id)
        {
            if(id == null)
            {
                return await _db.AngelTypes.OrderBy(a => a.Order).AsNoTracking().ToListAsync();
            }
            else
            {
                return await _db.AngelTypes.Where(a => a.Id == id).AsNoTracking().ToListAsync();
            }
        }
    }
}
