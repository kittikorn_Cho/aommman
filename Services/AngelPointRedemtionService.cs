﻿using Microsoft.EntityFrameworkCore;
using OONApi.Contracts.Requests.Queires;
using OONApi.Models;
using System.Collections.Generic;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Linq.Dynamic.Core;

namespace OONApi.Services
{
    public class AngelPointRedemtionService : IAngelPointRedemtionsService
    {
        private readonly AppDbContext _db;
        public AngelPointRedemtionService(AppDbContext db)
        {
            _db = db;
        }
        public async Task<bool> CreateAngelPointRedemtions(AngelPointRedemtion angelPointRedemtion)
        {
            _db.AngelPointRedemtions.Add(angelPointRedemtion);
            var created = await _db.SaveChangesAsync();
            return created > 0;
        }
        public async Task<AngelPointRedemtion> GetByIdAsync(int id)
        {
            return await _db.AngelPointRedemtions.Where(apr => apr.Id == id).SingleOrDefaultAsync();
        }
        public async Task<bool> CancelAngelPointRedemtions(int id)
        {
            var deleted = 0;
            var angelPointRedemtions = await GetByIdAsync(id);
            if (angelPointRedemtions != null)
            {
                _db.AngelPointRedemtions.Remove(angelPointRedemtions);
                deleted = await _db.SaveChangesAsync();
            }
            return deleted > 0;
        }
        public async Task<AngelPointRedemtion> GetAngelPointRedemtionByAngelId(int id) 
        {
            return await _db.AngelPointRedemtions.Where(apr => apr.Id == id).SingleOrDefaultAsync();
        }
        public async Task<List<AngelPointRedemtion>> GetAngelPointRedemtionsHistory(AngelPointRedemtionsHistorySearchQuery angelPointRedemtionsHistory, PaginationFilter paginationFilter = null)
        {
            if (paginationFilter == null)
            {
                return await _db.AngelPointRedemtions.Include(a => a.Angel).Include(cld => cld.CalendarDay).Where(a => a.AngelId == angelPointRedemtionsHistory.AngelId).AsNoTracking().ToListAsync();
            }

            if (typeof(AngelPointRedemtion).GetProperties().All(s => s.Name != paginationFilter.OrderBy.First().ToString().ToUpper() + paginationFilter.OrderBy.Substring(1)))
            {
                paginationFilter.OrderBy = "Id";
            }
            string direction = null;
            if (string.Equals(paginationFilter.OrderDirection, "desc", StringComparison.OrdinalIgnoreCase))
            {
                direction = "descending";
            }
            else
            {
                direction = "ascending";
            }
            var skip = (paginationFilter.PageNumber - 1) * paginationFilter.PageSize;
            IQueryable<AngelPointRedemtion> query = _db.AngelPointRedemtions.Include(a => a.Angel).Include(cld => cld.CalendarDay).Where(apr => apr.AngelId == angelPointRedemtionsHistory.AngelId);
            if (angelPointRedemtionsHistory.StartDate != null && angelPointRedemtionsHistory.EndDate != null)
            {
                query = query.Where(c => c.CalendarDay.ToDay >= angelPointRedemtionsHistory.StartDate && c.CalendarDay.ToDay <= angelPointRedemtionsHistory.EndDate);
            }
            paginationFilter.TotalRecords = query.Count();
            string sortBy = char.ToUpper(paginationFilter.OrderBy.First()) + paginationFilter.OrderBy.Substring(1).ToLower();
            if (paginationFilter == null)
            {
                return await query.AsNoTracking().ToListAsync();
            }
            return await query.OrderBy(sortBy + " " + direction).Skip(skip).Take(paginationFilter.PageSize).AsNoTracking().ToListAsync();
        }
    }
}
