﻿using OONApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Services
{
    public interface IMenuService
    {
        Task<List<Menu>> GetAllAsync();
    }
}
