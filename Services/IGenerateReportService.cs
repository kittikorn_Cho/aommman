﻿using OONApi.Contracts.Responses;
using OONApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Services
{
    public interface IGenerateReportService
    {
        string MemberSell(DateTime startDate,DateTime endDate, List<ReportMemberSellResponse> responses, SettingResponse setting);
        string SummaryDay(DateTime startDate, DateTime endDate, List<ReportSummaryDayResponse> responses,SummaryMassageResponse responseSummary, SettingResponse setting);
        string MemberPayment(DateTime startDate, DateTime endDate, List<ReportMemberPaymentResponse> responses);
        string PaymentAngelPerformance(DateTime startDate, DateTime endDate, List<ReportPaymentAngelPerformanceResponse> responses, SummaryTotalDebtResponse responseSummaryDebt, List<DebtTypeResponse> debtTypes, List<DeductTypeResponse> deductTypes);
        string DiscountMassageRoom(DateTime startDate, DateTime endDate, List<ReportDiscountRoomResponse> responses);
        string DiscountMassageAngel(DateTime startDate, DateTime endDate, List<ReportDiscountAngelResponse> responses);
        //string UnpaidBill(DateTime startDate, DateTime endDate, List<ReportUnpaidBillResponse> responses);
        string UnpaidBill(DateTime startDate, DateTime endDate, List<ReportMassageDebtResponse> responses);
        string MemberExpire(DateTime startDate, DateTime endDate, List<MemberResponse> responses);
        string DailyFinance(DateTime startDate, DateTime endDate, List<ReportSummaryDayResponse> responses, ReportDailyFinanceResponse response, SettingResponse setting);
        string ReceptionSell(DateTime startDate, DateTime endDate, List<ReportReceptionSellResponse> responses, List<ReportReceptionSellResponse> responseSummary);
        string ReceptionRound(DateTime startDate, DateTime endDate, List<ReportReceptionRoundTotalResponse> responses, List<ReportReceptionRoundTotalResponse> responseRoundTotal);
        string TableCashier(DateTime startDate, string username, ReportTableCashierResponse response, SettingResponse setting);
        string Maid(DateTime startDate, List<ReportPaymentAngelPerformanceResponse> response);
        string GetMemberFood(DateTime startDate, DateTime endDate, List<ReportMemberFoodResponse> response, SettingResponse setting);
        string SellSuite(DateTime startDate, DateTime endDate, List<ReportRoomSuiteResponse> responses, SettingResponse setting);
        string GetMemberTopup(DateTime startDate, DateTime endDate, List<ReportMemberTopupResponse> responses);
        string GetMemberCreditAmount(List<ReportMemberCreditAmountResponse> responses);
        string GetReportAngelDebt(List<ReportAngelDebtByAngelTypeResponse> angelType, List<DebtTypeResponse> debtType);
        string GetAllMemberExpire(List<MemberResponse> responses);
        string GetSalaryCountDay(DateTime startDate, DateTime endDate,AngelTypeResponse response);
        string GetIncomeDaily(DateTime startDate, DateTime endDate, List<MassageReportResponse> response, MassageReportResponse sumResponse, UserResponse user, SettingResponse setting);
        string GetReportCountries(DateTime startDate,DateTime endDate,List<MassageResponse> massageResponses,List<MemberResponse> memberResponses, List<CountriesResponse> countriesResponses);
        string GetReportAgencies(DateTime startDate, DateTime endDate, List<AgencyPerformanceResponse> response);
        string UnpaidBillOld(DateTime startDate, DateTime endDate, List<ReportUnpaidBillResponse> responses);
    }
}
