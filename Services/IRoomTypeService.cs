﻿using OONApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Services
{
    public interface IRoomTypeService
    {
        Task<List<RoomType>> GetAllAsync(PaginationFilter paginationFilter = null);
        Task<RoomType> GetByIdAsync(int id);
        Task<bool> CreateAsync(RoomType roomType);
        Task<bool> UpdateAsync(RoomType roomType);
        Task<bool> DeleteAsync(int id);
    }
}
