﻿using Microsoft.EntityFrameworkCore;
using OONApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Linq.Dynamic.Core;
using OONApi.Contracts.Requests.Queires;

namespace OONApi.Services
{
    public class ItemService : IItemService
    {
        private readonly AppDbContext _db;

        public ItemService(AppDbContext db)
        {
            _db = db;
        }

        public async Task<List<Item>> GetAllAsync(ItemSearchQuery item ,PaginationFilter paginationFilter = null)
        {
            if (paginationFilter == null)
            {
                return await _db.Items.AsNoTracking().ToListAsync();
            }

            if (typeof(Angel).GetProperties().All(l => l.Name != paginationFilter.OrderBy.First().ToString().ToUpper() + paginationFilter.OrderBy.Substring(1)))
            {
                paginationFilter.OrderBy = "Id";
            }

            string direction = null;
            direction = string.Equals(paginationFilter.OrderDirection, "desc", StringComparison.OrdinalIgnoreCase) ? "descending" : "ascending";

            var skip = (paginationFilter.PageNumber - 1) * paginationFilter.PageSize;

            IQueryable<Item> query = _db.Items;
            if (!String.IsNullOrEmpty(item.Slug))
            {
                query = query.Where(i => i.Slug.Contains(item.Slug));
            }
            if (!String.IsNullOrEmpty(item.Name))
            {
                query = query.Where(i => i.Name.Contains(item.Name));
            }
            if (!String.IsNullOrEmpty(paginationFilter.Keyword))
            {
                query = query.Where(a => a.Slug.Contains(paginationFilter.Keyword) || a.Name.Contains(paginationFilter.Keyword));
            }

            paginationFilter.TotalRecords = query.Count();
            string sortBy = char.ToUpper(paginationFilter.OrderBy.First()) + paginationFilter.OrderBy.Substring(1).ToLower();
            if (paginationFilter == null)
            {
                return await query.AsNoTracking().ToListAsync();
            }
            return await query.OrderBy(sortBy + " " + direction).Skip(skip).Take(paginationFilter.PageSize).AsNoTracking().ToListAsync();
        }
        public async Task<Item> GetByIdAsync(int id)
        {
            return await _db.Items.Where(a => a.Id == id).SingleOrDefaultAsync();
        }
        public async Task<Item> GetSuitSlug() 
        {
            return await _db.Items.Where(i => i.Slug == "suite").SingleOrDefaultAsync();
        }
        public async Task<bool> CreateAsync(Item item)
        {
            _db.Items.Add(item);
            var created = await _db.SaveChangesAsync();
            return created > 0;
        }
        public async Task<bool> UpdateAsync(Item item)
        {
            _db.Items.Update(item);
            var updated = await _db.SaveChangesAsync();
            return updated > 0;
        }
        public async Task<bool> DeleteAsync(int id)
        {
            var deleted = 0;
            var item = await GetByIdAsync(id);
            if (item != null)
            {
                var memberItems = await _db.MemberItems.Where(m => m.ItemId == item.Id).ToListAsync();
                foreach(MemberItem memberItem in memberItems)
                {
                    _db.Remove<MemberItem>(memberItem);
                    _db.SaveChanges();
                }
                _db.Items.Remove(item);
                deleted = await _db.SaveChangesAsync();
            }
            return deleted > 0;
        }
    }
}
