﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OONApi.Contracts.Responses;
using OONApi.Enum;
using OONApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Services
{
    public class ReportService : IReportService
    {
        private readonly AppDbContext _db;
        public ReportService(AppDbContext db)
        {
            _db = db;
        }

        public async Task<List<MemberTransaction>> MemberSell(List<int> calendarDayIds)
        {
            var memberSells = await _db.MemberTransactions.Include(m => m.CalendarDay).Include(m => m.Member).Include(m => m.Reception).Where(m => calendarDayIds.Contains(m.CalendarDayId)).OrderBy(i => i.Member.Code).ToListAsync();
            return memberSells;
        }

        public decimal TotalAngelType(List<int> calendarDayIds, int angelTypeId)
        {
            decimal totalRound = _db.AngelPerformances.Include(a => a.Angel).ThenInclude(an => an.AngelType).Include(a => a.CalendarDay).Where(a => a.Angel.AngelTypeId == angelTypeId && calendarDayIds.Contains(a.CarlendarDayId)).Sum(i => i.TotalRound);
            return totalRound;
        }

        public decimal TotalAngelType(int calendarDayId, int angelTypeId)
        {
            decimal totalRound = _db.AngelPerformances.Include(a => a.Angel).ThenInclude(an => an.AngelType).Include(a => a.CalendarDay).Where(a => a.Angel.AngelTypeId == angelTypeId && a.CarlendarDayId == calendarDayId).Sum(i => i.TotalRound);
            return totalRound;
        }
        public decimal TotalDeduct(List<int> calendarDayId)
        {
            decimal totalDeduct = _db.AngelPerformances.Include(a => a.CalendarDay).Include(a => a.Angel).ThenInclude(a => a.AngelType).Where(a => a.Angel.AngelType.Code != "T" && calendarDayId.Contains(a.CarlendarDayId)).Sum(i => i.TotalDeduct);
            return totalDeduct;
        }
        public decimal TotalDebt(List<int> calendarDayId)
        {
            decimal totalDebt = _db.AngelPerformances.Include(a => a.CalendarDay).Include(a => a.Angel).ThenInclude(a => a.AngelType).Where(a => a.Angel.AngelType.Code != "T" && calendarDayId.Contains(a.CarlendarDayId)).Sum(i => i.TotalDebt);
            return totalDebt;
        }
        public decimal TotalDailyDebt(List<int> calendarDayId)
        {
            decimal totalDailyDebt = _db.AngelPerformances.Include(a => a.CalendarDay).Include(a => a.Angel).ThenInclude(a => a.AngelType).Where(a => a.Angel.AngelType.Code != "T" && calendarDayId.Contains(a.CarlendarDayId)).Sum(i => i.TotalDailyDebt);
            return totalDailyDebt;
        }
        public SummaryMassageResponse SummaryMassage(List<int> calendarDayIds)
        {

            decimal DamagesTotal = 0;
            DamagesTotal = _db.Massages.Where(m => calendarDayIds.Contains(m.CalendarDayId)).Sum(i => i.DamagesTotal);

            decimal OtherServiceChargesTotal = 0;
            OtherServiceChargesTotal = _db.Massages.Where(m => calendarDayIds.Contains(m.CalendarDayId)).Sum(i => i.OtherServiceChargesTotal);

            decimal cashAngel = 0;
            cashAngel = _db.Massages.Where(m => calendarDayIds.Contains(m.CalendarDayId)).Sum(i => i.CashAngelTotal);

            decimal creditAngel = 0;
            creditAngel = _db.Massages.Where(m => calendarDayIds.Contains(m.CalendarDayId)).Sum(i => i.CreditAngelTotal);

            decimal memberAngel = 0;
            memberAngel = _db.Massages.Where(m => calendarDayIds.Contains(m.CalendarDayId)).Sum(i => i.MemberAngelTotal);

            decimal qrCodeAngel = 0;
            qrCodeAngel = _db.Massages.Where(m => calendarDayIds.Contains(m.CalendarDayId)).Sum(i => i.QrCodeAngelTotal);

            decimal cashFood = 0;
            cashFood = _db.Massages.Where(m => calendarDayIds.Contains(m.CalendarDayId)).Sum(i => i.CashFoodTotal);
            decimal creditFood = 0;
            creditFood = _db.Massages.Where(m => calendarDayIds.Contains(m.CalendarDayId)).Sum(i => i.CreditFoodTotal);
            decimal memberFood = 0;
            memberFood = _db.Massages.Where(m => calendarDayIds.Contains(m.CalendarDayId)).Sum(i => i.MemberFoodTotal);

            decimal qrCodeFood = 0;
            qrCodeFood = _db.Massages.Where(m => calendarDayIds.Contains(m.CalendarDayId)).Sum(i => i.QrCodeFoodTotal);

            decimal cutMemberfood = 0;
            cutMemberfood = _db.memberPayments.Where(m => calendarDayIds.Contains(m.CalendarId)).Sum(i => i.CerditAmount);

            decimal unpiad = 0;
            unpiad = _db.Massages.Where(m => calendarDayIds.Contains(m.CalendarDayId) && m.IsUnPaid == true).Sum(i => (i.CashAngelTotal + i.CreditAngelTotal + i.MemberAngelTotal + i.QrCodeAngelTotal) + (i.CashFoodTotal + i.CreditFoodTotal + i.MemberFoodTotal + i.QrCodeFoodTotal) + (i.CashRoomTotal + i.CreditRoomTotal + i.MemberRoomTotal + i.QrCodeRoomTotal) + (i.CashEtcTotal + i.CreditEtcTotal + i.MemberEtcTotal + i.QrCodeEtcTotal));

            decimal totalUnpaid = 0;
            totalUnpaid = _db.Massages.Where(m => calendarDayIds.Contains(m.CalendarDayId) && m.IsUnPaid == true).Sum(i => i.Total);

            decimal summaryUnpaid = totalUnpaid - unpiad;


            List<int> massageId = _db.Massages.Where(m => calendarDayIds.Contains(m.CalendarDayId) && m.IsPaid == true).Select(m => m.Id).ToList();

            List<int> _massageIdSuite = _db.MassageRooms.Include(mr => mr.Room).ThenInclude(r => r.RoomType).Where(i => massageId.Contains(i.MassageId) && (i.Room.RoomTypeId == (int)ERoomType.Suite || i.Room.RoomTypeId == (int)ERoomType.Suite3 || i.Room.RoomTypeId == (int)ERoomType.Suite4 || i.Room.RoomTypeId == (int)ERoomType.Suite5 || i.Room.RoomTypeId == (int)ERoomType.Suite6 || i.Room.RoomTypeId == (int)ERoomType.Suite7 || i.Room.RoomTypeId == (int)ERoomType.Suite8 || i.Room.RoomTypeId == (int)ERoomType.Suite9 || i.Room.RoomTypeId == (int)ERoomType.Suite10 || i.Room.RoomTypeId == (int)ERoomType.C3 || i.Room.RoomTypeId == (int)ERoomType.C5)).Select(mr => mr.MassageId).ToList();

            decimal summaryRoomSuite = 0;
            summaryRoomSuite = _db.Massages.Where(m => _massageIdSuite.Contains(m.Id)).Sum(m => m.RoomTotal);

            decimal memberRoomSuite = 0;
            memberRoomSuite = _db.Massages.Where(m => _massageIdSuite.Contains(m.Id)).Sum(m => m.MemberRoomTotal);

            decimal cashRoomSuite = 0;
            cashRoomSuite = _db.Massages.Where(m => _massageIdSuite.Contains(m.Id)).Sum(m => m.CashRoomTotal);

            decimal creditRoomSuite = 0;
            creditRoomSuite = _db.Massages.Where(m => _massageIdSuite.Contains(m.Id)).Sum(m => m.CreditRoomTotal);

            decimal qrCodeRoomSuite = 0;
            qrCodeRoomSuite = _db.Massages.Where(m => _massageIdSuite.Contains(m.Id)).Sum(m => m.QrCodeRoomTotal);

            List<int> _massageIdVip = _db.MassageRooms.Include(mr => mr.Room).ThenInclude(r => r.RoomType).Where(i => massageId.Contains(i.MassageId) && (i.Room.RoomTypeId == (int)ERoomType.Vip || i.Room.RoomTypeId == (int)ERoomType.Vip2 || i.Room.RoomTypeId == (int)ERoomType.Vip3)).Select(mr => mr.MassageId).ToList();

            decimal summaryRoomVip = 0;
            summaryRoomVip = _db.Massages.Where(m => _massageIdVip.Contains(m.Id)).Sum(m => m.RoomTotal);

            decimal cashRoomVip = 0;
            cashRoomVip = _db.Massages.Where(m => _massageIdVip.Contains(m.Id)).Sum(m => m.CashRoomTotal);

            decimal creditRoomVip = 0;
            creditRoomVip = _db.Massages.Where(m => _massageIdVip.Contains(m.Id)).Sum(m => m.CreditRoomTotal);

            decimal memberRoomVip = 0;
            memberRoomVip = _db.Massages.Where(m => _massageIdVip.Contains(m.Id)).Sum(m => m.MemberRoomTotal);

            decimal qrCodeRoomVip = 0;
            qrCodeRoomVip = _db.Massages.Where(m => _massageIdVip.Contains(m.Id)).Sum(m => m.QrCodeRoomTotal);

            decimal totaltip = 0;
            totaltip = _db.Massages.Where(m => calendarDayIds.Contains(m.CalendarDayId)).Sum(i => i.TipTotal);

            decimal qrCodetotalTip = 0;
            qrCodetotalTip = _db.Massages.Where(m => calendarDayIds.Contains(m.CalendarDayId)).Sum(i => i.TipQrCodeTotal);

            SummaryMassageResponse resp = new SummaryMassageResponse();

            resp.CashAngelTotal = cashAngel;
            resp.CreditAngelTotal = creditAngel;
            resp.MemberAngelTotal = memberAngel;
            resp.QrCodeAngelTotal = qrCodeAngel;
            resp.CashFoodTotal = cashFood;
            resp.CreditFoodTotal = creditFood;
            resp.MemberFoodTotal = memberFood;
            resp.CutMemberFood = cutMemberfood;
            resp.QrCodeFoodTotal = qrCodeFood;
            resp.TotalCash = resp.CashAngelTotal + resp.CashFoodTotal;
            resp.TotalCredit = resp.CreditAngelTotal + resp.CreditFoodTotal;
            resp.TotalMember = resp.MemberAngelTotal + resp.MemberFoodTotal;
            resp.TotalQrCode = resp.QrCodeAngelTotal + resp.QrCodeFoodTotal;
            resp.SummaryUnpaid = summaryUnpaid;
            resp.TotalRoomSuite = summaryRoomSuite;
            resp.TotalRoomVip = summaryRoomVip;
            resp.OtherServiceChargesTotal = OtherServiceChargesTotal;
            resp.DamagesTotal = DamagesTotal;
            resp.TipTotal = totaltip;
            resp.CashRoomSuiteTotal = cashRoomSuite;
            resp.CreditRoomSuiteTotal = creditRoomSuite;
            resp.MemberRoomSuiteTotal = memberRoomSuite;
            resp.QrCodeRoomSuiteTotal = qrCodeRoomSuite;
            resp.CashRoomVipTotal = cashRoomVip;
            resp.CreditRoomVipTotal = creditRoomVip;
            resp.MemberRoomVipTotal = memberRoomVip;
            resp.QrCodeRoomVipTotal = qrCodeRoomVip;
            resp.TipQrCodeTotal = qrCodetotalTip;

            return resp;
        }

        public async Task<List<MassageMemberPayment>> MemberPayment(List<int> calendarDayIds)
        {
            var member = await _db.MassageMemberPayments.Include(mp => mp.Massage).ThenInclude(c => c.CalendarDay).Include(mp => mp.Member).Where(mp => calendarDayIds.Contains(mp.Massage.CalendarDayId)).OrderBy(i => i.Member.Code).ToListAsync();
            return member;
        }
        public async Task<List<MassageMemberAndRoom>> GetMassageMemberPaymentAndRoom(int massageMemberPaymentId)
        {
            var member = await _db.massageMemberAndRooms.Include(mb => mb.Member).Include(mr => mr.MassageRoom).ThenInclude(r => r.Room).Include(mp => mp.MassageMemberPayment).Where(mm => mm.MassageMemberPaymentId == massageMemberPaymentId).ToListAsync();
            return member;
        }


        public async Task<MassageRoom> MassageRoomByMassageId(int massageId)
        {
            var massageRoom = await _db.MassageRooms.Include(mr => mr.Room).Where(mr => mr.MassageId == massageId && (mr.Room.RoomTypeId == (int)ERoomType.Suite || mr.Room.RoomTypeId == (int)ERoomType.Suite3 || mr.Room.RoomTypeId == (int)ERoomType.Suite4 || mr.Room.RoomTypeId == (int)ERoomType.Suite5 || mr.Room.RoomTypeId == (int)ERoomType.Vip || mr.Room.RoomTypeId == (int)ERoomType.Vip2 || mr.Room.RoomTypeId == (int)ERoomType.Vip3 || mr.Room.RoomTypeId == (int)ERoomType.Suite6 || mr.Room.RoomTypeId == (int)ERoomType.Suite7 || mr.Room.RoomTypeId == (int)ERoomType.Suite8 || mr.Room.RoomTypeId == (int)ERoomType.Suite9 || mr.Room.RoomTypeId == (int)ERoomType.Suite10 || mr.Room.RoomTypeId == (int)ERoomType.C3 || mr.Room.RoomTypeId == (int)ERoomType.C5)).Take(1).SingleOrDefaultAsync();
            return massageRoom;
        }
        public async Task<List<MassageRoom>> ListMassageRoomByMassageId(int massageId)
        {
            var massageRoom = await _db.MassageRooms.Include(mr => mr.Room).Where(mr => mr.MassageId == massageId && (mr.Room.RoomTypeId == (int)ERoomType.Suite || mr.Room.RoomTypeId == (int)ERoomType.Suite3 || mr.Room.RoomTypeId == (int)ERoomType.Suite4 || mr.Room.RoomTypeId == (int)ERoomType.Suite5 || mr.Room.RoomTypeId == (int)ERoomType.Vip || mr.Room.RoomTypeId == (int)ERoomType.Vip2 || mr.Room.RoomTypeId == (int)ERoomType.Vip3 || mr.Room.RoomTypeId == (int)ERoomType.Suite6 || mr.Room.RoomTypeId == (int)ERoomType.Suite7 || mr.Room.RoomTypeId == (int)ERoomType.Suite8 || mr.Room.RoomTypeId == (int)ERoomType.Suite9 || mr.Room.RoomTypeId == (int)ERoomType.Suite10 || mr.Room.RoomTypeId == (int)ERoomType.C3 || mr.Room.RoomTypeId == (int)ERoomType.C5)).ToListAsync();
            return massageRoom;
        }

        public async Task<List<MassageRoom>> DiscountMassageRoom(List<int> calendarDayIds)
        {
            var massageRooms = await _db.MassageRooms.Include(mr => mr.Room).Include(mr => mr.Reception).Include(mr => mr.Massage).ThenInclude(m => m.CalendarDay).Where(m => calendarDayIds.Contains(m.Massage.CalendarDayId) && m.IsDiscount == true).ToListAsync();
            return massageRooms;
        }
        public async Task<List<MassageAngel>> DiscountMaasageAngel(List<int> calendarDayIds)
        {
            var massageAngels = await _db.MassageAngels.Include(ma => ma.Angel).Include(ma => ma.Reception).Include(ma => ma.Massage).ThenInclude(m => m.CalendarDay).Where(m => calendarDayIds.Contains(m.Massage.CalendarDayId) && m.IsDiscount == true).ToListAsync();
            return massageAngels;
        }
        //public async Task<List<Massage>> UnpaidBill(List<int> calendarDayIds)
        //{
        //    var massage = await _db.Massages.Include(m => m.CalendarDay).Where(m => calendarDayIds.Contains(m.CalendarDayId) && (m.IsPaid == true && m.IsUnPaid == false && m.PayDate != null) || (m.IsPaid == true && m.IsUnPaid == true)).ToListAsync();
        //    return massage;
        //}
        public async Task<List<MassageDebt>> UnpaidBill(List<int> calendarDayIds)
        {
            var massage = await _db.MassageDebts.Include(m => m.Massage).Where(m => calendarDayIds.Contains(m.Massage.CalendarDayId)).OrderBy(m => m.Massage.CalendarDayId).ToListAsync();
            return massage;
        }
        public async Task<MassageAngel> GetIdReceptionInMassageAngelFirstRow(int massageId)
        {
            var massageAngel = await _db.MassageAngels.Where(ma => ma.MassageId == massageId).Take(1).SingleOrDefaultAsync();

            return massageAngel;
        }

        public async Task<List<Member>> MemberExpire(DateTime startDate, DateTime endDate)
        {
            var member = await _db.Members.Include(r => r.Reception).Include(m => m.MemberItems).Where(m => m.ExpiredDate >= startDate && m.ExpiredDate <= endDate).ToListAsync();
            return member;
        }

        public ReportDailyFinanceResponse SumTotalPaymentDailyFinance(List<int> calendarDayIds)
        {
            decimal totalCashAngelTotal = _db.Massages.Include(m => m.CalendarDay).Where(m => calendarDayIds.Contains(m.CalendarDayId)).Sum(s => s.CashAngelTotal);
            decimal totalCreditAngelTotal = _db.Massages.Include(m => m.CalendarDay).Where(m => calendarDayIds.Contains(m.CalendarDayId)).Sum(s => s.CreditAngelTotal);
            decimal totalMemberAngelTotal = _db.Massages.Include(m => m.CalendarDay).Where(m => calendarDayIds.Contains(m.CalendarDayId)).Sum(s => s.MemberAngelTotal);
            decimal totalQrCodeAngelTotal = _db.Massages.Include(m => m.CalendarDay).Where(m => calendarDayIds.Contains(m.CalendarDayId)).Sum(s => s.QrCodeAngelTotal);

            decimal totalMassageAngel = totalCashAngelTotal + totalCreditAngelTotal + totalMemberAngelTotal + totalQrCodeAngelTotal;

            decimal totalTip = _db.Massages.Include(m => m.CalendarDay).Where(m => m.CancelDate == null && calendarDayIds.Contains(m.CalendarDayId)).Sum(s => s.TipTotal);
            decimal totalTipComm = _db.Massages.Include(m => m.CalendarDay).Where(m => calendarDayIds.Contains(m.CalendarDayId)).Sum(s => s.TipCommTotal);
            decimal totalOtherServiceCharge = _db.Massages.Include(m => m.CalendarDay).Where(m => calendarDayIds.Contains(m.CalendarDayId)).Sum(s => s.OtherServiceChargesTotal);
            decimal totalDamages = _db.Massages.Include(m => m.CalendarDay).Where(m => calendarDayIds.Contains(m.CalendarDayId)).Sum(s => s.DamagesTotal);
            decimal tipQrCodeTotal = _db.Massages.Include(m => m.CalendarDay).Where(m => calendarDayIds.Contains(m.CalendarDayId)).Sum(s => s.TipQrCodeTotal);

            decimal totalCashEtc = _db.Massages.Include(m => m.CalendarDay).Where(m => calendarDayIds.Contains(m.CalendarDayId)).Sum(s => s.CashEtcTotal);
            decimal totalCreditEtc = _db.Massages.Include(m => m.CalendarDay).Where(m => calendarDayIds.Contains(m.CalendarDayId)).Sum(s => s.CreditEtcTotal);
            decimal totalMemberEtc = _db.Massages.Include(m => m.CalendarDay).Where(m => calendarDayIds.Contains(m.CalendarDayId)).Sum(s => s.MemberEtcTotal);

            decimal totalQrCodeEtc = _db.Massages.Include(m => m.CalendarDay).Where(m => calendarDayIds.Contains(m.CalendarDayId)).Sum(s => s.QrCodeEtcTotal);

            decimal totalEtc = _db.Massages.Include(m => m.CalendarDay).Where(m => calendarDayIds.Contains(m.CalendarDayId)).Sum(s => s.EtcTotal);

            decimal total = totalMassageAngel + totalEtc;

            ReportDailyFinanceResponse resp = new ReportDailyFinanceResponse();

            resp.TotalCashAngel = totalCashAngelTotal;
            resp.TotalCreditAngel = totalCreditAngelTotal;
            resp.TotalMemberAngel = totalMemberAngelTotal;
            resp.TotalQrCodeAngel = totalQrCodeAngelTotal;
            resp.TotalTip = totalTip;
            resp.TotalTipComm = totalTipComm;
            resp.TotalOtherServiceCharge = totalOtherServiceCharge;
            resp.TotalDamages = totalDamages;
            resp.TotalCashEtc = totalCashEtc;
            resp.TotalCreditEtc = totalCreditEtc;
            resp.TotalMemberEtc = totalMemberEtc;
            resp.TotalMassageAngel = totalMassageAngel;
            resp.TotalEtc = totalEtc;
            resp.Total = total;
            resp.TotalQrCodeEtc = totalQrCodeEtc;
            resp.TipQrCodeTotal = tipQrCodeTotal;

            return resp;
        }
        public ReportDailyFinanceResponse SumTotalRoundMassageAngel(List<int> calendarDayIds, int angelTypeId, decimal Fee)
        {
            decimal totalRound = _db.MassageAngels.Include(ma => ma.Angel).Include(ma => ma.Massage).ThenInclude(m => m.CalendarDay).Where(ma => calendarDayIds.Contains(ma.Massage.CalendarDayId) && ma.Angel.AngelTypeId == angelTypeId).Sum(s => s.Round);
            decimal totalRoundDiscount = _db.MassageAngels.Include(ma => ma.Massage).ThenInclude(m => m.CalendarDay).Where(ma => calendarDayIds.Contains(ma.Massage.CalendarDayId) && ma.IsDiscount == true && ma.Angel.AngelTypeId == angelTypeId).Sum(s => s.Round);
            decimal totalFee = Fee * totalRound;

            ReportDailyFinanceResponse resp = new ReportDailyFinanceResponse();
            resp.TotalFee = totalFee;
            resp.TotalRound = totalRound;
            resp.TotalRoundDiscount = totalRoundDiscount;

            return resp;
        }
        public ReportSummaryMemberTransactionResponse SummaryMemberTransaction(List<int> calendarDayIds, int receptionId)
        {
            decimal summary = _db.MemberTransactions.Include(mt => mt.Reception).Where(mt => calendarDayIds.Contains(mt.CalendarDayId) && mt.ReceptionId == receptionId).Sum(s => s.Total);

            ReportSummaryMemberTransactionResponse resp = new ReportSummaryMemberTransactionResponse();
            resp.Summary = summary;

            return resp;
        }
        public ReportReceptionRoundResponse ReceptionRoundTotal(List<int> calendarDayIds, int receptionId)
        {
            decimal totalRound = _db.MassageAngels.Include(ma => ma.Reception).Include(ma => ma.Massage).Where(ma => calendarDayIds.Contains(ma.Massage.CalendarDayId) && ma.ReceptionId == receptionId).Sum(s => s.Round);

            ReportReceptionRoundResponse resp = new ReportReceptionRoundResponse();
            resp.RoundTotal = totalRound;

            return resp;
        }

        public ReportTableCashierResponse TableCashier(int calendarId, int cashierId)
        {
            decimal total = _db.Massages.Where(m => m.CalendarDayId == calendarId && m.CashierId == cashierId && m.IsUnPaid == true).Sum(s => s.Total);
            decimal angelUnPaid = _db.Massages.Where(m => m.CalendarDayId == calendarId && m.CashierId == cashierId && m.IsUnPaid == true).Sum(s => (s.CashAngelTotal + s.CreditAngelTotal + s.MemberAngelTotal + s.QrCodeAngelTotal));
            decimal foodUnPaid = _db.Massages.Where(m => m.CalendarDayId == calendarId && m.CashierId == cashierId && m.IsUnPaid == true).Sum(s => (s.CashFoodTotal + s.CreditFoodTotal + s.MemberFoodTotal + s.QrCodeFoodTotal));
            decimal roomUnPaid = _db.Massages.Where(m => m.CalendarDayId == calendarId && m.CashierId == cashierId && m.IsUnPaid == true).Sum(s => (s.CashRoomTotal + s.CreditRoomTotal + s.MemberRoomTotal + s.QrCodeRoomTotal));
            decimal otherServiceChargeUnPaid = _db.Massages.Where(m => m.CalendarDayId == calendarId && m.CashierId == cashierId && m.IsUnPaid == true).Sum(s => (s.CashEtcTotal + s.CreditEtcTotal + s.MemberEtcTotal + s.QrCodeEtcTotal));

            decimal angelCash = _db.Massages.Where(m => m.CalendarDayId == calendarId && m.CashierId == cashierId).Sum(s => s.CashAngelTotal);
            decimal angelCredit = _db.Massages.Where(m => m.CalendarDayId == calendarId && m.CashierId == cashierId).Sum(s => s.CreditAngelTotal);
            decimal angelMember = _db.Massages.Where(m => m.CalendarDayId == calendarId && m.CashierId == cashierId).Sum(s => s.MemberAngelTotal);
            decimal angelQrCode = _db.Massages.Where(m => m.CalendarDayId == calendarId && m.CashierId == cashierId).Sum(s => s.QrCodeAngelTotal);

            decimal foodCash = _db.Massages.Where(m => m.CalendarDayId == calendarId && m.CashierId == cashierId).Sum(s => s.CashFoodTotal);
            decimal foodCredit = _db.Massages.Where(m => m.CalendarDayId == calendarId && m.CashierId == cashierId).Sum(s => s.CreditFoodTotal);
            decimal foodMember = _db.Massages.Where(m => m.CalendarDayId == calendarId && m.CashierId == cashierId).Sum(s => s.MemberFoodTotal);
            decimal foodQrCode = _db.Massages.Where(m => m.CalendarDayId == calendarId && m.CashierId == cashierId).Sum(s => s.QrCodeFoodTotal);
            decimal cutFoodMember = _db.memberPayments.Where(m => m.CalendarId == calendarId && m.CreatedBy == cashierId).Sum(s => s.CerditAmount);

            decimal roomCash = _db.Massages.Where(m => m.CalendarDayId == calendarId && m.CashierId == cashierId).Sum(s => s.CashRoomTotal);
            decimal roomCredit = _db.Massages.Where(m => m.CalendarDayId == calendarId && m.CashierId == cashierId).Sum(s => s.CreditRoomTotal);
            decimal roomMember = _db.Massages.Where(m => m.CalendarDayId == calendarId && m.CashierId == cashierId).Sum(s => s.MemberRoomTotal);
            decimal roomQrCode = _db.Massages.Where(m => m.CalendarDayId == calendarId && m.CashierId == cashierId).Sum(s => s.QrCodeRoomTotal);

            decimal otherServiceChargeCash = _db.Massages.Where(m => m.CalendarDayId == calendarId && m.CashierId == cashierId).Sum(s => s.CashEtcTotal);
            decimal otherServiceChargeCredit = _db.Massages.Where(m => m.CalendarDayId == calendarId && m.CashierId == cashierId).Sum(s => s.CreditEtcTotal);
            decimal otherServiceChargeMember = _db.Massages.Where(m => m.CalendarDayId == calendarId && m.CashierId == cashierId).Sum(s => s.MemberEtcTotal);
            decimal otherServiceChargeQrCode = _db.Massages.Where(m => m.CalendarDayId == calendarId && m.CashierId == cashierId).Sum(s => s.QrCodeEtcTotal);

            var reportCashier = _db.Settings.Where(m => m.Id == 1).SingleOrDefault();

            decimal sumCash;
            if (reportCashier.IsReportCashier)
            {
                //TL
                sumCash = angelCash + roomCash + otherServiceChargeCash;
            }
            else
            {
                //MR && CV
                sumCash = angelCash + foodCash + roomCash + otherServiceChargeCash;
            }

            decimal sumCredit = angelCredit + foodCredit + roomCredit + otherServiceChargeCredit;
            decimal sumMember = angelMember + foodMember + roomMember + otherServiceChargeMember + cutFoodMember;
            decimal sumQrCode = angelQrCode + foodQrCode + roomQrCode + otherServiceChargeQrCode;

            ReportTableCashierResponse resp = new ReportTableCashierResponse();

            resp.AngelCash = angelCash;
            resp.AngelCredit = angelCredit;
            resp.AngelMember = angelMember;
            resp.AngelQrCode = angelQrCode;

            resp.FoodCash = foodCash;
            resp.FoodCredit = foodCredit;
            resp.FoodMember = foodMember;
            resp.FoodQrCode = foodQrCode;
            resp.CutFoodMember = cutFoodMember;

            resp.RoomCash = roomCash;
            resp.RoomCredit = roomCredit;
            resp.RoomMember = roomMember;
            resp.RoomQrCode = roomQrCode;

            resp.OtherServiceChargeCash = otherServiceChargeCash;
            resp.OtherServiceChargeCredit = otherServiceChargeCredit;
            resp.OtherServiceChargeMember = otherServiceChargeMember;
            resp.OtherServiceChargeQrCode = otherServiceChargeQrCode;

            resp.SumCash = sumCash;
            resp.SumCredit = sumCredit;
            resp.SumMember = sumMember;
            resp.SumQrCode = sumQrCode;

            resp.SumUnpaid = total - (angelUnPaid + foodUnPaid + roomUnPaid + otherServiceChargeUnPaid);

            return resp;
        }

        public Task<List<MemberPayment>> GetMemberFood(List<int> calendarDayIds)
        {
            var members = _db.memberPayments.Include(m => m.Member).Where(m => calendarDayIds.Contains(m.CalendarId)).OrderBy(i => i.Member.Code).ToListAsync();
            return members;
        }

        public Task<List<MassageRoom>> GetMassageRoom(List<int> calendarDayIds, int roomId)
        {
            var massageRoom = _db.MassageRooms.Include(m => m.Massage).Include(r => r.Reception).Include(r => r.Room).Where(m => m.RoomId == roomId && m.Massage.IsPaid == true && (m.Massage.CashRoomTotal != 0 || m.Massage.CreditRoomTotal != 0 || m.Massage.MemberRoomTotal != 0 || m.Massage.QrCodeRoomTotal != 0) && calendarDayIds.Contains(m.Massage.CalendarDayId)).OrderBy(i => i.Room.Id).ToListAsync();
            return massageRoom;
        }

        public Task<MassageMemberPayment> GetMassageMemberPaymentByMassageId(int massageId)
        {
            var massageMemberPayment = _db.MassageMemberPayments.Include(m => m.Massage).Where(m => m.MassageId == massageId && m.Massage.IsPaid == true && (m.Massage.CashRoomTotal != 0 || m.Massage.CreditRoomTotal != 0 || m.Massage.MemberRoomTotal != 0)).SingleOrDefaultAsync();
            return massageMemberPayment;
        }

        public Task<List<Room>> GetRoomSuite()
        {
            var roomSuite = _db.Rooms.Include(r => r.RoomType).Where(r => r.RoomTypeId == (int)ERoomType.Suite || r.RoomTypeId == (int)ERoomType.Suite3 || r.RoomTypeId == (int)ERoomType.Suite4 || r.RoomTypeId == (int)ERoomType.Suite5 || r.RoomTypeId == (int)ERoomType.Suite6 || r.RoomTypeId == (int)ERoomType.Suite7 || r.RoomTypeId == (int)ERoomType.Suite8 || r.RoomTypeId == (int)ERoomType.Suite9 || r.RoomTypeId == (int)ERoomType.Suite10 || r.RoomTypeId == (int)ERoomType.C3 || r.RoomTypeId == (int)ERoomType.C5).OrderBy(r => r.Id).ToListAsync();
            return roomSuite;
        }

        public Task<Massage> GetSellSuite(int massageId)
        {
            return _db.Massages.Include(m => m.CalendarDay).Where(m => m.Id == massageId && m.IsPaid == true && (m.CashRoomTotal != 0 || m.CreditRoomTotal != 0 || m.MemberRoomTotal != 0 || m.QrCodeRoomTotal != 0)).SingleOrDefaultAsync();
        }


        public List<int> GetIdsAngelperformances(List<int> carlendarIds)
        {
            return _db.AngelPerformances.Where(a => carlendarIds.Contains(a.CarlendarDayId)).Select(a => a.Id).ToList();
        }

        public decimal SummaryAngelperformanceDetailDebtType(List<int> angelPerformaceIds, int debtTypeId)
        {
            return _db.AngelPerformanceDebts.Where(ad => angelPerformaceIds.Contains(ad.AngelPerformanceId) && ad.DebtTypeId == debtTypeId).Sum(ad => ad.Fee);
        }

        public async Task<List<MassageAngel>> GetMassageAngelsPerformance(List<int> carlendarIds, string angelTypeCode)
        {
            return await _db.MassageAngels.Include(ma => ma.Massage).Where(ma => carlendarIds.Contains(ma.Massage.CalendarDayId) && ma.Angel.AngelType.Code != angelTypeCode).ToListAsync();
        }

        public async Task<List<MemberTopup>> MemberTopup(List<int> carlendarIds)
        {
            return await _db.MemberTopups.Include(m => m.Member).Where(c => carlendarIds.Contains(c.CalendarId)).ToListAsync();
        }

        public async Task<List<Member>> MemberCreditAmount()
        {
            return await _db.Members.Include(m => m.Reception).Where(m => m.Status == 1).ToListAsync();
        }

        public async Task<List<AngelDebt>> GetReportAngelDebts(int angelId)
        {
            return await _db.AngelDebts.Include(ad => ad.Angel).Include(ad => ad.DebtType).Where(ad => ad.AngelId == angelId).ToListAsync();
        }

        public async Task<List<AngelPerformance>> GetAngelPerformances(List<int> carlendarIds)
        {
            return await _db.AngelPerformances.Include(a => a.CalendarDay).Include(a => a.Angel).ThenInclude(a => a.AngelType).Where(a => carlendarIds.Contains(a.CarlendarDayId)).ToListAsync();
        }
        public async Task<List<MassageAngel>> GetMassageAngelsByAngelIdAndAngelPerformancesId(int angelPerformancesId, int angelId)
        {
            return await _db.MassageAngels.Where(ma => ma.AngelPerformanceId == angelPerformancesId && ma.AngelId == angelId).ToListAsync();
        }
        public async Task<List<Member>> GetAllMemberExpire()
        {
            var member = await _db.Members.Include(r => r.Reception).Include(m => m.MemberItems).Where(m => m.Status == 2 && m.ExpiredDate < DateTime.Now).ToListAsync();
            return member;
        }
        public async Task<List<AgencyPerformance>> GetAgencyPerformancesByCalendarIdAndAgenciesId(List<int> calendarDaysId, int id)
        {
            return await _db.AgencyPerformances.Include(cld => cld.CalendarDay).Include(agc => agc.Agency).ThenInclude(agc => agc.AgencyType).Where(agcpfm => calendarDaysId.Contains(agcpfm.CarlendarDayId) && agcpfm.AgenciesId == id && agcpfm.IsPay == false).ToListAsync();
        }
        public async Task<List<Massage>> GetMassageByCalendarDayAndCountries(List<int> calendarDayIds)
        {
            return await _db.Massages.Include(ct => ct.Countries).Where(cld => calendarDayIds.Contains(cld.CalendarDayId)).ToListAsync();
        }

        public bool CheckMassageMemberAndRoom(int massageRoomId)
        {
            bool check = false;
            int count = _db.massageMemberAndRooms.Where(mr => mr.MassageRoomId == massageRoomId).Count();
            if (count > 0)
            {
                check = true;
            }
            return check;
        }
    }
}
