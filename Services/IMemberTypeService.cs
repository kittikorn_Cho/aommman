﻿using OONApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Services
{
    public interface IMemberTypeService
    {
        Task<List<MemberType>> GetAllAsync(PaginationFilter paginationFilter = null);
        Task<MemberType> GetByIdAsync(int id);
        Task<bool> CreateAsync(MemberType memberType);
        Task<bool> CreateMemberTypeItemAsync(List<MemberTypeItem> memberTypeItems);
        Task<bool> UpdateAsync(MemberType memberType);
        Task<bool> UpdateMemberTypeItemAsync(List<MemberTypeItem> memberTypeItems);
        Task<bool> DeleteAsync(int id);
        Task<MemberTypeItem> GetMemberTypeItemById(int id);
        Task<List<MemberTypeItem>> GetMemberTypeItems(int memberTypeId);
        Task<MemberTypeItem> GetMemberTypeItems(int memberTypeId, int ItemId);
    }
}
