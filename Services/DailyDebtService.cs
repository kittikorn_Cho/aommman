﻿using Microsoft.EntityFrameworkCore;
using OONApi.Contracts.Requests.Queires;
using OONApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace OONApi.Services
{
    public class DailyDebtService : IDailyDebtServicee
    {
        private readonly AppDbContext _db;

        public DailyDebtService(AppDbContext db)
        {
            _db = db;
        }

        public async Task<List<DailyDebt>> GetAllAsync(DailyDebtSearchQuery dailyDebt, PaginationFilter paginationFilter = null)
        {
            if (paginationFilter == null)
            {
                return await _db.DailyDebts.AsNoTracking().ToListAsync();
            }

            if (typeof(DailyDebt).GetProperties().All(s => s.Name != paginationFilter.OrderBy.First().ToString().ToUpper() + paginationFilter.OrderBy.Substring(1)))
            {
                paginationFilter.OrderBy = "Id";
            }
            string direction = null;
            if (string.Equals(paginationFilter.OrderDirection, "desc", StringComparison.OrdinalIgnoreCase))
            {
                direction = "descending";
            }
            else
            {
                direction = "ascending";
            }

            var skip = (paginationFilter.PageNumber - 1) * paginationFilter.PageSize;

            IQueryable<DailyDebt> query = _db.DailyDebts;
            if (!String.IsNullOrEmpty(dailyDebt.Name))
            {
                query = query.Where(a => a.Name == dailyDebt.Name);
            }
            if (!String.IsNullOrEmpty(paginationFilter.Keyword))
            {
                query = query.Where(a => a.Name.Contains(paginationFilter.Keyword));
            }
            paginationFilter.TotalRecords = query.Count();
            string sortBy = char.ToUpper(paginationFilter.OrderBy.First()) + paginationFilter.OrderBy.Substring(1).ToLower();
            if (paginationFilter == null)
            {
                return await query.AsNoTracking().ToListAsync();
            }
            return await query.OrderBy(sortBy + " "+ direction).Skip(skip).Take(paginationFilter.PageSize).AsNoTracking().ToListAsync();
        }

        public async Task<DailyDebt> GetByIdAsync(int id)
        {
            return await _db.DailyDebts.Where(a => a.Id == id).FirstOrDefaultAsync();
        }

        public async Task<bool> CreateAsync(DailyDebt dailyDebt)
        {
            _db.DailyDebts.Add(dailyDebt);
            var created = await _db.SaveChangesAsync();
            return created > 0;
        }
        public async Task<bool> UpdateAsync(DailyDebt dailyDebt)
        {
            _db.DailyDebts.Update(dailyDebt);
            var updated = await _db.SaveChangesAsync();
            return updated > 0;
        }
        public async Task<bool> DeleteAsync(int id)
        {
            var deleted = 0;
            var dailyDebt = await GetByIdAsync(id);
            if (dailyDebt != null)
            {
                _db.DailyDebts.Remove(dailyDebt);
                deleted = await _db.SaveChangesAsync();
            }
            return deleted > 0;
        }
    }
}
