﻿using Microsoft.EntityFrameworkCore;
using OONApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Linq.Dynamic.Core;

namespace OONApi.Services
{
    public class BuildingTypeService : IBuildingTypeService
    {
        private readonly AppDbContext _db;

        public BuildingTypeService(AppDbContext db)
        {
            _db = db;
        }

        public async Task<List<BuildingType>> GetAllAsync(PaginationFilter paginationFilter = null)
        {
            if (paginationFilter == null)
            {
                return await _db.BuildingTypes.AsNoTracking().ToListAsync();
            }

            if (typeof(Angel).GetProperties().All(l => l.Name != paginationFilter.OrderBy.First().ToString().ToUpper() + paginationFilter.OrderBy.Substring(1)))
            {
                paginationFilter.OrderBy = "Id";
            }

            string direction = null;
            direction = string.Equals(paginationFilter.OrderDirection, "desc", StringComparison.OrdinalIgnoreCase) ? "descending" : "ascending";

            var skip = (paginationFilter.PageNumber - 1) * paginationFilter.PageSize;

            paginationFilter.TotalRecords = _db.AngelTypes.Count(l =>
                                                EF.Functions.Like(l.Code, $"%{paginationFilter.Keyword}%")
                                            );

            var sortBy = char.ToUpper(paginationFilter.OrderBy.First()) + paginationFilter.OrderBy.Substring(1).ToLower();

            return await _db.BuildingTypes.Where(a => EF.Functions.Like(a.Name, $"%{paginationFilter.Keyword}%"))
                                        .OrderBy(sortBy + " " + direction)
                                        .Skip(skip).Take(paginationFilter.PageSize)
                                        .AsNoTracking()
                                        .ToListAsync();
        }

        public async Task<BuildingType> GetByIdAsync(int id)
        {
            return await _db.BuildingTypes.Where(b => b.Id == id).SingleOrDefaultAsync();
        }

        public async Task<bool> CreateAsync(BuildingType buildingType)
        {
            _db.BuildingTypes.Add(buildingType);
            var created = await _db.SaveChangesAsync();
            return created > 0;
        }

        public async Task<bool> UpdateAsync(BuildingType buildingType)
        {
            _db.BuildingTypes.Update(buildingType);
            var updated = await _db.SaveChangesAsync();
            return updated > 0;
        }
        public async Task<bool> DeleteAsync(int id)
        {
            var deleted = 0;
            var buildingType = await GetByIdAsync(id);
            if(buildingType != null)
            {
                _db.BuildingTypes.Remove(buildingType);
                deleted = await _db.SaveChangesAsync();
            }
            return deleted > 0;
        }
    }
}
