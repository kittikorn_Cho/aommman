﻿using Microsoft.EntityFrameworkCore;
using OONApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Linq.Dynamic.Core;
using OONApi.Enum;
using OONApi.Contracts.Requests.Queires;
using OONApi.Contracts.Responses;

namespace OONApi.Services
{
    public class RoomService : IRoomService
    {
        private readonly AppDbContext _db;

        public RoomService(AppDbContext db)
        {
            _db = db;
        }

        public async Task<List<Room>> GetAllAsync(RoomSearchQuery room, PaginationFilter paginationFilter = null)
        {
            if (paginationFilter == null)
            {
                return await _db.Rooms.Where(r => r.RoomTypeId != (int)ERoomType.SuiteSub).Include(r=>r.RoomType).Include(r=>r.BuildingType).AsNoTracking().ToListAsync();
            }
            if (typeof(Room).GetProperties().All(s => s.Name != paginationFilter.OrderBy.First().ToString().ToUpper() + paginationFilter.OrderBy.Substring(1)))
            {
                paginationFilter.OrderBy = "Id";
            }
            string direction = null;
            if (string.Equals(paginationFilter.OrderDirection, "desc", StringComparison.OrdinalIgnoreCase))
            {
                direction = "descending";
            }
            else
            {
                direction = "ascending";
            }

            var skip = (paginationFilter.PageNumber - 1) * paginationFilter.PageSize;

            IQueryable<Room> query = _db.Rooms.Where(a => a.RoomTypeId != (int)ERoomType.SuiteSub).Include(a => a.RoomType).Include(a => a.BuildingType);
            if (room.RoomTypeId != 0)
            {
                query = query.Where(a => a.RoomTypeId == room.RoomTypeId);
            }
            if (!String.IsNullOrEmpty(room.RoomNo))
            {
                query = query.Where(a => a.RoomNo.Contains(room.RoomNo));
            }
            if (room.BuildingTypeId != 0)
            {
                query = query.Where(a => a.BuildingTypeId == room.BuildingTypeId);
            }
            if (!String.IsNullOrEmpty(paginationFilter.Keyword))
            {
                query = query.Where(a => a.RoomNo.Contains(paginationFilter.Keyword));
            }
            paginationFilter.TotalRecords = query.Count();
            string sortBy = char.ToUpper(paginationFilter.OrderBy.First()) + paginationFilter.OrderBy.Substring(1).ToLower();
            if (paginationFilter == null)
            {
                return await query.AsNoTracking().ToListAsync();
            }
            return await query.OrderBy(sortBy + " " + direction).Skip(skip).Take(paginationFilter.PageSize).AsNoTracking().ToListAsync();
        }

        public async Task<Room> GetByIdAsync(int id)
        {
            return await _db.Rooms.Include(i => i.RoomType).Include(i =>i.BuildingType).Where(a => a.Id == id).SingleOrDefaultAsync();
        }
        public async Task<bool> CreateAsync(Room room)
        {
            _db.Rooms.Add(room);
            var created = await _db.SaveChangesAsync();
            return created > 0;
        }
        public async Task<bool> UpdateAsync(Room room)
        {
            _db.Rooms.Update(room);
            var updated = await _db.SaveChangesAsync();
            return updated > 0;
        }
        public async Task<bool> AddChildRooms(List<Room> childRoom)
        {
            await _db.Rooms.AddRangeAsync(childRoom);
            var add = await _db.SaveChangesAsync();
            return add > 0;
        }
        public async Task<int> AddChildRoom(Room childRoom)
        {
            await _db.Rooms.AddAsync(childRoom);
            var add = await _db.SaveChangesAsync();
            return childRoom.Id;
        }
        public bool DeleteChildRooms(int parentId,List<int> childIds)
        {
            var delete = _db.Rooms.Where(w => w.ParentRoomId == parentId && !childIds.Contains(w.Id)).ToList();
            _db.Rooms.RemoveRange(delete);
            _db.SaveChanges();
            return true;
        }
        public async Task<bool> UpdateChildRooms(List<Room> childRoom)
        {
            //var oldChildRoom = _db.Rooms.Where(r => r.Id == id).ToList();
            _db.Rooms.UpdateRange(childRoom);
            var updated = await _db.SaveChangesAsync();
            return updated > 0;
        }
        public async Task<bool> DeleteAsync(int id)
        {
            var deleted = 0;
            var room = await GetByIdAsync(id);
            if (room != null)
            {
                _db.Rooms.Remove(room);
                deleted = await _db.SaveChangesAsync();
            }
            return deleted > 0;
        }

        public async Task<List<Room>> GetChildRoomParent(int id)
        {
            List<Room> resp = await _db.Rooms.Include(r => r.BuildingType).Include(r => r.RoomType).Where(r => r.ParentRoomId == id).ToListAsync();
            return resp;
        }
        public async Task<List<Room>> GetRoomByRoomTypeId(int roomTypeId)
        {
            List<Room> resp = await _db.Rooms.Include(r => r.BuildingType).Include(r => r.RoomType).Where(r => r.RoomTypeId == roomTypeId).ToListAsync();
            return resp;
        }

        public async Task<List<Room>> GetRoomByParentId(int rarentRoomId)
        {
            List<Room> resp = await _db.Rooms.Include(r => r.BuildingType).Include(r => r.RoomType).Where(r => r.Id == rarentRoomId).ToListAsync();
            return resp;
        }
        public async Task<List<Room>> GetListRoomByRoomId(List<int> roomId)
        {
            return await _db.Rooms.Where(r => roomId.Contains(r.Id)).ToListAsync();
        }
        public async Task<Room> GetRoomByRoomId(int roomId) 
        {
            return await _db.Rooms.Where(r => r.Id == roomId).SingleOrDefaultAsync();
        }
        public async Task<List<Room>> GetRoomTypeByBuildingTypeIdAsync(int buildingTypeId,int roomTypeId)
        {
            IQueryable<Room> query = _db.Rooms.Include(a => a.RoomType).Include(a => a.BuildingType).Where(b => b.BuildingTypeId == buildingTypeId);
            if(roomTypeId == (int)ERoomType.Normal)
            {
                query = query.Where(b => b.RoomTypeId == (int)ERoomType.Normal);
            }
            if(roomTypeId == (int)ERoomType.Vip)
            {
                query = query.Where(b => b.RoomTypeId == (int)ERoomType.Vip);
            }
            if (roomTypeId == (int)ERoomType.Vip2)
            {
                query = query.Where(b => b.RoomTypeId == (int)ERoomType.Vip2);
            }
            if (roomTypeId == (int)ERoomType.Vip3)
            {
                query = query.Where(b => b.RoomTypeId == (int)ERoomType.Vip3);
            }
            if (roomTypeId == (int)ERoomType.Suite)
            {
                query = query.Where(b => b.RoomTypeId == (int)ERoomType.Suite);
            }
            if(roomTypeId == (int)ERoomType.SuiteSub)
            {
                query = query.Where(b => b.RoomTypeId == (int)ERoomType.SuiteSub);
            }
            if(roomTypeId == (int)ERoomType.Suite3)
            {
                query = query.Where(b => b.RoomTypeId == (int)ERoomType.Suite3);
            }
            if(roomTypeId == (int)ERoomType.Suite4)
            {
                query = query.Where(b => b.RoomTypeId == (int)ERoomType.Suite4);
            }
            if(roomTypeId == (int)ERoomType.Suite5)
            {
                query = query.Where(b => b.RoomTypeId == (int)ERoomType.Suite5);
            }
            if(roomTypeId == (int)ERoomType.RoundSell)
            {
                query = query.Where(b => b.RoomTypeId == (int)ERoomType.RoundSell);
            }
            if (roomTypeId == (int)ERoomType.C3)
            {
                query = query.Where(b => b.RoomTypeId == (int)ERoomType.C3);
            }
            if (roomTypeId == (int)ERoomType.C5)
            {
                query = query.Where(b => b.RoomTypeId == (int)ERoomType.C5);
            }
            if (roomTypeId == (int)ERoomType.Suite6)
            {
                query = query.Where(b => b.RoomTypeId == (int)ERoomType.Suite6);
            }
            if (roomTypeId == (int)ERoomType.Suite7)
            {
                query = query.Where(b => b.RoomTypeId == (int)ERoomType.Suite7);
            }
            if (roomTypeId == (int)ERoomType.Suite8)
            {
                query = query.Where(b => b.RoomTypeId == (int)ERoomType.Suite8);
            }
            if (roomTypeId == (int)ERoomType.Suite9)
            {
                query = query.Where(b => b.RoomTypeId == (int)ERoomType.Suite9);
            }
            if (roomTypeId == (int)ERoomType.Suite10)
            {
                query = query.Where(b => b.RoomTypeId == (int)ERoomType.Suite10);
            }
            return await query.ToListAsync();
        }
       
    }
}
