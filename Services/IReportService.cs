﻿using Microsoft.AspNetCore.Mvc;
using OONApi.Contracts.Requests;
using OONApi.Contracts.Responses;
using OONApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Services
{
    public interface IReportService
    {
        Task<List<MemberTransaction>> MemberSell(List<int> calendarDayIds);
        decimal TotalAngelType(List<int> calendarDayIds, int angelTypeId);
        decimal TotalDeduct(List<int> calendarDayId);
        decimal TotalDebt(List<int> calendarDayId);
        decimal TotalDailyDebt(List<int> calendarDayId);
        SummaryMassageResponse SummaryMassage(List<int> calendarDayIds);
        Task<List<MassageMemberPayment>> MemberPayment(List<int> calendarDayIds);
        Task<MassageRoom> MassageRoomByMassageId(int massageId);
        Task<List<MassageRoom>> ListMassageRoomByMassageId(int massageId);
        Task<List<MassageRoom>> DiscountMassageRoom(List<int> calendarDayIds);
        Task<List<MassageAngel>> DiscountMaasageAngel(List<int> calendarDayIds);
        //Task<List<Massage>> UnpaidBill(List<int> calendarDayIds);
        Task<List<MassageDebt>> UnpaidBill(List<int> calendarDayIds);
        Task<MassageAngel> GetIdReceptionInMassageAngelFirstRow(int massageId);
        Task<List<Member>> MemberExpire(DateTime startDate, DateTime endDate);
        ReportDailyFinanceResponse SumTotalRoundMassageAngel(List<int> calendarDayIds, int angelTypeId, decimal Fee);
        ReportDailyFinanceResponse SumTotalPaymentDailyFinance(List<int> calendarDayIds);
        ReportSummaryMemberTransactionResponse SummaryMemberTransaction(List<int> calendarDayIds, int receptionId);
        ReportReceptionRoundResponse ReceptionRoundTotal(List<int> calendarDayIds, int receptionId);
        ReportTableCashierResponse TableCashier(int calendarId, int cashierId);
        decimal TotalAngelType(int calendarDayId, int angelTypeId);
        Task<List<MemberPayment>> GetMemberFood(List<int> calendarDayIds);
        Task<List<MassageRoom>> GetMassageRoom(List<int> calendarDayIds, int roomId);
        Task<MassageMemberPayment> GetMassageMemberPaymentByMassageId(int massageId);
        Task<List<Room>> GetRoomSuite();
        Task<Massage> GetSellSuite(int massageId);
        List<int> GetIdsAngelperformances(List<int> carlendarIds);
        decimal SummaryAngelperformanceDetailDebtType(List<int> angelPerformaceIds, int debtTypeId);
        Task<List<MassageAngel>> GetMassageAngelsPerformance(List<int> carlendarIds, string angelTypeCode);
        Task<List<MemberTopup>> MemberTopup(List<int> carlendarIds);
        Task<List<Member>> MemberCreditAmount();
        Task<List<AngelDebt>> GetReportAngelDebts(int angelId);
        Task<List<AngelPerformance>> GetAngelPerformances(List<int> carlendarIds);
        Task<List<MassageAngel>> GetMassageAngelsByAngelIdAndAngelPerformancesId(int angelPerformancesId, int angelId);
        Task<List<Member>> GetAllMemberExpire();
        Task<List<AgencyPerformance>> GetAgencyPerformancesByCalendarIdAndAgenciesId(List<int> calendarDaysId, int id);
        Task<List<Massage>> GetMassageByCalendarDayAndCountries(List<int> calendarDayIds);
        Task<List<MassageMemberAndRoom>> GetMassageMemberPaymentAndRoom(int massageMemberPaymentId);
        bool CheckMassageMemberAndRoom(int massageRoomId);
    }
}
