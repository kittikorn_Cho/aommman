﻿using OONApi.Models;
using System.Threading.Tasks;

namespace OONApi.Services
{
    public class DailyPriceHistoryService : IDailyPriceHistoryService
    {
        private readonly AppDbContext _db;

        public DailyPriceHistoryService(AppDbContext db)
        {
            _db = db;
        }
        public async Task<bool> UpdateDailyPriceHistorys(DailyPriceHistory dailyPriceHistory)
        {
            _db.DailyPriceHistorys.Update(dailyPriceHistory);
            var updated = await _db.SaveChangesAsync();
            return updated > 0;
        }
        public async Task<bool> CreateDailyPriceHistory(DailyPriceHistory dailyPriceHistory)
        {
            _db.DailyPriceHistorys.Add(dailyPriceHistory);
            var created = await _db.SaveChangesAsync();
            return created > 0;
        }
    }
}
