﻿using Microsoft.EntityFrameworkCore;
using OONApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Services
{
    public class MenuService : IMenuService
    {
        private readonly AppDbContext _db;

        public MenuService(AppDbContext db)
        {
            _db = db;
        }

        public async Task<List<Menu>> GetAllAsync()
        {
            return await _db.Menus.ToListAsync();
        }
    }
}
