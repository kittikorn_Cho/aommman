﻿using OONApi.Contracts.Requests.Queires;
using OONApi.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OONApi.Services
{
    public interface IMemberPointRedemtionsService
    {
        public Task<bool> CreateMemberPointRedemtions(MemberPointRedemtion memberPointRedemtion);
        Task<MemberPointRedemtion> GetByIdAsync(int id);
        Task<bool> CancelMemberPointRedemtions(int id);
        Task<MemberPointRedemtion> GetMemberPointRedemtionByMemberId(int id);
        Task<List<MemberPointRedemtion>> GetMemberPointRedemtionsHistory(MemberPointRedemtionsHistorySearchQuery memberPointRedemtionsHistory, PaginationFilter paginationFilter = null);
    }
}
