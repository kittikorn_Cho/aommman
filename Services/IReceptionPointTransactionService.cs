﻿using OONApi.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OONApi.Services
{
    public interface IReceptionPointTransactionService
    {
        Task<bool> CreateReceptionPointTransaction(ReceptionPointTransaction receptionPointTransaction);
        Task<ReceptionPointTransaction> GetByIdAsync(int id);
        Task<List<ReceptionPointTransaction>> GetReceptionPointTransactionByMassageAngelId(List<int> massageAngelId);
        Task<bool> DeleteReceptionPointTransation(int id);
        Task<ReceptionPointTransaction> GetReceptionPointTransactionByMassageAngelIdAndReceptionId(int maasageAngelId, int receptionId);
    }
}
