﻿using Microsoft.EntityFrameworkCore;
using OONApi.Contracts.Requests.Queires;
using OONApi.Contracts.Responses;
using OONApi.Enum;
using OONApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using static OONApi.Contracts.ApiRoutes.ApiRoutes;

namespace OONApi.Services
{
    public class MemberService : IMemberService
    {
        private readonly AppDbContext _db;

        public MemberService(AppDbContext db)
        {
            _db = db;
        }

        public async Task<List<Member>> GetAllAsync(MemberSearchQuery member, PaginationFilter paginationFilter = null)
        {
            if (paginationFilter == null)
            {
                //return await _db.Members.Include(agc => agc.Agency).Include(c => c.Countries).Include(r => r.Reception).Include(m => m.MemberItems).Where(m => m.Status != (int)EAngelStatus.Cancel).AsNoTracking().ToListAsync();

                //return await _db.Members.Include(r => r.Reception).Include(m => m.MemberItems).Where(m => m.Status != (int)EAngelStatus.Cancel).AsNoTracking().ToListAsync();

                return await _db.Members.Include(agc => agc.Agency).Include(c => c.Countries).Include(r => r.Reception).Include(m => m.MemberItems).Where(m => m.Status != (int)EAngelStatus.Cancel).OrderBy(m => Convert.ToInt32(m.Code.Replace(m.Code.Substring(0,1), ""))).AsNoTracking().ToListAsync();
            }

            //if (typeof(Member).GetProperties().All(s => s.Name != paginationFilter.OrderBy.First().ToString().ToUpper() + paginationFilter.OrderBy.Substring(1)))
            //{
            //    paginationFilter.OrderBy = "Code";
            //}
            string direction = null;
            if (string.Equals(paginationFilter.OrderDirection, "desc", StringComparison.OrdinalIgnoreCase))
            {
                direction = "descending";
            }
            else
            {
                direction = "ascending";
            }

            var skip = (paginationFilter.PageNumber - 1) * paginationFilter.PageSize;

            IQueryable<Member> query = _db.Members.Include(agc => agc.Agency).Include(c => c.Countries).Include(r => r.Reception).Include(m => m.MemberItems).ThenInclude(m => m.Item).Where(m => m.Status != (int)EAngelStatus.Cancel).OrderBy(m => Convert.ToInt32(m.Code.Replace(m.Code.Substring(0, 1), "")));
            //IQueryable<Member> query = _db.Members.Include(r => r.Reception).Include(m => m.MemberItems).ThenInclude(m => m.Item).Where(m => m.Status != (int)EAngelStatus.Cancel);
            if (!String.IsNullOrEmpty(member.Code))
            {
                query = query.Where(r => r.Code.Contains(member.Code));
            }
            if (!String.IsNullOrEmpty(member.Firstname))
            {
                query = query.Where(r => r.Firstname.Contains(member.Firstname) || r.Lastname.Contains(member.Firstname));
            }
            if (!String.IsNullOrEmpty(member.Tel))
            {
                query = query.Where(r => r.Tel.Contains(member.Tel));
            }
            if (member.ReceptionId != 0)
            {
                query = query.Where(r => r.ReceptionId == member.ReceptionId);
            }
            if (member.Status != 0)
            {
                query = query.Where(r => r.Status == member.Status);
            }
            if (!String.IsNullOrEmpty(paginationFilter.Keyword))
            {
                query = query.Where(r => r.Firstname.Contains(paginationFilter.Keyword) || r.Lastname.Contains(paginationFilter.Keyword));
            }
            paginationFilter.TotalRecords = query.Count();
            string sortBy = char.ToUpper(paginationFilter.OrderBy.First()) + paginationFilter.OrderBy.Substring(1).ToLower();
            if (paginationFilter == null)
            {
                return await query.AsNoTracking().ToListAsync();
            }
            return await query.Skip(skip).Take(paginationFilter.PageSize).AsNoTracking().ToListAsync();
        }

        public async Task<Member> GetByIdAsync(int id)
        {
            return await _db.Members.Include(agc => agc.Agency).Include(c => c.Countries).Include(r => r.Reception).Include(m => m.MemberItems).ThenInclude(m => m.Item).Include(m => m.MemberTransactions).Where(i => i.Id == id).SingleOrDefaultAsync();
            //return await _db.Members.Include(r => r.Reception).Include(m => m.MemberItems).ThenInclude(m => m.Item).Include(m => m.MemberTransactions).Where(i => i.Id == id).SingleOrDefaultAsync();
        }
        public async Task<bool> CreateAsync(Member member)
        {
            _db.Members.Add(member);
            var create = await _db.SaveChangesAsync();
            return create > 0;
        }

        public async Task<Member> GetByCodeAsync(string code)
        {
            return await _db.Members.Where(r => r.Code == code && r.Status != 4).SingleOrDefaultAsync();
        }

        public async Task<bool> CreateMemberPayment(MemberPayment memberPayment)
        {
            _db.memberPayments.Add(memberPayment);
            var create = await _db.SaveChangesAsync();
            return create > 0;
        }
        public async Task<bool> CreateMemberItem(List<MemberItem> memberItems)
        {
            _db.MemberItems.AddRange(memberItems);
            var created = await _db.SaveChangesAsync();
            return created > 0;
        }
        public async Task<bool> CreateMemberTransactionAsync(List<MemberTransaction> memberTransactions)
        {
            _db.MemberTransactions.AddRange(memberTransactions);
            var created = await _db.SaveChangesAsync();
            return created > 0;
        }
        public async Task<bool> CreateMemberItemTransactionAsync(MemberItemTransaction memberItemTransaction)
        {
            _db.MemberItemTransactions.AddRange(memberItemTransaction);
            var created = await _db.SaveChangesAsync();
            return created > 0;
        }
        public async Task<bool> CreateMemberTopup(MemberTopup memberTopup)
        {
            _db.MemberTopups.Add(memberTopup);
            var create = await _db.SaveChangesAsync();
            return create > 0;
        }

        public async Task<bool> UpdateMemberItemAsync(List<MemberItem> memberItems)
        {
            _db.MemberItems.UpdateRange(memberItems);
            var updated = await _db.SaveChangesAsync();
            return updated > 0;
        }
        public async Task<bool> UpdateMemberItem(MemberItem memberItems)
        {
            _db.MemberItems.UpdateRange(memberItems);
            var updated = await _db.SaveChangesAsync();
            return updated > 0;
        }

        public async Task<bool> UpdateMemberTransactionAsync(MemberTransaction memberTransactions)
        {
            _db.MemberTransactions.Update(memberTransactions);
            var updated = await _db.SaveChangesAsync();

            return updated > 0;
        }
        public async Task<bool> UpdateAsync(Member member)
        {
            _db.Members.Update(member);
            var updated = await _db.SaveChangesAsync();
            return updated > 0;
        }
        public async Task<bool> UpdateMemberAndMemberItemAsync(Member member)
        {
            var updated = 0;

            foreach (MemberItem i in member.MemberItems.ToList())
            {
                _db.Update<MemberItem>(i);
                _db.SaveChanges();
            }
            if (member != null)
            {
                _db.Members.Update(member);
                updated = await _db.SaveChangesAsync();
            }
            return updated > 0;
        }

        public async Task<bool> UpdateMemberPayment(MemberPayment memberPayment)
        {
            _db.memberPayments.Update(memberPayment);
            var create = await _db.SaveChangesAsync();
            return create > 0;
        }

        public async Task<bool> DeleteAsync(int id)
        {
            var deleted = 0;

            var member = await GetByIdAsync(id);

            foreach(MemberItem i in member.MemberItems.ToList())
            {
                _db.Remove<MemberItem>(i);
                _db.SaveChanges();
            }

            if (member != null)
            {
                _db.Members.Remove(member);
                deleted = await _db.SaveChangesAsync();
            }
            return deleted > 0;
        }
        public async Task<bool> DeleteMemberTransaction(int id)
        {
            var deleted = 0;
            var memberTransaction = await GetMemberTransactionById(id);
            foreach (MemberItemTransaction i in memberTransaction.MemberItemTransactions.ToList())
            {
                _db.Remove<MemberItemTransaction>(i);
                _db.SaveChanges();
            }
            if (memberTransaction != null)
            {
                _db.MemberTransactions.Remove(memberTransaction);
                deleted = await _db.SaveChangesAsync();
            }
            return deleted > 0;
        }
        public async Task<MemberTransaction> GetMemberTransactionById(int id)
        {
            return await _db.MemberTransactions.Include(r => r.Reception).Include(m => m.Member).Include(c => c.CalendarDay).Include(m => m.MemberItemTransactions).Where(mt => mt.Id == id).SingleOrDefaultAsync();
        }

        public async Task<List<MemberTransaction>> GetMemberTransactions(MemberPaymentSearchQuery member, PaginationFilter paginationFilter = null)
        {
            if (paginationFilter == null)
            {
                return await _db.MemberTransactions.Include(r => r.Reception).Include(m => m.Member).Include(m=>m.CalendarDay).OrderByDescending(m=>m.Id).AsNoTracking().ToListAsync();
            }

            if (typeof(Member).GetProperties().All(s => s.Name != paginationFilter.OrderBy.First().ToString().ToUpper() + paginationFilter.OrderBy.Substring(1)))
            {
                paginationFilter.OrderBy = "Id";
            }
            string direction = null;
            if (string.Equals(paginationFilter.OrderDirection, "desc", StringComparison.OrdinalIgnoreCase))
            {
                direction = "descending";
            }
            else
            {
                direction = "ascending";
            }

            var skip = (paginationFilter.PageNumber - 1) * paginationFilter.PageSize;

            IQueryable<MemberTransaction> query = _db.MemberTransactions.Include(r => r.Reception).Include(m => m.Member).Include(m => m.CalendarDay).OrderByDescending(m => m.Id);
            if (!String.IsNullOrEmpty(member.Code))
            {
                query = query.Where(r => r.Member.Code.Contains(member.Code));
            }
            if (member.StartDate != null && member.EndDate != null)
            {
                query = query.Where(r => r.CalendarDay.ToDay >= member.StartDate && r.CalendarDay.ToDay <= member.EndDate);
            }
            if (member.ReceptionId != 0)
            {
                query = query.Where(r => r.ReceptionId == member.ReceptionId);
            }
            if (member.Status != 0)
            {
                if (member.Status == (int)EMemberPayment.Paid)
                {
                    query = query.Where(r => r.IsPaid && !r.IsUnPaid);
                }
                else if (member.Status == (int)EMemberPayment.Unpaid)
                {
                    query = query.Where(r => r.IsPaid && r.IsUnPaid);
                }
            }

            paginationFilter.TotalRecords = query.Count();
            string sortBy = char.ToUpper(paginationFilter.OrderBy.First()) + paginationFilter.OrderBy.Substring(1).ToLower();
            if (paginationFilter == null)
            {
                return await query.AsNoTracking().ToListAsync();
            }
            return await query.OrderBy(sortBy + " " + direction).Skip(skip).Take(paginationFilter.PageSize).AsNoTracking().ToListAsync();
        }

        public async Task<List<MassageMemberPayment>> GetMemberHistory(int id,MemberHistorySearchQuery memberHistory, PaginationFilter paginationFilter = null)
        {
            if (paginationFilter == null)
            {
                return await _db.MassageMemberPayments.Include(m => m.Massage).ThenInclude(m => m.CalendarDays).Where(m => m.MemberId == id).AsNoTracking().ToListAsync();
            }

            if (typeof(MassageMemberPayment).GetProperties().All(s => s.Name != paginationFilter.OrderBy.First().ToString().ToUpper() + paginationFilter.OrderBy.Substring(1)))
            {
                paginationFilter.OrderBy = "Id";
            }
            string direction = null;
            if (string.Equals(paginationFilter.OrderDirection, "desc", StringComparison.OrdinalIgnoreCase))
            {
                direction = "descending";
            }
            else
            {
                direction = "ascending";
            }

            var skip = (paginationFilter.PageNumber - 1) * paginationFilter.PageSize;

            IQueryable<MassageMemberPayment> query = _db.MassageMemberPayments.Include(m => m.Massage).ThenInclude(m => m.CalendarDays).Where(m => m.MemberId == id);
            if (memberHistory.StartDate != null && memberHistory.EndDate != null)
            {
                query = query.Where(m => m.Massage.CalendarDays.ToDay >= memberHistory.StartDate && m.Massage.CalendarDays.ToDay <= memberHistory.EndDate);
            }

            paginationFilter.TotalRecords = query.Count();
            string sortBy = char.ToUpper(paginationFilter.OrderBy.First()) + paginationFilter.OrderBy.Substring(1).ToLower();
            if (paginationFilter == null)
            {
                return await query.AsNoTracking().ToListAsync();
            }
            return await query.OrderBy(sortBy + " " + direction).Skip(skip).Take(paginationFilter.PageSize).AsNoTracking().ToListAsync();
        }

        public MemberSummaryResponse SummaryTotalMember(int calendarDayId)
        {
            decimal totalActive = 0;
            totalActive = _db.Members.Where(m => m.Status == 1).Sum(i => i.CreditAmount);
            decimal totalExpire = 0;
            totalExpire = _db.Members.Where(m => m.Status == 2).Sum(i => i.CreditAmount);
            decimal totalPaymentDay = 0;
            totalPaymentDay = _db.MassageMemberPayments.Include(mm => mm.Massage).Where(m => m.Massage.CalendarDayId == calendarDayId).Sum(i => i.MemberTotal);
            decimal totalFoodPaymentDay = 0;
            totalFoodPaymentDay = _db.memberPayments.Include(mm => mm.Member).Where(m => m.CalendarId == calendarDayId).Sum(i => i.CerditAmount);
            decimal totalHistoryActive = 0;
            totalHistoryActive = totalActive + totalPaymentDay + totalFoodPaymentDay;
            decimal total = totalActive + totalExpire;

            MemberSummaryResponse resp = new MemberSummaryResponse();

            resp.TotalActive = totalActive;
            resp.TotalExpire = totalExpire;
            resp.TotalPaymentDay = totalPaymentDay + totalFoodPaymentDay;
            resp.TotalHistoryActive = totalHistoryActive;
            resp.Total = total;

            return resp;
        }

        public async Task<List<MassageMemberPayment>> GetMemberPaymentByIsToday(int calendarDayId)
        {
            return await _db.MassageMemberPayments.Include(m => m.Member).ThenInclude(m=>m.Reception).Include(m => m.Massage).ThenInclude(m => m.CalendarDays).Where(m => m.Massage.CalendarDayId == calendarDayId).ToListAsync();
        }

        public async Task<List<MemberPayment>> GetMemberPaymentByCalendarDayId(int calendarDayId)
        {
            return await _db.memberPayments.Include(m => m.Member).Where(m => m.CalendarId == calendarDayId).ToListAsync();
        }

        public async Task<MemberItem> GetMemberItemSuite(int memberId,int itemId)
        {
            return await _db.MemberItems.Where(m => m.MemberId == memberId && m.ItemId == itemId).OrderByDescending(m => m.Id).FirstOrDefaultAsync();
        }

        public async Task<MemberItem> GetMemberItemById(int Id)
        {
            return await _db.MemberItems.Where(m => m.Id == Id).SingleOrDefaultAsync();
        }
        public async Task<List<MemberItem>> GetMemberItemByMemberId(int Id)
        {
            return await _db.MemberItems.Where(m => m.MemberId == Id).ToListAsync();
        }

        public async Task<List<MemberItemTransaction>> GetmemberItemTransactionByTransactionId(int memberTransactionId)
        {
            return await _db.MemberItemTransactions.Where(m => m.MemberTransactionId == memberTransactionId).ToListAsync();
        }

        public async Task<List<Member>> GetmemberByExpiredDate(DateTime dateTime)
        {
            return await _db.Members.Where(m => m.Status == 1 && (m.ExpiredDate >= dateTime.AddDays(-5) && m.ExpiredDate <= dateTime)).ToListAsync();
        }

        public async Task<List<MemberPayment>> GetMemberpayment(int id, List<int> carlendarIds, PaginationFilter paginationFilter = null)
        {
            if (paginationFilter == null)
            {
                return await _db.memberPayments.Include(m => m.Member).Include(m => m.CalendarDay).Where(m => m.MemberId == id).AsNoTracking().ToListAsync();
            }

            if (typeof(MemberPayment).GetProperties().All(s => s.Name != paginationFilter.OrderBy.First().ToString().ToUpper() + paginationFilter.OrderBy.Substring(1)))
            {
                paginationFilter.OrderBy = "Id";
            }
            string direction = null;
            if (string.Equals(paginationFilter.OrderDirection, "desc", StringComparison.OrdinalIgnoreCase))
            {
                direction = "descending";
            }
            else
            {
                direction = "ascending";
            }

            var skip = (paginationFilter.PageNumber - 1) * paginationFilter.PageSize;

            IQueryable<MemberPayment> query = _db.memberPayments.Include(m => m.Member).Include(m => m.CalendarDay).Where(m => m.MemberId == id);
            if (carlendarIds != null)
            {
                query = query.Where(m => carlendarIds.Contains(m.CalendarId));
            }

            paginationFilter.TotalRecords = query.Count();
            string sortBy = char.ToUpper(paginationFilter.OrderBy.First()) + paginationFilter.OrderBy.Substring(1).ToLower();
            if (paginationFilter == null)
            {
                return await query.AsNoTracking().ToListAsync();
            }
            return await query.OrderBy(sortBy + " " + direction).Skip(skip).Take(paginationFilter.PageSize).AsNoTracking().ToListAsync();
        }

        public async Task<MemberPayment> GetMemberPaymentById(int id)
        {
            return await _db.memberPayments.Where(m => m.Id == id).Include(m=>m.Member).SingleOrDefaultAsync();
        }

        public async Task<List<MemberPayment>> GetMemberPaymentUnpiad(List<int> carlendarIds, PaginationFilter paginationFilter = null)
        {
            if (paginationFilter == null)
            {
                return await _db.memberPayments.Include(m => m.Member).Include(m => m.CalendarDay).Where(m => m.IsUnPaid == true && m.IsPay == true).AsNoTracking().ToListAsync();
            }

            if (typeof(MemberPayment).GetProperties().All(s => s.Name != paginationFilter.OrderBy.First().ToString().ToUpper() + paginationFilter.OrderBy.Substring(1)))
            {
                paginationFilter.OrderBy = "Id";
            }
            string direction = null;
            if (string.Equals(paginationFilter.OrderDirection, "desc", StringComparison.OrdinalIgnoreCase))
            {
                direction = "descending";
            }
            else
            {
                direction = "ascending";
            }

            var skip = (paginationFilter.PageNumber - 1) * paginationFilter.PageSize;

            IQueryable<MemberPayment> query = _db.memberPayments.Include(m => m.Member).Include(m => m.CalendarDay).Where(m => m.IsUnPaid == true && m.IsPay == true);
            if (carlendarIds != null)
            {
                query = query.Where(m => carlendarIds.Contains(m.CalendarId));
            }

            paginationFilter.TotalRecords = query.Count();
            string sortBy = char.ToUpper(paginationFilter.OrderBy.First()) + paginationFilter.OrderBy.Substring(1).ToLower();
            if (paginationFilter == null)
            {
                return await query.AsNoTracking().ToListAsync();
            }
            return await query.OrderBy(sortBy + " " + direction).Skip(skip).Take(paginationFilter.PageSize).AsNoTracking().ToListAsync();
        }

        public async Task<List<MemberItemTransaction>> GetMemberItemTransactionByMemberTransactionId(int id)
        {
            return await _db.MemberItemTransactions.Include(m => m.MemberTransaction).Include(m => m.Member).Include(m => m.Item).Where(m => m.MemberTransactionId == id).ToListAsync();
        }

        public async Task<bool> CancelMemberPayment(int id)
        {
            var deleted = 0;
            var memberPayment = await GetMemberPaymentById(id);
            if (memberPayment != null) 
            {
                _db.memberPayments.Remove(memberPayment);
                deleted = await _db.SaveChangesAsync();
            }
            return deleted > 0;
        }

        //public async Task<bool> CreateMemberTransactionDebtAsync( List<MemberTransactionDebt> memberTransactionDebts)
        //{
        //    _db.MemberTransactionDebts.AddRange(memberTransactionDebts);
        //    var created = await _db.SaveChangesAsync();
        //    return created > 0;
        //}
        //public async Task<bool> UpdateMemberTransactionDebtAsync(MemberTransactionDebt memberTransactionDebt)
        //{
        //    _db.MemberTransactionDebts.Update(memberTransactionDebt);
        //    var updated = await _db.SaveChangesAsync(); 

        //    return updated > 0;
        //}
        //public async Task<MemberTransactionDebt> GetMemberTransactionDebtById(int memberTransactionDebtId)
        //{
        //    return await _db.MemberTransactionDebts.Include(m => m.MemberTransaction).Where(m => m.MemberTransactionId == memberTransactionDebtId).SingleOrDefaultAsync();
        //}

    }

}



    