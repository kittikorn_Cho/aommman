﻿using Microsoft.EntityFrameworkCore;
using OONApi.Contracts.Requests.Queires;
using OONApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace OONApi.Services
{
    public class RoleService : IRoleService
    {
        private readonly AppDbContext _db;

        public RoleService(AppDbContext db)
        {
            _db = db;
        }

        public async Task<List<Role>> GetAllAsync(RoleSearchQuery role, PaginationFilter paginationFilter = null, bool isRole = false)
        {
            if (paginationFilter == null)
            {
                if (isRole)
                {
                    return await _db.Roles.AsNoTracking().ToListAsync();
                }
                else
                {
                    return await _db.Roles.Where(r=>r.Id != 1).AsNoTracking().ToListAsync();
                }
                
            }
            if (typeof(Role).GetProperties().All(s => s.Name != paginationFilter.OrderBy.First().ToString().ToUpper() + paginationFilter.OrderBy.Substring(1)))
            {
                paginationFilter.OrderBy = "Id";
            }
            string direction = null;
            if (string.Equals(paginationFilter.OrderDirection, "desc", StringComparison.OrdinalIgnoreCase))
            {
                direction = "descending";
            }
            else
            {
                direction = "ascending";
            }

            var skip = (paginationFilter.PageNumber - 1) * paginationFilter.PageSize;

            IQueryable<Role> query = _db.Roles;
            if (!String.IsNullOrEmpty(role.Code))
            {
                query = query.Where(r => r.Code.Contains(role.Code));
            }
            if (!String.IsNullOrEmpty(role.Name))
            {
                query = query.Where(r => r.Name.Contains(role.Name));
            }
            if (!String.IsNullOrEmpty(paginationFilter.Keyword))
            {
                query = query.Where(r => r.Code.Contains(paginationFilter.Keyword) || r.Name.Contains(paginationFilter.Keyword));
            }
            if (!isRole)
            {
                query = query.Where(u => u.Id != 1);
            }
            paginationFilter.TotalRecords = query.Count();
            string sorBy = char.ToUpper(paginationFilter.OrderBy.First()) + paginationFilter.OrderBy.Substring(1).ToUpper();
            if(paginationFilter == null)
            {
                return await query.AsNoTracking().ToListAsync();
            }
            return await query.OrderBy(sorBy + " " + direction).Skip(skip).Take(paginationFilter.PageSize).AsNoTracking().ToListAsync();
        }
        public async Task<Role> GetByIdAsync(int id)
        {
            var role = await _db.Roles.Include(r=>r.RoleMenus).Where(r => r.Id == id).SingleOrDefaultAsync();
            if(role != null)
            {
                List<int> menuIds = new List<int>();
                foreach(var roleMenu in role.RoleMenus)
                {
                    int menuId = await _db.Menus.Where(m => m.Id == roleMenu.MenuId).Select(m=>m.Id).SingleOrDefaultAsync();
                    menuIds.Add(menuId);
                }
                role.MenuIds = menuIds;
            }
            return role;
        }
        public async Task<bool> CreateAsync(Role role)
        {
            _db.Roles.Add(role);
            var created = await _db.SaveChangesAsync();
            return created > 0;
        }
        public async Task<bool> UpdateAsync(Role role)
        {
            _db.Roles.Update(role);
            var updated = await _db.SaveChangesAsync();
            return updated > 0;
        }
        public async Task<bool> DeleteAsync(int id)
        {
            var deleted = 0;
            var role = await GetByIdAsync(id);

            foreach (RoleMenu i in role.RoleMenus.ToList())
            {
                _db.Remove<RoleMenu>(i);
                _db.SaveChanges();
            }

            if (role != null)
            {
                _db.Roles.Remove(role);
                deleted = await _db.SaveChangesAsync();
            }
            return deleted > 0;
        }

        public async Task<bool> CreateRoleMenu(Role role , List<int> MenuIds,string action)
        {
            if(action == "update")
            {
                if (role.RoleMenus.Count > 0)
                {
                    foreach (RoleMenu i in role.RoleMenus.ToList())
                    {
                        _db.Remove<RoleMenu>(i);
                        _db.SaveChanges();
                    }
                }
            }
          

            List<RoleMenu> list = new List<RoleMenu>();
            foreach(var id in MenuIds)
            {
                RoleMenu roleMenu = new RoleMenu
                {
                    RoleId = role.Id,
                    MenuId = id
                };
                list.Add(roleMenu);
            }

            _db.AddRange(list);
            var create = await _db.SaveChangesAsync();

            return create > 0;
        }

        //public async Task<bool> CreateRoleMenu(List<RoleMenu> roleMenus)
        //{
        //    _db.RoleMenus.AddRange(roleMenus);
        //    var created = await _db.SaveChangesAsync();
        //    return created > 0;
        //}

        //public async Task<List<int>> GetMenuByRoleId(int id)
        //{
        //    return await _db.RoleMenus.Where(r => r.RoleId == id).Select(m=>m.MenuId).ToListAsync();
        //}
    }
}
