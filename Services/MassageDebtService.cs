﻿using DocumentFormat.OpenXml.Office2010.Excel;
using Microsoft.EntityFrameworkCore;
using OONApi.Models;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Services
{
    public class MassageDebtService : IMassageDebtService
    {
        private readonly AppDbContext _db;

        public MassageDebtService(AppDbContext db)
        {
            _db = db;
        }

        public async Task<bool> CreateAsync(MassageDebt massageDebt)
        {
            _db.MassageDebts.Add(massageDebt);
            var created = await _db.SaveChangesAsync();
            return created > 0;
        }
        public async Task<bool> UpdateAsync(MassageDebt massageDebt)
        {
            _db.MassageDebts.Update(massageDebt);
            var updated = await _db.SaveChangesAsync();
            return updated > 0;
        }
        public async Task<bool> DeleteAsync(int id)
        {
            var deleted = 0;
            var massageDebt = await GetByIdAsync(id);
            if (massageDebt != null)
            {
                _db.MassageDebts.Remove(massageDebt);
                deleted = await _db.SaveChangesAsync();
            }
            return deleted > 0;
        }
        public async Task<MassageDebt> GetByIdAsync(int id)
        {
            return await _db.MassageDebts.Where(a => a.Id == id).SingleOrDefaultAsync();
        }
        public async Task<MassageDebt> GetMassageDebtByMassageId(int MassageId) 
        {
            return await _db.MassageDebts.Where(a => a.MassageId == MassageId).SingleOrDefaultAsync();
        }
    }
}
