﻿using OONApi.Models;
using System.Threading.Tasks;
using System.Collections.Generic;
using OONApi.Contracts.Requests.Queires;

namespace OONApi.Services
{
    public interface IAgencyPerformanceService
    {
        Task<bool> CreatePaymentAgencyAsync(AgencyPerformance agencyPerformance);
        Task<List<AgencyPerformance>> GetAllAgencyPerformances(AgencyPerformanceSearchQuery agencyPerformance, PaginationFilter paginationFilter = null);
        Task<AgencyPerformance> GetByIdAsync(int id);
        Task<bool> UpdateAsync(AgencyPerformance agencyPerformance);
        Task<bool> DeleteAgencyPerformances(int id);
        Task<AgencyPerformance> GetAgencyPerformancesByCalendarIdAndAgenciesId(int calendarDaysId ,int id);
        Task<List<AgencyPerformance>> GetAllByCalendarDays(List<int> calendarDayIds);
    }
}
