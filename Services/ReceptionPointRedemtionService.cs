﻿using Microsoft.EntityFrameworkCore;
using OONApi.Contracts.Requests.Queires;
using OONApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Linq.Dynamic.Core;

namespace OONApi.Services
{
    public class ReceptionPointRedemtionService : IReceptionPointRedemtionsService
    {
        private readonly AppDbContext _db;

        public ReceptionPointRedemtionService(AppDbContext db) 
        {
            _db = db;
        }
        public async Task<bool> CreateReceptionPointRedemtions(ReceptionPointRedemtion receptionPointRedemtion)
        {
            _db.ReceptionPointRedemtions.Add(receptionPointRedemtion);
            var created = await _db.SaveChangesAsync();
            return created > 0;
        }
        public async Task<ReceptionPointRedemtion> GetByIdAsync(int id)
        {
            return await _db.ReceptionPointRedemtions.Where(rpr => rpr.Id == id).SingleOrDefaultAsync();
        }
        public async Task<bool> CancelReceptionPointRedemtions(int id)
        {
            var deleted = 0;
            var receptionPointRedemtions = await GetByIdAsync(id);
            if (receptionPointRedemtions != null)
            {
                _db.ReceptionPointRedemtions.Remove(receptionPointRedemtions);
                deleted = await _db.SaveChangesAsync();
            }
            return deleted > 0;
        }
        public async Task<ReceptionPointRedemtion> GetReceptionPointRedemtionByReceptionId(int id) 
        {
            return await _db.ReceptionPointRedemtions.Where(rpr => rpr.Id == id).SingleOrDefaultAsync();
        }
        public async Task<List<ReceptionPointRedemtion>> GetReceptionPointRedemtionsHistory(ReceptionPointRedemtionsHistorySearchQuery receptionPointRedemtionsHistory, PaginationFilter paginationFilter = null) 
        {
            if (paginationFilter == null)
            {
                return await _db.ReceptionPointRedemtions.Include(r => r.Reception).Include(cld => cld.CalendarDay).Where(m => m.ReceptionId == receptionPointRedemtionsHistory.ReceptionId).AsNoTracking().ToListAsync();
            }

            if (typeof(ReceptionPointRedemtion).GetProperties().All(s => s.Name != paginationFilter.OrderBy.First().ToString().ToUpper() + paginationFilter.OrderBy.Substring(1)))
            {
                paginationFilter.OrderBy = "Id";
            }
            string direction = null;
            if (string.Equals(paginationFilter.OrderDirection, "desc", StringComparison.OrdinalIgnoreCase))
            {
                direction = "descending";
            }
            else
            {
                direction = "ascending";
            }
            var skip = (paginationFilter.PageNumber - 1) * paginationFilter.PageSize;
            IQueryable<ReceptionPointRedemtion> query = _db.ReceptionPointRedemtions.Include(r => r.Reception).Include(cld => cld.CalendarDay).Where(rpr => rpr.ReceptionId == receptionPointRedemtionsHistory.ReceptionId);
            if (receptionPointRedemtionsHistory.StartDate != null && receptionPointRedemtionsHistory.EndDate != null)
            {
                query = query.Where(c => c.CalendarDay.ToDay >= receptionPointRedemtionsHistory.StartDate && c.CalendarDay.ToDay <= receptionPointRedemtionsHistory.EndDate);
            }
            paginationFilter.TotalRecords = query.Count();
            string sortBy = char.ToUpper(paginationFilter.OrderBy.First()) + paginationFilter.OrderBy.Substring(1).ToLower();
            if (paginationFilter == null)
            {
                return await query.AsNoTracking().ToListAsync();
            }
            return await query.OrderBy(sortBy + " " + direction).Skip(skip).Take(paginationFilter.PageSize).AsNoTracking().ToListAsync();
        }
    }
}
