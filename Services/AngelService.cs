﻿using Microsoft.EntityFrameworkCore;
using OONApi.Contracts.Requests.Queires;
using OONApi.Enum;
using OONApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using static OONApi.Contracts.ApiRoutes.ApiRoutes;

namespace OONApi.Services
{
    public class AngelService : IAngelService
    {
        private readonly AppDbContext _db;

        public AngelService(AppDbContext db)
        {
            _db = db;
        }

        public async Task<List<Angel>> GetAllAsync(AngelSearchQuery angel, PaginationFilter paginationFilter = null)
        {
            if (paginationFilter == null)
            {
                return await _db.Angels.Include(a => a.AngelType).Where(a => a.Status != (int)EAngelStatus.Cancel).OrderBy(a => Convert.ToInt32(a.Code.Replace(a.Code, ""))).AsNoTracking().ToListAsync();
            }

            if (typeof(Angel).GetProperties().All(s => s.Name != paginationFilter.OrderBy.First().ToString().ToUpper() + paginationFilter.OrderBy.Substring(1)))
            {
                paginationFilter.OrderBy = "Id";
            }
            string direction = null;
            if (string.Equals(paginationFilter.OrderDirection, "desc", StringComparison.OrdinalIgnoreCase))
            {
                direction = "descending";
            }
            else
            {
                direction = "ascending";
            }

            var skip = (paginationFilter.PageNumber - 1) * paginationFilter.PageSize;

            IQueryable<Angel> query = _db.Angels.Include(a => a.AngelType).Where(a => a.Status != (int)EAngelStatus.Cancel).OrderBy(a => Convert.ToInt32(a.Code.Replace(a.Code, "")));
            if (angel.AngelTypeId != null)
            {
                query = query.Where(a => a.AngelTypeId == angel.AngelTypeId);
            }
            if (!String.IsNullOrEmpty(angel.Firstname))
            {
                query = query.Where(a => a.Firstname.Contains(angel.Firstname));
            }
            if (!String.IsNullOrEmpty(angel.Lastname))
            {
                query = query.Where(a => a.Lastname.Contains(angel.Lastname));
            }
            if (!String.IsNullOrEmpty(angel.Nickname))
            {
                query = query.Where(a => a.Nickname.Contains(angel.Nickname));
            }
            if (angel.Tel != null)
            {
                query = query.Where(a => a.Tel == a.Tel);
            }
            if (angel.Status != null)
            {
                query = query.Where(a => a.Status == angel.Status);
            }
            if (!String.IsNullOrEmpty(angel.Code))
            {
                query = query.Where(a => a.Code == angel.Code);
            }
            if (!String.IsNullOrEmpty(angel.Rfid))
            {
                query = query.Where(a => a.Rfid == angel.Rfid);
            }
            if (!String.IsNullOrEmpty(paginationFilter.Keyword))
            {
                query = query.Where(a => a.Firstname.Contains(paginationFilter.Keyword) || a.Lastname.Contains(paginationFilter.Keyword) || a.Nickname.Contains(paginationFilter.Keyword));
            }
            paginationFilter.TotalRecords = query.Count();
            string sortBy = char.ToUpper(paginationFilter.OrderBy.First()) + paginationFilter.OrderBy.Substring(1).ToLower();
            if (paginationFilter == null)
            {
                return await query.AsNoTracking().ToListAsync();
            }
            return await query.OrderBy(sortBy + " " + direction).Skip(skip).Take(paginationFilter.PageSize).AsNoTracking().ToListAsync();
        }
        public async Task<Angel> GetByIdAsync(int id)
        {
            return await _db.Angels.Include(i => i.AngelType).Where(a => a.Id == id).SingleOrDefaultAsync();
        }
        public async Task<List<AngelDebt>> GetAngelDebtByAngelId(int id)
        {
            return await _db.AngelDebts.AsNoTracking().Where(a => a.AngelId == id).ToListAsync();
        }
        public async Task<AngelDebt> GetAngelDebtByAngelidAndDebtTypeId(int angelId, int debtTypeId)
        {
            return await _db.AngelDebts.Where(a => a.AngelId == angelId && a.DebtTypeId == debtTypeId).SingleOrDefaultAsync();
        }
        public async Task<AngelDebt> GetAngelDebtById(int id)
        {
            return await _db.AngelDebts.Where(a => a.Id == id).SingleOrDefaultAsync();
        }
        public async Task<bool> CreateAsync(Angel angel)
        {
            _db.Angels.Add(angel);
            var created = await _db.SaveChangesAsync();
            return created > 0;
        }
        public async Task<bool> CreateAngelDebtAsync(List<AngelDebt> angelDebts)
        {
            _db.AngelDebts.AddRange(angelDebts);
            var created = await _db.SaveChangesAsync();
            return created > 0;
        }
        public async Task<bool> UpdateAsync(Angel angel)
        {
            _db.Angels.Update(angel);
            var updated = await _db.SaveChangesAsync();
            return updated > 0;
        }
        public async Task<bool> UpdateAngelDebtAsync(List<AngelDebt> angelDebts)
        {
            _db.AngelDebts.UpdateRange(angelDebts);
            var updated = await _db.SaveChangesAsync();
            return updated > 0;
        }

        public async Task<bool> UpdateAngelDebtByPerformance(AngelDebt angelDebt)
        {
            _db.AngelDebts.Update(angelDebt);
            var updated = await _db.SaveChangesAsync();
            return updated > 0;
        }
        public async Task<bool> DeleteAsync(int id)
        {
            var deleted = 0;
            var angel = await GetByIdAsync(id);
            if (angel != null)
            {
                _db.Angels.Remove(angel);
                deleted = await _db.SaveChangesAsync();
            }
            return deleted > 0;
        }
        public async Task<bool> IsDuplicateRfid(Angel angel,bool isUpdate = false)
        {
            if (isUpdate)
            {
                return await _db.Angels.AnyAsync(a => a.Rfid == angel.Rfid && a.Id != angel.Id);
            }
            else
            {
                return await _db.Angels.AnyAsync(a => a.Rfid == angel.Rfid);
            }
        }
        public async Task<bool> CheckInTimeAngel(AngelWorking angelWorking)
        {
            _db.AngelWorkings.Add(angelWorking);
            var created = await _db.SaveChangesAsync();
            return created > 0;
        }
        public async Task<bool>CheckOutTimeAngel(AngelWorking angelWorking)
        {
            _db.AngelWorkings.Update(angelWorking);
            var updated = await _db.SaveChangesAsync();
            return updated > 0;
        }
        public async Task<AngelWorking> GetAngelWorking(int angelId, int calendarDayId)
        {
            return await _db.AngelWorkings.Where(a => a.AngelId == angelId && a.CalendarDayId == calendarDayId).SingleOrDefaultAsync();
        }
        public async Task<List<AngelWorking>> GetAllWorkingAsync(int angelTypeId,int calendarDayId)
        {
            //return await _db.AngelWorkings.Include(a => a.Angel).ThenInclude(a => a.AngelType).Include(a => a.CalendarDay).Where(a => a.Angel.AngelTypeId == angelTypeId && a.CalendarDay.Id == calendarDayId).ToListAsync();
            var angelType = await _db.AngelTypes.Where(at => at.Id == angelTypeId).SingleOrDefaultAsync();
            return await _db.AngelWorkings.Include(a => a.Angel).ThenInclude(a => a.AngelType).Include(a => a.CalendarDay).Where(a => a.Angel.AngelTypeId == angelTypeId && a.CalendarDay.Id == calendarDayId).OrderBy(a => Convert.ToInt32(a.Angel.Code.Replace(angelType.Code, ""))).ToListAsync();
        }
        public async Task<Angel> GetByRfidAsync(string rfid)
        {
            return await _db.Angels.Include(i => i.AngelType).Where(a => a.Rfid == rfid).SingleOrDefaultAsync();
        }
        public async Task<List<Angel>> GetAngelByAngeltypeId(int angelTypeid)
        {
            return await _db.Angels.Where(a => a.AngelTypeId == angelTypeid).ToListAsync();
        }
        public async Task<List<Angel>> GetAngelByAngeltypeIdStatus(int angelTypeid)
        {
            var angelType = await _db.AngelTypes.Where(at => at.Id == angelTypeid).SingleOrDefaultAsync();
            return await _db.Angels.Where(a => a.AngelTypeId == angelTypeid && a.Status != (int)EAngelStatus.Cancel).OrderBy(a => Convert.ToInt32(a.Code.Replace(angelType.Code, ""))).ToListAsync();
        }
        public async Task<List<AngelPerformanceDebt>> GetHistoryDebt(List<int> calendarIds,AngelSearchQuery angel, PaginationFilter paginationFilter = null)
        {
            //if (paginationFilter == null)
            //{
            //    return await _db.Angels.Include(a => a.AngelType).AsNoTracking().ToListAsync();
            //}

            if (typeof(Angel).GetProperties().All(s => s.Name != paginationFilter.OrderBy.First().ToString().ToUpper() + paginationFilter.OrderBy.Substring(1)))
            {
                paginationFilter.OrderBy = "Id";
            }
            string direction = null;
            if (string.Equals(paginationFilter.OrderDirection, "desc", StringComparison.OrdinalIgnoreCase))
            {
                direction = "descending";
            }
            else
            {
                direction = "ascending";
            }

            var skip = (paginationFilter.PageNumber - 1) * paginationFilter.PageSize;

            IQueryable<AngelPerformanceDebt> query = _db.AngelPerformanceDebts.Include(ad => ad.DebtType).Include(ad => ad.AngelPerformance).ThenInclude(a => a.CalendarDay);
            if(angel.Id != null)
            {
                query = query.Where(ad => ad.AngelPerformance.AngelId == angel.Id);
            }
            if(calendarIds.Count > 0 && calendarIds != null)
            {
                query = query.Where(ad => calendarIds.Contains(ad.AngelPerformance.CarlendarDayId));
            }
            paginationFilter.TotalRecords = query.Count();
            string sortBy = char.ToUpper(paginationFilter.OrderBy.First()) + paginationFilter.OrderBy.Substring(1).ToLower();
            if (paginationFilter == null)
            {
                return await query.AsNoTracking().ToListAsync();
            }
            return await query.OrderBy(sortBy + " " + direction).Skip(skip).Take(paginationFilter.PageSize).AsNoTracking().ToListAsync();
        }
        public async Task<bool> CreateAngelGalleryAsync(AngelGallery angelGallery)
        {
            _db.AngelGallerys.Add(angelGallery);
            var created = await _db.SaveChangesAsync();
            return created > 0;
        }
        public async Task<AngelWorking> GetAngelByIdAsync(int id)
        {
            return await _db.AngelWorkings.Where(aw => aw.Id == id).SingleOrDefaultAsync();
        }
        public async Task<bool> DeleteAngelsWorking(int id)
        {
            var deleted = 0;
            var angelWorking = await GetAngelByIdAsync(id);
            if (angelWorking != null)
            {
                _db.AngelWorkings.Remove(angelWorking);
                deleted = await _db.SaveChangesAsync();
            }
            return deleted > 0;
        }
        public async Task<AngelWorking> GetAngelWorkingById(int id)
        {
            return await _db.AngelWorkings.Where(aw => aw.Id == id).SingleOrDefaultAsync();
        }
        //public async Task<AngelGallery>
    }
}
