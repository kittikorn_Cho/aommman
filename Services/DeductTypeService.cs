﻿using Microsoft.EntityFrameworkCore;
using OONApi.Contracts.Requests.Queires;
using OONApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace OONApi.Services
{
    public class DeductTypeService : IDeductTypeService
    {
        private readonly AppDbContext _db;

        public DeductTypeService(AppDbContext db)
        {
            _db = db;
        }

        public async Task<List<DeductType>> GetAllAsync(DeductTypeSearchQuery deductType, PaginationFilter paginationFilter = null)
        {
            if (paginationFilter == null)
            {
                return await _db.DeductTypes.AsNoTracking().ToListAsync();
            }

            if (typeof(DeductType).GetProperties().All(s => s.Name != paginationFilter.OrderBy.First().ToString().ToUpper() + paginationFilter.OrderBy.Substring(1)))
            {
                paginationFilter.OrderBy = "Id";
            }
            string direction = null;
            if (string.Equals(paginationFilter.OrderDirection, "desc", StringComparison.OrdinalIgnoreCase))
            {
                direction = "descending";
            }
            else
            {
                direction = "ascending";
            }

            var skip = (paginationFilter.PageNumber - 1) * paginationFilter.PageSize;

            IQueryable<DeductType> query = _db.DeductTypes;
            if (!String.IsNullOrEmpty(deductType.Name))
            {
                query = query.Where(d => d.Name == deductType.Name);
            }
            if (!String.IsNullOrEmpty(paginationFilter.Keyword))
            {
                query = query.Where(d => d.Name.Contains(paginationFilter.Keyword));
            }
            paginationFilter.TotalRecords = query.Count();
            string sortBy = char.ToUpper(paginationFilter.OrderBy.First()) + paginationFilter.OrderBy.Substring(1).ToLower();
            if (paginationFilter == null)
            {
                return await query.AsNoTracking().ToListAsync();
            }
            return await query.OrderBy(sortBy + " " + direction).Skip(skip).Take(paginationFilter.PageSize).AsNoTracking().ToListAsync();
        }

        public async Task<DeductType> GetByIdAsync(int id)
        {
            return await _db.DeductTypes.Where(a => a.Id == id).SingleOrDefaultAsync();
        }

        public async Task<bool> CreateAsync(DeductType deductType)
        {
            _db.DeductTypes.Add(deductType);
            var create = await _db.SaveChangesAsync();
            return create > 0;
        }

        public async Task<bool> UpdateAsync(DeductType deductType)
        {
            _db.DeductTypes.Update(deductType);
            var update = await _db.SaveChangesAsync();
            return update > 0;
        }

        public async Task<bool> DeleteAsync(int id)
        {
            var deleted = 0;
            var deductType = await GetByIdAsync(id);
            if (deductType != null)
            {
                _db.DeductTypes.Remove(deductType);
                deleted = await _db.SaveChangesAsync();
            }
            return deleted > 0;
        }

        public bool CheckAngelPerformanceDeduct(int massageAngelId)
        {
           int Count = _db.AngelPerformanceDeducts.Include(a => a.MassageAngel).Where(a => a.MassageAngelId == massageAngelId).Count();
            if(Count >= 2)
            {
                return true;
            }
            return false;
        }
    }
}
