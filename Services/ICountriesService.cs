﻿using DocumentFormat.OpenXml.Office2010.ExcelAc;
using OONApi.Models;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;

namespace OONApi.Services
{
    public interface ICountriesService
    {
        Task<List<Countries>> GetAllAsync();
        Task<Countries> GetByIdAsync(int id);
    }
}
