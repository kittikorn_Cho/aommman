﻿using OONApi.Contracts.Requests.Queires;
using OONApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Services
{
    public interface IRoomService
    {
        Task<List<Room>> GetAllAsync(RoomSearchQuery room, PaginationFilter paginationFilter = null);
        Task<Room> GetByIdAsync(int id);
        Task<bool> CreateAsync(Room room);
        Task<bool> UpdateAsync(Room room);
        Task<bool> DeleteAsync(int id);
        Task<List<Room>> GetChildRoomParent(int id);
        Task<List<Room>> GetRoomByRoomTypeId(int roomTypeId);
        Task<bool> AddChildRooms(List<Room> childRoom);
        Task<int> AddChildRoom(Room childRoom);
        bool DeleteChildRooms(int parentId, List<int> childIds);
        Task<bool> UpdateChildRooms(List<Room> childRoom);
        Task<List<Room>> GetRoomTypeByBuildingTypeIdAsync(int buildingTypeId, int roomTypeId);
        Task<List<Room>> GetRoomByParentId(int rarentRoomId);
        Task<List<Room>> GetListRoomByRoomId(List<int> roomId);
        Task<Room> GetRoomByRoomId(int roomId);
    }
}
