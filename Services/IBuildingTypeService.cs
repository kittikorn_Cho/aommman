﻿using OONApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Services
{
    public interface IBuildingTypeService
    {
        Task<List<BuildingType>> GetAllAsync(PaginationFilter paginationFilter = null);
        Task<BuildingType> GetByIdAsync(int id);
        Task<bool> CreateAsync(BuildingType buildingType);
        Task<bool> UpdateAsync(BuildingType buildingType);
        Task<bool> DeleteAsync(int id);
    }
}
