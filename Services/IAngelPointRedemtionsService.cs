﻿using OONApi.Contracts.Requests.Queires;
using OONApi.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OONApi.Services
{
    public interface IAngelPointRedemtionsService
    {
        public Task<bool> CreateAngelPointRedemtions(AngelPointRedemtion angelPointRedemtion);
        Task<AngelPointRedemtion> GetByIdAsync(int id);
        Task<bool> CancelAngelPointRedemtions(int id);
        Task<AngelPointRedemtion> GetAngelPointRedemtionByAngelId(int id);
        Task<List<AngelPointRedemtion>> GetAngelPointRedemtionsHistory(AngelPointRedemtionsHistorySearchQuery AngelPointRedemtionsHistory, PaginationFilter paginationFilter = null);
    }
}
