﻿using Microsoft.EntityFrameworkCore;
using OONApi.Contracts.Requests.Queires;
using OONApi.Contracts.Responses;
using OONApi.Enum;
using OONApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace OONApi.Services
{
    public class AgencyService : IAgencyService
    {
        private readonly AppDbContext _db;

        public AgencyService(AppDbContext db)
        {
            _db= db;
        }
        public async Task<List<Agency>> GetAllAsync(AgencySearchQuery agency, PaginationFilter paginationFilter = null)
        {
            if (paginationFilter == null)
            {
                return await _db.Agency.Include(a => a.AgencyType).Where(r => r.Status != (int)EAngelStatus.Cancel).OrderBy(r => r.Code).AsNoTracking().ToListAsync();
            }

            if (typeof(Agency).GetProperties().All(s => s.Name != paginationFilter.OrderBy.First().ToString().ToUpper() + paginationFilter.OrderBy.Substring(1)))
            {
                paginationFilter.OrderBy = "Id";
            }
            string direction = null;
            if (string.Equals(paginationFilter.OrderDirection, "desc", StringComparison.OrdinalIgnoreCase))
            {
                direction = "descending";
            }
            else
            {
                direction = "ascending";
            }

            var skip = (paginationFilter.PageNumber - 1) * paginationFilter.PageSize;

            IQueryable<Agency> query = _db.Agency.Include(a => a.AgencyType).Where(r => r.Status != (int)EAngelStatus.Cancel);

            if (agency.Id > 0)
            {
                query = query.Where(r => r.Id == agency.Id);
            }
            if (!String.IsNullOrEmpty(agency.Code))
            {
                query = query.Where(r => r.Code.Contains(agency.Code));
            }
            if (!String.IsNullOrEmpty(agency.Firstname))
            {
                query = query.Where(r => r.Firstname.Contains(agency.Firstname));
            }
            if (!String.IsNullOrEmpty(agency.Lastname))
            {
                query = query.Where(r => r.Lastname.Contains(agency.Lastname));
            }
            if (!String.IsNullOrEmpty(agency.Nickname))
            {
                query = query.Where(r => r.Nickname.Contains(agency.Nickname));
            }
            if (agency.Tel != null)
            {
                query = query.Where(r => r.Tel.Contains(agency.Tel));
            }

            if (!String.IsNullOrEmpty(paginationFilter.Keyword))
            {
                query = query.Where(r => r.Firstname.Contains(paginationFilter.Keyword) || r.Lastname.Contains(paginationFilter.Keyword) || r.Nickname.Contains(paginationFilter.Keyword));
            }
            paginationFilter.TotalRecords = query.Count();
            string sortBy = char.ToUpper(paginationFilter.OrderBy.First()) + paginationFilter.OrderBy.Substring(1).ToLower();
            if (paginationFilter == null)
            {
                return await query.AsNoTracking().ToListAsync();
            }
            return await query.OrderBy(sortBy + " " + direction).Skip(skip).Take(paginationFilter.PageSize).AsNoTracking().ToListAsync();
        }
        public async Task<Agency> GetByIdAsync(int id)
        {
            return await _db.Agency.Include(a => a.AgencyType).Where(agc => agc.Id == id).SingleOrDefaultAsync();
        }
        public async Task<bool> CreateAsync(Agency agency)
        {
            _db.Agency.Add(agency);
            var created = await _db.SaveChangesAsync();
            return created > 0;
        }
        public async Task<bool> UpdateAsync(Agency agency)
        {
            _db.Agency.Update(agency);
            var updated = await _db.SaveChangesAsync();
            return updated > 0;
        }
       
        public async Task<List<AgencyPerformance>> GetAgenciesPerformanceByCalendarDay(int calendarDayIds,int agenciesId)
        {
            return await _db.AgencyPerformances.Where(agcpfm => agcpfm.CarlendarDayId == calendarDayIds && agcpfm.AgenciesId == agenciesId).ToListAsync();
        }

        public async Task<List<AgencyPerformance>> GetAllAgenciesPerformanceAsync(List<int> calendarDayIds)
        {
            return await _db.AgencyPerformances.Where(agcpfm => calendarDayIds.Contains(agcpfm.CarlendarDayId)).ToListAsync();
        }

        public async Task<List<AgencyType>> GetAllAgencyTypesAsync()
        {
            return await _db.AgencyTypes.AsNoTracking().ToListAsync();
        }
        

    }
}
