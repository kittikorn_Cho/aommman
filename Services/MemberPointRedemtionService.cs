﻿using Microsoft.EntityFrameworkCore;
using OONApi.Contracts.Requests.Queires;
using OONApi.Enum;
using OONApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace OONApi.Services
{
    public class MemberPointRedemtionService : IMemberPointRedemtionsService
    {
        private readonly AppDbContext _db;
        public MemberPointRedemtionService(AppDbContext db)
        {
            _db = db;
        }
        public async Task<bool> CreateMemberPointRedemtions(MemberPointRedemtion memberPointRedemtion)
        {
            _db.MemberPointRedemtions.Add(memberPointRedemtion);
            var created = await _db.SaveChangesAsync();
            return created > 0;
        }
        public async Task<MemberPointRedemtion> GetByIdAsync(int id)
        {
            return await _db.MemberPointRedemtions.Where(mpr => mpr.Id == id).SingleOrDefaultAsync();
        }
        public async Task<bool> CancelMemberPointRedemtions(int id)
        {
            var deleted = 0;
            var memberPointtRedemtions = await GetByIdAsync(id);
            if (memberPointtRedemtions != null)
            {
                _db.MemberPointRedemtions.Remove(memberPointtRedemtions);
                deleted = await _db.SaveChangesAsync();
            }
            return deleted > 0;
        }

        public async Task<MemberPointRedemtion> GetMemberPointRedemtionByMemberId(int id) 
        {
            return await _db.MemberPointRedemtions.Where(mpr => mpr.Id == id).SingleOrDefaultAsync();
        }

        public async Task<List<MemberPointRedemtion>> GetMemberPointRedemtionsHistory(MemberPointRedemtionsHistorySearchQuery memberPointRedemtionsHistory, PaginationFilter paginationFilter = null) 
        {
            if (paginationFilter == null)
            {
                return await _db.MemberPointRedemtions.Include(m => m.Member).Include(cld => cld.CalendarDay).Where(m => m.MemberId == memberPointRedemtionsHistory.MemberId).AsNoTracking().ToListAsync();
            }

            if (typeof(MemberPointRedemtion).GetProperties().All(s => s.Name != paginationFilter.OrderBy.First().ToString().ToUpper() + paginationFilter.OrderBy.Substring(1)))
            {
                paginationFilter.OrderBy = "Id";
            }
            string direction = null;
            if (string.Equals(paginationFilter.OrderDirection, "desc", StringComparison.OrdinalIgnoreCase))
            {
                direction = "descending";
            }
            else
            {
                direction = "ascending";
            }
            var skip = (paginationFilter.PageNumber - 1) * paginationFilter.PageSize;

            IQueryable<MemberPointRedemtion> query = _db.MemberPointRedemtions.Include(m => m.Member).Include(cld => cld.CalendarDay).Where(mpr => mpr.MemberId == memberPointRedemtionsHistory.MemberId);
            if (memberPointRedemtionsHistory.StartDate != null && memberPointRedemtionsHistory.EndDate != null)
            {
                query = query.Where(c => c.CalendarDay.ToDay >= memberPointRedemtionsHistory.StartDate && c.CalendarDay.ToDay <= memberPointRedemtionsHistory.EndDate);
            }
            paginationFilter.TotalRecords = query.Count();
            string sortBy = char.ToUpper(paginationFilter.OrderBy.First()) + paginationFilter.OrderBy.Substring(1).ToLower();
            if (paginationFilter == null)
            {
                return await query.AsNoTracking().ToListAsync();
            }
            return await query.OrderBy(sortBy + " " + direction).Skip(skip).Take(paginationFilter.PageSize).AsNoTracking().ToListAsync();
        }
    }
}
