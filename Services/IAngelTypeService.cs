﻿using OONApi.Contracts.Requests.Queires;
using OONApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Services
{
    public interface IAngelTypeService
    {
        Task<List<AngelType>> GetAllAsync(AngelTypesSearchQuery angelType, PaginationFilter paginationFilter = null);
        Task<AngelType> GetByIdAsync(int id);
        Task<bool> CreateAsync(AngelType angelType);
        Task<bool> UpdateAsync(AngelType angelType);
        Task<bool> DeleteAsync(int id);
        Task<List<AngelType>> GetAngelTypeByReport(int? id);
    }
}
