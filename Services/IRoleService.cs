﻿using OONApi.Contracts.Requests.Queires;
using OONApi.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OONApi.Services
{
    public interface IRoleService
    {
        Task<List<Role>> GetAllAsync(RoleSearchQuery role, PaginationFilter paginationFilter = null, bool isRole = false);
        Task<Role> GetByIdAsync(int id);
        Task<bool> CreateAsync(Role role);
        Task<bool> UpdateAsync(Role role);
        Task<bool> DeleteAsync(int id);
        Task<bool> CreateRoleMenu(Role role, List<int> MenuIds, string action);
        //Task<List<int>> GetMenuByRoleId(int id);
    }
}
