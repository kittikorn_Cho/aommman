﻿using OONApi.Contracts.Requests.Queires;
using OONApi.Contracts.Responses;
using OONApi.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OONApi.Services
{
    public interface IAngelPerformanceService
    {
        Task<List<AngelPerformance>> GetAllAsync(AngelPerformanceSearchQuery angelPerformance, PaginationFilter paginationFilter = null);
        Task<AngelPerformance> GetByIdAsync(int id);
        Task<List<MassageAngel>> GetMassageAngelsDetailAsync(int CalendarDayId, int AngelId);
        Task<AngelPerformance> CreateOrUpdateByIdAsync(MassageAngel massageAngel, int calendarDayId);
        Task<bool> UpdateAsync(AngelPerformance angelPerformance);
        //Task<bool> UpdateAngelTypeAsync(int AngelTypeId);
        Task<bool> DeleteAsync(int angelPerformanceId);
        Task<TotalAngelPerformanceResponse> TotalAngelPerformance(int angelId, decimal round);
        Task<bool> CreatePerformanceDebtAsync(List<AngelPerformanceDebt> angelPerformanceDebt);
        Task<bool> UpdateAngelDebtByAngelId(AngelPerformanceDebt angelPerformanceDebt);
        Task<decimal> TotalAngelPerformanceDeduct(int angelPerformanceId);
        Task<List<AngelPerformanceDeduct>> GetAngelPerformanceDeducts(int massageAngelId);
        Task<List<AngelPerformanceDebt>> GetPerformanceDebtByAngelPerformanceIdAsync(int id);
        Task<List<AngelPerformanceDeduct>> GetPerformanceDeductByAngelPerformanceIdAsync(int id);
        decimal SummaryTotalRoundMonthByAngel(int angelId, int calendarId);
        Task<AngelPerformance> GetByCalendarIdAndAngelIdAsync(int calendarId, int angelId);
        Task<List<AngelPerformanceDebt>> GetAngelPerformanceDebtsByAngelPerformanceId(int angelPerformanceId);
        Task<List<MassageAngel>> GetMassageAngelsDetailAsync(int CarlendarDayId, int AngelId, int angelPerfornaceId);
        Task<bool> DeleteAngelPerformanceDebts(int id);
        Task<AngelPerformanceDeduct> GetAngelPerformanceDeductByAngelPerformanceIdAndDeductTypeId(int massageAngelId, int deductTypeId);
        Task<AngelPerformanceDeduct> GetAngelPerformanceDeductById(int id);
        Task<bool> UpdateAngelPerformanceDeduct(AngelPerformanceDeduct angelPerformanceDeduct);
        Task<bool> DeleteAngelPerformance(int id);
        Task<int> GetCountMassageAngel(int angelperformanceId);
    }
}
