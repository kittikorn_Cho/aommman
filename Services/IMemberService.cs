﻿using OONApi.Contracts.Requests.Queires;
using OONApi.Contracts.Responses;
using OONApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Services
{
    public interface IMemberService
    {
        Task<List<Member>> GetAllAsync(MemberSearchQuery member, PaginationFilter paginationFilter = null);
        Task<Member> GetByIdAsync(int id);
        Task<bool> CreateAsync(Member member);
        Task<bool> CreateMemberItem(List<MemberItem> memberItems);
        Task<bool> CreateMemberTransactionAsync(List<MemberTransaction> memberTransactions);
        Task<bool> UpdateAsync(Member member);
        Task<bool> UpdateMemberItemAsync(List<MemberItem> memberItems);
        Task<bool> UpdateMemberItem(MemberItem memberItems);
        Task<bool> UpdateMemberTransactionAsync(MemberTransaction memberTransactions);
        Task<bool> DeleteAsync(int id);
        Task<MemberTransaction> GetMemberTransactionById(int id);
        Task<List<MemberTransaction>> GetMemberTransactions(MemberPaymentSearchQuery member, PaginationFilter paginationFilter = null);
        Task<List<MassageMemberPayment>> GetMemberHistory(int id,MemberHistorySearchQuery memberHistory, PaginationFilter paginationFilter = null);
        MemberSummaryResponse SummaryTotalMember(int calendarDayId);
        Task<List<MassageMemberPayment>> GetMemberPaymentByIsToday(int calendarDayId);
        Task<List<MemberPayment>> GetMemberPaymentByCalendarDayId(int calendarDayId);
        Task<MemberItem> GetMemberItemSuite(int memberId, int itemId);
        Task<MemberItem> GetMemberItemById(int Id);
        Task<bool> CreateMemberPayment(MemberPayment memberPayment);
        Task<bool> DeleteMemberTransaction(int id);
        Task<bool> CreateMemberItemTransactionAsync(MemberItemTransaction memberItemTransaction);
        Task<List<MemberItem>> GetMemberItemByMemberId(int Id);
        Task<List<MemberItemTransaction>> GetmemberItemTransactionByTransactionId(int memberTransactionId);
        Task<bool> UpdateMemberAndMemberItemAsync(Member member);
        Task<bool> CreateMemberTopup(MemberTopup memberTopup);
        Task<List<Member>> GetmemberByExpiredDate(DateTime dateTime);
        Task<Member> GetByCodeAsync(string code);
        Task<List<MemberPayment>> GetMemberpayment(int id, List<int> carlendarIds, PaginationFilter paginationFilter = null);
        Task<List<MemberItemTransaction>> GetMemberItemTransactionByMemberTransactionId(int id);
        Task<List<MemberPayment>> GetMemberPaymentUnpiad(List<int> carlendarIds, PaginationFilter paginationFilter = null);
        Task<MemberPayment> GetMemberPaymentById(int id);
        Task<bool> UpdateMemberPayment(MemberPayment memberPayment);
        Task<bool> CancelMemberPayment(int id);
    }
}
