﻿namespace OONApi.Contracts.Requests
{
    public class MemberTopupRequest
    {
        public int MemberId { get; set; }
        public string Name_1 { get; set; }
        public decimal Amount_1 { get; set; }
        public string Name_2 { get; set; }
        public decimal Amount_2 { get; set; }
        public string Name_3 { get; set; }
        public decimal Amount_3 { get; set; }
        public string Name_4 { get; set; }
        public decimal Amount_4 { get; set; }
        public string Name_5 { get; set; }
        public decimal Amount_5 { get; set; }
        public decimal CreditAmount { get; set; }
    }
}
