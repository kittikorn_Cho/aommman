﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Contracts.Requests
{
    public class ChangePasswordRequest
    { 
        public string Password { get; set; }
    }
}
