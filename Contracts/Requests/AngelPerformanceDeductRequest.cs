﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Contracts.Requests
{
    public class AngelPerformanceDeductRequest
    {
        public int MassageAngelId { get; set; }
        public int DeductTypeId { get; set; }
        public decimal DiscountFee { get; set; }
    }
}
