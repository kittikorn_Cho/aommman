﻿using System.Collections.Generic;

namespace OONApi.Contracts.Requests
{
    public class AngelPerformanceRequest
    {
        public int Id { get; set; }
        public decimal AnotherDeductAmount { get; set; }
        public string AnotherDeductDescription { get; set; }
        public decimal AnotherAddAmount { get; set; }
        public string AnotherAddDescription { get; set; }
        public decimal TotalReceive { get; set; }
        public decimal TotalDailyDebt { get; set; }
        public decimal TotalDebt { get; set; }
        public decimal TotalDeduct { get; set; }
        public decimal TotalRound { get; set; }
        public decimal TotalFee { get; set; }
        public decimal TotalWage { get; set; }

        public List<AngelPerformanceDebtRequest> angelPerformanceDebts { get; set; }
        public List<AngelPerformanceDeductRequest> angelPerformanceDeducts { get; set; }

    }
}
