﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Contracts.Requests
{
    public class UpdateAngelPerformanceRequest
    {
        public int AngelPerformanceId { get; set; }
        public bool IsPay { get; set; }
    }
}
