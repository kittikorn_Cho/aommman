﻿namespace OONApi.Contracts.Requests
{
    public class DeductTypeRequest
    {
        public string Name { get; set; }
        public decimal Fee { get; set; }
        public string Slug { get; set; }
    }
}
