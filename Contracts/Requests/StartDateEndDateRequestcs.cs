﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Contracts.Requests
{
    public class StartDateEndDateRequestcs
    {
        public int? AngelId { get; set; }
        public int? massageId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int CashierId { get; set; }
        public int? AngelTypeId { get; set; }
        public int DropdownId { get; set; }
        public int AgenciesId { get; set; }
        public int? Status { get; set; }
    }
}
