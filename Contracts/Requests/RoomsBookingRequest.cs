﻿using System;
using System.Collections.Generic;
namespace OONApi.Contracts.Requests
{
    public class RoomsBookingRequest
    {
        public int? RoomId { get; set; }
        public string RoomNo { get; set; }
        public bool Flag { get; set; }

        public List<BookingAngelsRequest> BookingAngels { get; set; }

    }
}
