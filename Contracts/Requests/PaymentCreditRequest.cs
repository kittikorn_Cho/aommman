﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Contracts.Requests
{
    public class PaymentCreditRequest
    {
        public string CardNo { get; set; }
        public int? CardType { get; set; }
        public decimal CreditTotal { get; set; }
    }
}
