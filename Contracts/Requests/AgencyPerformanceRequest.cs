﻿using DocumentFormat.OpenXml.Office2010.ExcelAc;
using OONApi.Models;
using System.ComponentModel.DataAnnotations.Schema;

namespace OONApi.Contracts.Requests
{
    public class AgencyPerformanceRequest
    {
        public int CarlendarDayId { get; set; }
        public int AgenciesId { get; set; }
        public bool IsPay { get; set; }
        public bool IsUnPaid { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal TotalMassage { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal TotalRound { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal TotalMember { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal TotalReceive { get; set; }
        public string Remark { get; set; }
    }
}
