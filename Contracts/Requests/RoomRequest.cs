﻿using OONApi.Contracts.Requests.Queires;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Contracts.Requests
{
    public class RoomRequest
    {
        public int Id { get; set; }
        public int RoomTypeId { get; set; }
        public string RoomNo { get; set; }
        public int Status { get; set; }
        public int BuildingTypeId { get; set; }
        public int? ParentRoomId { get; set; }
        public int roomRound { get; set; }
        public DateTime? checkInRoomTime { get; set; }
        public DateTime? checkOutRoomTime { get; set; }

        public List<ChildRoomRequest> ChildRooms { get; set; }
    }
}
