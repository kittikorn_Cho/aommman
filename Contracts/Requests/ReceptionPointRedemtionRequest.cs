﻿namespace OONApi.Contracts.Requests
{
    public class ReceptionPointRedemtionRequest
    {
        public int ReceptionId { get; set; }
        public int CalendarDayId { get; set; }
        public decimal Point { get; set; }
        public decimal PointRedem { get; set; }
        public string Remark { get; set; }
    }
}
