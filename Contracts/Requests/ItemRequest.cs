﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Contracts.Requests
{
    public class ItemRequest
    {
        public string Name { get; set; }
        public string Slug { get; set; }
    }
}
