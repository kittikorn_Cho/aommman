﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Contracts.Requests
{
    public class MemberTypeItemRequest
    {
        public int? Id { get; set; }
        public int ItemId { get; set; }
        public decimal Amount { get; set; }
    }
}
