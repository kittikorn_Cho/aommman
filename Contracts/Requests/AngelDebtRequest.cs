﻿namespace OONApi.Contracts.Requests
{
    public class AngelDebtRequest
    {
        public int? Id { get; set; }
        public int DebtTypeId { get; set; }
        public decimal Fee { get; set; }
    }
}
