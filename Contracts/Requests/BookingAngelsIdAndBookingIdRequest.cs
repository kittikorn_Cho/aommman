﻿using System;

namespace OONApi.Contracts.Requests
{
    public class BookingAngelsIdAndBookingIdRequest
    {
        public int BookingId { get; set; }
        public int AngelId { get; set; }
        public DateTime? StartDate { get; set; }
        public TimeSpan? StartTime { get; set; }
        public TimeSpan? EndTime { get; set; }
    }
}
