﻿using System.Collections.Generic;

namespace OONApi.Contracts.Requests
{
    public class RoleRequest
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public List<int> MenuIds { get; set; }
    }
}
