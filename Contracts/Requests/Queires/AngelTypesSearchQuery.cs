﻿namespace OONApi.Contracts.Requests.Queires
{
    public class AngelTypesSearchQuery
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string OrderBy { get; set; }
        public string OrderDirection { get; set; }
        public string Keyword { get; set; }
        public string Code { get; set; }
        public decimal Fee { get; set; }
        public decimal Wage { get; set; }
        public decimal CreditComm { get; set; }
        public decimal CheerComm { get; set; }
        public int RoundTime { get; set; }
        public int Order { get; set; }

        public AngelTypesSearchQuery()
        {
            PageNumber = 1;
            PageSize = 10;
            OrderBy = "Id";
            OrderDirection = "ASC";
            Keyword = "";
            Code = "";
            Fee = 0;
            Wage = 0;
            CreditComm = 0;
            CheerComm = 0;
            RoundTime = 0;
            RoundTime = 0;
            Order = 0;

        }
        public AngelTypesSearchQuery(int pageNumber, int pageSize, string orderBy, string orderDirection, string keyword, string code, decimal fee, decimal wage, decimal creditComm, decimal cheerComm, int roundTime, int order)
        {
            PageNumber = pageNumber;
            PageSize = pageSize;
            OrderBy = orderBy;
            OrderDirection = orderDirection;
            Keyword = keyword;
            Code = code;
            Fee = fee;
            Wage = wage;
            CreditComm = creditComm;
            CheerComm= cheerComm;
            RoundTime= roundTime;
            Order = order;
        }
    }
}
