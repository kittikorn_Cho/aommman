﻿namespace OONApi.Contracts.Requests.Queires
{
    public class DeductTypeSearchQuery
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string OrderBy { get; set; }
        public string OrderDirection { get; set; }
        public string Keyword { get; set; }
        public string Name { get; set; }

        public DeductTypeSearchQuery()
        {
            PageNumber = 1;
            PageSize = 10;
            OrderBy = "Id";
            OrderDirection = "ASC";
            Keyword = "";
            Name = "";
        }

        public DeductTypeSearchQuery(int pageNumber, int pageSize, string orderBy, string orderDirection, string keyword, string name)
        {
            PageNumber = pageNumber;
            PageSize = pageSize;
            OrderBy = orderBy;
            OrderDirection = orderDirection;
            Keyword = keyword;
            Name = name;
        }
    }
}
