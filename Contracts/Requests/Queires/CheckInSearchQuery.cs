﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Contracts.Requests.Queires
{
    public class CheckInSearchQuery
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string OrderBy { get; set; }
        public string OrderDirection { get; set; }
        public string Keyword { get; set; }
        public int? AngelTypeId { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Nickname { get; set; }
        public string AngelCode { get; set; }
        public string RoomName { get; set; }
        public int? ReceptionId { get; set; }
        public bool IsStatusWaitingPaid { get; set; } = false;
        public int? RoomTypeId { get; set; }
        public string RfId { get; set; }

        public CheckInSearchQuery()
        {
            PageNumber = 1;
            PageSize = 10;
            OrderBy = "Id";
            OrderDirection = "ASC";
            Keyword = "";
            AngelTypeId = null;
            Firstname = "";
            Lastname = "";
            Nickname = "";
            AngelCode = "";
            RoomName = "";
            ReceptionId = null;
            IsStatusWaitingPaid = false;
            RoomTypeId = null;
            RfId = "";
        }

        public CheckInSearchQuery(int pageNumber, int pageSize, string orderBy, string orderDirection, string keyword, int? angelTypeId, string firstName, string lastName, string nickName, string angelCode,string roomName,int? receptionId,bool isStatusWaitingPaid,int? roomTypeId,string rfId)
        {
            PageNumber = pageNumber;
            PageSize = pageSize;
            OrderBy = orderBy;
            OrderDirection = orderDirection;
            Keyword = keyword;
            AngelTypeId = angelTypeId;
            Firstname = firstName;
            Lastname = lastName;
            Nickname = nickName;
            AngelCode = angelCode;
            RoomName = roomName;
            ReceptionId = receptionId;
            IsStatusWaitingPaid = isStatusWaitingPaid;
            RoomTypeId = roomTypeId;
            RfId = rfId;
        }
    }
}
