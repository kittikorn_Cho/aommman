﻿namespace OONApi.Contracts.Requests.Queires
{
    public class UserSearchQuery
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string OrderBy { get; set; }
        public string OrderDirection { get; set; }
        public string Keyword { get; set; }
        public string Username { get; set; }
        public string UserFullname { get; set; }
        public string Tel { get; set; }
        public int Status { get; set; }

        public UserSearchQuery()
        {
            PageNumber = 1;
            PageSize = 10;
            OrderBy = "Id";
            OrderDirection = "ASC";
            Keyword = "";
            Username = "";
            UserFullname = "";
            Tel = "";
            Status = 0;
        }

        public UserSearchQuery(int pageNumber, int pageSize, string orderBy, string orderDirection, string keyword, string username, string userFullname, string tel, int status)
        {
            PageNumber = pageNumber;
            PageSize = pageSize;
            OrderBy = orderBy;
            OrderDirection = orderDirection;
            Keyword = keyword;
            Username = username;
            UserFullname = userFullname;
            Tel = tel;
            Status = status;
        }
    }
}
