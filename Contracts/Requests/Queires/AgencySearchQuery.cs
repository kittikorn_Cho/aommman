﻿using System;
namespace OONApi.Contracts.Requests.Queires
{
    public class AgencySearchQuery
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string OrderBy { get; set; }
        public string OrderDirection { get; set; }
        public string Keyword { get; set; }
        public int? ReceptionTypeId { get; set; }
        public string Code { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Nickname { get; set; }
        public string Tel { get; set; }
        public int Id { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string DocumentNumber { get; set; }

        public AgencySearchQuery()
        {
            PageNumber = 1;
            PageSize = 10;
            OrderBy = "Id";
            OrderDirection = "ASC";
            Keyword = "";
            ReceptionTypeId = null;
            Code = "";
            Firstname = "";
            Lastname = "";
            Nickname = "";
            Tel = "";
            Id = 0;
            StartDate = DateTime.Now;
            EndDate = DateTime.Now;
            DocumentNumber = "";
        }

        public AgencySearchQuery(int pageNumber, int pageSize, string orderBy, string orderDirection, string keyword, int? receptionTypeId, string code, string firstName, string lastName, string nickName, string tel, int id, DateTime startDate, DateTime endDate, string documentNumber)
        {
            PageNumber = pageNumber;
            PageSize = pageSize;
            OrderBy = orderBy;
            OrderDirection = orderDirection;
            Keyword = keyword;
            ReceptionTypeId = receptionTypeId;
            Code = code;
            Firstname = firstName;
            Lastname = lastName;
            Nickname = nickName;
            Tel = tel;
            Id = id;
            StartDate = startDate;
            EndDate = endDate;
            DocumentNumber = documentNumber;
        }
    }
}
