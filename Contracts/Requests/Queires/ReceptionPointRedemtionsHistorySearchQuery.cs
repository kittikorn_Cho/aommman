﻿using System;

namespace OONApi.Contracts.Requests.Queires
{
    public class ReceptionPointRedemtionsHistorySearchQuery
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string OrderBy { get; set; }
        public string OrderDirection { get; set; }
        public string Keyword { get; set; }
        public int ReceptionId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        public ReceptionPointRedemtionsHistorySearchQuery()
        {
            PageNumber = 1;
            PageSize = 20;
            OrderBy = "Id";
            OrderDirection = "ASC";
            Keyword = "";
            StartDate = null;
            EndDate = null;
        }
        public ReceptionPointRedemtionsHistorySearchQuery(int pageNumber, int pageSize, string orderBy, string orderDirection, string keyword, DateTime? startDate, DateTime? endDate)
        {
            PageNumber = pageNumber;
            PageSize = pageSize;
            OrderBy = orderBy;
            OrderDirection = orderDirection;
            Keyword = keyword;
            StartDate = startDate;
            EndDate = endDate;
        }
    }
}
