﻿namespace OONApi.Contracts.Requests.Queires
{
    public class ChildRoomRequest
    {
        public int? Id { get; set; }
        public int? ParentRoomId { get; set; }
        public string RoomNo { get; set; }
        public int Status { get; set; }
    }
}
