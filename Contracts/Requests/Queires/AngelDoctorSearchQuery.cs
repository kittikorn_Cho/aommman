﻿namespace OONApi.Contracts.Requests.Queires
{
    public class AngelDoctorSearchQuery
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string OrderBy { get; set; }
        public string OrderDirection { get; set; }
        public string Keyword { get; set; }
        public int AngelId { get; set; }
        public string DoctorName { get; set; }
        public string Detail { get; set; }

        public AngelDoctorSearchQuery()
        {
            PageNumber = 1;
            PageSize = 20;
            OrderBy = "Id";
            OrderDirection = "ASC";
            Keyword = "";
            AngelId = 0;
            DoctorName = "";
            Detail = "";
        }

        public AngelDoctorSearchQuery(int pageNumber, int pageSize, string orderBy, string orderDirection, string keyword, int angelId, string doctorName, string detail)
        {
            PageNumber = pageNumber;
            PageSize = pageSize;
            OrderBy = orderBy;
            OrderDirection = orderDirection;
            Keyword = keyword;
            AngelId = angelId;
            DoctorName = doctorName;
            Detail = detail;
        }
    }
}
