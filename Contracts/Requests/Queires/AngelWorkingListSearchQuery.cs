﻿using System;

namespace OONApi.Contracts.Requests.Queires
{
    public class AngelWorkingListSearchQuery
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string OrderBy { get; set; }
        public string OrderDirection { get; set; }
        public string Keyword { get; set; }
        public DateTime? CalendarDay { get; set; }

        public AngelWorkingListSearchQuery()
        {
            PageNumber = 1;
            PageSize = 10;
            OrderBy = "Id";
            OrderDirection = "ASC";
            Keyword = "";
            CalendarDay = null;
        }

        public AngelWorkingListSearchQuery(int pageNumber, int pageSize, string orderBy, string orderDirection, string keyword, DateTime? calendarDay)
        {
            PageNumber = pageNumber;
            PageSize = pageSize;
            OrderBy = orderBy;
            OrderDirection = orderDirection;
            Keyword = keyword;
            CalendarDay = calendarDay;
        }
    }
}
