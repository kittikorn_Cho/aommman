﻿namespace OONApi.Contracts.Requests.Queires
{
    public class RoomSearchQuery
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string OrderBy { get; set; }
        public string OrderDirection { get; set; }
        public string Keyword { get; set; }
        public int RoomTypeId { get; set; }
        public string RoomNo { get; set; }
        public int BuildingTypeId { get; set; }

        public RoomSearchQuery()
        {
            PageNumber = 1;
            PageSize = 10;
            OrderBy = "Id";
            OrderDirection = "ASC";
            Keyword = "";
            RoomTypeId = 0;
            RoomNo = "";
            BuildingTypeId = 0;
        }
        public RoomSearchQuery(int pageNumber, int pageSize, string orderBy, string orderDirection, string keyword, int roomTypeId, string roomNo, int buildingTypeId)
        {
            PageNumber = pageNumber;
            PageSize = pageSize;
            OrderBy = orderBy;
            OrderDirection = orderDirection;
            Keyword = keyword;
            RoomTypeId = roomTypeId;
            RoomNo = roomNo;
            BuildingTypeId = buildingTypeId;
        }
    }
}
