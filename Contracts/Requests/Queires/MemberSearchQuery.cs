﻿namespace OONApi.Contracts.Requests.Queires
{
    public class MemberSearchQuery
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string OrderBy { get; set; }
        public string OrderDirection { get; set; }
        public string Keyword { get; set; }
        public string Code { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Tel { get; set; }
        public int ReceptionId { get; set; }
        public int Status { get; set; }

        public MemberSearchQuery()
        {
            PageNumber = 1;
            PageSize = 10;
            OrderBy = "Id";
            OrderDirection = "ASC";
            Keyword = "";
            Code = "";
            Firstname = "";
            Lastname = "";
            Tel = "";
            ReceptionId = 0;
            Status = 0;

        }
        public MemberSearchQuery(int pageNumber, int pageSize, string orderBy, string orderDirection, string keyword, string code, string firstName, string lastName, string tel,int receptionId, int status  )
        {
            PageNumber = pageNumber;
            PageSize = pageSize;
            OrderBy = orderBy;
            OrderDirection = orderDirection;
            Keyword = keyword;
            Code = code;
            Firstname = firstName;
            Lastname = lastName;
            Tel = tel;
            ReceptionId = receptionId;
            Status = status;
            
        }
    }
}
