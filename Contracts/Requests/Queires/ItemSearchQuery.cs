﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Contracts.Requests.Queires
{
    public class ItemSearchQuery
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string OrderBy { get; set; }
        public string OrderDirection { get; set; }
        public string Keyword { get; set; }
        public string Slug { get; set; }
        public string Name { get; set; }

        public ItemSearchQuery()
        {
            PageNumber = 1;
            PageSize = 10;
            OrderBy = "Id";
            OrderDirection = "ASC";
            Keyword = "";
            Slug = "";
            Name = "";
        }

        public ItemSearchQuery(int pageNumber, int pageSize, string orderBy, string orderDirection, string keyword, string slug,string name)
        {
            PageNumber = pageNumber;
            PageSize = pageSize;
            OrderBy = orderBy;
            OrderDirection = orderDirection;
            Keyword = keyword;
            Slug = slug;
            Name = name;
        }
    }
}
