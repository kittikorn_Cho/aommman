﻿using System;

namespace OONApi.Contracts.Requests.Queires
{
    public class WagePaymentCancelSearchQuery
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string OrderBy { get; set; }
        public string OrderDirection { get; set; }
        public string Keyword { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string CodeAngel {get; set;}
        public string RoomNo {get; set;}
        public int? ReceptionId { get; set; }
        public string MemberName {get; set;}
        public string MemberLastName {get; set;}
        public string DocumentNumber { get; set; }

        public WagePaymentCancelSearchQuery()
        {
            PageNumber = 1;
            PageSize = 9999;
            OrderBy = "Id";
            OrderDirection = "ASC";
            Keyword = "";
            StartDate = null;
            EndDate = null;
            CodeAngel = "";
            RoomNo = "";
            ReceptionId = null;
            MemberName = "";
            MemberLastName = "";
            DocumentNumber = "";
        }

        public WagePaymentCancelSearchQuery( int pageNumber, int pageSize, string orderBy, string orderDirection, string keyword, DateTime? startDate, DateTime? endDate, string codeAngel, string roomNo, int receptionId, string memberName, string memberLastName, string documentNumber)
        {
            PageNumber = pageNumber;
            PageSize = pageSize;
            OrderBy = orderBy;
            OrderDirection = orderDirection;
            Keyword = keyword;
            StartDate = startDate;
            EndDate = endDate;
            CodeAngel = codeAngel;
            RoomNo = roomNo;
            ReceptionId = receptionId;
            MemberName = memberName;
            MemberLastName = memberLastName;
            DocumentNumber = documentNumber;
        }
    }
}
