﻿namespace OONApi.Contracts.Requests.Queires
{
    public class RoleSearchQuery
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string OrderBy { get; set; }
        public string OrderDirection { get; set; }
        public string Keyword { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }

        public RoleSearchQuery()
        {
            PageNumber = 1;
            PageSize = 10;
            OrderBy = "Id";
            OrderDirection = "ASC";
            Keyword = "";
            Code = "";
            Name = "";
        }

        public RoleSearchQuery(int pageNumber, int pageSize, string orderBy, string orderDirection, string keyword, string code, string name)
        {
            PageNumber = pageNumber;
            PageSize = pageSize;
            OrderBy = orderBy;
            OrderDirection = orderDirection;
            Keyword = keyword;
            Code = code;
            Name = name;
        }
    }
}
