﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Contracts.Requests.Queires
{
    public class MemberPaymentSearchQuery
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string OrderBy { get; set; }
        public string OrderDirection { get; set; }
        public string Keyword { get; set; }
        public string Code { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int ReceptionId { get; set; }
        public int Status { get; set; }

        public MemberPaymentSearchQuery()
        {
            PageNumber = 1;
            PageSize = 10;
            OrderBy = "Id";
            OrderDirection = "ASC";
            Keyword = "";
            Code = "";
            StartDate = null;
            EndDate = null;
            ReceptionId = 0;
            Status = 0;

        }
        public MemberPaymentSearchQuery(int pageNumber, int pageSize, string orderBy, string orderDirection, string keyword, string code, DateTime? startDate, DateTime? endDate, int receptionId, int status)
        {
            PageNumber = pageNumber;
            PageSize = pageSize;
            OrderBy = orderBy;
            OrderDirection = orderDirection;
            Keyword = keyword;
            Code = code;
            StartDate = startDate;
            EndDate = endDate;
            ReceptionId = receptionId;
            Status = status;

        }
    }
}
