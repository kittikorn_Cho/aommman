﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Contracts.Requests.Queires
{
    public class AngelSearchQuery
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string OrderBy { get; set; }
        public string OrderDirection { get; set; }
        public string Keyword { get; set; }
        public int? AngelTypeId { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Nickname { get; set; }
        public string Tel { get; set; }
        public int? Status { get; set; }
        public string Code { get; set; }
        public string Rfid { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? Id { get; set; }

        public AngelSearchQuery()
        {
            PageNumber = 1;
            PageSize = 10;
            OrderBy = "Id";
            OrderDirection = "ASC";
            Keyword = "";
            AngelTypeId = null;
            Firstname = "";
            Lastname = "";
            Nickname = "";
            Tel = "";
            Status = null;
            Code = "";
            StartDate = null;
            EndDate = null;
            Id = null;
            Rfid = "";
        }
        public AngelSearchQuery(int pageNumber,int pageSize,string orderBy,string orderDirection,string keyword,int? angelTypeId,string firstName,string lastName,string nickName,string tel,int? status,string code,DateTime? startDate,DateTime? endDate,int? id, string rfid)
        {
            PageNumber = pageNumber;
            PageSize = pageSize;
            OrderBy = orderBy;
            OrderDirection = orderDirection;
            Keyword = keyword;
            AngelTypeId = angelTypeId;
            Firstname = firstName;
            Lastname = lastName;
            Nickname = nickName;
            Tel = tel;
            Status = status;
            Code = code;
            StartDate = startDate;
            EndDate = endDate;
            Id = id;
            Rfid = rfid;
        }
    }
}
