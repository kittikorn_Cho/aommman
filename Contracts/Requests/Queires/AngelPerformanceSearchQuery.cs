﻿using System;

namespace OONApi.Contracts.Requests.Queires
{
    public class AngelPerformanceSearchQuery
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string OrderBy { get; set; }
        public string OrderDirection { get; set; }
        public string Keyword { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int AngelId { get; set; }
        public bool? IsPay { get; set; }
        public string RfId { get; set; }

        public AngelPerformanceSearchQuery()
        {
            PageNumber = 1;
            PageSize = 20;
            OrderBy = "Id";
            OrderDirection = "ASC";
            Keyword = "";
            StartDate = null;
            AngelId = 0;
            IsPay = null;
            RfId = "";
            EndDate = null;
        }
        public AngelPerformanceSearchQuery(int pageNumber, int pageSize, string orderBy, string orderDirection, string keyword, DateTime? startDate, int angelId, bool isPay, string rfid,DateTime? endDate)
        {
            PageNumber = pageNumber;
            PageSize = pageSize;
            OrderBy = orderBy;
            OrderDirection = orderDirection;
            Keyword = keyword;
            StartDate = startDate;
            AngelId = angelId;
            IsPay = isPay;
            RfId = rfid;
            EndDate = endDate;
        }
    }
}
