﻿namespace OONApi.Contracts.Requests.Queires
{
    public class DailyDebtSearchQuery
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string OrderBy { get; set; }
        public string OrderDirection { get; set; }
        public string Keyword { get; set; }
        public string Name { get; set; }
        public string Fee { get; set; }

        public DailyDebtSearchQuery()
        {
            PageNumber = 1;
            PageSize = 10;
            OrderBy = "Id";
            OrderDirection = "ASC";
            Keyword = "";
            Name = "";
            Fee = null;
        }
        public DailyDebtSearchQuery(int pageNumber, int pageSize, string orderBy, string orderDirection, string keyword, string name, string fee)
        {
            PageNumber = pageNumber;
            PageSize = pageSize;
            OrderBy = orderBy;
            OrderDirection = orderDirection;
            Keyword = keyword;
            Name = name;
            Fee = fee;
        }
    }
}
