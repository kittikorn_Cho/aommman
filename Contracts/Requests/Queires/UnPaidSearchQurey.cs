﻿using System;

namespace OONApi.Contracts.Requests.Queires
{
    public class UnPaidSearchQurey
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string OrderBy { get; set; }
        public string OrderDirection { get; set; }
        public string Keyword { get; set; }
        public DateTime? Startdate { get; set; }
        public DateTime? Enddate { get; set; }
        public int? ReceptionId { get; set; }
        public string DocumentNumber { get; set; }

        public UnPaidSearchQurey()
        {
            PageNumber = 1;
            PageSize = 10;
            OrderBy = "Id";
            OrderDirection = "ASC";
            Keyword = "";
            Startdate = null;
            Enddate = null;
            ReceptionId = null;
            DocumentNumber = "";
        }

        public UnPaidSearchQurey(int pageNumber, int pageSize, string orderBy, string orderDirection, string keyword, DateTime? startdate, DateTime? enddate, int receptionid, string documentNumber)
        {
            PageNumber = pageNumber;
            PageSize = pageSize;
            OrderBy = orderBy;
            OrderDirection = orderDirection;
            Keyword = keyword;
            Startdate = startdate;
            Enddate = enddate;
            ReceptionId = receptionid;
            DocumentNumber = documentNumber;

        }
    }
}
