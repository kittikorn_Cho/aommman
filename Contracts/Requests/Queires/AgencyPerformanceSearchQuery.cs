﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace OONApi.Contracts.Requests.Queires
{
    public class AgencyPerformanceSearchQuery
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string OrderBy { get; set; }
        public string OrderDirection { get; set; }
        public string Keyword { get; set; }
        public int? AgenciesId { get; set; }
        public bool? IsPay { get; set; }
        public bool? IsUnPaid { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        public AgencyPerformanceSearchQuery()
        {
            PageNumber = 1;
            PageSize = 20;
            OrderBy = "Id";
            OrderDirection = "ASC";
            Keyword = "";
            AgenciesId = null;
            IsPay = null;
            IsUnPaid = null;
            StartDate = null;
            EndDate = null;
        }
        public AgencyPerformanceSearchQuery(int pageNumber, int pageSize, string orderBy, string orderDirection, string keyword,int agenciesId, bool isPay,bool isUnpaid, DateTime? startDate, DateTime? endDate) 
        {
            PageNumber = pageNumber;
            PageSize = pageSize;
            OrderBy = orderBy;
            OrderDirection = orderDirection;
            Keyword = keyword;
            AgenciesId = agenciesId;
            IsPay = isPay;
            IsUnPaid = isUnpaid;
            StartDate = startDate;
            EndDate = endDate;
        }
    }
}
