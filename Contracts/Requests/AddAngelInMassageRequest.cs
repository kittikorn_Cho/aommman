﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Contracts.Requests
{
    public class AddAngelInMassageRequest
    {
        public int MassageId { get; set; }
        public int AngelId { get; set; }
        public int RoomId { get; set; }
        public int ReceptionId { get; set; }
    }
}
