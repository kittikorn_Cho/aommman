﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Contracts.Requests
{
    public class PaymentMemberRequest
    {
        public int? MemberId { get; set; }
        public decimal MemberTotal { get; set; }
        public decimal UseSuite { get; set; }
        public decimal UseVip { get; set; }
        public decimal MemberFoodTotal { get; set; }
        public decimal MemberMassageTotal { get; set; }
        public decimal MemberRoomTotal { get; set; }
        public int? ItemId { get; set; }
    }
}
