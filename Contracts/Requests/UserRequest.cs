﻿using System.Collections.Generic;

namespace OONApi.Contracts.Requests
{
    public class UserRequest
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string UserFullname { get; set; }
        public string Tel { get; set; }
        public int Status { get; set; }

        public List<int> RoleIds { get; set; }
    }
}
