﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Contracts.Requests
{
    public class MassageAngelRequest
    {
        public int Id { get; set; }
        public int MassageId { get; set; }
        public int RoomId { get; set; }
        public int ReceptionId { get; set; }
        public int AngelId { get; set; }
        public DateTime? CheckInTime { get; set; }
        public DateTime? CheckOutTime { get; set; }
        public DateTime? FirstCheckInTime { get; set; }
        public int TimeMinute { get; set; }
        public decimal Round { get; set; }
        public bool IsNotCall { get; set; }
        public bool IsDiscount { get; set; }
        public bool IsDiscountBaht { get; set; }
        public decimal DiscountBaht { get; set; }
        public bool IsDiscountPercent { get; set; }
        public decimal DiscountPercent { get; set; }
        public bool IsDiscountAngelRound { get; set; }
        public decimal DiscountAngelRound { get; set; }
        public string DiscountRemark { get; set; }

        public int roomRound { get; set; }
        public DateTime? checkInRoomTime { get; set; }
        public DateTime? checkOutRoomTime { get; set; }

    }
}
