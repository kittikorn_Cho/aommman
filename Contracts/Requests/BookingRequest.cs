﻿using System;
using System.Collections.Generic;

namespace OONApi.Contracts.Requests
{
    public class BookingRequest
    {

        public DateTime? StartDate { get; set; }
        public TimeSpan? StartTime { get; set; }
        public TimeSpan? EndTime { get; set; }
        public string Tel { get; set; }
        public string Remark { get; set; }
        public int? RoomId { get; set; }

        public List<RoomsBookingRequest> Rooms { get; set; }
        public List<BookingAngelsIdAndBookingIdRequest> AngelBookings { get; set; }

    }
}
