﻿namespace OONApi.Contracts.Requests
{
    public class MassageDebtRequest
    {
        public int MassageId { get; set; }
        public decimal DebtTotal { get; set; }
        public decimal CashTotal { get; set; }
        public decimal CreditTotal { get; set; }
        public decimal QrTotal { get; set; }
        public decimal MemberTotal { get; set; }
        public decimal EntertainTotal { get; set; }
        public bool isMassageCheck { get; set; }

    }
}
