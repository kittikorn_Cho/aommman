﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Contracts.Requests
{
    public class UpdateMassageAngelRequest
    {
        public int? MassageId { get; set; }
        public int? MassageAngelId { get; set; }
        public int? MassageRoomId { get; set; }
        public int? RoomId { get; set; }
        public int? AngelId { get; set; }
        public int? ReceptionId { get; set; }
        public int? parentRoomId { get; set; }
        public decimal Round { get; set; } = 0;
        public decimal AddRound { get; set; } = 0;
        public bool IsNotCall { get; set; } = false;
        public DateTime? CheckInTime { get; set; }
        public bool IsRoom { get; set; } = false;
        public bool IsAngel { get; set; } = false;
        public bool IsMinute { get; set; } = false;
        public bool IsChangeRoom { get; set; } = false;
        public List<MassageAngelRequest> MassageAngels { get; set; }

    }
}
