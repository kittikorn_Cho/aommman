﻿namespace OONApi.Contracts.Requests
{
    public class AngelDoctorRequest
    {
        public int AngelId { get; set; }
        public string DoctorName { get; set; }
        public string Detail { get; set; }
    }
}
