﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Contracts.Requests
{
    public class ManageWorkingRequest
    {
        public int AngelId { get; set; }
        public int? Status { get; set; }
    }
}
