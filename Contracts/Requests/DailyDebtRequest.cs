﻿namespace OONApi.Contracts.Requests
{
    public class DailyDebtRequest
    {
        public string Name { get; set; }
        public decimal Fee { get; set; }
    }
}
