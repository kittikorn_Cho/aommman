﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Contracts.Requests
{
    public class MemberTypeRequest
    {
        public string Code { get; set; }
        public decimal Fee { get; set; }

        public List<MemberTypeItemRequest> MemberTypeItems { get; set; }
    }
}
