﻿using System;
using System.Collections.Generic;

namespace OONApi.Contracts.Requests
{
    public class MemberTransactionsRequest
    {
        public int? id { get; set; }
        public bool IsPaid { get; set; }
        public bool IsUnPaid { get; set; }
        public decimal Total { get; set; }
        public bool IsCash { get; set; }
        public bool IsCredit { get; set; }
        public bool IsEntertain { get; set; }
        public decimal CashTotal { get; set; }
        public decimal CreditTotal { get; set; }
        public decimal EntertainTotal { get; set; }
        public DateTime? PayDate { get; set; }
        public bool IsQrCode { get; set; }
        public decimal QrCodeTotal { get; set; }
        public bool IsPercent { get; set; }
        public decimal PercentTopup { get; set; }
        public decimal PercentTotalTopup { get; set; }

        public MemberUnpaidTotalRequest MemberUnpaidTotal { get; set; }
    }
}
