﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Contracts.Requests
{
    public class MemberPointRedemtionRequest
    {
        public int MemberId { get; set; }
        public int CalendarDayId { get; set; }
        public decimal Point { get; set; }
        public decimal PointRedem { get; set; }
        public string Remark { get; set; }
    }
}
