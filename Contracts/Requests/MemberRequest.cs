﻿using System;
using System.Collections.Generic;

namespace OONApi.Contracts.Requests
{
    public class MemberRequest
    {
        public string Code { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Tel { get; set; }
        public decimal CreditAmount { get; set; }
        public int ReceptionId { get; set; }
        public DateTime? ExpiredDate { get; set; }
        public int Status { get; set; }
        public DateTime? FromDate { get; set; }
        public int CountriesId { get; set; }
        public int AgenciesId { get; set; }
        public decimal Point { get; set; }
 

        public List<MemberItemRequest> MemberItems { get; set; }
        public List<MemberTransactionsRequest> MemberTransactions { get; set; }
        public List<MemberItemRequest> MemberItemTopups { get; set; }
        public List<int> Ids { get; set; }
    }
}
