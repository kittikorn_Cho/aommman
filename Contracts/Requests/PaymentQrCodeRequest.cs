﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Contracts.Requests
{
    public class PaymentQrCodeRequest
    {
        public decimal QrCodeTotal { get; set; }
    }
}
