﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Contracts.Requests
{
    public class MassageRequest
    {

        public int CalendarDayId { get; set; }
        public bool IsPaid { get; set; }
        public bool IsUnPaid { get; set; }
        public decimal Total { get; set; }
        public decimal AngelTotal { get; set; }
        public decimal FoodTotal { get; set; }
        public decimal RoomTotal { get; set; }
        public bool IsCashAngel { get; set; }
        public bool IsCreditAngel { get; set; }
        public bool IsMemberAngel { get; set; }
        public decimal CashAngelTotal { get; set; }
        public decimal CreditAngelTotal { get; set; }
        public decimal MemberAngelTotal { get; set; }
        public bool IsCashFood { get; set; }
        public bool IsCreditFood { get; set; }
        public bool IsMemberFood { get; set; }
        public decimal CashFoodTotal { get; set; }
        public decimal CreditFoodTotal { get; set; }
        public decimal MemberFoodTotal { get; set; }
        public bool IsCashRoom { get; set; }
        public bool IsCreditRoom { get; set; }
        public bool IsMemberRoom { get; set; }
        public decimal CashRoomTotal { get; set; }
        public decimal CreditRoomTotal { get; set; }
        public decimal MemberRoomTotal { get; set; }
        public bool IsEntertainAngel { get; set; }
        public decimal EntertainTotalAngel { get; set; }
        public string EntertainRemarkAngel { get; set; }
        public bool IsEntertainFood { get; set; }
        public decimal EntertainTotalFood { get; set; }
        public string EntertainRemarkFood { get; set; }
        public DateTime? PayDate { get; set; }
        public int? RoomId { get; set; }
        public int? ReceptionId { get; set; }
        public bool IsNotCall { get; set; }
        public bool IsSuite { get; set; }
        public int CountriesId { get; set; }
        public int AgenciesId { get; set; }
        public bool IsCheckInLater { get; set; }

        public List<MassageAngelRequest> MassageAngels { get; set; }
        public List<RoomRequest> Rooms { get; set; }
    }
}
