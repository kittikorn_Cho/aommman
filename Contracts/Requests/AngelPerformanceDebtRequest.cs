﻿using System.Collections.Generic;

namespace OONApi.Contracts.Requests
{
    public class AngelPerformanceDebtRequest
    {
        public int DebtTypeId { get; set; }
        public int AngelPerformanceId { get; set; }
        public decimal Fee { get; set; }
        public decimal DebtNow { get; set; }
    }
}
