﻿namespace OONApi.Contracts.Requests
{
    public class ReceptionTypeRequest
    {
        public string Code { get; set; }
        public string Name { get; set; }
    }
}
