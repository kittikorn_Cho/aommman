﻿namespace OONApi.Contracts.Requests
{
    public class MemberUnpaidTotalRequest
    {
        public decimal CashTotal { get; set; }
        public decimal QrCodeTotal { get; set; }
        public decimal CreditTotal { get; set; }
        public decimal EnterTainTotal { get; set; }
    }
}
