﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Contracts.Requests
{
    public class SettingRequest
    {
        public string MassageName { get; set; }
        public string MassagePath { get; set; }
        public decimal PointAngel { get; set; }
        public decimal PointMember { get; set; }
        public decimal PointReception { get; set; }
    }
}
