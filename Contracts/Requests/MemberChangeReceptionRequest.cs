﻿namespace OONApi.Contracts.Requests
{
    public class MemberChangeReceptionRequest
    {
        public int Id { get; set; }
        public int ReceptionId { get; set; } 
    }
}
