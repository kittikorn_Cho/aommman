﻿namespace OONApi.Contracts.Responses
{
    public class MassageMemberPaymentResponse
    {
        public int Id { get; set; }
        public int MassageId { get; set; }
        public int MemberId { get; set; }
        public decimal MemberTotal { get; set; }
        public decimal UseSuite { get; set; }
        public decimal UseVip { get; set; }
        public decimal MemberFoodTotal { get; set; }
        public decimal MemberMassageTotal { get; set; }
        public decimal MemberRoomTotal { get; set; }

        public MemberResponse member { get; set; }
        public MassageResponse Massage { get; set; }
    }
}
