﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Contracts.Responses
{
    public class ReportMemberSellResponse
    {
        public int Id { get; set; }
        public int CalendarDayId { get; set; }
        public int MemberId { get; set; }
        public int ReceptionId { get; set; }
        public bool IsPaid { get; set; }
        public bool IsUnPaid { get; set; }
        public decimal Total { get; set; }
        public bool IsCash { get; set; }
        public bool IsCredit { get; set; }
        public bool IsEntertain { get; set; }
        public decimal CashTotal { get; set; }
        public decimal CreditTotal { get; set; }
        public decimal EntertainTotal { get; set; }
        public DateTime? PayDate { get; set; }
        public int? CreatedBy { get; set; } = null!;
        public bool IsQrCode { get; set; }
        public decimal QrCodeTotal { get; set; }
        public bool IsPercent { get; set; }
        public decimal PercentTopup { get; set; }
        public decimal PercentTotalTopup { get; set; }

        public MemberResponse Member { get; set; }
        public ReceptionResponse Reception { get; set; }
        public CalendarDayResponse CalendarDay { get; set; }
        public UserResponse User { get; set; }
    }
}
