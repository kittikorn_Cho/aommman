﻿namespace OONApi.Contracts.Responses
{
    public class MemberItemTransactionResponse
    {
        public int Id { get; set; }
        public int MemberId { get; set; }
        public int ItemId { get; set; }
        public int MemberTransactionId { get; set; }
        public decimal Amount { get; set; }
        public decimal Point { get; set; }

        public MemberResponse Member { get; set; }
        public MemberTransactionResponse MemberTransaction { get; set; }
        public ItemResponse Item { get; set; }
    }
}
