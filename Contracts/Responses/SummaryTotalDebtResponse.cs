﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Contracts.Responses
{
    public class SummaryTotalDebtResponse
    {
        public decimal TotalDeduct { get; set; } = 0;
        public decimal TotalDebt { get; set; } = 0;
        public decimal TotalDailyDebt { get; set; } = 0;
        public decimal TotalSummaryDebt { get; set; } = 0;
        public decimal TotalTip { get; set; } = 0;
        public decimal DamagesTotal { get; set; } = 0;
        public decimal TipQrCodeTotal { get; set; } = 0;
    }
}
