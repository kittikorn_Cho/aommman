﻿namespace OONApi.Contracts.Responses
{
    public class AgencyTypeResponse
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal Fee { get; set; }
        public int Order { get; set; }
    }
}
