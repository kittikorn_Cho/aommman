﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Contracts.Responses
{
    public class RoomParentResponse
    {
        public int Id { get; set; }
        public int RoomTypeId { get; set; }
        public int ParentRoomId { get; set; }
        public string RoomNo { get; set; }
        public int Status { get; set; }
        public int BuildingTypeId { get; set; }
    }
}
