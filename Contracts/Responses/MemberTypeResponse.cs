﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Contracts.Responses
{
    public class MemberTypeResponse
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public decimal Fee { get; set; }

        public List<MemberTypeItemResponse> MemberTypeItems { get; set; }
    }
}
