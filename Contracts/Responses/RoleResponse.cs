﻿using System.Collections.Generic;

namespace OONApi.Contracts.Responses
{
    public class RoleResponse
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public List<int> MenuIds { get; set; }
        public List<MenuResponse> Menus { get; set; }
    }
}
