﻿using System.Collections.Generic;

namespace OONApi.Contracts.Responses
{
    public class AngelPerformanceDebtResponse
    {
        public int Id { get; set; }
        public int DebtTypeId { get; set; }
        public int AngelPerformanceId { get; set; }
        public decimal Fee { get; set; }
        public decimal DebtNow { get; set; }

        public DebtTypeResponse DebtType { get; set; }
        public AngelPerformanceResponse AngelPerformance { get; set; }
    }
}
