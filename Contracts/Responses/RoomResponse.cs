﻿using OONApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Contracts.Responses
{
    public class RoomResponse
    {
        public int Id { get; set; }
        public int RoomTypeId { get; set; }
        public int? ParentRoomId { get; set; }
        public string RoomNo { get; set; }
        public int Status { get; set; }
        public int BuildingTypeId { get; set; }

        public RoomTypeResponse RoomType { get; set; }
        public BuildingTypeResponse BuildingType { get; set; }
        public List<RoomParentResponse> ParentRooms { get; set; }
        public RoomParentResponse ParentRoom { get; set; }

        public List<BookingResponse> Bookings { get; set; }
    }
}
