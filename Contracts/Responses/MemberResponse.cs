﻿using OONApi.Models;
using System;
using System.Collections.Generic;

namespace OONApi.Contracts.Responses
{
    public class MemberResponse
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Tel { get; set; }
        public decimal CreditAmount { get; set; }
        public int ReceptionId { get; set; }
        public DateTime? ExpiredDate { get; set; }
        public int Status { get; set; }
        public DateTime? FromDate { get; set; }
        public int CountriesId { get; set; }
        public int AgenciesId { get; set; }
        public int Point { get; set; }

        public CountriesResponse Countries { get; set; }
        public AgencyResponse Agencies { get; set; }
        public ReceptionResponse Reception { get; set; }
        public List<MemberItemResponse> MemberItems { get; set; }
        public List<MemberTransactionResponse> MemberTransactions { get; set; }
        public List<MemberItemTopupResponse> MemberItemsTopup { get; set; }
        public MemberTransactionResponse Membertransaction { get; set; }
    }
}
