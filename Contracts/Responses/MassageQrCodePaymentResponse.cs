﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Contracts.Responses
{
    public class MassageQrCodePaymentResponse
    {
        public int Id { get; set; }
        public int MassageId { get; set; }
        public decimal QrCodeTotal { get; set; }
    }
}
