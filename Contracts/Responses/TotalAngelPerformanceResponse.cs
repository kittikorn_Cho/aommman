﻿namespace OONApi.Contracts.Responses
{
    public class TotalAngelPerformanceResponse
    {
        public decimal TotalFee { get; set; }
        public decimal TotalWage { get; set; }
    }
}
