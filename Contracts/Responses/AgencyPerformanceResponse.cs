﻿using DocumentFormat.OpenXml.Office2010.ExcelAc;
using OONApi.Models;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;

namespace OONApi.Contracts.Responses
{
    public class AgencyPerformanceResponse
    {
        public int Id { get; set; }
        public int CarlendarDayId { get; set; }
        public bool IsPay { get; set; }
        public bool IsUnPaid { get; set; }
        public decimal TotalMassage { get; set; }
        public decimal TotalRound { get; set; }
        public decimal TotalMember { get; set; }
        public decimal TotalReceive { get; set; }
        public int AgenciesId { get; set; }
        public string Remark { get; set; }

        public AgencyResponse agency { get; set; }
        public List<MassageResponse> massage { get; set; }
        public CalendarDayResponse calendarDay { get; set; }

    }
}
