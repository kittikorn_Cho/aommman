﻿using OONApi.Models;
using System;
using System.Collections.Generic;

namespace OONApi.Contracts.Responses
{
    public class ReportMemberCreditAmountResponse
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Tel { get; set; }
        public decimal CreditAmount { get; set; }
        public int ReceptionId { get; set; }
        public DateTime ExpiredDate { get; set; }
        public int Status { get; set; }
        public DateTime? FromDate { get; set; }


        public virtual Reception Reception { get; set; }

        public List<MemberItem> MemberItems { get; set; }
        public List<MemberTransaction> MemberTransactions { get; set; }
    }
}
