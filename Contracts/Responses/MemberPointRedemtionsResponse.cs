﻿namespace OONApi.Contracts.Responses
{
    public class MemberPointRedemtionsResponse
    {
        public int Id { get; set; }
        public int MemberId { get; set; }
        public int CalendarDayId { get; set; }
        public decimal Point { get; set; }
        public decimal PointRedem { get; set; }
        public string Remark { get; set; }

        public MemberResponse Member { get; set; }
        public CalendarDayResponse CalendarDay { get; set; }
    }
}
