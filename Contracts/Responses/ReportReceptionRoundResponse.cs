﻿namespace OONApi.Contracts.Responses
{
    public class ReportReceptionRoundResponse
    {
        public decimal RoundTotal { get; set; }
        public int Rank { get; set; }
    }
}
