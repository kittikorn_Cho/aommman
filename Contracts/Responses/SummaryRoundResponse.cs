﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Contracts.Responses
{
    public class SummaryRoundResponse
    {
        public decimal Round { get; set; }
    }
}
