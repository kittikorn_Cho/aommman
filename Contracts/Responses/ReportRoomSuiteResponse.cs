﻿using System.Collections.Generic;

namespace OONApi.Contracts.Responses
{
    public class ReportRoomSuiteResponse
    {
        public int Id { get; set; }
        public int RoomTypeId { get; set; }
        public int? ParentRoomId { get; set; }
        public string RoomNo { get; set; }
        public int Status { get; set; }
        public int BuildingTypeId { get; set; }

        public RoomTypeResponse RoomType { get; set; }
        public BuildingTypeResponse BuildingType { get; set; }
        public List<RoomParentResponse> ParentRooms { get; set; }
        public RoomParentResponse ParentRoom { get; set; }
        public List<ReportMassageSellResponse> reportMassageSellResponses { get; set; }
    }
}
