﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace OONApi.Contracts.Responses
{
    public class ReportMassageDebtResponse
    {
        public int MassageId { get; set; }
        public decimal DebtTotal { get; set; }
        public decimal CashTotal { get; set; }
        public decimal CreditTotal { get; set; }
        public decimal QrTotal { get; set; }
        public decimal MemberTotal { get; set; }
        public int UpdatedBy { get; set; }
        public string Cashier { get; set; } = "";
        public string Member { get; set; } = "";
        //public DateTime? CreatedDate { get; set; }
        public DateTime? PayUnpaidDate { get; set; }
        public string UnpaidRemark { get; set; } = "";
        public MassageResponse Massage { get; set; }
    }
}
