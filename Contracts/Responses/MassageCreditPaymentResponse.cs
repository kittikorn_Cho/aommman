﻿namespace OONApi.Contracts.Responses
{
    public class MassageCreditPaymentResponse
    {
        public int Id { get; set; }
        public int MassageId { get; set; }
        public string CardNo { get; set; }
        public int? CardType { get; set; }
        public decimal CreditTotal { get; set; }
    }
}
