﻿using OONApi.Models;

namespace OONApi.Contracts.Responses
{
    public class ReportMemberTopupResponse
    {
        public int Id { get; set; }
        public int MemberId { get; set; }
        public int CalendarId { get; set; }
        public decimal CerditAmount { get; set; }
        public string Name_1 { get; set; }
        public decimal Amount_1 { get; set; }
        public string Name_2 { get; set; }
        public decimal Amount_2 { get; set; }
        public string Name_3 { get; set; }
        public decimal Amount_3 { get; set; }
        public string Name_4 { get; set; }
        public decimal Amount_4 { get; set; }
        public string Name_5 { get; set; }
        public decimal Amount_5 { get; set; }

        public MemberResponse Member { get; set; }
        public CalendarDayResponse calendarDay { get; set; }
    }
}
