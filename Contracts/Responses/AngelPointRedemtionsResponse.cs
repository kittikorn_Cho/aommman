﻿using System.Collections.Generic;

namespace OONApi.Contracts.Responses
{
    public class AngelPointRedemtionsResponse
    {
        public int Id { get; set; }
        public int AngelId { get; set; }
        public int CalendarDayId { get; set; }
        public decimal Point { get; set; }
        public decimal PointRedem { get; set; }
        public string Remark { get; set; }

        public AngelResponse Angel { get; set; }
        public CalendarDayResponse CalendarDay { get; set; }

    }
}
