﻿namespace OONApi.Contracts.Responses
{
    public class ReceptionPointRedemtionsResponse
    {
        public int Id { get; set; }
        public int ReceptionId { get; set; }
        public int CalendarDayId { get; set; }
        public decimal Point { get; set; }
        public decimal PointRedem { get; set; }
        public string Remark { get; set; }

        public ReceptionResponse Reception { get; set; }
        public CalendarDayResponse CalendarDay { get; set; }
    }
}
