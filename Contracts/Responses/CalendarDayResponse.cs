﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Contracts.Responses
{
    public class CalendarDayResponse
    {
        public int Id { get; set; }
        public DateTime ToDay { get; set; }
        public bool IsDate { get; set; }
    }
}
