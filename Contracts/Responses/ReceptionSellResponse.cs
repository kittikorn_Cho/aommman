﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Contracts.Responses
{
    public class ReceptionSellResponse
    {
        public int Id { get; set; }
        public int ReceptionTypeId { get; set; }
        public string Code { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Nickname { get; set; }
        public string Tel { get; set; }
        public DateTime? WorkFromDate { get; set; }
        public DateTime? WorkToDate { get; set; }
        public decimal TotalAngelRound { get; set; }
        public decimal TotalSuiteCount { get; set; }
        public decimal TotalMemberSell { get; set; }

        public List<AngelTypeResponse> angelTypes { get; set; }
    }
}
