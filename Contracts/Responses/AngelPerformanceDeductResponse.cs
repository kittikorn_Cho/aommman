﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Contracts.Responses
{
    public class AngelPerformanceDeductResponse
    {
        public int Id { get; set; }
        public int MassageAngelId { get; set; }
        public int DeductTypeId { get; set; }
        public int AngelPerformanceId { get; set; }
        public decimal Fee { get; set; }
        public decimal DiscountFee { get; set; }

        public DeductTypeResponse DeductType { get; set; }
    }
}
