﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Contracts.Responses
{
    public class MassageAngelResponse
    {
        public int Id { get; set; }
        public int MassageId { get; set; }
        public int RoomId { get; set; }
        public int ReceptionId { get; set; }
        public int AngelId { get; set; }
        public DateTime? CheckInTime { get; set; }
        public DateTime? CheckOutTime { get; set; }
        public DateTime? FirstCheckInTime { get; set; }
        public int TimeMinute { get; set; }
        public decimal Round { get; set; }
        public decimal RoundFact { get; set; }
        public DateTime? LastCallTime { get; set; }
        public int CallCount { get; set; }
        public bool IsNotCall { get; set; }
        public int? PaymentTypeId { get; set; }
        public bool IsChangeRoom { get; set; }
        public bool IsDiscount { get; set; }
        public bool IsDiscountBaht { get; set; }
        public decimal DiscountBaht { get; set; }
        public bool IsDiscountPercent { get; set; }
        public decimal DiscountPercent { get; set; }
        public bool IsDiscountAngelRound { get; set; }
        public decimal DiscountAngelRound { get; set; }
        public string DiscountRemark { get; set; }
        public decimal AddRound { get; set; }

        public MassageResponse Massage { get; set; }
        public AngelResponse Angel { get; set; }
        public RoomResponse Room { get; set; }
        public ReceptionResponse Reception { get; set; }
        public List<AngelPerformanceDeductResponse> AngelPerformanceDeducts { get; set; }
    }
}
