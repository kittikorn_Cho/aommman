﻿namespace OONApi.Contracts.Responses
{
    public class CountriesResponse
    {
        public int Id { get; set; }
        public string NameEn { get; set; }
        public string NameTh { get; set; }
    }
}
