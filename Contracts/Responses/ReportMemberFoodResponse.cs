﻿namespace OONApi.Contracts.Responses
{
    public class ReportMemberFoodResponse
    {
        public int Id { get; set; }
        public int CalendarId { get; set; }
        public string DocumentNumber { get; set; }
        public int MemberId { get; set; }
        public string Name_1 { get; set; }
        public decimal Amount_1 { get; set; }
        public string Name_2 { get; set; }
        public decimal Amount_2 { get; set; }
        public string Name_3 { get; set; }
        public decimal Amount_3 { get; set; }
        public string Name_4 { get; set; }
        public decimal Amount_4 { get; set; }
        public string Name_5 { get; set; }
        public decimal Amount_5 { get; set; }
        public decimal CerditAmount { get; set; }
        public int CreatedBy { get; set; }
        public bool IsPay { get; set; }
        public bool IsUnPaid { get; set; }
        public decimal Total { get; set; }
        public decimal UnpaidTotal { get; set; }
        public decimal CashTotal { get; set; }
        public decimal CreditTotal { get; set; }
        public decimal QrCodeTotal { get; set; }

        public MemberResponse Member { get; set; }
        public UserResponse User { get; set; }
    }
}
