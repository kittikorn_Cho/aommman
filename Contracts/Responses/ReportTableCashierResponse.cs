﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Contracts.Responses
{
    public class ReportTableCashierResponse
    {
        public decimal AngelCash { get; set; }
        public decimal AngelCredit { get; set; }
        public decimal AngelMember { get; set; }
        public decimal AngelQrCode { get; set; }
        public decimal FoodCash { get; set; }
        public decimal FoodCredit { get; set; }
        public decimal FoodMember { get; set; }
        public decimal FoodQrCode { get; set; }
        public decimal CutFoodMember { get; set; }
        public decimal RoomCash { get; set; }
        public decimal RoomCredit { get; set; }
        public decimal RoomMember { get; set; }
        public decimal RoomQrCode { get; set; }
        public decimal OtherServiceChargeCash { get; set; }
        public decimal OtherServiceChargeCredit { get; set; }
        public decimal OtherServiceChargeMember { get; set; }
        public decimal OtherServiceChargeQrCode { get; set; }
        public decimal SumCash { get; set; }
        public decimal SumCredit { get; set; }
        public decimal SumMember { get; set; }
        public decimal SumQrCode { get; set; }
        public decimal SumUnpaid { get; set; }
    }
}
