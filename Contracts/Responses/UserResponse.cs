﻿using OONApi.Models;
using System.Collections.Generic;

namespace OONApi.Contracts.Responses
{
    public class UserResponse
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string UserFullname { get; set; }
        public string Tel { get; set; }
        public int Status { get; set; }

        public List<RoleResponse> Roles { get; set; }
        public List<Menu> Menus { get; set; }

        public string Token { get; set; }
    }
}
