﻿namespace OONApi.Contracts.Responses
{
    public class ReceptionPointTransactionResponse
    {
        public int Id { get; set; }
        public int MassageAngelId { get; set; }
        public int ReceptionId { get; set; }
        public decimal Point { get; set; }

        public MassageAngelResponse massageAngel { get; set; }
    }
}
