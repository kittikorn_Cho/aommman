﻿using OONApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Contracts.Responses
{
    public class MemberTypeItemResponse
    {
        public int Id { get; set; }
        public int ItemId { get; set; }
        public decimal Amount { get; set; }
        public ItemResponse Item { get; set; }
    }
}
