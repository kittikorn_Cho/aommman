﻿using System;

namespace OONApi.Contracts.Responses
{
    public class MassageReportResponse
    {
        public DateTime? BillDate { get; set; }
        public string BillNo { get; set; }
        public string ReceptionCode { get; set; }
        public string RoomNo { get; set; }
        public string AngelCode { get; set; }
        public DateTime? CheckInTime { get; set; }
        public DateTime? CheckOutTime { get; set; }
        public DateTime? SumTime { get; set; }
        public decimal Round { get; set; }
        public decimal Wage { get; set; }
        public decimal CashTotal { get; set; }
        public decimal QrCodeTotal { get; set; }
        public decimal CreditTotal { get; set; }
        public decimal MemberTotal { get; set; }
        public decimal UseSuite { get; set; }
        public decimal UseVip { get; set; }
        public string Remark { get; set; }
        public bool IsMassageRoom { get; set; }
        public double TimeMinute { get; set; }
        public decimal Total { get; set; }
        public decimal SumCashTotal { get; set; }
        public decimal SumQrCode { get; set; }
        public decimal SumCredit { get; set;}
        public decimal SumMember { get; set; }
        public decimal SumUseSuite { get; set; }
        public decimal SumUseVip { get; set; }
        public string MemberCode { get; set; }
        public decimal DiscountBaht { get; set; }
        public decimal DiscountPercent { get; set; }
    }
}
