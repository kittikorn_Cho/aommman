﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Contracts.Responses
{
    public class ReportDailyFinanceResponse
    {
        public decimal TotalFee { get; set; }
        public decimal TotalRound { get; set; }
        public decimal TotalRoundDiscount { get; set; }
        public decimal TotalCashAngel { get; set; }
        public decimal TotalCreditAngel { get; set; }
        public decimal TotalMemberAngel { get; set; }
        public decimal TotalQrCodeAngel { get; set; }
        public decimal TotalTip { get; set; }
        public decimal TotalTipComm { get; set; }
        public decimal TotalOtherServiceCharge { get; set; }
        public decimal TotalDamages { get; set; }
        public decimal TotalCashEtc { get; set; }
        public decimal TotalCreditEtc { get; set; }
        public decimal TotalMemberEtc { get; set; }
        public decimal TotalQrCodeEtc { get; set; }
        public decimal TotalEtc { get; set; }
        public decimal TotalMassageAngel { get; set; }
        public decimal Total { get; set; }
        public decimal TipQrCodeTotal { get; set; }

    }
}
