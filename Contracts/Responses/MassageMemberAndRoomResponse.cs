﻿using DocumentFormat.OpenXml.Office2010.ExcelAc;
using System.Collections.Generic;

namespace OONApi.Contracts.Responses
{
    public class MassageMemberAndRoomResponse
    {
        public int Id { get; set; }
        public int MemberId { get; set; }
        public int MassageRoomId { get; set; }
        public int MassageMemberPaymentId { get; set; }
        public decimal Round { get; set; }
        public string ZbewOnijang { get; set; }

        public MemberResponse Member { get; set; }
        public MassageRoomResponse MassageRoom { get; set; }
        public MassageMemberPaymentResponse MassageMemberPayment { get; set; }
        public List<MassageRoomResponse> MassageRooms { get; set; }
    }
}
