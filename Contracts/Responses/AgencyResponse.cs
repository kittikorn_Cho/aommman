﻿using OONApi.Models;
using System;

namespace OONApi.Contracts.Responses
{
    public class AgencyResponse
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Nickname { get; set; }
        public string Tel { get; set; }
        public string Address { get; set; }
        public DateTime? WorkFronData { get; set; }
        public DateTime? WorkToDate { get; set; }
        public int? Status { get; set; }
        public int AgencyTypeId { get; set; }

        public AgencyTypeResponse AgencyType { get; set; }

    }
}
