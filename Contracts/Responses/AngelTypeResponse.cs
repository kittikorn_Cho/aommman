﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Contracts.Responses
{
    public class AngelTypeResponse
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public decimal Fee { get; set; }
        public decimal Wage { get; set; }
        public decimal CreditComm { get; set; }
        public decimal CheerComm { get; set; }
        public int RoundTime { get; set; }
        public int Order { get; set; }

        public decimal totalRound { get; set; }
        public decimal TotalCount { get; set; }
        public bool IsSpecialComm { get; set; }
        public bool IsSpecialClean { get; set; }
        public List<AngelResponse> Angel { get; set; }

    }

}
