﻿using OONApi.Models;
using System.ComponentModel.DataAnnotations.Schema;
using System;
using System.Collections.Generic;

namespace OONApi.Contracts.Responses
{
    public class BookingAngelsResponse
    {
        public int Id { get; set; }
        public int BookingId { get; set; }
        public int AngelId { get; set; }
        public DateTime? StartDate { get; set; }
        public TimeSpan? StartTime { get; set; }
        public TimeSpan? EndTime { get; set; }
        public bool Flag { get; set; }

        public BookingResponse Booking { get; set; }
        public AngelResponse Angel { get; set; }
    }
}
