﻿using OONApi.Models;

namespace OONApi.Contracts.Responses
{
    public class MemberTopupResponse
    {

        public int MemberId { get; set; }
        public int CalendarId { get; set; }
        public decimal CerditAmount { get; set; }
        public string Name_1 { get; set; }
        public decimal Amount_1 { get; set; }
        public string Name_2 { get; set; }
        public decimal Amount_2 { get; set; }
        public string Name_3 { get; set; }
        public decimal Amount_3 { get; set; }
        public string Name_4 { get; set; }
        public decimal Amount_4 { get; set; }
        public string Name_5 { get; set; }
        public decimal Amount_5 { get; set; }
        public string UserName { get; set; }
        public Member Member { get; set; }
        public CalendarDay calendarDay { get; set; }
    }
}
