﻿namespace OONApi.Contracts.Responses
{
    public class ReportSummaryMemberTransactionResponse
    {
        public decimal Summary { get; set; }
        public int Rank { get; set; }
    }
}
