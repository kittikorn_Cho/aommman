﻿namespace OONApi.Contracts.Responses
{
    public class DailyDebtResponse
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal Fee { get; set; }
    }
}
