﻿using OONApi.Contracts.Requests;
using OONApi.Migrations;
using System;
using System.Collections.Generic;

namespace OONApi.Contracts.Responses
{
    public class BookingResponse
    {
        public int Id { get; set; }
        public int RoomId { get; set; }
        public string DocumentNumber { get; set; }
        public DateTime StartDate { get; set; }
        public TimeSpan StartTime { get; set; }
        public TimeSpan EndTime { get; set; }
        public bool Flag { get; set; }
        public string Tel { get; set; }
        public string Remark { get; set; }

        public List<BookingAngelsResponse> BookingAngels { get; set; }

    }
}
