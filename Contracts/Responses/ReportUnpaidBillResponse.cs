﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Contracts.Responses
{
    public class ReportUnpaidBillResponse
    {
        public int Id { get; set; }
        public int CalendarDayId { get; set; }
        public bool IsPaid { get; set; }
        public bool IsUnPaid { get; set; }
        public decimal Total { get; set; }
        public decimal AngelTotal { get; set; }
        public decimal FoodTotal { get; set; }
        public decimal RoomTotal { get; set; }
        public bool IsCashAngel { get; set; }
        public bool IsCreditAngel { get; set; }
        public bool IsMemberAngel { get; set; }
        public bool IsQrCodeAngel { get; set; }
        public decimal CashAngelTotal { get; set; }
        public decimal CreditAngelTotal { get; set; }
        public decimal MemberAngelTotal { get; set; }
        public decimal QrCodeAngelTotal { get; set; }
        public bool IsCashFood { get; set; }
        public bool IsCreditFood { get; set; }
        public bool IsMemberFood { get; set; }
        public bool IsQrCodeFood { get; set; }
        public decimal CashFoodTotal { get; set; }
        public decimal CreditFoodTotal { get; set; }
        public decimal MemberFoodTotal { get; set; }
        public decimal QrCodeFoodTotal { get; set; }
        public bool IsCashRoom { get; set; }
        public bool IsCreditRoom { get; set; }
        public bool IsMemberRoom { get; set; }
        public bool IsQrCodeRoom { get; set; }
        public decimal CashRoomTotal { get; set; }
        public decimal CreditRoomTotal { get; set; }
        public decimal MemberRoomTotal { get; set; }
        public decimal QrCodeRoomTotal { get; set; }
        public bool IsEntertainFood { get; set; }
        public decimal EntertainTotalFood { get; set; }
        public string EntertainRemarkFood { get; set; }
        public bool IsEntertainAngal { get; set; }
        public decimal EntertainTotalAngel { get; set; }
        public string EntertainRemarkAngel { get; set; }
        public DateTime? PayDate { get; set; }
        public string DocumentNumber { get; set; }
        public decimal EtcTotal { get; set; }
        public bool IsCashEtc { get; set; }
        public bool IsCreditEtc { get; set; }
        public bool IsMemberEtc { get; set; }
        public bool IsEntertainEtc { get; set; }
        public bool IsQrCodeEtc { get; set; }
        public string EntertainEtcRemark { get; set; }
        public decimal CashEtcTotal { get; set; }
        public decimal CreditEtcTotal { get; set; }
        public decimal MemberEtcTotal { get; set; }
        public decimal EntertainEtcTotal { get; set; }
        public decimal QrCodeEtcTotal { get; set; }
        public decimal TipTotal { get; set; }
        public decimal TipCommTotal { get; set; }
        public decimal DamagesTotal { get; set; }
        public decimal OtherServiceChargesTotal { get; set; }
        public string EtcRemark { get; set; }
        public int CreatedBy { get; set; }
        public int UpdatedBy { get; set; }

        public MassageRoomResponse massageRoom { get; set; }
        public List<MassageAngelResponse> massageAngels { get; set; }
        public MassageCreditPaymentResponse massageCreditPayment { get; set; }
        public List<MassageMemberPaymentResponse> massageMemberPayments { get; set; }
        public UserResponse user { get; set; }
        public ReceptionResponse reception { get; set; }
    }
}
