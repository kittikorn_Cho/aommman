﻿using System;

namespace OONApi.Contracts.Responses
{
    public class AngelWorkingResponse
    {
        public int Id { get; set; }
        public int CalendarDayId { get; set; }
        public int AngelId { get; set; }
        public DateTime? GetInTime { get; set; }
        public DateTime? GetOutTime { get; set; }

        public AngelResponse angel { get; set; }
    }
}
