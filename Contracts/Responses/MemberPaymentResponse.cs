﻿using OONApi.Models;

namespace OONApi.Contracts.Responses
{
    public class MemberPaymentResponse
    {
        public int Id { get; set; }
        public int CalendarId { get; set; }
        public string DocumentNumber { get; set; }
        public int MemberId { get; set; }
        public string Name_1 { get; set; }
        public decimal Amount_1 { get; set; }
        public string Name_2 { get; set; }
        public decimal Amount_2 { get; set; }
        public string Name_3 { get; set; }
        public decimal Amount_3 { get; set; }
        public string Name_4 { get; set; }
        public decimal Amount_4 { get; set; }
        public string Name_5 { get; set; }
        public decimal Amount_5 { get; set; }

        public decimal CerditAmount { get; set; }
        public bool IsPay { get; set; }
        public bool IsUnPaid { get; set; }
        public decimal Total { get; set; }
        public decimal UnpaidTotal { get; set; }
        public bool IsCash { get; set; }
        public bool IsCredit { get; set; }
        public bool IsQrCode { get; set; }
        public decimal CashTotal { get; set; }
        public decimal CreditTotal { get; set; }
        public decimal QrCodeTotal { get; set; }

        public Member Member { get; set; }
        public CalendarDay CalendarDay { get; set; }
    }
}
