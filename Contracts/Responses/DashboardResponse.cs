﻿using OONApi.Models;
using System.Collections.Generic;

namespace OONApi.Contracts.Responses
{
    public class DashboardResponse
    {
        public decimal TotalRound { get; set; }
        public decimal TotalAngel { get; set; }
        public List<AngelTypeResponse> AngelTypes { get; set; }
        public decimal TotalMassage { get; set; }
        public decimal TotalFood { get; set; }
        public decimal TotalRoomSuite { get; set; }
        public decimal TotalUseSuite { get; set; }
        public decimal TotalAddRound { get; set; }
    }
}
