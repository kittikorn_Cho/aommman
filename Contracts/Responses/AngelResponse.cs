﻿using OONApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Contracts.Responses
{
    public class AngelResponse
    {
        public int Id { get; set; }
        public int AngelTypeId { get; set; }
        public string Code { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Nickname { get; set; }
        public string Tel { get; set; }
        public int? Status { get; set; }
        public int Point { get; set; }
        public DateTime? WorkFromDate { get; set; }
        public DateTime? WorkToDate { get; set; }
        public string Rfid { get; set; }

        public AngelTypeResponse AngelType { get; set; }
        public List<AngelDebtResponse> AngelDebts { get; set; }
        public List<MassageAngelResponse> massageAngels { get; set; }
        public AngelSalaryCountDayResponse AngelSalaryCountDay { get; set; }
    }
}
