﻿namespace OONApi.Contracts.Responses
{
    public class DebtTypeResponse
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal SummaryPerformance { get; set; }
    }
}
