﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Contracts.Responses
{
    public class BuildingTypeResponse
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
