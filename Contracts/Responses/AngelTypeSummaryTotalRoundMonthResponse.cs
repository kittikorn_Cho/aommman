﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Contracts.Responses
{
    public class AngelTypeSummaryTotalRoundMonthResponse
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public decimal Fee { get; set; }
        public decimal Wage { get; set; }
        public decimal CreditComm { get; set; }
        public decimal CheerComm { get; set; }
        public int RoundTime { get; set; }
        public int Order { get; set; }
        public List<AngelSummaryTotalRoundMonthResponse> Angels { get; set; }
    }
}
