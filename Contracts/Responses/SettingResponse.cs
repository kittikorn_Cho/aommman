﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Contracts.Responses
{
    public class SettingResponse
    {
        public int Id { get; set; }
        public string MassageName { get; set; }
        public string MassagePath { get; set; }
        public bool IsReportCashier { get; set; }
        public bool IsAngelPerformanceCarlendar { get; set; }
        public bool IsDailyDebt { get; set; }
        public bool IsAngelDiscountComm { get; set; }
        public bool IsQrCodeReport { get; set; }
        public bool IsCountDaily { get; set; }
        public decimal PointAngel { get; set; }
        public decimal PointMember { get; set; }
        public decimal PointReception { get; set; }
    }
}
