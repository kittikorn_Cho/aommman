﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Contracts.Responses
{
    public class RoomSuiteCheckInResponse
    {
        public int Id { get; set; }
        public int MassageId { get; set; }
        public int RoomId { get; set; }
        public int ReceptionId { get; set; }
        public DateTime? CheckInTime { get; set; }
        public DateTime? CheckOutTime { get; set; }
        public DateTime? OldCheckInTime { get; set; }
        public int TimeMinute { get; set; }
        public DateTime? LastCallTime { get; set; }
        public int CallCount { get; set; }
        public bool IsNotCall { get; set; }
        public decimal Round { get; set; }
        public bool IsDiscount { get; set; }
        public bool IsDiscountBaht { get; set; }
        public decimal DiscountBaht { get; set; }
        public bool IsDiscountPercent { get; set; }
        public decimal DiscountPercent { get; set; }
        public string DiscountRemark { get; set; }

        public List<RoomMassageAngelResponse> Rooms { get; set; }
        public RoomResponse Room { get; set; }
    }
}
