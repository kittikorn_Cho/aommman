﻿using System;
using System.Collections.Generic;

namespace OONApi.Contracts.Responses
{
    public class MemberPaymentEtcResponse
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Tel { get; set; }
        public decimal CreditAmount { get; set; }
        public int ReceptionId { get; set; }
        public DateTime? ExpiredDate { get; set; }
        public int Status { get; set; }
        public DateTime? FromDate { get; set; }
        public string DocumentNumber { get; set; }
        public string UserName { get; set; }

        public string Name_1 { get; set; }
        public decimal Amount_1 { get; set; }
        public string Name_2 { get; set; }
        public decimal Amount_2 { get; set; }

        public decimal DebtCreditAmount { get; set; }
        public bool IsPay { get; set; }
        public bool IsUnPaid { get; set; }
        public decimal Total { get; set; }
        public decimal UnpaidTotal { get; set; }
        public bool IsCash { get; set; }
        public bool IsCredit { get; set; }
        public bool IsQrCode { get; set; }
        public decimal CashTotal { get; set; }
        public decimal CreditTotal { get; set; }
        public decimal QrCodeTotal { get; set; }

        public DateTime DateDebt { get; set; }
        public DateTime DateNow { get; set; }

        public List<MemberItemResponse> MemberItems { get; set; }
    }
}
