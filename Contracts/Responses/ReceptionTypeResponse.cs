﻿namespace OONApi.Contracts.Responses
{
    public class ReceptionTypeResponse
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
    }
}
