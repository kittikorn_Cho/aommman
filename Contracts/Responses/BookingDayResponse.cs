﻿namespace OONApi.Contracts.Responses
{
    public class BookingDayResponse
    {
        public int RoomId { get; set; }
        public string RoomNo { get; set; }
    }
}
