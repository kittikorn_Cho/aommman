﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Contracts.Responses
{
    public class ReportMemberPaymentResponse
    {
        public int Id { get; set; }
        public int MassageId { get; set; }
        public int MemberId { get; set; }
        public decimal MemberTotal { get; set; }
        public decimal UseSuite { get; set; }
        public decimal UseVip { get; set; }
        public decimal MemberFoodTotal { get; set; }
        public decimal MemberMassageTotal { get; set; }
        public decimal MemberRoomTotal { get; set; }

        public MemberResponse Member { get; set; }
        public MassageResponse Massage { get; set; }
        public MassageRoomResponse MassageRoom { get; set; }
        public List<MassageRoomResponse> MassageRooms { get; set; }
        public List<MassageMemberAndRoomResponse> MassageMemberAndRooms { get; set; }
    }
}
