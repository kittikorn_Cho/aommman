﻿using System.Collections.Generic;

namespace OONApi.Contracts.Responses
{
    public class AngelPointTransactionResponse
    {
        public int Id { get; set; }
        public int MassageAngelId { get; set; }
        public int AngelId { get; set; }
        public decimal Point { get; set; }

        public MassageAngelResponse massageAngel { get; set; }
    }
}
