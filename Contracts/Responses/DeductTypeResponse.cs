﻿using System.ComponentModel.DataAnnotations.Schema;

namespace OONApi.Contracts.Responses
{
    public class DeductTypeResponse
    {
        public int Id { get; set; }
        public string Name { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal Fee { get; set; }
        public string Slug { get; set; }
        public decimal SummaryPerformance { get; set; }
    }
}
