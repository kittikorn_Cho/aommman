﻿using System.Collections.Generic;

namespace OONApi.Contracts.Responses
{
    public class AngelPerformanceResponse
    {
        public int Id { get; set; }
        public int CarlendarDayId { get; set; }
        public int AngelId { get; set; }
        public bool IsPay { get; set; }
        public decimal TotalFee { get; set; }
        public decimal TotalWage { get; set; }
        public decimal TotalReceive { get; set; }
        public decimal TotalDeduct { get; set; }
        public decimal TotalDebt { get; set; }
        public decimal TotalRound { get; set; }
        public decimal AnotherDeductAmount { get; set; }
        public string AnotherDeductDescription { get; set; }
        public decimal AnotherAddAmount { get; set; }
        public string AnotherAddDescription { get; set; }
        public decimal TotalDailyDebt { get; set; }
        public bool IsPerformanceNew { get; set; }
        public int MassageAngelCount { get; set; }

        public AngelResponse Angel { get; set; }
        public List<MassageAngelResponse> MassageAngels { get; set; }
        public CalendarDayResponse CalendarDay { get; set; }
        public List<AngelPerformanceDebtResponse> AngelPerformanceDebts { get; set; }
        
    }
}
