﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Contracts.Responses
{
    public class SummaryMassageResponse
    {
        public decimal CashAngelTotal { get; set; }
        public decimal CreditAngelTotal { get; set; }
        public decimal MemberAngelTotal { get; set; }
        public decimal QrCodeAngelTotal { get; set; }
        public decimal CashFoodTotal { get; set; }
        public decimal CreditFoodTotal { get; set; }
        public decimal MemberFoodTotal { get; set; }
        public decimal CutMemberFood { get; set; }
        public decimal QrCodeFoodTotal { get; set; }
        public decimal TotalCash { get; set; }
        public decimal TotalCredit { get; set; }
        public decimal TotalMember { get; set; }
        public decimal TotalQrCode { get; set; }
        public decimal SummaryUnpaid { get; set; }
        public decimal TotalRoomSuite { get; set; }
        public decimal TotalRoomVip { get; set; }
        public decimal OtherServiceChargesTotal { get; set; }
        public decimal DamagesTotal { get; set; }
        public decimal TipTotal { get; set; }
        public decimal CashRoomSuiteTotal { get; set; }
        public decimal QrCodeRoomSuiteTotal { get; set; }
        public decimal CreditRoomSuiteTotal { get; set; }
        public decimal MemberRoomSuiteTotal { get; set; }
        public decimal CashRoomVipTotal { get; set; }
        public decimal QrCodeRoomVipTotal { get; set; }
        public decimal CreditRoomVipTotal { get; set; }
        public decimal MemberRoomVipTotal { get; set; }
        public decimal TipQrCodeTotal { get; set; }

    }
}
