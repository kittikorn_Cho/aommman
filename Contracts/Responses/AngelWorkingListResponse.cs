﻿using OONApi.Models;
using System.Collections.Generic;

namespace OONApi.Contracts.Responses
{
    public class AngelWorkingListResponse
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public decimal Fee { get; set; }
        public decimal Wage { get; set; }
        public decimal CreditComm { get; set; }
        public decimal CheerComm { get; set; }
        public int RoundTime { get; set; }
        public int Order { get; set; }

        public List<AngelWorkingResponse> angelWorkings { get; set; }
        public CalendarDayResponse calendarDay { get; set; }
    }
}
