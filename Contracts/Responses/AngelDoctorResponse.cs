﻿namespace OONApi.Contracts.Responses
{
    public class AngelDoctorResponse
    {
        public int Id { get; set; }
        public int AngelId { get; set; }
        public string DoctorName { get; set; }
        public string Detail { get; set; }

        public AngelResponse Angel { get; set; }
    }
}
