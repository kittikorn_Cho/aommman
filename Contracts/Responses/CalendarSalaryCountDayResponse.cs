﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Contracts.Responses
{
    public class CalendarSalaryCountDayResponse
    {
        public int Id { get; set; }
        public DateTime ToDay { get; set; }
        public bool IsDate { get; set; }
        public decimal CountRound { get; set; }
        public DateTime? GetInTime { get; set; }
        public DateTime? GetOutTime { get; set; }
        public bool IsDay { get; set; }
        public int CountDay { get; set; }
    }
}
