﻿namespace OONApi.Contracts.Responses
{
    public class AngelDebtResponse
    {
        public int Id { get; set; }
        public int AngelId { get; set; }
        public int DebtTypeId { get; set; }
        public decimal Fee { get; set; }

        public DebtTypeResponse DebtType { get; set; }
    }
}
