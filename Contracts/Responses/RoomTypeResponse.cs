﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Contracts.Responses
{
    public class RoomTypeResponse
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public decimal RoomRate { get; set; }
        public decimal RoomRateNext { get; set; }
        public int RoundTime { get; set; }

        public List<RoomResponse> Rooms { get; set; }
    }
}
