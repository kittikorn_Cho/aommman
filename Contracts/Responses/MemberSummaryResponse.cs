﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Contracts.Responses
{
    public class MemberSummaryResponse
    {
        public decimal TotalActive { get; set; }
        public decimal TotalExpire { get; set; }
        public decimal TotalPaymentDay { get; set; }
        public decimal TotalHistoryActive { get; set; }
        public decimal Total { get; set; }

        public List<MassageMemberPaymentResponse> massageMemberPayments { get; set; }
        public List<MemberPaymentResponse> memberPayments  { get; set; }
    }
}
