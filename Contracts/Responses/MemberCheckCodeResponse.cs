﻿namespace OONApi.Contracts.Responses
{
    public class MemberCheckCodeResponse
    {
        public bool IsMember { get; set; }
        public string MemberMessage { get; set; }
    }
}
