﻿using System;

namespace OONApi.Contracts.Responses
{
    public class ReportReceptionSellResponse
    {
        public int Id { get; set; }
        public int ReceptionTypeId { get; set; }
        public string Code { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Nickname { get; set; }
        public string Tel { get; set; }
        public string Address { get; set; }
        public DateTime? WorkFromDate { get; set; }
        public DateTime? WorkToDate { get; set; }

        public ReportSummaryMemberTransactionResponse Summarys { get; set; }
    }
}
