﻿using DocumentFormat.OpenXml.Math;
using NLog.Targets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Contracts.ApiRoutes
{
    public static class ApiRoutes
    {
        public const string Root = "api";
        public const string Version = "v1";
        public const string Base = Root + "/" + Version;

        public static class AngelTypes
        {
            public const string GetAll = Base + "/angelTypes";
            public const string Get = Base + "/angelTypes/{id}";
            public const string Create = Base + "/angelTypes/create";
            public const string Update = Base + "/angelTypes/{id}/update";
            public const string UpdateOrderAngelType = Base + "/angelTypes/{id}/updateOrderAngelType";
            public const string Delete = Base + "/angelTypes/{id}";
        }

        public static class Angels
        {
            public const string GetAll = Base + "/angels";
            public const string Get = Base + "/angels/{id}";
            public const string Create = Base + "/angels/create";
            public const string Update = Base + "/angels/{id}/update";
            public const string Cancel = Base + "/angels/{id}";
            public const string ManageWorking = Base + "/angels/manageWorking";
            public const string GetAllWorking = Base + "/angels/getAllWorking";
            public const string GetRfid = Base + "/angels/{rfid}/rfid";
            public const string GetSummaryTotalRoundMonth = Base + "/angels/summaryTotalRoundMonth"; 
            public const string GetHistoryDebt = Base + "/angels/historyDebt";
            public const string GetSalaryCountDay = Base + "/angels/salaryCountDay";
            public const string Upload = Base + "/angels/upload";
            public const string DeleteAngelsWorking = Base + "/angels/{id}/deleteAngelsWorking";
            public const string CreateAngelPointRedemtions = Base + "/angels/createAngelPointRedemtions";
            public const string CancelAngelPointRedemtions = Base + "/angels/{id}/cancelAngelPointRedemtions";
            public const string GetAngelPointRedemtionsHistory = Base + "/angels/getAngelPointRedemtionsHistory";
            public const string GetAngelAllPoint = Base + "/angels/getAngelAllPoint";
        }

        public static class BuildingTypes
        {
            public const string GetAll = Base + "/buildingTypes";
            public const string Get = Base + "/buildingTypes/{id}";
            public const string Create = Base + "/buildingTypes/create";
            public const string Update = Base + "/buildingTypes/{id}/update";
            public const string Delete = Base + "/buildingTypes/{id}";
        }

        public static class RoomTypes
        {
            public const string GetAll = Base + "/roomTypes";
            public const string Get = Base + "/roomTypes/{id}";
            public const string Create = Base + "/roomTypes/create";
            public const string Update = Base + "/roomTypes/{id}/update";
            public const string Delete = Base + "/roomTypes/{id}";
            public const string GetRoomByRoomTypeId = Base + "/roomTypes/childRoom";
        }

        public static class Room
        {
            public const string GetAll = Base + "/rooms";
            public const string Get = Base + "/rooms/{id}";
            public const string Create = Base + "/rooms/create";
            public const string Update = Base + "/rooms/{id}/update";
            public const string Delete = Base + "/rooms/{id}";
            public const string GetRoomTypeByBuildingTypeId = Base + "/rooms/getRoomTypeByBuildType";
        }

        public static class MemberTypes
        {
            public const string GetAll = Base + "/memberTypes";
            public const string Get = Base + "/memberTypes/{id}";
            public const string Create = Base + "/memberTypes/create";
            public const string Update = Base + "/memberTypes/{id}/update";
            public const string Delete = Base + "/memberTypes/{id}";
        }

        public static class Items
        {
            public const string GetAll = Base + "/items";
            public const string Get = Base + "/items/{id}";
            public const string Create = Base + "/items/create";
            public const string Update = Base + "/items/{id}/update";
            public const string Delete = Base + "/items/{id}";
        }

        public static class Members
        {
            public const string GetAll = Base + "/members";
            public const string Get = Base + "/members/{id}";
            public const string Create = Base + "/members/create";
            public const string Update = Base + "/members/{id}/update";
            public const string Delete = Base + "/members/{id}";
            public const string DeleteSelectAll = Base + "/members/selectDelete";
            public const string Search = Base + "/members/search";
            public const string GetListPayment = Base + "/members/listPayment";
            public const string GetPayment = Base + "/members/{id}/viewPayment";
            public const string GetMemberHistory = Base + "/members/{id}/viewMemberHistory";
            public const string GetSummaryTotalCredit = Base + "/members/summaryTotalCredit";
            public const string CreateMemberPayment = Base + "/members/createMemberPayment";
            public const string Cancel = Base + "/members/{id}/cancel";
            public const string CreateMemberTopup = Base + "/members/createMemberTopup";
            public const string MemberChangeReception = Base + "/members/memberChangeReception";
            public const string CheckCode = Base + "/members/checkCode";
            public const string GetAllMemberPayments = Base + "/members/{id}/getAllMemberPayments";
            public const string GetAllMemberPaymentsUnpaid = Base + "/members/getAllMemberPaymentsUnpaid";
            public const string GetMemberPaymentsUnpaidById = Base + "/members/{id}/getAllMemberPaymentsUnpaidById";
            public const string UpdateMemberPayment = Base + "/members/{id}/updateMemberPayment";
            public const string CancelMemberPayment = Base + "/members/{id}/cancelMemberPayment";
            public const string CreateMemberPointRedemtions = Base + "/members/createMemberPointRedemtions";
            public const string CancelMemberPointRedemtions = Base + "/members/{id}/cancelMemberPointRedemtions";
            public const string GetMemberPointRedemtionsHistory = Base + "/members/getMemberPointRedemtionsHistory";
            public const string GetMemberAllPoint = Base + "/members/getMemberAllPoint";
        }

        public static class Receptions
        {
            public const string GetAll = Base + "/receptions";
            public const string Get = Base + "/receptions/{id}";
            public const string Create = Base + "/receptions/create";
            public const string Update = Base + "/receptions/{id}/update";
            public const string Delete = Base + "/receptions/{id}";
            public const string Sell = Base + "/receptions/sell";
            public const string CreateReceptionPointRedemtions = Base + "/receptions/createReceptionPointRedemtions";
            public const string CancelReceptionPointRedemtions = Base + "/receptions/{id}/cancelReceptionPointRedemtions";
            public const string GetReceptionPointRedemtionsHistory = Base + "/receptions/getReceptionPointRedemtionsHistory";
            public const string GetReceptionAllPoint = Base + "/receptions/getReceptionAllPoint";
        }

        public static class AngelDoctors
        {
            public const string GetAll = Base + "/angelDoctors";
            public const string Get = Base + "/angelDoctors/{id}";
            public const string Create = Base + "/angelDoctors/create";
            public const string Update = Base + "/angelDoctors/{id}/update";
            public const string Delete = Base + "/angelDoctors/{id}";
        }

        public static class DebtTypes
        {
            public const string GetAll = Base + "/debtTypes";
            public const string Get = Base + "/debtTypes/{id}";
            public const string Create = Base + "/debtTypes/create";
            public const string Update = Base + "/debtTypes/{id}/update";
            public const string Delete = Base + "/debtTypes/{id}";
        }

        public static class DeductTypes
        {
            public const string GetAll = Base + "/deductTypes";
            public const string Get = Base + "/deductTypes/{id}";
            public const string Create = Base + "/deductTypes/create";
            public const string Update = Base + "/deductTypes/{id}/update";
            public const string Delete = Base + "/deductTypes/{id}";
        }

        public static class Roles
        {
            public const string GetAll = Base + "/roles";
            public const string Get = Base + "/roles/{id}";
            public const string Create = Base + "/roles/create";
            public const string Update = Base + "/roles/{id}/update";
            public const string Delete = Base + "/roles/{id}";
        }

        public static class Users
        {
            public const string GetAll = Base + "/users";
            public const string Get = Base + "/users/{id}";
            public const string Create = Base + "/users/create";
            public const string Authenticate = Base + "/users/authenticate";
            public const string Update = Base + "/users/{id}/update";
            public const string Delete = Base + "/users/{id}";
            public const string ChangePassword = Base + "/users/{id}/changePassword";
        }

        public static class Massages
        {
            public const string Create = Base + "/massages/create";
            public const string CheckOutAngelAndRoom = Base + "/massages/checkOutAngelAndRoom";
            public const string CheckOutRoomTypeSuite = Base + "/massages/checkOutRoomTypeSuite";
            public const string UpdateRoundAngelAndIsNotCall = Base + "/massages/updateRoundAngelAndIsNotCall";
            public const string UpdateCheckInTime = Base + "/massages/updateCheckInTime";
            public const string ChangeReception = Base + "/massages/changeReception";
            public const string ChangeRoom = Base + "/massages/changeRoom";
            public const string UpdateIsNotCall = Base + "/massages/updateIsNotCall";
            public const string UpdateLastCall = Base + "/massages/updateLastCall";
            public const string GetAllCheckIn = Base + "/massages/getAllCheckIn";
            public const string GetAllCheckInRoom = Base + "/massages/getAllCheckInRoom";
            public const string GetAllCheckInRoomSuite = Base + "/massages/getAllCheckInRoomSuite";
            public const string GetAllCheckInChangeRoomSuite = Base + "/massages/getAllCheckInChangeRoomSuite";
            public const string GetCheckIn = Base + "/massages/{id}/getCheckIn";
            public const string Payment = Base + "/massages/{id}/payment";
            public const string CancelPayment = Base + "/massages/{id}/cancelPayment";
            public const string GetAllUnPaid = Base + "/massages/getAllUnPaid";
            public const string AddAngel = Base + "/massages/addAngel";
            public const string GetBillCheckIn = Base + "/massages/{id}/getBillCheckIn";
            public const string GetBillCheckInAngelMassage = Base + "/massages/{id}/getBillCheckInAngelMassage";
            public const string GetCalendarDay = Base + "/massages/getCalendarDay";
            public const string GetWagePayment = Base + "/massages/getWagePayment";
            public const string GetWagePaymentSuit = Base + "/massages/getWagePaymentSuit";
            public const string CancelAngelAndRoom = Base + "/massages/cancelAngelAndRoom";
            public const string CancelRoomTypeSuite = Base + "/massages/cancelRoomTypeSuite";
            public const string CheckMinuteChangeRoom = Base + "/massages/checkMinute";
            public const string UpdateIsChangeRoom = Base + "/massages/{id}/updateIsChangeRoom";
            public const string GetSellBill = Base + "/massages/sellBill";
            public const string ChangeReceptionBill = Base + "/massages/changeReceptionBill";
            public const string GetWagePaymentCancel = Base + "/massages/getWagePaymentCancel";
            public const string GetWagePaymentSuitCancel = Base + "/massages/getWagePaymentSuitCancel";
            public const string SaveBill = Base + "/massages/{id}/saveBill";
            public const string CancelCheckOutAngel = Base + "/massages/cancelCheckOutAngel";
            public const string CancelCheckOutRoom = Base + "/massages/cancelCheckOutRoom";
            public const string GetMassageRoomSuiteCheckIn = Base + "/massages/{id}/getMassageRoomSuiteCheckIn"; 
            public const string GetAllTipAngels = Base + "/massages/getAllTipAngels";
            public const string GetTipAngelById = Base + "/massages/getTipAngelById";
            public const string PaymentTip = Base + "/massages/paymentTip";
            public const string MergeRoomCheckIn = Base + "/massages/mergeRoomCheckIn";
            public const string MergeCheckInLater = Base + "/massages/mergeCheckInLater";
            public const string CancelAngelAndRoomCheckInLater = Base + "/massages/{id}/cancelAngelAndRoomCheckInLater";
            public const string GetMergeRoomCheckIn = Base + "/massages/{id}/getMergeRoomCheckIn";
            public const string MergePayment = Base + "/massages/{id}/mergePayment";
            public const string MergeSaveBill = Base + "/massages/{id}/mergesaveBill";
            public const string MergeCheckOutAngelAndRoom = Base + "/massages/mergeCheckOutAngelAndRoom";
            public const string MergeCheckOutRoomTypeSuite = Base + "/massages/mergecheckOutRoomTypeSuite";
            public const string MergeChangeRoom = Base + "/massages/mergechangeRoom";
            public const string GetMergeBillCheckInAngelMassage = Base + "/massages/{id}/getmergebillcheckinangelmassage";
        }

        public static class AngelPerformances
        {
            public const string GetAll = Base + "/angelPerformances";
            public const string Get = Base + "/angelPerformances/{id}";
            public const string Payment = Base + "/angelperformances/{id}/payment";
            public const string CancelPayment = Base + "/angelperformances/{id}/cancelPayment";
            public const string CancelIsPayPerformance = Base + "/angelPerformances/cancelIsPay";
            public const string Delete = Base + "/angelperformances/{id}";
        }

        public static class Menus
        {
            public const string GetAll = Base + "/menus";
        }

        public static class DailyDebts
        {
            public const string GetAll = Base + "/dailyDebts";
            public const string Get = Base + "/dailyDebts/{id}";
            public const string Create = Base + "/dailyDebts/create";
            public const string Update = Base + "/dailyDebts/{id}/update";
            public const string Delete = Base + "/dailyDebts/{id}";
        }

        public static class Reports
        {
            public const string ReportMemberSell = Base + "/reports/memberSell";
            public const string ReportMemberSellExcel = Base + "/reports/memberSellExcel";
            public const string ReportPaymentAngelPerformance = Base + "/reports/paymentAngelPerformance";
            public const string ReportSummaryDay = Base + "/reports/summaryDay";
            public const string ReportMemberPayment = Base + "/reports/memberPayment";
            public const string ReportDiscountMassageRoom = Base + "/reports/discountRoom";
            public const string ReportDiscountMassageAngel = Base + "/reports/discountAngel";
            public const string ReportUnpaidBill = Base + "/reports/unpaidBill";
            public const string ReportMemberExpire = Base + "/reports/memberExpire";
            public const string ReportMemberExpireExcel = Base + "/reports/memberExpireExcel";
            public const string ReportSummaryDayFinance = Base + "/reports/summaryDayFinace";
            public const string ReportReceptionSell = Base + "/reports/receptionSell";
            public const string ReportReceptionRoundTotal = Base + "/reports/receptionRoundTotal";
            public const string ReportTableCashier = Base + "/reports/tablecashier";
            public const string MaidCountAngelTypeTotal = Base + "/reports/maidCountAngelTypeTotal";
            public const string GetTotalRoundMonthExcel = Base + "/reports/getTotalRoundMonthExcel"; 
            public const string ReportMemberFood = Base + "/reports/memberFood"; 
            public const string SellSuite = Base + "/reports/sellSuite";
            public const string MemberTopup = Base + "/reports/membertopup";
            public const string MemberCreditAmount = Base + "/reports/memberCreditAmount";
            public const string MemberCreditAmountExcel = Base + "/reports/memberCreditAmountExcel";
            public const string ReportAngelDebt = Base + "/reports/angelDebt";
            public const string ReportAngelPerformanceExcel = Base + "/reports/angelPerformanceExcel";
            public const string ReportAllMemberExpire = Base + "/reports/allMemberExpire";
            public const string ReportAllMemberExpireExcel = Base + "/reports/allMemberExpireExcel";
            public const string ReportCountWorkDaily = Base + "/reports/countWorkDaily";
            public const string ReportIncomeDaily = Base + "/reports/incomeDaily";
            public const string ReportCountryies = Base + "/reports/getReportCountries";
            public const string ReportAgencies = Base + "/repots/getReportAgencies";
        }

        public static class CalendarDays
        {
            public const string GetBetweenDate = Base + "/calendarDays/getBetweenDate";
            public const string GetDashboard = Base + "/calendarDays/getDashboard";
            public const string CreateSetting = Base + "/calendarDays/createSetting";
            public const string UpdateSetting = Base + "/calendarDays/{id}/updateSetting";
            public const string GetSettingById = Base + "/settings/{id}";
            public const string GetCalendarByMonth = Base + "/calendarDays/getCalendarByMonth";
        }

        public static class Countries
        {
            public const string GetAll = Base + "/countries";
        }

        public static class Agencies
        {
            public const string GetAll = Base + "/agencies";
            public const string Get = Base + "/agencies/{id}";
            public const string Create = Base + "/agencies/create";
            public const string Update = Base + "/agencies/{id}/update";
            public const string Delete = Base + "/agencies/{id}";
            public const string GetAllAgencyTypes = Base + "/agencies/type";
        }
        public static class AgencyPerformances
        {
            public const string Create = Base + "/agencyPerformances/create";
            public const string Payment = Base + "/agencyPerformances/{id}/paymentAgencies";
            public const string CancelPayment = Base + "/agencyPerformances/{id}/cancelPaymentAgencies";
            public const string GetAllAgenciesPerformancAsync = Base + "/agencyPerformances";
            public const string Get = Base + "/agencyPerformances/{id}";
            
        }
        public static class Bookings
        {
            public const string Create = Base + "/bookings/create";
            public const string Delete = Base + "/bookings/{id}";
            public const string Update = Base + "/bookings/{id}/update";
            public const string GetAll = Base + "/bookings";
            public const string Get = Base + "/bookings/{id}";
            public const string GetBookingByDay = Base + "/bookings/bookingsDay";
            public const string GetListBooking = Base + "/bookings/getlistbooking";
            public const string CreateBookingAngels = Base + "/bookingAngels/create";
            public const string DeleteBookingAngels = Base + "/bookingAngels/{id}";
        }
        public static class Settings 
        {
            public const string Update = Base + "/settingsPoint/{id}/update";
            public const string Get = Base + "/settingsPoint/{id}";

        }
    }
}