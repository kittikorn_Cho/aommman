﻿using AutoMapper;
using DocumentFormat.OpenXml.Bibliography;
using DocumentFormat.OpenXml.Office2010.Excel;
using DocumentFormat.OpenXml.Office2016.Excel;
using DocumentFormat.OpenXml.VariantTypes;
using DocumentFormat.OpenXml.Wordprocessing;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using OONApi.Contracts.ApiRoutes;
using OONApi.Contracts.Requests;
using OONApi.Contracts.Requests.Queires;
using OONApi.Contracts.Responses;
using OONApi.Enum;
using OONApi.Hubs;
using OONApi.Models;
using OONApi.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using static OONApi.Contracts.ApiRoutes.ApiRoutes;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace OONApi.Controllers
{
    [ApiController]
    [Authorize]
    public class MassageController : ControllerBase
    {
        private IAgencyService _agencyService;
        private ISettingService _settingService;
        private IMassageService _massageService;
        private IReceptionService _receptionService;
        private IMapper _mapper;
        private ILogger<MassageController> _logger;
        private IAngelService _angelService;
        private IRoomService _roomService;
        private IRoomTypeService _roomTypeService;
        private IAngelPerformanceService _angelPerformanceService;
        private IDeductTypeService _deductTypeService;
        private IMemberService _memberService;
        private IUserService _userService;
        private IAgencyPerformanceService _agencyPerformanceService;
        private IMassageDebtService _massageDebtService;
        private readonly AppDbContext _db;
        private readonly IHubContext<CheckInHub> _checkInHub;
        public MassageController(IAgencyService agencyService, ISettingService settingService, IMassageService massageService, IMapper mapper, ILogger<MassageController> logger, IAngelService angelService, IReceptionService receptionService, IRoomService roomService, IRoomTypeService roomTypeService, IAngelPerformanceService angelPerformanceService, IDeductTypeService deductTypeService, IMemberService memberService, AppDbContext db, IHubContext<CheckInHub> checkInHub, IUserService userService, IAgencyPerformanceService agencyPerformanceService, IMassageDebtService massageDebtService)
        {
            _agencyService = agencyService;
            _settingService = settingService;
            _massageService = massageService;
            _receptionService = receptionService;
            _roomTypeService = roomTypeService;
            _userService = userService;
            _mapper = mapper;
            _logger = logger;
            _angelService = angelService;
            _roomService = roomService;
            _angelPerformanceService = angelPerformanceService;
            _deductTypeService = deductTypeService;
            _memberService = memberService;
            _db = db;
            _checkInHub = checkInHub;
            _agencyPerformanceService = agencyPerformanceService;
            _massageDebtService = massageDebtService;
        }


        // POST api/<MassageController>
        [HttpPost(ApiRoutes.Massages.Create)]
        public async Task<IActionResult> CheckIn([FromBody] MassageRequest request)
        {
            using (var transaction = await _db.Database.BeginTransactionAsync())
            {
                try
                {
                    int? calenDarDayId = await GetCalendarDayId();

                    string documentNumber = _settingService.GetDocumentNumberAndUpdate((int)calenDarDayId, false);

                    if (calenDarDayId == null)
                    {
                        return NotFound();
                    }
                    Massage massage = new Massage
                    {
                        CalendarDayId = (int)calenDarDayId,
                        IsPaid = false,
                        IsUnPaid = false,
                        Total = 0,
                        AngelTotal = 0,
                        FoodTotal = 0,
                        RoomTotal = 0,
                        IsCashAngel = false,
                        IsCreditAngel = false,
                        IsMemberAngel = false,
                        CashAngelTotal = 0,
                        CreditAngelTotal = 0,
                        MemberAngelTotal = 0,
                        IsCashFood = false,
                        IsCreditFood = false,
                        IsMemberFood = false,
                        CashFoodTotal = 0,
                        CreditFoodTotal = 0,
                        MemberFoodTotal = 0,
                        IsCashRoom = false,
                        IsCreditRoom = false,
                        IsMemberRoom = false,
                        CashRoomTotal = 0,
                        CreditRoomTotal = 0,
                        MemberRoomTotal = 0,
                        IsEntertainAngel = false,
                        EntertainTotalAngel = 0,
                        EntertainRemarkAngel = "",
                        IsEntertainFood = false,
                        EntertainTotalFood = 0,
                        EntertainRemarkFood = "",
                        PayDate = null,
                        DocumentNumber = documentNumber,
                        QrCodeTotal = 0,
                        IsQrCodeAngel = false,
                        QrCodeAngelTotal = 0,
                        IsQrCodeFood = false,
                        QrCodeFoodTotal = 0,
                        IsQrCodeRoom = false,
                        QrCodeRoomTotal = 0,
                        IsQrCodeEtc = false,
                        QrCodeEtcTotal = 0,
                        TipQrCodeTotal = 0,
                        CountriesId = request.CountriesId,
                        AgenciesId = request.AgenciesId
                    };
                    await _massageService.CreateAsync(massage);

                    List<MassageAngel> listAngels = new List<MassageAngel>();
                    List<int> angelIds = new List<int>();
                    DateTime dateNow;
                    bool checkTime = CheckTime(DateTime.Now);
                    if (checkTime)
                    {
                        dateNow = RoundUp(DateTime.Now, TimeSpan.FromMinutes(5));
                    }
                    else
                    {
                        dateNow = RoundDown(DateTime.Now, TimeSpan.FromMinutes(5));
                    }


                    foreach (var angel in request.MassageAngels)
                    {
                        MassageAngel massageAngel = new MassageAngel();
                        massageAngel.MassageId = massage.Id;
                        massageAngel.RoomId = angel.RoomId;
                        massageAngel.ReceptionId = angel.ReceptionId;
                        massageAngel.AngelId = angel.AngelId;
                        massageAngel.CheckInTime = dateNow;
                        massageAngel.CheckOutTime = null;
                        massageAngel.FirstCheckInTime = dateNow;
                        massageAngel.TimeMinute = 0;
                        massageAngel.Round = 1;
                        massageAngel.IsNotCall = angel.IsNotCall;
                        listAngels.Add(massageAngel);
                        angelIds.Add(massageAngel.AngelId);
                    }
                    await _massageService.CheckInMassageAngelList(listAngels);

                    foreach (var angel in listAngels)
                    {
                        var updateAngel = await _angelService.GetByIdAsync(angel.AngelId);
                        if (updateAngel == null)
                        {
                            return NotFound();
                        }
                        updateAngel.Status = (int)EAngelStatus.Working;
                        await _angelService.UpdateAsync(updateAngel);

                        var updateRoom = await _roomService.GetByIdAsync(angel.RoomId);
                        if (updateRoom == null)
                        {
                            return NotFound();
                        }
                        updateRoom.Status = (int)ERoomStatus.Working;
                        await _roomService.UpdateAsync(updateRoom);
                    }


                    // roomType == Suite and Vip
                    var room = await _roomService.GetByIdAsync((int)request.RoomId);
                    if (room != null)
                    {
                        if (room.RoomTypeId == (int)ERoomType.Vip || room.RoomTypeId == (int)ERoomType.Vip2 || room.RoomTypeId == (int)ERoomType.Vip3 || room.RoomTypeId == (int)ERoomType.Suite || room.RoomTypeId == (int)ERoomType.Suite3 || room.RoomTypeId == (int)ERoomType.Suite4 || room.RoomTypeId == (int)ERoomType.Suite5 || room.RoomTypeId == (int)ERoomType.Suite6 || room.RoomTypeId == (int)ERoomType.Suite7 || room.RoomTypeId == (int)ERoomType.Suite8 || room.RoomTypeId == (int)ERoomType.Suite9 || room.RoomTypeId == (int)ERoomType.Suite10 || room.RoomTypeId == (int)ERoomType.C3 || room.RoomTypeId == (int)ERoomType.C5)
                        {
                            MassageRoom massageRoom = new MassageRoom();
                            massageRoom.MassageId = massage.Id;
                            massageRoom.RoomId = (int)request.RoomId;
                            massageRoom.ReceptionId = (int)request.ReceptionId;
                            massageRoom.CheckInTime = dateNow;
                            massageRoom.CheckOutTime = null;
                            massageRoom.OldCheckInTime = dateNow;
                            massageRoom.TimeMinute = 0;
                            massageRoom.LastCallTime = null;
                            massageRoom.CallCount = 0;
                            massageRoom.IsNotCall = request.IsNotCall;

                            await _massageService.CheckInMassageRoom(massageRoom);

                            room.Status = (int)ERoomStatus.Working;
                            await _roomService.UpdateAsync(room);
                        }
                    }
                    var agencies = await _agencyService.GetByIdAsync(request.AgenciesId);
                    if (agencies == null)
                    {
                        return NotFound();
                    }
                    var agenciesPerformance = await _agencyService.GetAgenciesPerformanceByCalendarDay((int)calenDarDayId, agencies.Id);

                    bool ispayAgency = false;
                    int i = 0;
                    int countAgency = agenciesPerformance.Count;
                    foreach (var item in agenciesPerformance)
                    {
                        i++;
                        if (item.IsPay)
                        {
                            if (i == countAgency)
                            {
                                ispayAgency = true;
                            }
                        }
                    }
                    if (request.AgenciesId != 1)
                    {
                        if (countAgency == 0 || ispayAgency)
                        {
                            AgencyPerformance agencyPerformance = new AgencyPerformance
                            {
                                CarlendarDayId = (int)calenDarDayId,
                                AgenciesId = request.AgenciesId,
                                IsPay = false,
                                IsUnPaid = false,
                                TotalMassage = 0,
                                TotalRound = 0,
                                TotalMember = 0,
                                TotalReceive = 0,
                                Remark = ""
                            };
                            await _agencyPerformanceService.CreatePaymentAgencyAsync(agencyPerformance);

                            var updateMassage = await _massageService.GetById(massage.Id);
                            updateMassage.AgencyPerformanceId = agencyPerformance.Id;
                            await _massageService.UpdateAsync(updateMassage);
                        }
                        else
                        {
                            var oldAgencyPerformance = await _agencyPerformanceService.GetAgencyPerformancesByCalendarIdAndAgenciesId((int)calenDarDayId, request.AgenciesId);

                            var updateMassage = await _massageService.GetById(massage.Id);
                            updateMassage.AgencyPerformanceId = oldAgencyPerformance.Id;
                            await _massageService.UpdateAsync(updateMassage);
                        }
                    }



                    _settingService.GetDocumentNumberAndUpdate((int)calenDarDayId, true);

                    //end
                    await transaction.CommitAsync();


                    var _room = await _roomService.GetByIdAsync(room.Id);
                    var roomResponse = _mapper.Map<RoomResponse>(_room);

                    List<Angel> listAngel = new List<Angel>();
                    foreach (var item in angelIds)
                    {
                        Angel angel = new Angel();
                        angel = await _angelService.GetByIdAsync(item);
                        listAngel.Add(angel);
                    }

                    var angelResponse = _mapper.Map<List<AngelResponse>>(listAngel);

                    var massageResponse = _mapper.Map<MassageResponse>(massage);
                    var massageAngels = await _massageService.GetMassageAngelByMassageId(massage.Id);
                    var massageRooms = await _massageService.GetMassageRoomByMassageId(massage.Id);
                    massageResponse.massageAngels = _mapper.Map<List<MassageAngelResponse>>(massageAngels);
                    massageResponse.massageRoom = _mapper.Map<MassageRoomResponse>(massageRooms);


                    await _checkInHub.Clients.All.SendAsync("AddCheckIn", massageResponse);
                    await _checkInHub.Clients.All.SendAsync("RoomStatusChange", roomResponse);
                    await _checkInHub.Clients.All.SendAsync("AngelStatusChange", angelResponse);

                    return Ok(massageResponse);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex.Message);
                    _logger.LogError(ex.StackTrace);
                    await transaction.RollbackAsync();
                    return BadRequest();
                }
            }
        }

        [HttpGet(ApiRoutes.Massages.GetCheckIn)]
        public async Task<IActionResult> GetCheckInById(int id)
        {
            try
            {
                var massage = await _massageService.GetById(id);
                if (massage == null)
                {
                    return NotFound();
                }
                var massageResponse = _mapper.Map<MassageResponse>(massage);
                if (massageResponse == null)
                {
                    return NotFound();
                }
                var massageRoom = await _massageService.GetMassageRoomByMassageId(massage.Id);
                massageResponse.massageRoom = _mapper.Map<MassageRoomResponse>(massageRoom);
                var massageAngels = await _massageService.GetMassageAngelByMassageId(massage.Id);
                if (massageAngels == null)
                {
                    return NotFound();
                }
                massageResponse.massageAngels = _mapper.Map<List<MassageAngelResponse>>(massageAngels);
                var massageCreditPayment = await _massageService.GetMassageCreditPaymentByMassageId(massage.Id);
                massageResponse.massageCreditPayment = _mapper.Map<MassageCreditPaymentResponse>(massageCreditPayment);
                var massageMemberPayment = await _massageService.GetMassageMemberPaymentsByMassageId(massage.Id);
                massageResponse.massageMemberPayments = _mapper.Map<List<MassageMemberPaymentResponse>>(massageMemberPayment);
                var massageQrCodePayment = await _massageService.GetMassageQrCodePaymentByMassageId(massage.Id);
                massageResponse.massageQrCodePayment = _mapper.Map<MassageQrCodePaymentResponse>(massageQrCodePayment);

                if (massage.UpdatedBy != null)
                {
                    var user = await _userService.GetByIdAsync((int)massage.UpdatedBy);
                    massageResponse.user = _mapper.Map<UserResponse>(user);
                }
                foreach (var memberResponse in massageResponse.massageMemberPayments)
                {
                    var massageMember = await _memberService.GetByIdAsync(memberResponse.MemberId);
                    memberResponse.member = _mapper.Map<MemberResponse>(massageMember);
                }

                return Ok(massageResponse);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }

        [HttpPatch(ApiRoutes.Massages.AddAngel)]
        public async Task<IActionResult> AddAngel([FromBody] UpdateMassageAngelRequest request)
        {
            using (var transaction = await _db.Database.BeginTransactionAsync())
            {
                try
                {
                    DateTime dateNow;
                    bool checkTime = CheckTime(DateTime.Now);
                    if (checkTime)
                    {
                        dateNow = RoundUp(DateTime.Now, TimeSpan.FromMinutes(5));
                    }
                    else
                    {
                        dateNow = RoundDown(DateTime.Now, TimeSpan.FromMinutes(5));
                    }
                    List<MassageAngel> list = new List<MassageAngel>();
                    foreach (var item in request.MassageAngels)
                    {
                        MassageAngel angel = new MassageAngel();
                        angel.MassageId = item.MassageId;
                        angel.ReceptionId = item.ReceptionId;
                        angel.RoomId = item.RoomId;
                        angel.AngelId = item.AngelId;
                        angel.Round = 1;
                        angel.CheckInTime = dateNow;
                        angel.FirstCheckInTime = dateNow;
                        angel.IsNotCall = item.IsNotCall;
                        list.Add(angel);
                    }

                    await _massageService.CheckInMassageAngelList(list);


                    foreach (var angel in list)
                    {
                        var updateAngel = await _angelService.GetByIdAsync(angel.AngelId);
                        if (updateAngel == null)
                        {
                            return NotFound();
                        }
                        updateAngel.Status = (int)EAngelStatus.Working;
                        await _angelService.UpdateAsync(updateAngel);

                        var updateRoom = await _roomService.GetByIdAsync(angel.RoomId);
                        if (updateRoom == null)
                        {
                            return NotFound();
                        }
                        updateRoom.Status = (int)ERoomStatus.Working;
                        await _roomService.UpdateAsync(updateRoom);

                    }

                    var massages = await _massageService.GetById(list[0].MassageId);
                    var massageResponse = _mapper.Map<MassageResponse>(massages);
                    var massageAngels = await _massageService.GetMassageAngelByMassageId(massageResponse.Id);
                    massageResponse.massageAngels = _mapper.Map<List<MassageAngelResponse>>(massageAngels);
                    var massageRooms = await _massageService.GetMassageRoomByMassageId(massageResponse.Id);
                    massageResponse.massageRoom = _mapper.Map<MassageRoomResponse>(massageRooms);

                    await transaction.CommitAsync();
                    return Ok(massageResponse);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex.Message);
                    _logger.LogError(ex.StackTrace);
                    await transaction.RollbackAsync();
                    return BadRequest();
                }
            }
        }
        [HttpPut(ApiRoutes.Massages.SaveBill)]
        public async Task<IActionResult> SaveBill([FromRoute] int id, [FromBody] PaymentRequest request)
        {
            using (var transaction = await _db.Database.BeginTransactionAsync())
            {
                try
                {
                    if (id <= 0)
                    {
                        return BadRequest();
                    }
                    var massage = await _massageService.GetById(id);
                    var oldAgencies = massage.AgenciesId;
                    if (massage == null)
                    {
                        return NotFound();
                    }
                    bool oldUnPaid = massage.IsUnPaid;

                    massage.IsPaid = false;
                    massage.IsUnPaid = false;
                    massage.Total = request.Total;
                    massage.AngelTotal = request.AngelTotal;
                    massage.FoodTotal = request.FoodTotal;
                    massage.RoomTotal = request.RoomTotal;
                    massage.IsCashAngel = request.IsCashAngel;
                    massage.IsCreditAngel = request.IsCreditAngel;
                    massage.IsMemberAngel = request.IsMemberAngel;
                    massage.CashAngelTotal = request.CashAngelTotal;
                    massage.CreditAngelTotal = request.CreditAngelTotal;
                    massage.MemberAngelTotal = request.MemberAngelTotal;
                    massage.IsCashFood = request.IsCashFood;
                    massage.IsCreditFood = request.IsCreditFood;
                    massage.IsMemberFood = request.IsMemberFood;
                    massage.CashFoodTotal = request.CashFoodTotal;
                    massage.CreditFoodTotal = request.CreditFoodTotal;
                    massage.MemberFoodTotal = request.MemberFoodTotal;
                    massage.IsCashRoom = request.IsCashRoom;
                    massage.IsCreditRoom = request.IsCreditRoom;
                    massage.IsMemberRoom = request.IsMemberRoom;
                    massage.CashRoomTotal = request.CashRoomTotal;
                    massage.CreditRoomTotal = request.CreditRoomTotal;
                    massage.MemberRoomTotal = request.MemberRoomTotal;
                    massage.IsEntertainAngel = request.IsEntertainAngel;
                    massage.EntertainTotalAngel = request.EntertainTotalAngel;
                    massage.EntertainRemarkAngel = request.EntertainRemarkAngel;
                    massage.IsEntertainFood = request.IsEntertainFood;
                    massage.EntertainTotalFood = request.EntertainTotalFood;
                    massage.EntertainRemarkFood = request.EntertainRemarkFood;
                    massage.EtcTotal = request.EtcTotal;
                    massage.IsCashEtc = request.IsCashEtc;
                    massage.IsCreditEtc = request.IsCreditEtc;
                    massage.IsMemberEtc = request.IsMemberEtc;
                    massage.IsEntertainEtc = request.IsEntertainEtc;
                    massage.EntertainEtcRemark = request.EntertainEtcRemark;
                    massage.CashEtcTotal = request.CashEtcTotal;
                    massage.CreditEtcTotal = request.CreditEtcTotal;
                    massage.MemberEtcTotal = request.MemberEtcTotal;
                    massage.EntertainEtcTotal = request.EntertainEtcTotal;
                    massage.TipTotal = request.TipTotal;
                    massage.TipCommTotal = request.TipCommTotal;
                    massage.DamagesTotal = request.DamagesTotal;
                    massage.OtherServiceChargesTotal = request.OtherServiceChargesTotal;
                    massage.EtcRemark = request.EtcRemark;
                    massage.PayDate = null;
                    massage.CashierId = request.CashierId;

                    massage.QrCodeTotal = request.QrCodeTotal;
                    massage.IsQrCodeAngel = request.IsQrCodeAngel;
                    massage.QrCodeAngelTotal = request.QrCodeAngelTotal;
                    massage.IsQrCodeFood = request.IsQrCodeFood;
                    massage.QrCodeFoodTotal = request.QrCodeFoodTotal;
                    massage.IsQrCodeRoom = request.IsQrCodeRoom;
                    massage.QrCodeRoomTotal = request.QrCodeRoomTotal;
                    massage.IsQrCodeEtc = request.IsQrCodeEtc;
                    massage.QrCodeEtcTotal = request.QrCodeEtcTotal;

                    massage.TipQrCodeTotal = request.TipQrCodeTotal;
                    massage.CountriesId = (int)request.CountriesId;
                    massage.AgenciesId = (int)request.AgenciesId;
                    massage.AgencyPerformanceId = (int)request.AgencyPerformanceId;

                    if (oldUnPaid && !request.IsUnPaid)
                    {
                        massage.PayUnPaidDate = DateTime.Now;
                    }

                    await _massageService.UpdateAsync(massage);

                    if (massage.IsCreditAngel || massage.IsCreditFood || massage.IsCreditRoom || massage.IsCreditEtc)
                    {
                        MassageCreditPayment _credit = new MassageCreditPayment();
                        _credit.MassageId = massage.Id;
                        _credit.CardNo = request.PaymentCredit.CardNo;
                        _credit.CardType = request.PaymentCredit.CardType;
                        _credit.CreditTotal = request.PaymentCredit.CreditTotal;
                        await _massageService.CreateMassagePaymentCredit(_credit);
                    }

                    if (massage.IsMemberAngel || massage.IsMemberFood || massage.IsMemberRoom || massage.IsMemberEtc)
                    {
                        foreach (var member in request.PaymentMembers)
                        {
                            MassageMemberPayment _member = new MassageMemberPayment();
                            _member.MassageId = massage.Id;
                            _member.MemberId = (int)member.MemberId;
                            _member.MemberTotal = member.MemberTotal;
                            _member.UseSuite = member.UseSuite;
                            _member.UseVip = member.UseVip;
                            await _massageService.CreateMassagePaymentMember(_member);

                            //update credit member
                            var getMember = await _memberService.GetByIdAsync(_member.MemberId);
                            if (getMember == null)
                            {
                                return NotFound();
                            }
                            getMember.CreditAmount -= _member.MemberTotal;
                            await _memberService.UpdateAsync(getMember);
                        }
                    }

                    if (massage.IsQrCodeAngel || massage.IsQrCodeFood || massage.IsQrCodeRoom || massage.IsQrCodeEtc)
                    {
                        MassageQrCodePayment _qrcode = new MassageQrCodePayment();
                        _qrcode.MassageId = massage.Id;
                        _qrcode.QrCodeTotal = request.PaymentQrCode.QrCodeTotal;

                        await _massageService.CreateMassageQrCode(_qrcode);
                    }

                    int? paymentTypeId = null;

                    if (request.IsCashAngel && request.IsCreditAngel && request.IsMemberAngel && request.IsQrCodeAngel)
                    {
                        paymentTypeId = (int)EPaymentType.CashAndCreditAndMemberAndQrCode;
                    }
                    else if (request.IsCashAngel && request.IsCreditAngel && request.IsMemberAngel)
                    {
                        paymentTypeId = (int)EPaymentType.CashAndCreditAndMember;
                    }
                    else if (request.IsCashAngel && request.IsCreditAngel && request.IsQrCodeAngel)
                    {
                        paymentTypeId = (int)EPaymentType.CashAndCreditAndQrCode;
                    }
                    else if (request.IsCashAngel && request.IsMemberAngel && request.IsQrCodeAngel)
                    {
                        paymentTypeId = (int)EPaymentType.CashAndMemberAndQrCode;
                    }
                    else if (request.IsCreditAngel && request.IsMemberAngel && request.IsQrCodeAngel)
                    {
                        paymentTypeId = (int)EPaymentType.CreditAndMemberAndQrCode;
                    }

                    else if (request.IsCashAngel && request.IsCreditAngel)
                    {
                        paymentTypeId = (int)EPaymentType.CashAndCredit;
                    }
                    else if (request.IsCashAngel && request.IsMemberAngel)
                    {
                        if (request.MemberAngelTotal == 0)
                        {
                            paymentTypeId = (int)EPaymentType.Cash;
                        }
                        else
                        {
                            paymentTypeId = (int)EPaymentType.CashAndMember;
                        }
                    }
                    else if (request.IsCashAngel && request.IsQrCodeAngel)
                    {
                        paymentTypeId = (int)EPaymentType.CashAndQrCode;
                    }
                    else if (request.IsCreditAngel && request.IsMemberAngel)
                    {
                        paymentTypeId = (int)EPaymentType.CreditAndMember;
                    }
                    else if (request.IsCreditAngel && request.IsQrCodeAngel)
                    {
                        paymentTypeId = (int)EPaymentType.CreditAndQrCode;
                    }
                    else if (request.IsMemberAngel && request.IsQrCodeAngel)
                    {
                        paymentTypeId = (int)EPaymentType.MemberAndQrCode;
                    }


                    else if (request.IsCashAngel && !request.IsCreditAngel && !request.IsMemberAngel && !request.IsQrCodeAngel)
                    {
                        paymentTypeId = (int)EPaymentType.Cash;
                    }
                    else if (request.IsCreditAngel && !request.IsCashAngel && !request.IsMemberAngel && !request.IsQrCodeAngel)
                    {
                        paymentTypeId = (int)EPaymentType.Credit;
                    }
                    else if (request.IsMemberAngel && !request.IsCashAngel && !request.IsCreditAngel && !request.IsQrCodeAngel)
                    {
                        paymentTypeId = (int)EPaymentType.Member;
                    }
                    else if (request.IsQrCodeAngel && !request.IsCashAngel && !request.IsCreditAngel && !request.IsMemberAngel)
                    {
                        paymentTypeId = (int)EPaymentType.QrCode;
                    }

                    else
                    {
                        paymentTypeId = (int)EPaymentType.NoPay;
                    }

                    var massageAngels = await _massageService.GetMassageAngelByMassageId(id);

                    foreach (var massageAngel in massageAngels)
                    {
                        _db.ChangeTracker.Clear();
                        MassageAngel _massageAngel = massageAngel;
                        massageAngel.PaymentTypeId = paymentTypeId;
                        await _massageService.UpdateMassageAngel(_massageAngel);
                    }

                    foreach (var massageAngelRound in request.MassageAngels)
                    {
                        var _massageAngelUpdate = await _massageService.GetMassageAngelById(massageAngelRound.Id);
                        _massageAngelUpdate.Round = massageAngelRound.Round;

                        if (massageAngelRound.IsDiscount || !massageAngelRound.IsDiscount)
                        {
                            _massageAngelUpdate.IsDiscount = massageAngelRound.IsDiscount;
                            _massageAngelUpdate.IsDiscountBaht = massageAngelRound.IsDiscountBaht;
                            _massageAngelUpdate.DiscountBaht = massageAngelRound.DiscountBaht;
                            _massageAngelUpdate.IsDiscountPercent = massageAngelRound.IsDiscountPercent;
                            _massageAngelUpdate.DiscountPercent = massageAngelRound.DiscountPercent;
                            _massageAngelUpdate.IsDiscountAngelRound = massageAngelRound.IsDiscountAngelRound;
                            _massageAngelUpdate.DiscountAngelRound = massageAngelRound.DiscountAngelRound;
                            _massageAngelUpdate.DiscountRemark = massageAngelRound.DiscountRemark;
                        }

                        await _massageService.UpdateMassageAngel(_massageAngelUpdate);
                    }

                    if (request.MassageRoom != null)
                    {
                        if (request.MassageRoom.IsDiscount || !request.MassageRoom.IsDiscount)
                        {
                            var _massageRoom = await _massageService.GetMassageRoomById(request.MassageRoom.Id);
                            _massageRoom.IsDiscountBaht = request.MassageRoom.IsDiscountBaht;
                            _massageRoom.DiscountBaht = request.MassageRoom.DiscountBaht;
                            _massageRoom.IsDiscountPercent = request.MassageRoom.IsDiscountPercent;
                            _massageRoom.DiscountPercent = request.MassageRoom.DiscountPercent;
                            _massageRoom.DiscountRemark = request.MassageRoom.DiscountRemark;
                            _massageRoom.IsDiscount = request.MassageRoom.IsDiscount;

                            await _massageService.UpdateMassageRoom(_massageRoom);
                        }
                    }



                    var massageRoom = await _massageService.GetMassageRoomByMassageId(massage.Id);
                    if (request.MemberItem.MemberId != null)
                    {
                        var itemMember = await _memberService.GetByIdAsync((int)request.MemberItem.MemberId);
                        Member _memberItem = itemMember;
                        foreach (var item in _memberItem.MemberItems)
                        {
                            _db.ChangeTracker.Clear();
                            if (item.ItemId == request.MemberItem.ItemId)
                            {
                                item.Amount = item.Amount - request.Round;
                                await _memberService.UpdateMemberItem(item);
                            }
                        }
                    }

                    if (oldAgencies != request.AgenciesId)
                    {
                        var agencyPerformance = await _agencyPerformanceService.GetAgencyPerformancesByCalendarIdAndAgenciesId(massage.CalendarDayId, oldAgencies);
                        agencyPerformance.AgenciesId = (int)request.AgenciesId;
                        await _agencyPerformanceService.UpdateAsync(agencyPerformance);
                    }

                    massage = await _massageService.GetById(id);
                    var massageResponse = _mapper.Map<MassageResponse>(massage);
                    if (massageResponse == null)
                    {
                        return NotFound();
                    }
                    var user = await _userService.GetByIdAsync((int)massage.UpdatedBy);
                    massageResponse.user = _mapper.Map<UserResponse>(user);
                    massageAngels = await _massageService.GetMassageAngelByMassageId(id);
                    massageRoom = await _massageService.GetMassageRoomByMassageId(massage.Id);
                    massageResponse.massageRoom = _mapper.Map<MassageRoomResponse>(massageRoom);
                    massageResponse.massageAngels = _mapper.Map<List<MassageAngelResponse>>(massageAngels);
                    var massageCreditPayment = await _massageService.GetMassageCreditPaymentByMassageId(massage.Id);
                    massageResponse.massageCreditPayment = _mapper.Map<MassageCreditPaymentResponse>(massageCreditPayment);
                    var massageMemberPayment = await _massageService.GetMassageMemberPaymentsByMassageId(massage.Id);
                    massageResponse.massageMemberPayments = _mapper.Map<List<MassageMemberPaymentResponse>>(massageMemberPayment);

                    var massageQrCodePayment = await _massageService.GetMassageQrCodePaymentByMassageId(massage.Id);
                    massageResponse.massageQrCodePayment = _mapper.Map<MassageQrCodePaymentResponse>(massageQrCodePayment);

                    foreach (var memberResponse in massageResponse.massageMemberPayments)
                    {
                        var massageMember = await _memberService.GetByIdAsync(memberResponse.MemberId);
                        memberResponse.member = _mapper.Map<MemberResponse>(massageMember);
                    }

                    await transaction.CommitAsync();
                    return Ok(massageResponse);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex.Message);
                    _logger.LogError(ex.StackTrace);
                    await transaction.RollbackAsync();
                    return BadRequest();
                }
            }
        }

        [HttpPut(ApiRoutes.Massages.Payment)]
        public async Task<IActionResult> Payment([FromRoute] int id, [FromBody] PaymentRequest request)
        {
            using (var transaction = await _db.Database.BeginTransactionAsync())
            {
                try
                {
                    if (id <= 0)
                    {
                        return BadRequest();
                    }
                    var massage = await _massageService.GetById(id);
                    var oldAgencies = massage.AgenciesId;
                    if (massage == null)
                    {
                        return NotFound();
                    }
                    bool oldUnPaid = massage.IsUnPaid;

                    massage.IsPaid = request.IsPaid;
                    massage.IsUnPaid = request.IsUnPaid;
                    massage.Total = request.Total;
                    massage.AngelTotal = request.AngelTotal;
                    massage.FoodTotal = request.FoodTotal;
                    massage.RoomTotal = request.RoomTotal;
                    massage.IsCashAngel = request.IsCashAngel;
                    massage.IsCreditAngel = request.IsCreditAngel;
                    massage.IsMemberAngel = request.IsMemberAngel;
                    massage.IsQrCodeAngel = request.IsQrCodeAngel;
                    massage.CashAngelTotal = request.CashAngelTotal;
                    massage.CreditAngelTotal = request.CreditAngelTotal;
                    massage.MemberAngelTotal = request.MemberAngelTotal;
                    massage.QrCodeAngelTotal = request.QrCodeAngelTotal;
                    massage.IsCashFood = request.IsCashFood;
                    massage.IsCreditFood = request.IsCreditFood;
                    massage.IsMemberFood = request.IsMemberFood;
                    massage.IsQrCodeFood = request.IsQrCodeFood;
                    massage.CashFoodTotal = request.CashFoodTotal;
                    massage.CreditFoodTotal = request.CreditFoodTotal;
                    massage.MemberFoodTotal = request.MemberFoodTotal;
                    massage.QrCodeFoodTotal = request.QrCodeFoodTotal;
                    massage.IsCashRoom = request.IsCashRoom;
                    massage.IsCreditRoom = request.IsCreditRoom;
                    massage.IsMemberRoom = request.IsMemberRoom;
                    massage.IsQrCodeRoom = request.IsQrCodeRoom;
                    massage.CashRoomTotal = request.CashRoomTotal;
                    massage.CreditRoomTotal = request.CreditRoomTotal;
                    massage.MemberRoomTotal = request.MemberRoomTotal;
                    massage.QrCodeRoomTotal = request.QrCodeRoomTotal;
                    massage.IsEntertainAngel = request.IsEntertainAngel;
                    massage.EntertainTotalAngel = request.EntertainTotalAngel;
                    massage.EntertainRemarkAngel = request.EntertainRemarkAngel;
                    massage.IsEntertainFood = request.IsEntertainFood;
                    massage.EntertainTotalFood = request.EntertainTotalFood;
                    massage.EntertainRemarkFood = request.EntertainRemarkFood;
                    massage.EtcTotal = request.EtcTotal;
                    massage.IsCashEtc = request.IsCashEtc;
                    massage.IsCreditEtc = request.IsCreditEtc;
                    massage.IsMemberEtc = request.IsMemberEtc;
                    massage.IsEntertainEtc = request.IsEntertainEtc;
                    massage.IsQrCodeEtc = request.IsQrCodeEtc;
                    massage.EntertainEtcRemark = request.EntertainEtcRemark;
                    massage.CashEtcTotal = request.CashEtcTotal;
                    massage.CreditEtcTotal = request.CreditEtcTotal;
                    massage.MemberEtcTotal = request.MemberEtcTotal;
                    massage.EntertainEtcTotal = request.EntertainEtcTotal;
                    massage.QrCodeEtcTotal = request.QrCodeEtcTotal;
                    massage.TipTotal = request.TipTotal;
                    massage.TipCommTotal = request.TipCommTotal;
                    massage.DamagesTotal = request.DamagesTotal;
                    massage.OtherServiceChargesTotal = request.OtherServiceChargesTotal;
                    massage.EtcRemark = request.EtcRemark;
                    massage.UnPaidRemark = request.UnPaidRemark;

                    massage.TipQrCodeTotal = request.TipQrCodeTotal;
                    massage.CountriesId = (int)request.CountriesId;
                    massage.AgenciesId = (int)request.AgenciesId;
                    massage.AgencyPerformanceId = (int)request.AgencyPerformanceId;

                    if (massage.CancelDate == null)
                    {
                        massage.PayDate = DateTime.Now;
                    }
                    else
                    {
                        massage.CancelDate = null;
                    }

                    massage.CashierId = request.CashierId;
                    if (oldUnPaid && !request.IsUnPaid)
                    {
                        massage.PayUnPaidDate = DateTime.Now;

                        var massageDebt = await _massageDebtService.GetMassageDebtByMassageId(massage.Id);
                        if(massageDebt != null)
                        {
                            MassageDebt _massageDebt = await _massageDebtService.GetByIdAsync(massageDebt.Id);

                            _massageDebt.CashTotal = request.CashAngelTotal;
                            _massageDebt.CreditTotal = request.CreditAngelTotal;
                            _massageDebt.MemberTotal = request.MemberAngelTotal;
                            _massageDebt.EntertainTotal = request.EntertainTotalAngel;
                            _massageDebt.QrTotal = request.QrCodeAngelTotal;

                            await _massageDebtService.UpdateAsync(_massageDebt);
                           
                        }

                    }
                   
                    await _massageService.UpdateAsync(massage);

                  

                    if (massage.IsCreditAngel || massage.IsCreditFood || massage.IsCreditRoom || massage.IsCreditEtc)
                    {
                        var _massageCreditPayment = await _massageService.GetMassageCreditPaymentByMassageId(massage.Id);
                        if (_massageCreditPayment != null)
                        {
                            var creditPayment = await _massageService.GetMassageCreditPaymentById(_massageCreditPayment.Id);
                            creditPayment.MassageId = massage.Id;
                            creditPayment.CardNo = request.PaymentCredit.CardNo;
                            creditPayment.CardType = request.PaymentCredit.CardType;
                            creditPayment.CreditTotal = request.PaymentCredit.CreditTotal;
                            await _massageService.UpdateMassagePaymentCredit(creditPayment);
                        }
                        else
                        {
                            MassageCreditPayment _credit = new MassageCreditPayment();
                            _credit.MassageId = massage.Id;
                            _credit.CardNo = request.PaymentCredit.CardNo;
                            _credit.CardType = request.PaymentCredit.CardType;
                            _credit.CreditTotal = request.PaymentCredit.CreditTotal;
                            await _massageService.CreateMassagePaymentCredit(_credit);
                        }
                    }

                    if (massage.IsMemberAngel || massage.IsMemberFood || massage.IsMemberRoom || massage.IsMemberEtc)
                    {
                        foreach (var member in request.PaymentMembers)
                        {
                            MassageMemberPayment _member = new MassageMemberPayment();
                            _member.MassageId = massage.Id;
                            _member.MemberId = (int)member.MemberId;
                            _member.MemberTotal = member.MemberTotal;
                            _member.UseSuite = member.UseSuite;
                            _member.UseVip = member.UseVip;
                            _member.MemberFoodTotal = member.MemberFoodTotal;
                            _member.MemberRoomTotal = member.MemberRoomTotal;
                            _member.MemberMassageTotal = member.MemberMassageTotal;
                            await _massageService.CreateMassagePaymentMember(_member);

                            //update credit member
                            var getMember = await _memberService.GetByIdAsync(_member.MemberId);
                            if (getMember == null)
                            {
                                return NotFound();
                            }
                            getMember.CreditAmount -= _member.MemberTotal;
                            await _memberService.UpdateAsync(getMember);
                        }
                    }

                    if (massage.IsQrCodeAngel || massage.IsQrCodeFood || massage.IsQrCodeRoom || massage.IsQrCodeEtc)
                    {
                        var _massageQrCodePayment = await _massageService.GetMassageQrCodePaymentByMassageId(massage.Id);
                        if (_massageQrCodePayment != null)
                        {
                            var qrCodePayment = await _massageService.GetMassageQrCodePaymentById(_massageQrCodePayment.Id);
                            qrCodePayment.MassageId = massage.Id;
                            qrCodePayment.QrCodeTotal = request.PaymentQrCode.QrCodeTotal;

                            await _massageService.UpdateMassagePaymentQrCode(qrCodePayment);
                        }
                        else
                        {
                            MassageQrCodePayment _qrcode = new MassageQrCodePayment();
                            _qrcode.MassageId = massage.Id;
                            _qrcode.QrCodeTotal = request.PaymentQrCode.QrCodeTotal;

                            await _massageService.CreateMassageQrCode(_qrcode);
                        }

                    }

                    int? paymentTypeId = null;

                    if (request.IsCashAngel && request.IsCreditAngel && request.IsMemberAngel && request.IsQrCodeAngel)
                    {
                        paymentTypeId = (int)EPaymentType.CashAndCreditAndMemberAndQrCode;
                    }
                    else if (request.IsCashAngel && request.IsCreditAngel && request.IsMemberAngel)
                    {
                        paymentTypeId = (int)EPaymentType.CashAndCreditAndMember;
                    }
                    else if (request.IsCashAngel && request.IsCreditAngel && request.IsQrCodeAngel)
                    {
                        paymentTypeId = (int)EPaymentType.CashAndCreditAndQrCode;
                    }
                    else if (request.IsCashAngel && request.IsMemberAngel && request.IsQrCodeAngel)
                    {
                        paymentTypeId = (int)EPaymentType.CashAndMemberAndQrCode;
                    }
                    else if (request.IsCreditAngel && request.IsMemberAngel && request.IsQrCodeAngel)
                    {
                        paymentTypeId = (int)EPaymentType.CreditAndMemberAndQrCode;
                    }

                    else if (request.IsCashAngel && request.IsCreditAngel)
                    {
                        paymentTypeId = (int)EPaymentType.CashAndCredit;
                    }
                    else if (request.IsCashAngel && request.IsMemberAngel)
                    {
                        if (request.MemberAngelTotal == 0)
                        {
                            paymentTypeId = (int)EPaymentType.Cash;
                        }
                        else
                        {
                            paymentTypeId = (int)EPaymentType.CashAndMember;
                        }
                    }
                    else if (request.IsCashAngel && request.IsQrCodeAngel)
                    {
                        paymentTypeId = (int)EPaymentType.CashAndQrCode;
                    }
                    else if (request.IsCreditAngel && request.IsMemberAngel)
                    {
                        paymentTypeId = (int)EPaymentType.CreditAndMember;
                    }
                    else if (request.IsCreditAngel && request.IsQrCodeAngel)
                    {
                        paymentTypeId = (int)EPaymentType.CreditAndQrCode;
                    }
                    else if (request.IsMemberAngel && request.IsQrCodeAngel)
                    {
                        paymentTypeId = (int)EPaymentType.MemberAndQrCode;
                    }


                    else if (request.IsCashAngel && !request.IsCreditAngel && !request.IsMemberAngel && !request.IsQrCodeAngel)
                    {
                        paymentTypeId = (int)EPaymentType.Cash;
                    }
                    else if (request.IsCreditAngel && !request.IsCashAngel && !request.IsMemberAngel && !request.IsQrCodeAngel)
                    {
                        paymentTypeId = (int)EPaymentType.Credit;
                    }
                    else if (request.IsMemberAngel && !request.IsCashAngel && !request.IsCreditAngel && !request.IsQrCodeAngel)
                    {
                        paymentTypeId = (int)EPaymentType.Member;
                    }
                    else if (request.IsQrCodeAngel && !request.IsCashAngel && !request.IsCreditAngel && !request.IsMemberAngel)
                    {
                        paymentTypeId = (int)EPaymentType.QrCode;
                    }

                    else
                    {
                        paymentTypeId = (int)EPaymentType.NoPay;
                    }

                    var massageAngels = await _massageService.GetMassageAngelByMassageId(id);

                    foreach (var massageAngel in massageAngels)
                    {
                        _db.ChangeTracker.Clear();
                        MassageAngel _massageAngel = massageAngel;
                        massageAngel.PaymentTypeId = paymentTypeId;
                        await _massageService.UpdateMassageAngel(_massageAngel);
                    }

                    foreach (var massageAngelRound in request.MassageAngels)
                    {
                        var _massageAngelUpdate = await _massageService.GetMassageAngelById(massageAngelRound.Id);
                        _massageAngelUpdate.Round = massageAngelRound.Round;

                        if (massageAngelRound.IsDiscount || !massageAngelRound.IsDiscount)
                        {
                            _massageAngelUpdate.IsDiscount = massageAngelRound.IsDiscount;
                            _massageAngelUpdate.IsDiscountBaht = massageAngelRound.IsDiscountBaht;
                            _massageAngelUpdate.DiscountBaht = massageAngelRound.DiscountBaht;
                            _massageAngelUpdate.IsDiscountPercent = massageAngelRound.IsDiscountPercent;
                            _massageAngelUpdate.DiscountPercent = massageAngelRound.DiscountPercent;
                            _massageAngelUpdate.IsDiscountAngelRound = massageAngelRound.IsDiscountAngelRound;
                            _massageAngelUpdate.DiscountAngelRound = massageAngelRound.DiscountAngelRound;
                            _massageAngelUpdate.DiscountRemark = massageAngelRound.DiscountRemark;
                        }

                        await _massageService.UpdateMassageAngel(_massageAngelUpdate);
                    }

                    if (request.MassageRoom != null)
                    {
                        if (request.MassageRoom.IsDiscount || !request.MassageRoom.IsDiscount)
                        {
                            var _massageRoom = await _massageService.GetMassageRoomById(request.MassageRoom.Id);
                            _massageRoom.IsDiscountBaht = request.MassageRoom.IsDiscountBaht;
                            _massageRoom.DiscountBaht = request.MassageRoom.DiscountBaht;
                            _massageRoom.IsDiscountPercent = request.MassageRoom.IsDiscountPercent;
                            _massageRoom.DiscountPercent = request.MassageRoom.DiscountPercent;
                            _massageRoom.DiscountRemark = request.MassageRoom.DiscountRemark;
                            _massageRoom.IsDiscount = request.MassageRoom.IsDiscount;

                            await _massageService.UpdateMassageRoom(_massageRoom);
                        }
                    }

                    var massageRoom = await _massageService.GetMassageRoomByMassageId(massage.Id);
                    if (request.MemberItem.MemberId != null)
                    {
                        var itemMember = await _memberService.GetByIdAsync((int)request.MemberItem.MemberId);
                        Member _memberItem = itemMember;
                        foreach (var item in _memberItem.MemberItems)
                        {
                            _db.ChangeTracker.Clear();
                            if (item.ItemId == request.MemberItem.ItemId)
                            {
                                item.Amount = item.Amount - request.Round;
                                await _memberService.UpdateMemberItem(item);
                            }
                        }
                    }

                    if (oldAgencies != request.AgenciesId)
                    {
                        var agencyPerformance = await _agencyPerformanceService.GetAgencyPerformancesByCalendarIdAndAgenciesId(massage.CalendarDayId, oldAgencies);
                        agencyPerformance.AgenciesId = (int)request.AgenciesId;
                        await _agencyPerformanceService.UpdateAsync(agencyPerformance);
                    }

                    massage = await _massageService.GetById(id);
                    var massageResponse = _mapper.Map<MassageResponse>(massage);
                    if (massageResponse == null)
                    {
                        return NotFound();
                    }
                    var user = await _userService.GetByIdAsync((int)massage.UpdatedBy);
                    massageResponse.user = _mapper.Map<UserResponse>(user);
                    massageAngels = await _massageService.GetMassageAngelByMassageId(id);
                    massageRoom = await _massageService.GetMassageRoomByMassageId(massage.Id);
                    massageResponse.massageRoom = _mapper.Map<MassageRoomResponse>(massageRoom);
                    massageResponse.massageAngels = _mapper.Map<List<MassageAngelResponse>>(massageAngels);
                    var massageCreditPayment = await _massageService.GetMassageCreditPaymentByMassageId(massage.Id);
                    massageResponse.massageCreditPayment = _mapper.Map<MassageCreditPaymentResponse>(massageCreditPayment);
                    var massageMemberPayment = await _massageService.GetMassageMemberPaymentsByMassageId(massage.Id);
                    massageResponse.massageMemberPayments = _mapper.Map<List<MassageMemberPaymentResponse>>(massageMemberPayment);

                    var massageQrCodePayment = await _massageService.GetMassageQrCodePaymentByMassageId(massage.Id);
                    massageResponse.massageQrCodePayment = _mapper.Map<MassageQrCodePaymentResponse>(massageQrCodePayment);

                    foreach (var memberResponse in massageResponse.massageMemberPayments)
                    {
                        var massageMember = await _memberService.GetByIdAsync(memberResponse.MemberId);
                        memberResponse.member = _mapper.Map<MemberResponse>(massageMember);
                    }

                    await transaction.CommitAsync();
                    return Ok(massageResponse);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex.Message);
                    _logger.LogError(ex.StackTrace);
                    await transaction.RollbackAsync();
                    return BadRequest();
                }
            }
        }

        [HttpPut(ApiRoutes.Massages.CancelPayment)]
        public async Task<IActionResult> CancelPayment([FromRoute] int id)
        {
            using (var transaction = await _db.Database.BeginTransactionAsync())
            {
                try
                {
                    if (id <= 0)
                    {
                        return BadRequest();
                    }
                    var massage = await _massageService.GetById(id);

                    bool isCredit = false;
                    bool isMember = false;
                    bool isQrCode = false;

                    if (massage.IsCreditAngel || massage.IsCreditFood || massage.IsCreditRoom || massage.IsCreditEtc)
                    {
                        isCredit = true;
                    }

                    if (massage.IsMemberAngel || massage.IsMemberFood || massage.IsMemberRoom || massage.IsMemberEtc)
                    {
                        isMember = true;
                    }

                    if (massage.IsQrCodeAngel || massage.IsQrCodeFood || massage.IsQrCodeRoom || massage.IsQrCodeEtc)
                    {
                        isQrCode = true;
                    }

                    massage.IsPaid = false;
                    massage.IsUnPaid = false;
                    massage.Total = 0;
                    massage.AngelTotal = 0;
                    massage.FoodTotal = 0;
                    massage.RoomTotal = 0;
                    massage.QrCodeTotal = 0;
                    massage.IsCashAngel = false;
                    massage.IsCreditAngel = false;
                    massage.IsMemberAngel = false;
                    massage.IsQrCodeAngel = false;
                    massage.CashAngelTotal = 0;
                    massage.CreditAngelTotal = 0;
                    massage.MemberAngelTotal = 0;
                    massage.QrCodeAngelTotal = 0;
                    massage.IsCashFood = false;
                    massage.IsCreditFood = false;
                    massage.IsMemberFood = false;
                    massage.IsQrCodeFood = false;
                    massage.CashFoodTotal = 0;
                    massage.CreditFoodTotal = 0;
                    massage.MemberFoodTotal = 0;
                    massage.QrCodeFoodTotal = 0;
                    massage.IsCashRoom = false;
                    massage.IsCreditRoom = false;
                    massage.IsMemberRoom = false;
                    massage.IsQrCodeRoom = false;
                    massage.CashRoomTotal = 0;
                    massage.CreditRoomTotal = 0;
                    massage.MemberRoomTotal = 0;
                    massage.QrCodeRoomTotal = 0;
                    massage.IsEntertainAngel = false;
                    massage.EntertainTotalAngel = 0;
                    massage.EntertainRemarkAngel = "";
                    massage.IsEntertainFood = false;
                    massage.EntertainTotalFood = 0;
                    massage.EntertainRemarkFood = "";
                    massage.EtcTotal = 0;
                    massage.IsCashEtc = false;
                    massage.IsCreditEtc = false;
                    massage.IsMemberEtc = false;
                    massage.IsEntertainEtc = false;
                    massage.IsQrCodeEtc = false;
                    massage.EntertainEtcRemark = "";
                    massage.CashEtcTotal = 0;
                    massage.CreditEtcTotal = 0;
                    massage.MemberEtcTotal = 0;
                    massage.EntertainEtcTotal = 0;
                    massage.QrCodeEtcTotal = 0;
                    massage.TipTotal = 0;
                    massage.TipCommTotal = 0;
                    massage.DamagesTotal = 0;
                    massage.OtherServiceChargesTotal = 0;
                    massage.EtcRemark = "";
                    massage.CashierId = null;
                    massage.UnPaidRemark = null;
                    //massage.PayDate = null;
                    massage.CancelDate = DateTime.Now;

                    massage.TipQrCodeTotal = 0;

                    //Flag Delete MassageDebt
                    var massageDebtId = await _massageDebtService.GetMassageDebtByMassageId(massage.Id);
                    if (massageDebtId != null) 
                    {
                        var massageDebt = await _massageDebtService.GetByIdAsync(massageDebtId.Id);
                        if (massageDebt != null)
                        {
                            await _massageDebtService.DeleteAsync(massageDebt.Id);
                        }
                    }
                    await _massageService.UpdateAsync(massage);

                    if (isCredit)
                    {
                        var payCredit = await _massageService.GetMassageCreditPaymentByMassageId(massage.Id);
                        //if (payCredit == null)
                        //{
                        //    return NotFound();
                        //}
                        //foreach (var itemCredit in payCredit)
                        //{
                        // cancel Payment Credit
                        await _massageService.CancelCreditPayment(payCredit);
                        //}
                    }
                    if (isQrCode)
                    {
                        var payQrCode = await _massageService.GetMassageQrCodePaymentByMassageId(massage.Id);

                        await _massageService.CancelQrCodePayment(payQrCode);
                    }
                    if (isMember)
                    {
                        var payMember = await _massageService.GetMassageMemberPaymentsByMassageId(massage.Id);
                        //if (payMember == null)
                        //{
                        //    return NotFound();
                        //}
                        foreach (var itemMember in payMember)
                        {
                            var member = await _memberService.GetByIdAsync(itemMember.MemberId);
                            if (member != null)
                            {
                                member.CreditAmount += itemMember.MemberTotal;
                                await _memberService.UpdateAsync(member);


                                var memberItem = await _memberService.GetMemberItemSuite(member.Id, 3);
                                memberItem.Id = memberItem.Id;
                                memberItem.MemberId = memberItem.MemberId;
                                memberItem.ItemId = memberItem.ItemId;
                                if (itemMember.UseSuite != 0)
                                {
                                    memberItem.Amount += itemMember.UseSuite;
                                }
                                else if (itemMember.UseVip != 0)
                                {
                                    memberItem.Amount += itemMember.UseVip;
                                }


                                await _memberService.UpdateMemberItem(memberItem);

                                // cancel Payment Member
                                await _massageService.CancelMemberPayment(itemMember);
                            }
                        }
                    }
                    await transaction.CommitAsync();
                    return Ok();
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex.Message);
                    _logger.LogError(ex.StackTrace);
                    await transaction.RollbackAsync();
                    return BadRequest();
                }
            }
        }

        [HttpGet(ApiRoutes.Massages.GetAllUnPaid)]
        public async Task<IActionResult> GetAllUnPaid([FromQuery] UnPaidSearchQurey paginationQuery)
        {
            try
            {
                List<int> calendarIds = await _settingService.GetCalendarId((DateTime)paginationQuery.Startdate, (DateTime)paginationQuery.Enddate);
                var pagination = _mapper.Map<PaginationFilter>(paginationQuery);
                var UnPaids = await _massageService.GetUnPaids(calendarIds, paginationQuery, pagination);
                if (UnPaids == null)
                {
                    return NotFound();
                }
                var UnPaidResponse = _mapper.Map<List<MassageResponse>>(UnPaids);

                foreach (var item in UnPaidResponse)
                {

                    var massageAngel = await _massageService.GetMassageAngelBymassageId(item.Id);
                    if (massageAngel == null)
                    {
                        return NotFound();
                    }
                    item.massageAngels = _mapper.Map<List<MassageAngelResponse>>(massageAngel);

                    foreach (var reception in item.massageAngels)
                    {
                        var receptions = await _receptionService.GetByIdAsync(reception.ReceptionId);
                        if (receptions == null)
                        {
                            return NotFound();
                        }
                        reception.Reception = _mapper.Map<ReceptionResponse>(receptions);
                    }
                }

                return Ok(new PagedResponse<MassageResponse>
                {
                    Data = UnPaidResponse,
                    PageNumber = pagination.PageNumber,
                    PageSize = pagination.PageSize,
                    Keyword = pagination.Keyword,
                    OrderBy = pagination.OrderBy,
                    TotalRecords = pagination.TotalRecords,
                    OrderDirection = pagination.OrderDirection
                });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }

        [HttpGet(ApiRoutes.Massages.GetAllCheckInRoom)]
        public async Task<IActionResult> GetAllCheckInRoom([FromQuery] CheckInSearchQuery paginationQuery)
        {
            try
            {
                int calendarDayId = await GetCalendarDayId();
                var pagination = _mapper.Map<PaginationFilter>(paginationQuery);
                var massageAngel = await _massageService.GetAllCheckInRoom(paginationQuery, pagination, calendarDayId);
                if (massageAngel == null)
                {
                    return NotFound();
                }



                var response = _mapper.Map<List<MassageAngelResponse>>(massageAngel);

                foreach (var item in response)
                {
                    if ((int)item.Room.RoomTypeId == (int)ERoomType.SuiteSub)
                    {
                        var parentRoom = await _roomService.GetByIdAsync((int)item.Room.ParentRoomId);

                        item.Room.ParentRoom = _mapper.Map<RoomParentResponse>(parentRoom);
                    }
                }

                return Ok(new PagedResponse<MassageAngelResponse>
                {
                    Data = response,
                    PageNumber = pagination.PageNumber,
                    PageSize = pagination.PageSize,
                    Keyword = pagination.Keyword,
                    OrderBy = pagination.OrderBy,
                    TotalRecords = pagination.TotalRecords,
                    OrderDirection = pagination.OrderDirection,
                });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }

        [HttpGet(ApiRoutes.Massages.GetAllCheckInRoomSuite)]
        public async Task<IActionResult> GetAllCheckInRoomSuite([FromQuery] CheckInSearchQuery paginationQuery)
        {
            try
            {
                int calendarDayId = await GetCalendarDayId();
                var pagination = _mapper.Map<PaginationFilter>(paginationQuery);
                var massageRoom = await _massageService.GetAllCheckInRoomSuite(paginationQuery, pagination, calendarDayId);
                if (massageRoom == null)
                {
                    return NotFound();
                }
                return Ok(new PagedResponse<MassageRoomResponse>
                {
                    Data = _mapper.Map<List<MassageRoomResponse>>(massageRoom),
                    PageNumber = pagination.PageNumber,
                    PageSize = pagination.PageSize,
                    Keyword = pagination.Keyword,
                    OrderBy = pagination.OrderBy,
                    TotalRecords = pagination.TotalRecords,
                    OrderDirection = pagination.OrderDirection,
                });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }

        [HttpGet(ApiRoutes.Massages.GetAllCheckInChangeRoomSuite)]
        public async Task<IActionResult> GetAllCheckInChangeRoomSuite([FromQuery] CheckInSearchQuery paginationQuery)
        {
            try
            {
                int calendarDayId = await GetCalendarDayId();
                var pagination = _mapper.Map<PaginationFilter>(paginationQuery);
                var massageRoom = await _massageService.GetAllCheckInChangeRoomSuite(paginationQuery, pagination, calendarDayId);
                if (massageRoom == null)
                {
                    return NotFound();
                }
                var response = _mapper.Map<List<MassageRoomResponse>>(massageRoom);
                foreach (var item in massageRoom)
                {
                    var newMassageRoom = await _massageService.GetMassageRoomByMassageId((int)item.NewMassageId);
                    if (newMassageRoom != null)
                    {
                        foreach (var responseItem in response)
                        {
                            responseItem.NewMassageRoom = _mapper.Map<NewMassageRoomResponse>(newMassageRoom);
                            var room = await _roomService.GetByIdAsync(responseItem.NewMassageRoom.RoomId);
                            responseItem.NewMassageRoom.Room = _mapper.Map<RoomResponse>(room);
                        }
                    }
                    else
                    {


                        foreach (var responseItem in response)
                        {
                            var oldMassageRoom = await _massageService.GetMassageRoomByMassageId((int)item.MassageId);
                            var _newMassageAngel = await _massageService.GetMassageAngelByMassageId((int)oldMassageRoom.NewMassageId);
                            responseItem.NewMassageRoom = _mapper.Map<NewMassageRoomResponse>(oldMassageRoom);
                            var room = await _roomService.GetByIdAsync(_newMassageAngel[0].RoomId);
                            responseItem.NewMassageRoom.Room = _mapper.Map<RoomResponse>(room);
                        }
                    }

                }
                return Ok(new PagedResponse<MassageRoomResponse>
                {
                    Data = response,
                    PageNumber = pagination.PageNumber,
                    PageSize = pagination.PageSize,
                    Keyword = pagination.Keyword,
                    OrderBy = pagination.OrderBy,
                    TotalRecords = pagination.TotalRecords,
                    OrderDirection = pagination.OrderDirection,
                });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }

        [HttpPatch(ApiRoutes.Massages.CancelAngelAndRoom)]
        public async Task<IActionResult> CancelAngelAndRoom([FromBody] UpdateMassageAngelRequest request)
        {
            using (var transaction = await _db.Database.BeginTransactionAsync())
            {
                try
                {
                    MassageAngel massageAngel = await _massageService.GetMassageAngelById((int)request.MassageAngelId);
                    if (massageAngel == null)
                    {
                        return NotFound();
                    }
                    var angel = await _angelService.GetByIdAsync(massageAngel.AngelId);
                    if (angel == null)
                    {
                        return NotFound();
                    }
                    angel.Status = (int)EAngelStatus.Ready;
                    await _angelService.UpdateAsync(angel);

                    var room = await _roomService.GetByIdAsync(massageAngel.RoomId);
                    if (room == null)
                    {
                        return NotFound();
                    }
                    room.Status = (int)ERoomStatus.Ready;
                    await _roomService.UpdateAsync(room);

                    await _massageService.DeleteMassageAngel(massageAngel.Id);

                    //////////////ZBew///////////////////
                    if (massageAngel.AngelPerformanceId != null)
                    {
                        var angelPerformance = await _angelPerformanceService.GetByIdAsync((int)massageAngel.AngelPerformanceId);
                        if (angelPerformance == null)
                        {
                            return NotFound();
                        }
                        var checkMassageAngel = await _massageService.GetMassageAngelByAngelPerformanceId(angelPerformance.Id);
                        if (checkMassageAngel.Count == 0)
                        {
                            await _angelPerformanceService.DeleteAngelPerformance(angelPerformance.Id);
                        }
                    }                   
                    /////////////////////////////////////

                    var massages = await _massageService.GetById(massageAngel.Massage.Id);
                    if (massages == null)
                    {
                        return NotFound();
                    }
                    massages.DamagesTotal = 0;
                    massages.OtherServiceChargesTotal = 0;
                    massages.EtcTotal = 0;
                    massages.TipTotal = 0;
                    massages.TipCommTotal = 0;
                    massages.CancelDate = DateTime.Now;
                    await _massageService.UpdateAsync(massages);

                    await transaction.CommitAsync();

                    return Ok();

                }
                catch (Exception ex)
                {
                    _logger.LogError(ex.Message);
                    _logger.LogError(ex.StackTrace);
                    await transaction.RollbackAsync();
                    return BadRequest();
                }
            }
        }
        [HttpPatch(ApiRoutes.Massages.CancelRoomTypeSuite)]
        public async Task<IActionResult> CancelRoomTypeSuite([FromBody] UpdateMassageAngelRequest request)
        {
            using (var transaction = await _db.Database.BeginTransactionAsync())
            {
                try
                {
                    MassageRoom massageRoom = await _massageService.GetMassageRoomById((int)request.MassageRoomId);

                    if (massageRoom == null)
                    {
                        return NotFound();
                    }

                    //var getmassageAngel = await _massageService.GetMassageAngelByMassageIdAndRoomId(massageRoom.MassageId, massageRoom.RoomId);
                    //if (getmassageAngel == null)
                    //{
                    //    return NotFound();
                    //}
                    var _room = await _roomService.GetByIdAsync(massageRoom.RoomId);
                    if (_room.RoomTypeId == (int)ERoomType.Suite || _room.RoomTypeId == (int)ERoomType.Suite3 || _room.RoomTypeId == (int)ERoomType.Suite4 || _room.RoomTypeId == (int)ERoomType.Suite5 || _room.RoomTypeId == (int)ERoomType.Suite6 || _room.RoomTypeId == (int)ERoomType.Suite7 || _room.RoomTypeId == (int)ERoomType.Suite8 || _room.RoomTypeId == (int)ERoomType.Suite9 || _room.RoomTypeId == (int)ERoomType.Suite10 || _room.RoomTypeId == (int)ERoomType.C3 || _room.RoomTypeId == (int)ERoomType.C5)
                    {
                        var _getmassageAngel = await _massageService.GetMassageAngelByRoomId(massageRoom.MassageId, massageRoom.RoomId);

                        foreach (var massageAngel in _getmassageAngel)
                        {
                            var roomAngel = await _roomService.GetByIdAsync(massageAngel.RoomId);
                            var angel = await _angelService.GetByIdAsync(massageAngel.AngelId);

                            roomAngel.Status = (int)ERoomStatus.Ready;
                            angel.Status = (int)EAngelStatus.Ready;
                            await _angelService.UpdateAsync(angel);
                            await _roomService.UpdateAsync(roomAngel);

                            await _massageService.DeleteMassageAngel(massageAngel.Id);

                            //////////////ZBew///////////////////
                            if (massageAngel.AngelPerformanceId != null)
                            {
                                var angelPerformance = await _angelPerformanceService.GetByIdAsync((int)massageAngel.AngelPerformanceId);
                                if (angelPerformance == null)
                                {
                                    return NotFound();
                                }
                                var checkMassageAngel = await _massageService.GetMassageAngelByAngelPerformanceId(angelPerformance.Id);
                                if (checkMassageAngel.Count == 0)
                                {
                                    await _angelPerformanceService.DeleteAngelPerformance(angelPerformance.Id);
                                }
                            }
                            /////////////////////////////////////
                        }
                    }
                    else
                    {
                        var _getmassageAngel = await _massageService.GetMassageAngelByMassageId(massageRoom.MassageId, massageRoom.RoomId);

                        foreach (var massageAngel in _getmassageAngel)
                        {
                            var roomAngel = await _roomService.GetByIdAsync(massageAngel.RoomId);
                            var angel = await _angelService.GetByIdAsync(massageAngel.AngelId);

                            roomAngel.Status = (int)ERoomStatus.Ready;
                            angel.Status = (int)EAngelStatus.Ready;
                            await _angelService.UpdateAsync(angel);
                            await _roomService.UpdateAsync(roomAngel);

                            await _massageService.DeleteMassageAngel(massageAngel.Id);

                            //////////////ZBew///////////////////
                            if (massageAngel.AngelPerformanceId != null)
                            {
                                var angelPerformance = await _angelPerformanceService.GetByIdAsync((int)massageAngel.AngelPerformanceId);
                                if (angelPerformance == null)
                                {
                                    return NotFound();
                                }
                                var checkMassageAngel = await _massageService.GetMassageAngelByAngelPerformanceId(angelPerformance.Id);
                                if (checkMassageAngel.Count == 0)
                                {
                                    await _angelPerformanceService.DeleteAngelPerformance(angelPerformance.Id);
                                }
                            }
                            ///////////////////////////////////// 
                            

                        }
                    }

                    var room = await _roomService.GetByIdAsync(massageRoom.RoomId);
                    room.Status = (int)ERoomStatus.Ready;
                    await _roomService.UpdateAsync(room);

                    var roomParent = await _roomService.GetChildRoomParent(room.Id);
                    if (roomParent != null)
                    {
                        foreach (var parentRoom in roomParent)
                        {
                            parentRoom.Status = (int)ERoomStatus.Ready;
                            await _roomService.UpdateAsync(parentRoom);
                        }
                    }

                    var massageRoomOld = await _massageService.GetMassageRoomByNewMassageId(massageRoom.MassageId);
                    if (massageRoomOld != null)
                    {
                        var roomOld = await _roomService.GetByIdAsync(massageRoomOld.RoomId);
                        roomOld.Status = (int)ERoomStatus.Ready;
                        await _roomService.UpdateAsync(roomOld);

                        var roomOldParent = await _roomService.GetChildRoomParent(roomOld.Id);
                        if (roomOldParent != null)
                        {
                            foreach (var parentRoomOld in roomOldParent)
                            {
                                parentRoomOld.Status = (int)ERoomStatus.Ready;
                                await _roomService.UpdateAsync(parentRoomOld);
                            }
                        }
                    }

                    await _massageService.DeleteMassageRoom(massageRoom.Id);

                    var _massageRoom = await _massageService.GetMassageRoomByNewMassageId(massageRoom.MassageId);
                    if (_massageRoom != null)
                    {
                        var room_ = await _roomService.GetByIdAsync(_massageRoom.RoomId);
                        room_.Status = (int)ERoomStatus.Ready;
                        await _roomService.UpdateAsync(room_);

                        await _massageService.DeleteMassageRoom(_massageRoom.Id);
                    }

                    var massages = await _massageService.GetById(massageRoom.Massage.Id);
                    if (massages == null)
                    {
                        return NotFound();
                    }
                    massages.DamagesTotal = 0;
                    massages.OtherServiceChargesTotal = 0;
                    massages.EtcTotal = 0;
                    massages.TipTotal = 0;
                    massages.TipCommTotal = 0;
                    massages.CancelDate = DateTime.Now;
                    await _massageService.UpdateAsync(massages);

                    await transaction.CommitAsync();

                    return Ok();
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex.Message);
                    _logger.LogError(ex.StackTrace);
                    await transaction.RollbackAsync();
                    return BadRequest();
                }
            }
        }

        [HttpPatch(ApiRoutes.Massages.CheckOutAngelAndRoom)]
        public async Task<IActionResult> CheckoutAngelAndRoom([FromBody] UpdateMassageAngelRequest request)
        {
            using (var transaction = await _db.Database.BeginTransactionAsync())
            {
                try
                {
                    int? calenDarDayId = await GetCalendarDayId();
                    if (calenDarDayId == null)
                    {
                        return NotFound();
                    }
                    MassageAngel massageAngel = await _massageService.GetMassageAngelById((int)request.MassageAngelId);
                    if (massageAngel == null)
                    {
                        return NotFound();
                    }
                    var getAngel = await _angelService.GetByIdAsync(massageAngel.AngelId);
                    if (getAngel == null)
                    {
                        return NotFound();
                    }
                    TimeSpan timespan = DateTime.Now - (DateTime)massageAngel.CheckInTime;
                    double totalMinute = timespan.TotalMinutes;
                    massageAngel.CheckOutTime = DateTime.Now;
                    massageAngel.TimeMinute = (int)totalMinute;

                    // logic sum roundFact
                    var modMinute = massageAngel.TimeMinute % getAngel.AngelType.RoundTime;
                    var roundFact = 1.0;
                    roundFact = Math.Floor((double)massageAngel.TimeMinute / getAngel.AngelType.RoundTime);

                    var haftRoundTime = Math.Floor((double)getAngel.AngelType.RoundTime / 2);

                    if (roundFact >= 1)
                    {
                        if (modMinute > 15 && modMinute <= haftRoundTime)
                        {
                            roundFact += 0.5;
                        }
                        else if (modMinute > haftRoundTime && modMinute < getAngel.AngelType.RoundTime)
                        {
                            roundFact += 1;
                        }
                    }
                    else
                    {
                        if (modMinute > 15)
                        {
                            roundFact = 1;
                        }
                    }

                    massageAngel.RoundFact = (decimal)roundFact;
                    massageAngel.PaymentTypeId = (int)EPaymentType.NoPay;
                    // end
                    await _massageService.UpdateMassageAngel(massageAngel);

                    // update status angel
                    var angel = await _angelService.GetByIdAsync(massageAngel.AngelId);
                    if (angel == null)
                    {
                        return NotFound();
                    }
                    angel.Status = (int)EAngelStatus.Ready;
                    await _angelService.UpdateAsync(angel);
                    //end

                    // update status room
                    var room = await _roomService.GetByIdAsync(massageAngel.RoomId);
                    if (room == null)
                    {
                        return NotFound();
                    }

                    if (room.RoomTypeId == (int)ERoomType.Normal || room.RoomTypeId == (int)ERoomType.RoundSell)
                    {
                        bool isMultiple = await _massageService.GetMultipleAngleByRoomNormal(massageAngel.MassageId);
                        if (!isMultiple)
                        {
                            room.Status = (int)ERoomStatus.Ready;
                            await _roomService.UpdateAsync(room);
                        }
                    }
                    //end
                    var angelPerformance = await _angelPerformanceService.CreateOrUpdateByIdAsync(massageAngel, (int)calenDarDayId);
                    if (angelPerformance == null)
                    {
                        return NotFound();
                    }
                    else //Aunz
                    {
                        MassageAngel _massageAngel = await _massageService.GetMassageAngelById((int)request.MassageAngelId);
                        _massageAngel.AngelPerformanceId = angelPerformance.Id;

                        await _massageService.UpdateMassageAngel(_massageAngel);
                    }

                    bool isDeductType = _deductTypeService.CheckAngelPerformanceDeduct(massageAngel.Id);

                    if (!isDeductType)
                    {
                        _logger.LogDebug("True CheckOutAngelAndRoom");
                        var deductType = await _deductTypeService.GetAllAsync(null, null);

                        if (deductType.Count > 0)
                        {
                            List<AngelPerformanceDeduct> angelPerformanceDeducts = new List<AngelPerformanceDeduct>();
                            foreach (var item in deductType)
                            {
                                AngelPerformanceDeduct angelPerformanceDeduct = new AngelPerformanceDeduct();
                                angelPerformanceDeduct.MassageAngelId = massageAngel.Id;
                                angelPerformanceDeduct.DeductTypeId = item.Id;
                                angelPerformanceDeduct.AngelPerformanceId = angelPerformance.Id;
                                angelPerformanceDeduct.Fee = item.Fee;
                                angelPerformanceDeducts.Add(angelPerformanceDeduct);

                            };
                            await _massageService.CreateAngelPerformanceDeduct(angelPerformanceDeducts);

                            //var Totaldeduct = await _angelPerformanceService.TotalAngelPerformanceDeduct(angelPerformance.Id);
                            //var angelperformance = await _angelPerformanceService.GetByIdAsync(angelPerformanceDeducts[0].AngelPerformanceId);
                            //if (angelperformance == null)
                            //{
                            //    return NotFound();
                            //}
                            //angelperformance.TotalDeduct = Totaldeduct;

                            //await _angelPerformanceService.UpdateAsync(angelperformance);
                        }
                    }
                    else
                    {
                        _logger.LogDebug("False CheckOutAngelAndRoom");
                    }


                    await transaction.CommitAsync();

                    var massageAngelResponse = _mapper.Map<MassageAngelResponse>(massageAngel);

                    await _checkInHub.Clients.All.SendAsync("CheckOutRoom", massageAngelResponse);


                    return Ok();
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex.Message);
                    _logger.LogError(ex.StackTrace);
                    await transaction.RollbackAsync();
                    return BadRequest();
                }
            }
        }
        [HttpPatch(ApiRoutes.Massages.CheckOutRoomTypeSuite)]
        public async Task<IActionResult> CheckoutRoomTypeSuite([FromBody] UpdateMassageAngelRequest request)
        {
            using (var transaction = await _db.Database.BeginTransactionAsync())
            {
                try
                {
                    MassageRoom massageRoom = await _massageService.GetMassageRoomById((int)request.MassageRoomId);
                    if (massageRoom == null)
                    {
                        return NotFound();
                    }
                    TimeSpan timespan = DateTime.Now - (DateTime)massageRoom.CheckInTime;
                    double totalMinute = timespan.TotalMinutes;
                    massageRoom.CheckOutTime = DateTime.Now;
                    massageRoom.TimeMinute = (int)totalMinute;

                    var modMinuteRoom = massageRoom.TimeMinute % massageRoom.Room.RoomType.RoundTime;
                    var deleteMinuteRoom = massageRoom.TimeMinute - massageRoom.Room.RoomType.RoundTime;
                    var roundFactRoom = 1.0;
                    roundFactRoom = Math.Floor((double)massageRoom.TimeMinute / massageRoom.Room.RoomType.RoundTime);
                    var haftRoundTimeRoom = Math.Floor((double)massageRoom.Room.RoomType.RoundTime / 2);

                    if (roundFactRoom >= 1)
                    {
                        if (massageRoom.Room.RoomType.Id == (int)ERoomType.Vip || massageRoom.Room.RoomType.Id == (int)ERoomType.Vip2 || massageRoom.Room.RoomType.Id == (int)ERoomType.Vip3)
                        {
                            if (modMinuteRoom > 15 && modMinuteRoom <= haftRoundTimeRoom)
                            {
                                roundFactRoom += 0.5;
                            }
                            else if (modMinuteRoom > haftRoundTimeRoom && modMinuteRoom < massageRoom.Room.RoomType.RoundTime)
                            {
                                roundFactRoom += 1;
                            }
                        }
                        else
                        {
                            roundFactRoom = Math.Floor((double)deleteMinuteRoom / 60) + 1;
                        }
                    }
                    else
                    {
                        roundFactRoom = 1;
                    }

                    massageRoom.Round = (decimal)roundFactRoom;

                    await _massageService.UpdateMassageRoom(massageRoom);

                    var room = await _roomService.GetByIdAsync(massageRoom.RoomId);
                    if (room != null)
                    {
                        room.Status = (int)ERoomStatus.Ready;
                        await _roomService.UpdateAsync(room);

                        var parentRoom = await _roomService.GetChildRoomParent(room.Id);
                        if (parentRoom != null)
                        {
                            foreach (var itemroom in parentRoom)
                            {
                                itemroom.Status = (int)ERoomStatus.Ready;
                                await _roomService.UpdateAsync(itemroom);
                            }
                        }
                    }

                    int? calenDarDayId = await GetCalendarDayId();
                    if (calenDarDayId == null)
                    {
                        return NotFound();
                    }

                    List<MassageAngel> massageAngels = new List<MassageAngel>();

                    var getmassageAngel = await _massageService.GetMassageAngelByMassageId(massageRoom.MassageId);

                    foreach (var item in getmassageAngel)
                    {
                        var itemMassageAngel = await _massageService.GetMassageAngelById(item.Id);

                        if (itemMassageAngel.CheckOutTime != null)
                        {
                            continue;
                        }

                        var getAngel = await _angelService.GetByIdAsync(itemMassageAngel.AngelId);
                        if (getAngel == null)
                        {
                            return NotFound();
                        }
                        TimeSpan timespanmassageAngel = DateTime.Now - (DateTime)itemMassageAngel.CheckInTime;
                        double totalMinutes = timespanmassageAngel.TotalMinutes;
                        itemMassageAngel.CheckOutTime = DateTime.Now;
                        itemMassageAngel.TimeMinute = (int)totalMinute;

                        // logic sum roundFact
                        var modMinute = itemMassageAngel.TimeMinute % getAngel.AngelType.RoundTime;
                        var roundFact = 1.0;
                        roundFact = Math.Floor((double)itemMassageAngel.TimeMinute / getAngel.AngelType.RoundTime);

                        var haftRoundTime = Math.Floor((double)getAngel.AngelType.RoundTime / 2);

                        if (roundFact >= 1)
                        {
                            if (modMinute > 15 && modMinute <= haftRoundTime)
                            {
                                roundFact += 0.5;
                            }
                            else if (modMinute > haftRoundTime && modMinute < getAngel.AngelType.RoundTime)
                            {
                                roundFact += 1;
                            }
                        }
                        else
                        {
                            if (modMinute > 15)
                            {
                                roundFact = 1;
                            }
                        }

                        itemMassageAngel.RoundFact = (decimal)roundFact;
                        // end
                        await _massageService.UpdateMassageAngel(itemMassageAngel);


                        // update status angel
                        var angel = await _angelService.GetByIdAsync(itemMassageAngel.AngelId);
                        if (angel == null)
                        {
                            return NotFound();
                        }
                        angel.Status = (int)EAngelStatus.Ready;
                        await _angelService.UpdateAsync(angel);
                        //end

                        // update status room
                        var massageroom = await _roomService.GetByIdAsync(itemMassageAngel.RoomId);
                        if (massageroom == null)
                        {
                            return NotFound();
                        }
                        if (massageroom.RoomTypeId != (int)ERoomType.Suite && massageroom.RoomTypeId != (int)ERoomType.Vip && massageroom.RoomTypeId != (int)ERoomType.Vip2 && massageroom.RoomTypeId != (int)ERoomType.Vip3 && massageroom.RoomTypeId != (int)ERoomType.Suite3 && massageroom.RoomTypeId != (int)ERoomType.Suite4 && massageroom.RoomTypeId != (int)ERoomType.Suite5 && massageroom.RoomTypeId != (int)ERoomType.Suite6 && massageroom.RoomTypeId != (int)ERoomType.Suite7 && massageroom.RoomTypeId != (int)ERoomType.Suite8 && massageroom.RoomTypeId != (int)ERoomType.Suite9 && massageroom.RoomTypeId != (int)ERoomType.Suite10 && massageroom.RoomTypeId != (int)ERoomType.C3 && massageroom.RoomTypeId != (int)ERoomType.C5)
                        {
                            massageroom.Status = (int)ERoomStatus.Ready;
                            await _roomService.UpdateAsync(massageroom);

                            var parentRoom = await _roomService.GetChildRoomParent(massageroom.Id);
                            if (parentRoom != null)
                            {
                                foreach (var itemroom in parentRoom)
                                {
                                    itemroom.Status = (int)ERoomStatus.Ready;
                                    await _roomService.UpdateAsync(itemroom);
                                }
                            }
                        }

                        var angelPerformance = await _angelPerformanceService.CreateOrUpdateByIdAsync(itemMassageAngel, (int)calenDarDayId);
                        if (angelPerformance == null)
                        {
                            return NotFound();
                        }

                        else //Aunz
                        {
                            itemMassageAngel.AngelPerformanceId = angelPerformance.Id;
                            await _massageService.UpdateMassageAngel(itemMassageAngel);
                        }

                        bool isDeductType = _deductTypeService.CheckAngelPerformanceDeduct(itemMassageAngel.Id);
                        if (!isDeductType)
                        {
                            //_logger.LogDebug("True CheckOutRoomTypeSuite");
                            var deductType = await _deductTypeService.GetAllAsync(null, null);

                            if (deductType.Count > 0)
                            {
                                List<AngelPerformanceDeduct> angelPerformanceDeducts = new List<AngelPerformanceDeduct>();
                                foreach (var itemdeduct in deductType)
                                {
                                    AngelPerformanceDeduct angelPerformanceDeduct = new AngelPerformanceDeduct();
                                    angelPerformanceDeduct.MassageAngelId = itemMassageAngel.Id;
                                    angelPerformanceDeduct.DeductTypeId = itemdeduct.Id;
                                    angelPerformanceDeduct.AngelPerformanceId = angelPerformance.Id;
                                    angelPerformanceDeduct.Fee = itemdeduct.Fee;
                                    angelPerformanceDeducts.Add(angelPerformanceDeduct);

                                };
                                await _massageService.CreateAngelPerformanceDeduct(angelPerformanceDeducts);

                                //var Totaldeduct = await _angelPerformanceService.TotalAngelPerformanceDeduct(angelPerformance.Id);

                                //var angelperformance = await _angelPerformanceService.GetByIdAsync(angelPerformanceDeducts[0].AngelPerformanceId);

                                //angelperformance.TotalDeduct = Totaldeduct;

                                //await _angelPerformanceService.UpdateAsync(angelperformance);
                            }
                        }
                        else
                        {
                            _logger.LogDebug("False CheckOutRoomTypeSuite");
                        }



                    }
                    await transaction.CommitAsync();



                    var massageAngelResponse = _mapper.Map<List<MassageAngelResponse>>(getmassageAngel);
                    var massageRoomResponse = _mapper.Map<MassageRoomResponse>(massageRoom);

                    await _checkInHub.Clients.All.SendAsync("CheckOutRoomSuite", new { MassageRoom = massageRoomResponse, MassageAngels = massageAngelResponse });

                    return Ok();
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex.Message);
                    _logger.LogError(ex.StackTrace);
                    await transaction.RollbackAsync();
                    return BadRequest();
                }
            }
        }

        [HttpPatch(ApiRoutes.Massages.UpdateRoundAngelAndIsNotCall)]
        public async Task<IActionResult> UpdateAngelRoundAndIsNotCall([FromBody] UpdateMassageAngelRequest request)
        {
            using (var transaction = await _db.Database.BeginTransactionAsync())
            {
                try
                {
                    MassageAngel massageAngel = await _massageService.GetMassageAngelById((int)request.MassageAngelId);
                    if (massageAngel == null)
                    {
                        return NotFound();
                    }
                    massageAngel.Round = (decimal)request.Round;
                    massageAngel.IsNotCall = request.IsNotCall;
                    massageAngel.AddRound = (decimal)request.AddRound;
                    await _massageService.UpdateMassageAngel(massageAngel);
                    await transaction.CommitAsync();
                    return Ok();
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex.Message);
                    _logger.LogError(ex.StackTrace);
                    await transaction.RollbackAsync();
                    return BadRequest();
                }
            }
        }
        [HttpPatch(ApiRoutes.Massages.UpdateCheckInTime)]
        public async Task<IActionResult> UpdateCheckInTime([FromBody] UpdateMassageAngelRequest request)
        {
            using (var transaction = await _db.Database.BeginTransactionAsync())
            {
                try
                {
                    if (request.IsRoom)
                    {
                        MassageRoom massageRoom = await _massageService.GetMassageRoomById((int)request.MassageRoomId);
                        if (massageRoom == null)
                        {
                            return NotFound();
                        }
                        massageRoom.CheckInTime = request.CheckInTime;
                        await _massageService.UpdateMassageRoom(massageRoom);
                    }
                    if (request.IsAngel)
                    {
                        MassageAngel massageAngel = await _massageService.GetMassageAngelById((int)request.MassageAngelId);
                        if (massageAngel == null)
                        {
                            return NotFound();
                        }
                        massageAngel.CheckInTime = request.CheckInTime;
                        await _massageService.UpdateMassageAngel(massageAngel);
                    }
                    await transaction.CommitAsync();
                    return Ok();
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex.Message);
                    _logger.LogError(ex.StackTrace);
                    await transaction.RollbackAsync();
                    return BadRequest();
                }
            }
        }

        [HttpPatch(ApiRoutes.Massages.ChangeReception)]
        public async Task<IActionResult> ChangeReception([FromBody] UpdateMassageAngelRequest request)
        {
            using (var transaction = await _db.Database.BeginTransactionAsync())
            {
                try
                {
                    if (request.IsRoom)
                    {
                        MassageRoom massageRoom = await _massageService.GetMassageRoomById((int)request.MassageRoomId);
                        if (massageRoom == null)
                        {
                            return NotFound();
                        }
                        massageRoom.ReceptionId = (int)request.ReceptionId;
                        await _massageService.UpdateMassageRoom(massageRoom);
                    }
                    if (request.IsAngel)
                    {
                        MassageAngel massageAngel = await _massageService.GetMassageAngelById((int)request.MassageAngelId);
                        if (massageAngel == null)
                        {
                            return NotFound();
                        }
                        massageAngel.ReceptionId = (int)request.ReceptionId;
                        await _massageService.UpdateMassageAngel(massageAngel);
                    }
                    await transaction.CommitAsync();
                    return Ok();
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex.Message);
                    _logger.LogError(ex.StackTrace);
                    await transaction.RollbackAsync();
                    return BadRequest();
                }
            }
        }

        [HttpPatch(ApiRoutes.Massages.ChangeRoom)] // Credit Aunz
        public async Task<IActionResult> ChangelRoom([FromBody] UpdateMassageAngelRequest request)
        {
            using (var transaction = await _db.Database.BeginTransactionAsync())
            {
                try
                {
                    var room = await _roomService.GetByIdAsync((int)request.RoomId); // new Room
                    var massageAngel = await _massageService.GetMassageAngelById((int)request.MassageAngelId); // old Room
                    var _massageRoom = await _massageService.GetMassageRoomByMassageId(massageAngel.MassageId);
                    var massageAngelByMassage = await _massageService.GetMassageAngelByMassageId(massageAngel.MassageId);
                    var _massageAngelByMassage = await _massageService.GetMassageAngelByMassageId(massageAngel.MassageId, massageAngel.RoomId);
                    var _massage = await _massageService.GetById((int)request.MassageId);
                    // ธรรมดา --> ธรรมดา
                    if ((massageAngel.Room.RoomTypeId == (int)ERoomType.Normal) && (room.RoomTypeId == (int)ERoomType.Normal))
                    {
                        int? calenDarDayId = await GetCalendarDayId();
                        string documentNumber = _settingService.GetDocumentNumberAndUpdate((int)calenDarDayId, false);

                        if (_massageAngelByMassage.Count == 1)
                        {
                            //old room update status ready
                            var oldRoom = await _roomService.GetByIdAsync(massageAngel.RoomId);
                            oldRoom.Status = (int)ERoomStatus.Ready;
                            await _roomService.UpdateAsync(oldRoom);
                        }

                        massageAngel.RoomId = room.Id;
                        await _massageService.UpdateMassageAngel(massageAngel);

                        //new room update status working
                        room.Status = (int)ERoomStatus.Working;
                        await _roomService.UpdateAsync(room);

                    }
                    // ธรรมดา --> vip
                    else if ((massageAngel.Room.RoomTypeId == (int)ERoomType.Normal) && (room.RoomTypeId == (int)ERoomType.Vip || room.RoomTypeId == (int)ERoomType.Vip2 || room.RoomTypeId == (int)ERoomType.Vip3))
                    {
                        int? calenDarDayId = await GetCalendarDayId();
                        string documentNumber = _settingService.GetDocumentNumberAndUpdate((int)calenDarDayId, false);
                        var roomMaster = await _roomService.GetByIdAsync((int)room.Id);
                        bool isDuplicate = await _massageService.CheckDuplicateRoomMaster(roomMaster.Id);

                        if (isDuplicate == true)
                        {
                            if (massageAngelByMassage.Count == 1)
                            {
                                //old room update status ready
                                var oldRoom = await _roomService.GetByIdAsync(massageAngel.RoomId);
                                oldRoom.Status = (int)ERoomStatus.Ready;
                                await _roomService.UpdateAsync(oldRoom);
                            }

                            var mmassageRoom = await _massageService.DuplicateRoomMaster(roomMaster.Id);

                            massageAngel.RoomId = room.Id;
                            massageAngel.MassageId = mmassageRoom.MassageId;
                            await _massageService.UpdateMassageAngel(massageAngel);

                            //new room update status working
                            room.Status = (int)ERoomStatus.Working;
                            await _roomService.UpdateAsync(room);

                        }
                        else
                        {
                            if (massageAngelByMassage.Count == 1)
                            {
                                //old room update status ready
                                var oldRoom = await _roomService.GetByIdAsync(massageAngel.RoomId);
                                oldRoom.Status = (int)ERoomStatus.Ready;
                                await _roomService.UpdateAsync(oldRoom);
                            }

                            massageAngel.RoomId = room.Id;
                            await _massageService.UpdateMassageAngel(massageAngel);

                            Massage massage = new Massage
                            {
                                CalendarDayId = (int)calenDarDayId,
                                IsPaid = false,
                                IsUnPaid = false,
                                Total = 0,
                                AngelTotal = 0,
                                FoodTotal = 0,
                                RoomTotal = 0,
                                QrCodeTotal = 0,
                                IsCashAngel = false,
                                IsCreditAngel = false,
                                IsMemberAngel = false,
                                IsQrCodeAngel = false,
                                CashAngelTotal = 0,
                                CreditAngelTotal = 0,
                                MemberAngelTotal = 0,
                                QrCodeAngelTotal = 0,
                                IsCashFood = false,
                                IsCreditFood = false,
                                IsMemberFood = false,
                                IsQrCodeFood = false,
                                CashFoodTotal = 0,
                                CreditFoodTotal = 0,
                                MemberFoodTotal = 0,
                                QrCodeFoodTotal = 0,
                                IsCashRoom = false,
                                IsCreditRoom = false,
                                IsMemberRoom = false,
                                IsQrCodeRoom = false,
                                CashRoomTotal = 0,
                                CreditRoomTotal = 0,
                                MemberRoomTotal = 0,
                                QrCodeRoomTotal = 0,
                                IsEntertainAngel = false,
                                EntertainTotalAngel = 0,
                                EntertainRemarkAngel = "",
                                IsEntertainFood = false,
                                EntertainTotalFood = 0,
                                EntertainRemarkFood = "",
                                PayDate = null,
                                DocumentNumber = documentNumber,
                                TipQrCodeTotal = 0,
                                CountriesId = _massage.CountriesId,
                                AgenciesId = _massage.AgenciesId,
                            };
                            await _massageService.CreateAsync(massage);

                            massageAngel.MassageId = massage.Id;
                            massageAngel.RoomId = room.Id;

                            await _massageService.UpdateMassageAngel(massageAngel);

                            //new room update status working
                            room.Status = (int)ERoomStatus.Working;
                            await _roomService.UpdateAsync(room);

                            DateTime dateNow = RoundUp(DateTime.Now, TimeSpan.FromMinutes(5));
                            MassageRoom massageRoom = new MassageRoom();
                            massageRoom.MassageId = massageAngel.MassageId;
                            massageRoom.RoomId = room.Id;
                            massageRoom.ReceptionId = massageAngel.ReceptionId;
                            massageRoom.CheckInTime = dateNow;
                            massageRoom.CheckOutTime = null;
                            massageRoom.OldCheckInTime = dateNow;
                            massageRoom.TimeMinute = 0;
                            massageRoom.LastCallTime = null;
                            massageRoom.CallCount = 0;
                            massageRoom.IsNotCall = massageAngel.IsNotCall;
                            await _massageService.CheckInMassageRoom(massageRoom);
                        }
                    }
                    // ธรรมดา --> สูทย่อย
                    else if ((massageAngel.Room.RoomTypeId == (int)ERoomType.Normal) && (room.RoomTypeId == (int)ERoomType.SuiteSub))
                    {
                        int? calenDarDayId = await GetCalendarDayId();
                        string documentNumber = _settingService.GetDocumentNumberAndUpdate((int)calenDarDayId, false);
                        var roomMaster = await _roomService.GetByIdAsync((int)room.ParentRoomId);

                        bool isDuplicate = await _massageService.CheckDuplicateRoomMaster(roomMaster.Id);

                        if (isDuplicate == true)
                        {

                            if (massageAngelByMassage.Count == 1)
                            {
                                //old room update status ready
                                var oldRoom = await _roomService.GetByIdAsync(massageAngel.RoomId);
                                oldRoom.Status = (int)ERoomStatus.Ready;
                                await _roomService.UpdateAsync(oldRoom);
                            }
                            var mmassageRoom = await _massageService.DuplicateRoomMaster(roomMaster.Id);

                            massageAngel.RoomId = room.Id;
                            massageAngel.MassageId = mmassageRoom.MassageId;
                            await _massageService.UpdateMassageAngel(massageAngel);

                            //new room update status working
                            room.Status = (int)ERoomStatus.Working;
                            await _roomService.UpdateAsync(room);

                            roomMaster.Status = (int)ERoomStatus.Working;
                            await _roomService.UpdateAsync(roomMaster);

                        }
                        else
                        {

                            //if (massageAngelByMassage.Count == 1)
                            //{
                            //    //old room update status ready
                            //    var oldRoom = await _roomService.GetByIdAsync(massageAngel.RoomId);
                            //    oldRoom.Status = (int)ERoomStatus.Ready;
                            //    await _roomService.UpdateAsync(oldRoom);
                            //}
                            if (massageAngelByMassage.Count == 1)
                            {
                                //old room update status ready
                                var oldRoom = await _roomService.GetByIdAsync(massageAngel.RoomId);
                                oldRoom.Status = (int)ERoomStatus.Ready;
                                await _roomService.UpdateAsync(oldRoom);
                            }

                            massageAngel.RoomId = room.Id;
                            await _massageService.UpdateMassageAngel(massageAngel);

                            Massage massage = new Massage
                            {
                                CalendarDayId = (int)calenDarDayId,
                                IsPaid = false,
                                IsUnPaid = false,
                                Total = 0,
                                AngelTotal = 0,
                                FoodTotal = 0,
                                RoomTotal = 0,
                                QrCodeTotal = 0,
                                IsCashAngel = false,
                                IsCreditAngel = false,
                                IsMemberAngel = false,
                                IsQrCodeAngel = false,
                                CashAngelTotal = 0,
                                CreditAngelTotal = 0,
                                MemberAngelTotal = 0,
                                QrCodeAngelTotal = 0,
                                IsCashFood = false,
                                IsCreditFood = false,
                                IsMemberFood = false,
                                IsQrCodeFood = false,
                                CashFoodTotal = 0,
                                CreditFoodTotal = 0,
                                MemberFoodTotal = 0,
                                QrCodeFoodTotal = 0,
                                IsCashRoom = false,
                                IsCreditRoom = false,
                                IsMemberRoom = false,
                                IsQrCodeRoom = false,
                                CashRoomTotal = 0,
                                CreditRoomTotal = 0,
                                MemberRoomTotal = 0,
                                QrCodeRoomTotal = 0,
                                IsEntertainAngel = false,
                                EntertainTotalAngel = 0,
                                EntertainRemarkAngel = "",
                                IsEntertainFood = false,
                                EntertainTotalFood = 0,
                                EntertainRemarkFood = "",
                                PayDate = null,
                                DocumentNumber = documentNumber,
                                TipQrCodeTotal = 0,
                                CountriesId = _massage.CountriesId,
                                AgenciesId = _massage.AgenciesId,
                            };
                            await _massageService.CreateAsync(massage);

                            massageAngel.MassageId = massage.Id;
                            massageAngel.RoomId = room.Id;

                            await _massageService.UpdateMassageAngel(massageAngel);

                            //new room update status working
                            room.Status = (int)ERoomStatus.Working;
                            await _roomService.UpdateAsync(room);

                            roomMaster.Status = (int)ERoomStatus.Working;
                            await _roomService.UpdateAsync(roomMaster);

                            DateTime dateNow = RoundUp(DateTime.Now, TimeSpan.FromMinutes(5));
                            MassageRoom massageRoom = new MassageRoom();
                            massageRoom.MassageId = massageAngel.MassageId;
                            massageRoom.RoomId = roomMaster.Id;
                            massageRoom.ReceptionId = massageAngel.ReceptionId;
                            massageRoom.CheckInTime = dateNow;
                            massageRoom.CheckOutTime = null;
                            massageRoom.OldCheckInTime = dateNow;
                            massageRoom.TimeMinute = 0;
                            massageRoom.LastCallTime = null;
                            massageRoom.CallCount = 0;
                            massageRoom.IsNotCall = massageAngel.IsNotCall;
                            await _massageService.CheckInMassageRoom(massageRoom);
                        }

                    }
                    // vip --> ธรรมดา
                    else if ((massageAngel.Room.RoomTypeId == (int)ERoomType.Vip || massageAngel.Room.RoomTypeId == (int)ERoomType.Vip2 || massageAngel.Room.RoomTypeId == (int)ERoomType.Vip3) && (room.RoomTypeId == (int)ERoomType.Normal))
                    {
                        int? calenDarDayId = await GetCalendarDayId();
                        string documentNumber = _settingService.GetDocumentNumberAndUpdate((int)calenDarDayId, false);
                        var roomMaster = await _roomService.GetByIdAsync((int)room.Id);
                        bool isDuplicate = await _massageService.CheckDuplicateMassageAngelRoomMaster(room.Id);

                        if (isDuplicate == true)
                        {
                            //if (massageAngelByMassage.Count == 1)
                            //{
                            //    //old room update status ready
                            //    var oldRoom = await _roomService.GetByIdAsync(massageAngel.RoomId);
                            //    oldRoom.Status = (int)ERoomStatus.Ready;
                            //    await _roomService.UpdateAsync(oldRoom);
                            //}
                            var mmassageAngel = await _massageService.DuplicateAngel(room.Id);

                            massageAngel.RoomId = room.Id;
                            _logger.LogDebug("AAA" + mmassageAngel.MassageId);
                            massageAngel.MassageId = mmassageAngel.MassageId;
                            await _massageService.UpdateMassageAngel(massageAngel);

                            //new room update status working
                            room.Status = (int)ERoomStatus.Working;
                            await _roomService.UpdateAsync(room);
                        }
                        else
                        {
                            //if (massageAngelByMassage.Count == 1)
                            //{
                            //    //old room update status ready
                            //    var oldRoom = await _roomService.GetByIdAsync(massageAngel.RoomId);
                            //    oldRoom.Status = (int)ERoomStatus.Ready;
                            //    await _roomService.UpdateAsync(oldRoom);
                            //}

                            Massage massage = new Massage
                            {
                                CalendarDayId = (int)calenDarDayId,
                                IsPaid = false,
                                IsUnPaid = false,
                                Total = 0,
                                AngelTotal = 0,
                                FoodTotal = 0,
                                RoomTotal = 0,
                                QrCodeTotal = 0,
                                IsCashAngel = false,
                                IsCreditAngel = false,
                                IsMemberAngel = false,
                                IsQrCodeAngel = false,
                                CashAngelTotal = 0,
                                CreditAngelTotal = 0,
                                MemberAngelTotal = 0,
                                QrCodeAngelTotal = 0,
                                IsCashFood = false,
                                IsCreditFood = false,
                                IsMemberFood = false,
                                IsQrCodeFood = false,
                                CashFoodTotal = 0,
                                CreditFoodTotal = 0,
                                MemberFoodTotal = 0,
                                QrCodeFoodTotal = 0,
                                IsCashRoom = false,
                                IsCreditRoom = false,
                                IsMemberRoom = false,
                                IsQrCodeRoom = false,
                                CashRoomTotal = 0,
                                CreditRoomTotal = 0,
                                MemberRoomTotal = 0,
                                QrCodeRoomTotal = 0,
                                IsEntertainAngel = false,
                                EntertainTotalAngel = 0,
                                EntertainRemarkAngel = "",
                                IsEntertainFood = false,
                                EntertainTotalFood = 0,
                                EntertainRemarkFood = "",
                                PayDate = null,
                                DocumentNumber = documentNumber,
                                TipQrCodeTotal = 0,
                                CountriesId = _massage.CountriesId,
                                AgenciesId = _massage.AgenciesId,
                            };
                            await _massageService.CreateAsync(massage);

                            massageAngel.MassageId = massage.Id;
                            massageAngel.RoomId = room.Id;

                            await _massageService.UpdateMassageAngel(massageAngel);

                            //massageRoom Update New MassageId
                            var mmassageRoom = await _massageService.GetMassageRoomByMassageId((int)request.MassageId);
                            var _mmassageRoom = await _massageService.GetMassageRoomById(mmassageRoom.Id);
                            _mmassageRoom.NewMassageId = massage.Id;
                            await _massageService.UpdateMassageRoom(_mmassageRoom);

                            //new room update status working
                            room.Status = (int)ERoomStatus.Working;
                            await _roomService.UpdateAsync(room);
                        }

                    }
                    // vip --> สูทย่อย
                    else if ((massageAngel.Room.RoomTypeId == (int)ERoomType.Vip || massageAngel.Room.RoomTypeId == (int)ERoomType.Vip2 || massageAngel.Room.RoomTypeId == (int)ERoomType.Vip3) && (room.RoomTypeId == (int)ERoomType.SuiteSub))
                    {
                        int? calenDarDayId = await GetCalendarDayId();
                        string documentNumber = _settingService.GetDocumentNumberAndUpdate((int)calenDarDayId, false);

                        var roomMaster = await _roomService.GetByIdAsync((int)room.ParentRoomId);
                        bool isDuplicate = await _massageService.CheckDuplicateRoomMaster(roomMaster.Id);

                        if (isDuplicate == true)
                        {
                            var mmassageRoom = await _massageService.DuplicateRoomMaster(roomMaster.Id);
                            //if (massageAngelByMassage.Count == 1)
                            //{
                            //    //old room update status ready
                            //    var oldRoom = await _roomService.GetByIdAsync(massageAngel.RoomId);
                            //    oldRoom.Status = (int)ERoomStatus.Ready;
                            //    await _roomService.UpdateAsync(oldRoom);
                            //}

                            massageAngel.RoomId = room.Id;
                            massageAngel.MassageId = mmassageRoom.MassageId;
                            await _massageService.UpdateMassageAngel(massageAngel);

                            //new room update status working
                            room.Status = (int)ERoomStatus.Working;
                            await _roomService.UpdateAsync(room);
                            roomMaster.Status = (int)ERoomStatus.Working;
                            await _roomService.UpdateAsync(roomMaster);

                        }
                        else
                        {
                            //if (massageAngelByMassage.Count == 1)
                            //{
                            //    //old room update status ready
                            //    var oldRoom = await _roomService.GetByIdAsync(massageAngel.RoomId);
                            //    oldRoom.Status = (int)ERoomStatus.Ready;
                            //    await _roomService.UpdateAsync(oldRoom);
                            //}

                            Massage massage = new Massage
                            {
                                CalendarDayId = (int)calenDarDayId,
                                IsPaid = false,
                                IsUnPaid = false,
                                Total = 0,
                                AngelTotal = 0,
                                FoodTotal = 0,
                                RoomTotal = 0,
                                QrCodeTotal = 0,
                                IsCashAngel = false,
                                IsCreditAngel = false,
                                IsMemberAngel = false,
                                IsQrCodeAngel = false,
                                CashAngelTotal = 0,
                                CreditAngelTotal = 0,
                                MemberAngelTotal = 0,
                                QrCodeAngelTotal = 0,
                                IsCashFood = false,
                                IsCreditFood = false,
                                IsMemberFood = false,
                                IsQrCodeFood = false,
                                CashFoodTotal = 0,
                                CreditFoodTotal = 0,
                                MemberFoodTotal = 0,
                                QrCodeFoodTotal = 0,
                                IsCashRoom = false,
                                IsCreditRoom = false,
                                IsMemberRoom = false,
                                IsQrCodeRoom = false,
                                CashRoomTotal = 0,
                                CreditRoomTotal = 0,
                                MemberRoomTotal = 0,
                                QrCodeRoomTotal = 0,
                                IsEntertainAngel = false,
                                EntertainTotalAngel = 0,
                                EntertainRemarkAngel = "",
                                IsEntertainFood = false,
                                EntertainTotalFood = 0,
                                EntertainRemarkFood = "",
                                PayDate = null,
                                DocumentNumber = documentNumber,
                                TipQrCodeTotal = 0,
                                CountriesId = _massage.CountriesId,
                                AgenciesId = _massage.AgenciesId,
                            };
                            await _massageService.CreateAsync(massage);

                            massageAngel.MassageId = massage.Id;
                            massageAngel.RoomId = room.Id;

                            await _massageService.UpdateMassageAngel(massageAngel);

                            //new room update status working
                            room.Status = (int)ERoomStatus.Working;
                            await _roomService.UpdateAsync(room);
                            roomMaster.Status = (int)ERoomStatus.Working;
                            await _roomService.UpdateAsync(roomMaster);

                            //massageRoom Update New MassageId
                            var mmassageRoom = await _massageService.GetMassageRoomByMassageId((int)request.MassageId);
                            var _mmassageRoom = await _massageService.GetMassageRoomById(mmassageRoom.Id);
                            _mmassageRoom.NewMassageId = massage.Id;
                            await _massageService.UpdateMassageRoom(_mmassageRoom);

                            //DateTime dateNow = RoundUp(DateTime.Now, TimeSpan.FromMinutes(5));
                            MassageRoom massageRoom = new MassageRoom();
                            massageRoom.MassageId = massageAngel.MassageId;
                            massageRoom.RoomId = roomMaster.Id;
                            massageRoom.ReceptionId = massageAngel.ReceptionId;
                            massageRoom.CheckInTime = mmassageRoom.CheckInTime;
                            massageRoom.CheckOutTime = null;
                            massageRoom.OldCheckInTime = mmassageRoom.OldCheckInTime;
                            massageRoom.TimeMinute = 0;
                            massageRoom.LastCallTime = null;
                            massageRoom.CallCount = 0;
                            massageRoom.IsNotCall = massageAngel.IsNotCall;
                            await _massageService.CheckInMassageRoom(massageRoom);

                            var _roomMaster = await _roomService.GetByIdAsync(roomMaster.Id);
                            _roomMaster.Status = (int)ERoomStatus.Working;
                            await _roomService.UpdateAsync(_roomMaster);
                        }
                    }
                    //VIP --> VIP
                    else if ((massageAngel.Room.RoomTypeId == (int)ERoomType.Vip || massageAngel.Room.RoomTypeId == (int)ERoomType.Vip2 || massageAngel.Room.RoomTypeId == (int)ERoomType.Vip3) && (room.RoomTypeId == (int)ERoomType.Vip || room.RoomTypeId == (int)ERoomType.Vip2 || room.RoomTypeId == (int)ERoomType.Vip3))
                    {
                        int? calenDarDayId = await GetCalendarDayId();
                        string documentNumber = _settingService.GetDocumentNumberAndUpdate((int)calenDarDayId, false);
                        var roomMaster = await _roomService.GetByIdAsync((int)room.Id);
                        bool isDuplicate = await _massageService.CheckDuplicateRoomMaster(roomMaster.Id);

                        if (isDuplicate == true)
                        {
                            //if (massageAngelByMassage.Count == 1)
                            //{
                            //    //old room update status ready
                            //    var oldRoom = await _roomService.GetByIdAsync(massageAngel.RoomId);
                            //    oldRoom.Status = (int)ERoomStatus.Ready;
                            //    await _roomService.UpdateAsync(oldRoom);
                            //}
                            var mmassageRoom = await _massageService.DuplicateRoomMaster(roomMaster.Id);

                            massageAngel.RoomId = room.Id;
                            massageAngel.MassageId = mmassageRoom.MassageId;
                            await _massageService.UpdateMassageAngel(massageAngel);

                            //new room update status working
                            room.Status = (int)ERoomStatus.Working;
                            await _roomService.UpdateAsync(room);

                        }
                        else
                        {
                            //if (massageAngelByMassage.Count == 1)
                            //{
                            //    //old room update status ready
                            //    var oldRoom = await _roomService.GetByIdAsync(massageAngel.RoomId);
                            //    oldRoom.Status = (int)ERoomStatus.Ready;
                            //    await _roomService.UpdateAsync(oldRoom);
                            //}

                            massageAngel.RoomId = room.Id;
                            await _massageService.UpdateMassageAngel(massageAngel);

                            Massage massage = new Massage
                            {
                                CalendarDayId = (int)calenDarDayId,
                                IsPaid = false,
                                IsUnPaid = false,
                                Total = 0,
                                AngelTotal = 0,
                                FoodTotal = 0,
                                RoomTotal = 0,
                                QrCodeTotal = 0,
                                IsCashAngel = false,
                                IsCreditAngel = false,
                                IsMemberAngel = false,
                                IsQrCodeAngel = false,
                                CashAngelTotal = 0,
                                CreditAngelTotal = 0,
                                MemberAngelTotal = 0,
                                QrCodeAngelTotal = 0,
                                IsCashFood = false,
                                IsCreditFood = false,
                                IsMemberFood = false,
                                IsQrCodeFood = false,
                                CashFoodTotal = 0,
                                CreditFoodTotal = 0,
                                MemberFoodTotal = 0,
                                QrCodeFoodTotal = 0,
                                IsCashRoom = false,
                                IsCreditRoom = false,
                                IsMemberRoom = false,
                                IsQrCodeRoom = false,
                                CashRoomTotal = 0,
                                CreditRoomTotal = 0,
                                MemberRoomTotal = 0,
                                QrCodeRoomTotal = 0,
                                IsEntertainAngel = false,
                                EntertainTotalAngel = 0,
                                EntertainRemarkAngel = "",
                                IsEntertainFood = false,
                                EntertainTotalFood = 0,
                                EntertainRemarkFood = "",
                                PayDate = null,
                                DocumentNumber = documentNumber,
                                TipQrCodeTotal = 0,
                                CountriesId = _massage.CountriesId,
                                AgenciesId = _massage.AgenciesId,
                            };
                            await _massageService.CreateAsync(massage);

                            massageAngel.MassageId = massage.Id;
                            massageAngel.RoomId = room.Id;

                            await _massageService.UpdateMassageAngel(massageAngel);

                            //new room update status working
                            room.Status = (int)ERoomStatus.Working;
                            await _roomService.UpdateAsync(room);

                            //massageRoom Update New MassageId
                            var mmassageRoom = await _massageService.GetMassageRoomByMassageId((int)request.MassageId);
                            var _mmassageRoom = await _massageService.GetMassageRoomById(mmassageRoom.Id);
                            _mmassageRoom.NewMassageId = massage.Id;
                            await _massageService.UpdateMassageRoom(_mmassageRoom);

                            //DateTime dateNow = RoundUp(DateTime.Now, TimeSpan.FromMinutes(5));
                            MassageRoom massageRoom = new MassageRoom();
                            massageRoom.MassageId = massageAngel.MassageId;
                            massageRoom.RoomId = room.Id;
                            massageRoom.ReceptionId = massageAngel.ReceptionId;
                            massageRoom.CheckInTime = mmassageRoom.CheckInTime;
                            massageRoom.CheckOutTime = null;
                            massageRoom.OldCheckInTime = mmassageRoom.OldCheckInTime;
                            massageRoom.TimeMinute = 0;
                            massageRoom.LastCallTime = null;
                            massageRoom.CallCount = 0;
                            massageRoom.IsNotCall = massageAngel.IsNotCall;
                            await _massageService.CheckInMassageRoom(massageRoom);
                        }
                    }
                    //สูทย่อย --> VIP
                    else if ((massageAngel.Room.RoomTypeId == (int)ERoomType.SuiteSub) && (room.RoomTypeId == (int)ERoomType.Vip || room.RoomTypeId == (int)ERoomType.Vip2 || room.RoomTypeId == (int)ERoomType.Vip3))
                    {
                        int? calenDarDayId = await GetCalendarDayId();
                        string documentNumber = _settingService.GetDocumentNumberAndUpdate((int)calenDarDayId, false);
                        var roomMaster = await _roomService.GetByIdAsync((int)room.Id);
                        bool isDuplicate = await _massageService.CheckDuplicateRoomMaster(roomMaster.Id);

                        if (isDuplicate == true)
                        {
                            if (massageAngelByMassage.Count == 1)
                            {
                                //old room update status ready
                                var oldRoom = await _roomService.GetByIdAsync(massageAngel.RoomId);
                                oldRoom.Status = (int)ERoomStatus.Ready;
                                await _roomService.UpdateAsync(oldRoom);
                                //roomMaster.Status = (int)ERoomStatus.Ready;
                                //await _roomService.UpdateAsync(roomMaster);
                            }

                            var mmassageRoom = await _massageService.DuplicateRoomMaster(roomMaster.Id);

                            massageAngel.RoomId = room.Id;
                            massageAngel.MassageId = mmassageRoom.MassageId;
                            await _massageService.UpdateMassageAngel(massageAngel);

                            //new room update status working
                            room.Status = (int)ERoomStatus.Working;
                            await _roomService.UpdateAsync(room);

                        }
                        else
                        {
                            if (massageAngelByMassage.Count == 1)
                            {
                                //old room update status ready
                                var oldRoom = await _roomService.GetByIdAsync(massageAngel.RoomId);
                                oldRoom.Status = (int)ERoomStatus.Ready;
                                await _roomService.UpdateAsync(oldRoom);
                            }

                            massageAngel.RoomId = room.Id;
                            await _massageService.UpdateMassageAngel(massageAngel);

                            Massage massage = new Massage
                            {
                                CalendarDayId = (int)calenDarDayId,
                                IsPaid = false,
                                IsUnPaid = false,
                                Total = 0,
                                AngelTotal = 0,
                                FoodTotal = 0,
                                RoomTotal = 0,
                                QrCodeTotal = 0,
                                IsCashAngel = false,
                                IsCreditAngel = false,
                                IsMemberAngel = false,
                                IsQrCodeAngel = false,
                                CashAngelTotal = 0,
                                CreditAngelTotal = 0,
                                MemberAngelTotal = 0,
                                QrCodeAngelTotal = 0,
                                IsCashFood = false,
                                IsCreditFood = false,
                                IsMemberFood = false,
                                IsQrCodeFood = false,
                                CashFoodTotal = 0,
                                CreditFoodTotal = 0,
                                MemberFoodTotal = 0,
                                QrCodeFoodTotal = 0,
                                IsCashRoom = false,
                                IsCreditRoom = false,
                                IsMemberRoom = false,
                                IsQrCodeRoom = false,
                                CashRoomTotal = 0,
                                CreditRoomTotal = 0,
                                MemberRoomTotal = 0,
                                QrCodeRoomTotal = 0,
                                IsEntertainAngel = false,
                                EntertainTotalAngel = 0,
                                EntertainRemarkAngel = "",
                                IsEntertainFood = false,
                                EntertainTotalFood = 0,
                                EntertainRemarkFood = "",
                                PayDate = null,
                                DocumentNumber = documentNumber,
                                TipQrCodeTotal = 0,
                                CountriesId = _massage.CountriesId,
                                AgenciesId = _massage.AgenciesId,
                            };
                            await _massageService.CreateAsync(massage);

                            massageAngel.MassageId = massage.Id;
                            massageAngel.RoomId = room.Id;

                            await _massageService.UpdateMassageAngel(massageAngel);

                            //new room update status working
                            room.Status = (int)ERoomStatus.Working;
                            await _roomService.UpdateAsync(room);

                            //massageRoom Update New MassageId
                            var mmassageRoom = await _massageService.GetMassageRoomByMassageId((int)request.MassageId);
                            var _mmassageRoom = await _massageService.GetMassageRoomById(mmassageRoom.Id);
                            _mmassageRoom.NewMassageId = massage.Id;
                            await _massageService.UpdateMassageRoom(_mmassageRoom);

                            //DateTime dateNow = RoundUp(DateTime.Now, TimeSpan.FromMinutes(5));
                            MassageRoom massageRoom = new MassageRoom();
                            massageRoom.MassageId = massageAngel.MassageId;
                            massageRoom.RoomId = room.Id;
                            massageRoom.ReceptionId = massageAngel.ReceptionId;
                            massageRoom.CheckInTime = mmassageRoom.CheckInTime;
                            massageRoom.CheckOutTime = null;
                            massageRoom.OldCheckInTime = mmassageRoom.OldCheckInTime;
                            massageRoom.TimeMinute = 0;
                            massageRoom.LastCallTime = null;
                            massageRoom.CallCount = 0;
                            massageRoom.IsNotCall = massageAngel.IsNotCall;
                            await _massageService.CheckInMassageRoom(massageRoom);
                        }
                    }
                    //สูทย่อย --> สูทย่อย
                    else if ((massageAngel.Room.RoomTypeId == (int)ERoomType.SuiteSub) && (room.RoomTypeId == (int)ERoomType.SuiteSub))
                    {
                        int? calenDarDayId = await GetCalendarDayId();
                        string documentNumber = _settingService.GetDocumentNumberAndUpdate((int)calenDarDayId, false);

                        var roomMaster = await _roomService.GetByIdAsync((int)room.ParentRoomId);
                        bool isDuplicate = await _massageService.CheckDuplicateRoomMaster(roomMaster.Id);

                        if (isDuplicate == true)
                        {
                            var mmassageRoom = await _massageService.DuplicateRoomMaster(roomMaster.Id);

                            if (massageAngelByMassage.Count == 1)
                            {
                                //old room update status ready
                                var oldRoom = await _roomService.GetByIdAsync(massageAngel.RoomId);
                                oldRoom.Status = (int)ERoomStatus.Ready;
                                await _roomService.UpdateAsync(oldRoom);
                            }

                            massageAngel.RoomId = room.Id;
                            massageAngel.MassageId = mmassageRoom.MassageId;
                            await _massageService.UpdateMassageAngel(massageAngel);

                            //new room update status working
                            room.Status = (int)ERoomStatus.Working;
                            await _roomService.UpdateAsync(room);
                            roomMaster.Status = (int)ERoomStatus.Working;
                            await _roomService.UpdateAsync(roomMaster);
                        }
                        else
                        {
                            if (massageAngelByMassage.Count == 1)
                            {
                                //old room update status ready
                                var oldRoom = await _roomService.GetByIdAsync(massageAngel.RoomId);
                                oldRoom.Status = (int)ERoomStatus.Ready;
                                await _roomService.UpdateAsync(oldRoom);
                            }

                            Massage massage = new Massage
                            {
                                CalendarDayId = (int)calenDarDayId,
                                IsPaid = false,
                                IsUnPaid = false,
                                Total = 0,
                                AngelTotal = 0,
                                FoodTotal = 0,
                                RoomTotal = 0,
                                QrCodeTotal = 0,
                                IsCashAngel = false,
                                IsCreditAngel = false,
                                IsMemberAngel = false,
                                IsQrCodeAngel = false,
                                CashAngelTotal = 0,
                                CreditAngelTotal = 0,
                                MemberAngelTotal = 0,
                                QrCodeAngelTotal = 0,
                                IsCashFood = false,
                                IsCreditFood = false,
                                IsMemberFood = false,
                                IsQrCodeFood = false,
                                CashFoodTotal = 0,
                                CreditFoodTotal = 0,
                                MemberFoodTotal = 0,
                                QrCodeFoodTotal = 0,
                                IsCashRoom = false,
                                IsCreditRoom = false,
                                IsMemberRoom = false,
                                IsQrCodeRoom = false,
                                CashRoomTotal = 0,
                                CreditRoomTotal = 0,
                                MemberRoomTotal = 0,
                                QrCodeRoomTotal = 0,
                                IsEntertainAngel = false,
                                EntertainTotalAngel = 0,
                                EntertainRemarkAngel = "",
                                IsEntertainFood = false,
                                EntertainTotalFood = 0,
                                EntertainRemarkFood = "",
                                PayDate = null,
                                DocumentNumber = documentNumber,
                                TipQrCodeTotal = 0,
                                CountriesId = _massage.CountriesId,
                                AgenciesId = _massage.AgenciesId,
                            };
                            await _massageService.CreateAsync(massage);

                            massageAngel.MassageId = massage.Id;
                            massageAngel.RoomId = room.Id;

                            await _massageService.UpdateMassageAngel(massageAngel);

                            //new room update status working
                            room.Status = (int)ERoomStatus.Working;
                            await _roomService.UpdateAsync(room);
                            roomMaster.Status = (int)ERoomStatus.Working;
                            await _roomService.UpdateAsync(roomMaster);

                            //massageRoom Update New MassageId
                            var mmassageRoom = await _massageService.GetMassageRoomByMassageId((int)request.MassageId);
                            var _mmassageRoom = await _massageService.GetMassageRoomById(mmassageRoom.Id);
                            _mmassageRoom.NewMassageId = massage.Id;
                            await _massageService.UpdateMassageRoom(_mmassageRoom);

                            //DateTime dateNow = RoundUp(DateTime.Now, TimeSpan.FromMinutes(5));
                            MassageRoom massageRoom = new MassageRoom();
                            massageRoom.MassageId = massageAngel.MassageId;
                            massageRoom.RoomId = roomMaster.Id;
                            massageRoom.ReceptionId = massageAngel.ReceptionId;
                            massageRoom.CheckInTime = mmassageRoom.CheckInTime;
                            massageRoom.CheckOutTime = null;
                            massageRoom.OldCheckInTime = mmassageRoom.OldCheckInTime;
                            massageRoom.TimeMinute = 0;
                            massageRoom.LastCallTime = null;
                            massageRoom.CallCount = 0;
                            massageRoom.IsNotCall = massageAngel.IsNotCall;
                            await _massageService.CheckInMassageRoom(massageRoom);

                            var _roomMaster = await _roomService.GetByIdAsync(roomMaster.Id);
                            _roomMaster.Status = (int)ERoomStatus.Working;
                            await _roomService.UpdateAsync(_roomMaster);
                        }
                    }
                    //สูทย่อย --> ธรรมดา
                    else if ((massageAngel.Room.RoomTypeId == (int)ERoomType.SuiteSub) && (room.RoomTypeId == (int)ERoomType.Normal))
                    {
                        int? calenDarDayId = await GetCalendarDayId();
                        string documentNumber = _settingService.GetDocumentNumberAndUpdate((int)calenDarDayId, false);
                        var roomMaster = await _roomService.GetByIdAsync((int)room.Id);
                        bool isDuplicate = await _massageService.CheckDuplicateMassageAngelRoomMaster(roomMaster.Id);

                        if (isDuplicate == true)
                        {
                            if (massageAngelByMassage.Count == 1)
                            {
                                //old room update status ready
                                var oldRoom = await _roomService.GetByIdAsync(massageAngel.RoomId);
                                oldRoom.Status = (int)ERoomStatus.Ready;
                                await _roomService.UpdateAsync(oldRoom);
                                //roomMaster.Status = (int)ERoomStatus.Ready;
                                //await _roomService.UpdateAsync(roomMaster);
                            }
                            var mmassageAngel = await _massageService.DuplicateAngel(room.Id);

                            massageAngel.RoomId = room.Id;
                            massageAngel.MassageId = mmassageAngel.MassageId;
                            await _massageService.UpdateMassageAngel(massageAngel);

                            //new room update status working
                            room.Status = (int)ERoomStatus.Working;
                            await _roomService.UpdateAsync(room);
                        }
                        else
                        {
                            if (massageAngelByMassage.Count == 1)
                            {
                                //old room update status ready
                                var oldRoom = await _roomService.GetByIdAsync(massageAngel.RoomId);
                                oldRoom.Status = (int)ERoomStatus.Ready;
                                await _roomService.UpdateAsync(oldRoom);
                                //roomMaster.Status = (int)ERoomStatus.Ready;
                                //await _roomService.UpdateAsync(roomMaster);
                            }

                            Massage massage = new Massage
                            {
                                CalendarDayId = (int)calenDarDayId,
                                IsPaid = false,
                                IsUnPaid = false,
                                Total = 0,
                                AngelTotal = 0,
                                FoodTotal = 0,
                                RoomTotal = 0,
                                QrCodeTotal = 0,
                                IsCashAngel = false,
                                IsCreditAngel = false,
                                IsMemberAngel = false,
                                IsQrCodeAngel = false,
                                CashAngelTotal = 0,
                                CreditAngelTotal = 0,
                                MemberAngelTotal = 0,
                                QrCodeAngelTotal = 0,
                                IsCashFood = false,
                                IsCreditFood = false,
                                IsMemberFood = false,
                                IsQrCodeFood = false,
                                CashFoodTotal = 0,
                                CreditFoodTotal = 0,
                                MemberFoodTotal = 0,
                                QrCodeFoodTotal = 0,
                                IsCashRoom = false,
                                IsCreditRoom = false,
                                IsMemberRoom = false,
                                IsQrCodeRoom = false,
                                CashRoomTotal = 0,
                                CreditRoomTotal = 0,
                                MemberRoomTotal = 0,
                                QrCodeRoomTotal = 0,
                                IsEntertainAngel = false,
                                EntertainTotalAngel = 0,
                                EntertainRemarkAngel = "",
                                IsEntertainFood = false,
                                EntertainTotalFood = 0,
                                EntertainRemarkFood = "",
                                PayDate = null,
                                DocumentNumber = documentNumber,
                                TipQrCodeTotal = 0,
                                CountriesId = _massage.CountriesId,
                                AgenciesId = _massage.AgenciesId,
                            };
                            await _massageService.CreateAsync(massage);

                            massageAngel.MassageId = massage.Id;
                            massageAngel.RoomId = room.Id;

                            await _massageService.UpdateMassageAngel(massageAngel);

                            //massageRoom Update New MassageId
                            var mmassageRoom = await _massageService.GetMassageRoomByMassageId((int)request.MassageId);
                            var _mmassageRoom = await _massageService.GetMassageRoomById(mmassageRoom.Id);
                            _mmassageRoom.NewMassageId = massage.Id;
                            await _massageService.UpdateMassageRoom(_mmassageRoom);

                            //new room update status working
                            room.Status = (int)ERoomStatus.Working;
                            await _roomService.UpdateAsync(room);
                        }
                    }

                    await transaction.CommitAsync();
                    return Ok();
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex.Message);
                    _logger.LogError(ex.StackTrace);
                    await transaction.RollbackAsync();
                    return BadRequest();
                }
            }
        }

        //[HttpPatch(ApiRoutes.Massages.ChangeRoom)] 
        //public async Task<IActionResult> ChangeRooms([FromBody] UpdateMassageAngelRequest request)
        //{
        //    using (var transaction = await _db.Database.BeginTransactionAsync())
        //    {
        //        try
        //        {
        //            int? calenDarDayId = await GetCalendarDayId();
        //            string documentNumber = _settingService.GetDocumentNumberAndUpdate((int)calenDarDayId, false);
        //            var room = await _roomService.GetByIdAsync((int)request.RoomId);
        //            var massageAngel = await _massageService.GetMassageAngelById((int)request.MassageAngelId);
        //            if (massageAngel != null)
        //            {
        //                //var MassageOldId = massageAngel.MassageId;
        //                await UpdateRoomInMassageAngel(massageAngel, (int)request.RoomId, request.IsChangeRoom);
        //                var massageRooms = await _massageService.GetMassageRoomsByMassageId(massageAngel.MassageId);

        //                if (room.RoomTypeId == (int)ERoomType.Vip)
        //                {
        //                    if(massageRooms.Count > 0)
        //                    {
        //                        if(massageRooms.Count == 1)
        //                        {
        //                            foreach(var item in massageRooms)
        //                            {
        //                                if(item.RoomId != (int)request.RoomId)
        //                                {

        //                                }
        //                            }
        //                        }
        //                    }
        //                    else
        //                    {
        //                        await CheckInNewRoom(room.Id, massageAngel.MassageId, massageAngel.ReceptionId, massageAngel.IsNotCall, massageAngel.RoomId);
        //                    }
        //                    //if (massageRooms.Count > 0)
        //                    //{
        //                    //    if (massageRooms.Count == 1)
        //                    //    {
        //                    //        foreach (var item in massageRooms)
        //                    //        {
        //                    //            if (item.RoomId != (int)request.RoomId)
        //                    //            {
        //                    //                var massage = await CreateMassageNew((int)calenDarDayId, documentNumber);

        //                    //                massageAngel.MassageId = massage.Id;

        //                    //                await _massageService.UpdateMassageAngel(massageAngel);

        //                    //                await CheckInNewRoom(room.Id, massageAngel.MassageId, massageAngel.ReceptionId, massageAngel.IsNotCall,massageAngel.RoomId);

        //                    //            }
        //                    //        }
        //                    //    }  
        //                    //}
        //                    //else
        //                    //{
        //                    //    await CheckInNewRoom(room.Id, massageAngel.MassageId, massageAngel.ReceptionId, massageAngel.IsNotCall, massageAngel.RoomId);
        //                    //}
        //                }
        //                else if (room.RoomTypeId == (int)ERoomType.SuiteSub)
        //                {
        //                    if (massageRooms.Count > 0)
        //                    {
        //                        if (massageRooms.Count == 1)
        //                        {
        //                            foreach (var item in massageRooms)
        //                            {
        //                                if (item.RoomId != (int)request.parentRoomId)
        //                                {
        //                                    var massage = await CreateMassageNew((int)calenDarDayId, documentNumber);

        //                                    massageAngel.MassageId = massage.Id;

        //                                    await _massageService.UpdateMassageAngel(massageAngel);

        //                                    await CheckInNewRoom((int)request.parentRoomId, massageAngel.MassageId, massageAngel.ReceptionId, massageAngel.IsNotCall, massageAngel.RoomId);


        //                                }
        //                            }
        //                        }
        //                    }
        //                    else
        //                    {
        //                        await CheckInNewRoom((int)request.parentRoomId, massageAngel.MassageId, massageAngel.ReceptionId, massageAngel.IsNotCall, massageAngel.RoomId);
        //                    }
        //                 }
        //            }
        //            //if (massageAngel != null)
        //            //{
        //            //    var massageRooms = await _massageService.GetMassageRoomsByMassageId(massageAngel.MassageId);
        //            //    if (massageRooms.Count > 1)
        //            //    {
        //            //        await UpdateRoomInMassageAngel(massageAngel, (int)request.RoomId, request.IsChangeRoom);
        //            //    }
        //            //    else
        //            //    {
        //            //        var roomOld = await _roomService.GetByIdAsync(massageAngel.RoomId);
        //            //        bool ischeck = false;
        //            //        if(massageRooms.Count == 0 && (room.RoomTypeId == (int)ERoomType.Suite || room.RoomTypeId == (int)ERoomType.Vip || room.RoomTypeId == (int)ERoomType.Suite3 || room.RoomTypeId == (int)ERoomType.Suite4 || room.RoomTypeId == (int)ERoomType.Suite5))
        //            //        {
        //            //            if(roomOld.RoomTypeId == (int)ERoomType.Normal)
        //            //            {
        //            //                await CheckInNewRoom(room.Id, massageAngel.MassageId, massageAngel.ReceptionId, massageAngel.IsNotCall, massageAngel.RoomId);
        //            //                await UpdateRoomInMassageAngel(massageAngel, room.Id, request.IsChangeRoom);
        //            //                ischeck = true;
        //            //            }
        //            //        }
        //            //        if (room.RoomTypeId == (int)ERoomType.Suite || room.RoomTypeId == (int)ERoomType.Vip || room.RoomTypeId == (int)ERoomType.Suite3 || room.RoomTypeId == (int)ERoomType.Suite4 || room.RoomTypeId == (int)ERoomType.Suite5)
        //            //        {
        //            //            if (!ischeck)
        //            //            {
        //            //                if(roomOld.RoomTypeId != (int)ERoomType.Normal)
        //            //                {
        //            //                    await CheckInNewRoom(room.Id, massageAngel.MassageId, massageAngel.ReceptionId, massageAngel.IsNotCall, massageAngel.RoomId);
        //            //                    await UpdateRoomInMassageAngel(massageAngel, room.Id, request.IsChangeRoom);
        //            //                }
        //            //                else
        //            //                {
        //            //                    await UpdateRoomInMassageAngel(massageAngel, room.Id, request.IsChangeRoom);
        //            //                }
        //            //            }
        //            //        }
        //            //        else
        //            //        {
        //            //            await UpdateRoomInMassageAngel(massageAngel, room.Id, request.IsChangeRoom);
        //            //        }
        //            //    }
        //            //}
        //            await transaction.CommitAsync();
        //            return Ok();
        //        }
        //        catch (Exception ex)
        //        {
        //            _logger.LogError(ex.Message);
        //            _logger.LogError(ex.StackTrace);
        //            await transaction.RollbackAsync();
        //            return BadRequest();
        //        }
        //    }
        //}
        [HttpPatch(ApiRoutes.Massages.UpdateIsNotCall)]
        public async Task<IActionResult> UpdateIsNotCall([FromBody] UpdateMassageAngelRequest request)
        {
            using (var transaction = await _db.Database.BeginTransactionAsync())
            {
                try
                {
                    if (request.IsAngel)
                    {
                        var massageAngel = await _massageService.GetMassageAngelById((int)request.MassageAngelId);
                        if (massageAngel == null)
                        {
                            return NotFound();
                        }
                        massageAngel.IsNotCall = true;
                        await _massageService.UpdateMassageAngel(massageAngel);
                    }

                    if (request.IsRoom)
                    {
                        var massageRoom = await _massageService.GetMassageRoomById((int)request.MassageRoomId);
                        if (massageRoom == null)
                        {
                            return NotFound();
                        }
                        massageRoom.IsNotCall = true;
                        await _massageService.UpdateMassageRoom(massageRoom);
                    }
                    await transaction.CommitAsync();
                    return Ok();
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex.Message);
                    _logger.LogError(ex.StackTrace);
                    await transaction.RollbackAsync();
                    return BadRequest();
                }
            }
        }
        [HttpPatch(ApiRoutes.Massages.UpdateLastCall)]
        public async Task<IActionResult> UpdateLastCall([FromBody] UpdateMassageAngelRequest request)
        {
            using (var transaction = await _db.Database.BeginTransactionAsync())
            {
                try
                {
                    if (request.IsAngel)
                    {
                        var massageAngel = await _massageService.GetMassageAngelById((int)request.MassageAngelId);
                        if (massageAngel == null)
                        {
                            return NotFound();
                        }
                        massageAngel.LastCallTime = DateTime.Now;
                        massageAngel.CallCount += 1;
                        await _massageService.UpdateMassageAngel(massageAngel);
                    }

                    if (request.IsRoom)
                    {
                        var massageRoom = await _massageService.GetMassageRoomById((int)request.MassageRoomId);
                        if (massageRoom == null)
                        {
                            return NotFound();
                        }
                        massageRoom.LastCallTime = DateTime.Now;
                        massageRoom.CallCount += 1;
                        await _massageService.UpdateMassageRoom(massageRoom);
                    }
                    await transaction.CommitAsync();
                    return Ok();
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex.Message);
                    _logger.LogError(ex.StackTrace);
                    await transaction.RollbackAsync();
                    return BadRequest();
                }
            }
        }
        [HttpGet(ApiRoutes.Massages.GetCalendarDay)]
        public async Task<IActionResult> GetCalendarDay()
        {
            try
            {
                var calendayDay = await _settingService.GetCarlendarDay();
                if (calendayDay == null)
                {
                    return NotFound();
                }
                return Ok(_mapper.Map<CalendarDayResponse>(calendayDay));
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }
        [HttpGet(ApiRoutes.Massages.GetBillCheckIn)]
        public async Task<IActionResult> GetMassageAngelById(int id)
        {
            try
            {
                var massageAngel = await _massageService.GetMassageAngelById(id);
                if (massageAngel == null)
                {
                    return NotFound();
                }
                return Ok(_mapper.Map<MassageAngelResponse>(massageAngel));
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }
        [HttpGet(ApiRoutes.Massages.GetBillCheckInAngelMassage)]
        public async Task<IActionResult> GetMassageAngelByMassageId(int id)
        {
            try
            {
                var massageAngel = await _massageService.GetMassageAngelByMassageId(id);
                if (massageAngel == null)
                {
                    return NotFound();
                }
                return Ok(_mapper.Map<List<MassageAngelResponse>>(massageAngel));
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }

        [HttpGet(ApiRoutes.Massages.GetWagePayment)]
        public async Task<IActionResult> GetWagePayment(DateTime? calendarDay,int? receptionId, string angelCode, string roomName)
        {
            try
            {
                //var calendayDay = await _settingService.GetCarlendarDay();
                //if (calendayDay == null)
                //{
                //    return NotFound();
                //}
                int calendarDayId = _settingService.GetCalendarId((DateTime)calendarDay);

                var massageAngel = await _massageService.GetWagePayment(calendarDayId);
              
                if (massageAngel == null)
                {
                    return NotFound();
                }
                if (receptionId != null) 
                {
                    massageAngel = massageAngel.Where(ma => ma.ReceptionId == receptionId).ToList();
                }
                if (angelCode != null)
                {
                    massageAngel = massageAngel.Where(a => a.Angel.Code == angelCode).ToList();
                }
                if (roomName != null)
                {
                    var room =  _db.Rooms.Where(r => r.RoomNo == roomName).SingleOrDefault();
                    if (room != null)
                    {
                        if (room.RoomTypeId == (int)ERoomType.Suite || room.RoomTypeId == (int)ERoomType.Suite3 || room.RoomTypeId == (int)ERoomType.Suite4 || room.RoomTypeId == (int)ERoomType.Suite5 || room.RoomTypeId == (int)ERoomType.Suite6 || room.RoomTypeId == (int)ERoomType.Suite7 || room.RoomTypeId == (int)ERoomType.Suite8 || room.RoomTypeId == (int)ERoomType.Suite9 || room.RoomTypeId == (int)ERoomType.Suite10 || room.RoomTypeId == (int)ERoomType.C3 || room.RoomTypeId == (int)ERoomType.C5)
                        {
                            massageAngel = massageAngel.Where(ma => ma.Room.ParentRoomId == room.Id).ToList();
                        }
                        else if (room.RoomTypeId == (int)ERoomType.SuiteSub)
                        {
                            massageAngel = massageAngel.Where(ma => ma.Room.Id == room.Id).ToList();
                        }
                        else
                        {
                            massageAngel = massageAngel.Where(ma => ma.Room.RoomNo.Contains(roomName)).ToList();
                        }
                    }
                    else
                    {
                        return Ok();
                    }



                }

                var massageAngelResponse = _mapper.Map<List<MassageAngelResponse>>(massageAngel);


                foreach (var item in massageAngelResponse)
                {
                    var massage = await _massageService.GetById(item.MassageId);
                    if (massage == null) 
                    {
                        return NotFound();
                    }
                    var reception = await _receptionService.GetByIdAsync(item.ReceptionId);
                    if (reception == null)
                    {
                        return NotFound();
                    }
                    var angel = await _angelService.GetByIdAsync(item.AngelId);
                    if (angel == null)
                    {
                        return NotFound();
                    }
                    var room = await _roomService.GetByIdAsync(item.RoomId);
                    if (room == null)
                    {
                        return NotFound();
                    }
                    item.Massage = _mapper.Map<MassageResponse>(massage);
                    item.Reception = _mapper.Map<ReceptionResponse>(reception);
                    item.Angel = _mapper.Map<AngelResponse>(angel);
                    item.Room = _mapper.Map<RoomResponse>(room);
                    if ((int)item.Room.RoomTypeId == (int)ERoomType.SuiteSub)
                    {
                        var parentRoom = await _roomService.GetByIdAsync((int)item.Room.ParentRoomId);
                        item.Room.ParentRoom = _mapper.Map<RoomParentResponse>(parentRoom);
                    }

                    var roomtype = await _roomTypeService.GetByIdAsync(item.Room.RoomTypeId);
                    if (roomtype == null)
                    {
                        return NotFound();
                    }
                    item.Room.RoomType = _mapper.Map<RoomTypeResponse>(roomtype);
                }

                return Ok(massageAngelResponse);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }
        [HttpGet(ApiRoutes.Massages.GetWagePaymentSuit)]
        public async Task<IActionResult> GetWagePaymentSuit(DateTime? calendarDay, int? receptionId, string roomName)
        {
            try
            {
                //var calendayDay = await _settingService.GetCarlendarDay();
                //if (calendayDay == null)
                //{
                //    return NotFound();
                //}
                int calendarDayId = _settingService.GetCalendarId((DateTime)calendarDay);

                var massageRoom = await _massageService.GetWagePaymentSuit(calendarDayId);
                if (massageRoom == null)
                {
                    return NotFound();
                }
                if (receptionId != null)
                {
                    massageRoom = massageRoom.Where(ma => ma.ReceptionId == receptionId).ToList();
                }
                if (roomName != null)
                {
                    var room = _db.Rooms.Where(r => r.RoomNo == roomName).SingleOrDefault();
                    if (room != null)
                    {
                        if (room.RoomTypeId == (int)ERoomType.Suite || room.RoomTypeId == (int)ERoomType.Suite3 || room.RoomTypeId == (int)ERoomType.Suite4 || room.RoomTypeId == (int)ERoomType.Suite5 || room.RoomTypeId == (int)ERoomType.Suite6 || room.RoomTypeId == (int)ERoomType.Suite7 || room.RoomTypeId == (int)ERoomType.Suite8 || room.RoomTypeId == (int)ERoomType.Suite9 || room.RoomTypeId == (int)ERoomType.Suite10 || room.RoomTypeId == (int)ERoomType.C3 || room.RoomTypeId == (int)ERoomType.C5)
                        {
                            massageRoom = massageRoom.Where(ma => ma.Room.Id == room.Id).ToList();
                        }
                        else if (room.RoomTypeId == (int)ERoomType.SuiteSub)
                        {
                            massageRoom = massageRoom.Where(ma => ma.Room.Id == room.ParentRoomId).ToList();

                        }
                        else
                        {
                            massageRoom = massageRoom.Where(ma => ma.Room.RoomNo.Contains(roomName)).ToList();
                        }
                    }
                    else
                    {
                        return Ok();
                    }
                    

                }

                var massageRoomResponse = _mapper.Map<List<MassageRoomResponse>>(massageRoom);

               

                foreach (var item in massageRoomResponse)
                {
                    var massage = await _massageService.GetById(item.MassageId);
                    if (massage == null)
                    {
                        return NotFound();
                    }
                    var reception = await _receptionService.GetByIdAsync(item.ReceptionId);
                    if (reception == null)
                    {
                        return NotFound();
                    }
                    var room = await _roomService.GetByIdAsync(item.RoomId);
                    if (room == null)
                    {
                        return NotFound();
                    }
                    item.massage = _mapper.Map<MassageResponse>(massage);
                    item.Reception = _mapper.Map<ReceptionResponse>(reception);
                    item.Room = _mapper.Map<RoomResponse>(room);
                }
                return Ok(massageRoomResponse);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }
        [HttpPost(ApiRoutes.Massages.CheckMinuteChangeRoom)]
        public async Task<IActionResult> CheckMinuteChangeRoom([FromBody] UpdateMassageAngelRequest request)
        {
            try
            {
                double totalMinute = 0;
                if (request.MassageRoomId != null)
                {
                    var massageRoom = await _massageService.GetMassageRoomById((int)request.MassageRoomId);
                    if (massageRoom == null)
                    {
                        return NotFound();
                    }
                    TimeSpan timespan = DateTime.Now - (DateTime)massageRoom.CheckInTime;

                    totalMinute = timespan.TotalMinutes;
                }
                else
                {
                    var massageAngel = await _massageService.GetMassageAngelById((int)request.MassageAngelId);
                    if (massageAngel == null)
                    {
                        return NotFound();
                    }
                    TimeSpan timespan = DateTime.Now - (DateTime)massageAngel.CheckInTime;

                    totalMinute = timespan.TotalMinutes;
                }

                return Ok(totalMinute);

            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }
       

        [HttpGet(ApiRoutes.Massages.GetSellBill)]
        public async Task<IActionResult> GetSellBill([FromQuery] ReceptionSearchQuery request)
        {
            try
            {
                var pagination = _mapper.Map<PaginationFilter>(request);
                int calendarDayId = _settingService.GetCalendarId(request.StartDate);

                var massages = await _massageService.GetSellMassageAsync(calendarDayId, request, pagination);

                var massageResponse = _mapper.Map<List<MassageResponse>>(massages);

                foreach (var massage in massageResponse)
                {
                    var massageRoom = await _massageService.GetMassageRoomByMassageId(massage.Id);
                    massage.massageRoom = _mapper.Map<MassageRoomResponse>(massageRoom);

                    var massageAngels = await _massageService.GetMassageAngelByMassageId(massage.Id);

                    massage.massageAngels = _mapper.Map<List<MassageAngelResponse>>(massageAngels);
                }

                return Ok(new PagedResponse<MassageResponse>
                {
                    Data = massageResponse,
                    PageNumber = pagination.PageNumber,
                    PageSize = pagination.PageSize,
                    Keyword = pagination.Keyword,
                    OrderBy = pagination.OrderBy,
                    TotalRecords = pagination.TotalRecords,
                    OrderDirection = pagination.OrderDirection,
                });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }

        [HttpPatch(ApiRoutes.Massages.ChangeReceptionBill)]
        public async Task<IActionResult> ChangeReceptionBill([FromBody] UpdateMassageAngelRequest request)
        {
            using (var transaction = await _db.Database.BeginTransactionAsync())
            {
                try
                {
                    var massageRoom = await _massageService.GetMassageRoomByMassageId((int)request.MassageId);
                    if (massageRoom != null)
                    {
                        var _massageRoom = await _massageService.GetMassageRoomById(massageRoom.Id);

                        _massageRoom.ReceptionId = (int)request.ReceptionId;
                        await _massageService.UpdateMassageRoom(_massageRoom);
                    }

                    List<MassageAngel> massageAngels = await _massageService.GetMassageAngelByMassageId((int)request.MassageId);
                    if (massageAngels.Count > 0)
                    {
                        foreach (var massageAngel in massageAngels)
                        {
                            MassageAngel _massageAngel = await _massageService.GetMassageAngelById(massageAngel.Id);

                            _massageAngel.ReceptionId = (int)request.ReceptionId;
                            await _massageService.UpdateMassageAngel(_massageAngel);
                        }
                    }

                    await transaction.CommitAsync();
                    return Ok();
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex.Message);
                    _logger.LogError(ex.StackTrace);
                    await transaction.RollbackAsync();
                    return BadRequest();
                }
            }
        }
        [HttpGet(ApiRoutes.Massages.GetWagePaymentSuitCancel)]
        public async Task<IActionResult> GetWagePaymentSuiteCancel([FromQuery] WagePaymentCancelSearchQuery paginationQuery)
        {
            try
            {
                var calendarDay = await _settingService.GetCarlendarDay();
                if (calendarDay == null)
                {
                    return NotFound();
                }
                List<int> calendarIds = new List<int>();
                if (paginationQuery.StartDate != null && paginationQuery.EndDate != null)
                {
                    calendarIds = await _settingService.GetCalendarId((DateTime)paginationQuery.StartDate, (DateTime)paginationQuery.EndDate);
                }

                var pagination = _mapper.Map<PaginationFilter>(paginationQuery);
                var massageRoom = await _massageService.GetWagePaymentSuitCancel(calendarIds, calendarDay.Id, paginationQuery, pagination);
                if (massageRoom == null)
                {
                    return NotFound();
                }
                var massageRoomResponse = _mapper.Map<List<MassageRoomResponse>>(massageRoom);

                foreach (var item in massageRoomResponse)
                {
                    var reception = await _receptionService.GetByIdAsync(item.ReceptionId);
                    if (reception == null)
                    {
                        return NotFound();
                    }
                    var room = await _roomService.GetByIdAsync(item.RoomId);
                    if (room == null)
                    {
                        return NotFound();
                    }
                    item.Reception = _mapper.Map<ReceptionResponse>(reception);
                    item.Room = _mapper.Map<RoomResponse>(room);
                }
                return Ok(massageRoomResponse);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }

        [HttpGet(ApiRoutes.Massages.GetWagePaymentCancel)]
        public async Task<IActionResult> GetWagePaymentCancel([FromQuery] WagePaymentCancelSearchQuery paginationQuery)
        {
            try
            {
                var calendarDay = await _settingService.GetCarlendarDay();
                if (calendarDay == null)
                {
                    return NotFound();
                }
                List<int> calendarIds = new List<int>();
                if (paginationQuery.StartDate != null && paginationQuery.EndDate != null)
                {
                    calendarIds = await _settingService.GetCalendarId((DateTime)paginationQuery.StartDate, (DateTime)paginationQuery.EndDate);
                }

                var pagination = _mapper.Map<PaginationFilter>(paginationQuery);
                var massageAngel = await _massageService.GetWagePaymentCancel(calendarIds, calendarDay.Id, paginationQuery, pagination);
                if (massageAngel == null)
                {
                    return NotFound();
                }

                var massageAngelResponse = _mapper.Map<List<MassageAngelResponse>>(massageAngel);

                foreach (var item in massageAngelResponse)
                {
                    var reception = await _receptionService.GetByIdAsync(item.ReceptionId);
                    if (reception == null)
                    {
                        return NotFound();
                    }
                    var angel = await _angelService.GetByIdAsync(item.AngelId);
                    if (angel == null)
                    {
                        return NotFound();
                    }
                    var room = await _roomService.GetByIdAsync(item.RoomId);
                    if (room == null)
                    {
                        return NotFound();
                    }
                    item.Reception = _mapper.Map<ReceptionResponse>(reception);
                    item.Angel = _mapper.Map<AngelResponse>(angel);
                    item.Room = _mapper.Map<RoomResponse>(room);

                    var roomtype = await _roomTypeService.GetByIdAsync(item.Room.RoomTypeId);
                    if (roomtype == null)
                    {
                        return NotFound();
                    }
                    item.Room.RoomType = _mapper.Map<RoomTypeResponse>(roomtype);
                }

                return Ok(massageAngelResponse);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }
        [HttpPatch(ApiRoutes.Massages.CancelCheckOutAngel)]
        public async Task<IActionResult> CancelCheckOutAngel([FromBody] UpdateMassageAngelRequest request)
        {
            using (var transaction = await _db.Database.BeginTransactionAsync())
            {
                try
                {
                    var massageAngel = await _massageService.GetMassageAngelById((int)request.MassageAngelId);

                    decimal totalDeduct = 0;
                    var deduct = await _deductTypeService.GetAllAsync(null);

                    for (int i = 0; i < deduct.Count; i++)
                    {
                        totalDeduct += deduct[i].Fee;
                    }


                    if (massageAngel != null)
                    {
                        massageAngel.CheckOutTime = null;
                        massageAngel.TimeMinute = 0;
                        massageAngel.RoundFact = 0;

                        await _massageService.UpdateMassageAngel(massageAngel);

                        //angel
                        var angel = await _angelService.GetByIdAsync((int)massageAngel.AngelId);
                        if (angel != null)
                        {
                            angel.Status = (int)EAngelStatus.Working;
                            await _angelService.UpdateAsync(angel);
                        }

                        // room
                        var room = await _roomService.GetByIdAsync((int)massageAngel.RoomId);
                        if (room != null)
                        {
                            room.Status = (int)ERoomStatus.Working;
                            await _roomService.UpdateAsync(room);
                        }

                        //decimal totalRound = massageAngel.Round;

                        //int? calenDarDayId = await GetCalendarDayId();

                        //var performance = await _angelPerformanceService.GetByCalendarIdAndAngelIdAsync((int)calenDarDayId, massageAngel.AngelId);

                        //var angelPerformance = await _angelPerformanceService.GetByIdAsync(performance.Id);

                        //angelPerformance.TotalRound -= totalRound;

                        //angelPerformance.TotalFee = massageAngel.Angel.AngelType.Fee * angelPerformance.TotalRound;

                        //angelPerformance.TotalWage = massageAngel.Angel.AngelType.Wage * angelPerformance.TotalRound;

                        //angelPerformance.TotalDeduct = angelPerformance.TotalRound * totalDeduct;

                        //await _angelPerformanceService.DeleteAsync(angelPerformance.Id);

                        //await _angelPerformanceService.UpdateAsync(angelPerformance);
                    }

                    await transaction.CommitAsync();
                    return Ok();
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex.Message);
                    _logger.LogError(ex.StackTrace);
                    return BadRequest();
                }
            }
        }
        [HttpPatch(ApiRoutes.Massages.CancelCheckOutRoom)]
        public async Task<IActionResult> CancelChekoutRoom([FromBody] UpdateMassageAngelRequest request)
        {
            using (var transaction = await _db.Database.BeginTransactionAsync())
            {
                try
                {
                    var massageRoom = await _massageService.GetMassageRoomById((int)request.MassageRoomId);
                    if (massageRoom != null)
                    {
                        massageRoom.CheckOutTime = null;
                        massageRoom.TimeMinute = 0;
                        massageRoom.Round = 0;
                        var room = await _roomService.GetByIdAsync(massageRoom.RoomId);
                        if (room == null)
                        {
                            return NotFound();
                        }
                        room.Status = (int)ERoomStatus.Working;
                        await _roomService.UpdateAsync(room);

                        await _massageService.UpdateMassageRoom(massageRoom);
                    }

                    await transaction.CommitAsync();
                    return Ok();
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex.Message);
                    _logger.LogError(ex.StackTrace);
                    return BadRequest();
                }
            }
        }
        [HttpGet(ApiRoutes.Massages.GetMassageRoomSuiteCheckIn)]
        public async Task<IActionResult> GetMassageRoomSuiteCheckInById(int id)
        {
            try
            {


                var massageRoom = await _massageService.GetMassageRoomById(id);

                if (massageRoom != null)
                {
                    var response = _mapper.Map<RoomSuiteCheckInResponse>(massageRoom);

                    var rooms = await _roomService.GetChildRoomParent(response.RoomId);

                    response.Rooms = _mapper.Map<List<RoomMassageAngelResponse>>(rooms);

                    var _room = await _roomService.GetByIdAsync(response.RoomId);

                    response.Room = _mapper.Map<RoomResponse>(_room);

                    foreach (var room in response.Rooms)
                    {
                        var massageAngel = await _massageService.GetmassageAngelByRoomIdAndMassageId(room.Id, massageRoom.MassageId);

                        room.MassageAngels = _mapper.Map<List<MassageAngelResponse>>(massageAngel);

                    }
                    return Ok(response);
                }
                return Ok();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }

        [HttpGet(ApiRoutes.Massages.GetAllTipAngels)]
        public async Task<IActionResult> GetAllTipAngels([FromQuery] StartDateEndDateRequestcs request)
        {
            try
            {
                int calendarId = 0;
                calendarId = _settingService.GetCalendarId(request.StartDate);
                var massageTipAngel = await _massageService.GetMassagelTipAngel(calendarId);
                var response = _mapper.Map<List<MassageResponse>>(massageTipAngel);

                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }

        [HttpGet(ApiRoutes.Massages.GetTipAngelById)]
        public async Task<IActionResult> GetTipAngelById([FromQuery] StartDateEndDateRequestcs request)
        {
            try
            {
                var massage = await _massageService.GetById((int)request.massageId);
                var response = _mapper.Map<MassageResponse>(massage);
                var user = await _userService.GetByIdAsync((int)massage.CashierId);
                response.user = _mapper.Map<UserResponse>(user);

                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }

        [HttpPatch(ApiRoutes.Massages.PaymentTip)]
        public async Task<IActionResult> PaymentTip([FromBody] StartDateEndDateRequestcs request)
        {
            using (var transaction = await _db.Database.BeginTransactionAsync())
            {
                try
                {
                    var massage = await _massageService.GetById((int)request.massageId);
                    if (massage == null)
                    {
                        return NotFound();
                    }
                    massage.IsPaymentTip = true;

                    await _massageService.UpdateAsync(massage);
                    await transaction.CommitAsync();

                    return Ok();
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex.Message);
                    _logger.LogError(ex.StackTrace);
                    return BadRequest();
                }
            }
        }

        [HttpPost(ApiRoutes.Massages.MergeRoomCheckIn)]
        public async Task<IActionResult> MergeRoomCheckIn([FromBody] MassageRequest request)
        {
            using (var transaction = await _db.Database.BeginTransactionAsync())
            {
                try
                {
                    int? calenDarDayId = await GetCalendarDayId();

                    string documentNumber = _settingService.GetDocumentNumberAndUpdate((int)calenDarDayId, false);

                    if (calenDarDayId == null)
                    {
                        return NotFound();
                    }
                    Massage massage = new Massage
                    {
                        CalendarDayId = (int)calenDarDayId,
                        IsPaid = false,
                        IsUnPaid = false,
                        Total = 0,
                        AngelTotal = 0,
                        FoodTotal = 0,
                        RoomTotal = 0,
                        IsCashAngel = false,
                        IsCreditAngel = false,
                        IsMemberAngel = false,
                        CashAngelTotal = 0,
                        CreditAngelTotal = 0,
                        MemberAngelTotal = 0,
                        IsCashFood = false,
                        IsCreditFood = false,
                        IsMemberFood = false,
                        CashFoodTotal = 0,
                        CreditFoodTotal = 0,
                        MemberFoodTotal = 0,
                        IsCashRoom = false,
                        IsCreditRoom = false,
                        IsMemberRoom = false,
                        CashRoomTotal = 0,
                        CreditRoomTotal = 0,
                        MemberRoomTotal = 0,
                        IsEntertainAngel = false,
                        EntertainTotalAngel = 0,
                        EntertainRemarkAngel = "",
                        IsEntertainFood = false,
                        EntertainTotalFood = 0,
                        EntertainRemarkFood = "",
                        PayDate = null,
                        DocumentNumber = documentNumber,
                        QrCodeTotal = 0,
                        IsQrCodeAngel = false,
                        QrCodeAngelTotal = 0,
                        IsQrCodeFood = false,
                        QrCodeFoodTotal = 0,
                        IsQrCodeRoom = false,
                        QrCodeRoomTotal = 0,
                        IsQrCodeEtc = false,
                        QrCodeEtcTotal = 0,
                        TipQrCodeTotal = 0,
                        CountriesId = request.CountriesId,
                        AgenciesId = request.AgenciesId,
                    };
                    await _massageService.CreateAsync(massage);

                    List<MassageAngel> listAngels = new List<MassageAngel>();
                    List<int> roomsIds = new List<int>();
                    List<int> angelIds = new List<int>();
                    DateTime dateNow;
                    bool checkTime = CheckTime(DateTime.Now);
                    if (checkTime)
                    {
                        dateNow = RoundUp(DateTime.Now, TimeSpan.FromMinutes(5));
                    }
                    else
                    {
                        dateNow = RoundDown(DateTime.Now, TimeSpan.FromMinutes(5));
                    }

                    if (request.MassageAngels.Count > 0)
                    {
                        foreach (var angel in request.MassageAngels)
                        {
                            MassageAngel massageAngel = new MassageAngel();
                            massageAngel.MassageId = massage.Id;
                            massageAngel.RoomId = angel.RoomId;
                            massageAngel.ReceptionId = angel.ReceptionId;
                            massageAngel.AngelId = angel.AngelId;
                            massageAngel.CheckInTime = dateNow;
                            massageAngel.CheckOutTime = null;
                            massageAngel.FirstCheckInTime = dateNow;
                            massageAngel.TimeMinute = 0;
                            massageAngel.Round = 1;
                            massageAngel.IsNotCall = angel.IsNotCall;
                            await _massageService.CheckInMassageAngel(massageAngel);
                            listAngels.Add(massageAngel);
                            angelIds.Add(massageAngel.AngelId);
                            var room = await _roomService.GetByIdAsync(massageAngel.RoomId);

                            if (room.RoomType.Id == (int)ERoomType.Vip || room.RoomType.Id == (int)ERoomType.Vip2
                               || room.RoomType.Id == (int)ERoomType.Vip3 || room.RoomType.Id == (int)ERoomType.C3
                               || room.RoomType.Id == (int)ERoomType.C5)
                            {
                                var checkRoom = await _massageService.GetMassageRoomByMassageIdAndRoomId(massageAngel.MassageId, massageAngel.RoomId);
                                if (checkRoom == null)
                                {
                                    MassageRoom massageRoom = new MassageRoom();
                                    massageRoom.MassageId = massageAngel.MassageId;
                                    massageRoom.RoomId = massageAngel.RoomId;
                                    massageRoom.ReceptionId = massageAngel.ReceptionId;
                                    massageRoom.CheckInTime = massageAngel.CheckInTime;
                                    massageRoom.CheckOutTime = null;
                                    massageRoom.OldCheckInTime = dateNow;
                                    massageRoom.TimeMinute = 0;
                                    massageRoom.LastCallTime = null;
                                    massageRoom.CallCount = 0;
                                    massageRoom.IsNotCall = massageAngel.IsNotCall;
                                    await _massageService.CheckInMassageRoomList(massageRoom);
                                    roomsIds.Add(massageRoom.RoomId);
                                }
                            }
                            else if (room.RoomType.Id == (int)ERoomType.Suite || room.RoomType.Id == (int)ERoomType.SuiteSub || room.RoomType.Id == (int)ERoomType.Suite3
                                || room.RoomType.Id == (int)ERoomType.Suite4 || room.RoomType.Id == (int)ERoomType.Suite5 || room.RoomType.Id == (int)ERoomType.Suite6
                                || room.RoomType.Id == (int)ERoomType.Suite7 || room.RoomType.Id == (int)ERoomType.Suite8 || room.RoomType.Id == (int)ERoomType.Suite9
                                || room.RoomType.Id == (int)ERoomType.Suite10)
                            {
                                var parentRoom = await _roomService.GetByIdAsync(room.Id);
                                var _checkRoom = await _massageService.GetMassageRoomByMassageIdAndRoomId(massageAngel.MassageId, (int)parentRoom.ParentRoomId);
                                if (_checkRoom == null)
                                {
                                    MassageRoom massageRoom = new MassageRoom();
                                    massageRoom.MassageId = massageAngel.MassageId;
                                    massageRoom.RoomId = (int)room.ParentRoomId;
                                    massageRoom.ReceptionId = massageAngel.ReceptionId;
                                    massageRoom.CheckInTime = massageAngel.CheckInTime;
                                    massageRoom.CheckOutTime = null;
                                    massageRoom.OldCheckInTime = dateNow;
                                    massageRoom.TimeMinute = 0;
                                    massageRoom.LastCallTime = null;
                                    massageRoom.CallCount = 0;
                                    massageRoom.IsNotCall = massageAngel.IsNotCall;
                                    await _massageService.CheckInMassageRoomList(massageRoom);
                                    roomsIds.Add(massageRoom.RoomId);
                                    var _room = await _roomService.GetByIdAsync((int)room.ParentRoomId);
                                    _room.Status = (int)ERoomStatus.Working;
                                    await _roomService.UpdateAsync(_room);
                                }

                            }
                        }

                        foreach (var angel in listAngels)
                        {
                            var updateAngel = await _angelService.GetByIdAsync(angel.AngelId);
                            if (updateAngel == null)
                            {
                                return NotFound();
                            }
                            updateAngel.Status = (int)EAngelStatus.Working;
                            await _angelService.UpdateAsync(updateAngel);

                            var updateRoom = await _roomService.GetByIdAsync(angel.RoomId);
                            if (updateRoom == null)
                            {
                                return NotFound();
                            }
                            updateRoom.Status = (int)ERoomStatus.Working;
                            await _roomService.UpdateAsync(updateRoom);
                        }
                    }
                    else
                    {
                        foreach (var roomRequest in request.Rooms)
                        {
                            var room = await _roomService.GetByIdAsync(roomRequest.Id);

                            if (room.RoomType.Id != (int)ERoomType.Normal || room.RoomType.Id != (int)ERoomType.RoundSell)
                            {
                                var checkRoom = await _massageService.GetMassageRoomByMassageIdAndRoomId(massage.Id, room.Id);
                                if (checkRoom == null)
                                {
                                    MassageRoom massageRoom = new MassageRoom();
                                    massageRoom.MassageId = massage.Id;
                                    massageRoom.RoomId = room.Id;
                                    massageRoom.ReceptionId = (int)request.ReceptionId;
                                    massageRoom.CheckInTime = dateNow;
                                    massageRoom.CheckOutTime = null;
                                    massageRoom.OldCheckInTime = dateNow;
                                    massageRoom.TimeMinute = 0;
                                    massageRoom.LastCallTime = null;
                                    massageRoom.CallCount = 0;
                                    massageRoom.IsNotCall = request.IsNotCall;
                                    await _massageService.CheckInMassageRoomList(massageRoom);
                                    roomsIds.Add(massageRoom.RoomId);
                                    var _room = await _roomService.GetByIdAsync(room.Id);
                                    _room.Status = (int)ERoomStatus.Working;
                                    await _roomService.UpdateAsync(_room);
                                }
                            }
                        }
                        
                    }

                    var agencies = await _agencyService.GetByIdAsync(request.AgenciesId);
                    if (agencies == null)
                    {
                        return NotFound();
                    }
                    var agenciesPerformance = await _agencyService.GetAgenciesPerformanceByCalendarDay((int)calenDarDayId, agencies.Id);

                    bool ispayAgency = false;
                    int i = 0;
                    int countAgency = agenciesPerformance.Count;
                    foreach (var item in agenciesPerformance)
                    {
                        i++;
                        if (item.IsPay)
                        {
                            if (i == countAgency)
                            {
                                ispayAgency = true;
                            }
                        }
                    }
                    if (request.AgenciesId != 1)
                    {
                        if (countAgency == 0 || ispayAgency)
                        {
                            AgencyPerformance agencyPerformance = new AgencyPerformance
                            {
                                CarlendarDayId = (int)calenDarDayId,
                                AgenciesId = request.AgenciesId,
                                IsPay = false,
                                IsUnPaid = false,
                                TotalMassage = 0,
                                TotalRound = 0,
                                TotalMember = 0,
                                TotalReceive = 0,
                                Remark = ""
                            };
                            await _agencyPerformanceService.CreatePaymentAgencyAsync(agencyPerformance);

                            var updateMassage = await _massageService.GetById(massage.Id);
                            updateMassage.AgencyPerformanceId = agencyPerformance.Id;
                            await _massageService.UpdateAsync(updateMassage);
                        }
                        else
                        {
                            var oldAgencyPerformance = await _agencyPerformanceService.GetAgencyPerformancesByCalendarIdAndAgenciesId((int)calenDarDayId, request.AgenciesId);

                            var updateMassage = await _massageService.GetById(massage.Id);
                            updateMassage.AgencyPerformanceId = oldAgencyPerformance.Id;
                            await _massageService.UpdateAsync(updateMassage);
                        }
                    }

                    _settingService.GetDocumentNumberAndUpdate((int)calenDarDayId, true);

                    List<Angel> listAngel = new List<Angel>();
                    foreach (var item in angelIds)
                    {
                        Angel angel = new Angel();
                        angel = await _angelService.GetByIdAsync(item);
                        listAngel.Add(angel);
                    }
                    var angelResponse = _mapper.Map<List<AngelResponse>>(listAngel);

                    var listRooms = await _roomService.GetListRoomByRoomId(roomsIds);
                    var roomResponse = _mapper.Map<List<RoomResponse>>(listRooms);


                    await transaction.CommitAsync();
                    var massageResponse = _mapper.Map<MassageResponse>(massage);
                    var massageAngels = await _massageService.GetMassageAngelByMassageId(massage.Id);
                    var massageRooms = await _massageService.GetListMassageRoomByMassageId(massage.Id);
                    massageResponse.massageAngels = _mapper.Map<List<MassageAngelResponse>>(massageAngels);
                    massageResponse.massageRooms = _mapper.Map<List<MassageRoomResponse>>(massageRooms);

                    await _checkInHub.Clients.All.SendAsync("AddListCheckIn", massageResponse);
                    await _checkInHub.Clients.All.SendAsync("RoomStatusChange", roomResponse);
                    await _checkInHub.Clients.All.SendAsync("AngelStatusChange", angelResponse);
                    return Ok(massageResponse);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex.Message);
                    _logger.LogError(ex.StackTrace);
                    await transaction.RollbackAsync();
                    return BadRequest();
                }
            }

        }
        [HttpPost(ApiRoutes.Massages.MergeCheckInLater)]
        public async Task<IActionResult> MergeCheckInLater([FromBody] MassageCheckInLaterRequest request) 
        {
            using (var transaction = await _db.Database.BeginTransactionAsync()) 
            {
                try
                {
                    int? calenDarDayId = await GetCalendarDayId();
                    string documentNumber = _settingService.GetDocumentNumberAndUpdate((int)calenDarDayId, false);
                    //if (calenDarDayId == null)
                    //{
                    //    return NotFound();
                    //}
                    Massage massage = new Massage
                    {
                        CalendarDayId = (int)calenDarDayId,
                        IsPaid = false,
                        IsUnPaid = false,
                        Total = 0,
                        AngelTotal = 0,
                        FoodTotal = 0,
                        RoomTotal = 0,
                        IsCashAngel = false,
                        IsCreditAngel = false,
                        IsMemberAngel = false,
                        CashAngelTotal = 0,
                        CreditAngelTotal = 0,
                        MemberAngelTotal = 0,
                        IsCashFood = false,
                        IsCreditFood = false,
                        IsMemberFood = false,
                        CashFoodTotal = 0,
                        CreditFoodTotal = 0,
                        MemberFoodTotal = 0,
                        IsCashRoom = false,
                        IsCreditRoom = false,
                        IsMemberRoom = false,
                        CashRoomTotal = 0,
                        CreditRoomTotal = 0,
                        MemberRoomTotal = 0,
                        IsEntertainAngel = false,
                        EntertainTotalAngel = 0,
                        EntertainRemarkAngel = "",
                        IsEntertainFood = false,
                        EntertainTotalFood = 0,
                        EntertainRemarkFood = "",
                        PayDate = null,
                        DocumentNumber = documentNumber,
                        QrCodeTotal = 0,
                        IsQrCodeAngel = false,
                        QrCodeAngelTotal = 0,
                        IsQrCodeFood = false,
                        QrCodeFoodTotal = 0,
                        IsQrCodeRoom = false,
                        QrCodeRoomTotal = 0,
                        IsQrCodeEtc = false,
                        QrCodeEtcTotal = 0,
                        TipQrCodeTotal = 0,
                        CountriesId = request.CountriesId,
                        AgenciesId = request.AgenciesId,
                        IsCheckInLater = true,
                    };
                    await _massageService.CreateAsync(massage);

                    List<MassageAngel> listAngels = new List<MassageAngel>();
                    List<int> roomsIds = new List<int>();
                    List<int> angelIds = new List<int>();
                    DateTime dateNow;
                    bool checkTime = CheckTime(DateTime.Now);
                    if (checkTime)
                    {
                        dateNow = RoundUp(DateTime.Now, TimeSpan.FromMinutes(5));
                    }
                    else
                    {
                        dateNow = RoundDown(DateTime.Now, TimeSpan.FromMinutes(5));
                    }

                    if (request.MassageAngels.Count > 0)
                    {
                        foreach (var angel in request.MassageAngels)
                        {
                            MassageAngel massageAngel = new MassageAngel(); 
                            massageAngel.MassageId = massage.Id;
                            massageAngel.RoomId = angel.RoomId;
                            massageAngel.ReceptionId = angel.ReceptionId;
                            massageAngel.AngelId = angel.AngelId;
                            massageAngel.CheckInTime = angel.CheckInTime;
                            massageAngel.CheckOutTime = angel.CheckOutTime;
                            massageAngel.FirstCheckInTime = angel.CheckInTime;
                            massageAngel.TimeMinute = 0;
                            massageAngel.Round = angel.Round;
                            massageAngel.IsNotCall = angel.IsNotCall;
                            await _massageService.CheckInMassageAngel(massageAngel);
                            listAngels.Add(massageAngel);
                            angelIds.Add(massageAngel.AngelId);
                            var room = await _roomService.GetByIdAsync(massageAngel.RoomId);

                            if (room.RoomType.Id == (int)ERoomType.Vip || room.RoomType.Id == (int)ERoomType.Vip2
                               || room.RoomType.Id == (int)ERoomType.Vip3 || room.RoomType.Id == (int)ERoomType.C3
                               || room.RoomType.Id == (int)ERoomType.C5)
                            {
                                var checkRoom = await _massageService.GetMassageRoomByMassageIdAndRoomId(massageAngel.MassageId, massageAngel.RoomId);
                                if (checkRoom == null)
                                {
                                    MassageRoom massageRoom = new MassageRoom();
                                    massageRoom.MassageId = massageAngel.MassageId;
                                    massageRoom.RoomId = massageAngel.RoomId;
                                    massageRoom.ReceptionId = massageAngel.ReceptionId;
                                    massageRoom.CheckInTime = angel.checkInRoomTime;
                                    massageRoom.CheckOutTime = angel.checkOutRoomTime;
                                    massageRoom.OldCheckInTime = angel.checkInRoomTime;
                                    massageRoom.Round = angel.roomRound;
                                    massageRoom.TimeMinute = 0;
                                    massageRoom.LastCallTime = null;
                                    massageRoom.CallCount = 0;
                                    massageRoom.IsNotCall = massageAngel.IsNotCall;
                                    await _massageService.CheckInMassageRoomList(massageRoom);
                                    roomsIds.Add(massageRoom.RoomId);
                                }
                            }
                            else if (room.RoomType.Id == (int)ERoomType.Suite || room.RoomType.Id == (int)ERoomType.SuiteSub || room.RoomType.Id == (int)ERoomType.Suite3
                                || room.RoomType.Id == (int)ERoomType.Suite4 || room.RoomType.Id == (int)ERoomType.Suite5 || room.RoomType.Id == (int)ERoomType.Suite6
                                || room.RoomType.Id == (int)ERoomType.Suite7 || room.RoomType.Id == (int)ERoomType.Suite8 || room.RoomType.Id == (int)ERoomType.Suite9
                                || room.RoomType.Id == (int)ERoomType.Suite10)
                            {
                                var parentRoom = await _roomService.GetByIdAsync(room.Id);
                                var _checkRoom = await _massageService.GetMassageRoomByMassageIdAndRoomId(massageAngel.MassageId, (int)parentRoom.ParentRoomId);
                                if (_checkRoom == null)
                                {
                                    MassageRoom massageRoom = new MassageRoom();
                                    massageRoom.MassageId = massageAngel.MassageId;
                                    massageRoom.RoomId = (int)room.ParentRoomId;
                                    massageRoom.ReceptionId = massageAngel.ReceptionId;
                                    massageRoom.CheckInTime = angel.checkInRoomTime;
                                    massageRoom.CheckOutTime = angel.checkOutRoomTime;
                                    massageRoom.OldCheckInTime = angel.checkInRoomTime;
                                    massageRoom.Round = angel.roomRound;
                                    massageRoom.TimeMinute = 0;
                                    massageRoom.LastCallTime = null;
                                    massageRoom.CallCount = 0;
                                    massageRoom.IsNotCall = massageAngel.IsNotCall;
                                    await _massageService.CheckInMassageRoomList(massageRoom);
                                    roomsIds.Add(massageRoom.RoomId);
                                    var _room = await _roomService.GetByIdAsync((int)room.ParentRoomId);
                                    //_room.Status = (int)ERoomStatus.Working;
                                    //await _roomService.UpdateAsync(_room);
                                }

                            }

                            var _massageAngel = await _massageService.GetMassageAngelById(massageAngel.Id);
                            if (_massageAngel == null)
                            {
                                return NotFound();
                            }
                            TimeSpan timespan = (DateTime)_massageAngel.CheckOutTime - (DateTime)_massageAngel.CheckInTime;
                            double totalMinute = timespan.TotalMinutes;
                            _massageAngel.TimeMinute = (int)totalMinute;
                            var angelPerformance = await _angelPerformanceService.CreateOrUpdateByIdAsync(_massageAngel, (int)calenDarDayId);
                            if (angelPerformance == null)
                            {
                                return NotFound();
                            }
                            else //Aunz
                            {
                                MassageAngel massageAngel_ = await _massageService.GetMassageAngelById(_massageAngel.Id);
                                massageAngel_.AngelPerformanceId = angelPerformance.Id;

                                await _massageService.UpdateMassageAngel(massageAngel_);
                            }
                            bool isDeductType = _deductTypeService.CheckAngelPerformanceDeduct(_massageAngel.Id);
                            if (!isDeductType)
                            {
                                _logger.LogDebug("True CheckInLater");
                                var deductType = await _deductTypeService.GetAllAsync(null, null);

                                if (deductType.Count > 0)
                                {
                                    List<AngelPerformanceDeduct> angelPerformanceDeducts = new List<AngelPerformanceDeduct>();
                                    foreach (var item in deductType)
                                    {
                                        AngelPerformanceDeduct angelPerformanceDeduct = new AngelPerformanceDeduct();
                                        angelPerformanceDeduct.MassageAngelId = _massageAngel.Id;
                                        angelPerformanceDeduct.DeductTypeId = item.Id;
                                        angelPerformanceDeduct.AngelPerformanceId = angelPerformance.Id;
                                        angelPerformanceDeduct.Fee = item.Fee;
                                        angelPerformanceDeducts.Add(angelPerformanceDeduct);

                                    };
                                    await _massageService.CreateAngelPerformanceDeduct(angelPerformanceDeducts);
                                }
                            }
                            else
                            {
                                _logger.LogDebug("False CheckInLater");
                            }
                        }

                        //foreach (var angel in listAngels)
                        //{
                        //    var updateAngel = await _angelService.GetByIdAsync(angel.AngelId);
                        //    if (updateAngel == null)
                        //    {
                        //        return NotFound();
                        //    }
                        //    updateAngel.Status = (int)EAngelStatus.Working;
                        //    await _angelService.UpdateAsync(updateAngel);

                        //    var updateRoom = await _roomService.GetByIdAsync(angel.RoomId);
                        //    if (updateRoom == null)
                        //    {
                        //        return NotFound();
                        //    }
                        //    updateRoom.Status = (int)ERoomStatus.Working;
                        //    await _roomService.UpdateAsync(updateRoom);
                        //}
                    }
                    else
                    {
                        foreach (var roomRequest in request.Rooms)
                        {
                            var room = await _roomService.GetByIdAsync(roomRequest.Id);

                            if (room.RoomType.Id != (int)ERoomType.Normal || room.RoomType.Id != (int)ERoomType.RoundSell)
                            {
                                var checkRoom = await _massageService.GetMassageRoomByMassageIdAndRoomId(massage.Id, room.Id);
                                if (checkRoom == null)
                                {
                                    MassageRoom massageRoom = new MassageRoom();
                                    massageRoom.MassageId = massage.Id;
                                    massageRoom.RoomId = room.Id;
                                    massageRoom.ReceptionId = (int)request.ReceptionId;
                                    massageRoom.CheckInTime = roomRequest.checkInRoomTime;
                                    massageRoom.CheckOutTime = roomRequest.checkOutRoomTime;
                                    massageRoom.OldCheckInTime = roomRequest.checkInRoomTime;
                                    massageRoom.Round = roomRequest.roomRound;
                                    massageRoom.TimeMinute = 0;
                                    massageRoom.LastCallTime = null;
                                    massageRoom.CallCount = 0;
                                    massageRoom.IsNotCall = request.IsNotCall;
                                    await _massageService.CheckInMassageRoomList(massageRoom);
                                    roomsIds.Add(massageRoom.RoomId);
                                    var _room = await _roomService.GetByIdAsync(room.Id);
                                    //_room.Status = (int)ERoomStatus.Working;
                                    //await _roomService.UpdateAsync(_room);
                                }
                            }
                        }

                    }
                    


                    var agencies = await _agencyService.GetByIdAsync(request.AgenciesId);
                    if (agencies == null)
                    {
                        return NotFound();
                    }
                    var agenciesPerformance = await _agencyService.GetAgenciesPerformanceByCalendarDay((int)calenDarDayId, agencies.Id);

                    bool ispayAgency = false;
                    int i = 0;
                    int countAgency = agenciesPerformance.Count;
                    foreach (var item in agenciesPerformance)
                    {
                        i++;
                        if (item.IsPay)
                        {
                            if (i == countAgency)
                            {
                                ispayAgency = true;
                            }
                        }
                    }
                    if (request.AgenciesId != 1)
                    {
                        if (countAgency == 0 || ispayAgency)
                        {
                            AgencyPerformance agencyPerformance = new AgencyPerformance
                            {
                                CarlendarDayId = (int)calenDarDayId,
                                AgenciesId = request.AgenciesId,
                                IsPay = false,
                                IsUnPaid = false,
                                TotalMassage = 0,
                                TotalRound = 0,
                                TotalMember = 0,
                                TotalReceive = 0,
                                Remark = ""
                            };
                            await _agencyPerformanceService.CreatePaymentAgencyAsync(agencyPerformance);

                            var updateMassage = await _massageService.GetById(massage.Id);
                            updateMassage.AgencyPerformanceId = agencyPerformance.Id;
                            await _massageService.UpdateAsync(updateMassage);
                        }
                        else
                        {
                            var oldAgencyPerformance = await _agencyPerformanceService.GetAgencyPerformancesByCalendarIdAndAgenciesId((int)calenDarDayId, request.AgenciesId);

                            var updateMassage = await _massageService.GetById(massage.Id);
                            updateMassage.AgencyPerformanceId = oldAgencyPerformance.Id;
                            await _massageService.UpdateAsync(updateMassage);
                        }
                    }

                    _settingService.GetDocumentNumberAndUpdate((int)calenDarDayId, true);

                    List<Angel> listAngel = new List<Angel>();
                    foreach (var item in angelIds)
                    {
                        Angel angel = new Angel();
                        angel = await _angelService.GetByIdAsync(item);
                        listAngel.Add(angel);
                    }
                    var angelResponse = _mapper.Map<List<AngelResponse>>(listAngel);

                    var listRooms = await _roomService.GetListRoomByRoomId(roomsIds);
                    var roomResponse = _mapper.Map<List<RoomResponse>>(listRooms);

                    await transaction.CommitAsync();
                    var massageResponse = _mapper.Map<MassageResponse>(massage);
                    var massageAngels = await _massageService.GetMassageAngelByMassageId(massage.Id);
                    var massageRooms = await _massageService.GetListMassageRoomByMassageId(massage.Id);
                    massageResponse.massageAngels = _mapper.Map<List<MassageAngelResponse>>(massageAngels);
                    massageResponse.massageRooms = _mapper.Map<List<MassageRoomResponse>>(massageRooms);

                    await _checkInHub.Clients.All.SendAsync("AddListCheckIn", massageResponse);
                    await _checkInHub.Clients.All.SendAsync("RoomStatusChange", roomResponse);
                    await _checkInHub.Clients.All.SendAsync("AngelStatusChange", angelResponse);
                    return Ok(massageResponse);
                }
                catch (Exception ex) 
                {
                    _logger.LogError(ex.Message);
                    _logger.LogError(ex.StackTrace);
                    await transaction.RollbackAsync();
                    return BadRequest();
                }
            }
        }
        [HttpPut(ApiRoutes.Massages.CancelAngelAndRoomCheckInLater)]
        public async Task<IActionResult> CancelAngelAndRoomCheckInLater([FromRoute] int id) 
        {
            using (var transaction = await _db.Database.BeginTransactionAsync()) 
            {
                try
                {
                    if (id == 0) 
                    {
                        return BadRequest();
                    }
                    var massage = await _massageService.GetById(id);
                    var massageAngels = await _massageService.GetMassageAngelByMassageId(id);
                    var massageRooms = await _massageService.GetListMassageRoomByMassageId(id);
                    foreach (var massageAngel in massageAngels) 
                    {
                        //var angelPerformances = await _angelPerformanceService.GetByIdAsync((int)massageAngel.AngelPerformanceId);
                        //if (angelPerformances != null) 
                        //{
                        //    await _angelPerformanceService.DeleteAngelPerformance(angelPerformances.Id);
                        //}
                        await _massageService.DeleteMassageAngel(massageAngel.Id);
                    }
                    foreach (var massageRoom in massageRooms) 
                    {
                        await _massageService.DeleteMassageRoom(massageRoom.Id);
                    }
                    await _massageService.CancelCheckInLater(massage.Id);

                    await transaction.CommitAsync();
                    return Ok();
                }
                catch (Exception ex) 
                {
                    _logger.LogError(ex.Message);
                    _logger.LogError(ex.StackTrace);
                    await transaction.RollbackAsync();
                    return BadRequest();
                }
            }
        }

        [HttpGet(ApiRoutes.Massages.GetMergeRoomCheckIn)]
        public async Task<IActionResult> GetMergeRoomCheckIn(int id)
        {
            try
            {
                var massage = await _massageService.GetById(id);
                if (massage == null)
                {
                    return NotFound();
                }
                var massageResponse = _mapper.Map<MassageResponse>(massage);
                if (massageResponse == null)
                {
                    return NotFound();
                }
                var massageRoom = await _massageService.GetListMassageRoomByMassageId(massage.Id);
                massageResponse.massageRooms = _mapper.Map<List<MassageRoomResponse>>(massageRoom);
                foreach (var _massageRoom in massageResponse.massageRooms)
                {
                    var memberAndRoom = await _massageService.GetMassageMemberAndRoomByMassageRoomId(_massageRoom.Id);
                    _massageRoom.MassageMemberAndRoom = _mapper.Map<MassageMemberAndRoomResponse>(memberAndRoom);
                }
                var massageAngels = await _massageService.GetMassageAngelByMassageId(massage.Id);
                massageResponse.massageAngels = _mapper.Map<List<MassageAngelResponse>>(massageAngels);
                var massageCreditPayment = await _massageService.GetMassageCreditPaymentByMassageId(massage.Id);
                massageResponse.massageCreditPayment = _mapper.Map<MassageCreditPaymentResponse>(massageCreditPayment);
                var massageMemberPayments = await _massageService.GetMassageMemberPaymentsByMassageId(massage.Id);
                massageResponse.massageMemberPayments = _mapper.Map<List<MassageMemberPaymentResponse>>(massageMemberPayments);
                var massageQrCodePayment = await _massageService.GetMassageQrCodePaymentByMassageId(massage.Id);
                massageResponse.massageQrCodePayment = _mapper.Map<MassageQrCodePaymentResponse>(massageQrCodePayment);

                if (massage.UpdatedBy != null)
                {
                    var user = await _userService.GetByIdAsync((int)massage.UpdatedBy);
                    massageResponse.user = _mapper.Map<UserResponse>(user);
                }
                foreach (var memberResponse in massageResponse.massageMemberPayments)
                {
                    var massageMember = await _memberService.GetByIdAsync(memberResponse.MemberId);
                    memberResponse.member = _mapper.Map<MemberResponse>(massageMember);
                }
                return Ok(massageResponse);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }

        [HttpPut(ApiRoutes.Massages.MergePayment)]
        public async Task<IActionResult> MergePayment([FromRoute] int id, [FromBody] ListPaymentRequest request)
        {
            using (var transaction = await _db.Database.BeginTransactionAsync())
            {
                try
                {
                    if (id <= 0)
                    {
                        return BadRequest();
                    }
                    var massage = await _massageService.GetById(id);
                    var oldAgencies = massage.AgenciesId;
                    if (massage == null)
                    {
                        return NotFound();
                    }
                    bool oldUnpaid = massage.IsUnPaid;

                    massage.IsPaid = request.IsPaid;
                    massage.IsUnPaid = request.IsUnPaid;
                    massage.Total = request.Total;
                    massage.AngelTotal = request.AngelTotal;
                    massage.FoodTotal = request.FoodTotal;
                    massage.RoomTotal = request.RoomTotal;
                    massage.IsCashAngel = request.IsCashAngel;
                    massage.IsCreditAngel = request.IsCreditAngel;
                    massage.IsMemberAngel = request.IsMemberAngel;
                    massage.IsQrCodeAngel = request.IsQrCodeAngel;
                    massage.CashAngelTotal = request.CashAngelTotal;
                    massage.CreditAngelTotal = request.CreditAngelTotal;
                    massage.MemberAngelTotal = request.MemberAngelTotal;
                    massage.QrCodeAngelTotal = request.QrCodeAngelTotal;
                    massage.IsCashFood = request.IsCashFood;
                    massage.IsCreditFood = request.IsCreditFood;
                    massage.IsMemberFood = request.IsMemberFood;
                    massage.IsQrCodeFood = request.IsQrCodeFood;
                    massage.CashFoodTotal = request.CashFoodTotal;
                    massage.CreditFoodTotal = request.CreditFoodTotal;
                    massage.MemberFoodTotal = request.MemberFoodTotal;
                    massage.QrCodeFoodTotal = request.QrCodeFoodTotal;
                    massage.IsCashRoom = request.IsCashRoom;
                    massage.IsCreditRoom = request.IsCreditRoom;
                    massage.IsMemberRoom = request.IsMemberRoom;
                    massage.IsQrCodeRoom = request.IsQrCodeRoom;
                    massage.CashRoomTotal = request.CashRoomTotal;
                    massage.CreditRoomTotal = request.CreditRoomTotal;
                    massage.MemberRoomTotal = request.MemberRoomTotal;
                    massage.QrCodeRoomTotal = request.QrCodeRoomTotal;
                    massage.IsEntertainAngel = request.IsEntertainAngel;
                    massage.EntertainTotalAngel = request.EntertainTotalAngel;
                    massage.EntertainRemarkAngel = request.EntertainRemarkAngel;
                    massage.IsEntertainFood = request.IsEntertainFood;
                    massage.EntertainTotalFood = request.EntertainTotalFood;
                    massage.EntertainRemarkFood = request.EntertainRemarkFood;
                    massage.EtcTotal = request.EtcTotal;
                    massage.IsCashEtc = request.IsCashEtc;
                    massage.IsCreditEtc = request.IsCreditEtc;
                    massage.IsMemberEtc = request.IsMemberEtc;
                    massage.IsEntertainEtc = request.IsEntertainEtc;
                    massage.IsQrCodeEtc = request.IsQrCodeEtc;
                    massage.EntertainEtcRemark = request.EntertainEtcRemark;
                    massage.CashEtcTotal = request.CashEtcTotal;
                    massage.CreditEtcTotal = request.CreditEtcTotal;
                    massage.MemberEtcTotal = request.MemberEtcTotal;
                    massage.EntertainEtcTotal = request.EntertainEtcTotal;
                    massage.QrCodeEtcTotal = request.QrCodeEtcTotal;
                    massage.TipTotal = request.TipTotal;
                    massage.TipCommTotal = request.TipCommTotal;
                    massage.DamagesTotal = request.DamagesTotal;
                    massage.OtherServiceChargesTotal = request.OtherServiceChargesTotal;
                    massage.EtcRemark = request.EtcRemark;
                    massage.UnPaidRemark = request.UnPaidRemark;

                    massage.TipQrCodeTotal = request.TipQrCodeTotal;
                    massage.CountriesId = (int)request.CountriesId;
                    massage.AgenciesId = (int)request.AgenciesId;
                    massage.AgencyPerformanceId = (int)request.AgencyPerformanceId;

                    if (massage.CancelDate == null)
                    {
                        massage.PayDate = DateTime.Now;
                    }
                    else
                    {
                        massage.CancelDate = null;
                    }

                    massage.CashierId = request.CashierId;
                    if (oldUnpaid && !request.IsUnPaid)
                    {
                        massage.PayUnPaidDate = DateTime.Now;
                    }
                    if (request.IsUnPaid) 
                    {

                        MassageDebt massageDebt = new MassageDebt();
                        massageDebt.MassageId = massage.Id;
                        massageDebt.DebtTotal = request.DebtTotal;
                        massageDebt.CashTotal = 0;
                        massageDebt.CreditTotal = 0;
                        massageDebt.QrTotal = 0;
                        massageDebt.MemberTotal = 0;
                        massageDebt.EntertainTotal = 0;
                        await _massageDebtService.CreateAsync(massageDebt);
                    }
                    await _massageService.UpdateAsync(massage);
                    _logger.LogDebug("DocumentNumber ===> "+ massage.DocumentNumber);
                    _logger.LogDebug("IsPaid ===> " + massage.IsPaid);


                    if (massage.IsCreditAngel || massage.IsCreditFood || massage.IsCreditRoom || massage.IsCreditEtc)
                    {
                        var _massageCreditPayment = await _massageService.GetMassageCreditPaymentByMassageId(massage.Id);
                        if (_massageCreditPayment != null)
                        {
                            var creditPayment = await _massageService.GetMassageCreditPaymentById(_massageCreditPayment.Id);
                            creditPayment.MassageId = massage.Id;
                            creditPayment.CardNo = request.PaymentCredit.CardNo;
                            creditPayment.CardType = request.PaymentCredit.CardType;
                            creditPayment.CreditTotal = request.PaymentCredit.CreditTotal;
                            await _massageService.UpdateMassagePaymentCredit(creditPayment);
                        }
                        else
                        {
                            MassageCreditPayment _credit = new MassageCreditPayment();
                            _credit.MassageId = massage.Id;
                            _credit.CardNo = request.PaymentCredit.CardNo;
                            _credit.CardType = request.PaymentCredit.CardType;
                            _credit.CreditTotal = request.PaymentCredit.CreditTotal;
                            await _massageService.CreateMassagePaymentCredit(_credit);
                        }
                    }

                    if (massage.IsMemberAngel || massage.IsMemberFood || massage.IsMemberRoom || massage.IsMemberEtc)
                    {
                        foreach (var member in request.PaymentMembers)
                        {
                            MassageMemberPayment _member = new MassageMemberPayment();
                            _member.MassageId = massage.Id;
                            _member.MemberId = (int)member.MemberId;
                            _member.MemberTotal = member.MemberTotal;
                            _member.UseSuite = member.UseSuite;
                            _member.UseVip = member.UseVip;
                            _member.MemberFoodTotal = member.MemberFoodTotal;
                            _member.MemberRoomTotal = member.MemberRoomTotal;
                            _member.MemberMassageTotal = member.MemberMassageTotal;
                            await _massageService.CreateMassagePaymentMember(_member);

                            //update credit member
                            var getMember = await _memberService.GetByIdAsync(_member.MemberId);
                            if (getMember == null)
                            {
                                return NotFound();
                            }
                            getMember.CreditAmount -= _member.MemberTotal;
                            await _memberService.UpdateAsync(getMember);

                            if (request.MassageRooms.Count > 0)
                            {
                                foreach(var room in request.MassageRooms)
                                {
                                    if (room.MemberId != null)
                                    {
                                        if ((int)room.MemberId == _member.MemberId)
                                        {
                                            MassageMemberAndRoom memberAndRoom = new MassageMemberAndRoom();
                                            memberAndRoom.MemberId = _member.MemberId;
                                            memberAndRoom.MassageMemberPaymentId = _member.Id;
                                            memberAndRoom.MassageRoomId = room.Id;
                                            memberAndRoom.Round = room.Round;
                                            memberAndRoom.RoomWage = room.Wage;
                                            await _massageService.CreateMassageMemberAndRoom(memberAndRoom);
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if (massage.IsQrCodeAngel || massage.IsQrCodeFood || massage.IsQrCodeRoom || massage.IsQrCodeEtc)
                    {
                        var _massageQrCodePayment = await _massageService.GetMassageQrCodePaymentByMassageId(massage.Id);
                        if (_massageQrCodePayment != null)
                        {
                            var qrCodePayment = await _massageService.GetMassageQrCodePaymentById(_massageQrCodePayment.Id);
                            qrCodePayment.MassageId = massage.Id;
                            qrCodePayment.QrCodeTotal = request.PaymentQrCode.QrCodeTotal;

                            await _massageService.UpdateMassagePaymentQrCode(qrCodePayment);
                        }
                        else
                        {
                            MassageQrCodePayment _qrcode = new MassageQrCodePayment();
                            _qrcode.MassageId = massage.Id;
                            _qrcode.QrCodeTotal = request.PaymentQrCode.QrCodeTotal;

                            await _massageService.CreateMassageQrCode(_qrcode);
                        }
                    }

                    int? paymentTypeId = null;

                    if (request.IsCashAngel && request.IsCreditAngel && request.IsMemberAngel && request.IsQrCodeAngel)
                    {
                        paymentTypeId = (int)EPaymentType.CashAndCreditAndMemberAndQrCode;
                    }
                    else if (request.IsCashAngel && request.IsCreditAngel && request.IsMemberAngel)
                    {
                        paymentTypeId = (int)EPaymentType.CashAndCreditAndMember;
                    }
                    else if (request.IsCashAngel && request.IsCreditAngel && request.IsQrCodeAngel)
                    {
                        paymentTypeId = (int)EPaymentType.CashAndCreditAndQrCode;
                    }
                    else if (request.IsCashAngel && request.IsMemberAngel && request.IsQrCodeAngel)
                    {
                        paymentTypeId = (int)EPaymentType.CashAndMemberAndQrCode;
                    }
                    else if (request.IsCreditAngel && request.IsMemberAngel && request.IsQrCodeAngel)
                    {
                        paymentTypeId = (int)EPaymentType.CreditAndMemberAndQrCode;
                    }

                    else if (request.IsCashAngel && request.IsCreditAngel)
                    {
                        paymentTypeId = (int)EPaymentType.CashAndCredit;
                    }
                    else if (request.IsCashAngel && request.IsMemberAngel)
                    {
                        if (request.MemberAngelTotal == 0)
                        {
                            paymentTypeId = (int)EPaymentType.Cash;
                        }
                        else
                        {
                            paymentTypeId = (int)EPaymentType.CashAndMember;
                        }
                    }
                    else if (request.IsCashAngel && request.IsQrCodeAngel)
                    {
                        paymentTypeId = (int)EPaymentType.CashAndQrCode;
                    }
                    else if (request.IsCreditAngel && request.IsMemberAngel)
                    {
                        paymentTypeId = (int)EPaymentType.CreditAndMember;
                    }
                    else if (request.IsCreditAngel && request.IsQrCodeAngel)
                    {
                        paymentTypeId = (int)EPaymentType.CreditAndQrCode;
                    }
                    else if (request.IsMemberAngel && request.IsQrCodeAngel)
                    {
                        paymentTypeId = (int)EPaymentType.MemberAndQrCode;
                    }


                    else if (request.IsCashAngel && !request.IsCreditAngel && !request.IsMemberAngel && !request.IsQrCodeAngel)
                    {
                        paymentTypeId = (int)EPaymentType.Cash;
                    }
                    else if (request.IsCreditAngel && !request.IsCashAngel && !request.IsMemberAngel && !request.IsQrCodeAngel)
                    {
                        paymentTypeId = (int)EPaymentType.Credit;
                    }
                    else if (request.IsMemberAngel && !request.IsCashAngel && !request.IsCreditAngel && !request.IsQrCodeAngel)
                    {
                        paymentTypeId = (int)EPaymentType.Member;
                    }
                    else if (request.IsQrCodeAngel && !request.IsCashAngel && !request.IsCreditAngel && !request.IsMemberAngel)
                    {
                        paymentTypeId = (int)EPaymentType.QrCode;
                    }

                    else
                    {
                        paymentTypeId = (int)EPaymentType.NoPay;
                    }

                    var massageAngels = await _massageService.GetMassageAngelByMassageId(id);

                    foreach (var massageAngel in massageAngels)
                    {
                        _db.ChangeTracker.Clear();
                        MassageAngel _massageAngel = massageAngel;
                        massageAngel.PaymentTypeId = paymentTypeId;
                        await _massageService.UpdateMassageAngel(_massageAngel);
                    }

                    foreach (var massageAngelRound in request.MassageAngels)
                    {
                        var _massageAngelUpdate = await _massageService.GetMassageAngelById(massageAngelRound.Id);
                        _massageAngelUpdate.Round = massageAngelRound.Round;

                        if (massageAngelRound.IsDiscount || !massageAngelRound.IsDiscount)
                        {
                            _massageAngelUpdate.IsDiscount = massageAngelRound.IsDiscount;
                            _massageAngelUpdate.IsDiscountBaht = massageAngelRound.IsDiscountBaht;
                            _massageAngelUpdate.DiscountBaht = massageAngelRound.DiscountBaht;
                            _massageAngelUpdate.IsDiscountPercent = massageAngelRound.IsDiscountPercent;
                            _massageAngelUpdate.DiscountPercent = massageAngelRound.DiscountPercent;
                            _massageAngelUpdate.IsDiscountAngelRound = massageAngelRound.IsDiscountAngelRound;
                            _massageAngelUpdate.DiscountAngelRound = massageAngelRound.DiscountAngelRound;
                            _massageAngelUpdate.DiscountRemark = massageAngelRound.DiscountRemark;
                        }

                        await _massageService.UpdateMassageAngel(_massageAngelUpdate);
                    }

                    
                    if (request.MassageRooms.Count > 0)
                    {
                        foreach (var massageRoom_ in request.MassageRooms)
                        {
                            var massageRoom = await _massageService.GetMassageRoomById(massageRoom_.Id);
                            if (massageRoom_.IsDiscount || !massageRoom_.IsDiscount)
                            {
                                massageRoom.IsDiscountBaht = massageRoom_.IsDiscountBaht;
                                massageRoom.DiscountBaht = massageRoom_.DiscountBaht;
                                massageRoom.IsDiscountPercent = massageRoom_.IsDiscountPercent;
                                massageRoom.DiscountPercent = massageRoom_.DiscountPercent;
                                massageRoom.DiscountRemark = massageRoom_.DiscountRemark;
                                massageRoom.IsDiscount = massageRoom_.IsDiscount;
                            }
                            await _massageService.UpdateMassageRoom(massageRoom);
                        }
                    }

                    if (request.PaymentMembers != null)
                    {
                        foreach (var memberPayment in request.PaymentMembers)
                        {
                            var itemMember = await _memberService.GetByIdAsync((int)memberPayment.MemberId);
                            if (itemMember.MemberItems != null)
                            {
                                foreach (var memberItem in itemMember.MemberItems)
                                {
                                    if (memberItem.ItemId == memberPayment.ItemId)
                                    {
                                        memberItem.Amount = memberItem.Amount - (memberPayment.UseVip + memberPayment.UseSuite);
                                        await _memberService.UpdateMemberItem(memberItem);
                                    }
                                }
                            }
                        }
                    }
                    if (oldAgencies != request.AgenciesId)
                    {
                        var agencyPerformance = await _agencyPerformanceService.GetAgencyPerformancesByCalendarIdAndAgenciesId(massage.CalendarDayId, oldAgencies);
                        if (agencyPerformance == null)
                        {
                            AgencyPerformance _agencyPerformance = new AgencyPerformance
                            {
                                CarlendarDayId = massage.CalendarDayId,
                                AgenciesId = (int)request.AgenciesId,
                                IsPay = false,
                                IsUnPaid = false,
                                TotalMassage = 0,
                                TotalRound = 0,
                                TotalMember = 0,
                                TotalReceive = 0,
                                Remark = ""
                            };
                            await _agencyPerformanceService.CreatePaymentAgencyAsync(_agencyPerformance);
                        }
                        else
                        {
                            agencyPerformance.AgenciesId = (int)request.AgenciesId;
                            await _agencyPerformanceService.UpdateAsync(agencyPerformance);
                        }
                    }

                    massage = await _massageService.GetById(id);
                    var massageResponse = _mapper.Map<MassageResponse>(massage);
                    if (massageResponse == null)
                    {
                        return NotFound();
                    }
                    var user = await _userService.GetByIdAsync((int)massage.UpdatedBy);
                    massageResponse.user = _mapper.Map<UserResponse>(user);
                    massageAngels = await _massageService.GetMassageAngelByMassageId(id);
                    var massageRooms = await _massageService.GetListMassageRoomByMassageId(massage.Id);
                    massageResponse.massageRooms = _mapper.Map<List<MassageRoomResponse>>(massageRooms);
                    massageResponse.massageAngels = _mapper.Map<List<MassageAngelResponse>>(massageAngels);
                    var massageCreditPayment = await _massageService.GetMassageCreditPaymentByMassageId(massage.Id);
                    massageResponse.massageCreditPayment = _mapper.Map<MassageCreditPaymentResponse>(massageCreditPayment);
                    var massageMemberPayment = await _massageService.GetMassageMemberPaymentsByMassageId(massage.Id);
                    massageResponse.massageMemberPayments = _mapper.Map<List<MassageMemberPaymentResponse>>(massageMemberPayment);

                    var massageQrCodePayment = await _massageService.GetMassageQrCodePaymentByMassageId(massage.Id);
                    massageResponse.massageQrCodePayment = _mapper.Map<MassageQrCodePaymentResponse>(massageQrCodePayment);

                    foreach (var memberResponse in massageResponse.massageMemberPayments)
                    {
                        var massageMember = await _memberService.GetByIdAsync(memberResponse.MemberId);
                        memberResponse.member = _mapper.Map<MemberResponse>(massageMember);
                    }

                    await transaction.CommitAsync();
                    return Ok(massageResponse);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex.Message);
                    _logger.LogError(ex.StackTrace);
                    await transaction.RollbackAsync();
                    return BadRequest();
                }
            }

        }
        [HttpPut(ApiRoutes.Massages.MergeSaveBill)]
        public async Task<IActionResult> MergeSaveBill([FromRoute] int id, [FromBody] ListPaymentRequest request)
        {
            using (var transaction = await _db.Database.BeginTransactionAsync())
            {
                try
                {
                    if (id <= 0)
                    {
                        return BadRequest();
                    }
                    var massage = await _massageService.GetById(id);
                    var oldAgencies = massage.AgenciesId;
                    if (massage == null)
                    {
                        return NotFound();
                    }
                    bool oldUnPaid = massage.IsUnPaid;

                    massage.IsPaid = false;
                    massage.IsUnPaid = false;
                    massage.Total = request.Total;
                    massage.AngelTotal = request.AngelTotal;
                    massage.FoodTotal = request.FoodTotal;
                    massage.RoomTotal = request.RoomTotal;
                    massage.IsCashAngel = request.IsCashAngel;
                    massage.IsCreditAngel = request.IsCreditAngel;
                    massage.IsMemberAngel = request.IsMemberAngel;
                    massage.CashAngelTotal = request.CashAngelTotal;
                    massage.CreditAngelTotal = request.CreditAngelTotal;
                    massage.MemberAngelTotal = request.MemberAngelTotal;
                    massage.IsCashFood = request.IsCashFood;
                    massage.IsCreditFood = request.IsCreditFood;
                    massage.IsMemberFood = request.IsMemberFood;
                    massage.CashFoodTotal = request.CashFoodTotal;
                    massage.CreditFoodTotal = request.CreditFoodTotal;
                    massage.MemberFoodTotal = request.MemberFoodTotal;
                    massage.IsCashRoom = request.IsCashRoom;
                    massage.IsCreditRoom = request.IsCreditRoom;
                    massage.IsMemberRoom = request.IsMemberRoom;
                    massage.CashRoomTotal = request.CashRoomTotal;
                    massage.CreditRoomTotal = request.CreditRoomTotal;
                    massage.MemberRoomTotal = request.MemberRoomTotal;
                    massage.IsEntertainAngel = request.IsEntertainAngel;
                    massage.EntertainTotalAngel = request.EntertainTotalAngel;
                    massage.EntertainRemarkAngel = request.EntertainRemarkAngel;
                    massage.IsEntertainFood = request.IsEntertainFood;
                    massage.EntertainTotalFood = request.EntertainTotalFood;
                    massage.EntertainRemarkFood = request.EntertainRemarkFood;
                    massage.EtcTotal = request.EtcTotal;
                    massage.IsCashEtc = request.IsCashEtc;
                    massage.IsCreditEtc = request.IsCreditEtc;
                    massage.IsMemberEtc = request.IsMemberEtc;
                    massage.IsEntertainEtc = request.IsEntertainEtc;
                    massage.EntertainEtcRemark = request.EntertainEtcRemark;
                    massage.CashEtcTotal = request.CashEtcTotal;
                    massage.CreditEtcTotal = request.CreditEtcTotal;
                    massage.MemberEtcTotal = request.MemberEtcTotal;
                    massage.EntertainEtcTotal = request.EntertainEtcTotal;
                    massage.TipTotal = request.TipTotal;
                    massage.TipCommTotal = request.TipCommTotal;
                    massage.DamagesTotal = request.DamagesTotal;
                    massage.OtherServiceChargesTotal = request.OtherServiceChargesTotal;
                    massage.EtcRemark = request.EtcRemark;
                    massage.PayDate = null;
                    massage.CashierId = request.CashierId;

                    massage.QrCodeTotal = request.QrCodeTotal;
                    massage.IsQrCodeAngel = request.IsQrCodeAngel;
                    massage.QrCodeAngelTotal = request.QrCodeAngelTotal;
                    massage.IsQrCodeFood = request.IsQrCodeFood;
                    massage.QrCodeFoodTotal = request.QrCodeFoodTotal;
                    massage.IsQrCodeRoom = request.IsQrCodeRoom;
                    massage.QrCodeRoomTotal = request.QrCodeRoomTotal;
                    massage.IsQrCodeEtc = request.IsQrCodeEtc;
                    massage.QrCodeEtcTotal = request.QrCodeEtcTotal;

                    massage.TipQrCodeTotal = request.TipQrCodeTotal;
                    massage.CountriesId = (int)request.CountriesId;
                    massage.AgenciesId = (int)request.AgenciesId;
                    massage.AgencyPerformanceId = (int)request.AgencyPerformanceId;

                    if (oldUnPaid && !request.IsUnPaid)
                    {
                        massage.PayUnPaidDate = DateTime.Now;
                    }

                    await _massageService.UpdateAsync(massage);

                    if (massage.IsCreditAngel || massage.IsCreditFood || massage.IsCreditRoom || massage.IsCreditEtc)
                    {
                        MassageCreditPayment _credit = new MassageCreditPayment();
                        _credit.MassageId = massage.Id;
                        _credit.CardNo = request.PaymentCredit.CardNo;
                        _credit.CardType = request.PaymentCredit.CardType;
                        _credit.CreditTotal = request.PaymentCredit.CreditTotal;
                        await _massageService.CreateMassagePaymentCredit(_credit);
                    }

                    if (massage.IsMemberAngel || massage.IsMemberFood || massage.IsMemberRoom || massage.IsMemberEtc)
                    {
                        foreach (var member in request.PaymentMembers)
                        {
                            MassageMemberPayment _member = new MassageMemberPayment();
                            _member.MassageId = massage.Id;
                            _member.MemberId = (int)member.MemberId;
                            _member.MemberTotal = member.MemberTotal;
                            _member.UseSuite = member.UseSuite;
                            _member.UseVip = member.UseVip;
                            await _massageService.CreateMassagePaymentMember(_member);

                            //update credit member
                            var getMember = await _memberService.GetByIdAsync(_member.MemberId);
                            if (getMember == null)
                            {
                                return NotFound();
                            }
                            getMember.CreditAmount -= _member.MemberTotal;
                            await _memberService.UpdateAsync(getMember);
                        }
                    }

                    if (massage.IsQrCodeAngel || massage.IsQrCodeFood || massage.IsQrCodeRoom || massage.IsQrCodeEtc)
                    {
                        MassageQrCodePayment _qrcode = new MassageQrCodePayment();
                        _qrcode.MassageId = massage.Id;
                        _qrcode.QrCodeTotal = request.PaymentQrCode.QrCodeTotal;

                        await _massageService.CreateMassageQrCode(_qrcode);
                    }

                    int? paymentTypeId = null;

                    if (request.IsCashAngel && request.IsCreditAngel && request.IsMemberAngel && request.IsQrCodeAngel)
                    {
                        paymentTypeId = (int)EPaymentType.CashAndCreditAndMemberAndQrCode;
                    }
                    else if (request.IsCashAngel && request.IsCreditAngel && request.IsMemberAngel)
                    {
                        paymentTypeId = (int)EPaymentType.CashAndCreditAndMember;
                    }
                    else if (request.IsCashAngel && request.IsCreditAngel && request.IsQrCodeAngel)
                    {
                        paymentTypeId = (int)EPaymentType.CashAndCreditAndQrCode;
                    }
                    else if (request.IsCashAngel && request.IsMemberAngel && request.IsQrCodeAngel)
                    {
                        paymentTypeId = (int)EPaymentType.CashAndMemberAndQrCode;
                    }
                    else if (request.IsCreditAngel && request.IsMemberAngel && request.IsQrCodeAngel)
                    {
                        paymentTypeId = (int)EPaymentType.CreditAndMemberAndQrCode;
                    }

                    else if (request.IsCashAngel && request.IsCreditAngel)
                    {
                        paymentTypeId = (int)EPaymentType.CashAndCredit;
                    }
                    else if (request.IsCashAngel && request.IsMemberAngel)
                    {
                        if (request.MemberAngelTotal == 0)
                        {
                            paymentTypeId = (int)EPaymentType.Cash;
                        }
                        else
                        {
                            paymentTypeId = (int)EPaymentType.CashAndMember;
                        }
                    }
                    else if (request.IsCashAngel && request.IsQrCodeAngel)
                    {
                        paymentTypeId = (int)EPaymentType.CashAndQrCode;
                    }
                    else if (request.IsCreditAngel && request.IsMemberAngel)
                    {
                        paymentTypeId = (int)EPaymentType.CreditAndMember;
                    }
                    else if (request.IsCreditAngel && request.IsQrCodeAngel)
                    {
                        paymentTypeId = (int)EPaymentType.CreditAndQrCode;
                    }
                    else if (request.IsMemberAngel && request.IsQrCodeAngel)
                    {
                        paymentTypeId = (int)EPaymentType.MemberAndQrCode;
                    }


                    else if (request.IsCashAngel && !request.IsCreditAngel && !request.IsMemberAngel && !request.IsQrCodeAngel)
                    {
                        paymentTypeId = (int)EPaymentType.Cash;
                    }
                    else if (request.IsCreditAngel && !request.IsCashAngel && !request.IsMemberAngel && !request.IsQrCodeAngel)
                    {
                        paymentTypeId = (int)EPaymentType.Credit;
                    }
                    else if (request.IsMemberAngel && !request.IsCashAngel && !request.IsCreditAngel && !request.IsQrCodeAngel)
                    {
                        paymentTypeId = (int)EPaymentType.Member;
                    }
                    else if (request.IsQrCodeAngel && !request.IsCashAngel && !request.IsCreditAngel && !request.IsMemberAngel)
                    {
                        paymentTypeId = (int)EPaymentType.QrCode;
                    }

                    else
                    {
                        paymentTypeId = (int)EPaymentType.NoPay;
                    }

                    var massageAngels = await _massageService.GetMassageAngelByMassageId(id);

                    foreach (var massageAngel in massageAngels)
                    {
                        _db.ChangeTracker.Clear();
                        MassageAngel _massageAngel = massageAngel;
                        massageAngel.PaymentTypeId = paymentTypeId;
                        await _massageService.UpdateMassageAngel(_massageAngel);
                    }

                    foreach (var massageAngelRound in request.MassageAngels)
                    {
                        var _massageAngelUpdate = await _massageService.GetMassageAngelById(massageAngelRound.Id);
                        _massageAngelUpdate.Round = massageAngelRound.Round;

                        if (massageAngelRound.IsDiscount || !massageAngelRound.IsDiscount)
                        {
                            _massageAngelUpdate.IsDiscount = massageAngelRound.IsDiscount;
                            _massageAngelUpdate.IsDiscountBaht = massageAngelRound.IsDiscountBaht;
                            _massageAngelUpdate.DiscountBaht = massageAngelRound.DiscountBaht;
                            _massageAngelUpdate.IsDiscountPercent = massageAngelRound.IsDiscountPercent;
                            _massageAngelUpdate.DiscountPercent = massageAngelRound.DiscountPercent;
                            _massageAngelUpdate.IsDiscountAngelRound = massageAngelRound.IsDiscountAngelRound;
                            _massageAngelUpdate.DiscountAngelRound = massageAngelRound.DiscountAngelRound;
                            _massageAngelUpdate.DiscountRemark = massageAngelRound.DiscountRemark;
                        }

                        await _massageService.UpdateMassageAngel(_massageAngelUpdate);
                    }
                    if (request.MassageRooms.Count > 0)
                    {
                        foreach (var massageRoom_ in request.MassageRooms)
                        {
                            var massageRoom = await _massageService.GetMassageRoomById(massageRoom_.Id);
                            if (massageRoom_.IsDiscount || !massageRoom_.IsDiscount)
                            {
                                massageRoom.IsDiscountBaht = massageRoom_.IsDiscountBaht;
                                massageRoom.DiscountBaht = massageRoom_.DiscountBaht;
                                massageRoom.IsDiscountPercent = massageRoom_.IsDiscountPercent;
                                massageRoom.DiscountPercent = massageRoom_.DiscountPercent;
                                massageRoom.DiscountRemark = massageRoom_.DiscountRemark;
                                massageRoom.IsDiscount = massageRoom_.IsDiscount;
                            }
                            await _massageService.UpdateMassageRoom(massageRoom);
                        }
                    }

                    if (request.PaymentMembers != null)
                    {
                        foreach (var memberPayment in request.PaymentMembers)
                        {
                            var itemMember = await _memberService.GetByIdAsync((int)memberPayment.MemberId);
                            if (itemMember.MemberItems != null)
                            {
                                foreach (var memberItem in itemMember.MemberItems)
                                {
                                    if (memberItem.ItemId == memberPayment.ItemId)
                                    {
                                        memberItem.Amount = memberItem.Amount - (memberPayment.UseVip + memberPayment.UseSuite);
                                        await _memberService.UpdateMemberItem(memberItem);
                                    }
                                }
                            }
                        }
                    }
                    if (oldAgencies != request.AgenciesId)
                    {
                        var agencyPerformance = await _agencyPerformanceService.GetAgencyPerformancesByCalendarIdAndAgenciesId(massage.CalendarDayId, oldAgencies);
                        if (agencyPerformance == null)
                        {
                            AgencyPerformance _agencyPerformance = new AgencyPerformance
                            {
                                CarlendarDayId = massage.CalendarDayId,
                                AgenciesId = (int)request.AgenciesId,
                                IsPay = false,
                                IsUnPaid = false,
                                TotalMassage = 0,
                                TotalRound = 0,
                                TotalMember = 0,
                                TotalReceive = 0,
                                Remark = ""
                            };
                            await _agencyPerformanceService.CreatePaymentAgencyAsync(_agencyPerformance);
                        }
                        else
                        {
                            agencyPerformance.AgenciesId = (int)request.AgenciesId;
                            await _agencyPerformanceService.UpdateAsync(agencyPerformance);
                        }
                    }

                    massage = await _massageService.GetById(id);
                    var massageResponse = _mapper.Map<MassageResponse>(massage);
                    if (massageResponse == null)
                    {
                        return NotFound();
                    }
                    var user = await _userService.GetByIdAsync((int)massage.UpdatedBy);
                    massageResponse.user = _mapper.Map<UserResponse>(user);
                    massageAngels = await _massageService.GetMassageAngelByMassageId(id);
                    var listMassageRooms = await _massageService.GetListMassageRoomByMassageId(massage.Id);
                    massageResponse.massageRooms = _mapper.Map<List<MassageRoomResponse>>(listMassageRooms);
                    massageResponse.massageAngels = _mapper.Map<List<MassageAngelResponse>>(massageAngels);
                    var massageCreditPayment = await _massageService.GetMassageCreditPaymentByMassageId(massage.Id);
                    massageResponse.massageCreditPayment = _mapper.Map<MassageCreditPaymentResponse>(massageCreditPayment);
                    var massageMemberPayment = await _massageService.GetMassageMemberPaymentsByMassageId(massage.Id);
                    massageResponse.massageMemberPayments = _mapper.Map<List<MassageMemberPaymentResponse>>(massageMemberPayment);

                    var massageQrCodePayment = await _massageService.GetMassageQrCodePaymentByMassageId(massage.Id);
                    massageResponse.massageQrCodePayment = _mapper.Map<MassageQrCodePaymentResponse>(massageQrCodePayment);

                    foreach (var memberResponse in massageResponse.massageMemberPayments)
                    {
                        var massageMember = await _memberService.GetByIdAsync(memberResponse.MemberId);
                        memberResponse.member = _mapper.Map<MemberResponse>(massageMember);
                    }

                    await transaction.CommitAsync();
                    return Ok(massageResponse);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex.Message);
                    _logger.LogError(ex.StackTrace);
                    await transaction.RollbackAsync();
                    return BadRequest();
                }
            }
        }
        [HttpPatch(ApiRoutes.Massages.MergeCheckOutAngelAndRoom)]
        public async Task<IActionResult> MergeCheckOutAngelAndRoom([FromBody] UpdateMassageAngelRequest request)
        {
            using (var transaction = await _db.Database.BeginTransactionAsync())
            {
                try
                {
                    int? calenDarDayId = await GetCalendarDayId();
                    if (calenDarDayId == null)
                    {
                        return NotFound();
                    }
                    MassageAngel massageAngel = await _massageService.GetMassageAngelById((int)request.MassageAngelId);
                    if (massageAngel == null)
                    {
                        return NotFound();
                    }
                    var getAngel = await _angelService.GetByIdAsync(massageAngel.AngelId);
                    if (getAngel == null)
                    {
                        return NotFound();
                    }
                    TimeSpan timespan = DateTime.Now - (DateTime)massageAngel.CheckInTime;
                    double totalMinute = timespan.TotalMinutes;
                    massageAngel.CheckOutTime = DateTime.Now;
                    massageAngel.TimeMinute = (int)totalMinute;

                    // logic sum roundFact
                    var modMinute = massageAngel.TimeMinute % getAngel.AngelType.RoundTime;
                    var roundFact = 1.0;
                    roundFact = Math.Floor((double)massageAngel.TimeMinute / getAngel.AngelType.RoundTime);

                    var haftRoundTime = Math.Floor((double)getAngel.AngelType.RoundTime / 2);

                    if (roundFact >= 1)
                    {
                        if (modMinute > 15 && modMinute <= haftRoundTime)
                        {
                            roundFact += 0.5;
                        }
                        else if (modMinute > haftRoundTime && modMinute < getAngel.AngelType.RoundTime)
                        {
                            roundFact += 1;
                        }
                    }
                    else
                    {
                        if (modMinute > 15)
                        {
                            roundFact = 1;
                        }
                    }

                    massageAngel.RoundFact = (decimal)roundFact;
                    massageAngel.PaymentTypeId = (int)EPaymentType.NoPay;
                    // end
                    await _massageService.UpdateMassageAngel(massageAngel);

                    // update status angel
                    var angel = await _angelService.GetByIdAsync(massageAngel.AngelId);
                    if (angel == null)
                    {
                        return NotFound();
                    }
                    angel.Status = (int)EAngelStatus.Ready;
                    await _angelService.UpdateAsync(angel);
                    //end

                    // update status room
                    var room = await _roomService.GetByIdAsync(massageAngel.RoomId);
                    if (room == null)
                    {
                        return NotFound();
                    }

                    if (room.RoomTypeId == (int)ERoomType.Normal || room.RoomTypeId == (int)ERoomType.RoundSell)
                    {
                        bool isMultiple = await _massageService.GetListMultipleAngleByRoomNormal(massageAngel.MassageId,massageAngel.RoomId);
                        if (!isMultiple)
                        {
                            room.Status = (int)ERoomStatus.Ready;
                            await _roomService.UpdateAsync(room);
                        }
                    }
                    //end
                    var angelPerformance = await _angelPerformanceService.CreateOrUpdateByIdAsync(massageAngel, (int)calenDarDayId);
                    if (angelPerformance == null)
                    {
                        return NotFound();
                    }
                    else //Aunz
                    {
                        MassageAngel _massageAngel = await _massageService.GetMassageAngelById((int)request.MassageAngelId);
                        _massageAngel.AngelPerformanceId = angelPerformance.Id;

                        await _massageService.UpdateMassageAngel(_massageAngel);
                    }

                    bool isDeductType = _deductTypeService.CheckAngelPerformanceDeduct(massageAngel.Id);

                    if (!isDeductType)
                    {
                        _logger.LogDebug("True CheckOutAngelAndRoom");
                        var deductType = await _deductTypeService.GetAllAsync(null, null);

                        if (deductType.Count > 0)
                        {
                            List<AngelPerformanceDeduct> angelPerformanceDeducts = new List<AngelPerformanceDeduct>();
                            foreach (var item in deductType)
                            {
                                AngelPerformanceDeduct angelPerformanceDeduct = new AngelPerformanceDeduct();
                                angelPerformanceDeduct.MassageAngelId = massageAngel.Id;
                                angelPerformanceDeduct.DeductTypeId = item.Id;
                                angelPerformanceDeduct.AngelPerformanceId = angelPerformance.Id;
                                angelPerformanceDeduct.Fee = item.Fee;
                                angelPerformanceDeducts.Add(angelPerformanceDeduct);

                            };
                            await _massageService.CreateAngelPerformanceDeduct(angelPerformanceDeducts);
                        }
                    }
                    else
                    {
                        _logger.LogDebug("False CheckOutAngelAndRoom");
                    }


                    await transaction.CommitAsync();

                    var massageAngelResponse = _mapper.Map<MassageAngelResponse>(massageAngel);

                    await _checkInHub.Clients.All.SendAsync("CheckOutRoom", massageAngelResponse);


                    return Ok();
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex.Message);
                    _logger.LogError(ex.StackTrace);
                    await transaction.RollbackAsync();
                    return BadRequest();
                }
            }
        }
        [HttpPatch(ApiRoutes.Massages.MergeCheckOutRoomTypeSuite)]
        public async Task<IActionResult> MergeCheckOutRoomTypeSuite([FromBody] UpdateMassageAngelRequest request)
        {
            using (var transaction = await _db.Database.BeginTransactionAsync())
            {
                try
                {
                    List<int> listParentRoom = new List<int>();
                    MassageRoom massageRoom = await _massageService.GetMassageRoomById((int)request.MassageRoomId);
                    if (massageRoom == null)
                    {
                        return NotFound();
                    }
                    TimeSpan timespan = DateTime.Now - (DateTime)massageRoom.CheckInTime;
                    double totalMinute = timespan.TotalMinutes;
                    massageRoom.CheckOutTime = DateTime.Now;
                    massageRoom.TimeMinute = (int)totalMinute;

                    var modMinuteRoom = massageRoom.TimeMinute % massageRoom.Room.RoomType.RoundTime;
                    var deleteMinuteRoom = massageRoom.TimeMinute - massageRoom.Room.RoomType.RoundTime;
                    var roundFactRoom = 1.0;
                    roundFactRoom = Math.Floor((double)massageRoom.TimeMinute / massageRoom.Room.RoomType.RoundTime);
                    var haftRoundTimeRoom = Math.Floor((double)massageRoom.Room.RoomType.RoundTime / 2);

                    if (roundFactRoom >= 1)
                    {
                        if (massageRoom.Room.RoomType.Id == (int)ERoomType.Vip || massageRoom.Room.RoomType.Id == (int)ERoomType.Vip2 || massageRoom.Room.RoomType.Id == (int)ERoomType.Vip3)
                        {
                            if (modMinuteRoom > 15 && modMinuteRoom <= haftRoundTimeRoom)
                            {
                                roundFactRoom += 0.5;
                            }
                            else if (modMinuteRoom > haftRoundTimeRoom && modMinuteRoom < massageRoom.Room.RoomType.RoundTime)
                            {
                                roundFactRoom += 1;
                            }
                        }
                        else
                        {
                            if ((modMinuteRoom > 15 && modMinuteRoom <= haftRoundTimeRoom) || (modMinuteRoom > haftRoundTimeRoom && modMinuteRoom < massageRoom.Room.RoomType.RoundTime))
                            {
                                roundFactRoom += 1;
                            }
                            //roundFactRoom = Math.Floor((double)deleteMinuteRoom / 60) + 1;
                        }
                    }
                    else
                    {
                        roundFactRoom = 1;
                    }

                    massageRoom.Round = (decimal)roundFactRoom;

                    await _massageService.UpdateMassageRoom(massageRoom);

                    var room = await _roomService.GetByIdAsync(massageRoom.RoomId);
                    if (room != null)
                    {
                        room.Status = (int)ERoomStatus.Ready;
                        await _roomService.UpdateAsync(room);

                        var parentRoom = await _roomService.GetChildRoomParent(room.Id);
                        if (parentRoom.Count > 0)
                        {
                            foreach (var itemroom in parentRoom)
                            {
                                listParentRoom.Add((int)itemroom.Id);
                                itemroom.Status = (int)ERoomStatus.Ready;
                                await _roomService.UpdateAsync(itemroom);
                            }
                        }
                    }

                    int? calenDarDayId = await GetCalendarDayId();
                    if (calenDarDayId == null)
                    {
                        return NotFound();
                    }

                    List<MassageAngel> massageAngels = new List<MassageAngel>();
                    if (room.RoomTypeId == (int)ERoomType.Vip || room.RoomTypeId == (int)ERoomType.Vip2 || room.RoomTypeId == (int)ERoomType.Vip3)
                    {
                        var getMassageAngelCheckOutIsNull = await _massageService.GetMassageAngelByMassageIdAndRoomIdAndCheckoutIsNull(massageRoom.MassageId, room.Id);
                        foreach (var item in getMassageAngelCheckOutIsNull)
                        {
                            var itemMassageAngel = await _massageService.GetMassageAngelById(item.Id);

                            var getAngel = await _angelService.GetByIdAsync(itemMassageAngel.AngelId);
                            if (getAngel == null)
                            {
                                return NotFound();
                            }
                            TimeSpan timespanmassageAngel = DateTime.Now - (DateTime)itemMassageAngel.CheckInTime;
                            double totalMinutes = timespanmassageAngel.TotalMinutes;
                            itemMassageAngel.CheckOutTime = DateTime.Now;
                            itemMassageAngel.TimeMinute = (int)totalMinute;

                            // logic sum roundFact
                            var modMinute = itemMassageAngel.TimeMinute % getAngel.AngelType.RoundTime;
                            var roundFact = 1.0;
                            roundFact = Math.Floor((double)itemMassageAngel.TimeMinute / getAngel.AngelType.RoundTime);

                            var haftRoundTime = Math.Floor((double)getAngel.AngelType.RoundTime / 2);

                            if (roundFact >= 1)
                            {
                                if (modMinute > 15 && modMinute <= haftRoundTime)
                                {
                                    roundFact += 0.5;
                                }
                                else if (modMinute > haftRoundTime && modMinute < getAngel.AngelType.RoundTime)
                                {
                                    roundFact += 1;
                                }
                            }
                            else
                            {
                                if (modMinute > 15)
                                {
                                    roundFact = 1;
                                }
                            }
                            itemMassageAngel.RoundFact = (decimal)roundFact;
                            // end
                            await _massageService.UpdateMassageAngel(itemMassageAngel);


                            // update status angel
                            var angel = await _angelService.GetByIdAsync(itemMassageAngel.AngelId);
                            if (angel == null)
                            {
                                return NotFound();
                            }
                            angel.Status = (int)EAngelStatus.Ready;
                            await _angelService.UpdateAsync(angel);
                            //end

                            // update status room
                            var massageroom = await _roomService.GetByIdAsync(item.RoomId);
                            if (massageroom == null)
                            {
                                return NotFound();
                            }
                            if (massageroom.RoomTypeId == (int)ERoomType.Normal || massageroom.RoomTypeId == (int)ERoomType.RoundSell)
                            {
                                massageroom.Status = (int)ERoomStatus.Ready;
                                await _roomService.UpdateAsync(massageroom);

                                var parentRoom = await _roomService.GetChildRoomParent(massageroom.Id);
                                if (parentRoom != null)
                                {
                                    foreach (var itemroom in parentRoom)
                                    {
                                        itemroom.Status = (int)ERoomStatus.Ready;
                                        await _roomService.UpdateAsync(itemroom);
                                    }
                                }
                            }

                            var angelPerformance = await _angelPerformanceService.CreateOrUpdateByIdAsync(itemMassageAngel, (int)calenDarDayId);
                            if (angelPerformance == null)
                            {
                                return NotFound();
                            }

                            else //Aunz
                            {
                                itemMassageAngel.AngelPerformanceId = angelPerformance.Id;
                                await _massageService.UpdateMassageAngel(itemMassageAngel);
                            }

                            bool isDeductType = _deductTypeService.CheckAngelPerformanceDeduct(itemMassageAngel.Id);
                            if (!isDeductType)
                            {
                                //_logger.LogDebug("True CheckOutRoomTypeSuite");
                                var deductType = await _deductTypeService.GetAllAsync(null, null);

                                if (deductType.Count > 0)
                                {
                                    List<AngelPerformanceDeduct> angelPerformanceDeducts = new List<AngelPerformanceDeduct>();
                                    foreach (var itemdeduct in deductType)
                                    {
                                        AngelPerformanceDeduct angelPerformanceDeduct = new AngelPerformanceDeduct();
                                        angelPerformanceDeduct.MassageAngelId = itemMassageAngel.Id;
                                        angelPerformanceDeduct.DeductTypeId = itemdeduct.Id;
                                        angelPerformanceDeduct.AngelPerformanceId = angelPerformance.Id;
                                        angelPerformanceDeduct.Fee = itemdeduct.Fee;
                                        angelPerformanceDeducts.Add(angelPerformanceDeduct);

                                    };
                                    await _massageService.CreateAngelPerformanceDeduct(angelPerformanceDeducts);

                                    //var Totaldeduct = await _angelPerformanceService.TotalAngelPerformanceDeduct(angelPerformance.Id);

                                    //var angelperformance = await _angelPerformanceService.GetByIdAsync(angelPerformanceDeducts[0].AngelPerformanceId);

                                    //angelperformance.TotalDeduct = Totaldeduct;

                                    //await _angelPerformanceService.UpdateAsync(angelperformance);
                                }
                            }
                            else
                            {
                                _logger.LogDebug("False CheckOutRoomTypeSuite");
                            }

                        }
                    }
                    else
                    {
                        foreach (var parentRooms in listParentRoom)
                        {
                            var getMassageAngelCheckOutIsNull = await _massageService.GetMassageAngelByMassageIdAndRoomIdAndCheckoutIsNull(massageRoom.MassageId, parentRooms);
                            foreach (var item in getMassageAngelCheckOutIsNull)
                            {
                                var itemMassageAngel = await _massageService.GetMassageAngelById(item.Id);

                                var getAngel = await _angelService.GetByIdAsync(itemMassageAngel.AngelId);
                                if (getAngel == null)
                                {
                                    return NotFound();
                                }
                                TimeSpan timespanmassageAngel = DateTime.Now - (DateTime)itemMassageAngel.CheckInTime;
                                double totalMinutes = timespanmassageAngel.TotalMinutes;
                                itemMassageAngel.CheckOutTime = DateTime.Now;
                                itemMassageAngel.TimeMinute = (int)totalMinute;

                                // logic sum roundFact
                                var modMinute = itemMassageAngel.TimeMinute % getAngel.AngelType.RoundTime;
                                var roundFact = 1.0;
                                roundFact = Math.Floor((double)itemMassageAngel.TimeMinute / getAngel.AngelType.RoundTime);

                                var haftRoundTime = Math.Floor((double)getAngel.AngelType.RoundTime / 2);

                                if (roundFact >= 1)
                                {
                                    if (modMinute > 15 && modMinute <= haftRoundTime)
                                    {
                                        roundFact += 0.5;
                                    }
                                    else if (modMinute > haftRoundTime && modMinute < getAngel.AngelType.RoundTime)
                                    {
                                        roundFact += 1;
                                    }
                                }
                                else
                                {
                                    if (modMinute > 15)
                                    {
                                        roundFact = 1;
                                    }
                                }
                                itemMassageAngel.RoundFact = (decimal)roundFact;
                                // end
                                await _massageService.UpdateMassageAngel(itemMassageAngel);


                                // update status angel
                                var angel = await _angelService.GetByIdAsync(itemMassageAngel.AngelId);
                                if (angel == null)
                                {
                                    return NotFound();
                                }
                                angel.Status = (int)EAngelStatus.Ready;
                                await _angelService.UpdateAsync(angel);
                                //end

                                // update status room
                                var massageroom = await _roomService.GetByIdAsync(item.RoomId);
                                if (massageroom == null)
                                {
                                    return NotFound();
                                }
                                if (massageroom.RoomTypeId == (int)ERoomType.Normal || massageroom.RoomTypeId == (int)ERoomType.RoundSell)
                                {
                                    massageroom.Status = (int)ERoomStatus.Ready;
                                    await _roomService.UpdateAsync(massageroom);

                                    var parentRoom = await _roomService.GetChildRoomParent(massageroom.Id);
                                    if (parentRoom != null)
                                    {
                                        foreach (var itemroom in parentRoom)
                                        {
                                            itemroom.Status = (int)ERoomStatus.Ready;
                                            await _roomService.UpdateAsync(itemroom);
                                        }
                                    }
                                }

                                var angelPerformance = await _angelPerformanceService.CreateOrUpdateByIdAsync(itemMassageAngel, (int)calenDarDayId);
                                if (angelPerformance == null)
                                {
                                    return NotFound();
                                }

                                else //Aunz
                                {
                                    itemMassageAngel.AngelPerformanceId = angelPerformance.Id;
                                    await _massageService.UpdateMassageAngel(itemMassageAngel);
                                }

                                bool isDeductType = _deductTypeService.CheckAngelPerformanceDeduct(itemMassageAngel.Id);
                                if (!isDeductType)
                                {
                                    var deductType = await _deductTypeService.GetAllAsync(null, null);

                                    if (deductType.Count > 0)
                                    {
                                        List<AngelPerformanceDeduct> angelPerformanceDeducts = new List<AngelPerformanceDeduct>();
                                        foreach (var itemdeduct in deductType)
                                        {
                                            AngelPerformanceDeduct angelPerformanceDeduct = new AngelPerformanceDeduct();
                                            angelPerformanceDeduct.MassageAngelId = itemMassageAngel.Id;
                                            angelPerformanceDeduct.DeductTypeId = itemdeduct.Id;
                                            angelPerformanceDeduct.AngelPerformanceId = angelPerformance.Id;
                                            angelPerformanceDeduct.Fee = itemdeduct.Fee;
                                            angelPerformanceDeducts.Add(angelPerformanceDeduct);

                                        };
                                        await _massageService.CreateAngelPerformanceDeduct(angelPerformanceDeducts);

                                        //var Totaldeduct = await _angelPerformanceService.TotalAngelPerformanceDeduct(angelPerformance.Id);

                                        //var angelperformance = await _angelPerformanceService.GetByIdAsync(angelPerformanceDeducts[0].AngelPerformanceId);

                                        //angelperformance.TotalDeduct = Totaldeduct;

                                        //await _angelPerformanceService.UpdateAsync(angelperformance);
                                    }
                                }
                                else
                                {
                                    _logger.LogDebug("False CheckOutRoomTypeSuite");
                                }

                            }

                        }
                    }
                   
                    await transaction.CommitAsync();
                    foreach (var parentRooms in listParentRoom)
                    {
                        var getMassageAngelCheckOutIsNull = await _massageService.GetMassageAngelByMassageIdAndRoomIdAndCheckoutIsNull(massageRoom.MassageId, parentRooms);

                        var massageAngelResponse = _mapper.Map<List<MassageAngelResponse>>(getMassageAngelCheckOutIsNull);
                        var massageRoomResponse = _mapper.Map<MassageRoomResponse>(massageRoom);
                        await _checkInHub.Clients.All.SendAsync("CheckOutRoomSuite", new { MassageRoom = massageRoomResponse, MassageAngels = massageAngelResponse });
                    }


                    return Ok();
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex.Message);
                    _logger.LogError(ex.StackTrace);
                    await transaction.RollbackAsync();
                    return BadRequest();
                }
            }
        }

        [HttpPatch(ApiRoutes.Massages.MergeChangeRoom)]
        public async Task<IActionResult> MergeChangeRoom([FromBody] UpdateMassageAngelRequest request)
        {
            using (var transaction = await _db.Database.BeginTransactionAsync())
            {
                try
                {
                    var room = await _roomService.GetByIdAsync((int)request.RoomId); //ห้องใหม่
                    var massageAngel = await _massageService.GetMassageAngelById((int)request.MassageAngelId);
                    var _massageRoom = await _massageService.GetMassageRoomByMassageId(massageAngel.MassageId); //ห้องเก่า
                    var massageAngelByMassage = await _massageService.GetMassageAngelByMassageId(massageAngel.MassageId);
                    var _massageAngelByMassage = await _massageService.GetMassageAngelByMassageId(massageAngel.MassageId, massageAngel.RoomId);

                    // ธรรมดา --> ธรรมดา
                    if ((massageAngel.Room.RoomTypeId == (int)ERoomType.Normal) && (room.RoomTypeId == (int)ERoomType.Normal))
                    {
                        if (_massageAngelByMassage.Count == 1)
                        {
                            var oldRoom = await _roomService.GetByIdAsync(massageAngel.RoomId);
                            oldRoom.Status = (int)ERoomStatus.Ready;
                            await _roomService.UpdateAsync(oldRoom);
                        }
                        massageAngel.RoomId = room.Id;
                        await _massageService.UpdateMassageAngel(massageAngel);

                        //อัพเดรตสเตตัสห้องใหม่
                        room.Status = (int)ERoomStatus.Working;
                        await _roomService.UpdateAsync(room);

                    }
                    // ธรรมดา --> วีไอพี
                    if ((massageAngel.Room.RoomTypeId == (int)ERoomType.Normal) && (room.RoomTypeId == (int)ERoomType.Vip || room.RoomTypeId == (int)ERoomType.Vip2 || room.RoomTypeId == (int)ERoomType.Vip3))
                    {
                        if (_massageAngelByMassage.Count == 1)
                        {
                            var oldRoom = await _roomService.GetByIdAsync(massageAngel.RoomId);
                            oldRoom.Status = (int)ERoomStatus.Ready;
                            await _roomService.UpdateAsync(oldRoom);
                        }
                        var checkRoom = await _massageService.GetMassageRoomByMassageIdAndRoomId(massageAngel.MassageId, room.Id);
                        if (checkRoom != null)
                        {
                            massageAngel.RoomId = room.Id;
                            await _massageService.UpdateMassageAngel(massageAngel);


                            //ห้องใหญ่
                            room.Status = (int)ERoomStatus.Working;
                            await _roomService.UpdateAsync(room);
                        }
                        else
                        {
                            DateTime dateNow = RoundUp(DateTime.Now, TimeSpan.FromMinutes(5));
                            MassageRoom massageRoom = new MassageRoom();
                            massageRoom.MassageId = massageAngel.MassageId;
                            massageRoom.RoomId = room.Id;
                            massageRoom.ReceptionId = massageAngel.ReceptionId;
                            massageRoom.CheckInTime = dateNow;
                            massageRoom.CheckOutTime = null;
                            massageRoom.OldCheckInTime = dateNow;
                            massageRoom.TimeMinute = 0;
                            massageRoom.LastCallTime = null;
                            massageRoom.CallCount = 0;
                            massageRoom.IsNotCall = massageAngel.IsNotCall;
                            await _massageService.CheckInMassageRoom(massageRoom);

                            massageAngel.RoomId = room.Id;
                            await _massageService.UpdateMassageAngel(massageAngel);

                            room.Status = (int)ERoomStatus.Working;
                            await _roomService.UpdateAsync(room);
                        }

                    }
                    // ธรรมดา --> สูท
                    else if ((massageAngel.Room.RoomTypeId == (int)ERoomType.Normal) && (room.RoomTypeId == (int)ERoomType.SuiteSub))
                    {
                        var _room = await _roomService.GetByIdAsync((int)room.ParentRoomId);
                        if (_massageAngelByMassage.Count == 1)
                        {
                            var oldRoom = await _roomService.GetByIdAsync(massageAngel.RoomId);
                            oldRoom.Status = (int)ERoomStatus.Ready;
                            await _roomService.UpdateAsync(oldRoom);
                        }
                        var checkRoom = await _massageService.GetMassageRoomByMassageIdAndRoomId(massageAngel.MassageId, _room.Id);
                        if (checkRoom != null)
                        {
                            massageAngel.RoomId = room.Id;
                            await _massageService.UpdateMassageAngel(massageAngel);

                            //ห้องใหญ่
                            room.Status = (int)ERoomStatus.Working;
                            await _roomService.UpdateAsync(room);

                            //ห้องย่อย
                            _room.Status = (int)ERoomStatus.Working;
                            await _roomService.UpdateAsync(_room);
                        }
                        else
                        {
                            DateTime dateNow = RoundUp(DateTime.Now, TimeSpan.FromMinutes(5));
                            MassageRoom massageRoom = new MassageRoom();
                            massageRoom.MassageId = massageAngel.MassageId;
                            massageRoom.RoomId = _room.Id;
                            massageRoom.ReceptionId = massageAngel.ReceptionId;
                            massageRoom.CheckInTime = dateNow;
                            massageRoom.CheckOutTime = null;
                            massageRoom.OldCheckInTime = dateNow;
                            massageRoom.TimeMinute = 0;
                            massageRoom.LastCallTime = null;
                            massageRoom.CallCount = 0;
                            massageRoom.IsNotCall = massageAngel.IsNotCall;
                            await _massageService.CheckInMassageRoom(massageRoom);

                            massageAngel.RoomId = room.Id;
                            await _massageService.UpdateMassageAngel(massageAngel);

                            room.Status = (int)ERoomStatus.Working;
                            await _roomService.UpdateAsync(room);

                            _room.Status = (int)ERoomStatus.Working;
                            await _roomService.UpdateAsync(_room);
                        }
                    }
                    // วีไอพี --> ธรรมดา
                    else if ((massageAngel.Room.RoomTypeId == (int)ERoomType.Vip || massageAngel.Room.RoomTypeId == (int)ERoomType.Vip2 || massageAngel.Room.RoomTypeId == (int)ERoomType.Vip3) && (room.RoomTypeId == (int)ERoomType.Normal))
                    {
                        massageAngel.RoomId = room.Id;
                        await _massageService.UpdateMassageAngel(massageAngel);

                        room.Status = (int)ERoomStatus.Working;
                        await _roomService.UpdateAsync(room);

                    }
                    // วีไอพี --> วีไอพี
                    else if ((massageAngel.Room.RoomTypeId == (int)ERoomType.Vip || massageAngel.Room.RoomTypeId == (int)ERoomType.Vip2 || massageAngel.Room.RoomTypeId == (int)ERoomType.Vip3) && (room.RoomTypeId == (int)ERoomType.Vip || room.RoomTypeId == (int)ERoomType.Vip2 || room.RoomTypeId == (int)ERoomType.Vip3))
                    {
                        var checkRoom = await _massageService.GetMassageRoomByMassageIdAndRoomId(massageAngel.MassageId, room.Id);
                        if (checkRoom != null)
                        {
                            massageAngel.RoomId = room.Id;
                            await _massageService.UpdateMassageAngel(massageAngel);

                            //ห้องใหญ่
                            room.Status = (int)ERoomStatus.Working;
                            await _roomService.UpdateAsync(room);

                        }
                        else
                        {
                            DateTime dateNow = RoundUp(DateTime.Now, TimeSpan.FromMinutes(5));
                            MassageRoom massageRoom = new MassageRoom();
                            massageRoom.MassageId = massageAngel.MassageId;
                            massageRoom.RoomId = room.Id;
                            massageRoom.ReceptionId = massageAngel.ReceptionId;
                            massageRoom.CheckInTime = dateNow;
                            massageRoom.CheckOutTime = null;
                            massageRoom.OldCheckInTime = dateNow;
                            massageRoom.TimeMinute = 0;
                            massageRoom.LastCallTime = null;
                            massageRoom.CallCount = 0;
                            massageRoom.IsNotCall = massageAngel.IsNotCall;
                            await _massageService.CheckInMassageRoom(massageRoom);

                            massageAngel.RoomId = room.Id;
                            await _massageService.UpdateMassageAngel(massageAngel);

                            room.Status = (int)ERoomStatus.Working;
                            await _roomService.UpdateAsync(room);
                        }
                    }
                    // วีไอพี --> สูท
                    else if ((massageAngel.Room.RoomTypeId == (int)ERoomType.Vip || massageAngel.Room.RoomTypeId == (int)ERoomType.Vip2 || massageAngel.Room.RoomTypeId == (int)ERoomType.Vip3) && (room.RoomTypeId == (int)ERoomType.SuiteSub))
                    {
                        var _room = await _roomService.GetByIdAsync((int)room.ParentRoomId);
                       
                        var checkRoom = await _massageService.GetMassageRoomByMassageIdAndRoomId(massageAngel.MassageId, _room.Id);
                        if (checkRoom != null)
                        {
                            massageAngel.RoomId = room.Id;
                            await _massageService.UpdateMassageAngel(massageAngel);

                            //ห้องใหญ่
                            room.Status = (int)ERoomStatus.Working;
                            await _roomService.UpdateAsync(room);

                            //ห้องย่อย
                            _room.Status = (int)ERoomStatus.Working;
                            await _roomService.UpdateAsync(_room);
                        }
                        else
                        {
                            DateTime dateNow = RoundUp(DateTime.Now, TimeSpan.FromMinutes(5));
                            MassageRoom massageRoom = new MassageRoom();
                            massageRoom.MassageId = massageAngel.MassageId;
                            massageRoom.RoomId = _room.Id;
                            massageRoom.ReceptionId = massageAngel.ReceptionId;
                            massageRoom.CheckInTime = dateNow;
                            massageRoom.CheckOutTime = null;
                            massageRoom.OldCheckInTime = dateNow;
                            massageRoom.TimeMinute = 0;
                            massageRoom.LastCallTime = null;
                            massageRoom.CallCount = 0;
                            massageRoom.IsNotCall = massageAngel.IsNotCall;
                            await _massageService.CheckInMassageRoom(massageRoom);

                            massageAngel.RoomId = room.Id;
                            await _massageService.UpdateMassageAngel(massageAngel);

                            room.Status = (int)ERoomStatus.Working;
                            await _roomService.UpdateAsync(room);

                            _room.Status = (int)ERoomStatus.Working;
                            await _roomService.UpdateAsync(_room);
                        }
                    }
                    // สูท --> ธรรมดา
                    else if ((massageAngel.Room.RoomTypeId == (int)ERoomType.SuiteSub) && (room.RoomTypeId == (int)ERoomType.Normal))
                    {
                        massageAngel.RoomId = room.Id;
                        await _massageService.UpdateMassageAngel(massageAngel);

                        room.Status = (int)ERoomStatus.Working;
                        await _roomService.UpdateAsync(room);
                    }
                    // สูท --> วีไอพี
                    else if ((massageAngel.Room.RoomTypeId == (int)ERoomType.SuiteSub) && (room.RoomTypeId == (int)ERoomType.Vip || room.RoomTypeId == (int)ERoomType.Vip2 || room.RoomTypeId == (int)ERoomType.Vip3))
                    {
                        var checkRoom = await _massageService.GetMassageRoomByMassageIdAndRoomId(massageAngel.MassageId, room.Id);
                        if (checkRoom != null)
                        {
                            massageAngel.RoomId = room.Id;
                            await _massageService.UpdateMassageAngel(massageAngel);
                            
                            //ห้องใหญ่
                            room.Status = (int)ERoomStatus.Working;
                            await _roomService.UpdateAsync(room);
                        }
                        else
                        {
                            DateTime dateNow = RoundUp(DateTime.Now, TimeSpan.FromMinutes(5));
                            MassageRoom massageRoom = new MassageRoom();
                            massageRoom.MassageId = massageAngel.MassageId;
                            massageRoom.RoomId = room.Id;
                            massageRoom.ReceptionId = massageAngel.ReceptionId;
                            massageRoom.CheckInTime = dateNow;
                            massageRoom.CheckOutTime = null;
                            massageRoom.OldCheckInTime = dateNow;
                            massageRoom.TimeMinute = 0;
                            massageRoom.LastCallTime = null;
                            massageRoom.CallCount = 0;
                            massageRoom.IsNotCall = massageAngel.IsNotCall;
                            await _massageService.CheckInMassageRoom(massageRoom);

                            massageAngel.RoomId = room.Id;
                            await _massageService.UpdateMassageAngel(massageAngel);

                            room.Status = (int)ERoomStatus.Working;
                            await _roomService.UpdateAsync(room);
                        }
                    }
                    // สูท --> สูท
                    else if ((massageAngel.Room.RoomTypeId == (int)ERoomType.SuiteSub) && (room.RoomTypeId == (int)ERoomType.SuiteSub))
                    {
                        var _room = await _roomService.GetByIdAsync((int)room.ParentRoomId);

                        if (_massageAngelByMassage.Count == 1)
                        {
                            var oldRoom = await _roomService.GetByIdAsync(massageAngel.RoomId);
                            oldRoom.Status = (int)ERoomStatus.Ready;
                            await _roomService.UpdateAsync(oldRoom);
                        }

                        var checkRoom = await _massageService.GetMassageRoomByMassageIdAndRoomId(massageAngel.MassageId, _room.Id);
                        if (checkRoom != null)
                        {
                            massageAngel.RoomId = room.Id;
                            await _massageService.UpdateMassageAngel(massageAngel);
                            //ห้องใหญ่
                            room.Status = (int)ERoomStatus.Working;
                            await _roomService.UpdateAsync(room);

                            //ห้องย่อย
                            _room.Status = (int)ERoomStatus.Working;
                            await _roomService.UpdateAsync(_room);
                        }
                        else
                        {
                            DateTime dateNow = RoundUp(DateTime.Now, TimeSpan.FromMinutes(5));
                            MassageRoom massageRoom = new MassageRoom();
                            massageRoom.MassageId = massageAngel.MassageId;
                            massageRoom.RoomId = _room.Id;
                            massageRoom.ReceptionId = massageAngel.ReceptionId;
                            massageRoom.CheckInTime = dateNow;
                            massageRoom.CheckOutTime = null;
                            massageRoom.OldCheckInTime = dateNow;
                            massageRoom.TimeMinute = 0;
                            massageRoom.LastCallTime = null;
                            massageRoom.CallCount = 0;
                            massageRoom.IsNotCall = massageAngel.IsNotCall;
                            await _massageService.CheckInMassageRoom(massageRoom);

                            massageAngel.RoomId = room.Id;
                            await _massageService.UpdateMassageAngel(massageAngel);

                            //ห้องใหญ่
                            room.Status = (int)ERoomStatus.Working;
                            await _roomService.UpdateAsync(room);

                            //ห้องย่อย
                            _room.Status = (int)ERoomStatus.Working;
                            await _roomService.UpdateAsync(_room);
                        }
                    }
                        await transaction.CommitAsync();
                    return Ok();
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex.Message);
                    _logger.LogError(ex.StackTrace);
                    await transaction.RollbackAsync();
                    return BadRequest();
                }
            }
        }

        [HttpGet(ApiRoutes.Massages.GetMergeBillCheckInAngelMassage)]
        public async Task<IActionResult> GetMergeBillCheckInAngelMassage(int id)
        {
            try
            {
                var massageRoom = await _massageService.GetMassageRoomById(id);
                var massageAngel = await _massageService.GetMassageAngelByMassageId(massageRoom.MassageId,massageRoom.RoomId);
                if (massageAngel.Count == 0)
                {
                    var getMassageRoom = await _massageService.GetMassageRoomByMassageIdAndRoomId(massageRoom.MassageId, massageRoom.RoomId);
                    var response = _mapper.Map<MassageRoomResponse>(getMassageRoom);
                    return Ok(response);
                }
                else
                {
                    var response = _mapper.Map<List<MassageAngelResponse>>(massageAngel);
                    return Ok(response);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }

        private DateTime RoundUp(DateTime dt, TimeSpan d)
        {
            return new DateTime((dt.Ticks + d.Ticks - 1) / d.Ticks * d.Ticks, dt.Kind);
        }
        private DateTime RoundDown(DateTime dt, TimeSpan d)
        {
            var delta = dt.Ticks % d.Ticks;
            return new DateTime(dt.Ticks - delta, dt.Kind);
        }
        private bool CheckTime(DateTime dt)
        {
            bool _dt = false;
            if (dt.Minute >= 5 && dt.Minute <= 9)
            {
                _dt = true;
            }
            else if (dt.Minute >= 15 && dt.Minute <= 19)
            {
                _dt = true;
            }
            else if (dt.Minute >= 25 && dt.Minute <= 29)
            {
                _dt = true;
            }
            else if (dt.Minute >= 35 && dt.Minute <= 39)
            {
                _dt = true;
            }
            else if (dt.Minute >= 45 && dt.Minute <= 49)
            {
                _dt = true;
            }
            else if (dt.Minute >= 55 && dt.Minute <= 59)
            {
                _dt = true;
            }
            else
            {
                _dt = false;
            }
            return _dt;
        }
        private async Task<int> GetCalendarDayId()
        {
            var calendayDay = await _settingService.GetCarlendarDay();
            return calendayDay.Id;
        }
        private async Task<int> GetCalendarDayIdLater()
        {
            var calendayDay = await _settingService.GetCarlendarDay();
            DateTime calendarDayLater = calendayDay.ToDay.AddDays(-1);
            var calendarDayId =  _settingService.GetCalendarId(calendarDayLater);
            return calendarDayId;
        }
        private async Task<bool> CheckOutRoomAndSummaryRound(MassageRoom massageRoom)
        {
            using (var transaction = await _db.Database.BeginTransactionAsync())
            {
                try
                {
                    TimeSpan timespan = DateTime.Now - (DateTime)massageRoom.CheckInTime;
                    double totalMinute = timespan.TotalMinutes;
                    massageRoom.CheckOutTime = DateTime.Now;
                    massageRoom.TimeMinute = (int)totalMinute;

                    var modMinuteRoom = massageRoom.TimeMinute % massageRoom.Room.RoomType.RoundTime;
                    var deleteMinuteRoom = massageRoom.TimeMinute - massageRoom.Room.RoomType.RoundTime;
                    var roundFactRoom = 1.0;
                    roundFactRoom = Math.Floor((double)massageRoom.TimeMinute / massageRoom.Room.RoomType.RoundTime);
                    var haftRoundTimeRoom = Math.Floor((double)massageRoom.Room.RoomType.RoundTime / 2);

                    if (roundFactRoom >= 1)
                    {
                        if (massageRoom.Room.RoomType.Id == (int)ERoomType.Vip || massageRoom.Room.RoomType.Id == (int)ERoomType.Vip2 || massageRoom.Room.RoomType.Id == (int)ERoomType.Vip3)
                        {
                            if (modMinuteRoom > 15 && modMinuteRoom <= haftRoundTimeRoom)
                            {
                                roundFactRoom += 0.5;
                            }
                            else if (modMinuteRoom > haftRoundTimeRoom && modMinuteRoom < massageRoom.Room.RoomType.RoundTime)
                            {
                                roundFactRoom += 1;
                            }
                        }
                        else
                        {
                            roundFactRoom = Math.Floor((double)deleteMinuteRoom / 60) + 1;
                        }
                    }
                    else
                    {
                        roundFactRoom = 1;
                    }

                    massageRoom.Round = (decimal)roundFactRoom;

                    await _massageService.UpdateMassageRoom(massageRoom);

                    var room = await _roomService.GetByIdAsync(massageRoom.RoomId);
                    if (room != null)
                    {
                        room.Status = (int)ERoomStatus.Ready;
                        await _roomService.UpdateAsync(room);
                    }

                    await transaction.CommitAsync();
                    return true;
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex.Message);
                    _logger.LogError(ex.StackTrace);
                    await transaction.RollbackAsync();
                    return false;
                }

            }
        }
        private async Task<bool> CheckInNewRoom(int roomIdNew, int massageId, int receptionId, bool isNotCall, int roomIdOld)
        {
            try
            {
                DateTime dateNow = RoundUp(DateTime.Now, TimeSpan.FromMinutes(5));
                MassageRoom massageRoom = new MassageRoom();
                massageRoom.MassageId = massageId;
                massageRoom.RoomId = roomIdNew;
                massageRoom.ReceptionId = receptionId;
                massageRoom.CheckInTime = dateNow;
                massageRoom.CheckOutTime = null;
                massageRoom.OldCheckInTime = dateNow;
                massageRoom.TimeMinute = 0;
                massageRoom.LastCallTime = null;
                massageRoom.CallCount = 0;
                massageRoom.IsNotCall = isNotCall;

                await _massageService.CheckInMassageRoom(massageRoom);

                var roomNew = await _roomService.GetByIdAsync(roomIdNew);
                roomNew.Status = (int)ERoomStatus.Working;
                await _roomService.UpdateAsync(roomNew);

                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return false;
            }
        }
        private async Task<bool> UpdateRoomInMassageAngel(MassageAngel massageAngel, int roomIdNew, bool isChangeRoom)
        {
            try
            {
                int roomOld = massageAngel.RoomId;

                massageAngel.RoomId = roomIdNew;
                if (isChangeRoom)
                {
                    massageAngel.IsChangeRoom = true;
                }
                await _massageService.UpdateMassageAngel(massageAngel);

                var roomNew = await _roomService.GetByIdAsync(roomIdNew);
                roomNew.Status = (int)ERoomStatus.Working;
                await _roomService.UpdateAsync(roomNew);


                var massageAngels = await _massageService.GetMassageAngelByMassageIdAndRoomId(massageAngel.MassageId, roomOld);
                if (massageAngels.Count == 0)
                {
                    var room = await _roomService.GetByIdAsync(roomOld);
                    room.Status = (int)ERoomStatus.Ready;
                    await _roomService.UpdateAsync(room);
                }

                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return false;
            }
        }
        private async Task<Massage> CreateMassageNew(int calenDarDayId, string documentNumber)
        {
            try
            {
                Massage massage = new Massage
                {
                    CalendarDayId = (int)calenDarDayId,
                    IsPaid = false,
                    IsUnPaid = false,
                    Total = 0,
                    AngelTotal = 0,
                    FoodTotal = 0,
                    RoomTotal = 0,
                    QrCodeTotal = 0,
                    IsCashAngel = false,
                    IsCreditAngel = false,
                    IsMemberAngel = false,
                    IsQrCodeAngel = false,
                    CashAngelTotal = 0,
                    CreditAngelTotal = 0,
                    MemberAngelTotal = 0,
                    QrCodeAngelTotal = 0,
                    IsCashFood = false,
                    IsCreditFood = false,
                    IsMemberFood = false,
                    IsQrCodeFood = false,
                    CashFoodTotal = 0,
                    CreditFoodTotal = 0,
                    MemberFoodTotal = 0,
                    QrCodeFoodTotal = 0,
                    IsCashRoom = false,
                    IsCreditRoom = false,
                    IsMemberRoom = false,
                    IsQrCodeRoom = false,
                    CashRoomTotal = 0,
                    CreditRoomTotal = 0,
                    MemberRoomTotal = 0,
                    QrCodeRoomTotal = 0,
                    IsEntertainAngel = false,
                    EntertainTotalAngel = 0,
                    EntertainRemarkAngel = "",
                    IsEntertainFood = false,
                    EntertainTotalFood = 0,
                    EntertainRemarkFood = "",
                    PayDate = null,
                    DocumentNumber = documentNumber,
                    TipQrCodeTotal = 0,
                };
                await _massageService.CreateAsync(massage);

                return (massage);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
            }
            return (null);
        }
    }
}
