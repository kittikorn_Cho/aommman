﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using OONApi.Contracts.ApiRoutes;
using OONApi.Contracts.Requests;
using OONApi.Contracts.Requests.Queires;
using OONApi.Contracts.Responses;
using OONApi.Models;
using OONApi.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OONApi.Controllers
{
    [ApiController]
    [Authorize]
    public class DebtTypeController : ControllerBase
    {
        private IDebtTypeService _debtTypeService;
        private IMapper _mapper;
        private ILogger<DebtTypeController> _logger;
        public DebtTypeController(IDebtTypeService debtTypeService, IMapper mapper, ILogger<DebtTypeController> logger)
        {
            _debtTypeService = debtTypeService;
            _mapper = mapper;
            _logger = logger;
        }

        [HttpGet(ApiRoutes.DebtTypes.GetAll)]
        public async Task<IActionResult> GetAll([FromQuery] DebtTypeSearchQuery paginationQuery)
        {
            try
            {
                var pagination = _mapper.Map<PaginationFilter>(paginationQuery);
                var debtType = await _debtTypeService.GetAllAsync(paginationQuery, pagination);
                if (debtType == null)
                {
                    return NotFound();
                }

                return Ok(new PagedResponse<DebtTypeResponse>
                {
                    Data = _mapper.Map<List<DebtTypeResponse>>(debtType),
                    PageNumber = pagination.PageNumber,
                    PageSize = pagination.PageSize,
                    Keyword = pagination.Keyword,
                    OrderBy = pagination.OrderBy,
                    TotalRecords = pagination.TotalRecords,
                    OrderDirection = pagination.OrderDirection,
                });
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }

        [HttpGet(ApiRoutes.DebtTypes.Get)]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                var debtType = await _debtTypeService.GetByIdAsync(id);
                if(debtType == null)
                {
                    return NotFound();
                }
                return Ok(_mapper.Map<DebtTypeResponse>(debtType));
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }

        [HttpPost(ApiRoutes.DebtTypes.Create)]
        public async Task<IActionResult> Post([FromBody] DebtTypeRequest request)
        {
            try
            {
                DebtType debtType = new DebtType
                {
                    Name = request.Name
                };
                await _debtTypeService.CreateAsync(debtType);
                return Ok(_mapper.Map<DebtTypeResponse>(debtType));
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }

        [HttpPut(ApiRoutes.DebtTypes.Update)]
        public async Task<IActionResult> Put([FromRoute] int id, [FromBody] DebtTypeRequest request)
        {
            try
            {
                var debtType = await _debtTypeService.GetByIdAsync(id);
                if(debtType == null)
                {
                    return NotFound();
                }
                DebtType update = debtType;
                update.Name = request.Name;
                await _debtTypeService.UpdateAsync(update);
                return Ok(_mapper.Map<DebtTypeResponse>(update));
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }

        [HttpDelete(ApiRoutes.DebtTypes.Delete)]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await _debtTypeService.DeleteAsync(id);
                return Ok();
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }
    }

}
