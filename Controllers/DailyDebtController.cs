﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using OONApi.Contracts.ApiRoutes;
using OONApi.Contracts.Requests;
using OONApi.Contracts.Requests.Queires;
using OONApi.Contracts.Responses;
using OONApi.Models;
using OONApi.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace OONApi.Controllers
{
    [ApiController]
    [Authorize]
    public class DailyDebtController : ControllerBase
    {
        private IDailyDebtServicee _dailyDebtServie;
        private IMapper _mapper;
        private ILogger<DailyDebtController> _logger;
        private readonly AppDbContext _db;
        public DailyDebtController(IDailyDebtServicee dailyDebtServie, IMapper mapper, ILogger<DailyDebtController> logger, AppDbContext db)
        {
            _dailyDebtServie = dailyDebtServie;
            _mapper = mapper;
            _logger = logger;
            _db = db;
        }

        [HttpGet(ApiRoutes.DailyDebts.GetAll)]
        public async Task<IActionResult> GetAll([FromQuery] DailyDebtSearchQuery paginationQuery)
        {
            try
            {
                var pagination = _mapper.Map<PaginationFilter>(paginationQuery);
                var dailyDebt = await _dailyDebtServie.GetAllAsync(paginationQuery, pagination);
                if (dailyDebt == null)
                {
                    return NotFound();
                }
                return Ok(new PagedResponse<DailyDebtResponse>
                {
                    Data = _mapper.Map<List<DailyDebtResponse>>(dailyDebt),
                    PageNumber = pagination.PageNumber,
                    PageSize = pagination.PageSize,
                    Keyword = pagination.Keyword,
                    OrderBy = pagination.OrderBy,
                    TotalRecords = pagination.TotalRecords,
                    OrderDirection = pagination.OrderDirection
                });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }

        [HttpGet(ApiRoutes.DailyDebts.Get)]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                var dailyDebt = await _dailyDebtServie.GetByIdAsync(id);
                if (dailyDebt == null)
                {
                    return NotFound();
                }
                var dailyDebtResponse = _mapper.Map<DailyDebtResponse>(dailyDebt);
                if (dailyDebtResponse == null)
                {
                    return NotFound();
                }
                return Ok(dailyDebtResponse);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }

        [HttpPost(ApiRoutes.DailyDebts.Create)]
        public async Task<IActionResult> Post([FromBody] DailyDebtRequest request)
        {
            try
            {
                DailyDebt dailyDebt = new DailyDebt
                {
                    Name = request.Name,
                    Fee = request.Fee,
                };
                await _dailyDebtServie.CreateAsync(dailyDebt);
                return Ok(_mapper.Map<DailyDebtResponse>(dailyDebt));
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }

        [HttpPut(ApiRoutes.DailyDebts.Update)]
        public async Task<IActionResult> Put([FromRoute]int id, [FromBody] DailyDebtRequest request)
        {
            try
            {
                var dailyDebts = await _dailyDebtServie.GetByIdAsync(id);
                if (dailyDebts == null)
                {
                    return NotFound();
                }
                DailyDebt update = dailyDebts;
                update.Name = request.Name;
                update.Fee = request.Fee;
                await _dailyDebtServie.UpdateAsync(update);
                return Ok(_mapper.Map<DailyDebtResponse>(update));
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }

        }

        [HttpDelete(ApiRoutes.DailyDebts.Delete)]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await _dailyDebtServie.DeleteAsync(id);
                return Ok();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }

        }
    }
}
