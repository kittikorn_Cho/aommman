﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using NLog;
using OONApi.Contracts.ApiRoutes;
using OONApi.Contracts.Requests;
using OONApi.Contracts.Requests.Queires;
using OONApi.Contracts.Responses;
using OONApi.Helpers;
using OONApi.Models;
using OONApi.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace OONApi.Controllers
{
    [ApiController]
    [Authorize]
    public class AngelTypeController : ControllerBase
    {
        private IAngelTypeService _angelTypeService;
        private IMapper _mapper;
        private readonly ILogger<AngelTypeController> _logger;

        public AngelTypeController(IAngelTypeService angelTypeService, IMapper mapper, ILogger<AngelTypeController> logger)
        {
            _angelTypeService = angelTypeService;
            _mapper = mapper;
            _logger = logger;
        }

        [HttpGet(ApiRoutes.AngelTypes.GetAll)]
        public async Task<IActionResult> GetAll([FromQuery] AngelTypesSearchQuery paginationQuery)
        {
            try
            {
                var pagination = _mapper.Map<PaginationFilter>(paginationQuery);
                var angelType = await _angelTypeService.GetAllAsync(paginationQuery, pagination);
                if (angelType == null)
                {
                    return NotFound();
                }
                return Ok(new PagedResponse<AngelTypeResponse>
                {
                    Data = _mapper.Map<List<AngelTypeResponse>>(angelType),
                    PageNumber = pagination.PageNumber,
                    PageSize = pagination.PageSize,
                    Keyword = pagination.Keyword,
                    OrderBy = pagination.OrderBy,
                    TotalRecords = pagination.TotalRecords,
                    OrderDirection = pagination.OrderDirection,
                });
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }

        }

        [HttpGet(ApiRoutes.AngelTypes.Get)]
        public async Task<IActionResult> GetById([FromRoute] int id)
        {
            try
            {
                var angelType = await _angelTypeService.GetByIdAsync(id);
                if (angelType == null)
                {
                    return NotFound();
                }
                return Ok(_mapper.Map<AngelTypeResponse>(angelType));
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }

        }

        [HttpPost(ApiRoutes.AngelTypes.Create)]
        public async Task<IActionResult> Post([FromBody] AngelTypeRequest request)
        {
            try
            {
                AngelType angelType = new AngelType
                {
                    Code = request.Code,
                    Fee = request.Fee,
                    Wage = request.Wage,
                    CreditComm = request.CreditComm,
                    CheerComm = request.CheerComm,
                    RoundTime = request.RoundTime,
                };
                await _angelTypeService.CreateAsync(angelType);
                return Ok(_mapper.Map<AngelTypeResponse>(angelType));
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }

        }

        [HttpPut(ApiRoutes.AngelTypes.Update)]
        public async Task<IActionResult> Put([FromRoute]int id, [FromBody] AngelTypeRequest request)
        {
            try
            {
                var angelTypes = await _angelTypeService.GetByIdAsync(id);
                if (angelTypes == null)
                {
                    return NotFound();
                }
                AngelType update = angelTypes;
                update.Code = request.Code;
                update.Fee = request.Fee;
                update.Wage = request.Wage;
                update.CreditComm = request.CreditComm;
                update.CheerComm = request.CheerComm;
                update.RoundTime = request.RoundTime;
                await _angelTypeService.UpdateAsync(update);
                return Ok(_mapper.Map<AngelTypeResponse>(update));
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }

        }

        [HttpPatch(ApiRoutes.AngelTypes.UpdateOrderAngelType)]
        public async Task<IActionResult> UpdateOrderAngelType([FromRoute] int id, [FromBody] AngelTypeRequest request)
        {
            try
            {
                AngelType angelTypes = await _angelTypeService.GetByIdAsync(id);
                if (angelTypes == null)
                {
                    return NotFound();
                }
                angelTypes.Order = request.Order;
                await _angelTypeService.UpdateAsync(angelTypes);
                return Ok();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }

        [HttpDelete(ApiRoutes.AngelTypes.Delete)]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await _angelTypeService.DeleteAsync(id);
                return Ok();
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }

        }
    }
}
