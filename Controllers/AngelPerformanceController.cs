﻿using AutoMapper;
using DinkToPdf;
using DinkToPdf.Contracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using OONApi.Contracts.ApiRoutes;
using OONApi.Contracts.Requests;
using OONApi.Contracts.Requests.Queires;
using OONApi.Contracts.Responses;
using OONApi.Models;
using OONApi.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading.Tasks;
using static OONApi.Contracts.ApiRoutes.ApiRoutes;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace OONApi.Controllers
{
    [ApiController]
    [Authorize]
    public class AngelPerformanceController : ControllerBase
    {
        private IAngelPerformanceService _angelPerformanceService;
        private IDebtTypeService _debtTypeService;
        private IDeductTypeService _deductTypeService;
        private IMapper _mapper;
        private ILogger<AngelPerformanceController> _logger;
        private IAngelService _angelService;
        private ISettingService _settingService;
        private IAngelTypeService _angelTypeService;
        private IAngelPointTransactionService _angelPointTransactionService;
        private IReceptionPointTransactionService _receptionPointTransactionService;
        private IMassageService _massageService;
        private IReceptionService _receptionService;
        private readonly AppDbContext _db;

        public AngelPerformanceController(IAngelPerformanceService angelPerformanceService, IAngelService angelService, IMapper mapper, ILogger<AngelPerformanceController> logger, AppDbContext db,IDebtTypeService debtTypeService, IDeductTypeService deductTypeService, ISettingService settingService, IAngelTypeService angelTypeService, IAngelPointTransactionService angelPointTransactionService, IReceptionPointTransactionService receptionPointTransactionService, IMassageService massageService, IReceptionService receptionService)
        {
            _angelPerformanceService = angelPerformanceService;
            _angelService = angelService;
            _mapper = mapper;
            _logger = logger;
            _db = db;
            _debtTypeService = debtTypeService;
            _deductTypeService = deductTypeService;
            _settingService = settingService;
            _angelTypeService = angelTypeService;
            _angelPointTransactionService = angelPointTransactionService;
            _receptionPointTransactionService = receptionPointTransactionService;
            _massageService = massageService;
            _receptionService = receptionService;
        }

        [HttpGet(ApiRoutes.AngelPerformances.GetAll)]
        public async Task<IActionResult> GetAll([FromQuery] AngelPerformanceSearchQuery paginationQuery)
        {
            try
            {
                var pagination = _mapper.Map<PaginationFilter>(paginationQuery);
                var angelPerformance = await _angelPerformanceService.GetAllAsync(paginationQuery, pagination);
                if (angelPerformance == null)
                {
                    return NotFound();
                }

                var angelPerformanceResponses = _mapper.Map<List<AngelPerformanceResponse>>(angelPerformance);

                foreach(var _angelPerformance in angelPerformanceResponses)
                {
                    int count = await _angelPerformanceService.GetCountMassageAngel(_angelPerformance.Id);

                    _angelPerformance.MassageAngelCount = count;
                }

                return Ok(new PagedResponse<AngelPerformanceResponse>
                {
                    //Data = _mapper.Map<List<AngelPerformanceResponse>>(angelPerformance),
                    Data = angelPerformanceResponses,
                    PageNumber = pagination.PageNumber,
                    PageSize = pagination.PageSize,
                    Keyword = pagination.Keyword,
                    OrderBy = pagination.OrderBy,
                    TotalRecords = pagination.TotalRecords,
                    OrderDirection = pagination.OrderDirection,
                });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }

        [HttpGet(ApiRoutes.AngelPerformances.Get)]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                var angelPerformance = await _angelPerformanceService.GetByIdAsync(id);
                
                if (angelPerformance == null)
                {
                    return NotFound();
                }
                var angelPerformanceResponse = _mapper.Map<AngelPerformanceResponse>(angelPerformance);
                if (angelPerformanceResponse == null)
                {
                    return NotFound();
                }

                var angelPerformanceDebts = await _angelPerformanceService.GetPerformanceDebtByAngelPerformanceIdAsync(id);

                angelPerformanceResponse.AngelPerformanceDebts = _mapper.Map<List<AngelPerformanceDebtResponse>>(angelPerformanceDebts);

                foreach(var item in angelPerformanceResponse.AngelPerformanceDebts)
                {
                    var debtType = await _debtTypeService.GetByIdAsync(item.DebtTypeId);
                    item.DebtType = _mapper.Map<DebtTypeResponse>(debtType);
                }

                var angelDebts = await _angelService.GetAngelDebtByAngelId(angelPerformance.AngelId);
                if (angelDebts == null)
                {
                    return NotFound();
                }
                angelPerformanceResponse.Angel.AngelDebts = _mapper.Map<List<AngelDebtResponse>>(angelDebts);

                foreach(var item in angelPerformanceResponse.Angel.AngelDebts)
                {
                    var debtType = await _debtTypeService.GetByIdAsync(item.DebtTypeId);
                    item.DebtType = _mapper.Map<DebtTypeResponse>(debtType);
                }

                //1415

                //var massageAngel = await _angelPerformanceService.GetMassageAngelsDetailAsync(angelPerformance.CarlendarDayId, angelPerformance.AngelId);
                //if (massageAngel == null)
                //{
                //    return NotFound();
                //}
                //angelPerformanceResponse.MassageAngels = _mapper.Map<List<MassageAngelResponse>>(massageAngel);

                var setting = await _settingService.GetSettingById(1);

                //if (angelPerformance.CarlendarDayId >= 1417) // Aunz
                //if (angelPerformance.CarlendarDayId >= 1792) // caviar
                if (angelPerformance.CarlendarDayId >= 0 && setting.IsAngelPerformanceCarlendar) // TL && MR
                {
                    var massageAngel = await _angelPerformanceService.GetMassageAngelsDetailAsync(angelPerformance.CarlendarDayId, angelPerformance.AngelId, id);
                    if (massageAngel == null)
                    {
                        return NotFound();
                    }
                    angelPerformanceResponse.MassageAngels = _mapper.Map<List<MassageAngelResponse>>(massageAngel);
                }
                else if (angelPerformance.CarlendarDayId >= 1792 && !setting.IsAngelPerformanceCarlendar) // caviar
                {
                    var massageAngel = await _angelPerformanceService.GetMassageAngelsDetailAsync(angelPerformance.CarlendarDayId, angelPerformance.AngelId, id);
                    if (massageAngel == null)
                    {
                        return NotFound();
                    }
                    angelPerformanceResponse.MassageAngels = _mapper.Map<List<MassageAngelResponse>>(massageAngel);
                }
                else
                {
                    var massageAngel = await _angelPerformanceService.GetMassageAngelsDetailAsync(angelPerformance.CarlendarDayId, angelPerformance.AngelId);
                    if (massageAngel == null)
                    {
                        return NotFound();
                    }
                    angelPerformanceResponse.MassageAngels = _mapper.Map<List<MassageAngelResponse>>(massageAngel);
                }

               

                foreach (var item in angelPerformanceResponse.MassageAngels)
                {
                    var angelDeduct = await _angelPerformanceService.GetAngelPerformanceDeducts(item.Id);
                    if (angelDeduct == null)
                    {
                        return NotFound();
                    }
                    item.AngelPerformanceDeducts = _mapper.Map<List<AngelPerformanceDeductResponse>>(angelDeduct);

                    foreach(var itemDeduct in item.AngelPerformanceDeducts)
                    {
                        var deductType = await _deductTypeService.GetByIdAsync(itemDeduct.DeductTypeId);
                        itemDeduct.DeductType = _mapper.Map<DeductTypeResponse>(deductType);
                    }
                }
                return Ok(angelPerformanceResponse);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }

        }
        [HttpPut(ApiRoutes.AngelPerformances.CancelPayment)]
        public async Task<IActionResult> CancelPayment([FromRoute] int id, [FromBody] AngelPerformanceRequest request)
        {
            using (var transaction = await _db.Database.BeginTransactionAsync())
            {
                try
                {
                    var setting = await _settingService.GetSettingById(1);
                    
                    var angelPerformaceDebt = await _angelPerformanceService.GetPerformanceDebtByAngelPerformanceIdAsync(id);
                    var angelperformance = await _angelPerformanceService.GetByIdAsync(id);
                    if (angelperformance == null)
                    {
                        return NotFound();
                    }
                    if (setting.IsPoint)
                    {
                        foreach (var item in angelPerformaceDebt)
                        {
                            var angelDebt = await _angelService.GetAngelDebtByAngelidAndDebtTypeId(angelperformance.AngelId, item.DebtTypeId);
                            if (angelDebt != null)
                            {
                                var _angelDebt = await _angelService.GetAngelDebtById(angelDebt.Id);

                                _angelDebt.Fee = item.DebtNow;

                                await _angelService.UpdateAngelDebtByPerformance(_angelDebt);
                            }
                            await _angelPerformanceService.DeleteAngelPerformanceDebts(item.Id);
                        }
                        angelperformance.TotalDeduct = request.TotalDeduct;
                        angelperformance.TotalFee = request.TotalFee;
                        angelperformance.TotalReceive = request.TotalReceive;
                        angelperformance.TotalWage = request.TotalWage;
                        angelperformance.TotalDebt = request.TotalDebt;
                        angelperformance.TotalRound = request.TotalRound;
                        angelperformance.TotalDailyDebt = request.TotalDailyDebt;
                        angelperformance.IsPay = false;

                        await _angelPerformanceService.UpdateAsync(angelperformance);


                        var massageAngels = await _massageService.GetMassageAngelByAngelPerformanceIdIsPoint(id);
                        decimal totalPointAngel = 0;
                        foreach(var massageAngel in massageAngels)
                        {
                            var angelPointTransaction = await _angelPointTransactionService.GetAngelPointTransactionByMaasageAngelIdAndAngelId(massageAngel.Id, massageAngel.AngelId);

                            totalPointAngel += angelPointTransaction.Point;

                            await _angelPointTransactionService.DeleteAngelPointTransation(angelPointTransaction.Id);
                        }

                        var angel = await _angelService.GetByIdAsync(angelperformance.AngelId);


                        angel.Point = angel.Point - totalPointAngel;

                        await _angelService.UpdateAsync(angel);


                        //reception
                        decimal totalPointReception = 0;
                        foreach (var _massageAngel in massageAngels)
                        {
                            var receptionPointTransaction = await _receptionPointTransactionService.GetReceptionPointTransactionByMassageAngelIdAndReceptionId(_massageAngel.Id,_massageAngel.ReceptionId);

                            totalPointReception += receptionPointTransaction.Point;

                            await _receptionPointTransactionService.DeleteReceptionPointTransation(receptionPointTransaction.Id);

                            var reception = await _receptionService.GetByIdAsync(_massageAngel.ReceptionId);

                            reception.Point = reception.Point - totalPointReception;

                            await _receptionService.UpdateAsync(reception);

                        }
                    }
                    else 
                    {
                        foreach (var item in angelPerformaceDebt)
                        {
                            var angelDebt = await _angelService.GetAngelDebtByAngelidAndDebtTypeId(angelperformance.AngelId, item.DebtTypeId);
                            if (angelDebt != null)
                            {
                                var _angelDebt = await _angelService.GetAngelDebtById(angelDebt.Id);

                                _angelDebt.Fee = item.DebtNow;

                                await _angelService.UpdateAngelDebtByPerformance(_angelDebt);
                            }
                            await _angelPerformanceService.DeleteAngelPerformanceDebts(item.Id);
                        }
                        angelperformance.TotalDeduct = request.TotalDeduct;
                        angelperformance.TotalFee = request.TotalFee;
                        angelperformance.TotalReceive = request.TotalReceive;
                        angelperformance.TotalWage = request.TotalWage;
                        angelperformance.TotalDebt = request.TotalDebt;
                        angelperformance.TotalRound = request.TotalRound;
                        angelperformance.TotalDailyDebt = request.TotalDailyDebt;
                        angelperformance.IsPay = false;

                        await _angelPerformanceService.UpdateAsync(angelperformance);
                    }

                    //var angelPerformance = await _angelPerformanceService.GetByIdAsync(id);
                    await transaction.CommitAsync();

                    return Ok();
                }
                 catch(Exception ex)
                {
                    _logger.LogError(ex.Message);
                    _logger.LogError(ex.StackTrace);
                    return BadRequest();
                }
            }
        } 

        [HttpPut(ApiRoutes.AngelPerformances.Payment)]
        public async Task<IActionResult> Payment([FromRoute] int id,[FromBody] AngelPerformanceRequest request)
        {
            using (var transaction = await _db.Database.BeginTransactionAsync())
            {
                try
                {
                    foreach(var item in request.angelPerformanceDeducts)
                    {
                        var angelPerformanceDeductId = await _angelPerformanceService.GetAngelPerformanceDeductByAngelPerformanceIdAndDeductTypeId(item.MassageAngelId, item.DeductTypeId);


                        if (angelPerformanceDeductId != null)
                        {

                            var angelPerformanceDeduct = await _angelPerformanceService.GetAngelPerformanceDeductById(angelPerformanceDeductId.Id);

                            angelPerformanceDeduct.DiscountFee = item.DiscountFee;

                            await _angelPerformanceService.UpdateAngelPerformanceDeduct(angelPerformanceDeduct);
                        }
                    }

                    List<AngelPerformanceDebt> angelPerformanceDebts = new List<AngelPerformanceDebt>();
                    foreach (var item in request.angelPerformanceDebts)
                    {
                        AngelPerformanceDebt angelPerformanceDebt = new AngelPerformanceDebt();
                        angelPerformanceDebt.DebtTypeId = item.DebtTypeId;
                        angelPerformanceDebt.AngelPerformanceId = item.AngelPerformanceId;
                        angelPerformanceDebt.Fee = item.Fee;
                        angelPerformanceDebt.DebtNow = item.DebtNow;

                        angelPerformanceDebts.Add(angelPerformanceDebt);

                        await _angelPerformanceService.UpdateAngelDebtByAngelId(angelPerformanceDebt);
                    };
                    await _angelPerformanceService.CreatePerformanceDebtAsync(angelPerformanceDebts);

                    var angelperformance = await _angelPerformanceService.GetByIdAsync(id);
                    if (angelperformance != null)
                    {
                        var setting = await _settingService.GetSettingById(1);
                        //if (angelperformance.CarlendarDayId >= 1792) // caviar
                        if (setting.IsPoint)
                        {
                          
                            if (angelperformance.CarlendarDayId >= 0 && setting.IsAngelPerformanceCarlendar) // TL && MR
                            {
                                var massageAngel = await _angelPerformanceService.GetMassageAngelsDetailAsync(angelperformance.CarlendarDayId, angelperformance.AngelId, id);

                                decimal round = 0;
                               
                                foreach (var a in massageAngel)
                                {
                                    decimal totalAngelPoint = 0;
                                    decimal totalReception = 0;
                                    round += a.Round;

                                    AngelPointTransaction angelPointTransaction = new AngelPointTransaction();

                                    angelPointTransaction.MassageAngelId = a.Id;
                                    angelPointTransaction.AngelId = a.AngelId;
                                    angelPointTransaction.Point = a.Round * setting.PointAngel;

                                    totalAngelPoint += angelPointTransaction.Point;

                                    await _angelPointTransactionService.CreateAngelPointTransaction(angelPointTransaction);
                                    
                                    var angel = await _angelService.GetByIdAsync(a.AngelId);
                                    if (angel != null)
                                    {
                                        angel.Point += totalAngelPoint;

                                        await _angelService.UpdateAsync(angel);
                                    }

                                    ReceptionPointTransaction receptionPointTransaction = new ReceptionPointTransaction();

                                    receptionPointTransaction.MassageAngelId = a.Id;
                                    receptionPointTransaction.ReceptionId = a.ReceptionId;
                                    receptionPointTransaction.Point = a.Round * setting.PointReception;

                                    totalReception += receptionPointTransaction.Point;

                                    await _receptionPointTransactionService.CreateReceptionPointTransaction(receptionPointTransaction);

                                    var reception = await _receptionService.GetByIdAsync(a.ReceptionId);

                                    if(reception != null)
                                    {
                                        reception.Point += totalReception;

                                        await _receptionService.UpdateAsync(reception);
                                    }
                                }

                                angelperformance.TotalFee = round * angelperformance.Angel.AngelType.Fee;
                                angelperformance.TotalWage = round * angelperformance.Angel.AngelType.Wage;
                                angelperformance.TotalRound = round;
                                angelperformance.TotalDeduct = request.TotalDeduct;

                                angelperformance.AnotherDeductAmount = request.AnotherDeductAmount;
                                angelperformance.AnotherDeductDescription = request.AnotherDeductDescription;
                                angelperformance.AnotherAddAmount = request.AnotherAddAmount;
                                angelperformance.AnotherAddDescription = request.AnotherAddDescription;
                                angelperformance.TotalDebt = request.TotalDebt;
                                angelperformance.TotalDailyDebt = request.TotalDailyDebt;
                                angelperformance.TotalReceive = request.TotalReceive;
                                angelperformance.IsPay = true;

                                await _angelPerformanceService.UpdateAsync(angelperformance);


                            }
                            else if (angelperformance.CarlendarDayId >= 1792 && !setting.IsAngelPerformanceCarlendar)// caviar
                            {
                                var massageAngel = await _angelPerformanceService.GetMassageAngelsDetailAsync(angelperformance.CarlendarDayId, angelperformance.AngelId, id);

                                decimal round = 0;

                                foreach (var a in massageAngel)
                                {
                                    decimal totalAngelPoint = 0;
                                    decimal totalReception = 0;
                                    round += a.Round;
                                    AngelPointTransaction angelPointTransaction = new AngelPointTransaction();

                                    angelPointTransaction.MassageAngelId = a.Id;
                                    angelPointTransaction.AngelId = a.AngelId;
                                    angelPointTransaction.Point = a.Round * setting.PointAngel;

                                    totalAngelPoint += angelPointTransaction.Point;

                                    await _angelPointTransactionService.CreateAngelPointTransaction(angelPointTransaction);

                                    var angel = await _angelService.GetByIdAsync(a.AngelId);

                                    if (angel != null)
                                    {
                                        angel.Point = totalAngelPoint;

                                        await _angelService.UpdateAsync(angel);
                                    }


                                    ReceptionPointTransaction receptionPointTransaction = new ReceptionPointTransaction();

                                    receptionPointTransaction.MassageAngelId = a.Id;
                                    receptionPointTransaction.ReceptionId = a.ReceptionId;
                                    receptionPointTransaction.Point = a.Round * setting.PointReception;

                                    totalReception += receptionPointTransaction.Point;

                                    await _receptionPointTransactionService.CreateReceptionPointTransaction(receptionPointTransaction);

                                    var reception = await _receptionService.GetByIdAsync(a.ReceptionId);

                                    if (reception != null)
                                    {
                                        reception.Point = totalReception;

                                        await _receptionService.UpdateAsync(reception);
                                    }
                                }

                                angelperformance.TotalFee = round * angelperformance.Angel.AngelType.Fee;
                                angelperformance.TotalWage = round * angelperformance.Angel.AngelType.Wage;
                                angelperformance.TotalRound = round;
                                angelperformance.TotalDeduct = request.TotalDeduct;

                                angelperformance.AnotherDeductAmount = request.AnotherDeductAmount;
                                angelperformance.AnotherDeductDescription = request.AnotherDeductDescription;
                                angelperformance.AnotherAddAmount = request.AnotherAddAmount;
                                angelperformance.AnotherAddDescription = request.AnotherAddDescription;
                                angelperformance.TotalDebt = request.TotalDebt;
                                angelperformance.TotalDailyDebt = request.TotalDailyDebt;
                                angelperformance.TotalReceive = request.TotalReceive;
                                angelperformance.IsPay = true;

                                await _angelPerformanceService.UpdateAsync(angelperformance);
                            }
                            else
                            {
                                var massageAngel = await _angelPerformanceService.GetMassageAngelsDetailAsync(angelperformance.CarlendarDayId, angelperformance.AngelId);

                                decimal round = 0;

                                foreach (var a in massageAngel)
                                {
                                    decimal totalAngelPoint = 0;
                                    decimal totalReception = 0;
                                    round += a.Round;
                                    AngelPointTransaction angelPointTransaction = new AngelPointTransaction();

                                    angelPointTransaction.MassageAngelId = a.Id;
                                    angelPointTransaction.AngelId = a.AngelId;
                                    angelPointTransaction.Point = a.Round * setting.PointAngel;

                                    totalAngelPoint += angelPointTransaction.Point;

                                    await _angelPointTransactionService.CreateAngelPointTransaction(angelPointTransaction);

                                    var angel = await _angelService.GetByIdAsync(a.AngelId);

                                    if (angel != null)
                                    {
                                        angel.Point = totalAngelPoint;

                                        await _angelService.UpdateAsync(angel);
                                    }


                                    ReceptionPointTransaction receptionPointTransaction = new ReceptionPointTransaction();

                                    receptionPointTransaction.MassageAngelId = a.Id;
                                    receptionPointTransaction.ReceptionId = a.ReceptionId;
                                    receptionPointTransaction.Point = a.Round * setting.PointReception;

                                    totalReception += receptionPointTransaction.Point;

                                    await _receptionPointTransactionService.CreateReceptionPointTransaction(receptionPointTransaction);

                                    var reception = await _receptionService.GetByIdAsync(a.ReceptionId);

                                    if (reception != null)
                                    {
                                        reception.Point = totalReception;

                                        await _receptionService.UpdateAsync(reception);
                                    }
                                }

                                angelperformance.TotalFee = round * angelperformance.Angel.AngelType.Fee;
                                angelperformance.TotalWage = round * angelperformance.Angel.AngelType.Wage;
                                angelperformance.TotalRound = round;
                                angelperformance.TotalDeduct = request.TotalDeduct;

                                angelperformance.AnotherDeductAmount = request.AnotherDeductAmount;
                                angelperformance.AnotherDeductDescription = request.AnotherDeductDescription;
                                angelperformance.AnotherAddAmount = request.AnotherAddAmount;
                                angelperformance.AnotherAddDescription = request.AnotherAddDescription;
                                angelperformance.TotalDebt = request.TotalDebt;
                                angelperformance.TotalDailyDebt = request.TotalDailyDebt;
                                angelperformance.TotalReceive = request.TotalReceive;
                                angelperformance.IsPay = true;

                                await _angelPerformanceService.UpdateAsync(angelperformance);
                            }
                        }
                        else 
                        {
                            if (angelperformance.CarlendarDayId >= 0 && setting.IsAngelPerformanceCarlendar) // TL && MR
                            {
                                var massageAngel = await _angelPerformanceService.GetMassageAngelsDetailAsync(angelperformance.CarlendarDayId, angelperformance.AngelId, id);

                                decimal round = 0;
                                foreach (var a in massageAngel)
                                {
                                    round += a.Round;
                                }

                                angelperformance.TotalFee = round * angelperformance.Angel.AngelType.Fee;
                                angelperformance.TotalWage = round * angelperformance.Angel.AngelType.Wage;
                                angelperformance.TotalRound = round;
                                angelperformance.TotalDeduct = request.TotalDeduct;

                                angelperformance.AnotherDeductAmount = request.AnotherDeductAmount;
                                angelperformance.AnotherDeductDescription = request.AnotherDeductDescription;
                                angelperformance.AnotherAddAmount = request.AnotherAddAmount;
                                angelperformance.AnotherAddDescription = request.AnotherAddDescription;
                                angelperformance.TotalDebt = request.TotalDebt;
                                angelperformance.TotalDailyDebt = request.TotalDailyDebt;
                                angelperformance.TotalReceive = request.TotalReceive;
                                angelperformance.IsPay = true;

                                await _angelPerformanceService.UpdateAsync(angelperformance);


                            }
                            else if (angelperformance.CarlendarDayId >= 1792 && !setting.IsAngelPerformanceCarlendar)// caviar
                            {
                                var massageAngel = await _angelPerformanceService.GetMassageAngelsDetailAsync(angelperformance.CarlendarDayId, angelperformance.AngelId, id);

                                decimal round = 0;

                                foreach (var a in massageAngel)
                                {
                                    round += a.Round;
                                }

                                angelperformance.TotalFee = round * angelperformance.Angel.AngelType.Fee;
                                angelperformance.TotalWage = round * angelperformance.Angel.AngelType.Wage;
                                angelperformance.TotalRound = round;
                                angelperformance.TotalDeduct = request.TotalDeduct;

                                angelperformance.AnotherDeductAmount = request.AnotherDeductAmount;
                                angelperformance.AnotherDeductDescription = request.AnotherDeductDescription;
                                angelperformance.AnotherAddAmount = request.AnotherAddAmount;
                                angelperformance.AnotherAddDescription = request.AnotherAddDescription;
                                angelperformance.TotalDebt = request.TotalDebt;
                                angelperformance.TotalDailyDebt = request.TotalDailyDebt;
                                angelperformance.TotalReceive = request.TotalReceive;
                                angelperformance.IsPay = true;

                                await _angelPerformanceService.UpdateAsync(angelperformance);
                            }
                            else
                            {
                                var massageAngel = await _angelPerformanceService.GetMassageAngelsDetailAsync(angelperformance.CarlendarDayId, angelperformance.AngelId);

                                decimal round = 0;

                                foreach (var a in massageAngel)
                                {
                                    round += a.Round;
                                }

                                angelperformance.TotalFee = round * angelperformance.Angel.AngelType.Fee;
                                angelperformance.TotalWage = round * angelperformance.Angel.AngelType.Wage;
                                angelperformance.TotalRound = round;
                                angelperformance.TotalDeduct = request.TotalDeduct;

                                angelperformance.AnotherDeductAmount = request.AnotherDeductAmount;
                                angelperformance.AnotherDeductDescription = request.AnotherDeductDescription;
                                angelperformance.AnotherAddAmount = request.AnotherAddAmount;
                                angelperformance.AnotherAddDescription = request.AnotherAddDescription;
                                angelperformance.TotalDebt = request.TotalDebt;
                                angelperformance.TotalDailyDebt = request.TotalDailyDebt;
                                angelperformance.TotalReceive = request.TotalReceive;
                                angelperformance.IsPay = true;

                                await _angelPerformanceService.UpdateAsync(angelperformance);
                            }
                        }
                      
                    }
                   
                    await transaction.CommitAsync();


                    var response = await Get(id);

                    return Ok(response);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex.Message);
                    _logger.LogError(ex.StackTrace);
                    await transaction.RollbackAsync();
                    return BadRequest();
                }
            }
        }

        [HttpPatch(ApiRoutes.AngelPerformances.CancelIsPayPerformance)]
        public async Task<IActionResult> CancelIsPayPerformance([FromBody] UpdateAngelPerformanceRequest request)
        {

            using (var transaction = await _db.Database.BeginTransactionAsync())
            {
                try
                {
                    var angelPerformance = await _angelPerformanceService.GetByIdAsync(request.AngelPerformanceId);
                    if (angelPerformance == null)
                    {
                        return NotFound();
                    }
                    angelPerformance.IsPay = request.IsPay;
                    await _angelPerformanceService.UpdateAsync(angelPerformance);
                    await transaction.CommitAsync();
                    return Ok();
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex.Message);
                    _logger.LogError(ex.StackTrace);
                    await transaction.RollbackAsync();
                    return BadRequest();
                }
            }
        }

        [HttpDelete(ApiRoutes.AngelPerformances.Delete)]
        public async Task<IActionResult> DeleteAngelPerformance(int id)
        {
            try
            {
                await _angelPerformanceService.DeleteAsync(id);

                return Ok();
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);

                return BadRequest();
            }
        }

        private async Task<int> GetCalendarDayId()
        {
            var calendayDay = await _settingService.GetCarlendarDay();
            return calendayDay.Id;
        }
    }

}
