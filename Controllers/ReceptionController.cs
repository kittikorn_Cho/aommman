﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using OONApi.Contracts.ApiRoutes;
using OONApi.Contracts.Requests;
using OONApi.Contracts.Requests.Queires;
using OONApi.Contracts.Responses;
using OONApi.Enum;
using OONApi.Models;
using OONApi.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static OONApi.Contracts.ApiRoutes.ApiRoutes;

namespace OONApi.Controllers
{
    [ApiController]
    [Authorize]
    public class ReceptionController : ControllerBase
    {
        private IReceptionService _receptionService;
        private IMapper _mapper;
        private ILogger<ReceptionController> _logger;
        private IMassageService _massageService;
        private ISettingService _settingService;
        private IAngelTypeService _angelTypeService;
        private IReceptionPointRedemtionsService _receptionPointRedemtionsService;
        private readonly AppDbContext _db;
        public ReceptionController(IReceptionService receptionService, IMapper mapper, ILogger<ReceptionController> logger, IMassageService massageService, ISettingService settingService,IAngelTypeService angelTypeService, AppDbContext db, IReceptionPointRedemtionsService receptionPointRedemtionsService)
        {
            _receptionService = receptionService;
            _mapper = mapper;
            _logger = logger;
            _massageService = massageService;
            _settingService = settingService;
            _angelTypeService = angelTypeService;
            _db = db;
            _receptionPointRedemtionsService = receptionPointRedemtionsService;
        }

        [HttpGet(ApiRoutes.Receptions.GetAll)]
        public async Task<IActionResult> GetAll([FromQuery] ReceptionSearchQuery paginationQuery)
        {
            try
            {
                var pagination = _mapper.Map<PaginationFilter>(paginationQuery);
                var angel = await _receptionService.GetAllAsync(paginationQuery, pagination);
                if (angel == null)
                {
                    return NotFound();
                }
                return Ok(new PagedResponse<ReceptionResponse>
                {
                    Data = _mapper.Map<List<ReceptionResponse>>(angel.OrderBy(a => a.Code)),
                    PageNumber = pagination.PageNumber,
                    PageSize = pagination.PageSize,
                    Keyword = pagination.Keyword,
                    OrderBy = pagination.OrderBy,
                    TotalRecords = pagination.TotalRecords,
                    OrderDirection = pagination.OrderDirection,
                });
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }

        }

        [HttpGet(ApiRoutes.Receptions.Get)]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                var reception = await _receptionService.GetByIdAsync(id);
                if (reception == null)
                {
                    return NotFound();
                }
                return Ok(_mapper.Map<ReceptionResponse>(reception));
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }

        }

        [HttpGet(ApiRoutes.Receptions.Sell)]
        public async Task<IActionResult> GetSell([FromQuery] ReceptionSearchQuery request)
        {
            try
            {
                var pagination = _mapper.Map<PaginationFilter>(request);
                var receptions = await _receptionService.GetAllAsync(request, pagination);

                var responseReception = _mapper.Map<List<ReceptionSellResponse>>(receptions.OrderBy(r=>r.Code));

                var angelTypes = await _angelTypeService.GetAllAsync(null);

                List<int> calendarIds = await _settingService.GetCalendarId(request.StartDate, request.EndDate);

                foreach (var reception in responseReception)
                {
                    reception.angelTypes = _mapper.Map<List<AngelTypeResponse>>(angelTypes);

                    reception.TotalSuiteCount = _receptionService.GetSellRoomTypeSuite(reception.Id, calendarIds);

                    reception.TotalMemberSell = _receptionService.GetSellMember(reception.Id, calendarIds);

                    reception.TotalAngelRound = 0;

                    foreach(var angelType in reception.angelTypes)
                    {
                        var round = _receptionService.GetSellAngelTypes(reception.Id, angelType.Id, calendarIds);
                        angelType.totalRound = round;

                        reception.TotalAngelRound += round;
                    }
                }

                return Ok(responseReception);

            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }
    
        [HttpPost(ApiRoutes.Receptions.Create)]
        public async Task<IActionResult> Post([FromBody] ReceptionRequest request)
        {
            try
            {
                Reception reception = new Reception
                {
                    Code = request.Code,
                    Firstname = request.Firstname,
                    Lastname = request.Lastname,
                    Nickname = request.Nickname,
                    Tel = request.Tel,
                    Address = request.Address,
                    WorkFromDate = DateTime.Now,
                    WorkToDate = null
                };
                await _receptionService.CreateAsync(reception);
                return Ok(_mapper.Map<ReceptionResponse>(reception));
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }

        }

        [HttpPut(ApiRoutes.Receptions.Update)]
        public async Task<IActionResult> Put([FromRoute] int id, [FromBody] ReceptionRequest request)
        {
            try
            {
                var reception = await _receptionService.GetByIdAsync(id);
                if (reception == null)
                {
                    return NotFound();
                }
                Reception update = reception;
                update.Code = request.Code;
                update.Firstname = request.Firstname;
                update.Lastname = request.Lastname;
                update.Nickname = request.Nickname;
                update.Tel = request.Tel;
                update.Address = request.Address;
                await _receptionService.UpdateAsync(update);
                return Ok(_mapper.Map<ReceptionResponse>(update));
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }

        }

        [HttpPut(ApiRoutes.Receptions.Delete)]
        public async Task<IActionResult> Delete(int id)
        {
            using (var transaction = await _db.Database.BeginTransactionAsync())
            {
                try
                {
                    //await _receptionService.DeleteAsync(id);
                    Reception reception = await _receptionService.GetByIdAsync(id);
                    if(reception == null)
                    {
                        return NotFound();
                    }
                    reception.Status = (int)EAngelStatus.Cancel;
                    await _receptionService.UpdateAsync(reception);
                    _db.ChangeTracker.Clear();

                    await transaction.CommitAsync();
                    return Ok();
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex.Message);
                    _logger.LogError(ex.StackTrace);
                    return BadRequest();
                }
            }
        }

        [HttpPost(ApiRoutes.Receptions.CreateReceptionPointRedemtions)]
        public async Task<IActionResult> CreateReceptionPointRedemtions([FromBody] ReceptionPointRedemtionRequest request) 
        {
            using (var transaction = await _db.Database.BeginTransactionAsync())
            {
                try
                {
                    int? calendarDayId = await GetCalendarDayId();
                    var reception = await _receptionService.GetByIdAsync(request.ReceptionId);
                    ReceptionPointRedemtion receptionPointRedemtion = new ReceptionPointRedemtion();
                    receptionPointRedemtion.ReceptionId = reception.Id;
                    receptionPointRedemtion.CalendarDayId = (int)calendarDayId;
                    receptionPointRedemtion.Point = reception.Point;
                    receptionPointRedemtion.PointRedem = request.PointRedem;
                    receptionPointRedemtion.Remark = request.Remark;

                    await _receptionPointRedemtionsService.CreateReceptionPointRedemtions(receptionPointRedemtion);

                    reception.Point -= receptionPointRedemtion.PointRedem;
                    await _receptionService.UpdateAsync(reception);

                    await transaction.CommitAsync();
                    return Ok();
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex.Message);
                    _logger.LogError(ex.StackTrace);
                    await transaction.RollbackAsync();
                    return BadRequest();
                }
            }
        }

        [HttpDelete(ApiRoutes.Receptions.CancelReceptionPointRedemtions)]
        public async Task<IActionResult> CancelReceptionPointRedemtions(int id)
        {
            try
            {
                var receptionPointRedemtion = await _receptionPointRedemtionsService.GetReceptionPointRedemtionByReceptionId(id);
                if (receptionPointRedemtion == null) 
                {
                    return NotFound();
                }
                var reception = await _receptionService.GetByIdAsync(receptionPointRedemtion.ReceptionId);

                reception.Point += receptionPointRedemtion.PointRedem;

                await _receptionPointRedemtionsService.CancelReceptionPointRedemtions(receptionPointRedemtion.Id);
                await _receptionService.UpdateAsync(reception);
                return Ok();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }

        }
        [HttpGet(ApiRoutes.Receptions.GetReceptionPointRedemtionsHistory)]
        public async Task<IActionResult> GetReceptionPointRedemtionsHistory([FromQuery] ReceptionPointRedemtionsHistorySearchQuery paginationQuery) 
        {
            try
            {
                var pagination = _mapper.Map<PaginationFilter>(paginationQuery);
                var receptionPointRedemtionsHistory = await _receptionPointRedemtionsService.GetReceptionPointRedemtionsHistory(paginationQuery, pagination);
                if (receptionPointRedemtionsHistory == null)
                {
                    return NotFound();
                }
                return Ok(new PagedResponse<ReceptionPointRedemtion>
                {
                    Data = _mapper.Map<List<ReceptionPointRedemtion>>(receptionPointRedemtionsHistory),
                    PageNumber = pagination.PageNumber,
                    PageSize = pagination.PageSize,
                    Keyword = pagination.Keyword,
                    OrderBy = pagination.OrderBy,
                    TotalRecords = pagination.TotalRecords,
                    OrderDirection = pagination.OrderDirection
                });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }

        [HttpGet(ApiRoutes.Receptions.GetReceptionAllPoint)]
        public async Task<IActionResult> GetReceptionAllPoint([FromQuery] ReceptionSearchQuery paginationQuery)
        {
            try
            {
                var pagination = _mapper.Map<PaginationFilter>(paginationQuery);
                var angel = await _receptionService.GetAllAsync(paginationQuery, pagination);
                if (angel == null)
                {
                    return NotFound();
                }
                return Ok(new PagedResponse<ReceptionResponse>
                {
                    Data = _mapper.Map<List<ReceptionResponse>>(angel.OrderByDescending(a => a.Point)),
                    PageNumber = pagination.PageNumber,
                    PageSize = pagination.PageSize,
                    Keyword = pagination.Keyword,
                    OrderBy = pagination.OrderBy,
                    TotalRecords = pagination.TotalRecords,
                    OrderDirection = pagination.OrderDirection,
                });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }

        }
        private async Task<int> GetCalendarDayId()
        {
            var calendayDay = await _settingService.GetCarlendarDay();
            return calendayDay.Id;
        }
    }
}
