﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using OONApi.Contracts.ApiRoutes;
using OONApi.Contracts.Requests;
using OONApi.Contracts.Requests.Queires;
using OONApi.Contracts.Responses;
using OONApi.Enum;
using OONApi.Models;
using OONApi.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace OONApi.Controllers
{
    [ApiController]
    [Authorize]
    public class RoomController : ControllerBase
    {
        private IRoomService _roomService;
        private IMapper _mapper;
        private ILogger<RoomController> _logger;
        private readonly AppDbContext _db;

        public RoomController(IMapper mapper,IRoomService roomService, ILogger<RoomController> logger, AppDbContext db)
        {
            _roomService = roomService;
            _mapper = mapper;
            _logger = logger;
            _db = db;
        }

        [HttpGet(ApiRoutes.Room.GetAll)]
        public async Task<IActionResult> GetAll([FromQuery] RoomSearchQuery paginationQuery)
        {
            try
            {
                var pagination = _mapper.Map<PaginationFilter>(paginationQuery);
                var room = await _roomService.GetAllAsync(paginationQuery, pagination);
                if (room == null)
                {
                    return NotFound();
                }

                return Ok(new PagedResponse<RoomResponse>
                {
                    Data = _mapper.Map<List<RoomResponse>>(room),
                    PageNumber = pagination.PageNumber,
                    PageSize = pagination.PageSize,
                    Keyword = pagination.Keyword,
                    OrderBy = pagination.OrderBy,
                    TotalRecords = pagination.TotalRecords,
                    OrderDirection = pagination.OrderDirection,
                });
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }


        }

        [HttpGet(ApiRoutes.Room.Get)]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                var room = await _roomService.GetByIdAsync(id);
                if (room == null)
                {
                    return NotFound();
                }

                var childRoom = await _roomService.GetChildRoomParent(id);
                if (childRoom == null)
                {
                    return NotFound();
                }
                return Ok(new
                {
                    Id = room.Id,
                    RoomNo = room.RoomNo,
                    Status = room.Status,
                    BuildingType = room.BuildingType,
                    BuildingTypeId = room.BuildingTypeId,
                    RoomTypeId = room.RoomTypeId,
                    RoomType = room.RoomType,
                    ParentRoomId = room.ParentRoomId,
                    ChildRooms = _mapper.Map<List<RoomParentResponse>>(childRoom)
                });
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }

        }

        [HttpPost(ApiRoutes.Room.Create)]
        public async Task<IActionResult> Post([FromBody] RoomRequest request)
        {
            using (var transaction = await _db.Database.BeginTransactionAsync())
            {
                try
                {
                
                    Room room = new Room
                    {
                        RoomNo = request.RoomNo,
                        RoomTypeId = request.RoomTypeId,
                        Status = request.Status,
                        BuildingTypeId = request.BuildingTypeId
                    };

                    await _roomService.CreateAsync(room);
                    if (room.RoomTypeId == (int)ERoomType.Suite || room.RoomTypeId == (int)ERoomType.Suite3 || room.RoomTypeId == (int)ERoomType.Suite4 || room.RoomTypeId == (int)ERoomType.Suite5 || room.RoomTypeId == (int)ERoomType.Suite6 || room.RoomTypeId == (int)ERoomType.Suite7 || room.RoomTypeId == (int)ERoomType.Suite8 || room.RoomTypeId == (int)ERoomType.Suite9 || room.RoomTypeId == (int)ERoomType.Suite10 || room.RoomTypeId == (int)ERoomType.C3 || room.RoomTypeId == (int)ERoomType.C5)
                    {
                        List<Room> addChildRooms = new List<Room>();
                        foreach (var item in request.ChildRooms) {
                            Room tmp = new Room
                            {
                                ParentRoomId = room.Id,
                                RoomTypeId = (int)ERoomType.SuiteSub,
                                RoomNo = item.RoomNo,
                                Status = item.Status,
                                BuildingTypeId = room.BuildingTypeId
                            };
                            addChildRooms.Add(tmp);
                        }
                        await _roomService.AddChildRooms(addChildRooms);
                    }

                        //_logger.LogDebug("==" + request.ParentRoomId);
                    room.ParentRoomId = request.ParentRoomId;
                    await _roomService.UpdateAsync(room);

                    var parentRooms = new Room();

                    room = await _roomService.GetByIdAsync(room.Id);
                    await transaction.CommitAsync();
                    return Ok(new
                    {
                        Id = room.Id,
                        RoomTypeId = room.RoomTypeId,
                        RoomType = room.RoomType,
                        RoomNo = room.RoomNo,
                        Status = room.Status,
                        BuildingTypeId = room.BuildingTypeId,
                        BuildingType = room.BuildingType,
                        ParentRoomId = room.ParentRoomId,
                        ParentRoom = _mapper.Map<RoomParentResponse>(parentRooms)
                    });
                }
                catch(Exception ex)
                {
                    _logger.LogError(ex.Message);
                    _logger.LogError(ex.StackTrace);
                    return BadRequest();
                }
            }
        }

        [HttpPut(ApiRoutes.Room.Update)]
        public async Task<IActionResult> Update(int id, [FromBody] RoomRequest request)
        {
            using (var transaction = await _db.Database.BeginTransactionAsync())
            {
                try
                {
                    if (id <= 0)
                    {
                        return NotFound();
                    }
                    var room = await _roomService.GetByIdAsync(id);
                    if (room == null)
                    {
                        return NotFound();
                    }
                    room.RoomNo = request.RoomNo;
                    room.RoomTypeId = request.RoomTypeId;
                    room.BuildingTypeId = request.BuildingTypeId;
                    room.Status = request.Status;

                    await _roomService.UpdateAsync(room);

                    //var parentRoom = await _roomService.GetByIdAsync(room.Id);
                    if (room.RoomTypeId == (int)ERoomType.Suite || room.RoomTypeId == (int)ERoomType.Suite3 || room.RoomTypeId == (int)ERoomType.Suite4 || room.RoomTypeId == (int)ERoomType.Suite5 || room.RoomTypeId == (int)ERoomType.Suite6 || room.RoomTypeId == (int)ERoomType.Suite7 || room.RoomTypeId == (int)ERoomType.Suite8 || room.RoomTypeId == (int)ERoomType.Suite9 || room.RoomTypeId == (int)ERoomType.Suite10 || room.RoomTypeId == (int)ERoomType.C3 || room.RoomTypeId == (int)ERoomType.C5)
                    {
                        List<int> childIds = new List<int>();
                        List<Room> addChildRooms = new List<Room>();
                        List<Room> updateChildRooms = new List<Room>();
                        foreach (var item in request.ChildRooms)
                        {
                            if (item.Id != null)
                            {
                                Room tmp = new Room
                                {
                                    Id = (int)item.Id,
                                    ParentRoomId = room.Id,
                                    RoomTypeId = (int)ERoomType.SuiteSub,
                                    RoomNo = item.RoomNo,
                                    Status = item.Status,
                                    BuildingTypeId = room.BuildingTypeId
                                };
                                updateChildRooms.Add(tmp);
                                childIds.Add(tmp.Id);
                            }
                            else
                            {
                                Room tmp = new Room
                                {
                                    ParentRoomId = room.Id,
                                    RoomTypeId = (int)ERoomType.SuiteSub,
                                    RoomNo = item.RoomNo,
                                    Status = item.Status,
                                    BuildingTypeId = room.BuildingTypeId
                                };
                                int createId = await _roomService.AddChildRoom(tmp);
                                childIds.Add(createId);
                            }
                        }

                        if (addChildRooms.Count > 0) await _roomService.AddChildRooms(addChildRooms);
                        if (updateChildRooms.Count > 0) await _roomService.UpdateChildRooms(updateChildRooms);
                        _roomService.DeleteChildRooms(room.Id, childIds);
                    }
                    await transaction.CommitAsync();
                    return Ok(_mapper.Map<RoomResponse>(room));
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex.Message);
                    _logger.LogError(ex.StackTrace);
                    return BadRequest();
                }
            }
        }

        [HttpDelete(ApiRoutes.Room.Delete)]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await _roomService.DeleteAsync(id);
                return Ok();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }

        [HttpGet(ApiRoutes.Room.GetRoomTypeByBuildingTypeId)]
        public async Task<IActionResult> GetRoomByBuildingTypeId([FromQuery] RoomRequest request)
        {
            try
            {
                var room = await _roomService.GetRoomTypeByBuildingTypeIdAsync(request.BuildingTypeId, request.RoomTypeId);

                var rooms = _mapper.Map<List<RoomResponse>>(room);

                foreach(var item in rooms)
                {
                    var parentRooms = await _roomService.GetChildRoomParent(item.Id);
                    item.ParentRooms = _mapper.Map<List<RoomParentResponse>>(parentRooms);

                    if ((int)item.RoomTypeId == (int)ERoomType.SuiteSub)
                    {
                        var parentRoom = await _roomService.GetByIdAsync((int)item.ParentRoomId);
                        item.ParentRoom = _mapper.Map<RoomParentResponse>(parentRoom);
                    }
                }
                

                return Ok(rooms);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }
    }
}
