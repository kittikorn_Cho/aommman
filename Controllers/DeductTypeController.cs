﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using OONApi.Contracts.ApiRoutes;
using OONApi.Contracts.Requests;
using OONApi.Contracts.Requests.Queires;
using OONApi.Contracts.Responses;
using OONApi.Models;
using OONApi.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OONApi.Controllers
{
    [ApiController]
    [Authorize]
    public class DeductTypeController : ControllerBase
    {
        private IDeductTypeService _deductTypeService;
        private IMapper _mapper;
        private ILogger<DeductTypeController> _logger;
        public DeductTypeController(IDeductTypeService deductTypeService, IMapper mapper, ILogger<DeductTypeController> logger)
        {
            _deductTypeService = deductTypeService;
            _mapper = mapper;
            _logger = logger;
        }
        
        [HttpGet(ApiRoutes.DeductTypes.GetAll)]
        public async Task<IActionResult> GetAll([FromQuery] DeductTypeSearchQuery pagiantionQuery)
        {
            try
            {
                var pagination = _mapper.Map<PaginationFilter>(pagiantionQuery);
                var deductType = await _deductTypeService.GetAllAsync(pagiantionQuery, pagination);
                if (deductType == null)
                {
                    return NotFound();
                }
                return Ok(new PagedResponse<DeductTypeResponse>
                {
                    Data = _mapper.Map<List<DeductTypeResponse>>(deductType),
                    PageNumber = pagiantionQuery.PageNumber,
                    PageSize = pagiantionQuery.PageSize,
                    Keyword = pagiantionQuery.Keyword,
                    OrderBy = pagiantionQuery.OrderBy,
                    TotalRecords = pagination.TotalRecords,
                    OrderDirection = pagiantionQuery.OrderDirection,
                });
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }

        [HttpGet(ApiRoutes.DeductTypes.Get)]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                var deductType = await _deductTypeService.GetByIdAsync(id);
                if(deductType == null)
                {
                    return NotFound();
                }
                return Ok(_mapper.Map<DeductTypeResponse>(deductType));
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }

        [HttpPost(ApiRoutes.DeductTypes.Create)]
        public async Task<IActionResult> Post([FromBody] DeductTypeRequest request)
        {
            try
            {
                DeductType deductType = new DeductType
                {
                    Name = request.Name,
                    Fee = request.Fee,
                };
                await _deductTypeService.CreateAsync(deductType);
                return Ok(_mapper.Map<DeductTypeResponse>(deductType));
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }

        [HttpPut(ApiRoutes.DeductTypes.Update)]
        public async Task<IActionResult> Put([FromRoute] int id, [FromBody] DeductTypeRequest request)
        {
            try
            {
                var deductType = await _deductTypeService.GetByIdAsync(id);
                if(deductType == null)
                {
                    return NotFound();
                }
                DeductType update = deductType;
                update.Name = request.Name;
                update.Fee = request.Fee;
                await _deductTypeService.UpdateAsync(update);
                return Ok(_mapper.Map<DeductTypeResponse>(update));
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }

        [HttpDelete(ApiRoutes.DeductTypes.Delete)]
        public async Task<IActionResult> Detele(int id)
        {
            try
            {
                await _deductTypeService.DeleteAsync(id);
                return Ok();
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }
    }
}
