﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using OONApi.Contracts.ApiRoutes;
using OONApi.Contracts.Responses;
using OONApi.Services;
using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using OONApi.Contracts.Requests;
using OONApi.Contracts.Requests.Queires;
using System.Linq;
using OONApi.Models;
using static OONApi.Contracts.ApiRoutes.ApiRoutes;
using OONApi.Migrations;
using DocumentFormat.OpenXml.InkML;
using DocumentFormat.OpenXml.Office2016.Excel;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace OONApi.Controllers
{

    [ApiController]
    [Authorize]
    public class AgencyPerformanceController : ControllerBase
    {
        private IAgencyService _agencyService;
        private IMassageService _massageService;
        private IAgencyPerformanceService _agencyPerformanceService;
        private ISettingService _settingService;
        private IMapper _mapper;
        private ILogger<AgencyPerformanceController> _logger;
        private readonly AppDbContext _db;

        public AgencyPerformanceController(IAgencyService agencyService, IMassageService massageService,ISettingService settingService, IAgencyPerformanceService agencyPerformanceService, IMapper mapper, ILogger<AgencyPerformanceController> logger, AppDbContext db)
        {
            _agencyService = agencyService;
            _massageService = massageService;
            _agencyPerformanceService = agencyPerformanceService;
            _settingService = settingService;
            _mapper = mapper;
            _logger = logger;
            _db = db;
        }

        [HttpPost(ApiRoutes.AgencyPerformances.Create)]
        public async Task<IActionResult> CreateAgencyPerformance([FromBody] AgencyPerformanceRequest request) 
        {
            try
            {
                int? calenDarDayId = await GetCalendarDayId();
                var agencies = await _agencyService.GetByIdAsync(request.AgenciesId);
                if (agencies == null)
                {
                    return NotFound();
                }
                var agenciesPerformance = await _agencyService.GetAgenciesPerformanceByCalendarDay((int)calenDarDayId, agencies.Id);

                bool ispayAgency = false;
                int i = 0;
                int countAgency = agenciesPerformance.Count;
                foreach (var item in agenciesPerformance)
                {
                    i++;
                    if (item.IsPay)
                    {
                        if (i == countAgency)
                        {
                            ispayAgency = true;
                        }
                    }
                }

                if (request.AgenciesId != 1)
                {
                    if (countAgency == 0 || ispayAgency)
                    {
                        AgencyPerformance agencyPerformance = new AgencyPerformance
                        {
                            CarlendarDayId = (int)calenDarDayId,
                            AgenciesId = request.AgenciesId,
                            IsPay = false,
                            IsUnPaid = false,
                            TotalMassage = 0,
                            TotalRound = 0,
                            TotalMember = 0,
                            TotalReceive = 0,
                            Remark = request.Remark
                        };
                        await _agencyPerformanceService.CreatePaymentAgencyAsync(agencyPerformance);
                    }
                    else
                    {
                        return BadRequest(new { Message = "เอเจนซี่นี้มีอยู่แล้ว" });
                    }
                }
                else 
                {
                    return BadRequest(new { Message = "กรุณาระบุเอเจนซี่" });
                }
                return Ok();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }

        [HttpPut(ApiRoutes.AgencyPerformances.Payment)]
        public async Task<IActionResult> PaymentAgencies([FromRoute] int id,[FromBody] AgencyPerformanceRequest request)
        {
            using (var transaction = await _db.Database.BeginTransactionAsync())
            {
                try
                {
                    if(id <= 0) 
                    {
                        return BadRequest();
                    }
                   var agencyPerformances =  await _agencyPerformanceService.GetByIdAsync(id);
                   if(agencyPerformances == null) 
                    {
                        NotFound();
                    }
                    agencyPerformances.IsPay = true;
                    agencyPerformances.IsUnPaid = false;
                    agencyPerformances.TotalMassage = request.TotalMassage;
                    agencyPerformances.TotalRound = request.TotalRound;
                    agencyPerformances.TotalMember = request.TotalMember;
                    agencyPerformances.TotalReceive = request.TotalReceive;
                    agencyPerformances.Remark = request.Remark;

                    await _agencyPerformanceService.UpdateAsync(agencyPerformances);
                    await transaction.CommitAsync();

                    var response = _mapper.Map<AgencyPerformanceResponse>(agencyPerformances);
                    var massages = await _massageService.GetMassageByAgenciesId(response.AgenciesId, response.CarlendarDayId, response.Id);
                    response.massage = _mapper.Map<List<MassageResponse>>(massages);
                    return Ok(response);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex.Message);
                    _logger.LogError(ex.StackTrace);
                    await transaction.RollbackAsync();
                    return BadRequest();
                }
            }
        }

        [HttpPut(ApiRoutes.AgencyPerformances.CancelPayment)]
        public async Task<IActionResult> CanelPayment([FromRoute] int id)
        {
                try
                {
                    if (id <= 0) 
                    {
                        return BadRequest();
                    }
                    var agencyPerformances = await _agencyPerformanceService.GetByIdAsync(id);
                    agencyPerformances.IsPay = false;
                    agencyPerformances.IsUnPaid = false;
                    agencyPerformances.TotalMassage = 0;
                    agencyPerformances.TotalRound = 0;
                    agencyPerformances.TotalMember = 0;
                    agencyPerformances.TotalReceive = 0;
                    agencyPerformances.Remark = "";

                    await _agencyPerformanceService.UpdateAsync(agencyPerformances);

                    return Ok();
                }
                catch (Exception ex) 
                {
                    _logger.LogError(ex.Message);
                    _logger.LogError(ex.StackTrace);
                    return BadRequest();
                }
        }

        [HttpGet(ApiRoutes.AgencyPerformances.GetAllAgenciesPerformancAsync)]
        public async Task<IActionResult> GetAllAgencyPerformances([FromQuery] AgencyPerformanceSearchQuery paginationQuery)
        {
            try
            {
                var pagination = _mapper.Map<PaginationFilter>(paginationQuery);
                var agencyPerformance = await _agencyPerformanceService.GetAllAgencyPerformances(paginationQuery, pagination);
                if (agencyPerformance == null)
                {
                    return NotFound();
                }

                return Ok(new PagedResponse<AgencyPerformanceResponse>
                {
                    Data = _mapper.Map<List<AgencyPerformanceResponse>>(agencyPerformance),
                    PageNumber = pagination.PageNumber,
                    PageSize = pagination.PageSize,
                    Keyword = pagination.Keyword,
                    OrderBy = pagination.OrderBy,
                    TotalRecords = pagination.TotalRecords,
                    OrderDirection = pagination.OrderDirection,
                });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }

        [HttpGet(ApiRoutes.AgencyPerformances.Get)]
        public async Task<IActionResult> GetByIdAsync(int id)
        {
            try
            {
                var agencyPerformancesId = await _agencyPerformanceService.GetByIdAsync(id);
                if (agencyPerformancesId == null)
                {
                    return NotFound();
                }
                var response = _mapper.Map<AgencyPerformanceResponse>(agencyPerformancesId);
                var massages = await _massageService.GetMassageByAgenciesId(response.AgenciesId, response.CarlendarDayId, response.Id);
                response.massage =  _mapper.Map<List<MassageResponse>>(massages);
            
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }


        private async Task<int> GetCalendarDayId()
        {
            var calendayDay = await _settingService.GetCarlendarDay();
            return calendayDay.Id;
        }
    }
}
