﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using OONApi.Contracts.ApiRoutes;
using OONApi.Contracts.Requests;
using OONApi.Contracts.Responses;
using OONApi.Enum;
using OONApi.Models;
using OONApi.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace OONApi.Controllers
{
    [ApiController]
    public class CalendarDayController : ControllerBase
    {
        private ILogger<CalendarDayController> _logger;
        private ISettingService _settingService;
        private IAngelTypeService _angelTypeService;
        private IMapper _mapper;
        private readonly AppDbContext _db;

        public CalendarDayController(ISettingService settingService,IMapper mapper, ILogger<CalendarDayController> logger, IAngelTypeService angelTypeService, AppDbContext db)
        {
            _settingService = settingService;
            _mapper = mapper;
            _logger = logger;
            _angelTypeService = angelTypeService;
            _db = db;
        }

        [HttpPost(ApiRoutes.CalendarDays.CreateSetting)]
        public async Task<IActionResult> PostSetting([FromBody] SettingRequest request)
        {

            using (var transaction = await _db.Database.BeginTransactionAsync())
            {
                try
                {
                    Setting setting = new Setting
                    {
                        MassageName = request.MassageName,
                        MassagePath = request.MassagePath
                    };

                    await _settingService.CreateSetting(setting);

                    await transaction.CommitAsync();

                    var _setting = await _settingService.GetSettingById(setting.Id);

                    var response = _mapper.Map<SettingResponse>(_setting);

                    return Ok(response);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex.Message);
                    _logger.LogError(ex.StackTrace);
                    await transaction.RollbackAsync();
                    return BadRequest();
                }

            }
        }

        [HttpPut(ApiRoutes.CalendarDays.UpdateSetting)]
        public async Task<IActionResult> PutSetting([FromRoute] int id, [FromBody] SettingRequest request)
        {
            using (var transaction = await _db.Database.BeginTransactionAsync())
            {
                try
                {
                    var setting = await _settingService.GetSettingById(id);
                    if(setting == null)
                    {
                        return BadRequest();
                    }

                    setting.MassageName = request.MassageName;
                    setting.MassagePath = request.MassagePath;

                    await _settingService.UpdateSetting(setting);

                    await transaction.CommitAsync();

                    var _setting = await _settingService.GetSettingById(setting.Id);

                    var response = _mapper.Map<SettingResponse>(_setting);

                    return Ok(response);
                }
                catch(Exception ex)
                {
                    _logger.LogError(ex.Message);
                    _logger.LogError(ex.StackTrace);
                    await transaction.RollbackAsync();
                    return BadRequest();
                }
            }
        }

        [HttpGet(ApiRoutes.CalendarDays.GetSettingById)]
        public async Task<IActionResult> GetSettingById(int id)
        {
            try
            {
                var setting = await _settingService.GetSettingById(id);
                if (setting == null)
                {
                    return NotFound();
                }

                var response = _mapper.Map<SettingResponse>(setting);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }

        }

        [HttpGet(ApiRoutes.CalendarDays.GetBetweenDate)]
        public async Task<IActionResult> GetBetweenDate([FromQuery] StartDateEndDateRequestcs request)
        {
            try
            {
                var calendarDays = await _settingService.GetCalendarDayStartDateAndEndDate(request.StartDate, request.EndDate);
                var response = _mapper.Map<List<CalendarDayResponse>>(calendarDays);

                return Ok(response);
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }
        [HttpGet(ApiRoutes.CalendarDays.GetDashboard)]
        public async Task<IActionResult> GetDashboard(DateTime? calendarDay)
        {
            try
            {
                //
                int calendarDayId = 0;
                if (calendarDay != null)
                {
                    calendarDayId = _settingService.GetCalendarId((DateTime)calendarDay);
                    if (calendarDayId == 0)
                    {
                        return NotFound();
                    }
                }
                else
                {
                    calendarDayId = await GetCalendarDayId();
                }

                decimal totalAngel = await _settingService.GetCountAngelWorkingByCalendarDayId(calendarDayId);
                decimal totalMassage = 0;
                decimal totalFood = 0;
                decimal totalRound = 0;
                decimal totalAddRound = 0;
                var massageResponse = await _settingService.GetMassageAngelByCalendarDayId(calendarDayId);
                if (massageResponse == null)
                {
                    return NotFound();
                }
                foreach (var item in massageResponse)
                {
                    totalRound += item.Round;
                    totalMassage += item.Massage.Total;
                    totalFood += item.Massage.FoodTotal;
                    totalAddRound += item.AddRound;
                }
                decimal countMassageRoom = await _settingService.GetMassageRoomByCalendarDayId(calendarDayId, (int)ERoomType.Suite, (int)ERoomType.Suite3, (int)ERoomType.Suite4, (int)ERoomType.Suite5, (int)ERoomType.Suite6, (int)ERoomType.Suite7, (int)ERoomType.Suite8, (int)ERoomType.Suite9, (int)ERoomType.Suite10, (int)ERoomType.C3, (int)ERoomType.C5);

                decimal TotalUseSuite = await _settingService.GetMassageRoomByCalendarDayIdAndCheckIsNull(calendarDayId, (int)ERoomType.Suite, (int)ERoomType.Suite3, (int)ERoomType.Suite4, (int)ERoomType.Suite5, (int)ERoomType.Suite6, (int)ERoomType.Suite7, (int)ERoomType.Suite8, (int)ERoomType.Suite9, (int)ERoomType.Suite10, (int)ERoomType.C3, (int)ERoomType.C5);

                DashboardResponse dashboard = new DashboardResponse();
                dashboard.TotalRound = totalRound;
                dashboard.TotalMassage = totalMassage;
                dashboard.TotalFood = totalFood;
                dashboard.TotalAngel = totalAngel;
                dashboard.TotalRoomSuite = countMassageRoom;
                dashboard.TotalUseSuite = TotalUseSuite;
                dashboard.TotalAddRound = totalAddRound;
                var angelTypes = await _angelTypeService.GetAllAsync(null);
                if (angelTypes == null)
                {
                    return NotFound();
                }
                dashboard.AngelTypes = _mapper.Map<List<AngelTypeResponse>>(angelTypes);        
                foreach (var _dashboard in dashboard.AngelTypes)
                {
                    _dashboard.TotalCount = await _settingService.TotalCountAngelType(_dashboard.Id, calendarDayId);
                    var massageAngels = await _settingService.GetMassageAngelByCalendarDayId(_dashboard.Id, calendarDayId);
                    foreach (var massageAngel in massageAngels)
                    {
                        _dashboard.totalRound += massageAngel.Round;
                    }
                }
                return Ok(dashboard);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }
        [HttpGet(ApiRoutes.CalendarDays.GetCalendarByMonth)]
        public async Task<IActionResult> GetCalendarByMonth() 
        {
            try 
            {
                var calendayDays = await _settingService.GetCarlendarDayByMonth();
                var response = _mapper.Map<List<CalendarDayResponse>>(calendayDays);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }
        private async Task<int> GetCalendarDayId()
        {
            var calendayDay = await _settingService.GetCarlendarDay();
            return calendayDay.Id;
        }
    }
}
