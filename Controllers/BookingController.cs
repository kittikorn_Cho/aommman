﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using OONApi.Contracts.ApiRoutes;
using OONApi.Contracts.Requests;
using OONApi.Contracts.Responses;
using OONApi.Models;
using OONApi.Services;
using System.Collections.Generic;
using System.Globalization;
using System.Threading.Tasks;
using System;
using static OONApi.Contracts.ApiRoutes.ApiRoutes;
using DocumentFormat.OpenXml.Bibliography;
using OONApi.Migrations;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace OONApi.Controllers
{
    [ApiController]
    [Authorize]
    public class BookingController : ControllerBase
    {
        private IBookingService _bookingService;
        private ISettingService _settingService;
        private IRoomTypeService _roomTypeService;
        private IRoomService _roomService;
        private IAngelService _angelService;
        private IMapper _mapper;
        private ILogger<BookingController> _logger;
        private readonly AppDbContext _db;

        public BookingController(IAngelService angelService,IBookingService bookingService, ISettingService settingService, IRoomTypeService roomTypeService, IRoomService roomService, IMapper mapper, ILogger<BookingController> logger, AppDbContext db)
        {
            _bookingService = bookingService;
            _settingService = settingService;
            _roomTypeService = roomTypeService;
            _roomService = roomService;
            _angelService = angelService;
            _mapper = mapper;
            _logger = logger;
            _db = db;
        }

        [HttpPost(ApiRoutes.Bookings.Create)]
        public async Task<IActionResult> Booking([FromBody] BookingRequest request)
        {
            using (var transaction = await _db.Database.BeginTransactionAsync())
            {
                try
                {
                    int? calenDarDayId = await GetCalendarDayId();
                    string documentNumber = _settingService.GetDocumentNumberAndUpdate((int)calenDarDayId, false);
                    foreach (var room in request.Rooms) 
                     {
                        DateTime _endDate;
                        DateTime date = (DateTime)request.StartDate;
                        string startDate = date.ToString("yyyy/MM/dd", new CultureInfo("en-US"));
                        DateTime _startDate = DateTime.ParseExact(startDate+" "+request.StartTime, "yyyy/MM/dd HH:mm:ss", new CultureInfo("en-US"));
                        if (request.StartTime > request.EndTime)
                        {
                             _endDate = DateTime.ParseExact(startDate + " " + request.EndTime, "yyyy/MM/dd HH:mm:ss", new CultureInfo("en-US")).AddDays(1);
                        }
                        else 
                        {
                             _endDate = DateTime.ParseExact(startDate + " " + request.EndTime, "yyyy/MM/dd HH:mm:ss", new CultureInfo("en-US"));
                        }
                          Booking _bookings = new Booking();
                          _bookings.RoomId = (int)room.RoomId;
                          _bookings.StartDate = _startDate;
                          _bookings.EndDate = _endDate;
                          _bookings.StartTime = request.StartTime;
                          _bookings.EndTime = request.EndTime;
                          _bookings.Flag = true;
                          _bookings.Tel = request.Tel;
                          _bookings.Remark = request.Remark;
                          _bookings.DocumentNumber = documentNumber;
                          await _bookingService.Booking(_bookings);

                        foreach (var angel in room.BookingAngels) 
                        {
                            bool checkAngel =  _bookingService.GetBookingAngels(_startDate, _endDate, angel.AngelId);
                            if (!checkAngel)
                            {
                                BookingAngel bookingAngel = new BookingAngel();
                                bookingAngel.BookingId = _bookings.Id;
                                bookingAngel.AngelId = angel.AngelId;
                                bookingAngel.StartDate = _startDate;
                                bookingAngel.StartDate = _endDate;
                                bookingAngel.StartTime = _bookings.StartTime;
                                bookingAngel.EndTime = _bookings.EndTime;
                                bookingAngel.Flag = true;
                                await _bookingService.BookingAngels(bookingAngel);
                            }
                            else 
                            {
                                return BadRequest(new { Message = "ช่วงเวลาที่คุณเลือกพนักงานบริการถูกจองแล้ว !" });
                            }
                        }
                     }

                    _settingService.GetDocumentNumberAndUpdate((int)calenDarDayId, true);
                    await transaction.CommitAsync();
                    return Ok();
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex.Message);
                    _logger.LogError(ex.StackTrace);
                    await transaction.RollbackAsync();
                    return BadRequest();

                }
            }
        }

        [HttpDelete(ApiRoutes.Bookings.Delete)]
        public async Task<IActionResult> CancelBooking(int id)
        {
            {
                try
                {
                    var bookingAngels = await _bookingService.GetBookingAngelsByBookingId(id);
                    foreach (var bookingAngel in bookingAngels) 
                    {
                        var _bookingAngel = await _bookingService.GetByIdAsync(bookingAngel.Id);
                        await _bookingService.DeleteBookingAngels(_bookingAngel.Id);
                    }
                    await _bookingService.DeleteAsync(id);
                    return Ok();
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex.Message);
                    _logger.LogError(ex.StackTrace);
                    return BadRequest();
                }
            }
        }

        [HttpPut(ApiRoutes.Bookings.Update)]
        public async Task<IActionResult> Put([FromRoute] int id, [FromBody] BookingRequest request)
        {
            try
            {
                var booking = await _bookingService.GetByIdAsync(id);
                if (booking == null)
                {
                    return NotFound();
                }
                
                DateTime _endDate;
                DateTime date = (DateTime)request.StartDate;
                string startDate = date.ToString("yyyy/MM/dd", new CultureInfo("en-US"));
                DateTime _startDate = DateTime.ParseExact(startDate + " " + request.StartTime, "yyyy/MM/dd HH:mm:ss", new CultureInfo("en-US"));
                if (request.StartTime > request.EndTime)
                {
                    _endDate = DateTime.ParseExact(startDate + " " + request.EndTime, "yyyy/MM/dd HH:mm:ss", new CultureInfo("en-US")).AddDays(1);
                }
                else
                {
                    _endDate = DateTime.ParseExact(startDate + " " + request.EndTime, "yyyy/MM/dd HH:mm:ss", new CultureInfo("en-US"));
                }

                BookingResponse responses = new BookingResponse();
                foreach (var room in request.Rooms)
                {
                    var checkTime = await _bookingService.GetBookingByStartDateAndStartTimeEndTime(id, _startDate, (TimeSpan)request.StartTime, (TimeSpan)request.EndTime, (int)room.RoomId);
                    Booking update = booking;
                    if (checkTime.Count < 0)
                    {

                        update.RoomId = (int)room.RoomId;
                        update.StartDate = _startDate;
                        update.StartTime = request.StartTime;
                        update.EndTime = request.EndTime;
                        update.Tel = request.Tel;
                        update.Remark = request.Remark;

                        await _bookingService.UpdateAsync(update);
                    }
                    else
                    {
                        return BadRequest(new { Message = "ช่วงเวลาที่คุณเลือกไม่ว่างกรุณาเลือกใหม่ค่ะ !" });
                    }
                    var response = _mapper.Map<BookingResponse>(update);
                    responses = response;
                }
                return Ok(responses);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }

        [HttpGet(ApiRoutes.Bookings.Get)]
        public async Task<IActionResult> GetBookingByIdAsync(int id)
        {
            try
            {
                var booking = await _bookingService.GetByIdAsync(id);
                if (booking == null)
                {
                    return NotFound();
                }
                var response = _mapper.Map<BookingResponse>(booking);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }
        [HttpGet(ApiRoutes.Bookings.GetAll)]
        public async Task<IActionResult> GetAll([FromQuery] BookingRequest request)
        {
            try
            {
                var roomTypes = await _roomTypeService.GetAllAsync();
                if (roomTypes == null)
                {
                    return NotFound();
                }
                var response = _mapper.Map<List<RoomTypeResponse>>(roomTypes);
                foreach (var roomType in response)
                {
                    var rooms = await _roomService.GetRoomByRoomTypeId(roomType.Id);
                    if (rooms == null)
                    {
                        return NotFound();
                    }
                    roomType.Rooms = _mapper.Map<List<RoomResponse>>(rooms);
                    foreach (var room in roomType.Rooms)
                    {
                        DateTime _endDate;
                        DateTime date = (DateTime)request.StartDate;
                        string startDate = date.ToString("yyyy/MM/dd", new CultureInfo("en-US"));
                        DateTime _startDate = DateTime.ParseExact(startDate + " " + request.StartTime, "yyyy/MM/dd HH:mm:ss", new CultureInfo("en-US"));
                        if (request.StartTime > request.EndTime)
                        {
                            _endDate = DateTime.ParseExact(startDate + " " + request.EndTime, "yyyy/MM/dd HH:mm:ss", new CultureInfo("en-US")).AddDays(1);
                        }
                        else
                        {
                            _endDate = DateTime.ParseExact(startDate + " " + request.EndTime, "yyyy/MM/dd HH:mm:ss", new CultureInfo("en-US"));
                        }

                        var bookings = await _bookingService.GetBookingByDateTimeAndRoomId(_startDate, _endDate, room.Id);
                       
                        room.Bookings = _mapper.Map<List<BookingResponse>>(bookings);
                        foreach (var booking in room.Bookings) 
                        {
                            var bookingAngels = await _bookingService.GetBookingAngelsByBookingId(booking.Id);

                            booking.BookingAngels = _mapper.Map<List<BookingAngelsResponse>>(bookingAngels);
                            foreach (var bookingAngel in booking.BookingAngels)
                            {
                                var _angel = await _angelService.GetByIdAsync(bookingAngel.AngelId);
                                bookingAngel.Angel = _mapper.Map<AngelResponse>(_angel);
                            }
                        }
                    }
                }

                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }
        [HttpGet(ApiRoutes.Bookings.GetBookingByDay)]
        public async Task<IActionResult> GetBookingDay([FromQuery] BookingRequest request)
        {
            try
            {
                DateTime _endDate;
                DateTime date = (DateTime)request.StartDate;
                string startDate = date.ToString("yyyy/MM/dd", new CultureInfo("en-US"));
                _endDate = DateTime.ParseExact(startDate + " " + "05:00:00", "yyyy/MM/dd HH:mm:ss", new CultureInfo("en-US")).AddDays(1);

                var bookings = await _bookingService.GetBookingByDay((DateTime)request.StartDate, _endDate);
                if (bookings == null)
                {
                    return NotFound();
                }

                return Ok(bookings);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }
        [HttpGet(ApiRoutes.Bookings.GetListBooking)]
        public async Task<IActionResult> GetListBooking([FromQuery] BookingRequest request) 
        {
            try
            {
                DateTime _endDate;
                DateTime date = (DateTime)request.StartDate;
                string startDate = date.ToString("yyyy/MM/dd", new CultureInfo("en-US"));
                _endDate = DateTime.ParseExact(startDate + " " + "05:00:00", "yyyy/MM/dd HH:mm:ss", new CultureInfo("en-US")).AddDays(1);

                var bookings = await _bookingService.GetListBooking((DateTime)request.StartDate, _endDate, (int)request.RoomId);
                if (bookings == null)
                {
                    return NotFound();
                }
                var responses = _mapper.Map<List<BookingResponse>>(bookings);
                foreach (var response in responses) 
                {
                    var bookingAngels = await _bookingService.GetBookingAngelsByBookingId(response.Id);
                    response.BookingAngels = _mapper.Map<List<BookingAngelsResponse>>(bookingAngels);
                    foreach (var bookingAngel in response.BookingAngels) 
                    {
                        var angel = await _angelService.GetByIdAsync(bookingAngel.AngelId);
                        bookingAngel.Angel = _mapper.Map<AngelResponse>(angel);
                    }
                }
                return Ok(responses);
            }
            catch (Exception ex) 
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }

        [HttpPost(ApiRoutes.Bookings.CreateBookingAngels)]
        public async Task<IActionResult> BookingAngels([FromBody] BookingRequest request) 
        {
            using (var transaction = await _db.Database.BeginTransactionAsync())
            {
                try
                {
                    
                    foreach (var angelBooking in request.AngelBookings) 
                     {
                        DateTime _endDate;
                        DateTime date = (DateTime)angelBooking.StartDate;
                        string startDate = date.ToString("yyyy/MM/dd", new CultureInfo("en-US"));
                        DateTime _startDate = DateTime.ParseExact(startDate + " " + angelBooking.StartTime, "yyyy/MM/dd HH:mm:ss", new CultureInfo("en-US"));
                        if (angelBooking.StartTime > angelBooking.EndTime)
                        {
                            _endDate = DateTime.ParseExact(startDate + " " + angelBooking.EndTime, "yyyy/MM/dd HH:mm:ss", new CultureInfo("en-US")).AddDays(1);
                        }
                        else
                        {
                            _endDate = DateTime.ParseExact(startDate + " " + angelBooking.EndTime, "yyyy/MM/dd HH:mm:ss", new CultureInfo("en-US"));
                        }
                        bool checkBookingAngels =  _bookingService.GetBookingAngels(_startDate, _endDate, angelBooking.AngelId);
                        if (!checkBookingAngels)
                        {
                            BookingAngel bookingAngel = new BookingAngel();
                            bookingAngel.BookingId = angelBooking.BookingId;
                            bookingAngel.AngelId = angelBooking.AngelId;
                            bookingAngel.StartDate = _startDate;
                            bookingAngel.EndDate = _endDate;
                            bookingAngel.StartTime = angelBooking.StartTime;
                            bookingAngel.EndTime = angelBooking.EndTime;
                            bookingAngel.Flag = true;
                            await _bookingService.BookingAngels(bookingAngel);
                        }
                        else
                        {
                            return BadRequest(new { Message = "ช่วงเวลาที่คุณเลือกพนักงานบริการถูกจองแล้ว !" });
                        }
                    }
                    await transaction.CommitAsync();
                    return Ok();
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex.Message);
                    _logger.LogError(ex.StackTrace);
                    await transaction.RollbackAsync();
                    return BadRequest();

                }
            }
        }

        [HttpDelete(ApiRoutes.Bookings.DeleteBookingAngels)]
        public async Task<IActionResult> DeleteBookingAngels(int id)
        {
            {
                try
                {
                    await _bookingService.DeleteBookingAngels(id);
                    return Ok();
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex.Message);
                    _logger.LogError(ex.StackTrace);
                    return BadRequest();
                }
            }
        }


        private async Task<int> GetCalendarDayId()
        {
            var calendayDay = await _settingService.GetCarlendarDay();
            return calendayDay.Id;
        }
    }
}