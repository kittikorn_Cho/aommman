﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using OONApi.Contracts.ApiRoutes;
using OONApi.Contracts.Responses;
using OONApi.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace OONApi.Controllers
{
    [ApiController]
    public class MenuController : ControllerBase
    {
        private IMenuService _menuService;
        private IMapper _mapper;
        private ILogger<MenuController> _logger;

        public MenuController(IMenuService menuService, IMapper mapper, ILogger<MenuController> logger)
        {
            _menuService = menuService;
            _mapper = mapper;
            _logger = logger;
        }
        // GET: api/<MenuController>
        [HttpGet(ApiRoutes.Menus.GetAll)]
        public async Task<IActionResult> GetAll()
        {
            try
            {
                var menus = await _menuService.GetAllAsync();
                if(menus == null)
                {
                    return NotFound();
                }
                return Ok(_mapper.Map<List<MenuResponse>>(menus));
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.StackTrace);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }
    }
}
