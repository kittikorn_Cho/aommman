﻿using AutoMapper;
using DocumentFormat.OpenXml.Office2016.Excel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using OONApi.Contracts.ApiRoutes;
using OONApi.Contracts.Requests;
using OONApi.Contracts.Requests.Queires;
using OONApi.Contracts.Responses;
using OONApi.Enum;
using OONApi.Models;
using OONApi.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace OONApi.Controllers
{
    [ApiController]
    [Authorize]
    public class MemberController : ControllerBase
    {
        private IMemberService _memberService;
        private IItemService _itemService;
        private IMapper _mapper;
        private ILogger<MemberController> _logger;
        private ISettingService _settingService;
        private IMassageService _massageService;
        private IUserService _userService;
        private IMemberPointRedemtionsService _memberPointRedemtionsService;
        private readonly AppDbContext _db;
        public MemberController(IMemberService memberService, IMapper mapper, ILogger<MemberController> logger, ISettingService settingService, AppDbContext db, IItemService itemService, IMassageService massageService, IUserService userService, IMemberPointRedemtionsService memberPointRedemtionsService)
        {
            _memberService = memberService;
            _mapper = mapper;
            _logger = logger;
            _settingService = settingService;
            _db = db;
            _itemService = itemService;
            _massageService = massageService;
            _userService = userService;
            _memberPointRedemtionsService = memberPointRedemtionsService;
        }

        [HttpGet(ApiRoutes.Members.GetAll)]
        public async Task<IActionResult> GetAll([FromQuery] MemberSearchQuery paginationQuery)
        {
            try
            {
                var pagination = _mapper.Map<PaginationFilter>(paginationQuery);
                var member = await _memberService.GetAllAsync(paginationQuery, pagination);

                var response = _mapper.Map<List<MemberResponse>>(member);

                foreach (var item in response)
                {
                    foreach (var item2 in item.MemberItems)
                    {
                        var item1 = await _itemService.GetByIdAsync(item2.ItemId);
                        item2.Item = _mapper.Map<ItemResponse>(item1);

                    }
                }

                return Ok(new PagedResponse<MemberResponse>
                {
                    Data = response,
                    PageNumber = pagination.PageNumber,
                    PageSize = pagination.PageSize,
                    Keyword = pagination.Keyword,
                    OrderBy = pagination.OrderBy,
                    TotalRecords = pagination.TotalRecords,
                    OrderDirection = pagination.OrderDirection,
                });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }

        }

        [HttpGet(ApiRoutes.Members.GetListPayment)]
        public async Task<IActionResult> GetMemberTransaction([FromQuery] MemberPaymentSearchQuery paginationQuery)
        {
            try
            {
                var pagination = _mapper.Map<PaginationFilter>(paginationQuery);
                var member = await _memberService.GetMemberTransactions(paginationQuery, pagination);
                if (member == null)
                {
                    return NotFound();
                }


                return Ok(new PagedResponse<MemberTransactionResponse>
                {
                    Data = _mapper.Map<List<MemberTransactionResponse>>(member),
                    PageNumber = pagination.PageNumber,
                    PageSize = pagination.PageSize,
                    Keyword = pagination.Keyword,
                    OrderBy = pagination.OrderBy,
                    TotalRecords = pagination.TotalRecords,
                    OrderDirection = pagination.OrderDirection,
                });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }

        }

        [HttpGet(ApiRoutes.Members.Get)]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                var member = await _memberService.GetByIdAsync(id);
                if (member == null)
                {
                    return NotFound();
                }

                var response = _mapper.Map<MemberResponse>(member);

                foreach (var item in response.MemberItems)
                {
                    var i = await _itemService.GetByIdAsync(item.ItemId);
                    item.Item = _mapper.Map<ItemResponse>(i);
                }

                return Ok(_mapper.Map<MemberResponse>(member));
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }

        }

        [HttpGet(ApiRoutes.Members.GetPayment)]
        public async Task<IActionResult> GetMemberTransactionById(int id)
        {
            try
            {
                var member = await _memberService.GetMemberTransactionById(id);
                if (member == null)
                {
                    return NotFound();
                }
                var response = _mapper.Map<MemberTransactionResponse>(member);
                var memberitemtransaction = await _memberService.GetMemberItemTransactionByMemberTransactionId(member.Id);
                response.MemberItemTransactions = _mapper.Map<List<MemberItemTransactionResponse>>(memberitemtransaction);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }

        [HttpGet(ApiRoutes.Members.CheckCode)]
        public async Task<IActionResult> GetMemberByCode([FromQuery] MemberPaymentSearchQuery paginationQuery)
        {
            try
            {
                var member = await _memberService.GetByCodeAsync(paginationQuery.Code);
                bool isMember = false;
                string memberMessage = "";
                if (member != null)
                {
                    isMember = true;
                    memberMessage = "มีรหัสนี้อยู่แล้ว";
                }
                else
                {
                    isMember = false;
                    memberMessage = "สามารถใช้งานได้";
                }

                MemberCheckCodeResponse checkCodeResponse = new MemberCheckCodeResponse();
                checkCodeResponse.IsMember = isMember;
                checkCodeResponse.MemberMessage = memberMessage;

                return Ok(checkCodeResponse);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }

        [HttpPost(ApiRoutes.Members.Create)]
        public async Task<IActionResult> Post([FromBody] MemberRequest request)
        {
            using (var transaction = await _db.Database.BeginTransactionAsync())
            {
                try
                {
                    var point = await _settingService.GetSettingById(1);
                    int? calendarDayId = await GetCalendarDayId();
                    if (calendarDayId == null)
                    {
                        return NotFound();
                    }
                    Member member = new Member
                    {
                        Code = request.Code,
                        Firstname = request.Firstname,
                        Lastname = request.Lastname,
                        Tel = request.Tel,
                        CreditAmount = request.CreditAmount,
                        ReceptionId = request.ReceptionId,
                        ExpiredDate = DateTime.Now.AddYears(1),
                        Status = 1,
                        FromDate = DateTime.Now,
                        CountriesId = request.CountriesId,
                        AgenciesId = request.AgenciesId,
                        //Point = request.CreditAmount / point.PointMember,

                    };
                    await _memberService.CreateAsync(member);

                    List<MemberItem> memberItems = new List<MemberItem>();
                    foreach (var item in request.MemberItems)
                    {
                        MemberItem memberItem = new MemberItem();
                        memberItem.MemberId = member.Id;
                        memberItem.ItemId = item.ItemId;
                        memberItem.Amount = item.Amount;
                        memberItems.Add(memberItem);
                    }

                    await _memberService.CreateMemberItem(memberItems);

                    List<MemberTransaction> memberTransactions = new List<MemberTransaction>();
                    foreach (var transactions in request.MemberTransactions)
                    {
                        MemberTransaction memberTransaction = new MemberTransaction();
                        memberTransaction.MemberId = member.Id;
                        memberTransaction.CalendarDayId = (int)calendarDayId;
                        memberTransaction.ReceptionId = request.ReceptionId;
                        memberTransaction.IsPaid = transactions.IsPaid;
                        memberTransaction.IsUnPaid = transactions.IsUnPaid;
                        memberTransaction.Total = transactions.Total;
                        memberTransaction.IsCash = transactions.IsCash;
                        memberTransaction.IsCredit = transactions.IsCredit;
                        memberTransaction.IsEntertain = transactions.IsEntertain;
                        memberTransaction.CashTotal = transactions.CashTotal;
                        memberTransaction.CreditTotal = transactions.CreditTotal;
                        memberTransaction.EntertainTotal = transactions.EntertainTotal;
                        memberTransaction.PayDate = DateTime.Now;
                        memberTransaction.IsQrCode = transactions.IsQrCode;
                        memberTransaction.QrCodeTotal = transactions.QrCodeTotal;
                        memberTransaction.IsPercent = transactions.IsPercent;
                        memberTransaction.PercentTopup = transactions.PercentTopup;
                        memberTransaction.PercentTotalTopup = transactions.PercentTotalTopup;
                        
                        if (point.PointMember != 0) 
                        {
                            memberTransaction.Point = transactions.Total / point.PointMember;
                        }
                        memberTransactions.Add(memberTransaction);

                        if (point.PointMember != 0) 
                        {
                           member.Point = transactions.Total / point.PointMember;
                        }
                       
                        await _memberService.UpdateAsync(member);
                    }

                    await _memberService.CreateMemberTransactionAsync(memberTransactions);

                    foreach (var item in request.MemberItems)
                    {
                        MemberItemTransaction memberItemTransaction = new MemberItemTransaction();
                        memberItemTransaction.MemberId = member.Id;
                        memberItemTransaction.ItemId = item.ItemId;
                        memberItemTransaction.MemberTransactionId = memberTransactions[0].Id;
                        memberItemTransaction.Amount = item.Amount;
                        await _memberService.CreateMemberItemTransactionAsync(memberItemTransaction);
                    }
                    await transaction.CommitAsync();

                    var memberMap = await _memberService.GetByIdAsync(member.Id);

                    foreach (var memberItem in memberMap.MemberItems)
                    {
                        var item = await _itemService.GetByIdAsync(memberItem.ItemId);

                        memberItem.Item = item;
                    }

                    var memberResponse = _mapper.Map<MemberResponse>(memberMap);

                    return Ok(memberResponse);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex.Message);
                    _logger.LogError(ex.StackTrace);
                    await transaction.RollbackAsync();
                    return BadRequest();
                }
            }
        }

        [HttpPut(ApiRoutes.Members.Update)]
        public async Task<IActionResult> Put([FromRoute] int id, [FromBody] MemberRequest request, [FromQuery] string action = "edit")
        {
            using (var transaction = await _db.Database.BeginTransactionAsync())
            {
                try
                {
                    var point = await _settingService.GetSettingById(1);
                    int? calendarDayId = await GetCalendarDayId();
                    if (calendarDayId == null)
                    {
                        return NotFound();
                    }
                    var member = await _memberService.GetByIdAsync(id);
                    if (member == null)
                    {
                        return NotFound();
                    }

                    int transactionId = 0;

                    member.Code = request.Code;
                    member.Firstname = request.Firstname;
                    member.Lastname = request.Lastname;
                    member.Tel = request.Tel;
                    member.CreditAmount = request.CreditAmount;
                    member.CountriesId = request.CountriesId;
                    member.AgenciesId = request.AgenciesId;
                    if (action == "edit")
                    {
                        member.ReceptionId = request.ReceptionId;
                    }

                    if (request.ExpiredDate != null)
                    {
                        member.ExpiredDate = (DateTime)request.ExpiredDate;

                        if (member.ExpiredDate < DateTime.Now.Date)
                        {
                            member.Status = 2;
                        }
                        else
                        {
                            member.Status = 1;
                        }

                    }
                    if (action == "topup") // khun chai jutateb dup dup dup 
                    {
                        member.ExpiredDate = DateTime.Now.AddYears(1);
                        member.Status = 1;
                    }
                    await _memberService.UpdateAsync(member);

                    _db.ChangeTracker.Clear();

                    if (action == "topup" || action == "edit" || action == "payment")
                    {
                        if (action == "topup" || action == "edit")
                        {
                            List<MemberItem> addMemberItems = new List<MemberItem>();
                            List<MemberItem> updateMemberitems = new List<MemberItem>();
                            foreach (var item in request.MemberItems)
                            {
                                if (item.Id != null)
                                {
                                    MemberItem temp = new MemberItem
                                    {
                                        Id = (int)item.Id,
                                        MemberId = member.Id,
                                        ItemId = item.ItemId,
                                        Amount = item.Amount,
                                    };
                                    updateMemberitems.Add(temp);
                                }
                                else
                                {
                                    MemberItem temp = new MemberItem
                                    {
                                        MemberId = member.Id,
                                        ItemId = item.ItemId,
                                        Amount = item.Amount,
                                    };
                                    addMemberItems.Add(temp);
                                }
                            }
                            if (addMemberItems.Count > 0)
                            {
                                await _memberService.CreateMemberItem(addMemberItems);
                            }
                            if (updateMemberitems.Count > 0)
                            {
                                await _memberService.UpdateMemberItemAsync(updateMemberitems);
                            }
                        }

                        if (action == "topup")
                        {
                            List<MemberTransaction> memberTransactions = new List<MemberTransaction>();
                            foreach (var transactions in request.MemberTransactions)
                            {
                                MemberTransaction memberTransaction = new MemberTransaction();
                                memberTransaction.MemberId = id;
                                memberTransaction.CalendarDayId = (int)calendarDayId;
                                memberTransaction.ReceptionId = request.ReceptionId;
                                memberTransaction.IsPaid = transactions.IsPaid;
                                memberTransaction.IsUnPaid = transactions.IsUnPaid;
                                memberTransaction.Total = transactions.Total;
                                memberTransaction.IsCash = transactions.IsCash;
                                memberTransaction.IsCredit = transactions.IsCredit;
                                memberTransaction.IsEntertain = transactions.IsEntertain;
                                memberTransaction.CashTotal = transactions.CashTotal;
                                memberTransaction.CreditTotal = transactions.CreditTotal;
                                memberTransaction.EntertainTotal = transactions.EntertainTotal;
                                memberTransaction.IsQrCode = transactions.IsQrCode;
                                memberTransaction.QrCodeTotal = transactions.QrCodeTotal;

                                memberTransaction.IsPercent = transactions.IsPercent;
                                memberTransaction.PercentTopup = transactions.PercentTopup;
                                memberTransaction.PercentTotalTopup = transactions.PercentTotalTopup;

                                if (point.PointMember != 0)
                                {
                                    memberTransaction.Point = transactions.Total / point.PointMember;
                                }


                                memberTransactions.Add(memberTransaction);
                                
                                var _member = await _memberService.GetByIdAsync(id);
                                if (point.PointMember != 0) 
                                {
                                    _member.Point += (transactions.Total / point.PointMember);
                                }
                                

                                await _memberService.UpdateAsync(_member);
                            }
                            
                            await _memberService.CreateMemberTransactionAsync(memberTransactions);

                           

                            transactionId = memberTransactions[0].Id;
                        }

                        if (action == "payment")
                        {
                            var membertransaction = await _memberService.GetMemberTransactionById((int)request.MemberTransactions[0].id);
                            if (membertransaction == null)
                            {
                                return NotFound();
                            }
                            foreach (var transactions in request.MemberTransactions)
                            {
                                membertransaction.IsPaid = transactions.IsPaid;
                                membertransaction.IsUnPaid = transactions.IsUnPaid;
                                membertransaction.Total = transactions.Total;
                                membertransaction.IsCash = transactions.IsCash;
                                membertransaction.IsCredit = transactions.IsCredit;
                                membertransaction.IsEntertain = transactions.IsEntertain;
                                membertransaction.CashTotal = transactions.CashTotal;
                                membertransaction.CreditTotal = transactions.CreditTotal;
                                membertransaction.EntertainTotal = transactions.EntertainTotal;
                                membertransaction.IsQrCode = transactions.IsQrCode;
                                membertransaction.QrCodeTotal = transactions.QrCodeTotal;
                                membertransaction.PayDate = DateTime.Now;
                            }
                            await _memberService.UpdateMemberTransactionAsync(membertransaction);

                            transactionId = membertransaction.Id;
                        }
                    }

                    var memberMap = await _memberService.GetByIdAsync(member.Id);

                    var response = _mapper.Map<MemberResponse>(memberMap);

                    foreach (var memberItem in response.MemberItems)
                    {
                        var item = await _itemService.GetByIdAsync(memberItem.ItemId);

                        memberItem.Item = _mapper.Map<ItemResponse>(item);
                    }

                    if (action == "topup" || action == "payment")
                    {
                        var memberTransaction = await _memberService.GetMemberTransactionById(transactionId);

                        response.Membertransaction = _mapper.Map<MemberTransactionResponse>(memberTransaction);
                    }

                    if (action == "topup")
                    {
                        List<MemberItemTopupResponse> memberItems = new List<MemberItemTopupResponse>();

                        foreach (var item in request.MemberItemTopups)
                        {
                            MemberItemTopupResponse memberItem = new MemberItemTopupResponse();
                            memberItem.Id = (int)item.Id;
                            memberItem.MemberId = member.Id;
                            memberItem.ItemId = item.ItemId;
                            memberItem.Amount = item.Amount;
                            memberItems.Add(memberItem);

                            MemberItemTransaction memberItemTransaction = new MemberItemTransaction();
                            memberItemTransaction.MemberId = member.Id;
                            memberItemTransaction.ItemId = item.ItemId;
                            memberItemTransaction.MemberTransactionId = transactionId;
                            memberItemTransaction.Amount = item.Amount;

                            await _memberService.CreateMemberItemTransactionAsync(memberItemTransaction);
                        }

                        response.MemberItemsTopup = _mapper.Map<List<MemberItemTopupResponse>>(memberItems);


                        foreach (var item in response.MemberItemsTopup)
                        {
                            var _item = await _itemService.GetByIdAsync(item.ItemId);

                            item.Item = _mapper.Map<ItemResponse>(_item);
                        }

                    }


                    await transaction.CommitAsync();
                    return Ok(response);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex.Message);
                    _logger.LogError(ex.StackTrace);
                    await transaction.RollbackAsync();
                    return BadRequest();
                }
            }
        }

        [HttpPut(ApiRoutes.Members.Cancel)]
        public async Task<IActionResult> Cancel(int id)
        {
            using (var transaction = await _db.Database.BeginTransactionAsync())
            {
                try
                {
                    var point = await _settingService.GetSettingById(1);
                    var memberTransaction = await _memberService.GetMemberTransactionById(id);
                    memberTransaction.MemberItemTransactions = await _memberService.GetmemberItemTransactionByTransactionId(memberTransaction.Id);
                    var member = await _memberService.GetByIdAsync(memberTransaction.MemberId);
                    member.MemberItems = await _memberService.GetMemberItemByMemberId(member.Id);

                   
                    member.CreditAmount -= memberTransaction.Total;

                    //CancelPoint Member
                    if (point.PointMember != 0) 
                    {
                        member.Point = memberTransaction.Total / point.PointMember;
                    }
                   
                    member.Point -= memberTransaction.Point;
                    foreach (var item in member.MemberItems)
                    {
                        foreach (var itemTransaction in memberTransaction.MemberItemTransactions)
                        {
                            if (item.ItemId == itemTransaction.ItemId)
                            {
                                item.Amount -= itemTransaction.Amount;
                            }
                        }
                    }
                    await _memberService.UpdateMemberAndMemberItemAsync(member);

                    _db.ChangeTracker.Clear();

                    await _memberService.DeleteMemberTransaction(memberTransaction.Id);
                    await transaction.CommitAsync();
                    return Ok();
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex.Message);
                    _logger.LogError(ex.StackTrace);
                    await transaction.RollbackAsync();
                    return BadRequest();
                }
            }
        }

        [HttpGet(ApiRoutes.Members.GetMemberHistory)]
        public async Task<IActionResult> GetMemberHistory([FromQuery] MemberHistorySearchQuery paginationQuery, int id)
        {
            try
            {
                var pagination = _mapper.Map<PaginationFilter>(paginationQuery);
                var memberHistory = await _memberService.GetMemberHistory(id, paginationQuery, pagination);
                if (memberHistory == null)
                {
                    return NotFound();
                }
                return Ok(new PagedResponse<MassageMemberPayment>
                {
                    Data = _mapper.Map<List<MassageMemberPayment>>(memberHistory),
                    PageNumber = pagination.PageNumber,
                    PageSize = pagination.PageSize,
                    Keyword = pagination.Keyword,
                    OrderBy = pagination.OrderBy,
                    TotalRecords = pagination.TotalRecords,
                    OrderDirection = pagination.OrderDirection
                });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }

        [HttpPut(ApiRoutes.Members.Delete)]
        public async Task<IActionResult> Delete(int id)
        {
            using (var transaction = await _db.Database.BeginTransactionAsync())
            {
                try
                {
                    //await _memberService.DeleteAsync(id);
                    Member member = await _memberService.GetByIdAsync(id);
                    if (member == null)
                    {
                        return NotFound();
                    }
                    member.Status = (int)EAngelStatus.Cancel;
                    await _memberService.UpdateAsync(member);
                    _db.ChangeTracker.Clear();

                    await transaction.CommitAsync();
                    return Ok();
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex.Message);
                    _logger.LogError(ex.StackTrace);
                    return BadRequest();
                }
            }
        }

        [HttpPost(ApiRoutes.Members.DeleteSelectAll)]
        public async Task<IActionResult> DeleteSelectAll([FromBody] MemberRequest request)
        {
            try
            {
                foreach (var item in request.Ids)
                {
                    await _memberService.DeleteAsync(item);
                }
                return Ok();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }
        [HttpGet(ApiRoutes.Members.GetSummaryTotalCredit)]
        public async Task<IActionResult> GetSummaryTotalCreadit()
        {
            try
            {
                int calendarDayId = await GetCalendarDayId();

                var resp = _memberService.SummaryTotalMember(calendarDayId);

                var memberPayments = await _memberService.GetMemberPaymentByIsToday(calendarDayId);

                resp.massageMemberPayments = _mapper.Map<List<MassageMemberPaymentResponse>>(memberPayments);

                var memberPayment = await _memberService.GetMemberPaymentByCalendarDayId(calendarDayId);

                resp.memberPayments = _mapper.Map<List<MemberPaymentResponse>>(memberPayment);

                foreach (var massageMemberPayment in resp.massageMemberPayments)
                {
                    var massage = await _massageService.GetById(massageMemberPayment.MassageId);

                    massageMemberPayment.Massage = _mapper.Map<MassageResponse>(massage);
                }

                return Ok(resp);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }

        [HttpGet(ApiRoutes.Members.GetAllMemberPaymentsUnpaid)]
        public async Task<IActionResult> GetAllMemberPaymentsUnPaid([FromQuery] MemberHistorySearchQuery paginationQuery)
        {
            try
            {
                var pagination = _mapper.Map<PaginationFilter>(paginationQuery);
                List<int> calendarIds = null;
                if (paginationQuery.StartDate != null && paginationQuery.EndDate != null)
                {
                    calendarIds = await _settingService.GetCalendarId((DateTime)paginationQuery.StartDate, (DateTime)paginationQuery.EndDate);
                }
                var memberPayment = await _memberService.GetMemberPaymentUnpiad(calendarIds, pagination);
                if (memberPayment == null)
                {
                    return NotFound();
                }
                return Ok(new PagedResponse<MemberPaymentResponse>
                {
                    Data = _mapper.Map<List<MemberPaymentResponse>>(memberPayment),
                    PageNumber = pagination.PageNumber,
                    PageSize = pagination.PageSize,
                    Keyword = pagination.Keyword,
                    OrderBy = pagination.OrderBy,
                    TotalRecords = pagination.TotalRecords,
                    OrderDirection = pagination.OrderDirection
                });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }
        [HttpGet(ApiRoutes.Members.GetMemberPaymentsUnpaidById)]
        public async Task<IActionResult> GetByIdMemberPaymentUnPaid(int id)
        {
            try
            {
                var member = await _memberService.GetMemberPaymentById(id);
                if (member == null)
                {
                    return NotFound();
                }

                var response = _mapper.Map<MemberPaymentResponse>(member);


                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }

        }
        [HttpPut(ApiRoutes.Members.UpdateMemberPayment)]
        public async Task<IActionResult> UpdateMemberPayment([FromRoute] int id, [FromBody] MemberPaymentEtcRequest request)
        {
            try
            {
                var memberPayment = await _memberService.GetMemberPaymentById(id);
                decimal oldCreditAmount = memberPayment.CerditAmount;

                memberPayment.IsUnPaid = false;
                memberPayment.UnpaidTotal = 0;
                memberPayment.IsPay = true;
                memberPayment.Total = request.Total;
                memberPayment.IsCash = request.IsCash;
                memberPayment.CashTotal = request.CashTotal;
                memberPayment.IsCredit = request.IsCredit;
                memberPayment.CreditTotal = request.CreditTotal;
                memberPayment.IsQrCode = request.IsQrCode;
                memberPayment.QrCodeTotal = request.QrCodeTotal;
                memberPayment.CerditAmount = request.CerditAmount;
                memberPayment.PayDate = DateTime.Now;

                await _memberService.UpdateMemberPayment(memberPayment);

                if (request.CerditAmount != 0)
                {
                    var member = await _memberService.GetByIdAsync(request.MemberId);
                    member.CreditAmount = member.CreditAmount - (request.CerditAmount - oldCreditAmount);
                    await _memberService.UpdateAsync(member);
                }

                //var member = await _memberService.GetMemberPaymentById(id);
                //var response = _mapper.Map<MemberPaymentResponse>(member);
                var _member = await _memberService.GetByIdAsync(request.MemberId);

                var response = _mapper.Map<MemberPaymentEtcResponse>(_member);

                foreach (var item in response.MemberItems)
                {
                    var i = await _itemService.GetByIdAsync(item.ItemId);
                    item.Item = _mapper.Map<ItemResponse>(i);
                }

                if (!String.IsNullOrEmpty(request.Name_1))
                {
                    var _item = await _itemService.GetByIdAsync(Convert.ToInt32(request.Name_1));
                    response.Name_1 = _item.Name;
                    response.Amount_1 = request.Amount_1;
                }

                if (!String.IsNullOrEmpty(request.Name_2))
                {
                    var _item = await _itemService.GetByIdAsync(Convert.ToInt32(request.Name_2));
                    response.Name_2 = _item.Name;
                    response.Amount_2 = request.Amount_2;
                }

                response.IsPay = true;
                response.IsUnPaid = request.IsUnPaid;
                response.Total = request.Total;
                response.DebtCreditAmount = request.CerditAmount;
                if (request.IsCash)
                {
                    response.IsCash = true;
                    response.CashTotal = request.CashTotal;
                }
                if (request.IsQrCode)
                {
                    response.IsQrCode = true;
                    response.QrCodeTotal = request.QrCodeTotal;
                }
                if (request.IsCredit)
                {
                    response.IsCredit = true;
                    response.CreditTotal = request.CreditTotal;
                }

                response.DateDebt = request.startDate;
                response.DateNow = DateTime.Now;
                response.DocumentNumber = memberPayment.DocumentNumber;
                response.UnpaidTotal = request.UnpaidTotal;

                var user = await _userService.GetByIdAsync((int)memberPayment.CreatedBy);
                response.UserName = user.Username;

                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }


        [HttpPost(ApiRoutes.Members.CreateMemberPayment)]
        public async Task<ActionResult> MemberPaymentEtc([FromBody] MemberPaymentEtcRequest request)
        {
            try
            {
                int calendarId = _settingService.GetCalendarId(request.startDate);

                string documentNumber = _settingService.GetDocumentNumberAndUpdate((int)calendarId, false);

                MemberPayment memberPayment = new MemberPayment();
                memberPayment.CalendarId = calendarId;
                memberPayment.DocumentNumber = documentNumber;
                memberPayment.MemberId = request.MemberId;
                memberPayment.CerditAmount = request.CerditAmount;
                memberPayment.Name_1 = request.Name_1;
                memberPayment.Amount_1 = request.Amount_1;
                memberPayment.Name_2 = request.Name_2;
                memberPayment.Amount_2 = request.Amount_2;
                memberPayment.Name_3 = request.Name_3;
                memberPayment.Amount_3 = request.Amount_3;
                memberPayment.Name_4 = request.Name_4;
                memberPayment.Amount_4 = request.Amount_4;
                memberPayment.Name_5 = request.Name_5;
                memberPayment.Amount_5 = request.Amount_5;
                memberPayment.PayDate = DateTime.Now;
                memberPayment.IsPay = true;

                if (request.IsUnPaid)
                {
                    memberPayment.IsUnPaid = request.IsUnPaid;
                    memberPayment.UnpaidTotal = request.UnpaidTotal;
                    memberPayment.Total = request.Total;
                }
                if (request.IsCash)
                {
                    memberPayment.IsCash = request.IsCash;
                    memberPayment.CashTotal = request.CashTotal;
                }
                if (request.IsCredit)
                {
                    memberPayment.IsCredit = request.IsCredit;
                    memberPayment.CreditTotal = request.CreditTotal;
                }
                if (request.IsQrCode)
                {
                    memberPayment.IsQrCode = request.IsQrCode;
                    memberPayment.QrCodeTotal = request.QrCodeTotal;
                }
                memberPayment.Total = request.Total;

                await _memberService.CreateMemberPayment(memberPayment);

                if (!String.IsNullOrEmpty(request.Name_1))
                {
                    var memberItem = await _memberService.GetMemberItemSuite(request.MemberId, Convert.ToInt32(request.Name_1));
                    var member = await _memberService.GetMemberItemById(memberItem.Id);

                    member.Amount = member.Amount - request.Amount_1;

                    await _memberService.UpdateMemberItem(member);
                }

                if (!String.IsNullOrEmpty(request.Name_2))
                {
                    var memberItem = await _memberService.GetMemberItemSuite(request.MemberId, Convert.ToInt32(request.Name_2));
                    var member = await _memberService.GetMemberItemById(memberItem.Id);

                    member.Amount = member.Amount - request.Amount_2;

                    await _memberService.UpdateMemberItem(member);
                }

                if (request.CerditAmount != 0)
                {
                    var member = await _memberService.GetByIdAsync(request.MemberId);

                    member.CreditAmount = member.CreditAmount - request.CerditAmount;

                    await _memberService.UpdateAsync(member);
                }

                var _member = await _memberService.GetByIdAsync(request.MemberId);

                var response = _mapper.Map<MemberPaymentEtcResponse>(_member);

                foreach (var item in response.MemberItems)
                {
                    var i = await _itemService.GetByIdAsync(item.ItemId);
                    item.Item = _mapper.Map<ItemResponse>(i);
                }

                if (!String.IsNullOrEmpty(request.Name_1))
                {
                    var _item = await _itemService.GetByIdAsync(Convert.ToInt32(request.Name_1));
                    response.Name_1 = _item.Name;
                    response.Amount_1 = request.Amount_1;
                }

                if (!String.IsNullOrEmpty(request.Name_2))
                {
                    var _item = await _itemService.GetByIdAsync(Convert.ToInt32(request.Name_2));
                    response.Name_2 = _item.Name;
                    response.Amount_2 = request.Amount_2;
                }

                response.IsPay = true;
                response.IsUnPaid = request.IsUnPaid;
                response.Total = request.Total;
                response.DebtCreditAmount = request.CerditAmount;
                if (request.IsCash)
                {
                    response.IsCash = true;
                    response.CashTotal = request.CashTotal;
                }
                if (request.IsQrCode)
                {
                    response.IsQrCode = true;
                    response.QrCodeTotal = request.QrCodeTotal;
                }
                if (request.IsCredit)
                {
                    response.IsCredit = true;
                    response.CreditTotal = request.CreditTotal;
                }

                response.DateDebt = request.startDate;
                response.DateNow = DateTime.Now;
                response.DocumentNumber = documentNumber;
                response.UnpaidTotal = request.UnpaidTotal;

                var user = await _userService.GetByIdAsync((int)memberPayment.CreatedBy);
                response.UserName = user.Username;

                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }

        [HttpPost(ApiRoutes.Members.CreateMemberTopup)]
        public async Task<ActionResult> MemberTopup([FromBody] MemberTopupRequest request)
        {
            try
            {
                int? calendarDayId = await GetCalendarDayId();
                if (calendarDayId == null)
                {
                    return NotFound();
                }
                MemberTopup memberTopup = new MemberTopup();
                memberTopup.CalendarId = (int)calendarDayId;
                memberTopup.MemberId = request.MemberId;
                memberTopup.CerditAmount = request.CreditAmount;
                memberTopup.Name_1 = request.Name_1;
                memberTopup.Amount_1 = request.Amount_1;
                memberTopup.Name_2 = request.Name_2;
                memberTopup.Amount_2 = request.Amount_2;
                memberTopup.Name_3 = request.Name_3;
                memberTopup.Amount_3 = request.Amount_3;
                memberTopup.Name_4 = request.Name_4;
                memberTopup.Amount_4 = request.Amount_4;
                memberTopup.Name_5 = request.Name_5;
                memberTopup.Amount_5 = request.Amount_5;

                await _memberService.CreateMemberTopup(memberTopup);

                if (!String.IsNullOrEmpty(request.Name_1))
                {
                    var memberItem = await _memberService.GetMemberItemSuite(request.MemberId, Convert.ToInt32(request.Name_1));
                    var member = await _memberService.GetMemberItemById(memberItem.Id);

                    member.Amount = member.Amount - request.Amount_1;

                    await _memberService.UpdateMemberItem(member);
                }

                if (!String.IsNullOrEmpty(request.Name_2))
                {
                    var memberItem = await _memberService.GetMemberItemSuite(request.MemberId, Convert.ToInt32(request.Name_2));
                    var member = await _memberService.GetMemberItemById(memberItem.Id);

                    member.Amount = member.Amount - request.Amount_2;

                    await _memberService.UpdateMemberItem(member);
                }

                if (!String.IsNullOrEmpty(request.Name_3))
                {
                    var memberItem = await _memberService.GetMemberItemSuite(request.MemberId, Convert.ToInt32(request.Name_3));
                    var member = await _memberService.GetMemberItemById(memberItem.Id);

                    member.Amount = member.Amount - request.Amount_3;

                    await _memberService.UpdateMemberItem(member);
                }

                if (request.CreditAmount != 0)
                {
                    var member = await _memberService.GetByIdAsync(request.MemberId);

                    member.CreditAmount = member.CreditAmount + request.CreditAmount;

                    await _memberService.UpdateAsync(member);
                }

                var response = _mapper.Map<MemberTopupResponse>(memberTopup);

                if (!String.IsNullOrEmpty(request.Name_1))
                {
                    var _item = await _itemService.GetByIdAsync(Convert.ToInt32(request.Name_1));
                    response.Name_1 = _item.Name;
                    response.Amount_1 = request.Amount_1;
                }

                if (!String.IsNullOrEmpty(request.Name_2))
                {
                    var _item = await _itemService.GetByIdAsync(Convert.ToInt32(request.Name_2));
                    response.Name_2 = _item.Name;
                    response.Amount_2 = request.Amount_2;
                }

                if (!String.IsNullOrEmpty(request.Name_3))
                {
                    var _item = await _itemService.GetByIdAsync(Convert.ToInt32(request.Name_3));
                    response.Name_3 = _item.Name;
                    response.Amount_3 = request.Amount_3;
                }

                var member_ = await _memberService.GetByIdAsync(request.MemberId);
                response.Member = _mapper.Map<Member>(member_);

                var user = await _userService.GetByIdAsync((int)memberTopup.CreatedBy);
                response.UserName = user.Username;

                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }

        [HttpPatch(ApiRoutes.Members.MemberChangeReception)]
        public async Task<ActionResult> MemberChangeReception([FromBody] MemberChangeReceptionRequest request)
        {
            using (var transaction = await _db.Database.BeginTransactionAsync())
            {
                try
                {
                    var memberTransaction = await _memberService.GetMemberTransactionById(request.Id);
                    memberTransaction.ReceptionId = request.ReceptionId;
                    await _memberService.UpdateMemberTransactionAsync(memberTransaction);

                    var member = await _memberService.GetByIdAsync(memberTransaction.MemberId);
                    member.ReceptionId = request.ReceptionId;
                    await _memberService.UpdateAsync(member);

                    await transaction.CommitAsync();
                    return Ok();
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex.Message);
                    _logger.LogError(ex.StackTrace);
                    return BadRequest();
                }
            }
        }

        [HttpGet(ApiRoutes.Members.GetAllMemberPayments)]
        public async Task<ActionResult> GetAllMemberpayments([FromQuery] MemberHistorySearchQuery paginationQuery, int id)
        {
            try
            {
                var pagination = _mapper.Map<PaginationFilter>(paginationQuery);
                List<int> calendarIds = null;
                if (paginationQuery.StartDate != null && paginationQuery.EndDate != null)
                {
                    calendarIds = await _settingService.GetCalendarId((DateTime)paginationQuery.StartDate, (DateTime)paginationQuery.EndDate);
                }
                var memberPayment = await _memberService.GetMemberpayment(id, calendarIds, pagination);
                if (memberPayment == null)
                {
                    return NotFound();
                }
                return Ok(new PagedResponse<MemberPaymentResponse>
                {
                    Data = _mapper.Map<List<MemberPaymentResponse>>(memberPayment),
                    PageNumber = pagination.PageNumber,
                    PageSize = pagination.PageSize,
                    Keyword = pagination.Keyword,
                    OrderBy = pagination.OrderBy,
                    TotalRecords = pagination.TotalRecords,
                    OrderDirection = pagination.OrderDirection
                });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }

        [HttpDelete(ApiRoutes.Members.CancelMemberPayment)]
        public async Task<IActionResult> CancelMemberPayment([FromRoute] int id) 
        {
            using (var transaction = await _db.Database.BeginTransactionAsync())
            {
                try
                {
                    var memberPayment = await _memberService.GetMemberPaymentById(id);
                    if (memberPayment == null) 
                    {
                        NotFound();
                    }
                    var member = await _memberService.GetByIdAsync(memberPayment.MemberId);
                    if (member == null)
                    {
                        NotFound();
                    }
                    if (memberPayment.CerditAmount > 0) 
                    {
                        member.CreditAmount += memberPayment.CerditAmount;
                    }
                    await _memberService.UpdateMemberPayment(memberPayment);

                    if (member.MemberItems != null)
                    {
                        foreach (var item in member.MemberItems)
                        {
                            if (memberPayment.Name_1 != null) 
                            {
                               
                                if (item.ItemId == int.Parse(memberPayment.Name_1)) 
                                {
                                    item.Amount += memberPayment.Amount_1;
                                }
                            }
                            if (memberPayment.Name_2 != null) 
                            {
                                if (item.ItemId == int.Parse(memberPayment.Name_2)) 
                                {
                                    item.Amount += memberPayment.Amount_2;
                                }
                            }
                            await _memberService.UpdateMemberItem(item);
                         }
                    }
                    await _memberService.CancelMemberPayment(memberPayment.Id);

                    await transaction.CommitAsync();
                    return Ok();
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex.Message);
                    _logger.LogError(ex.StackTrace);
                    await transaction.RollbackAsync();
                    return BadRequest();
                }
            }
        }

        [HttpPost(ApiRoutes.Members.CreateMemberPointRedemtions)]
        public async Task<IActionResult> CreateMemberPointRedemtions([FromBody] MemberPointRedemtionRequest request)
        {
            using (var transaction = await _db.Database.BeginTransactionAsync())
            {
                try
                {
                    int? calendarDayId = await GetCalendarDayId();
                    var member = await _memberService.GetByIdAsync(request.MemberId);
                    MemberPointRedemtion memberPointRedemtion = new MemberPointRedemtion();
                    memberPointRedemtion.MemberId = member.Id;
                    memberPointRedemtion.CalendarDayId = (int)calendarDayId;
                    memberPointRedemtion.Point = member.Point;
                    memberPointRedemtion.PointRedem = request.PointRedem;
                    memberPointRedemtion.Remark = request.Remark;

                    await _memberPointRedemtionsService.CreateMemberPointRedemtions(memberPointRedemtion);

                    member.Point -= memberPointRedemtion.PointRedem;
                    await _memberService.UpdateAsync(member);

                    await transaction.CommitAsync();
                    return Ok();
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex.Message);
                    _logger.LogError(ex.StackTrace);
                    await transaction.RollbackAsync();
                    return BadRequest();
                }
            }
        }

        [HttpDelete(ApiRoutes.Members.CancelMemberPointRedemtions)]
        public async Task<IActionResult> CancelMemberPointRedemtions(int id)
        {
            try
            {
                var memberPointRedemtion = await _memberPointRedemtionsService.GetMemberPointRedemtionByMemberId(id);
                if(memberPointRedemtion == null) 
                {
                    return NotFound();
                }
                var member = await _memberService.GetByIdAsync(memberPointRedemtion.MemberId);

                member.Point += memberPointRedemtion.PointRedem;

                await _memberPointRedemtionsService.CancelMemberPointRedemtions(memberPointRedemtion.Id);
                await _memberService.UpdateAsync(member);
                return Ok();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }

        }

        [HttpGet(ApiRoutes.Members.GetMemberPointRedemtionsHistory)]
        public async Task<IActionResult> GetMemberPointRedemtionsHistory([FromQuery] MemberPointRedemtionsHistorySearchQuery paginationQuery) 
        {
            try 
            {
                var pagination = _mapper.Map<PaginationFilter>(paginationQuery);
                var memberPointRedemtionsHistory = await _memberPointRedemtionsService.GetMemberPointRedemtionsHistory(paginationQuery, pagination);
                if (memberPointRedemtionsHistory == null)
                {
                    return NotFound();
                }
                return Ok(new PagedResponse<MemberPointRedemtion>
                {
                    Data = _mapper.Map<List<MemberPointRedemtion>>(memberPointRedemtionsHistory),
                    PageNumber = pagination.PageNumber,
                    PageSize = pagination.PageSize,
                    Keyword = pagination.Keyword,
                    OrderBy = pagination.OrderBy,
                    TotalRecords = pagination.TotalRecords,
                    OrderDirection = pagination.OrderDirection
                });
            }
            catch (Exception ex) 
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }

        [HttpGet(ApiRoutes.Members.GetMemberAllPoint)]
        public async Task<IActionResult> GetMemberAllPoint([FromQuery] MemberSearchQuery paginationQuery)
        {
            try
            {
                var pagination = _mapper.Map<PaginationFilter>(paginationQuery);
                var member = await _memberService.GetAllAsync(paginationQuery, pagination);

                //var response = _mapper.Map<List<MemberResponse>>(member);
                var response = _mapper.Map<List<MemberResponse>>(member.OrderByDescending(i => i.Point));

                foreach (var item in response)
                {
                    foreach (var item2 in item.MemberItems)
                    {
                        var item1 = await _itemService.GetByIdAsync(item2.ItemId);
                        item2.Item = _mapper.Map<ItemResponse>(item1);

                    }
                }

                return Ok(new PagedResponse<MemberResponse>
                {
                    Data = response,
                    PageNumber = pagination.PageNumber,
                    PageSize = pagination.PageSize,
                    Keyword = pagination.Keyword,
                    OrderBy = pagination.OrderBy,
                    TotalRecords = pagination.TotalRecords,
                    OrderDirection = pagination.OrderDirection,
                });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }

        }

        private async Task<int> GetCalendarDayId()
        {
            var calendayDay = await _settingService.GetCarlendarDay();
            return calendayDay.Id;
        }

      
    }
}
