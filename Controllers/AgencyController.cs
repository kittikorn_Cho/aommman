﻿using AutoMapper;
using DocumentFormat.OpenXml.Office2010.ExcelAc;
using DocumentFormat.OpenXml.Office2021.DocumentTasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using OONApi.Contracts.ApiRoutes;
using OONApi.Contracts.Responses;
using OONApi.Services;
using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using OONApi.Contracts.Requests;
using OONApi.Enum;
using OONApi.Contracts.Requests.Queires;
using System.Linq;
using OONApi.Models;
using static OONApi.Contracts.ApiRoutes.ApiRoutes;

namespace OONApi.Controllers
{
    [ApiController]
    [Authorize]
    public class AgencyController : ControllerBase
    {
        private IAgencyService _agencyService;
        private IMassageService _massageService;
        private ISettingService _settingService;
        private IMapper _mapper;
        private ILogger<AgencyController> _logger;
        private readonly AppDbContext _db;
        public AgencyController(ISettingService settingService, IMassageService massageService, IAgencyService agencyService, IMapper mapper, ILogger<AgencyController> logger, AppDbContext db)
        {
            _agencyService = agencyService;
            _massageService = massageService;
            _settingService = settingService;
            _mapper = mapper;
            _logger = logger;
            _db = db;
        }


        [HttpGet(ApiRoutes.Agencies.GetAll)]
        public async Task<IActionResult> GetAll([FromQuery] AgencySearchQuery paginationQuery)
        {
            try
            {
                var pagination = _mapper.Map<PaginationFilter>(paginationQuery);
                var agency = await _agencyService.GetAllAsync(paginationQuery, pagination);
                if (agency == null)
                {
                    return NotFound();
                }
                //var response = _mapper.Map<List<AgencyResponse>>(agency);
                return Ok(new PagedResponse<AgencyResponse>
                {
                    Data = _mapper.Map<List<AgencyResponse>>(agency.OrderBy(a => a.Code)),
                    PageNumber = pagination.PageNumber,
                    PageSize = pagination.PageSize,
                    Keyword = pagination.Keyword,
                    OrderBy = pagination.OrderBy,
                    TotalRecords = pagination.TotalRecords,
                    OrderDirection = pagination.OrderDirection,
                });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }

        [HttpGet(ApiRoutes.Agencies.Get)]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                var agencyId = await _agencyService.GetByIdAsync(id);
                if (agencyId == null)
                {
                    return NotFound();
                }
                var response = _mapper.Map<AgencyResponse>(agencyId);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }

        [HttpPost(ApiRoutes.Agencies.Create)]
        public async Task<IActionResult> Post([FromBody] AgencyRequest request)
        {
            try
            {
                Agency agency = new Agency
                {
                    AgencyTypeId = request.AgencyTypeId,
                    Code = request.Code,
                    Firstname = request.Firstname,
                    Lastname = request.Lastname,
                    Nickname = request.Nickname,
                    Tel = request.Tel,
                    Address = request.Address,
                    WorkFromDate = DateTime.Now,
                    WorkToDate = null
                };
                await _agencyService.CreateAsync(agency);
                return Ok(_mapper.Map<AgencyResponse>(agency));
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }

        [HttpPut(ApiRoutes.Agencies.Update)]
        public async Task<IActionResult> Put([FromRoute] int id, [FromBody] AgencyRequest request)
        {
            try
            {
                var agency = await _agencyService.GetByIdAsync(id);
                if (agency == null)
                {
                    NotFound();
                }
                Agency update = agency;
                update.AgencyTypeId = request.AgencyTypeId;
                update.Code = request.Code;
                update.Firstname = request.Firstname;
                update.Lastname = request.Lastname;
                update.Nickname = request.Nickname;
                update.Tel = request.Tel;
                update.Address = request.Address;
                await _agencyService.UpdateAsync(update);
                return Ok(_mapper.Map<AgencyResponse>(update));
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }
        [HttpDelete(ApiRoutes.Agencies.Delete)]
        public async Task<IActionResult> Delete(int id)
        {
            using (var transaction = await _db.Database.BeginTransactionAsync())
            {
                try
                {
                    Agency agency = await _agencyService.GetByIdAsync(id);
                    if (agency == null)
                    {
                        return NotFound();
                    }
                    agency.Status = (int)EAngelStatus.Cancel;
                    await _agencyService.UpdateAsync(agency);
                    _db.ChangeTracker.Clear();

                    await transaction.CommitAsync();
                    return Ok();
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex.Message);
                    _logger.LogError(ex.StackTrace);
                    await transaction.RollbackAsync();
                    return BadRequest();
                }
            }
        }

        [HttpGet(ApiRoutes.Agencies.GetAllAgencyTypes)]
        public async Task<IActionResult> GetAllAgencyTypes()
        {
            try
            {
                var agencyTypes = await _agencyService.GetAllAgencyTypesAsync();
                var response = _mapper.Map<List<AgencyTypeResponse>>(agencyTypes);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }
    }
}
