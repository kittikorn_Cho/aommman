﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using OONApi.Contracts.ApiRoutes;
using OONApi.Contracts.Requests;
using OONApi.Contracts.Requests.Queires;
using OONApi.Contracts.Responses;
using OONApi.Models;
using OONApi.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace OONApi.Controllers
{
    [ApiController]
    [Authorize]
    public class BuildingTypeController : ControllerBase
    {
        private IMapper _mapper;
        private IBuildingTypeService _buildingTypeService;
        private ILogger<BuildingTypeController> _logger;

        public BuildingTypeController(IMapper mapper,IBuildingTypeService buildingTypeService, ILogger<BuildingTypeController> logger)
        {
            _mapper = mapper;
            _buildingTypeService = buildingTypeService;
            _logger = logger;
        }

        [HttpGet(ApiRoutes.BuildingTypes.GetAll)]
        public async Task<IActionResult> GetAll([FromQuery] PaginationQuery paginationQuery)
        {
            try
            {
                var pagination = _mapper.Map<PaginationFilter>(paginationQuery);
                var buildingTypes = await _buildingTypeService.GetAllAsync(pagination);
                if (buildingTypes == null)
                {
                    return NotFound();
                }
                return Ok(new PagedResponse<BuildingTypeResponse>
                {
                    Data = _mapper.Map<List<BuildingTypeResponse>>(buildingTypes),
                    PageNumber = pagination.PageNumber,
                    PageSize = pagination.PageSize,
                    Keyword = pagination.Keyword,
                    OrderBy = pagination.OrderBy,
                    TotalRecords = pagination.TotalRecords,
                    OrderDirection = pagination.OrderDirection,
                });
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }

        }

        [HttpGet(ApiRoutes.BuildingTypes.Get)]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                var buildingType = await _buildingTypeService.GetByIdAsync(id);
                if (buildingType == null)
                {
                    return NotFound();
                }
                return Ok(_mapper.Map<BuildingTypeResponse>(buildingType));
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }

        }


        [HttpPost(ApiRoutes.BuildingTypes.Create)]
        public async Task<IActionResult> Post([FromBody] BuildingTypeRequest request)
        {
            try
            {
                BuildingType buildingType = new BuildingType
                {
                    Name = request.Name
                };
                await _buildingTypeService.CreateAsync(buildingType);
                return Ok(_mapper.Map<BuildingTypeResponse>(buildingType));
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }

        }


        [HttpPut(ApiRoutes.BuildingTypes.Update)]
        public async Task<IActionResult> Put(int id, [FromBody] BuildingTypeRequest request)
        {
            try
            {
                var buildingType = await _buildingTypeService.GetByIdAsync(id);
                if (buildingType == null)
                {
                    return NotFound();
                }
                BuildingType update = buildingType;
                update.Name = request.Name;
                await _buildingTypeService.UpdateAsync(update);
                return Ok(_mapper.Map<BuildingType>(update));
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }

        }


        [HttpDelete(ApiRoutes.BuildingTypes.Delete)]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await _buildingTypeService.DeleteAsync(id);
                return Ok();
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }

        }
    }
}
