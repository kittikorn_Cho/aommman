﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using OONApi.Contracts.ApiRoutes;
using OONApi.Contracts.Requests;
using OONApi.Contracts.Requests.Queires;
using OONApi.Contracts.Responses;
using OONApi.Helpers;
using OONApi.Models;
using OONApi.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace OONApi.Controllers
{

    [ApiController]
    [Authorize]
    public class MemberTypeController : ControllerBase
    {
        private IMapper _mapper;
        private IMemberTypeService _memberTypeService;
        private IUriService _uriService;
        private ILogger<MemberTypeController> _logger;
        private readonly AppDbContext _db;

        public MemberTypeController(IMapper mapper,IMemberTypeService memberTypeService,IUriService uriService ,ILogger<MemberTypeController> logger,AppDbContext db)
        {
            _mapper = mapper;
            _memberTypeService = memberTypeService;
            _uriService = uriService;
            _logger = logger;
            _db = db;
        }

        [HttpGet(ApiRoutes.MemberTypes.GetAll)]
        public async Task<IActionResult> GetAll([FromQuery] PaginationQuery paginationQuery)
        {
            try
            {
                var pagination = _mapper.Map<PaginationFilter>(paginationQuery);
                var memberTypes = await _memberTypeService.GetAllAsync(pagination);
                if (memberTypes == null)
                {
                    return NotFound();
                }
                var memberTypeResponse = _mapper.Map<List<MemberTypeResponse>>(memberTypes);
                if (memberTypeResponse == null)
                {
                    return NotFound();
                }
                foreach (var memberType in memberTypeResponse)
                {
                    var memberTypeItem = await _memberTypeService.GetMemberTypeItems(memberType.Id);
                    if (memberTypeItem == null)
                    {
                        return NotFound();
                    }
                    memberType.MemberTypeItems = _mapper.Map<List<MemberTypeItemResponse>>(memberTypeItem);
                }

                if (pagination == null || pagination.PageNumber < 1 || pagination.PageSize < 1)
                {
                    return Ok(new PagedResponse<MemberTypeResponse>(memberTypeResponse));
                }

                var resp = PaginationHelpers.CreatePaginationResponse(_uriService, pagination, memberTypeResponse);
                if (resp == null)
                {
                    return NotFound();
                }
                return Ok(resp);
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }

        }


        [HttpGet(ApiRoutes.MemberTypes.Get)]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                var memberType = await _memberTypeService.GetByIdAsync(id);
                if (memberType == null)
                {
                    return NotFound();
                }
                return Ok(_mapper.Map<MemberTypeResponse>(memberType));
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }

        }


        [HttpPost(ApiRoutes.MemberTypes.Create)]
        public async Task<IActionResult> Post([FromBody] MemberTypeRequest request)
        {
            using (var transaction = await _db.Database.BeginTransactionAsync())
            {
                try
                {
                    MemberType memberType = new MemberType
                    {
                        Code = request.Code,
                        Fee = request.Fee
                    };
                    await _memberTypeService.CreateAsync(memberType);

                    List<MemberTypeItem> memberTypeItems = new List<MemberTypeItem>();
                    foreach (var item in request.MemberTypeItems)
                    {
                        if (item.Amount != 0)
                        {
                            MemberTypeItem memberTypeItem = new MemberTypeItem();
                            memberTypeItem.MemberTypeId = memberType.Id;
                            memberTypeItem.ItemId = item.ItemId;
                            memberTypeItem.Amount = item.Amount;
                            memberTypeItems.Add(memberTypeItem);
                        }
                    }

                    await _memberTypeService.CreateMemberTypeItemAsync(memberTypeItems);
                    await transaction.CommitAsync();

                    return Ok(_mapper.Map<MemberTypeResponse>(memberType));
                }
                catch (Exception ex)
                {
                    await transaction.RollbackAsync();
                    _logger.LogError(ex.Message);
                    _logger.LogError(ex.StackTrace);
                    return BadRequest();
                }
            }
               

        }


        [HttpPut(ApiRoutes.MemberTypes.Update)]
        public async Task<IActionResult> Put(int id, [FromBody] MemberTypeRequest request)
        {
            using (var transaction = await _db.Database.BeginTransactionAsync())
            {
                try
                {
                    var membertypes = await _memberTypeService.GetByIdAsync(id);
                    if (membertypes == null)
                    {
                        return NotFound();
                    }

                    membertypes.Code = request.Code;
                    membertypes.Fee = request.Fee;
                    await _memberTypeService.UpdateAsync(membertypes);
                    _db.ChangeTracker.Clear();

                    List<MemberTypeItem> addMemberTypeItems = new List<MemberTypeItem>();
                    List<MemberTypeItem> updateMemberTypeItems = new List<MemberTypeItem>();
                    List<int> memberTypeItemIds = new List<int>();
                    foreach (var item in request.MemberTypeItems)
                    {
                        if (item.Id != null)
                        {
                            MemberTypeItem temp = new MemberTypeItem
                            {
                                Id = (int)item.Id,
                                MemberTypeId = membertypes.Id,
                                ItemId = item.ItemId,
                                Amount = item.Amount
                            };
                            updateMemberTypeItems.Add(temp);
                        }
                        else
                        {
                            MemberTypeItem temp = new MemberTypeItem
                            {
                                MemberTypeId = membertypes.Id,
                                ItemId = item.ItemId,
                                Amount = item.Amount
                            };
                            addMemberTypeItems.Add(temp);
                        }
                    }

                    if(addMemberTypeItems.Count > 0)
                    {
                        await _memberTypeService.CreateMemberTypeItemAsync(addMemberTypeItems);
                    }
                    if(updateMemberTypeItems.Count > 0)
                    {
                        await _memberTypeService.UpdateMemberTypeItemAsync(updateMemberTypeItems);
                    }
                    
                    await transaction.CommitAsync();

                    return Ok(_mapper.Map<MemberTypeResponse>(membertypes));
                }
                catch (Exception ex)
                {
                    await transaction.RollbackAsync();
                    _logger.LogError(ex.Message);
                    _logger.LogError(ex.StackTrace);
                    return BadRequest();
                }
            }
        }


        [HttpDelete(ApiRoutes.MemberTypes.Delete)]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await _memberTypeService.DeleteAsync(id);
                return Ok();
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }

        }
    }
}
