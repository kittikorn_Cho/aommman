﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using OONApi.Contracts.ApiRoutes;
using OONApi.Contracts.Requests;
using OONApi.Contracts.Requests.Queires;
using OONApi.Contracts.Responses;
using OONApi.Models;
using OONApi.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OONApi.Controllers
{
    [ApiController]
    [Authorize]
    public class AngelDoctorController : ControllerBase
    {
        private IAngelDoctorService _angelDoctorServie;
        private IMapper _mapper;
        private ILogger<AngelDoctorController> _logger;
        public AngelDoctorController(IAngelDoctorService angelDoctorServie, IMapper mapper, ILogger<AngelDoctorController> logger)
        {
            _angelDoctorServie = angelDoctorServie;
            _mapper = mapper;
            _logger = logger;
        }

        [HttpGet(ApiRoutes.AngelDoctors.GetAll)]
        public async Task<IActionResult> GetAll([FromQuery] AngelDoctorSearchQuery paginationQuery)
        {
            try
            {
                var pagination = _mapper.Map<PaginationFilter>(paginationQuery);
                var angelDoctor = await _angelDoctorServie.GetAllAsync(paginationQuery, pagination);
                if (angelDoctor == null)
                {
                    return NotFound();
                }

                return Ok(new PagedResponse<AngelDoctorResponse>
                {
                    Data = _mapper.Map<List<AngelDoctorResponse>>(angelDoctor),
                    PageNumber = pagination.PageNumber,
                    PageSize = pagination.PageSize,
                    Keyword = pagination.Keyword,
                    OrderBy = pagination.OrderBy,
                    TotalRecords = pagination.TotalRecords,
                    OrderDirection = pagination.OrderDirection,
                });
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }

        }

        [HttpGet(ApiRoutes.AngelDoctors.Get)]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                var angelDoctor = await _angelDoctorServie.GetByIdAsync(id);
                if (angelDoctor == null)
                {
                    return NotFound();
                }
                return Ok(_mapper.Map<AngelDoctorResponse>(angelDoctor));
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }

        }

        [HttpPost(ApiRoutes.AngelDoctors.Create)]
        public async Task<IActionResult> Post([FromBody] AngelDoctorRequest request)
        {
            try
            {
                AngelDoctor angelDoctor = new AngelDoctor
                {
                    AngelId = request.AngelId,
                    DoctorName = request.DoctorName,
                    Detail = request.Detail
                };
                await _angelDoctorServie.CreateAsync(angelDoctor);
                return Ok(_mapper.Map<AngelDoctorResponse>(angelDoctor));
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }

        }

        [HttpPut(ApiRoutes.AngelDoctors.Update)]
        public async Task<IActionResult> Put([FromRoute] int id, [FromBody] AngelDoctorRequest request)
        {
            try
            {
                var angelDoctor = await _angelDoctorServie.GetByIdAsync(id);
                if (angelDoctor == null)
                {
                    return NotFound();
                }
                AngelDoctor update = angelDoctor;
                update.AngelId = request.AngelId;
                update.DoctorName = request.DoctorName;
                update.Detail = request.Detail;
                await _angelDoctorServie.UpdateAsync(update);
                return Ok(_mapper.Map<AngelDoctorResponse>(update));
                
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }

        }

        [HttpDelete(ApiRoutes.AngelDoctors.Delete)]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await _angelDoctorServie.DeleteAsync(id);
                return Ok();
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }

        }
    }
}
