﻿using AutoMapper;
using ClosedXML.Excel;
using DinkToPdf;
using DinkToPdf.Contracts;
using DocumentFormat.OpenXml.Bibliography;
using DocumentFormat.OpenXml.Office2010.ExcelAc;
using DocumentFormat.OpenXml.Office2016.Excel;
using DocumentFormat.OpenXml.Spreadsheet;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using OONApi.Contracts.ApiRoutes;
using OONApi.Contracts.Requests;
using OONApi.Contracts.Responses;
using OONApi.Enum;
using OONApi.Models;
using OONApi.Services;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using static OONApi.Contracts.ApiRoutes.ApiRoutes;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace OONApi.Controllers
{
    [ApiController]
    public class ReportController : ControllerBase
    {
        private IAgencyPerformanceService _agencyPerformanceService;
        private IAgencyService _agencyService;
        private ILogger<ReportController> _logger;
        private IMapper _mapper;
        private IReportService _reportService;
        private ISettingService _settingService;
        private IUserService _userService;
        private IAngelTypeService _angelTypeService;
        private IConverter _converter;
        private IGenerateReportService _generateReportService;
        private IReceptionService _receptionService;
        private IMemberService _memberService;
        private IAngelService _angelService;
        private IAngelPerformanceService _angelPerformanceService;
        private IRoomService _roomService;
        private IDebtTypeService _debtTypeService;
        private IDeductTypeService _deductTypeService;
        private IMassageService _massageService;
        private ICountriesService _countriesService;
        private IItemService _itemService;
        private readonly IWebHostEnvironment _hostingEnvironment;

        public ReportController(IAgencyPerformanceService agencyPerformanceService, IAgencyService agencyService, IMapper mapper, ILogger<ReportController> logger, IReportService reportService, ISettingService settingService, IUserService userService, IAngelTypeService angelTypeService, IConverter converter, IGenerateReportService generateReportService, IWebHostEnvironment hostingEnvironment, IReceptionService receptionService, IMemberService memberService, IAngelService angelService, IAngelPerformanceService angelPerformanceService, IRoomService roomService, IDebtTypeService debtTypeService, IDeductTypeService deductTypeService, IMassageService massageService, ICountriesService countriesService, IItemService itemService)
        {
            _agencyPerformanceService = agencyPerformanceService;
            _agencyService = agencyService;
            _mapper = mapper;
            _logger = logger;
            _reportService = reportService;
            _settingService = settingService;
            _userService = userService;
            _angelTypeService = angelTypeService;
            _converter = converter;
            _generateReportService = generateReportService;
            _hostingEnvironment = hostingEnvironment;
            _receptionService = receptionService;
            _memberService = memberService;
            _angelService = angelService;
            _angelPerformanceService = angelPerformanceService;
            _roomService = roomService;
            _debtTypeService = debtTypeService;
            _deductTypeService = deductTypeService;
            _massageService = massageService;
            _countriesService = countriesService;
            _itemService = itemService;
        }
        [HttpGet(ApiRoutes.Reports.ReportMemberSell)]
        public async Task<IActionResult> ReportMemberSell([FromQuery] StartDateEndDateRequestcs request)
        {
            try
            {
                List<int> calendarIds = await _settingService.GetCalendarId(request.StartDate, request.EndDate);
                var report = await _reportService.MemberSell(calendarIds);
                var response = _mapper.Map<List<ReportMemberSellResponse>>(report);
                var setting = await _settingService.GetSettingById(1);
                var responseSetting = _mapper.Map<SettingResponse>(setting);
                foreach (var item in response)
                {

                    if (item.CreatedBy != null)
                    {
                        var user = await _userService.GetByIdToReportAsync((int)item.CreatedBy);

                        if (user != null)
                        {
                            item.User = _mapper.Map<UserResponse>(user);
                        }
                    }


                }

                _logger.LogDebug("report member-sell json =" + response.ToArray());

                var globalSettings = new GlobalSettings
                {
                    ColorMode = ColorMode.Color,
                    Orientation = Orientation.Landscape,
                    PaperSize = PaperKind.A4,
                    Margins = new MarginSettings { Top = 10 },
                    DocumentTitle = "PDF Report",
                    //Out = _hostingEnvironment.WebRootPath + "/pdf/report.pdf",
                };

                var objectSettings = new ObjectSettings
                {
                    PagesCount = true,
                    HtmlContent = _generateReportService.MemberSell(request.StartDate, request.EndDate, response, responseSetting),
                    WebSettings = { DefaultEncoding = "utf-8", UserStyleSheet = _hostingEnvironment.WebRootPath + "/assets/pdf.css" },
                };

                var pdf = new HtmlToPdfDocument()
                {
                    GlobalSettings = globalSettings,
                    Objects = { objectSettings }
                };
                //return Ok(response);
                var file = _converter.Convert(pdf);
                return File(file, "application/pdf");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }

        [HttpGet(ApiRoutes.Reports.ReportMemberSellExcel)]
        public async Task<IActionResult> ReportMemberSellExcel([FromQuery] StartDateEndDateRequestcs request)
        {
            try
            {
                List<int> calendarIds = await _settingService.GetCalendarId(request.StartDate, request.EndDate);
                var report = await _reportService.MemberSell(calendarIds);
                var response = _mapper.Map<List<ReportMemberSellResponse>>(report);
                foreach (var item in response)
                {

                    if (item.CreatedBy != null)
                    {
                        var user = await _userService.GetByIdToReportAsync((int)item.CreatedBy);

                        if (user != null)
                        {
                            item.User = _mapper.Map<UserResponse>(user);
                        }
                    }
                }

                using (var workbook = new XLWorkbook())
                {
                    var worksheet = workbook.Worksheets.Add("MemberSell");
                    var currentRow = 1;
                    worksheet.Cell(currentRow, 1).Value = "ลำดับ";
                    worksheet.Cell(currentRow, 2).Value = "วันที่สมัคร";
                    worksheet.Cell(currentRow, 3).Value = "เมมเบอร์";
                    worksheet.Cell(currentRow, 4).Value = "ชื่อ";
                    worksheet.Cell(currentRow, 5).Value = "เชียร์แขก";
                    worksheet.Cell(currentRow, 6).Value = "ราคา";
                    worksheet.Cell(currentRow, 7).Value = "เงินสด";
                    worksheet.Cell(currentRow, 8).Value = "บัตรเครดิต";
                    worksheet.Cell(currentRow, 9).Value = "ค้างชำระ";
                    worksheet.Cell(currentRow, 10).Value = "เปอร์เซนต์";
                    worksheet.Cell(currentRow, 11).Value = "ยอดเปอร์เซนต์";
                    worksheet.Cell(currentRow, 12).Value = "ผู้ทำรายการ";
                    int i = 0;

                    foreach (var item in response)
                    {
                        string toDay = item.CalendarDay.ToDay.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));
                        i++;
                        decimal unPaid = item.Total - (item.CashTotal + item.CreditTotal + item.EntertainTotal);
                        string user = "";
                        if (item.User != null)
                        {
                            user = item.User.UserFullname;
                        }

                        currentRow++;
                        worksheet.Cell(currentRow, 1).Value = i;
                        worksheet.Cell(currentRow, 2).Value = toDay;
                        worksheet.Cell(currentRow, 3).Value = item.Member.Code;
                        worksheet.Cell(currentRow, 4).Value = item.Member.Firstname + " " + item.Member.Lastname;
                        worksheet.Cell(currentRow, 5).Value = item.Reception.Nickname;
                        worksheet.Cell(currentRow, 6).Value = item.Total;
                        worksheet.Cell(currentRow, 7).Value = item.CashTotal;
                        worksheet.Cell(currentRow, 8).Value = item.CreditTotal;
                        worksheet.Cell(currentRow, 9).Value = unPaid;
                        worksheet.Cell(currentRow, 10).Value = item.PercentTopup;
                        worksheet.Cell(currentRow, 11).Value = item.PercentTotalTopup;
                        worksheet.Cell(currentRow, 12).Value = user;
                    }

                    using (var stream = new MemoryStream())
                    {
                        workbook.SaveAs(stream);
                        var content = stream.ToArray();

                        return File(content, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "memberSell.xlsx");
                    }

                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.Message);
                return BadRequest();
            }
        }

        [HttpGet(ApiRoutes.Reports.ReportAngelPerformanceExcel)]
        public async Task<IActionResult> ReportAngelPerformanceExcel([FromQuery] StartDateEndDateRequestcs request)
        {
            try
            {
                List<int> calendarIds = await _settingService.GetCalendarId(request.StartDate, request.EndDate);
                var report = await _reportService.GetAngelPerformances(calendarIds);
                var response = _mapper.Map<List<AngelPerformanceResponse>>(report);
                foreach (var item in response)
                {
                    var massageAngel = await _reportService.GetMassageAngelsByAngelIdAndAngelPerformancesId(item.Id, item.AngelId);
                    item.MassageAngels = _mapper.Map<List<MassageAngelResponse>>(massageAngel);
                }
                using (var workbook = new XLWorkbook())
                {
                    var worksheet = workbook.Worksheets.Add("AngelPerformance");
                    var currentRow = 1;
                    worksheet.Cell(currentRow, 1).Value = "วันที่";
                    worksheet.Cell(currentRow, 2).Value = "พนักงาน";
                    worksheet.Cell(currentRow, 3).Value = "รอบ";
                    worksheet.Cell(currentRow, 4).Value = "ค่าตอบแทน";
                    worksheet.Cell(currentRow, 5).Value = "ค่าคอม";
                    worksheet.Cell(currentRow, 6).Value = "ค่าตะกร้า";
                    worksheet.Cell(currentRow, 7).Value = "ค่าเสริมสวย";
                    worksheet.Cell(currentRow, 8).Value = "หักหนี้ค้างชำระ";
                    worksheet.Cell(currentRow, 9).Value = "คงเหลือ";
                    worksheet.Cell(currentRow, 10).Value = "รับแล้ว";
                    worksheet.Cell(currentRow, 11).Value = "ค้างจ่าย";

                    decimal totalRound = 0;
                    decimal totalWage = 0;
                    decimal totalDeduct = 0;
                    decimal totalDailyDebt = 0;
                    decimal totalDebt = 0;
                    decimal totalReceive = 0;
                    decimal totalReceive_ = 0;
                    decimal totalUnPaid = 0;

                    foreach (var item in response)
                    {
                        string toDay = item.CalendarDay.ToDay.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));
                        if (item.TotalWage == 0 && item.Angel.AngelType.Code != "A")
                        {
                            currentRow++;
                            decimal round = 0;
                            decimal wage = 0;
                            if (item.MassageAngels == null)
                            {
                                return NotFound();
                            }
                            foreach (var massageAngel in item.MassageAngels)
                            {
                                round += massageAngel.Round;
                                wage += item.Angel.AngelType.Wage * massageAngel.Round;
                            }
                            decimal receive = (wage - 100);

                            worksheet.Cell(currentRow, 1).Value = toDay;
                            worksheet.Cell(currentRow, 2).Value = item.Angel.Code;
                            worksheet.Cell(currentRow, 3).Value = round;
                            worksheet.Cell(currentRow, 4).Value = wage;
                            worksheet.Cell(currentRow, 5).Value = 0;
                            worksheet.Cell(currentRow, 6).Value = 0;
                            worksheet.Cell(currentRow, 7).Value = 100;
                            worksheet.Cell(currentRow, 8).Value = 0;
                            worksheet.Cell(currentRow, 9).Value = receive;
                            worksheet.Cell(currentRow, 10).Value = 0;
                            worksheet.Cell(currentRow, 11).Value = receive;

                            totalRound += round;
                            totalWage += wage;
                            totalDailyDebt += 100;
                            totalReceive += receive;
                            totalUnPaid += receive;
                        }
                        else
                        {
                            currentRow++;
                            worksheet.Cell(currentRow, 1).Value = toDay;
                            worksheet.Cell(currentRow, 2).Value = item.Angel.Code;
                            worksheet.Cell(currentRow, 3).Value = item.TotalRound;
                            worksheet.Cell(currentRow, 4).Value = item.TotalWage;
                            worksheet.Cell(currentRow, 5).Value = item.TotalDeduct;
                            worksheet.Cell(currentRow, 6).Value = 0;
                            worksheet.Cell(currentRow, 7).Value = item.TotalDailyDebt;
                            worksheet.Cell(currentRow, 8).Value = item.TotalDebt;
                            worksheet.Cell(currentRow, 9).Value = item.TotalReceive;
                            worksheet.Cell(currentRow, 10).Value = item.TotalReceive;
                            worksheet.Cell(currentRow, 11).Value = 0;

                            totalRound += item.TotalRound;
                            totalWage += item.TotalWage;
                            totalDeduct += item.TotalDeduct;
                            totalDailyDebt += item.TotalDailyDebt;
                            totalDebt += item.TotalDebt;
                            totalReceive += item.TotalReceive;
                            totalReceive_ += item.TotalReceive;
                        }
                    }
                    currentRow++;
                    currentRow++;
                    worksheet.Cell(currentRow, 1).Value = "รวม";
                    worksheet.Cell(currentRow, 3).Value = totalRound;
                    worksheet.Cell(currentRow, 4).Value = totalWage;
                    worksheet.Cell(currentRow, 5).Value = totalDeduct;
                    worksheet.Cell(currentRow, 6).Value = 0;
                    worksheet.Cell(currentRow, 7).Value = totalDailyDebt;
                    worksheet.Cell(currentRow, 8).Value = totalDebt;
                    worksheet.Cell(currentRow, 9).Value = totalReceive;
                    worksheet.Cell(currentRow, 10).Value = totalReceive_;
                    worksheet.Cell(currentRow, 11).Value = totalUnPaid;

                    using (var stream = new MemoryStream())
                    {
                        workbook.SaveAs(stream);
                        var content = stream.ToArray();

                        return File(content, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "AngelPerformance.xlsx");
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.Message);
                return BadRequest();
            }
        }

        [HttpGet(ApiRoutes.Reports.ReportPaymentAngelPerformance)]
        public async Task<IActionResult> ReportPaymentAngelPerformance([FromQuery] StartDateEndDateRequestcs request)
        {
            try
            {
                List<int> calendarIds = await _settingService.GetCalendarId(request.StartDate, request.EndDate);
                var angelTypes = await _angelTypeService.GetAllAsync(null);
                var setting = await _settingService.GetSettingById(1);
                string angelTypeCode = "";
                if (setting.MassageCode == "CV")
                {
                    angelTypeCode = "T";
                }
                else if (setting.MassageCode == "MR")
                {
                    angelTypeCode = "A";
                }
                else if (setting.MassageCode == "TB")
                {
                    angelTypeCode = "O";
                }
                var a = angelTypes.Where(b => b.Code != angelTypeCode);
                var response = _mapper.Map<List<ReportPaymentAngelPerformanceResponse>>(a);
                decimal totalSummary = 0;
                foreach (var item in response)
                {
                    item.Summary = 0;
                    item.TotalRound = _reportService.TotalAngelType(calendarIds, item.Id);
                    item.Summary = item.Wage * item.TotalRound;
                    totalSummary += item.Summary;
                }

                List<int> angelPerofrmanceids = _reportService.GetIdsAngelperformances(calendarIds);

                var debtTypes = await _debtTypeService.GetAllAsync(null);
                List<DebtTypeResponse> debtTypeList = new List<DebtTypeResponse>();
                foreach (var debtType in debtTypes)
                {
                    DebtTypeResponse debt = new DebtTypeResponse();
                    debt.Name = debtType.Name;
                    debt.SummaryPerformance = _reportService.SummaryAngelperformanceDetailDebtType(angelPerofrmanceids, debtType.Id);

                    debtTypeList.Add(debt);
                }

                var deductTypes = await _deductTypeService.GetAllAsync(null);

                var massageAngelPerformances = await _reportService.GetMassageAngelsPerformance(calendarIds, angelTypeCode);
                List<DeductTypeResponse> deductTypeList = new List<DeductTypeResponse>();

                foreach (var deductType in deductTypes)
                {
                    DeductTypeResponse sumDeductType = new DeductTypeResponse();

                    foreach (var massageAngel in massageAngelPerformances)
                    {
                        if (massageAngel.PaymentTypeId == (int)EPaymentType.Cash && deductType.Slug == "Comm")
                        {
                            sumDeductType.Name = deductType.Name;
                            sumDeductType.SummaryPerformance += 0;
                            sumDeductType.Slug = deductType.Slug;
                        }
                        else
                        {
                            sumDeductType.Name = deductType.Name;

                            if (deductType.Slug == "Clean")
                            {
                                decimal round = Math.Ceiling(massageAngel.Round);
                                sumDeductType.SummaryPerformance += round * deductType.Fee;
                                sumDeductType.Slug = deductType.Slug;
                            }
                            else
                            {
                                sumDeductType.SummaryPerformance += massageAngel.Round * deductType.Fee;
                                sumDeductType.Slug = deductType.Slug;
                            }
                        }
                    }
                    deductTypeList.Add(sumDeductType);
                }




                SummaryTotalDebtResponse responseSummaryDebt = new SummaryTotalDebtResponse();

                responseSummaryDebt.TotalDailyDebt = _reportService.TotalDailyDebt(calendarIds);
                responseSummaryDebt.TotalDebt = _reportService.TotalDebt(calendarIds);
                responseSummaryDebt.TotalDeduct = _reportService.TotalDeduct(calendarIds);
                var totalTip = _reportService.SumTotalPaymentDailyFinance(calendarIds);
                responseSummaryDebt.TotalTip = totalTip.TotalTip;
                responseSummaryDebt.DamagesTotal = totalTip.TotalDamages;
                responseSummaryDebt.TipQrCodeTotal = totalTip.TipQrCodeTotal;
                //responseSummaryDebt.TotalSummaryDebt = responseSummaryDebt.TotalDailyDebt + responseSummaryDebt.TotalDebt + responseSummaryDebt.TotalDeduct + responseSummaryDebt.TotalTip;

                var globalSettings = new GlobalSettings
                {
                    ColorMode = ColorMode.Color,
                    Orientation = Orientation.Portrait,
                    PaperSize = PaperKind.A4,
                    Margins = new MarginSettings { Top = 10 },
                    DocumentTitle = "PDF Report",
                    //Out = _hostingEnvironment.WebRootPath + "/pdf/report.pdf",
                };

                var objectSettings = new ObjectSettings
                {
                    PagesCount = true,
                    HtmlContent = _generateReportService.PaymentAngelPerformance(request.StartDate, request.EndDate, response, responseSummaryDebt, debtTypeList, deductTypeList),
                    WebSettings = { DefaultEncoding = "utf-8", UserStyleSheet = _hostingEnvironment.WebRootPath + "/assets/pdf.css" },
                };

                var pdf = new HtmlToPdfDocument()
                {
                    GlobalSettings = globalSettings,
                    Objects = { objectSettings }
                };

                var file = _converter.Convert(pdf);
                return File(file, "application/pdf");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }

        [HttpGet(ApiRoutes.Reports.ReportSummaryDay)]
        public async Task<IActionResult> ReportSummaryDay([FromQuery] StartDateEndDateRequestcs request)
        {
            try
            {
                List<int> calendarIds = await _settingService.GetCalendarId(request.StartDate, request.EndDate);

                var angelTypes = await _angelTypeService.GetAllAsync(null);
                var response = _mapper.Map<List<ReportSummaryDayResponse>>(angelTypes);
                decimal totalSummary = 0;
                decimal totalRound = 0;
                var setting = await _settingService.GetSettingById(1);
                var responseSetting = _mapper.Map<SettingResponse>(setting);
                foreach (var item in response)
                {
                    item.Summary = 0;
                    item.TotalRound = _reportService.TotalAngelType(calendarIds, item.Id);
                    item.Summary = item.Fee * item.TotalRound;
                    totalSummary += item.Summary;
                    totalRound += item.TotalRound;
                }

                SummaryMassageResponse responseSummary = _reportService.SummaryMassage(calendarIds);

                var globalSettings = new GlobalSettings
                {
                    ColorMode = ColorMode.Color,
                    Orientation = Orientation.Portrait,
                    PaperSize = PaperKind.A4,
                    Margins = new MarginSettings { Top = 10 },
                    DocumentTitle = "PDF Report",
                    //Out = _hostingEnvironment.WebRootPath + "/pdf/report.pdf",
                };

                var objectSettings = new ObjectSettings
                {
                    PagesCount = true,
                    HtmlContent = _generateReportService.SummaryDay(request.StartDate, request.EndDate, response, responseSummary, responseSetting),
                    WebSettings = { DefaultEncoding = "utf-8", UserStyleSheet = _hostingEnvironment.WebRootPath + "/assets/pdf.css" },
                };

                var pdf = new HtmlToPdfDocument()
                {
                    GlobalSettings = globalSettings,
                    Objects = { objectSettings }
                };

                var file = _converter.Convert(pdf);
                return File(file, "application/pdf");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }

        [HttpGet(ApiRoutes.Reports.ReportMemberPayment)]
        public async Task<IActionResult> ReportMemberPayment([FromQuery] StartDateEndDateRequestcs request)
        {
            try
            {
                List<int> calendarIds = await _settingService.GetCalendarId(request.StartDate, request.EndDate);
                var member = await _reportService.MemberPayment(calendarIds);

                var response = _mapper.Map<List<ReportMemberPaymentResponse>>(member);

                foreach (var item in response)
                {
                    var massageMemberAndRooms = await _reportService.GetMassageMemberPaymentAndRoom(item.Id);
                    if (massageMemberAndRooms.Count == 0)
                    {
                        var massageRoom = await _reportService.ListMassageRoomByMassageId(item.MassageId);
                        item.MassageRooms = _mapper.Map<List<MassageRoomResponse>>(massageRoom);
                    }
                    else
                    {
                        item.MassageMemberAndRooms = _mapper.Map<List<MassageMemberAndRoomResponse>>(massageMemberAndRooms);

                    }

                }
                var globalSettings = new GlobalSettings
                {
                    ColorMode = ColorMode.Color,
                    Orientation = Orientation.Portrait,
                    PaperSize = PaperKind.A4,
                    Margins = new MarginSettings { Top = 10 },
                    DocumentTitle = "PDF Report",
                    //Out = _hostingEnvironment.WebRootPath + "/pdf/report.pdf",
                };

                var objectSettings = new ObjectSettings
                {
                    PagesCount = true,
                    HtmlContent = _generateReportService.MemberPayment(request.StartDate, request.EndDate, response),
                    WebSettings = { DefaultEncoding = "utf-8", UserStyleSheet = _hostingEnvironment.WebRootPath + "/assets/pdf.css" },
                };

                var pdf = new HtmlToPdfDocument()
                {
                    GlobalSettings = globalSettings,
                    Objects = { objectSettings }
                };

                var file = _converter.Convert(pdf);
                return File(file, "application/pdf");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }
        [HttpGet(ApiRoutes.Reports.ReportDiscountMassageRoom)]
        public async Task<IActionResult> ReportDiscountMassageRoom([FromQuery] StartDateEndDateRequestcs request)
        {
            try
            {
                List<int> calendarIds = await _settingService.GetCalendarId(request.StartDate, request.EndDate);
                var massaageRooms = await _reportService.DiscountMassageRoom(calendarIds);

                var response = _mapper.Map<List<ReportDiscountRoomResponse>>(massaageRooms);

                var globalSettings = new GlobalSettings
                {
                    ColorMode = ColorMode.Color,
                    Orientation = Orientation.Portrait,
                    PaperSize = PaperKind.A4,
                    Margins = new MarginSettings { Top = 10 },
                    DocumentTitle = "PDF Report",
                    //Out = _hostingEnvironment.WebRootPath + "/pdf/report.pdf",
                };

                var objectSettings = new ObjectSettings
                {
                    PagesCount = true,
                    HtmlContent = _generateReportService.DiscountMassageRoom(request.StartDate, request.EndDate, response),
                    WebSettings = { DefaultEncoding = "utf-8", UserStyleSheet = _hostingEnvironment.WebRootPath + "/assets/pdf.css" },
                };

                var pdf = new HtmlToPdfDocument()
                {
                    GlobalSettings = globalSettings,
                    Objects = { objectSettings }
                };

                var file = _converter.Convert(pdf);

                return File(file, "application/pdf");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }
        [HttpGet(ApiRoutes.Reports.ReportDiscountMassageAngel)]
        public async Task<IActionResult> ReportDiscountMassageAngel([FromQuery] StartDateEndDateRequestcs request)
        {
            try
            {
                List<int> calendarIds = await _settingService.GetCalendarId(request.StartDate, request.EndDate);
                var massaageAngels = await _reportService.DiscountMaasageAngel(calendarIds);

                var response = _mapper.Map<List<ReportDiscountAngelResponse>>(massaageAngels);

                var globalSettings = new GlobalSettings
                {
                    ColorMode = ColorMode.Color,
                    Orientation = Orientation.Portrait,
                    PaperSize = PaperKind.A4,
                    Margins = new MarginSettings { Top = 10 },
                    DocumentTitle = "PDF Report",
                    //Out = _hostingEnvironment.WebRootPath + "/pdf/report.pdf",
                };

                var objectSettings = new ObjectSettings
                {
                    PagesCount = true,
                    HtmlContent = _generateReportService.DiscountMassageAngel(request.StartDate, request.EndDate, response),
                    WebSettings = { DefaultEncoding = "utf-8", UserStyleSheet = _hostingEnvironment.WebRootPath + "/assets/pdf.css" },
                };

                var pdf = new HtmlToPdfDocument()
                {
                    GlobalSettings = globalSettings,
                    Objects = { objectSettings }
                };

                var file = _converter.Convert(pdf);
                return File(file, "application/pdf");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }
        //[HttpGet(ApiRoutes.Reports.ReportUnpaidBill)]
        //public async Task<IActionResult> ReportUnpaidBill([FromQuery] StartDateEndDateRequestcs request)
        //{
        //    try
        //    {
        //        List<int> calendarIds = await _settingService.GetCalendarId(request.StartDate, request.EndDate);
        //        var massages = await _reportService.UnpaidBill(calendarIds);

        //        var response = _mapper.Map<List<ReportUnpaidBillResponse>>(massages);

        //        foreach (var resp in response)
        //        {
        //            var massage = await _reportService.GetIdReceptionInMassageAngelFirstRow(resp.Id);

        //            if (massage != null)
        //            {
        //                var reception = await _receptionService.GetByIdAsync(massage.ReceptionId);

        //                resp.reception = _mapper.Map<ReceptionResponse>(reception);
        //            }

        //            if (resp.UpdatedBy > 0)
        //            {
        //                var userCreateBy = await _userService.GetByIdAsync(resp.UpdatedBy);

        //                resp.user = _mapper.Map<UserResponse>(userCreateBy);
        //            }


        //        }

        //        var globalSettings = new GlobalSettings
        //        {
        //            ColorMode = ColorMode.Color,
        //            Orientation = Orientation.Portrait,
        //            PaperSize = PaperKind.A4,
        //            Margins = new MarginSettings { Top = 10 },
        //            DocumentTitle = "PDF Report",
        //            //Out = _hostingEnvironment.WebRootPath + "/pdf/report.pdf",
        //        };

        //        var objectSettings = new ObjectSettings
        //        {
        //            PagesCount = true,
        //            HtmlContent = _generateReportService.UnpaidBill(request.StartDate, request.EndDate, response),
        //            WebSettings = { DefaultEncoding = "utf-8", UserStyleSheet = _hostingEnvironment.WebRootPath + "/assets/pdf.css" },
        //        };

        //        var pdf = new HtmlToPdfDocument()
        //        {
        //            GlobalSettings = globalSettings,
        //            Objects = { objectSettings }
        //        };

        //        var file = _converter.Convert(pdf);
        //        return File(file, "application/pdf");
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.LogError(ex.Message);
        //        _logger.LogError(ex.StackTrace);
        //        return BadRequest();
        //    }
        //}

        [HttpGet(ApiRoutes.Reports.ReportUnpaidBill)]
        public async Task<IActionResult> ReportUnpaidBill([FromQuery] StartDateEndDateRequestcs request)
        {
            try
            {
               
                List<int> calendarIds = await _settingService.GetCalendarId(request.StartDate, request.EndDate);

                var setting = await _settingService.GetSettingById(1);

                bool isOld = false;

                if (setting.MassageCode == "CV")
                {
                    if (calendarIds[0] <= 2017)
                    {
                        isOld = true;
                    }
                }
                else if(setting.MassageCode == "MR")
                { 
                    if (calendarIds[0] <= 153)
                    {
                        isOld = true;
                    }
                }

                if (isOld)
                {
                    var massages = await _reportService.UnpaidBill(calendarIds);

                    var response = _mapper.Map<List<ReportUnpaidBillResponse>>(massages);

                    foreach (var resp in response)
                    {
                        var massage = await _reportService.GetIdReceptionInMassageAngelFirstRow(resp.Id);

                        if (massage != null)
                        {
                            var reception = await _receptionService.GetByIdAsync(massage.ReceptionId);

                            resp.reception = _mapper.Map<ReceptionResponse>(reception);
                        }

                        if (resp.UpdatedBy > 0)
                        {
                            var userCreateBy = await _userService.GetByIdAsync(resp.UpdatedBy);

                            resp.user = _mapper.Map<UserResponse>(userCreateBy);
                        }


                    }

                    var globalSettings = new GlobalSettings
                    {
                        ColorMode = ColorMode.Color,
                        Orientation = Orientation.Portrait,
                        PaperSize = PaperKind.A4,
                        Margins = new MarginSettings { Top = 10 },
                        DocumentTitle = "PDF Report",
                        //Out = _hostingEnvironment.WebRootPath + "/pdf/report.pdf",
                    };

                    var objectSettings = new ObjectSettings
                    {
                        PagesCount = true,
                        HtmlContent = _generateReportService.UnpaidBillOld(request.StartDate, request.EndDate, response),
                        WebSettings = { DefaultEncoding = "utf-8", UserStyleSheet = _hostingEnvironment.WebRootPath + "/assets/pdf.css" },
                    };

                    var pdf = new HtmlToPdfDocument()
                    {
                        GlobalSettings = globalSettings,
                        Objects = { objectSettings }
                    };

                    var file = _converter.Convert(pdf);
                    return File(file, "application/pdf");
                }

                else
                {
                    var massages = await _reportService.UnpaidBill(calendarIds);

                    var responses = _mapper.Map<List<ReportMassageDebtResponse>>(massages);
                    foreach (var resp in responses)
                    {
                        var user = await _userService.GetByIdAsync(resp.Massage.CashierId);
                        if (user != null)
                        {
                            resp.Cashier = user.UserFullname;
                        }
                        var members = await _massageService.GetMassageMemberPaymentsByMassageId(resp.MassageId);
                        if (members != null)
                        {
                            int i = 0;
                            foreach (var member in members)
                            {

                                if (i > 0)
                                {
                                    if (member.MemberFoodTotal == 0 && member.MemberMassageTotal != 0)
                                    {
                                        resp.Member += member.Member.Code + "<br>";
                                    }
                                    else if (member.MemberMassageTotal != 0 && member.MemberFoodTotal != 0)
                                    {
                                        resp.Member += member.Member.Code + "<br>";
                                    }
                                }
                                else
                                {
                                    if (member.MemberFoodTotal == 0 && member.MemberMassageTotal != 0)
                                    {
                                        resp.Member = member.Member.Code + "<br>";
                                    }
                                    else if (member.MemberMassageTotal != 0 && member.MemberFoodTotal != 0)
                                    {
                                        resp.Member = member.Member.Code + "<br>";
                                    }
                                }
                                i++;
                            }

                        }
                        resp.UnpaidRemark = resp.Massage.UnPaidRemark;

                    }

                    var globalSettings = new GlobalSettings
                    {
                        ColorMode = ColorMode.Color,
                        Orientation = Orientation.Landscape,
                        PaperSize = PaperKind.A4,
                        Margins = new MarginSettings { Top = 10 },
                        DocumentTitle = "PDF Report",
                        //Out = _hostingEnvironment.WebRootPath + "/pdf/report.pdf",
                    };

                    var objectSettings = new ObjectSettings
                    {
                        PagesCount = true,
                        HtmlContent = _generateReportService.UnpaidBill(request.StartDate, request.EndDate, responses),
                        WebSettings = { DefaultEncoding = "utf-8", UserStyleSheet = _hostingEnvironment.WebRootPath + "/assets/pdf.css" },
                    };

                    var pdf = new HtmlToPdfDocument()
                    {
                        GlobalSettings = globalSettings,
                        Objects = { objectSettings }
                    };

                    var file = _converter.Convert(pdf);
                    return File(file, "application/pdf");
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }

        [HttpGet(ApiRoutes.Reports.ReportMemberExpire)]
        public async Task<IActionResult> ReportMemberExpire([FromQuery] StartDateEndDateRequestcs request)
        {
            try
            {
                var members = await _reportService.MemberExpire(request.StartDate, request.EndDate);
                var response = _mapper.Map<List<MemberResponse>>(members);

                var globalSettings = new GlobalSettings
                {
                    ColorMode = ColorMode.Color,
                    Orientation = Orientation.Portrait,
                    PaperSize = PaperKind.A4,
                    Margins = new MarginSettings { Top = 10 },
                    DocumentTitle = "PDF Report",
                    //Out = _hostingEnvironment.WebRootPath + "/pdf/report.pdf",
                };

                var objectSettings = new ObjectSettings
                {
                    PagesCount = true,
                    HtmlContent = _generateReportService.MemberExpire(request.StartDate, request.EndDate, response),
                    WebSettings = { DefaultEncoding = "utf-8", UserStyleSheet = _hostingEnvironment.WebRootPath + "/assets/pdf.css" },
                };

                var pdf = new HtmlToPdfDocument()
                {
                    GlobalSettings = globalSettings,
                    Objects = { objectSettings }
                };

                var file = _converter.Convert(pdf);
                return File(file, "application/pdf");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }

        [HttpGet(ApiRoutes.Reports.ReportMemberExpireExcel)]
        public async Task<IActionResult> ReportMemberExpireExcel([FromQuery] StartDateEndDateRequestcs request)
        {
            try
            {
                var members = await _reportService.MemberExpire(request.StartDate, request.EndDate);
                var response = _mapper.Map<List<MemberResponse>>(members);
                var itemMember = await _itemService.GetSuitSlug();
                decimal suite = 0;
                using (var workbook = new XLWorkbook())
                {
                    var worksheet = workbook.Worksheets.Add("MemberExpire");
                    var currentRow = 1;
                    worksheet.Cell(currentRow, 1).Value = "รหัสเมมเบอร์";
                    worksheet.Cell(currentRow, 2).Value = "ขื่อ";
                    worksheet.Cell(currentRow, 3).Value = "เชียร์แขก";
                    worksheet.Cell(currentRow, 4).Value = "วันหมดอายุ";
                    worksheet.Cell(currentRow, 5).Value = "ยอดคงเหลือ";
                    worksheet.Cell(currentRow, 6).Value = "ห้องสูท";

                    foreach (var item in response)
                    {
                        DateTime expireDate = (DateTime)item.ExpiredDate;
                        string _expireDate = expireDate.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));
                        var creditAmount = String.Format("{0:N}", item.CreditAmount);

                        var MemberItems = item.MemberItems.Where(i => i.ItemId == itemMember.Id && i.MemberId == item.Id).OrderByDescending(i => i.Id).FirstOrDefault();

                        if (MemberItems != null)
                        {
                            suite = MemberItems.Amount;
                        }

                        currentRow++;
                        worksheet.Cell(currentRow, 1).Value = item.Code;
                        worksheet.Cell(currentRow, 2).Value = item.Firstname;
                        worksheet.Cell(currentRow, 3).Value = item.Reception.Nickname;
                        worksheet.Cell(currentRow, 4).Value = _expireDate;
                        worksheet.Cell(currentRow, 5).Value = item.CreditAmount;
                        worksheet.Cell(currentRow, 6).Value = suite;
                    }
                    using (var stream = new MemoryStream())
                    {
                        workbook.SaveAs(stream);
                        var content = stream.ToArray();

                        return File(content, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "memberexpire.xlsx");
                    }
                }

            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }

        [HttpGet(ApiRoutes.Reports.ReportSummaryDayFinance)]
        public async Task<IActionResult> ReportDailyFinance([FromQuery] StartDateEndDateRequestcs request)
        {
            try
            {
                List<int> calendarIds = await _settingService.GetCalendarId(request.StartDate, request.EndDate);
                var angelTypes = await _angelTypeService.GetAllAsync(null);
                var response = _mapper.Map<List<ReportSummaryDayResponse>>(angelTypes);
                var setting = await _settingService.GetSettingById(1);
                var responseSetting = _mapper.Map<SettingResponse>(setting);
                foreach (var angelType in response)
                {
                    var Finance = _reportService.SumTotalRoundMassageAngel(calendarIds, angelType.Id, angelType.Fee);
                    angelType.Finance = _mapper.Map<ReportDailyFinanceResponse>(Finance);
                }

                var paymentTotal = _reportService.SumTotalPaymentDailyFinance(calendarIds);
                var responsePayment = _mapper.Map<ReportDailyFinanceResponse>(paymentTotal);

                var globalSettings = new GlobalSettings
                {
                    ColorMode = ColorMode.Color,
                    Orientation = Orientation.Portrait,
                    PaperSize = PaperKind.A4,
                    Margins = new MarginSettings { Top = 10 },
                    DocumentTitle = "PDF Report",
                    //Out = _hostingEnvironment.WebRootPath + "/pdf/report.pdf",
                };

                var objectSettings = new ObjectSettings
                {
                    PagesCount = true,
                    HtmlContent = _generateReportService.DailyFinance(request.StartDate, request.EndDate, response, responsePayment, responseSetting),
                    WebSettings = { DefaultEncoding = "utf-8", UserStyleSheet = _hostingEnvironment.WebRootPath + "/assets/pdf.css" },
                };

                var pdf = new HtmlToPdfDocument()
                {
                    GlobalSettings = globalSettings,
                    Objects = { objectSettings }
                };

                var file = _converter.Convert(pdf);
                return File(file, "application/pdf");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }
        [HttpGet(ApiRoutes.Reports.ReportReceptionSell)]
        public async Task<IActionResult> ReportReceptionSell([FromQuery] StartDateEndDateRequestcs request)
        {
            try
            {
                List<int> calendarIds = await _settingService.GetCalendarId(request.StartDate, request.EndDate);
                var receptions = await _receptionService.GetAllAsync(null);
                var response = _mapper.Map<List<ReportReceptionSellResponse>>(receptions);
                foreach (var reception in response)
                {
                    var summary = _reportService.SummaryMemberTransaction(calendarIds, reception.Id);
                    reception.Summarys = _mapper.Map<ReportSummaryMemberTransactionResponse>(summary);

                }
                response = _mapper.Map<List<ReportReceptionSellResponse>>(response.OrderBy(i => i.Code));

                var responses = _mapper.Map<List<ReportReceptionSellResponse>>(receptions);
                foreach (var item in responses)
                {
                    var summary = _reportService.SummaryMemberTransaction(calendarIds, item.Id);
                    item.Summarys = _mapper.Map<ReportSummaryMemberTransactionResponse>(summary);

                }
                responses = _mapper.Map<List<ReportReceptionSellResponse>>(responses.OrderByDescending(i => i.Summarys.Summary));
                bool isCheck = true;
                decimal oldSummary = 0;
                int i = 1;
                foreach (var itemSummary in responses)
                {
                    if (isCheck)
                    {
                        oldSummary = itemSummary.Summarys.Summary;
                        isCheck = false;
                    }
                    if (oldSummary != itemSummary.Summarys.Summary)
                    {
                        i++;
                    }

                    foreach (var item in response)
                    {
                        if (itemSummary.Id == item.Id)
                        {
                            item.Summarys.Rank = i;
                        }
                    }
                    oldSummary = itemSummary.Summarys.Summary;
                }

                var globalSettings = new GlobalSettings
                {
                    ColorMode = ColorMode.Color,
                    Orientation = Orientation.Portrait,
                    PaperSize = PaperKind.A4,
                    Margins = new MarginSettings { Top = 10 },
                    DocumentTitle = "PDF Report",
                    //Out = _hostingEnvironment.WebRootPath + "/pdf/report.pdf",
                };

                var objectSettings = new ObjectSettings
                {
                    PagesCount = true,
                    HtmlContent = _generateReportService.ReceptionSell(request.StartDate, request.EndDate, response, responses),
                    WebSettings = { DefaultEncoding = "utf-8", UserStyleSheet = _hostingEnvironment.WebRootPath + "/assets/pdf.css" },
                };

                var pdf = new HtmlToPdfDocument()
                {
                    GlobalSettings = globalSettings,
                    Objects = { objectSettings }
                };

                var file = _converter.Convert(pdf);
                return File(file, "application/pdf");

                //return Ok(response);

            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }


        [HttpGet(ApiRoutes.Reports.ReportReceptionRoundTotal)]
        public async Task<IActionResult> ReportReceptionRoundTotal([FromQuery] StartDateEndDateRequestcs request)
        {
            try
            {
                List<int> calendarIds = await _settingService.GetCalendarId(request.StartDate, request.EndDate);
                var receptions = await _receptionService.GetAllAsync(null);
                var response = _mapper.Map<List<ReportReceptionRoundTotalResponse>>(receptions);
                foreach (var reception in response)
                {
                    var RoundTotal = _reportService.ReceptionRoundTotal(calendarIds, reception.Id);
                    reception.ReceptionRound = _mapper.Map<ReportReceptionRoundResponse>(RoundTotal);
                }
                response = _mapper.Map<List<ReportReceptionRoundTotalResponse>>(response.OrderBy(i => i.Code));

                var responses = _mapper.Map<List<ReportReceptionRoundTotalResponse>>(receptions);
                foreach (var item in responses)
                {
                    var RoundTotal = _reportService.ReceptionRoundTotal(calendarIds, item.Id);
                    item.ReceptionRound = _mapper.Map<ReportReceptionRoundResponse>(RoundTotal);
                }
                responses = _mapper.Map<List<ReportReceptionRoundTotalResponse>>(responses.OrderByDescending(i => i.ReceptionRound.RoundTotal));
                bool isCheck = true;
                decimal oldTotalRound = 0;
                int i = 1;
                foreach (var itemTotalRound in responses)
                {
                    if (isCheck)
                    {
                        oldTotalRound = itemTotalRound.ReceptionRound.RoundTotal;
                        isCheck = false;
                    }
                    if (oldTotalRound != itemTotalRound.ReceptionRound.RoundTotal)
                    {
                        i++;
                    }

                    foreach (var item in response)
                    {
                        if (itemTotalRound.Id == item.Id)
                        {
                            item.ReceptionRound.Rank = i;
                        }
                    }
                    oldTotalRound = itemTotalRound.ReceptionRound.RoundTotal;
                }
                var globalSettings = new GlobalSettings
                {
                    ColorMode = ColorMode.Color,
                    Orientation = Orientation.Portrait,
                    PaperSize = PaperKind.A4,
                    Margins = new MarginSettings { Top = 10 },
                    DocumentTitle = "PDF Report",
                    //Out = _hostingEnvironment.WebRootPath + "/pdf/report.pdf",
                };

                var objectSettings = new ObjectSettings
                {
                    PagesCount = true,
                    HtmlContent = _generateReportService.ReceptionRound(request.StartDate, request.EndDate, response, responses),
                    WebSettings = { DefaultEncoding = "utf-8", UserStyleSheet = _hostingEnvironment.WebRootPath + "/assets/pdf.css" },
                };

                var pdf = new HtmlToPdfDocument()
                {
                    GlobalSettings = globalSettings,
                    Objects = { objectSettings }
                };

                var file = _converter.Convert(pdf);
                return File(file, "application/pdf");
                //return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }

        [HttpGet(ApiRoutes.Reports.ReportTableCashier)]
        public async Task<IActionResult> ReportTableCashier([FromQuery] StartDateEndDateRequestcs request)
        {
            try
            {
                int calendarId = _settingService.GetCalendarId(request.StartDate);

                var report = _reportService.TableCashier(calendarId, request.CashierId);

                var userCashier = await _userService.GetByIdAsync(request.CashierId);

                var setting = await _settingService.GetSettingById(1);
                var responseSetting = _mapper.Map<SettingResponse>(setting);

                var globalSettings = new GlobalSettings
                {
                    ColorMode = ColorMode.Color,
                    Orientation = Orientation.Portrait,
                    PaperSize = PaperKind.A4,
                    Margins = new MarginSettings { Top = 10 },
                    DocumentTitle = "PDF Report",
                    //Out = _hostingEnvironment.WebRootPath + "/pdf/report.pdf",
                };

                var objectSettings = new ObjectSettings
                {
                    PagesCount = true,
                    HtmlContent = _generateReportService.TableCashier(request.StartDate, userCashier.UserFullname, report, responseSetting),
                    WebSettings = { DefaultEncoding = "utf-8", UserStyleSheet = _hostingEnvironment.WebRootPath + "/assets/pdf.css" },
                };

                var pdf = new HtmlToPdfDocument()
                {
                    GlobalSettings = globalSettings,
                    Objects = { objectSettings }
                };

                var file = _converter.Convert(pdf);
                return File(file, "application/pdf");
                //return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }

        [HttpGet(ApiRoutes.Reports.MaidCountAngelTypeTotal)]
        public async Task<IActionResult> MaidCountTotalRound([FromQuery] StartDateEndDateRequestcs request)
        {
            try
            {
                int calendarId = _settingService.GetCalendarId(request.StartDate);
                var angelTypes = await _angelTypeService.GetAllAsync(null);
                var response = _mapper.Map<List<ReportPaymentAngelPerformanceResponse>>(angelTypes);
                foreach (var item in response)
                {
                    item.Summary = 0;
                    item.TotalRound = _reportService.TotalAngelType(calendarId, item.Id);
                }

                var globalSettings = new GlobalSettings
                {
                    ColorMode = ColorMode.Color,
                    Orientation = Orientation.Portrait,
                    PaperSize = PaperKind.A4,
                    Margins = new MarginSettings { Top = 10 },
                    DocumentTitle = "PDF Report",
                    //Out = _hostingEnvironment.WebRootPath + "/pdf/report.pdf",
                };

                var objectSettings = new ObjectSettings
                {
                    PagesCount = true,
                    HtmlContent = _generateReportService.Maid(request.StartDate, response),
                    WebSettings = { DefaultEncoding = "utf-8", UserStyleSheet = _hostingEnvironment.WebRootPath + "/assets/pdf.css" },
                };

                var pdf = new HtmlToPdfDocument()
                {
                    GlobalSettings = globalSettings,
                    Objects = { objectSettings }
                };

                var file = _converter.Convert(pdf);
                return File(file, "application/pdf");

                //return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }

        [HttpGet(ApiRoutes.Reports.ReportMemberFood)]
        public async Task<IActionResult> MemberFood([FromQuery] StartDateEndDateRequestcs request)
        {
            try
            {
                List<int> calendarIds = await _settingService.GetCalendarId(request.StartDate, request.EndDate);
                var Member = await _reportService.GetMemberFood(calendarIds);
                var response = _mapper.Map<List<ReportMemberFoodResponse>>(Member);
                var setting = await _settingService.GetSettingById(1);
                var responseSetting = _mapper.Map<SettingResponse>(setting);
                foreach (var user in response)
                {
                    var user_ = await _userService.GetByIdAsync(user.CreatedBy);
                    user.User = _mapper.Map<UserResponse>(user_);
                }

                var globalSettings = new GlobalSettings
                {
                    ColorMode = ColorMode.Color,
                    Orientation = Orientation.Landscape,
                    PaperSize = PaperKind.A4,
                    Margins = new MarginSettings { Top = 10 },
                    DocumentTitle = "PDF Report",
                    //Out = _hostingEnvironment.WebRootPath + "/pdf/report.pdf",
                };

                var objectSettings = new ObjectSettings
                {
                    PagesCount = true,
                    HtmlContent = _generateReportService.GetMemberFood(request.StartDate, request.EndDate, response, responseSetting),
                    WebSettings = { DefaultEncoding = "utf-8", UserStyleSheet = _hostingEnvironment.WebRootPath + "/assets/pdf.css" },
                };

                var pdf = new HtmlToPdfDocument()
                {
                    GlobalSettings = globalSettings,
                    Objects = { objectSettings }
                };

                var file = _converter.Convert(pdf);
                return File(file, "application/pdf");

                //return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }

        [HttpGet(ApiRoutes.Reports.SellSuite)]
        public async Task<IActionResult> SellSuite([FromQuery] StartDateEndDateRequestcs request)
        {
            try
            {
                List<int> calendarIds = await _settingService.GetCalendarId(request.StartDate, request.EndDate);
                var room = await _reportService.GetRoomSuite();
                var responses = _mapper.Map<List<ReportRoomSuiteResponse>>(room);
                var setting = await _settingService.GetSettingById(1);
                var responseSetting = _mapper.Map<SettingResponse>(setting);
                foreach (var response in responses)
                {
                    var massageRooms = await _reportService.GetMassageRoom(calendarIds, response.Id);
                    List<MassageRoom> massageRoomList = new List<MassageRoom>();
                    foreach (var massageRoom in massageRooms)
                    {
                        var check = _reportService.CheckMassageMemberAndRoom(massageRoom.Id);
                        if (!check)
                        {
                            massageRoomList.Add(massageRoom);
                        }
                    }
                    response.reportMassageSellResponses = _mapper.Map<List<ReportMassageSellResponse>>(massageRoomList);

                    foreach (var massage in response.reportMassageSellResponses)
                    {
                        var massagePayment = await _reportService.GetSellSuite(massage.MassageId);
                        massage.Massage = _mapper.Map<MassageResponse>(massagePayment);

                        //var massageMemberPayment = await _reportService.GetMassageMemberPaymentByMassageId(massage.MassageId);
                        //massage.MassageMemberPayment = _mapper.Map<MassageMemberPaymentResponse>(massageMemberPayment);
                    }
                }


                var globalSettings = new GlobalSettings
                {
                    ColorMode = ColorMode.Color,
                    Orientation = Orientation.Portrait,
                    PaperSize = PaperKind.A4,
                    Margins = new MarginSettings { Top = 10 },
                    DocumentTitle = "PDF Report",
                    //Out = _hostingEnvironment.WebRootPath + "/pdf/report.pdf",
                };

                var objectSettings = new ObjectSettings
                {
                    PagesCount = true,
                    HtmlContent = _generateReportService.SellSuite(request.StartDate, request.EndDate, responses, responseSetting),
                    WebSettings = { DefaultEncoding = "utf-8", UserStyleSheet = _hostingEnvironment.WebRootPath + "/assets/pdf.css" },
                };

                var pdf = new HtmlToPdfDocument()
                {
                    GlobalSettings = globalSettings,
                    Objects = { objectSettings }
                };

                var file = _converter.Convert(pdf);
                return File(file, "application/pdf");

                //return Ok(responses);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }

        [HttpGet(ApiRoutes.Reports.MemberTopup)]
        public async Task<IActionResult> GetMemberTopup([FromQuery] StartDateEndDateRequestcs request)
        {
            try
            {
                List<int> calendarIds = await _settingService.GetCalendarId(request.StartDate, request.EndDate);
                var report = await _reportService.MemberTopup(calendarIds);
                var responses = _mapper.Map<List<ReportMemberTopupResponse>>(report);
                foreach (var item in responses)
                {
                    var member = await _memberService.GetByIdAsync(item.MemberId);
                    item.Member = _mapper.Map<MemberResponse>(member);
                    var calendar = await _settingService.GetCalendarDayById(item.CalendarId);
                    item.calendarDay = _mapper.Map<CalendarDayResponse>(calendar);
                }

                var globalSettings = new GlobalSettings
                {
                    ColorMode = ColorMode.Color,
                    Orientation = Orientation.Portrait,
                    PaperSize = PaperKind.A4,
                    Margins = new MarginSettings { Top = 10 },
                    DocumentTitle = "PDF Report",
                    //Out = _hostingEnvironment.WebRootPath + "/pdf/report.pdf",
                };

                var objectSettings = new ObjectSettings
                {
                    PagesCount = true,
                    HtmlContent = _generateReportService.GetMemberTopup(request.StartDate, request.EndDate, responses),
                    WebSettings = { DefaultEncoding = "utf-8", UserStyleSheet = _hostingEnvironment.WebRootPath + "/assets/pdf.css" },
                };

                var pdf = new HtmlToPdfDocument()
                {
                    GlobalSettings = globalSettings,
                    Objects = { objectSettings }
                };

                var file = _converter.Convert(pdf);
                return File(file, "application/pdf");

                //return Ok(responses);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }

        [HttpGet(ApiRoutes.Reports.MemberCreditAmount)]
        public async Task<IActionResult> GetMemberCreditAmount([FromQuery] StartDateEndDateRequestcs request)
        {
            try
            {
                //List<int> calendarIds = await _settingService.GetCalendarId(request.StartDate, request.EndDate);
                var report = await _reportService.MemberCreditAmount();
                var responses = _mapper.Map<List<ReportMemberCreditAmountResponse>>(report);

                var globalSettings = new GlobalSettings
                {
                    ColorMode = ColorMode.Color,
                    Orientation = Orientation.Portrait,
                    PaperSize = PaperKind.A4,
                    Margins = new MarginSettings { Top = 10 },
                    DocumentTitle = "PDF Report",
                    //Out = _hostingEnvironment.WebRootPath + "/pdf/report.pdf",
                };

                var objectSettings = new ObjectSettings
                {
                    PagesCount = true,
                    HtmlContent = _generateReportService.GetMemberCreditAmount(responses),
                    WebSettings = { DefaultEncoding = "utf-8", UserStyleSheet = _hostingEnvironment.WebRootPath + "/assets/pdf.css" },
                };

                var pdf = new HtmlToPdfDocument()
                {
                    GlobalSettings = globalSettings,
                    Objects = { objectSettings }
                };

                var file = _converter.Convert(pdf);
                return File(file, "application/pdf");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }

        }

        [HttpGet(ApiRoutes.Reports.MemberCreditAmountExcel)]
        public async Task<IActionResult> MemberCreditAmountExcel([FromQuery] StartDateEndDateRequestcs request)
        {
            try
            {
                var report = await _reportService.MemberCreditAmount();
                var responses = _mapper.Map<List<ReportMemberCreditAmountResponse>>(report);

                using (var workbook = new XLWorkbook())
                {
                    var worksheet = workbook.Worksheets.Add("MemberCreditAmount");
                    var currentRow = 1;
                    worksheet.Cell(currentRow, 1).Value = "เบอร์";
                    worksheet.Cell(currentRow, 2).Value = "ชื่อ";
                    worksheet.Cell(currentRow, 3).Value = "ยอดคงเหลือ";
                    worksheet.Cell(currentRow, 4).Value = "วันหมดอายุ";
                    worksheet.Cell(currentRow, 5).Value = "เชียร์แขก";
                    int i = 0;

                    foreach (var item in responses)
                    {
                        i++;

                        currentRow++;
                        worksheet.Cell(currentRow, 1).Value = item.Code;
                        worksheet.Cell(currentRow, 2).Value = item.Firstname + " " + item.Lastname;
                        worksheet.Cell(currentRow, 3).Value = item.CreditAmount;
                        worksheet.Cell(currentRow, 4).Value = item.ExpiredDate;
                        worksheet.Cell(currentRow, 5).Value = item.Reception.Nickname;
                    }

                    using (var stream = new MemoryStream())
                    {
                        workbook.SaveAs(stream);
                        var content = stream.ToArray();

                        return File(content, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "memberCreditAmount.xlsx");
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }

        [HttpGet(ApiRoutes.Reports.GetTotalRoundMonthExcel)]
        public async Task<IActionResult> GetTotalRoundMonthExcel([FromQuery] StartDateEndDateRequestcs request)
        {
            try
            {
                var angelTypes = await _angelTypeService.GetAllAsync(null);
                var calendarDays = await _settingService.GetCalendarDayStartDateAndEndDate(request.StartDate, request.EndDate);
                var response = _mapper.Map<List<AngelTypeSummaryTotalRoundMonthResponse>>(angelTypes);
                foreach (var angelType in response)
                {
                    var angels = await _angelService.GetAngelByAngeltypeIdStatus(angelType.Id);
                    angelType.Angels = _mapper.Map<List<AngelSummaryTotalRoundMonthResponse>>(angels);
                    foreach (var angel in angelType.Angels)
                    {
                        angel.SummaryTotalRound = 0;
                        angel.CalendarDays = _mapper.Map<List<CalendarSummaryTotalRoundMonthResponse>>(calendarDays);
                        foreach (var calendar in angel.CalendarDays)
                        {
                            calendar.TotalRound = _angelPerformanceService.SummaryTotalRoundMonthByAngel(angel.Id, calendar.Id);
                            angel.SummaryTotalRound += calendar.TotalRound;
                        }
                    }
                    List<AngelSummaryTotalRoundMonthResponse> _angel = new List<AngelSummaryTotalRoundMonthResponse>();
                    foreach (var checkAngel in angelType.Angels)
                    {
                        if (checkAngel.SummaryTotalRound != 0)
                        {
                            _angel.Add(checkAngel);
                        }
                    }
                    angelType.Angels = _angel;
                }

                using (var workbook = new XLWorkbook())
                {
                    string StartDate = request.StartDate.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));
                    string EndDate = request.EndDate.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));
                    var worksheet = workbook.Worksheets.Add("ตรวจสอบจำนวนรอบตามช่วง");
                    var currentRow = 1;
                    int i = 1;
                    int v = 1;

                    worksheet.Cell(currentRow, 1).Value = StartDate + "-" + EndDate;
                    foreach (var angelType in response)
                    {
                        i = 1;
                        currentRow++;
                        currentRow++;
                        worksheet.Cell(currentRow, 1).Value = angelType.Code;
                        currentRow++;
                        worksheet.Cell(currentRow, i).Value = "พนง";
                        foreach (var calendarDay in calendarDays)
                        {
                            i++;
                            string toDay = calendarDay.ToDay.ToString("dd", new CultureInfo("th-TH"));
                            worksheet.Cell(currentRow, i).Value = toDay;
                        }
                        i++;
                        worksheet.Cell(currentRow, i).Value = "รวม";

                        foreach (var angel in angelType.Angels)
                        {
                            v = 1;
                            currentRow++;
                            worksheet.Cell(currentRow, v).Value = angel.Code + (angel.Nickname);
                            foreach (var calendarDay in angel.CalendarDays)
                            {
                                v++;
                                if (calendarDay.TotalRound != 0)
                                {
                                    worksheet.Cell(currentRow, v).Value = calendarDay.TotalRound;
                                }
                                else
                                {
                                    worksheet.Cell(currentRow, v).Value = null;
                                }
                            }
                            v++;
                            worksheet.Cell(currentRow, v).Value = angel.SummaryTotalRound;
                        }
                    }

                    using (var stream = new MemoryStream())
                    {
                        workbook.SaveAs(stream);
                        var content = stream.ToArray();

                        return File(content, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "summaryTotalRoundMonth.xlsx");
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }

        [HttpGet(ApiRoutes.Reports.ReportAngelDebt)]
        public async Task<IActionResult> ReportAngelDebt([FromQuery] StartDateEndDateRequestcs request)
        {
            try
            {
                var angelTypes = await _angelTypeService.GetAngelTypeByReport(request.AngelTypeId);
                var responses = _mapper.Map<List<ReportAngelDebtByAngelTypeResponse>>(angelTypes);

                var debtTypes = await _debtTypeService.GetAllAsync(null);
                var debtTypeResponse = _mapper.Map<List<DebtTypeResponse>>(debtTypes);

                foreach (var angelType in responses)
                {
                    var angels = await _angelService.GetAngelByAngeltypeIdStatus(angelType.Id);

                    angelType.Angels = _mapper.Map<List<ReportAngelDebtByAngelResponse>>(angels);

                    foreach (var angel in angelType.Angels)
                    {
                        var angelDebts = await _reportService.GetReportAngelDebts(angel.Id);

                        angel.AngelDebts = _mapper.Map<List<AngelDebtResponse>>(angelDebts);
                    }
                }

                var globalSettings = new GlobalSettings
                {
                    ColorMode = ColorMode.Color,
                    Orientation = Orientation.Landscape,
                    PaperSize = PaperKind.A4,
                    Margins = new MarginSettings { Top = 10 },
                    DocumentTitle = "PDF Report",
                    //Out = _hostingEnvironment.WebRootPath + "/pdf/report.pdf",
                };

                var objectSettings = new ObjectSettings
                {
                    PagesCount = true,
                    HtmlContent = _generateReportService.GetReportAngelDebt(responses, debtTypeResponse),
                    WebSettings = { DefaultEncoding = "utf-8", UserStyleSheet = _hostingEnvironment.WebRootPath + "/assets/pdf.css" },
                };

                var pdf = new HtmlToPdfDocument()
                {
                    GlobalSettings = globalSettings,
                    Objects = { objectSettings }
                };

                var file = _converter.Convert(pdf);
                return File(file, "application/pdf");

            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }

        [HttpGet(ApiRoutes.Reports.ReportAllMemberExpire)]
        public async Task<IActionResult> GetAllMemberExpire()
        {
            try
            {
                var members = await _reportService.GetAllMemberExpire();
                var response = _mapper.Map<List<MemberResponse>>(members);

                var globalSettings = new GlobalSettings
                {
                    ColorMode = ColorMode.Color,
                    Orientation = Orientation.Portrait,
                    PaperSize = PaperKind.A4,
                    Margins = new MarginSettings { Top = 10 },
                    DocumentTitle = "PDF Report",
                    //Out = _hostingEnvironment.WebRootPath + "/pdf/report.pdf",
                };

                var objectSettings = new ObjectSettings
                {
                    PagesCount = true,
                    HtmlContent = _generateReportService.GetAllMemberExpire(response),
                    WebSettings = { DefaultEncoding = "utf-8", UserStyleSheet = _hostingEnvironment.WebRootPath + "/assets/pdf.css" },
                };

                var pdf = new HtmlToPdfDocument()
                {
                    GlobalSettings = globalSettings,
                    Objects = { objectSettings }
                };

                var file = _converter.Convert(pdf);
                return File(file, "application/pdf");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }

        [HttpGet(ApiRoutes.Reports.ReportAllMemberExpireExcel)]
        public async Task<IActionResult> GetAllMemberExpireExcel()
        {
            try
            {
                var members = await _reportService.GetAllMemberExpire();
                var response = _mapper.Map<List<MemberResponse>>(members);

                using (var workbook = new XLWorkbook())
                {
                    var worksheet = workbook.Worksheets.Add("MemberSell");
                    var currentRow = 1;
                    worksheet.Cell(currentRow, 1).Value = "ลำดับ";
                    worksheet.Cell(currentRow, 2).Value = "เมมเบอร์";
                    worksheet.Cell(currentRow, 3).Value = "ชื่อ";
                    worksheet.Cell(currentRow, 4).Value = "เชียร์แขก";
                    worksheet.Cell(currentRow, 5).Value = "เบอร์โทรศัพท์";
                    worksheet.Cell(currentRow, 6).Value = "ยอดคงเหลือ";
                    worksheet.Cell(currentRow, 7).Value = "วันหมดอายุ";
                    int i = 0;

                    foreach (var item in response)
                    {
                        DateTime expireDate = (DateTime)item.ExpiredDate;
                        string _expireDate = expireDate.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));

                        i++;
                        currentRow++;
                        worksheet.Cell(currentRow, 1).Value = i;
                        worksheet.Cell(currentRow, 2).Value = item.Code;
                        worksheet.Cell(currentRow, 3).Value = item.Firstname + " " + item.Lastname;
                        worksheet.Cell(currentRow, 4).Value = item.Reception.Nickname;
                        worksheet.Cell(currentRow, 5).Value = item.Tel;
                        worksheet.Cell(currentRow, 6).Value = item.CreditAmount;
                        worksheet.Cell(currentRow, 7).Value = _expireDate;
                    }

                    using (var stream = new MemoryStream())
                    {
                        workbook.SaveAs(stream);
                        var content = stream.ToArray();

                        return File(content, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "allmemberexpire.xlsx");
                    }

                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }

        [HttpGet(ApiRoutes.Reports.ReportCountWorkDaily)]
        public async Task<IActionResult> GetCountWorkDaily([FromQuery] StartDateEndDateRequestcs request)
        {
            try
            {
                var angelTypes = await _angelTypeService.GetByIdAsync((int)request.AngelTypeId);
                var responses = _mapper.Map<AngelTypeResponse>(angelTypes);
                var angels = await _angelService.GetAngelByAngeltypeIdStatus(angelTypes.Id);
                responses.Angel = _mapper.Map<List<AngelResponse>>(angels);
                var calendarDays = await _settingService.GetCalendarDayStartDateAndEndDate(request.StartDate, request.EndDate);
                var setting = await _settingService.GetSettingById(1);
                var responseSetting = _mapper.Map<SettingResponse>(setting);
                foreach (var item in responses.Angel)
                {
                    var angel = await _angelService.GetByIdAsync(item.Id);
                    item.AngelSalaryCountDay = _mapper.Map<AngelSalaryCountDayResponse>(angel);
                    item.AngelSalaryCountDay.CalendarDays = _mapper.Map<List<CalendarSalaryCountDayResponse>>(calendarDays);
                    foreach (var calendarDay in item.AngelSalaryCountDay.CalendarDays)
                    {
                        calendarDay.CountRound = _angelPerformanceService.SummaryTotalRoundMonthByAngel(angel.Id, calendarDay.Id);
                        var getTime = await _settingService.GetAngelWorking(angel.Id, calendarDay.Id);
                        if (getTime != null)
                        {
                            calendarDay.GetInTime = getTime.GetInTime;
                            if (getTime.GetOutTime != null)
                            {
                                calendarDay.GetOutTime = getTime.GetOutTime;
                            }
                            else
                            {
                                if (setting.IsCountDaily)
                                {
                                    if (calendarDay.CountRound >= 2)
                                    {
                                        calendarDay.IsDay = true;
                                        item.AngelSalaryCountDay.CountDay += 1;
                                    }
                                }
                                else
                                {
                                    if (calendarDay.CountRound >= 3)
                                    {
                                        calendarDay.IsDay = true;
                                        item.AngelSalaryCountDay.CountDay += 1;
                                    }
                                }
                                continue;
                            }
                            DateTime startDay = new DateTime(calendarDay.ToDay.Year, calendarDay.ToDay.Month, calendarDay.ToDay.Day, 17, 59, 59);
                            DateTime endDay = new DateTime(calendarDay.ToDay.Year, calendarDay.ToDay.Month, calendarDay.ToDay.Day, 23, 59, 59);

                            if (calendarDay.GetInTime < startDay && calendarDay.GetOutTime > endDay)
                            {
                                if (calendarDay.CountRound >= 1)
                                {
                                    calendarDay.IsDay = true;
                                    item.AngelSalaryCountDay.CountDay += 1;
                                }
                            }
                            else
                            {
                                if (setting.IsCountDaily)
                                {
                                    if (calendarDay.CountRound >= 2)
                                    {
                                        calendarDay.IsDay = true;
                                        item.AngelSalaryCountDay.CountDay += 1;
                                    }
                                }
                                else
                                {
                                    if (calendarDay.CountRound >= 3)
                                    {
                                        calendarDay.IsDay = true;
                                        item.AngelSalaryCountDay.CountDay += 1;
                                    }
                                }

                            }
                        }
                    }

                }

                var globalSettings = new GlobalSettings
                {
                    ColorMode = ColorMode.Color,
                    Orientation = Orientation.Landscape,
                    PaperSize = PaperKind.A4,
                    Margins = new MarginSettings { Top = 10 },
                    DocumentTitle = "PDF Report",
                    //Out = _hostingEnvironment.WebRootPath + "/pdf/report.pdf",
                };

                var objectSettings = new ObjectSettings
                {
                    PagesCount = true,
                    HtmlContent = _generateReportService.GetSalaryCountDay(request.StartDate, request.EndDate, responses),
                    WebSettings = { DefaultEncoding = "utf-8", UserStyleSheet = _hostingEnvironment.WebRootPath + "/assets/pdf.css" },
                };

                var pdf = new HtmlToPdfDocument()
                {
                    GlobalSettings = globalSettings,
                    Objects = { objectSettings }
                };

                var file = _converter.Convert(pdf);
                return File(file, "application/pdf");
                //return Ok(responses);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }

        [HttpGet(ApiRoutes.Reports.ReportIncomeDaily)]

        public async Task<IActionResult> GetIncomeDaily([FromQuery] StartDateEndDateRequestcs request)
        {
            try
            {
                List<int> calendarDayIds = await _settingService.GetCalendarId(request.StartDate, request.EndDate);
                var massages = await _massageService.GetMassageByCashierId((int)request.CashierId, calendarDayIds);
                var response = _mapper.Map<List<MassageResponse>>(massages);
                var user = await _userService.GetByIdAsync((int)request.CashierId);
                var _user = _mapper.Map<UserResponse>(user);
                var setting = await _settingService.GetSettingById(1);
                var responseSetting = _mapper.Map<SettingResponse>(setting);
                List<MassageReportResponse> respList = new List<MassageReportResponse>();
                MassageReportResponse sumResponse = new MassageReportResponse();
                foreach (var massage in response)
                {

                    var respMassgeRoom = await _massageService.GetListMassageRoomByMassageId(massage.Id);
                    var carlendar = await _settingService.GetCalendarDayById(massage.CalendarDayId);
                    foreach (var itemRoom in respMassgeRoom)
                    {
                        MassageReportResponse resp = new MassageReportResponse();
                        resp.BillDate = carlendar.ToDay;
                        resp.BillNo = massage.DocumentNumber;
                        resp.ReceptionCode = itemRoom.Reception.Code;
                        resp.RoomNo = itemRoom.Room.RoomNo;
                        resp.CheckInTime = itemRoom.CheckInTime;
                        resp.CheckOutTime = itemRoom.CheckOutTime;
                        resp.IsMassageRoom = true;
                        var massageMemberPaymentAndRoom = await _massageService.GetMassageMemberAndRoomByMassageRoomId(itemRoom.Id);
                        if (massageMemberPaymentAndRoom != null)
                        {
                            var massageMemberPayment = await _massageService.GetMassageMemberPaymentById(massageMemberPaymentAndRoom.MassageMemberPaymentId);
                            resp.UseSuite = massageMemberPayment.UseSuite;
                            resp.UseVip = massageMemberPayment.UseVip;
                        }


                        if (itemRoom.IsDiscount)
                        {
                            if (itemRoom.IsDiscountBaht)
                            {
                                string disCountBath = String.Format("{0:N}", itemRoom.DiscountBaht);
                                resp.Remark = itemRoom.DiscountRemark + " " + ":" + " " + disCountBath + " " + "บาท";
                            }
                            if (itemRoom.IsDiscountPercent)
                            {
                                resp.Remark = itemRoom.DiscountRemark + " " + ":" + " " + itemRoom.DiscountPercent + "%";
                            }
                        }
                        resp.TimeMinute = itemRoom.TimeMinute;
                        respList.Add(resp);
                    }

                    var respAngel = await _massageService.GetMassageAngelByMassageId(massage.Id);
                    foreach (var itemAngel in respAngel)
                    {
                        MassageReportResponse resp = new MassageReportResponse();
                        resp.BillDate = carlendar.ToDay;
                        resp.BillNo = massage.DocumentNumber;
                        resp.ReceptionCode = itemAngel.Reception.Code;
                        resp.RoomNo = itemAngel.Room.RoomNo;
                        resp.CheckInTime = itemAngel.CheckInTime;
                        resp.CheckOutTime = itemAngel.CheckOutTime;
                        resp.Round = itemAngel.Round;
                        resp.AngelCode = itemAngel.Angel.Code;
                        resp.IsMassageRoom = false;
                        decimal priceAngel = itemAngel.Angel.AngelType.Fee * itemAngel.Round;
                        if (itemAngel.IsDiscountBaht)
                        {
                            priceAngel -= itemAngel.DiscountBaht;
                        }
                        if (itemAngel.IsDiscountPercent)
                        {
                            priceAngel -= (priceAngel * (itemAngel.DiscountPercent / 100));
                        }
                        if (itemAngel.IsDiscountAngelRound)
                        {
                            priceAngel -= (itemAngel.DiscountAngelRound * itemAngel.Round);
                        }

                        resp.Wage = priceAngel;
                        resp.CashTotal = massage.CashAngelTotal + massage.CashRoomTotal + massage.CashFoodTotal + massage.CashEtcTotal;
                        resp.CreditTotal = massage.CreditAngelTotal + massage.CreditRoomTotal + massage.CreditFoodTotal + massage.CreditEtcTotal;
                        resp.QrCodeTotal = massage.QrCodeAngelTotal + massage.QrCodeRoomTotal + massage.QrCodeFoodTotal + massage.QrCodeEtcTotal;
                        resp.MemberTotal = massage.MemberAngelTotal + massage.MemberRoomTotal + massage.MemberFoodTotal + massage.MemberEtcTotal;
                        resp.Remark = massage.EntertainRemarkAngel;
                        resp.TimeMinute = itemAngel.TimeMinute;
                        var msMemberPayments = await _massageService.GetMassageMemberPaymentsByMassageId(itemAngel.MassageId);
                        if (msMemberPayments != null)
                        {
                            foreach (var msmPayment in msMemberPayments)
                            {
                                string memberTotal = String.Format("{0:N}", msmPayment.MemberTotal);
                                resp.MemberCode += msmPayment.Member.Code + " " + ":" + " " + memberTotal + "<br>";
                            }
                        }
                        respList.Add(resp);
                    }

                    sumResponse.Total += massage.Total;
                    sumResponse.SumCashTotal += massage.CashAngelTotal + massage.CashRoomTotal + massage.CashFoodTotal + massage.CashEtcTotal;
                    sumResponse.SumQrCode += massage.QrCodeAngelTotal + massage.QrCodeRoomTotal + massage.QrCodeFoodTotal + massage.QrCodeEtcTotal;
                    sumResponse.SumCredit += massage.CreditAngelTotal + massage.CreditRoomTotal + massage.CreditFoodTotal + massage.CreditEtcTotal;
                    sumResponse.SumMember += massage.MemberAngelTotal + massage.MemberRoomTotal + massage.MemberFoodTotal + massage.MemberEtcTotal;
                    foreach (var _respMassgeRoom in respMassgeRoom)
                    {
                        var _massageMemberPaymentAndRoom = await _massageService.GetMassageMemberAndRoomByMassageRoomId(_respMassgeRoom.Id);
                        if (_massageMemberPaymentAndRoom != null)
                        {
                            var massageMemberPayment = await _massageService.GetMassageMemberPaymentById(_massageMemberPaymentAndRoom.MassageMemberPaymentId);
                            sumResponse.SumUseSuite += massageMemberPayment.UseSuite;
                            sumResponse.SumUseVip += massageMemberPayment.UseVip;
                        }
                    }

                }
                var globalSettings = new GlobalSettings
                {
                    ColorMode = ColorMode.Color,
                    Orientation = Orientation.Landscape,
                    PaperSize = PaperKind.A4,
                    Margins = new MarginSettings { Top = 10 },
                    DocumentTitle = "PDF Report",
                    //Out = _hostingEnvironment.WebRootPath + "/pdf/report.pdf",
                };

                var objectSettings = new ObjectSettings
                {
                    PagesCount = true,
                    HtmlContent = _generateReportService.GetIncomeDaily(request.StartDate, request.EndDate, respList, sumResponse, _user, responseSetting),
                    WebSettings = { DefaultEncoding = "utf-8", UserStyleSheet = _hostingEnvironment.WebRootPath + "/assets/pdf.css" },
                };

                var pdf = new HtmlToPdfDocument()
                {
                    GlobalSettings = globalSettings,
                    Objects = { objectSettings }
                };

                var file = _converter.Convert(pdf);
                return File(file, "application/pdf");

                //return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }

        [HttpGet(ApiRoutes.Reports.ReportCountryies)]

        public async Task<IActionResult> GetReportCountries([FromQuery] StartDateEndDateRequestcs request)
        {
            try
            {
                //List<string> responses = new List<string>();
                List<MassageResponse> massageResponse = new List<MassageResponse>();
                List<MemberResponse> memberResponse = new List<MemberResponse>();
                var countries = await _countriesService.GetAllAsync();
                var countriesResponse = _mapper.Map<List<CountriesResponse>>(countries);
                if (request.DropdownId == (int)EReport.isMassage)
                {
                    List<int> calendarDayIds = await _settingService.GetCalendarId(request.StartDate, request.EndDate);
                    var massage = await _massageService.GetAllAsync();
                    var massageCountries = await _reportService.GetMassageByCalendarDayAndCountries(calendarDayIds);
                    massageResponse = _mapper.Map<List<MassageResponse>>(massageCountries);
                    foreach (var massageAngel in massageResponse)
                    {
                        var massageAngelResponse = await _massageService.GetMassageAngelByMassageId(massageAngel.Id);
                        massageAngel.massageAngels = _mapper.Map<List<MassageAngelResponse>>(massageAngelResponse);
                    }

                }
                else if (request.DropdownId == (int)EReport.isMember)
                {

                    var members = await _memberService.GetAllAsync(null);
                    if (request.Status != null)
                    {
                        List<MemberResponse> listmember = new List<MemberResponse>();
                        foreach (var member in members)
                        {
                            if (member.Status == (int)request.Status)
                            {
                                var country = await _countriesService.GetByIdAsync(member.CountriesId);
                                MemberResponse _member = new MemberResponse();
                                _member.Id = member.Id;
                                _member.Code = member.Code;
                                _member.Firstname = member.Firstname;
                                _member.Lastname = member.Lastname;
                                _member.Tel = member.Tel;
                                _member.CreditAmount = member.CreditAmount;
                                _member.ReceptionId = member.ReceptionId;
                                _member.ExpiredDate = member.ExpiredDate;
                                _member.Status = member.Status;
                                _member.FromDate = member.FromDate;
                                _member.CountriesId = member.CountriesId;
                                _member.AgenciesId = member.AgenciesId;
                                //_member.Countries.Id = member.Countries.Id;
                                //_member.Countries.NameEn = member.Countries.NameEn;
                                //_member.Countries.NameTh = member.Countries.NameTh;
                                listmember.Add(_member);

                            }
                        }
                        memberResponse = _mapper.Map<List<MemberResponse>>(listmember);
                    }
                    else
                    {
                        memberResponse = _mapper.Map<List<MemberResponse>>(members);
                    }

                    //return Ok(response);
                }
                var globalSettings = new GlobalSettings
                {
                    ColorMode = ColorMode.Color,
                    Orientation = Orientation.Landscape,
                    PaperSize = PaperKind.A4,
                    Margins = new MarginSettings { Top = 10 },
                    DocumentTitle = "PDF Report",
                    //Out = _hostingEnvironment.WebRootPath + "/pdf/report.pdf",
                };

                var objectSettings = new ObjectSettings
                {
                    PagesCount = true,
                    HtmlContent = _generateReportService.GetReportCountries((DateTime)request.StartDate, (DateTime)request.EndDate, massageResponse, memberResponse, countriesResponse),
                    WebSettings = { DefaultEncoding = "utf-8", UserStyleSheet = _hostingEnvironment.WebRootPath + "/assets/pdf.css" },
                };

                var pdf = new HtmlToPdfDocument()
                {
                    GlobalSettings = globalSettings,
                    Objects = { objectSettings }
                };

                var file = _converter.Convert(pdf);
                return File(file, "application/pdf");

                //return Ok();

            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }

        [HttpGet(ApiRoutes.Reports.ReportAgencies)]
        public async Task<IActionResult> GetReportAgencies([FromQuery] StartDateEndDateRequestcs request)
        {
            try
            {
                //List<int> calendarDayIds = await _settingService.GetCalendarId(request.StartDate, request.EndDate);
                List<int> calendarDayIds = await _settingService.GetCalendarId(request.StartDate, request.EndDate);
                var agencyPerformance = await _agencyPerformanceService.GetAllByCalendarDays(calendarDayIds);
                var response = _mapper.Map<List<AgencyPerformanceResponse>>(agencyPerformance);
                var globalSettings = new GlobalSettings
                {
                    ColorMode = ColorMode.Color,
                    Orientation = Orientation.Landscape,
                    PaperSize = PaperKind.A4,
                    Margins = new MarginSettings { Top = 10 },
                    DocumentTitle = "PDF Report",
                    //Out = _hostingEnvironment.WebRootPath + "/pdf/report.pdf",
                };

                var objectSettings = new ObjectSettings
                {
                    PagesCount = true,
                    HtmlContent = _generateReportService.GetReportAgencies(request.StartDate, request.EndDate, response),
                    WebSettings = { DefaultEncoding = "utf-8", UserStyleSheet = _hostingEnvironment.WebRootPath + "/assets/pdf.css" },
                };

                var pdf = new HtmlToPdfDocument()
                {
                    GlobalSettings = globalSettings,
                    Objects = { objectSettings }
                };

                var file = _converter.Convert(pdf);
                return File(file, "application/pdf");

                //return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }
    }
}
