﻿using AutoMapper;
using DocumentFormat.OpenXml.Office2021.DocumentTasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using OONApi.Contracts.ApiRoutes;
using OONApi.Contracts.Responses;
using OONApi.Models;
using OONApi.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using static OONApi.Contracts.ApiRoutes.ApiRoutes;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace OONApi.Controllers
{
    [ApiController]
    [Authorize]
    public class CountriesController : ControllerBase
    {
        private ICountriesService _countriesService;
        private IMapper _mapper;
        private ILogger<CountriesController> _logger;

        public CountriesController(ICountriesService countriesService, IMapper mapper, ILogger<CountriesController> logger)
        {
            _countriesService = countriesService;
            _mapper = mapper;
            _logger = logger;
        }

        [HttpGet(ApiRoutes.Countries.GetAll)]
        public async Task<IActionResult> GetAll()
        {
            try 
            {
               var countries = await _countriesService.GetAllAsync();
                if (countries == null)
                {
                    return NotFound();
                }
                var Response = _mapper.Map<List<CountriesResponse>>(countries);
                
                return Ok(Response);
                //return Ok(_mapper.Map<List<CountriesResponse>>(countries));
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }
    }
}
