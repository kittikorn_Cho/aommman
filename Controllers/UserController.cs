﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using OONApi.Contracts.ApiRoutes;
using OONApi.Contracts.Requests;
using OONApi.Contracts.Requests.Queires;
using OONApi.Contracts.Responses;
using OONApi.Enum;
using OONApi.Helpers;
using OONApi.Models;
using OONApi.Services;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;



// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace OONApi.Controllers
{
    [ApiController]
    [Authorize]
    public class UserController : ControllerBase
    {
        private IUserService _userService;
        private IMapper _mapper;
        private ISettingService _settingService;
        private ILogger<UserController> _logger;
        private AppSettings _appSettings;
        private IConfiguration _config;
        private IMemberService _memberService;
        private IAngelTypeService _angelTypeService;
        private IDailyDebtServicee _dailyDebtServie;
        private IDeductTypeService _deductTypeService;
        private IDailyPriceHistoryService _dailyPriceHistoryService;

        public UserController(IUserService userService, IMapper mapper, ILogger<UserController> logger, ISettingService settingService, IOptions<AppSettings> appSettings, IConfiguration config, IMemberService memberService, IAngelTypeService angelTypeService, IDailyDebtServicee dabtServicee, IDeductTypeService deductTypeService, IDailyPriceHistoryService dailyPriceHistoryService)
        {
            _userService = userService;
            _mapper = mapper;
            _logger = logger;
            _settingService = settingService;
            _appSettings = appSettings.Value;
            _config = config;
            _memberService = memberService;
            _angelTypeService = angelTypeService;
            _dailyDebtServie = dabtServicee;
            _deductTypeService = deductTypeService;
            _dailyPriceHistoryService = dailyPriceHistoryService;
        }

        [HttpGet(ApiRoutes.Users.GetAll)]
       
        public async Task<IActionResult> GetAll([FromQuery] UserSearchQuery paginationQuery)
        {
            try
            {
                int userId = int.Parse(User.Identity.Name);
                var _user = await _userService.GetByIdAsync(userId);
                bool isRole = false;
                foreach(var role in _user.Roles)
                {
                    if(role.Code == "_SUPERADMIN")
                    {
                        isRole = true;
                        break;
                    }
                }   
                var pagination = _mapper.Map<PaginationFilter>(paginationQuery);
                var user = await _userService.GetAllAsync(paginationQuery, pagination, isRole);
                if (user == null)
                {
                    return NotFound();
                }
                return Ok(new PagedResponse<UserResponse>
                {
                    Data = _mapper.Map<List<UserResponse>>(user),
                    PageNumber = pagination.PageNumber,
                    PageSize = pagination.PageSize,
                    Keyword = pagination.Keyword,
                    OrderBy = pagination.OrderBy,
                    TotalRecords = pagination.TotalRecords,
                    OrderDirection = pagination.OrderDirection,
                });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }

        [HttpGet(ApiRoutes.Users.Get)]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                var user = await _userService.GetByIdAsync(id);
                if (user == null)
                {
                    return NotFound();
                }
                var userResponse = _mapper.Map<UserResponse>(user);
                return Ok(userResponse);
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }

        [HttpPost(ApiRoutes.Users.Create)]
        public async Task<IActionResult> Post([FromBody] UserRequest request)
        {
            try
            {
                if (await _userService.IsDuplicateUser(request.Username))
                {
                    return BadRequest(new { Message = "User is duplicated" });
                }
                HashSalt hashSalt = UserService.EncryptPassword(request.Password);
                User user = new User
                {
                    Username = request.Username,
                    Password = hashSalt.Hash,
                    PasswordSalt = hashSalt.Salt,
                    UserFullname = request.UserFullname,
                    Tel = request.Tel,
                    Status = request.Status,
                };
                await _userService.CreateAsync(user, request.RoleIds);
                return Ok(_mapper.Map<UserResponse>(user));
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }

        [HttpPut(ApiRoutes.Users.Update)]
        public async Task<IActionResult> Put([FromRoute] int id, [FromBody] UserRequest request)
        {
            try
            {
                var user = await _userService.GetByIdAsync(id);
                if (user == null)
                {
                    return NotFound();
                }
                User update = user;
                update.Username = request.Username;
                //update.Password = request.Password;
                update.UserFullname = request.UserFullname;
                update.Tel = request.Tel;
                update.Status = request.Status;
                if (!string.IsNullOrEmpty(request.Password))
                {
                    HashSalt hashSalt = UserService.EncryptPassword(request.Password);
                    update.Password = hashSalt.Hash;
                    update.PasswordSalt = hashSalt.Salt;
                }
                await _userService.UpdateAsync(update, request.RoleIds);

                update = await _userService.GetByIdAsync(update.Id);

                return Ok(_mapper.Map<UserResponse>(update));
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }
        [HttpPatch(ApiRoutes.Users.ChangePassword)]
        public async Task<IActionResult> ChangePassword([FromRoute] int id ,[FromBody] ChangePasswordRequest request)
        {
            try
            {
                var user = await _userService.GetByIdAsync(id);
                if(user == null)
                {
                    return NotFound();
                }
                User update = user;

                HashSalt hashSalt = UserService.EncryptPassword(request.Password);
                update.Password = hashSalt.Hash;
                update.PasswordSalt = hashSalt.Salt;

                _userService.ChangePassword(update);

                return Ok(_mapper.Map<UserResponse>(update));
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }

        [HttpPost(ApiRoutes.Users.Authenticate)]
        [AllowAnonymous]
        public async Task<IActionResult> Authenticate(AuthenticateRequest request)
        {
            try
            {
                User user = await _userService.Authenticate(request.Username, request.Password);
                if (user == null)
                {
                    return BadRequest(new { Message = "Username or password is incorrect" });
                }

                user = await _userService.GetByIdAsync(user.Id);

                string openDate = DateTime.Now.ToString("dd/MM/yyyy", new CultureInfo("en-US")) + " " + _appSettings.OpenTime;

                DateTime _openDate = DateTime.ParseExact(openDate, "dd/MM/yyyy HH:mm:ss", new CultureInfo("en-US"));

                if (DateTime.Now > _openDate)
                {
                    var calendar = await _settingService.GetCarlendarDay();
                    if (calendar == null)
                    {
                        CalendarDay calendarDayNew = new CalendarDay();
                        calendarDayNew.ToDay = DateTime.Now.Date;
                        calendarDayNew.IsDate = true;
                        await _settingService.CreateCalendarDay(calendarDayNew);

                        DocumentNumber documentNumber = new DocumentNumber();
                        documentNumber.CalendarDayId = calendarDayNew.Id;
                        documentNumber.RunningNumber = 0;
                        documentNumber.PadLength = 3;
                        await _settingService.CreateDocumentNumber(documentNumber);
                    }
                    else if (DateTime.Now.Date != calendar.ToDay.Date)
                    {
                        var members = await _memberService.GetmemberByExpiredDate(calendar.ToDay.Date);
                        foreach (var member in members)
                        {
                            var getMember = await _memberService.GetByIdAsync(member.Id);
                            getMember.Status = 2;
                            await _memberService.UpdateAsync(getMember);
                        }

                        calendar.IsDate = false;
                        await _settingService.UpdateCalendarDay(calendar);

                        CalendarDay calendarDayNew = new CalendarDay();
                        calendarDayNew.ToDay = DateTime.Now.Date;
                        calendarDayNew.IsDate = true;
                        await _settingService.CreateCalendarDay(calendarDayNew);

                        DocumentNumber documentNumber = new DocumentNumber();
                        documentNumber.CalendarDayId = calendarDayNew.Id;
                        documentNumber.RunningNumber = 0;
                        documentNumber.PadLength = 3;
                        await _settingService.CreateDocumentNumber(documentNumber);

                        var angelTypes = await _angelTypeService.GetAllAsync(null);
                        var deductTypes = await _deductTypeService.GetAllAsync(null);
                        var dailyDebt = await _dailyDebtServie.GetAllAsync(null);

                        List<DailyPriceHistory> dailyPriceHistorys = new List<DailyPriceHistory>();
                        foreach (var angelType in angelTypes)
                        {
                            DailyPriceHistory dailyPriceHistory = new DailyPriceHistory();
                            dailyPriceHistory.CalendarDayId = calendarDayNew.Id;
                            dailyPriceHistory.AngelTypeId = angelType.Id;
                            dailyPriceHistory.Round = 0;
                            dailyPriceHistory.Price = angelType.Fee;
                            dailyPriceHistory.Wage = angelType.Wage;
                            foreach (var deductType in deductTypes)
                            {
                                if (deductType.Id == (int)EDeducttype.Comm1)
                                {
                                    dailyPriceHistory.CommId1 = deductType.Id;
                                    dailyPriceHistory.Comm1 = deductType.Fee;

                                }
                                else if (deductType.Id == (int)EDeducttype.Comm2)
                                {
                                    dailyPriceHistory.CommId2 = deductType.Id;
                                    dailyPriceHistory.Comm2 = deductType.Fee;

                                }

                            }
                            foreach (var dailyPrice in dailyDebt)
                            {
                                if (dailyPrice.Id == (int)EDailydebt.Dailydebt1)
                                {
                                    dailyPriceHistory.DailydebtId1 = dailyPrice.Id;
                                    dailyPriceHistory.Dailydebt1 = dailyPrice.Fee;

                                }
                                else if (dailyPrice.Id == (int)EDailydebt.Dailydebt2)
                                {
                                    dailyPriceHistory.DailydebtId2 = dailyPrice.Id;
                                    dailyPriceHistory.Dailydebt2 = dailyPrice.Fee;

                                }
                            }
                            dailyPriceHistorys.Add(dailyPriceHistory);
                            await _dailyPriceHistoryService.CreateDailyPriceHistory(dailyPriceHistory);
                        }
                    }
                }
                string token = GenerateJSONWebToken(user);
                var resp = _mapper.Map<UserResponse>(user);
                resp.Token = token;
                return Ok(resp);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }

        private string GenerateJSONWebToken(User userInfo)
        {
                var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
                var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);
                List<Claim> claims = new List<Claim>();
                claims.Add(new Claim(ClaimTypes.Name, userInfo.Id.ToString()));
                claims.Add(new Claim(ClaimTypes.Email, userInfo.Username));
                foreach (var user in userInfo.Roles)
                {
                    claims.Add(new Claim(ClaimTypes.Role, user.Code));
                }
            var token = new JwtSecurityToken(_config["Jwt:Issuer"],
                    _config["Jwt:Issuer"],
                    claims,
                    expires: DateTime.Now.AddDays(2),
                    signingCredentials: credentials);

                return new JwtSecurityTokenHandler().WriteToken(token);  
        }

        [HttpDelete(ApiRoutes.Users.Delete)]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await _userService.DeleteAsync(id);
                return Ok();
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }
    }
}
