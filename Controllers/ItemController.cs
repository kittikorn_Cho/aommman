﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using OONApi.Contracts.ApiRoutes;
using OONApi.Contracts.Requests;
using OONApi.Contracts.Requests.Queires;
using OONApi.Contracts.Responses;
using OONApi.Models;
using OONApi.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace OONApi.Controllers
{
    [ApiController]
    [Authorize]
    public class ItemController : ControllerBase
    {
        private IMapper _mapper;
        private IItemService _itemService;
        private ILogger<ItemController> _logger;

        public ItemController(IMapper mapper, IItemService itemService, ILogger<ItemController> logger)
        {
            _mapper = mapper;
            _itemService = itemService;
            _logger = logger;
        }

        [HttpGet(ApiRoutes.Items.GetAll)]
        public async Task<IActionResult> GetAll([FromQuery] ItemSearchQuery paginationQuery)
        {
            try
            {
                var pagination = _mapper.Map<PaginationFilter>(paginationQuery);
                var Items = await _itemService.GetAllAsync(paginationQuery, pagination);
                if (Items == null)
                {
                    return NotFound();
                }

                return Ok(new PagedResponse<ItemResponse>
                {
                    Data = _mapper.Map<List<ItemResponse>>(Items),
                    PageNumber = pagination.PageNumber,
                    PageSize = pagination.PageSize,
                    Keyword = pagination.Keyword,
                    OrderBy = pagination.OrderBy,
                    TotalRecords = pagination.TotalRecords,
                    OrderDirection = pagination.OrderDirection,
                });
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }

        }


        [HttpGet(ApiRoutes.Items.Get)]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                var Item = await _itemService.GetByIdAsync(id);
                if (Item == null)
                {
                    return NotFound();
                }
                return Ok(_mapper.Map<ItemResponse>(Item));
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }

        }


        [HttpPost(ApiRoutes.Items.Create)]
        public async Task<IActionResult> Post([FromBody] ItemRequest request)
        {
            try
            {
                Item Item = new Item
                {
                    Name = request.Name,
                    Slug = request.Slug
                };
                await _itemService.CreateAsync(Item);
                return Ok(_mapper.Map<ItemResponse>(Item));
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }

        }


        [HttpPut(ApiRoutes.Items.Update)]
        public async Task<IActionResult> Put(int id, [FromBody] ItemRequest request)
        {
            try
            {
                var items = await _itemService.GetByIdAsync(id);
                if (items == null)
                {
                    return NotFound();
                }
                Item update = items;
                update.Name = request.Name;
                update.Slug = request.Slug;
                await _itemService.UpdateAsync(update);

                return Ok(_mapper.Map<ItemResponse>(update));
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }

        }


        [HttpDelete(ApiRoutes.Items.Delete)]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await _itemService.DeleteAsync(id);
                return Ok();
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }

        }
    }
}
