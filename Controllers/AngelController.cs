﻿using AutoMapper;
using ClosedXML.Excel;
using DocumentFormat.OpenXml.ExtendedProperties;
using DocumentFormat.OpenXml.Office2016.Excel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using OONApi.Contracts.ApiRoutes;
using OONApi.Contracts.Requests;
using OONApi.Contracts.Requests.Queires;
using OONApi.Contracts.Responses;
using OONApi.Enum;
using OONApi.Models;
using OONApi.Services;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using static OONApi.Contracts.ApiRoutes.ApiRoutes;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace OONApi.Controllers
{
    [ApiController]
    [Authorize]
    public class AngelController : ControllerBase
    {
        private IAngelService _angelServie;
        private IAngelTypeService _angelTypeServie;
        private IMassageService _massageService;
        private IReceptionService _receptionService;
        private IMapper _mapper;
        private ILogger<AngelController> _logger;
        private ISettingService _settingService;
        private readonly AppDbContext _db;
        private IAngelPerformanceService _angelPerformanceService;
        private IAngelPointRedemtionsService _angelPointRedemtionsService;
        public AngelController(IAngelService angelService,IAngelTypeService angelTypeService, IMassageService massageService, IMapper mapper, ILogger<AngelController> logger, AppDbContext db, ISettingService settingService, IReceptionService receptionService, IAngelPerformanceService angelPerformanceService, IAngelPointRedemtionsService angelPointRedemtionsService)
        {
            _angelServie = angelService;
            _angelTypeServie = angelTypeService;
            _massageService = massageService;
            _receptionService = receptionService;
            _mapper = mapper;
            _logger = logger;
            _db = db;
            _settingService = settingService;
            _receptionService = receptionService;
            _angelPerformanceService = angelPerformanceService;
            _angelPointRedemtionsService = angelPointRedemtionsService;
        }

        [HttpGet(ApiRoutes.Angels.GetAll)]
        public async Task<IActionResult> GetAll([FromQuery] AngelSearchQuery paginationQuery)
        {
            try
            {
                var pagination = _mapper.Map<PaginationFilter>(paginationQuery);
                var angel = await _angelServie.GetAllAsync(paginationQuery, pagination);
                if (angel == null)
                {
                    return NotFound();
                }
                return Ok(new PagedResponse<AngelResponse>
                {
                    Data = _mapper.Map<List<AngelResponse>>(angel.OrderBy(a=>a.AngelType.Order)),
                    PageNumber = pagination.PageNumber,
                    PageSize = pagination.PageSize,
                    Keyword = pagination.Keyword,
                    OrderBy = pagination.OrderBy,
                    TotalRecords = pagination.TotalRecords,
                    OrderDirection = pagination.OrderDirection
                });
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }

        }

        [HttpGet(ApiRoutes.Angels.Get)]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                
                var angel = await _angelServie.GetByIdAsync(id);
                if (angel == null)
                {
                    return NotFound();
                }
                var angelResponse = _mapper.Map<AngelResponse>(angel);
                if (angelResponse == null)
                {
                    return NotFound();
                }
                var angelDebts = await _angelServie.GetAngelDebtByAngelId(id);
                if (angelDebts == null)
                {
                    return NotFound();
                }
                angelResponse.AngelDebts = _mapper.Map<List<AngelDebtResponse>>(angelDebts);

                return Ok(angelResponse);
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }

        }

        [HttpGet(ApiRoutes.Angels.GetRfid)]
        public async Task<IActionResult> GetByRfid(string rfid)
        {
            try
            {
                var angel = await _angelServie.GetByRfidAsync(rfid);
                if(angel == null)
                {
                    return NotFound();
                }
                var angelResponse = _mapper.Map<AngelResponse>(angel);

                var angelDebts = await _angelServie.GetAngelDebtByAngelId(angel.Id);
                if (angelDebts == null)
                {
                    return NotFound();
                }
                angelResponse.AngelDebts = _mapper.Map<List<AngelDebtResponse>>(angelDebts);

                return Ok(angelResponse);
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }

        [HttpPost(ApiRoutes.Angels.Create)]
        public async Task<IActionResult> Post([FromBody] AngelRequest request)
        {
            using (var transaction = await _db.Database.BeginTransactionAsync())
            {
                try
                {
                    Angel angel = new Angel
                    {
                        AngelTypeId = request.AngelTypeId,
                        Code = request.Code,
                        Firstname = request.Firstname,
                        Lastname = request.Lastname,
                        Nickname = request.Nickname,
                        Tel = request.Tel,
                        Status = request.Status,
                        WorkFromDate = DateTime.Now,
                        WorkToDate = null,
                        Rfid = request.Rfid
                    };
                    if (await _angelServie.IsDuplicateRfid(angel) && !String.IsNullOrEmpty(request.Rfid))
                    {
                        return BadRequest(new { Message = "พบข้อมูลการ์ดซ้ำหรือถูกใช้งานแล้ว" });
                    }
                    if (String.IsNullOrEmpty(angel.Rfid)) { angel.Rfid = null; }
                    await _angelServie.CreateAsync(angel);

                    List<AngelDebt> angelDebts = new List<AngelDebt>();
                    foreach (var item in request.AngelDebts)
                    {
                        AngelDebt angelDebt = new AngelDebt();
                        angelDebt.AngelId = angel.Id;
                        angelDebt.DebtTypeId = item.DebtTypeId;
                        angelDebt.Fee = item.Fee;
                        angelDebts.Add(angelDebt);
                    }

                    await _angelServie.CreateAngelDebtAsync(angelDebts);
                    await transaction.CommitAsync();
                    return Ok(_mapper.Map<AngelResponse>(angel));
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex.Message);
                    _logger.LogError(ex.StackTrace);
                    await transaction.RollbackAsync();
                    return BadRequest();
                }
            }
        }

        [HttpPut(ApiRoutes.Angels.Update)]
        public async Task<IActionResult> Put([FromRoute] int id, [FromBody] AngelRequest request)
        {

            using (var transaction = await _db.Database.BeginTransactionAsync())
            {
                try
                {
                    var angel = await _angelServie.GetByIdAsync(id);
                    if (angel == null)
                    {
                        return NotFound();
                    }
                    angel.AngelTypeId = request.AngelTypeId;
                    angel.Code = request.Code;
                    angel.Firstname = request.Firstname;
                    angel.Lastname = request.Lastname;
                    angel.Nickname = request.Nickname;
                    angel.Tel = request.Tel;
                    angel.Rfid = request.Rfid;

                    if (await _angelServie.IsDuplicateRfid(angel, true) && !String.IsNullOrEmpty(angel.Rfid))
                    {
                        return BadRequest(new { Message = "พบข้อมูลการ์ดซ้ำหรือถูกใช้งานแล้ว" });
                    }
                    if (String.IsNullOrEmpty(angel.Rfid))
                    {
                        angel.Rfid = null;
                    }
                    await _angelServie.UpdateAsync(angel);
                    _db.ChangeTracker.Clear();

                    List<AngelDebt> addAngelDebts = new List<AngelDebt>();
                    List<AngelDebt> updateAngelDebts = new List<AngelDebt>();
                    List<int> angelDebtIds = new List<int>();
                    foreach (var item in request.AngelDebts)
                    {
                        if (item.Id != null)
                        {
                            AngelDebt temp = new AngelDebt
                            {
                                Id = (int)item.Id,
                                AngelId = angel.Id,
                                DebtTypeId = item.DebtTypeId,
                                Fee = item.Fee
                            };
                            updateAngelDebts.Add(temp);
                        }
                        else
                        {
                            AngelDebt temp = new AngelDebt
                            {
                                AngelId = angel.Id,
                                DebtTypeId = item.DebtTypeId,
                                Fee = item.Fee
                            };
                            addAngelDebts.Add(temp);
                        }
                    }

                    if(addAngelDebts.Count > 0)
                    {
                        await _angelServie.CreateAngelDebtAsync(addAngelDebts);
                    }
                    if(updateAngelDebts.Count > 0)
                    {
                        await _angelServie.UpdateAngelDebtAsync(updateAngelDebts);
                    }

                    await transaction.CommitAsync();
                    return Ok(_mapper.Map<AngelResponse>(angel));
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex.Message);
                    _logger.LogError(ex.StackTrace);
                    if (ex.InnerException!=null)
                    {
                        _logger.LogError(ex.InnerException.Message);
                        _logger.LogError(ex.InnerException.StackTrace);
                    }
                    await transaction.RollbackAsync();
                    return BadRequest();
                }
            }
        }

        [HttpPut(ApiRoutes.Angels.Cancel)]
        public async Task<IActionResult> Delete([FromRoute] int id)
        {
            using (var transaction = await _db.Database.BeginTransactionAsync())
            {
                try
                {
                    Angel angels = await _angelServie.GetByIdAsync(id);
                    if (angels == null)
                    {
                        return NotFound();
                    }
                    angels.Rfid = null;
                    angels.Status = (int)EAngelStatus.Cancel;
                    await _angelServie.UpdateAsync(angels);
                    _db.ChangeTracker.Clear();

                    await transaction.CommitAsync();
                    //await _angelServie.DeleteAsync(id);
                    return Ok();
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex.Message);
                    _logger.LogError(ex.StackTrace);
                    return BadRequest();
                }
            }
        }

        [HttpPost(ApiRoutes.Angels.ManageWorking)]
        public async Task<IActionResult> ManageWorking([FromBody] ManageWorkingRequest request)
        {
            try
            {
                int carlendarDayId = await GetCalendarDayId();
                var angelWorking = await _angelServie.GetAngelWorking(request.AngelId, carlendarDayId);
                if (angelWorking == null && request.Status != null)
                {
                    AngelWorking working = new AngelWorking
                    {
                        AngelId = request.AngelId,
                        CalendarDayId = carlendarDayId,
                        GetInTime = DateTime.Now
                    };
                    await _angelServie.CheckInTimeAngel(working);
                  
                    var angel = await _angelServie.GetByIdAsync(working.AngelId);
                    angel.Status = (int)EAngelStatus.Ready;
                    await _angelServie.UpdateAsync(angel);
                }
                else if (angelWorking != null)
                {
                    TimeSpan sumTime = DateTime.Now - (DateTime)angelWorking.GetInTime;
                    int totalTime = sumTime.Hours;
                    var _angel = await _angelServie.GetByIdAsync(angelWorking.AngelId);
                    if (angelWorking.GetOutTime != null && request.Status == (int)EAngelStatus.Ready)
                    {
                        angelWorking.GetOutTime = null;
                        _angel.Status = (int)EAngelStatus.Ready;
                        await _angelServie.UpdateAsync(_angel);
                    }
                    else if (totalTime > 3)
                    {

                        angelWorking.GetOutTime = DateTime.Now;
                        await _angelServie.CheckOutTimeAngel(angelWorking);
                        var angel = await _angelServie.GetByIdAsync(angelWorking.AngelId);
                        angel.Status = (int)EAngelStatus.BackHome;
                        await _angelServie.UpdateAsync(angel);
                    }
                    else 
                    {
                        angelWorking.GetInTime = DateTime.Now;
                        _angel.Status = (int)EAngelStatus.Ready;
                        await _angelServie.UpdateAsync(_angel);
                    }
                }
              
               
                return Ok();
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }

        [HttpGet(ApiRoutes.Angels.GetAllWorking)]
        public async Task<IActionResult> GetAllWorking(DateTime? calendarDay)
        {
            try
            {
                var angelType = await _angelTypeServie.GetAllAsync(null);
                var angelTypeResponse = _mapper.Map<List<AngelWorkingListResponse>>(angelType);

                int calendarDayId = _settingService.GetCalendarId((DateTime)calendarDay);
                foreach (var item in angelTypeResponse)
                {
                    var _calendarDay = await _settingService.GetCalendarDayById(calendarDayId);
                    item.calendarDay = _mapper.Map<CalendarDayResponse>(_calendarDay);

                    var angelWorking = await _angelServie.GetAllWorkingAsync(item.Id, calendarDayId);
                    item.angelWorkings = _mapper.Map<List<AngelWorkingResponse>>(angelWorking);

                    foreach (var massageAngel in item.angelWorkings)
                    {
                        var massageAngels = await _massageService.GetMassageAngelByAngelIdAsync(massageAngel.AngelId, massageAngel.CalendarDayId);
                        massageAngel.angel.massageAngels = _mapper.Map<List<MassageAngelResponse>>(massageAngels);
                        
                        foreach (var reception in massageAngel.angel.massageAngels)
                        {
                            var receptions = await _receptionService.GetByIdAsync(reception.ReceptionId);
                            reception.Reception = _mapper.Map<ReceptionResponse>(receptions);
                        }
                    }
                }
                return Ok(angelTypeResponse);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }
        [HttpGet(ApiRoutes.Angels.GetHistoryDebt)]
        public async Task<IActionResult> GetHistoryDebt([FromQuery] AngelSearchQuery paginationQuery)
        {
            try
            {
                if(paginationQuery.Id != null || (paginationQuery.StartDate !=null && paginationQuery.EndDate != null))
                {
                    var angel = await _angelServie.GetByIdAsync((int)paginationQuery.Id);
                    var response = _mapper.Map<AngelHistoryDebtResponse>(angel);

                    List<int> calendarIds = new List<int>();
                    if (paginationQuery.StartDate != null && paginationQuery.EndDate != null)
                    {
                        calendarIds = await _settingService.GetCalendarId((DateTime)paginationQuery.StartDate, (DateTime)paginationQuery.EndDate);
                    }
                    var pagination = _mapper.Map<PaginationFilter>(paginationQuery);
                    var history = await _angelServie.GetHistoryDebt(calendarIds, paginationQuery, pagination);

                    response.AngelPerformanceDebts = _mapper.Map<List<AngelPerformanceDebtResponse>>(history);

                    return Ok(response);
                }
                else
                {
                    return NotFound();
                }
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }
        [HttpGet(ApiRoutes.Angels.GetSummaryTotalRoundMonth)]
        public async Task<IActionResult> GetTotalRoundMonth([FromQuery] StartDateEndDateRequestcs request)
        {
            try
            {
                var angelTypes = await _angelTypeServie.GetAllAsync(null);
                var calendarDays = await _settingService.GetCalendarDayStartDateAndEndDate(request.StartDate, request.EndDate);
                var response = _mapper.Map<List<AngelTypeSummaryTotalRoundMonthResponse>>(angelTypes);
                foreach (var angelType in response)
                {
                    var angels = await _angelServie.GetAngelByAngeltypeId(angelType.Id);
                    angelType.Angels = _mapper.Map<List<AngelSummaryTotalRoundMonthResponse>>(angels);
                    foreach(var angel in angelType.Angels)
                    {
                        angel.SummaryTotalRound = 0;
                        angel.CalendarDays = _mapper.Map<List<CalendarSummaryTotalRoundMonthResponse>>(calendarDays);
                        foreach(var calendar in angel.CalendarDays)
                        {
                            calendar.TotalRound = _angelPerformanceService.SummaryTotalRoundMonthByAngel(angel.Id, calendar.Id);
                            angel.SummaryTotalRound += calendar.TotalRound;
                        }
                    }
                    List<AngelSummaryTotalRoundMonthResponse> _angel = new List<AngelSummaryTotalRoundMonthResponse>();
                    foreach (var checkAngel in angelType.Angels)
                    {
                        if (checkAngel.SummaryTotalRound != 0)
                        {
                            _angel.Add(checkAngel);
                        }
                    }
                    angelType.Angels = _angel;
                }


                return Ok(response);
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }

        

        [HttpGet(ApiRoutes.Angels.GetSalaryCountDay)]
        public async Task<IActionResult> GetSalaryCountDay([FromQuery] StartDateEndDateRequestcs request)
        {
            try
            {
                var angel = await _angelServie.GetByIdAsync((int)request.AngelId);
                if(angel == null)
                {
                    return NotFound();
                }
                var response = _mapper.Map<AngelSalaryCountDayResponse>(angel);
                var calendarDays = await _settingService.GetCalendarDayStartDateAndEndDate(request.StartDate, request.EndDate);

                response.CalendarDays = _mapper.Map<List<CalendarSalaryCountDayResponse>>(calendarDays);
                var setting = await _settingService.GetSettingById(1);
                var responseSetting = _mapper.Map<SettingResponse>(setting);
                foreach (var calendarDay in response.CalendarDays)
                {
                    calendarDay.CountRound = _angelPerformanceService.SummaryTotalRoundMonthByAngel(angel.Id, calendarDay.Id);
                    var getTime = await _settingService.GetAngelWorking(angel.Id, calendarDay.Id);
                    if(getTime != null)
                    {
                        calendarDay.GetInTime = getTime.GetInTime;
                        if (getTime.GetOutTime != null)
                        {
                            calendarDay.GetOutTime = getTime.GetOutTime;
                        }
                        else
                        {
                            if (setting.IsCountDaily)
                            {
                                if (calendarDay.CountRound >= 2)
                                {
                                    calendarDay.IsDay = true;
                                    response.CountDay += 1;
                                }
                            }
                            else 
                            {
                                if (calendarDay.CountRound >= 3)
                                {
                                    calendarDay.IsDay = true;
                                    response.CountDay += 1;
                                }
                            }
                            continue;
                        }

                        DateTime startDay = new DateTime(calendarDay.ToDay.Year, calendarDay.ToDay.Month, calendarDay.ToDay.Day, 17, 59, 59);
                        DateTime endDay = new DateTime(calendarDay.ToDay.Year, calendarDay.ToDay.Month, calendarDay.ToDay.Day, 23, 59, 59);

                        if (calendarDay.GetInTime < startDay && calendarDay.GetOutTime > endDay)
                        {
                            if (calendarDay.CountRound >= 1)
                            {
                                calendarDay.IsDay = true;
                                response.CountDay += 1;
                            }
                        }
                        else
                        {
                            if (setting.IsCountDaily)
                            {
                                if (calendarDay.CountRound >= 2)
                                {
                                    calendarDay.IsDay = true;
                                    response.CountDay += 1;
                                }
                            }
                            else
                            {
                                if (calendarDay.CountRound >= 3)
                                {
                                    calendarDay.IsDay = true;
                                    response.CountDay += 1;
                                }
                            }
                        }
                    }
                }
                return Ok(response);
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }

        [HttpPost(ApiRoutes.Angels.Upload)]
        public async Task<IActionResult> Upload([FromForm] List<IFormFile> file, [FromForm] int angelId, [FromForm] int angelTypeId)
        {
            try
            {
                var angelTypes = await _angelTypeServie.GetAllAsync(null);
                var angelType = await _angelTypeServie.GetByIdAsync(angelTypeId);
                string pathDirectory = "wwwroot/categories/"+angelType.Code;

                foreach (var angelType_ in angelTypes)
                {
                    string currentDirectoryPath = "wwwroot/categories/"+angelType_.Code;

                    if (!Directory.Exists(currentDirectoryPath))
                    {
                        Directory.CreateDirectory(currentDirectoryPath);
                        _logger.LogDebug("file" + currentDirectoryPath);
                    }
                }

                foreach (var fileObj in file)
                {
                    string fileName = fileObj.FileName;
                    string path = Path.Combine(Directory.GetCurrentDirectory(), pathDirectory, fileName);
                    using (FileStream fileStream = new FileStream(path, FileMode.Create))
                    {
                        await fileObj.CopyToAsync(fileStream);
                    }

                    AngelGallery angelGallery = new AngelGallery
                    {
                        AngelId = angelId,
                        Path = angelType.Code + "/" + fileObj.FileName
                    };
                    await _angelServie.CreateAngelGalleryAsync(angelGallery);
                }

                return Ok();

            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }

        [HttpDelete(ApiRoutes.Angels.DeleteAngelsWorking)]
        public async Task<IActionResult> DeleteAngelsWorking(int id)
        {
             using (var transaction = await _db.Database.BeginTransactionAsync())
             {
                    try
                    {
                    
                        var angelWorking = await _angelServie.GetAngelWorkingById(id);
                        if (angelWorking == null)
                        {
                            return NotFound();
                        }
                        var angel = await _angelServie.GetByIdAsync(angelWorking.AngelId);
                        angel.Status = (int)EAngelStatus.BackHome;
                        await _angelServie.UpdateAsync(angel);
                        await _angelServie.DeleteAngelsWorking(id);
                        await transaction.CommitAsync();
                        return Ok();
                    }
                    catch (Exception ex)
                    {
                        _logger.LogError(ex.Message);
                        _logger.LogError(ex.StackTrace);
                        if (ex.InnerException != null)
                        {
                            _logger.LogError(ex.InnerException.Message);
                            _logger.LogError(ex.InnerException.StackTrace);
                        }
                        await transaction.RollbackAsync();
                        return BadRequest();
                    }
             }
              
        }

        [HttpPost(ApiRoutes.Angels.CreateAngelPointRedemtions)]
        public async Task<IActionResult> CreateAngelPointRedemtions([FromBody] AngelPointRedemtionRequest request) 
        {
            using (var transaction = await _db.Database.BeginTransactionAsync())
            {
                try
                {
                    int? calendarDayId = await GetCalendarDayId();
                    var angel = await _angelServie.GetByIdAsync(request.AngelId);
                    AngelPointRedemtion angelPointRedemtion = new AngelPointRedemtion();
                    angelPointRedemtion.AngelId = angel.Id;
                    angelPointRedemtion.CalendarDayId = (int)calendarDayId;
                    angelPointRedemtion.Point = angel.Point;
                    angelPointRedemtion.PointRedem = request.PointRedem;
                    angelPointRedemtion.Remark = request.Remark;

                    await _angelPointRedemtionsService.CreateAngelPointRedemtions(angelPointRedemtion);

                    angel.Point -= angelPointRedemtion.PointRedem;
                    await _angelServie.UpdateAsync(angel);

                    await transaction.CommitAsync();
                    return Ok();
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex.Message);
                    _logger.LogError(ex.StackTrace);
                    await transaction.RollbackAsync();
                    return BadRequest();
                }
            }
        }
        [HttpDelete(ApiRoutes.Angels.CancelAngelPointRedemtions)]
        public async Task<IActionResult> CancelAngelPointRedemtions(int id)
        {
            try
            {
                var angelPointRedemtion = await _angelPointRedemtionsService.GetAngelPointRedemtionByAngelId(id);
                if (angelPointRedemtion == null) 
                {
                    return NotFound();
                }
                var angel = await _angelServie.GetByIdAsync(angelPointRedemtion.AngelId);

                angel.Point += angelPointRedemtion.PointRedem;

                await _angelPointRedemtionsService.CancelAngelPointRedemtions(angelPointRedemtion.Id);
                await _angelServie.UpdateAsync(angel);
                return Ok();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }

        }
        [HttpGet(ApiRoutes.Angels.GetAngelPointRedemtionsHistory)]
        public async Task<IActionResult> GetAngelPointRedemtionsHistory([FromQuery] AngelPointRedemtionsHistorySearchQuery paginationQuery) 
        {
            try
            {
                var pagination = _mapper.Map<PaginationFilter>(paginationQuery);
                var angelPointRedemtionsHistory = await _angelPointRedemtionsService.GetAngelPointRedemtionsHistory(paginationQuery, pagination);
                if (angelPointRedemtionsHistory == null)
                {
                    return NotFound();
                }
                return Ok(new PagedResponse<AngelPointRedemtion>
                {
                    Data = _mapper.Map<List<AngelPointRedemtion>>(angelPointRedemtionsHistory),
                    PageNumber = pagination.PageNumber,
                    PageSize = pagination.PageSize,
                    Keyword = pagination.Keyword,
                    OrderBy = pagination.OrderBy,
                    TotalRecords = pagination.TotalRecords,
                    OrderDirection = pagination.OrderDirection
                });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }

        [HttpGet(ApiRoutes.Angels.GetAngelAllPoint)]
        public async Task<IActionResult> GetAngelAllPoint([FromQuery] AngelSearchQuery paginationQuery)
        {
            try
            {
                var pagination = _mapper.Map<PaginationFilter>(paginationQuery);
                var angel = await _angelServie.GetAllAsync(paginationQuery, pagination);
                if (angel == null)
                {
                    return NotFound();
                }
                return Ok(new PagedResponse<AngelResponse>
                {
                    Data = _mapper.Map<List<AngelResponse>>(angel.OrderByDescending(i => i.Point)),
                    PageNumber = pagination.PageNumber,
                    PageSize = pagination.PageSize,
                    Keyword = pagination.Keyword,
                    OrderBy = pagination.OrderBy,
                    TotalRecords = pagination.TotalRecords,
                    OrderDirection = pagination.OrderDirection
                });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }

        }

        //[HttpPost(ApiRoutes.Angels.GetGallery)]
        //public async Task<IActionResult> GetGallery(int AngelTypeId)
        //{
        //    var angelGallery = await _angelServie.
        //}

        private async Task<int> GetCalendarDayId()
        {
            var calendayDay = await _settingService.GetCarlendarDay();
            return calendayDay.Id;
        }


    }
}
