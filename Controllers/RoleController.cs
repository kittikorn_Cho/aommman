﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using OONApi.Contracts.ApiRoutes;
using OONApi.Contracts.Requests;
using OONApi.Contracts.Requests.Queires;
using OONApi.Contracts.Responses;
using OONApi.Models;
using OONApi.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace OONApi.Controllers
{
    [ApiController]
    public class RoleController : ControllerBase
    {
        private IRoleService _roleService;
        private IMapper _mapper;
        private IUserService _userService;
        private ILogger<RoleController> _logger;

        public RoleController(IRoleService roleService, IUserService userService, IMapper mapper, ILogger<RoleController> logger)
        {
            _roleService = roleService;
            _userService = userService;
            _mapper = mapper;
            _logger = logger;
        }

        [HttpGet(ApiRoutes.Roles.GetAll)]
        public async Task<IActionResult> GetAll([FromQuery] RoleSearchQuery paginationQuery)
        {
            try
            {
                int userId = int.Parse(User.Identity.Name);
                var _user = await _userService.GetByIdAsync(userId);
                bool isRole = false;
                foreach (var _role in _user.Roles)
                {
                    if (_role.Code == "_SUPERADMIN")
                    {
                        isRole = true;
                        break;
                    }
                }

                var pagination = _mapper.Map<PaginationFilter>(paginationQuery);
                var role = await _roleService.GetAllAsync(paginationQuery, pagination, isRole);
                if (role == null)
                {
                    return NotFound();
                }
                return Ok(new PagedResponse<RoleResponse>
                {
                    Data = _mapper.Map<List<RoleResponse>>(role),
                    PageNumber = pagination.PageNumber,
                    PageSize = pagination.PageSize,
                    Keyword = pagination.Keyword,
                    OrderBy = pagination.OrderBy,
                    TotalRecords = pagination.TotalRecords,
                    OrderDirection = pagination.OrderDirection,
                }) ;
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }

        [HttpGet(ApiRoutes.Roles.Get)]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                var role = await _roleService.GetByIdAsync(id);

                if (role == null)
                {
                    return NotFound();
                }

                var roleResponse = _mapper.Map<RoleResponse>(role);

                return Ok(roleResponse);         
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }

        [HttpPost(ApiRoutes.Roles.Create)]
        public async Task<IActionResult> Post([FromBody] RoleRequest request)
        {
            try
            {
                Role role = new Role
                {
                    Code = request.Code,
                    Name = request.Name,
                };
                await _roleService.CreateAsync(role);

                //List<RoleMenu> list = new List<RoleMenu>();
                //foreach(var menuId in request.MenuIds)
                //{
                //    RoleMenu roleMenu = new RoleMenu();
                //    roleMenu.RoleId = role.Id;
                //    roleMenu.MenuId = menuId;
                //    list.Add(roleMenu);
                //}

                await _roleService.CreateRoleMenu(role, request.MenuIds, "create");

                return Ok(_mapper.Map<Role>(role));
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }

        [HttpPut(ApiRoutes.Roles.Update)]
        public async Task<IActionResult> Put([FromRoute] int id, [FromBody] RoleRequest request)
        {
            try
            {
                var role = await _roleService.GetByIdAsync(id);
                if (role == null)
                {
                    return NotFound();
                }
                Role update = role;
                update.Code = request.Code;
                update.Name = request.Name;
                await _roleService.UpdateAsync(update);

                await _roleService.CreateRoleMenu(update, request.MenuIds, "update");
                return Ok(_mapper.Map<RoleResponse>(update));
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }

        [HttpDelete(ApiRoutes.Roles.Delete)]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await _roleService.DeleteAsync(id);
                return Ok();
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }
    }
}
