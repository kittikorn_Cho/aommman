﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using OONApi.Contracts.ApiRoutes;
using OONApi.Contracts.Requests;
using OONApi.Contracts.Responses;
using OONApi.Models;
using OONApi.Services;
using System;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace OONApi.Controllers
{
    [ApiController]
    [Authorize]
    public class SettingController : ControllerBase
    {
        private ISettingService _settingService;
        private IMapper _mapper;
        private ILogger<SettingController> _logger;
        private readonly AppDbContext _db;

        public SettingController(ISettingService settingService, IMassageService massageService, IAgencyService agencyService, IMapper mapper, ILogger<SettingController> logger, AppDbContext db)
        {
            _settingService = settingService;
            _mapper = mapper;
            _logger = logger;
            _db = db;
        }

        [HttpGet(ApiRoutes.Settings.Get)]
        public async Task<IActionResult> GetById(int id)
        {
            try
            {
                var setting = await _settingService.GetSettingById(id);
                if (setting == null)
                {
                    return NotFound();
                }
                var response = _mapper.Map<SettingResponse>(setting);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }

        [HttpPut(ApiRoutes.Settings.Update)]
        public async Task<IActionResult> Put([FromRoute] int id, [FromBody] SettingRequest request)
        {
            try
            {
                var setting = await _settingService.GetSettingById(id);
                if (setting == null)
                {
                    return NotFound();
                }
                Setting update = setting;
                update.PointAngel = request.PointAngel;
                update.PointMember = request.PointMember;
                update.PointReception = request.PointReception;

                await _settingService.UpdateSetting(update);
                return Ok();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }
        }
       
    }
}
