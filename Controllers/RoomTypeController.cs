﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using OONApi.Contracts.ApiRoutes;
using OONApi.Contracts.Requests;
using OONApi.Contracts.Requests.Queires;
using OONApi.Contracts.Responses;
using OONApi.Enum;
using OONApi.Helpers;
using OONApi.Models;
using OONApi.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace OONApi.Controllers
{
    [ApiController]
    [Authorize]
    public class RoomTypeController : ControllerBase
    {
        private IMapper _mapper;
        private IRoomTypeService _roomTypeService;
        private IRoomService _roomService;
        private ILogger<RoomTypeController> _logger; 

        public RoomTypeController(IMapper mapper, IRoomTypeService roomTypeService,IRoomService roomService, ILogger<RoomTypeController> logger)
        {
            _mapper = mapper;
            _roomTypeService = roomTypeService;
            _roomService = roomService;
            _logger = logger;
        }

        [HttpGet(ApiRoutes.RoomTypes.GetAll)]
        public async Task<IActionResult> GetAll([FromQuery] PaginationQuery paginationQuery)
        {
            try
            {
                var pagination = _mapper.Map<PaginationFilter>(paginationQuery);
                var roomTypes = await _roomTypeService.GetAllAsync(pagination);
                if (roomTypes == null)
                {
                    return NotFound();
                }
                return Ok(new PagedResponse<RoomTypeResponse>
                {
                    Data = _mapper.Map<List<RoomTypeResponse>>(roomTypes),
                    PageNumber = pagination.PageNumber,
                    PageSize = pagination.PageSize,
                    Keyword = pagination.Keyword,
                    OrderBy = pagination.OrderBy,
                    TotalRecords = pagination.TotalRecords,
                    OrderDirection = pagination.OrderDirection,
                });
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }

        }


        [HttpGet(ApiRoutes.RoomTypes.Get)]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                var roomType = await _roomTypeService.GetByIdAsync(id);
                if (roomType == null)
                {
                    return NotFound();
                }
                return Ok(_mapper.Map<RoomTypeResponse>(roomType));
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }

        }

        [HttpPost(ApiRoutes.RoomTypes.Create)]
        public async Task<IActionResult> Post([FromBody] RoomTypeRequest request)
        {
            try
            {
                RoomType roomType = new RoomType
                {
                    Code = request.Code,
                    Name = request.Name,
                    RoomRate = request.RoomRate,
                    RoomRateNext = request.RoomRateNext,
                    RoundTime = request.RoundTime
                };
                await _roomTypeService.CreateAsync(roomType);
                return Ok(_mapper.Map<RoomTypeResponse>(roomType));
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }

        }

        [HttpPut(ApiRoutes.RoomTypes.Update)]
        public async Task<IActionResult> Put(int id, [FromBody] RoomTypeRequest request)
        {
            try
            {
                var roomType = await _roomTypeService.GetByIdAsync(id);
                if (roomType == null)
                {
                    return NotFound();
                }
                RoomType update = roomType;
                update.Code = request.Code;
                update.Name = request.Name;
                update.RoomRate = request.RoomRate;
                update.RoomRateNext = request.RoomRateNext;
                update.RoundTime = request.RoundTime;
                await _roomTypeService.UpdateAsync(update);
                return Ok(_mapper.Map<RoomTypeResponse>(update));
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }

        }


        [HttpDelete(ApiRoutes.RoomTypes.Delete)]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await _roomTypeService.DeleteAsync(id);
                return Ok();
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }

        }

        [HttpGet(ApiRoutes.RoomTypes.GetRoomByRoomTypeId)]
        public async Task<IActionResult> GetRoomByRoomTypeId()
        {
            try
            {
                var roomTypes = await _roomTypeService.GetAllAsync();
                if (roomTypes == null)
                {
                    return NotFound();
                }
                var roomTypeResponse = _mapper.Map<List<RoomTypeChildRoomResponse>>(roomTypes);
                if (roomTypeResponse == null)
                {
                    return NotFound();
                }
                foreach (var roomType in roomTypeResponse)
                {
                    var room = await _roomService.GetRoomByRoomTypeId(roomType.Id);
                    if (room == null)
                    {
                        return NotFound();
                    }
                    roomType.Rooms = _mapper.Map<List<RoomResponse>>(room);

                    foreach(var item in roomType.Rooms)
                    {
                        var parentRoom = await _roomService.GetChildRoomParent(item.Id);

                        item.ParentRooms = _mapper.Map<List<RoomParentResponse>>(parentRoom);
                    }
                }
                return Ok(roomTypeResponse);
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return BadRequest();
            }

        }
    }
}
